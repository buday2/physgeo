QT       += core gui
QT       += widgets

CONFIG += c++20

QMAKE_CXXFLAGS += -fopenmp
QMAKE_CXXFLAGS += -larmadillo
QMAKE_CXXFLAGS += -lhdf5
QMAKE_CXXFLAGS += -O3
#QMAKE_LFLAGS += -static
LIBS += -fopenmp
LIBS += -larmadillo
LIBS += -lhdf5

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    src/bouguer.cpp \
    src/ggfm_meat.cpp \
    src/ggfm.cpp \
    src/geodetic_functions.cpp \
    src/gprism.cpp \
    src/phys_math.cpp \
    src/tess_taylor_coeff.cpp \
    src/physical_constants.cpp \
    src/loadpoints.cpp \
    src/interpolations.cpp \
    src/geodetic_sphere.cpp \
    #src/elliptic_int.cpp \ # not finished yes
    src/dms.cpp \
    src/tesseroid.cpp \
    src/legendre.cpp \
    src/fnalfs.cpp   \
    src/points.cpp \
    src/timer.cpp \
    src/isgemgeoid.cpp \
    src/progressbar.cpp \
    src/my_vector.cpp \
    src/gaussquad.cpp \
    src/tessquad.cpp \
    src/my_parser.cpp \
    src/lvec.cpp \
    src/tmat.cpp \

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    include/bouguer.h \
    include/ggfm_meat.h \
    include/ggfm.h \
    include/geodetic_functions.h \
    include/ggfm_meat.h \
    include/gprism.h \
    include/phys_math.h \
    include/tess_taylor_coeff.h \
    #include/elliptic_int.h \ # not finished yet
    include/geodetic_sphere.h \
    include/interpolations.h \
    include/loadpoints.h \
    include/physical_constants.h \
    include/dms.h \
    include/tesseroid.h \
    include/legendre.h \
    include/fnalfs.h \
    include/points.h \
    include/timer.h \
    include/isgemgeoid.h \
    include/progressbar.h \
    include/my_vector.h \
    include/gaussquad.h \
    include/tessquad.h \
    include/my_parser.h \
    include/lvec.h \
    include/tmat.h \
    

DISTFILES += \
    py_gdist.py \
    pysrc/Ggfm.py \
    pysrc/__init__.py \
    pysrc/create_test_data.py \
    pysrc/bouguer.py \
    pysrc/gbvp.py \
    pysrc/image.py \
    pysrc/plot_isg.py \
    py_main.py \
    py_main3.py \
    pysrc/kostelecky_gdat.py \
    pysrc/legen_iterative.py \
    pysrc/geodetic_functions.py \
    pysrc/dms.py \
    pysrc/stokes.py \
    pysrc/great_circle.py \
    pysrc/normal_dist_test.py
