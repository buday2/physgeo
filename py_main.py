# -*- coding: utf-8 -*-
""" NUMPY LIB """
import numpy as nps

""" MY LIB """
from pysrc.plot_isg         import Isgem_model , Terrain, Gravity, Gravity2, Grayscale, Grayscale2
from pysrc                  import geodetic_functions as geo_f

import pysrc.great_circle as gcirc
import pysrc.tesseroid    as tess

""" MATPLOTLIB """
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import scipy.ndimage as ndimage
""" PYTHON3 """
import os, sys

""" BASEMAP PLOT """
try:
    from mpl_toolkits.basemap import Basemap
except:
    raise ImportError("Unable to locate the Basemap toolkit on your computer.")

sign = lambda a: (a>0) - (a<0)
""" ----------------------------------------------------------------------------------------- """

def main() -> None:
    print("Welcome to the PhysGeo python part")
    tess.plot_tess( )


if __name__ == "main":
    main()




