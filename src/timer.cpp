#include "../include/timer.h"

Timer::Timer( const std::string& info_timer ) {
    t_timer = info_timer;
    t_init_time = omp_get_wtime();
}

Timer::~Timer(){
    dms hours; hours.deg2dms( ( -this->t_init_time + omp_get_wtime()) / 3600. );
    std::cout << "\n\n========" << setw(40) << t_timer << "========" << std::endl;
    std::cout << "== Total computation time: " << setw(8) << setprecision(5)
                                               << hours
                                               << "  ===="     << std::endl;
    std::cout << "========================================================" << std::endl;
}
