#include "../include/points.h"

inline int first_char (const char* c){
    int i = 0;
    while (c[i]){
        if ( isspace(c[i])){
            i++;
        } else {
            return i;
        }
    }
    return 0;
}

// Constructor
Points::Points()
{

}

//Destructor
Points::~Points() {

}

void Points::clear(){
    pointsID.clear();
    coords.clear();
}

void Points::load_from_file(const string &path, const string &comments)
{
    Points::clear();

    ifstream in_file( path );
    if ( in_file.is_open() ) {

        string line, word;
        vector<string> vec_word;

        unsigned int nrows = 0, ncols = 0;

        while ( !in_file.eof() ) {
            getline( in_file, line );

            if ( line.empty() ) {
                continue;
            }

            int st = first_char( line.c_str() );
            if ( line.at(st) == comments.at(0) ){
                continue; // commented lines
            } else {
                nrows++;

                stringstream is( line );
                while ( is >> word ){
                    vec_word.push_back( word );
                }

                unsigned columns_i =  vec_word.size();
                ncols = ( columns_i > ncols ) ? columns_i : ncols;

                vec_word.clear();
            }
        }

        in_file.clear();
        in_file.seekg(0, ios::beg);

        //== Loading data to Points class ==//
        pointsID.resize( nrows );
        coords.resize( nrows, ncols - 1 );
        nrows = 0;

        while ( !in_file.eof() ) {
            getline( in_file, line );

            if ( line.empty() ) {
                continue;
            }

            int st = first_char( line.c_str() );
            if ( line.at(st) == comments.at(0) ){
                continue; // commented lines
            } else {
                stringstream is( line );
                while ( is >> word ){
                    vec_word.push_back( word );
                }

                pointsID[nrows] = vec_word[0];

                for ( unsigned j = 1; j < vec_word.size(); j++) {
                    coords(nrows, j-1) = stod( vec_word[j] );
                }

                vec_word.clear();
                nrows++;
            }
        }
    } else {
        cout << "File \"" << path << "\" does not exist! Function \"Points::load_from_file()\" failed to load data." << endl;
    }
    in_file.close();
}


void Points::load_from_file(const string &path,
                            const string &comments,
                            unsigned int noc) {
    vector<string> vec_word;

    ifstream in_file( path );

    if ( in_file.is_open() ) {
        // loading Data

        Points::clear();
        while ( !in_file.eof() ) {
            string line, word;

            getline( in_file, line );

            if ( line.empty() ) {
                continue;
            }

            int st = first_char( line.c_str() );
            if ( line.at(st) == comments.at(0) ){
                continue; // commented lines
            }

            stringstream is( line );
            while ( is >> word ){
                vec_word.push_back( word );
            }

            pointsID.push_back( vec_word[0] );


            arma::rowvec crdline( vec_word.size() -1  );
            for ( unsigned int i = 1; i<= noc; i++) {
                crdline(i-1) =  stod( vec_word[i] ) ;
            }

            vec_word.clear();
            coords = arma::join_cols( coords  , crdline );
        }
    } else {
        cout << "File \"" << path << "\" does not exist! Function \"Points::load_from_file\" failed to load data." << endl;
    }
    in_file.close();
}

void Points::rwhch_from_beg( string& sentence ) {
    while (  sentence.length() != 0 ) {
        if ( isspace(sentence[0]) ) {
            sentence.erase (sentence.begin(), sentence.begin()+1);
        } else {
            break;
        }
    }
}
