#include "../include/my_parser.h"

my_parser::my_parser()
{
    // constructor
}

my_parser::~my_parser()
{
    // destructor
}

int my_parser::execute(const int& argc, char* argv[])
{
        po::options_description options("PhysGeo options");
        options.add_options()
        // First parameter describes option name/short name
        // The second is parameter to option
        // The third is description

        // No argument required 
        ("help,h", "Print help message. See option --help-module for further information on separate modules. Since some of the command line options are module specific.")
        ("info,i", "Get info about \"PhysGeo\" project.")
        ("license", "Get the LICENSE conditions for \"PhysGeo\" usage.")
        ("version,v", "Display version of the program.")

        // Specifying the arguments
        ("algorithm,a", 
            po::value<std::string>(&algorithm_option)->default_value("standard"), 
                "Specify the algorithm type for computing the " \
                "fnALFs")
        ("boundaries,b", 
            po::value<std::string>(&boundary_string), 
            "Parse the boundaries for various computations (such as ggfm), " \
            "example: \"-90.2 12.5 12.5 36.\"")
        ("computation-type,c", 
            po::value<std::vector<std::string> >(&computation_type)->multitoken(), 
            "Specify what computation should be done.")
        ("coordinate-system,crd",
            po::value<std::string>(&crd_system)->default_value("ell"),
                "Select either ellipsoidal or spherical coordinate system.")
        ("degree-range,d", 
            po::value<std::vector<unsigned int> >(&degree_range)->multitoken(), 
            "Specify the range of GGFM model degree [min, max]. "\
            "If not specified the whole possible range is taken into account.")
        ("density,d", 
            po::value<std::vector<std::string> >(&density_options)->multitoken(), 
            "Some clever density description.")
        ("ellipsoid,e", 
            po::value<std::string>(&ellipsoid)->default_value("wgs84"), 
            "The reference ellipsoid for the computations. Default value is \"wgs84\".")
        ("extra-data",
            po::value<std::vector<std::string> >(&extra_data)->multitoken(),
            "To be able to provide additional data or options to some specific computations.")
        ("extend-data,x", 
            po::value<std::string>(&extend_data), 
            "Specify if the data should be extended when computing the Stokes'/Hotine's integral around the whole globe.")
        ("file,f",
            po::value<std::string>(&file_name), 
            "Name of the input file with points coordinates.")
        ("help-module",  
            po::value<std::string>(&help_option), 
            "Get further help fo module.")
        ("kernel,k", 
            po::value<std::vector<std::string> >(&kernel_options)->multitoken(), 
                "Kernel options when computing the Stokes' or Hotine's "\
                "integral.")
        ("love-number,l", 
            po::value<double>(&love_number)->default_value(0.29525), 
            "Love number for C20 term transformation.")
        ("model,m", 
            po::value<std::vector<std::string> >(&model_parameters)->multitoken(), 
            "Path to GGFM model, also possible to pass additional parameters.")
        ("output,o", 
            po::value<std::string>(&output_file)->default_value("result.txt"),  
            "Output file")
        ("parameters,p", 
            po::value<std::vector<std::string> >(&parameters)->multitoken(), 
            "The parameter of the Earth's gravity field to calculate.")
        ("radius,r", 
            po::value<std::vector<std::string> >(&radius_options)->multitoken(), 
            "Some clever description for the radius.")
        ("solution,s", 
            po::value<std::vector<std::string> >(&solution_options)->multitoken(), 
            "Specify the solution, such as \"sp\", \"grid\", \"ongrid\". See help for more info.")
        ("tell-me-more", 
            po::value<std::string>(&tell_me_more), 
            "Get more information about parser options. Example: \"--tell-me-more ellipsoid\".")
        ("tide-system,n", 
            po::value<std::string>(&tide_system), 
            "Permanent Tide system managment. Possible options: \"mean_tide\", " \
            "zero_tide\", \"tide_free\". If not specified the tidal system of the GGFM is used.");


    /// And the battle begins
    try {
        po::variables_map vm;
        po::command_line_parser parser(argc, argv);

        parser.options(options).allow_unregistered().style(
            po::command_line_style::unix_style |
            po::command_line_style::allow_slash_for_short   
        );

        po::store(parser.run(), vm);
        po::notify(vm);

        // Handling the options such as help, verions, help-module
        // the options that returns more information, not doing
        // any computations
        if( vm.count("help"))
        {
            std::cout << options << std::endl;
            return SUCCESS;
        }

        if (vm.count("help-module"))
        {
            //this->help_option = vm["help-module"].as<std::string>();

            if ( help_option.compare("ggfm") == 0 )
            {
                my_parser::generate_ggfm_module_help();
                return SUCCESS;
            } else if ( help_option.compare("tess") == 0  ) {
                my_parser::generate_fgfm_module_help();
                return SUCCESS;
            } else {
                std::cerr << "No valid option for \"--help-module\" provided." << std::endl
                          << "Option provided was: " << this->help_option << std::endl;
                return ERROR_IN_COMMAND_LINE;
            }
        }

        if (vm.count("version"))
        {
            std::cout << "PhysGeo version : " << PROGRAM_VERSION << std::endl;
            return SUCCESS;
        }  

        if (vm.count("license")) 
        {
            my_parser::get_license();
            return SUCCESS;
        }

        if (vm.count("info")) 
        {
            my_parser::generate_physgeo_info();
            return SUCCESS;
        }
    
        // Set/get the reference ellipsoid
        if (vm.count("ellipsoid"))
        {
            if (ellipsoid.compare("grs80") == 0){
                ell = geo_f::get_grs80<double>();
                ell_ld = geo_f::get_grs80<long double>();
            } else if (ellipsoid.compare("bessel") == 0) {
                ell = geo_f::get_bessel<double>();
                ell_ld = geo_f::get_bessel<long double>();
            } else {
                ell = geo_f::get_wgs84<double>();
                ell_ld = geo_f::get_wgs84<long double>();
            }
        } else {
            ell = geo_f::get_wgs84<double>();
            ell_ld = geo_f::get_wgs84<long double>();
        }

        if (vm.count("boundaries"))
        {   // parse the string into vector;
            this->boundaries = my_parser::parse_boundaries(boundary_string);
        }

        // ** Start the timer ***
        Timer T( " =| PhysGeo program |= " );

        if (vm.count("computation-type"))
        {
            //==============================================================================================//
            // G L O B A L    G R A V I T Y     F I E L D     M O D E L S                                   //
            //==============================================================================================//
            if (computation_type[0].compare("ggfm") == 0)
            {
                // Usefull variables
                std::string model_format; // "standard isgem or converted to hdf5"
                bool overwrite_zero_term; // change the C_{0,0} term in model
                std::string tidesys; // what tide system should be used for the computation
                bool extended_precision; // true if the <long double> instance of ggfm should be used, false other wise

                double ln; // Love's number in <double> precision
                long double ln_ld; // Love's number in <long double> precision

                if (my_parser::do_vector_contains<std::string>(computation_type, "ld") ||
                    my_parser::do_vector_contains<std::string>(computation_type, "extended") ) {
                    extended_precision = true;
                } else {
                    extended_precision = false;
                }

                // Declaring the objects for GGFMs
                std::cout << "The GGFM computation type was chosen. Now the PhysGeo program" << std::endl
                          << "will do its best for his or hers user." << std::endl;

                Ggfm<double> gfieldModel;         // <double>
                Ggfm<long double> gfieldModel_ld; // <long double>

                // Algorithm type used for computing the Fully normalized
                // associated Legendre's polynomials
                if (vm.count("algorithm"))
                {
                    // check the option, but probably not
                    // even needed
                }

                if( !vm.count("solution"))
                {
                    std::cerr << "Option --solution,-s is mandatory for the GGFM computations. Program ends here" << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }

                if( !vm.count("parameters"))
                {
                    std::cerr << "Option --parameters,-p is mandatory for the GGFM computations. Program ends here" << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }


                // === Loading the model ===
                if( vm.count("model"))
                {
                    // first read all of the additional parameters
                    model_format = (my_parser::do_vector_contains<std::string>(model_parameters, "hdf5")) ? "hdf5" : "default";
                    overwrite_zero_term = (my_parser::do_vector_contains<std::string>(model_parameters, "keep_zt")) ? false : true;

                    // Path to the model should always be the first argument
                    std::cout << ".... Loading the GGFM model: "
                              << model_parameters[0] << std::endl;

                    if (extended_precision) 
                    {
                        if (model_format.compare("hdf5") == 0) {
                            gfieldModel_ld.load_hdf5_model( model_parameters[0], algorithm_option);
                        } else {
                            gfieldModel_ld.icgem_loadggm(model_parameters[0], algorithm_option, overwrite_zero_term);
                        }
                    } else {
                         if (model_format.compare("hdf5") == 0) {
                            gfieldModel.load_hdf5_model( model_parameters[0], algorithm_option);
                        } else {
                            gfieldModel.icgem_loadggm(model_parameters[0], algorithm_option, overwrite_zero_term);
                        }
                    }

                    std::cout << "Model successfully loaded." << std::endl;
                } else {
                    std::cerr << "The argument \"--model\" is mandatory. No Global gravity field model was provided." << std::endl;
                    throw new std::runtime_error("The argument \"--model\" is mandatory. No Global gravity field model was provided.");
                }


                // degree and order of the computation
                if (!vm.count("degree-range"))
                {
                    degree_range.resize(2);

                    if ( extended_precision ) {
                        gfieldModel_ld.get_degree_and_order( degree_range[0] , degree_range[1] );
                    } else {
                        gfieldModel.get_degree_and_order( degree_range[0] , degree_range[1] );
                    }
                } else {
                    // If set via command line option, check if contains two arguments
                    if (!(degree_range.size() == 2))
                    {
                        degree_range.resize(2);

                        if ( extended_precision ) {
                            gfieldModel_ld.get_degree_and_order( degree_range[0] , degree_range[1] );
                        } else {
                            gfieldModel.get_degree_and_order( degree_range[0] , degree_range[1] );
                        }
                    }
                }

                // === setting the tide system ===
                if (vm.count("tide-system"))
                {
                    // check if the value provided is from 3 possible options, from model otherwise
                    if ( !(tide_system.compare("zero_tide") == 0 || 
                           tide_system.compare("mean_tide") == 0 || 
                           tide_system.compare("tide_free") == 0)) 
                    {
                        if (extended_precision) {
                            tidesys = gfieldModel_ld.model_tide_system();
                        } else {
                            tidesys = gfieldModel.model_tide_system();
                        }
                    }
                } else {
                    // if not set, the value is obtained from GGFM
                    if (extended_precision) {
                        tidesys = gfieldModel_ld.model_tide_system();
                    } else {
                        tidesys = gfieldModel.model_tide_system();
                    }
                }

                // Begining of the computations
                // solution can be "sp" for single point
                // "grid" to compute grid values defined in vector boundaries
                // "ongrid" approach when the gradient approach is used
                if (solution_options[0].compare("sp") == 0)
                { // <<<<< single points >>>>>>
                    // File with points <pointID b[deg] l[deg] , hel[m]
                    Points Pts;
                    
                    if (vm.count("file")) {
                        Pts.load_from_file(file_name, "#");
                    } else {
                        std::cerr << "--file/-f no input file with coordinates given. Program ends here" << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }

                    // declaring armadillo matrix to store the results
                    unsigned itmax = Pts.coords.n_rows;
                    unsigned nc = parameters.size();
                    std::string grav_parameter;

                    arma::mat computed(itmax, nc);

                    for ( size_t i = 0; i < parameters.size();  i++) {
                        // pointer to class member function
                        unsigned iui = static_cast<unsigned>(i);
                        grav_parameter = parameters[i];

                        std::cout << "\nComputing SP (GGFM) : quantity " << grav_parameter << std::endl;

                        double (Ggfm<double>::*ptr2function) (const double& ,
                                                            const double& ,
                                                            const double& ,
                                                            const unsigned int& ,
                                                            const unsigned int& ,
                                                            const std::string& ,
                                                            const double& ,
                                                            const geo_f::ellipsoid<double>& );

                        long double (Ggfm<long double>::*ptr2function_ld) (const long double& ,
                                                                        const long double& ,
                                                                        const long double& ,
                                                                        const unsigned int& ,
                                                                        const unsigned int& ,
                                                                        const std::string& ,
                                                                        const long double& ,
                                                                        const geo_f::ellipsoid<long double>& );
                        // end - pointer to class member function


                        if ( grav_parameter.compare("gpot_v") == 0) {
                            ptr2function = &Ggfm<double>::gpot_v;
                            ptr2function_ld = &Ggfm<long double>::gpot_v;
                        } else if (  grav_parameter.compare("gpot_vr") == 0) {
                            ptr2function = &Ggfm<double>::gpot_vr;
                            ptr2function_ld = &Ggfm<long double>::gpot_vr;
                        } else if (  grav_parameter.compare("gpot_vrr") == 0  ) {
                            ptr2function = &Ggfm<double>::gpot_vrr;
                            ptr2function_ld = &Ggfm<long double>::gpot_vrr;
                        } else if (  grav_parameter.compare("gpot_w") == 0) {
                            ptr2function = &Ggfm<double>::gpot_w;
                            ptr2function_ld = &Ggfm<long double>::gpot_w;
                        } else if ( grav_parameter.compare("gpot_t") == 0 ) {
                            ptr2function = &Ggfm<double>::gpot_t;
                            ptr2function_ld = &Ggfm<long double>::gpot_t;
                        } else if ( grav_parameter.compare("grav_gy") == 0 ) {
                            ptr2function = &Ggfm<double>::grav_gy;
                            ptr2function_ld = &Ggfm<long double>::grav_gy;
                        } else if ( grav_parameter.compare("grav_gx") ==0 ) {
                            ptr2function = &Ggfm<double>::grav_gx;
                            ptr2function_ld = &Ggfm<long double>::grav_gx;
                        } else if ( grav_parameter.compare("grav_gz") == 0 ) {
                            ptr2function = &Ggfm<double>::grav_gz;
                            ptr2function_ld = &Ggfm<long double>::grav_gz;
                        } else if ( grav_parameter.compare("def_vert_eta") == 0 ) {
                            ptr2function = &Ggfm<double>::def_vert_eta;
                            ptr2function_ld = &Ggfm<long double>::def_vert_eta;
                        } else if ( grav_parameter.compare("def_vert_xi") ==0 ) {
                            ptr2function = &Ggfm<double>::def_vert_xi;
                            ptr2function_ld = &Ggfm<long double>::def_vert_xi;
                        } else if ( grav_parameter.compare("def_vert") == 0 ) {
                            ptr2function = &Ggfm<double>::def_vertical;
                            ptr2function_ld = &Ggfm<long double>::def_vertical;
                        } else if ( grav_parameter.compare("grav_anom_sa") == 0 ) {
                            ptr2function = &Ggfm<double>::grav_anom_sa;
                            ptr2function_ld = &Ggfm<long double>::grav_anom_sa;
                        } else if ( grav_parameter.compare("grav_anom_sa_r") == 0 ) {
                            ptr2function = &Ggfm<double>::grav_anom_sa_r;
                            ptr2function_ld = &Ggfm<long double>::grav_anom_sa_r;
                        } else if ( grav_parameter.compare("grav_anom_sa_rr") == 0 ) {
                            ptr2function = &Ggfm<double>::grav_anom_sa_rr;
                            ptr2function_ld = &Ggfm<long double>::grav_anom_sa_rr;
                        } else if ( grav_parameter.compare("grav_dist_sa") == 0 ) {
                            ptr2function = &Ggfm<double>::grav_dist_sa;
                            ptr2function_ld = &Ggfm<long double>::grav_dist_sa;
                        } else if ( grav_parameter.compare("height_anomaly") ==0 ) { //-
                            ptr2function = &Ggfm<double>::height_anom;
                            ptr2function_ld = &Ggfm<long double>::height_anom;
                        } else if ( grav_parameter.compare("grav_vec_mag") == 0 ) {
                            ptr2function = &Ggfm<double>::grav_vec_mag;
                            ptr2function_ld = &Ggfm<long double>::grav_vec_mag;
                        } else {
                            std::cerr << "WARNING: --parameter/-p \"" << grav_parameter << "\" is unknown option." << std::endl
                                      << "The column values are set to 0.000." << std::endl;

                            computed.col(i).fill(0.000);
                            continue;
                        }

                        //arma::vec computed( itmax);
                        Progressbar p_bar( static_cast<int>(itmax) );
                        p_bar.set_name( "Ggfm::single point solution" );

                        #pragma omp parallel shared( Pts , computed, iui)
                        {
                        if ( extended_precision  ) { // <long double>
                            #pragma omp for
                            for ( unsigned it = 0; it < itmax ; it++ ) {
                                // need to convert inputs to <long double>
                                computed(it, iui)= (gfieldModel_ld.*ptr2function_ld)(   Pts.coords(it, 0), // double phi,
                                                                                        Pts.coords(it, 1), // double lambda,
                                                                                        Pts.coords(it, 2), // double height,
                                                                                        degree_range[0], // unsigned int min_n,
                                                                                        degree_range[1], // unsigned int max_n,
                                                                                        tide_system, // const string& tide_mode,
                                                                                        static_cast<long double>(love_number), // double ln,
                                                                                        ell_ld );// struct geo_f::ellipsoid<double> ell,
                            #pragma omp critical
                            { p_bar.update(); }
                            }
                        } else { // <double>
                            #pragma omp for
                            for ( unsigned it = 0; it < itmax ; it++ ) {
                                computed(it, iui)= (gfieldModel.*ptr2function)( Pts.coords(it, 0), // double phi,
                                                                                Pts.coords(it, 1), // double lambda,
                                                                                Pts.coords(it, 2), // double height,
                                                                                degree_range[0], // unsigned int min_n,
                                                                                degree_range[1], // unsigned int max_n,
                                                                                tide_system, // const string& tide_mode,
                                                                                love_number, // double ln,
                                                                                ell );// struct geo_f::ellipsoid<double> ell,
                            #pragma omp critical
                            { p_bar.update(); }
                            }
                        }

                        #pragma omp barrier
                        } // #pragma omp parallel shared( Pts , computed)

                        ptr2function = nullptr;
                        ptr2function_ld = nullptr;
                        grav_parameter.clear();
                    }

                    // Save the computation results to file
                    std::ofstream ostream_out; 
                    ostream_out.open(output_file);
                    
                    if( ostream_out.is_open())
                    {
                        // #TODO setw() and setprecision() via globally declared variables
                        for ( unsigned ii = 0; ii <itmax; ii++) {
                            ostream_out << Pts.pointsID[ii] ;
                            ostream_out << " " << arma::as_scalar( Pts.coords(ii,0) ); // b [deg]
                            ostream_out << " " << arma::as_scalar( Pts.coords(ii,1) ); // l [deg]
                            ostream_out << " " << arma::as_scalar( Pts.coords(ii,2) ); // hell [m]

                            for ( unsigned jj = 0 ; jj < nc; jj++ ) {
                            ostream_out << " " << setw(16) << setprecision(12)
                                    << arma::as_scalar( computed(ii,jj) );
                            }

                            ostream_out << endl;
                        }

                        ostream_out.close();
                        return SUCCESS;
                    } else {
                        std::cerr << "Unable to save the computation results to file \""
                                  << output_file << "\". Please check your privilages\n"
                                  << "to access the given path and folder." << std::endl;
                        return ERROR_UNHANDLED_EXCEPTION;
                    }          
                } else if (solution_options[0].compare("grid") == 0) {
                    // <<<<< GGFM::GRID_COMPUTATION >>>>>>
                    // read boundaries in format "--boundaries bmin, bmax, lmin, lmax, deltab, deltal, h", in deg format
                    int numrows, numcols;
                    std::string grav_parameter, preffix, suffix;

                    if (vm.count("boundaries"))
                    {
                        check_boundaries_vector();

                        numrows = static_cast<int>( (boundaries[1] - boundaries[0]) / boundaries[5] +1);
                        numcols = static_cast<int>( (boundaries[3] - boundaries[2]) / boundaries[6] +1);
                    } else {
                        std::cerr << "--boundaries/-b does not have enough arguments. Need 7 arguments: {bmin, bmax, lmin, lmax, height,  db, dl}" << std::endl
                                  << "Program ends here." << std::endl;
                        return ERROR_UNHANDLED_EXCEPTION;
                    }

                    std::cout << setw(20) << setprecision(9);
                    std::cout << "Uf.... " << numrows * numcols << " points to compute. Let's get started." << std::endl
                              << "b_min   := " << boundaries[0] << std::endl
                              << "b_max   := " << boundaries[1] << std::endl
                              << "l_min   := " << boundaries[2] << std::endl
                              << "l_max   := " << boundaries[3] << std::endl
                              << "h_ell   := " << boundaries[4] << std::endl
                              << "delta_b := " << boundaries[5] << std::endl
                              << "delta_l := " << boundaries[6] << std::endl;


                    // === Gravity quantities such as gravity potential, etc. ===
                    for ( int i = 0; i < parameters.size();  i++) {
                        // pointer to class member function
                        grav_parameter = parameters[i];

                        arma::mat (Ggfm<double>::*ptr2gridFunction)(const double& , // const eT &b_min,
                                                                    const double& , // const eT &b_max,
                                                                    const double& , // const eT &l_min,
                                                                    const double& , // const eT &l_max,
                                                                    const double& , // const eT &height,
                                                                    const double& , // const eT &bstep,
                                                                    const double& , // const eT &lstep,
                                                                    const unsigned int& , // const unsigned int &min_n,
                                                                    const unsigned int& , // const unsigned int &max_n,
                                                                    const std::string& ,
                                                                    const double& ,
                                                                    const geo_f::ellipsoid<double>& ,
                                                                    std::ofstream&,
                                                                    bool);

                        arma::mat (Ggfm<long double>::*ptr2gridFunction_ld)(const long double& , // const eT &b_min,
                                                                            const long double& , // const eT &b_max,
                                                                            const long double& , // const eT &l_min,
                                                                            const long double& , // const eT &l_max,
                                                                            const long double& , // const eT &height,
                                                                            const long double& , // const eT &bstep,
                                                                            const long double& , //  const eT &lstep,
                                                                            const unsigned int& , // const unsigned int &min_n,
                                                                            const unsigned int& , // const unsigned int &max_n,
                                                                            const std::string& ,
                                                                            const long double& ,
                                                                            const geo_f::ellipsoid<long double>& ,
                                                                            std::ofstream&,
                                                                            bool);


                        if ( grav_parameter.compare("gpot_v") == 0 ) {
                            ptr2gridFunction    = &Ggfm<double>::gpot_v_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::gpot_v_grid;
                        } else if (  grav_parameter.compare("gpot_w") == 0 ) {
                            ptr2gridFunction = &Ggfm<double>::gpot_w_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::gpot_w_grid;
                        } else if ( grav_parameter.compare("gpot_t") == 0 ) {
                            ptr2gridFunction = &Ggfm<double>::gpot_t_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::gpot_t_grid;
                        } else if ( grav_parameter.compare("grav_gy") == 0 ) {
                            ptr2gridFunction = &Ggfm<double>::grav_gy_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::grav_gy_grid;
                        } else if ( grav_parameter.compare("grav_gx") == 0 ) {
                            ptr2gridFunction = &Ggfm<double>::grav_gx_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::grav_gx_grid;
                        } else if ( grav_parameter.compare("grav_gz") == 0 ) {
                            ptr2gridFunction = &Ggfm<double>::grav_gz_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::grav_gz_grid;
                        } else if ( grav_parameter.compare("def_vert_eta") == 0 ) {
                            ptr2gridFunction = &Ggfm<double>::def_vert_eta_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::def_vert_eta_grid;
                        } else if ( grav_parameter.compare("def_vert_xi") == 0 ) {
                            ptr2gridFunction = &Ggfm<double>::def_vert_xi_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::def_vert_xi_grid;
                        } else if ( grav_parameter.compare("def_vert") == 0 ) {
                            ptr2gridFunction = &Ggfm<double>::def_vertical_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::def_vertical_grid;
                        } else if ( grav_parameter.compare("terrain") == 0 ) {
                            ptr2gridFunction = &Ggfm<double>::terrain_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::terrain_grid;
                        } else if ( grav_parameter.compare("grav_anom_sa") == 0 ) {
                            ptr2gridFunction = &Ggfm<double>::grav_anom_sa_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::grav_anom_sa_grid;
                        } else if ( grav_parameter.compare("grav_dist_sa") == 0 ) {
                            ptr2gridFunction = &Ggfm<double>::grav_dist_sa_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::grav_dist_sa_grid;
                        } else if ( grav_parameter.compare("height_anomaly") == 0 ) { //-
                            ptr2gridFunction = &Ggfm<double>::height_anom_grid;
                            ptr2gridFunction_ld = &Ggfm<long double>::height_anom_grid;
                        } else {
                            std::cerr << "Incorrect value for --parameter/-p :\"" << grav_parameter << "\"" << std::endl;
                            return ERROR_IN_COMMAND_LINE;
                        }

                        // Computation itself
                        split_file_name(output_file, preffix, suffix);
                        std::ofstream ostream_out;
                        ostream_out.open( preffix + "-" + grav_parameter + suffix);

                        if (ostream_out.is_open() ) {
                            if ( extended_precision ) { // true stands for <long double>
                                arma::mat gridComputed = (gfieldModel_ld.*ptr2gridFunction_ld ) (
                                                        static_cast<long double>(boundaries[0]), // <long double> b_min,
                                                        static_cast<long double>(boundaries[1]), // <long double> b_max,
                                                        static_cast<long double>(boundaries[2]), // <long double> l_min,
                                                        static_cast<long double>(boundaries[3]), // <long double> l_max,
                                                        static_cast<long double>(boundaries[4]), // <long double> height,
                                                        static_cast<long double>(boundaries[5]), // <long double> bstep,
                                                        static_cast<long double>(boundaries[6]), // <long double> lstep,
                                                        degree_range[0],  // unsigned int min_n,
                                                        degree_range[1],  // unsigned int max_n,
                                                        tide_system,          // const string& tide_mode,
                                                        static_cast<long double>(love_number), // double ln,
                                                        ell_ld, // struct geo_f::ellipsoid<double> ell,
                                                        ostream_out, // ostream &out_stream) {
                                                        true);

                                gridComputed.reset();
                            } else { // false for stands <double>
                                arma::mat gridComputed =   (gfieldModel.*ptr2gridFunction)(boundaries[0], // double b_min,
                                                        boundaries[1], // double b_max,
                                                        boundaries[2], // double l_min,
                                                        boundaries[3], // double l_max,
                                                        boundaries[4], // double height,
                                                        boundaries[5], // double bstep,
                                                        boundaries[6], // double lstep,
                                                        degree_range[0], // unsigned int min_n,
                                                        degree_range[1], // unsigned int max_n,
                                                        tide_system, // const string& tide_mode,
                                                        love_number, // double ln,
                                                        ell, // struct geo_f::ellipsoid<double> ell,
                                                        ostream_out, // ostream &out_stream) {
                                                        true);

                                gridComputed.reset();
                            }

                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                << output_file << "\". Please check your privilages\n"
                                << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                    }

                    return SUCCESS;
                } else if (solution_options[0].compare("ongrid") == 0) {
                    // <<<<< GGFM::ONGRID_COMPUTATION >>>>>>
                    // File with points terrain , for now only ISGEM model type format
                    Isgemgeoid terrain;
                    std::string grav_parameter, preffix, suffix;
                    unsigned int taylor = 5;

                    if (vm.count("file")) {
                        terrain.load_isgem_model( file_name );
                    } else {
                        std::cerr << "--file/-f no input file with terrain model given. Program ends here" << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }

                    if (computation_type.size() > 1) {
                        try {
                            taylor = std::stoul(computation_type[1]);
                        } catch (...) {
                            taylor = 5;
                        }
                    }

                    /// === Ponter to the gravity quantity (etc gravity potential)
                    arma::mat ( Ggfm<double>::*ptr2onGridF) (const Isgemgeoid&,
                                                            const unsigned&,
                                                            const unsigned&,
                                                            const unsigned&,
                                                            const std::string&,
                                                            const double& ,
                                                            const geo_f::ellipsoid<double>&,
                                                            const std::string&,
                                                            std::ofstream&,
                                                            bool);

                    arma::mat ( Ggfm<long double>::*ptr2onGridF_ld) (const Isgemgeoid&,
                                                                    const unsigned&,
                                                                    const unsigned&,
                                                                    const unsigned&,
                                                                    const std::string&,
                                                                    const long double& ,
                                                                    const geo_f::ellipsoid<long double>&,
                                                                    const std::string&,
                                                                    std::ofstream&,
                                                                    bool);

                    // === Gravity quantities such as gravity potential, etc. ===
                    for ( size_t i = 0; i < parameters.size(); i++) {
                        // pointer to class member function
                        grav_parameter = parameters[i];

                        if ( grav_parameter.compare("gpot_v") == 0 ) {
                            ptr2onGridF    = &Ggfm<double>::gpot_v_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::gpot_v_ongrid;
                        } else if ( grav_parameter.compare("gpot_w") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::gpot_w_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::gpot_w_ongrid;
                        } else if ( grav_parameter.compare("gpot_t") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::gpot_t_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::gpot_t_ongrid;
                        } else if (grav_parameter.compare("gpot_t_rr") == 0) {
                            ptr2onGridF = &Ggfm<double>::gpot_t_rr_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::gpot_t_rr_ongrid;
                        } else if ( grav_parameter.compare("grav_gy") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::grav_gy_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::grav_gy_ongrid;
                        } else if ( grav_parameter.compare("grav_gx") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::grav_gx_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::grav_gx_ongrid;
                        } else if ( grav_parameter.compare("grav_gz") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::grav_gz_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::grav_gz_ongrid;
                        } else if ( grav_parameter.compare("def_vert_eta") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::def_vert_eta_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::def_vert_eta_ongrid;
                        } else if ( grav_parameter.compare("def_vert_xi") == 0) {
                            ptr2onGridF = &Ggfm<double>::def_vert_xi_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::def_vert_xi_ongrid;
                        } else if ( grav_parameter.compare("def_vert") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::def_vertical_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::def_vertical_ongrid;
                        } else if ( grav_parameter.compare("grav_anom_sa") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::grav_anom_sa_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::grav_anom_sa_ongrid;
                        } else if ( grav_parameter.compare("grav_anom_sa_r") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::grav_anom_sa_r_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::grav_anom_sa_r_ongrid;
                        } else if ( grav_parameter.compare("grav_anom_sa_rr") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::grav_anom_sa_rr_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::grav_anom_sa_rr_ongrid;
                        } else if ( grav_parameter.compare("grav_dist_sa") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::grav_dist_sa_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::grav_dist_sa_ongrid;
                        } else if ( grav_parameter.compare("height_anomaly") == 0 ) {
                            ptr2onGridF = &Ggfm<double>::height_anom_ongrid;
                            ptr2onGridF_ld = &Ggfm<long double>::height_anom_ongrid;
                        } else {
                            std::cerr << "Incorrect value for --parameter/-p :\"" << grav_parameter << "\"" << std::endl;
                            return ERROR_IN_COMMAND_LINE;
                        }

                        // output file
                        // Computation itself
                        split_file_name(output_file, preffix, suffix);
                        std::ofstream ostream_out;
                        ostream_out.open( preffix + "-" + grav_parameter + suffix);

                        terrain.set_print_coeff(false);
                        std::cout << "Computing the GGFM gradient approach for following grid data " << endl;
                        std::cout << terrain << endl
                            << "Degree and order from-to          " << degree_range[0] << " - " << degree_range[1] << endl
                            << "Degree of the Taylor polynomial : " << taylor << endl
                            << "Used tide system :                " << tidesys << endl;

                        if (ostream_out.is_open() ) {
                            if ( extended_precision ) { // true stands for <long double>
                                arma::mat gridComputed = (gfieldModel_ld.*ptr2onGridF_ld) ( terrain,
                                                                                        degree_range[0], // unsigned int min_n,
                                                                                        degree_range[1], // unsigned int max_n,);
                                                                                        taylor , // Taylor order
                                                                                        tide_system, // const string& tide_mode,
                                                                                        static_cast<long double>(ln_ld), // double ln,
                                                                                        ell_ld, // struct geo_f::ellipsoid<double> ell,
                                                                                        crd_system, // spherical {sph} or ellispoidal {ell}
                                                                                        ostream_out, // ostream &out_stream) {
                                                                                        true);
                            } else {
                                arma::mat gridComputed = (gfieldModel.*ptr2onGridF) ( terrain,
                                                                                    degree_range[0], // unsigned int min_n,
                                                                                    degree_range[1], // unsigned int max_n,);
                                                                                    taylor , // Taylor order
                                                                                    tide_system, // const string& tide_mode,
                                                                                    ln, // double ln,
                                                                                    ell, // struct geo_f::ellipsoid<double> ell,
                                                                                    crd_system, // spherical {sph} or ellispoidal {ell}
                                                                                    ostream_out, // ostream &out_stream) {
                                                                                    true);
                            }
                            
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                << output_file << "\". Please check your privilages\n"
                                << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }

                        return SUCCESS;
                    }
                } else {
                    std::cerr << "Unknown --solution option for --computation-type \"ggfm\". Program ends here." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }
        //==============================================================================================//
        // G R A V I T Y     F I E L D     M O D E L I N G     F R O M     D E M                        //
        //==============================================================================================//    
            } else if (computation_type[0].compare("tess") == 0) {
                // Declaring the objects for FGFM
                std::cout << "Forward gravity field modeling at your service! Now the PhysGeo program" << std::endl
                          << "will do its best for his or hers user." << std::endl;

                // Declaring mutual variables
                Points Pts;
                std::string opt, preffix, suffix;
                std::string grav_parameter;
                std::string model_format = "isgem"; // default format
                double density_constant; 
                double computation_radius;
                double ref_sphere_radius;
                bool rho_constant = true;
                bool is_radius_set;
                int idx_option = 0;
                unsigned int ascii_format;
                Tesseroid tess_dmr; // DEM model loaded to the computers memory
                Isgemgeoid density_model;
                size_t size_of_model_parameters = model_parameters.size();

                if (!vm.count("model")) {
                    std::cerr << "--model/-m is mandatory option for \"tess\" --computation-type. Program can not serve anymore." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }

                // Loading model
                std::cout << ".... Loading digital terrain models for forward gravity field modeling." << std::endl;
                if ( size_of_model_parameters == 1 )
                {
                    std::cout << "Upper layer is set to \"" << model_parameters[0] 
                              << "\", format of the file \"" << model_format << "\"" << std::endl;
                    // When model fails to load, program ends.
                    if (!tess_dmr.load_layer(model_parameters[0], opt, 0, idx_option, true))
                    {
                        std::cout << "Unable to load DEM model." << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }
                } else {
                    my_parser::handle_model_format(model_parameters[size_of_model_parameters-1], model_format);

                    std::cout << "Upper layer is set to \"" << model_parameters[0] 
                              << "\", format of the file \"" << model_format << "\"" << std::endl;
                    std::cout << "Lower layer is set to \"" << model_parameters[1] 
                              << "\", format of the file \"" << model_format << "\"" << std::endl;

                    if (!tess_dmr.load_layer(model_parameters[0], opt, 0, idx_option, true))
                    {
                        std::cout << "Unable to load upper layer of the DEM model." << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }

                    if (!tess_dmr.load_layer(model_parameters[1], opt, 1, idx_option, false))
                    {
                        std::cout << "Unable to load lower layer of the DEM model." << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }
                }
                tess_dmr.do_check();

                // Setting the constant density value or add density grid
                if (!vm.count("density")) {
                    density_constant = 2670; // kg / m 3
                } else {
                    size_t density_option_size = density_options.size();

                    if (density_option_size == 1)
                    {
                        // Just simple std::string to double conversion
                        density_constant = std::stod(density_options[0]);
                    } else {
                        // Check the last parameter for model format
                        my_parser::handle_model_format(density_options[1], model_format);

                        std::cout << "Loading density model \"" << density_options[0] << "\", format type \""
                                  << model_format << "\"" << std::endl
                                  << "(for option --density only first two (or 3 for ascii) arguments are taken)" << std::endl;
                    
                        if (model_format.compare("isgem") == 0) {
                            if (!density_model.load_isgem_model(density_options[0]))
                            {
                                std::cerr << "Unable to load density model in \"isgem\" format." << std::endl;
                                return ERROR_IN_COMMAND_LINE;
                            }

                            rho_constant = false;
                        } else if ( model_format.compare("ascii") == 0) {
                            try {
                                ascii_format = stoul(density_options[2]);
                            } catch (...) {
                                std::cout << "FGFM: unknown \"ascii\" format for density model. Default value is set to [b l rho]" << std::endl;
                                ascii_format = 3;
                            }

                            if (!density_model.load_model_from_ascii(density_options[0], 
                                                            ascii_format, 
                                                            "Unknown rho model", 
                                                            "density_model",
                                                            "kg/m3",
                                                            ell.name.c_str(),
                                                            "0.0.1beta",
                                                            -9999.999)
                                ) {
                                std::cerr << "Unable to load density model in \"ascii\" format." << std::endl;
                                return ERROR_IN_COMMAND_LINE;
                            }

                            rho_constant = false;
                        } else {
                            std::cerr << "FGFM: Unable to parse and load density model. Uknown format." << std::endl;
                            return ERROR_IN_COMMAND_LINE;
                        }
                    }
                }

                // Setting the radiuses, computation radius and also the radius for reference sphere
                if (!vm.count("radius")) {
                    is_radius_set = false;

                    computation_radius = 111320.0; // approx. 1 degree
                    ref_sphere_radius = 6.3710087714e+6;
                } else {
                    size_t radius_option_size = radius_options.size();

                    // first argument is --radius parameter is integration radius
                    is_radius_set = true;

                    computation_radius = std::stod(radius_options[0]);

                    if (radius_option_size > 1) {
                        ref_sphere_radius = std::stod(radius_options[1]);
                    }
                }
            
                // Create pointer to class method
                double ( Tesseroid::*ptr2tess) (double,double,double,double,double,double, // gravity_phi
                                            double,double,double,double,double);

                for (unsigned int i = 0; i < static_cast<unsigned>(parameters.size()); ++i)
                {
                    grav_parameter = parameters[i];

                    if ( grav_parameter.compare("gpot") == 0) {
                        ptr2tess = &Tesseroid::potential_u;
                    } else if ( grav_parameter.compare("grav_r") == 0 ) {
                        ptr2tess = &Tesseroid::gravity_r;
                    } else if ( grav_parameter.compare("grav_b") == 0) {
                        ptr2tess = &Tesseroid::gravity_phi;
                    } else if ( grav_parameter.compare("grav_l") == 0 ) {
                        ptr2tess = &Tesseroid::gravity_lambda;
                    } else if ( grav_parameter.compare("marussi_rr") == 0 ) {
                        ptr2tess = &Tesseroid::marussi_rr;
                    } else if ( grav_parameter.compare("marussi_rb") == 0 || 
                                grav_parameter.compare("marussi_br") == 0 ) {
                        ptr2tess = &Tesseroid::marussi_rb;
                    } else if ( grav_parameter.compare("marussi_rl") == 0 || 
                                grav_parameter.compare("marussi_lr") == 0) {
                        ptr2tess = &Tesseroid::marussi_rl;
                    } else if ( grav_parameter.compare("marussi_bb") == 0) {
                        ptr2tess = &Tesseroid::marussi_bb;
                    } else if ( grav_parameter.compare("marussi_ll") == 0) {
                        ptr2tess = &Tesseroid::marussi_rr;
                    } else if ( grav_parameter.compare("marussi_bl") == 0 || 
                                grav_parameter.compare("marussi_lb") == 0) {
                        ptr2tess = &Tesseroid::marussi_bl;
                    } else {
                        std::cerr << "Incorrect value for --parameter/-p :\"" << grav_parameter << "\"" << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }


                    // computation itself
                    if (solution_options[0].compare("sp") == 0) {
                        Points Pts;
                        
                        if (vm.count("file")) {
                            Pts.load_from_file(file_name, "#");
                        } else {
                            std::cerr << "--file/-f no input file with coordinates given. Program ends here" << std::endl;
                            return ERROR_IN_COMMAND_LINE;
                        }

                        // declaring armadillo matrix to store the results
                        unsigned itmax = Pts.coords.n_rows;
                        unsigned nc = parameters.size();

                        //Save results to output file
                        // Computation itself
                        split_file_name(output_file, preffix, suffix);
                        std::ofstream ostream_out;
                        ostream_out.open( preffix + "-" + grav_parameter + suffix);

                        if (ostream_out.is_open() ) { 
                            arma::mat sp_points = Pts.coords.cols( 0,2 );

                            if (rho_constant) {
                                tess_dmr.grav_crd_list(sp_points, is_radius_set, computation_radius, arma::datum::G, density_constant, ell , ptr2tess );
                            } else {
                                tess_dmr.grav_crd_list(sp_points, is_radius_set, computation_radius, arma::datum::G, density_model, ell , ptr2tess );
                            }

                            for ( unsigned ii = 0; ii < itmax; ii++) {
                                ostream_out << Pts.pointsID[ii]
                                        << setprecision(12)
                                        << scientific;
                                ostream_out << " " << arma::as_scalar( sp_points(ii,0) ); // b [deg]
                                ostream_out << " " << arma::as_scalar( sp_points(ii,1) ); // l [deg]
                                ostream_out << " " << arma::as_scalar( sp_points(ii,2) ); // hell [m]
                                ostream_out << " " << arma::as_scalar( sp_points(ii,3) ) << std::endl;
                            }
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                << output_file << "\". Please check your privilages\n"
                                << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                    } else if (solution_options[0].compare("grid") == 0) {
                        arma::mat fgm;
                        
                        // Save results to output file
                        // Computation itself
                        split_file_name(output_file, preffix, suffix);
                        std::ofstream ostream_out;
                        ostream_out.open( preffix + "-" + grav_parameter + suffix);

                        if (ostream_out.is_open() ) { 

                            if (rho_constant) {
                                fgm = tess_dmr.grav_on_grid( is_radius_set , true , computation_radius, arma::datum::G, density_constant, ell, ptr2tess  );
                            } else {
                                fgm = tess_dmr.grav_on_grid( is_radius_set , true , computation_radius, arma::datum::G, density_model, ell, ptr2tess  );
                            }

                            // Saving to File
                            ostream_out << tess_dmr;
                            fgm = arma::flipud( fgm );
                            fgm.raw_print ( ostream_out );

                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                << output_file << "\". Please check your privilages\n"
                                << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                    } else {
                        std::cerr << "....uknown solution type" << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }
                    return SUCCESS;
                }
        //==============================================================================================//
        // D A T A   C O N V E R S I O N                                                                //
        //==============================================================================================//
            } else if (computation_type[0].compare("convert") == 0) {
                if( !vm.count("solution"))
                {
                    std::cerr << "Option --solution,-s is mandatory for the Data conversion. Program ends here" << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }

                if (solution_options[0].compare("crd_transform") == 0) {
                    Points Pts;
                    size_t solution_option_size = solution_options.size();

                    if (vm.count("file")) {
                        Pts.load_from_file(file_name, "#");
                    } else {
                        std::cerr << "--file/-f no input file with coordinates given. Program ends here" << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }

                    if (solution_option_size <= 1) {
                        std::cerr << "Unknown transformation request. No second argument was given." << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }

                    //pointer to function
                    std::vector<double> (*transformation) (double, double, double, geo_f::ellipsoid<double>);

                    if (solution_options[1].compare("blh2xyz") == 0) {
                        transformation = &geo_f::blh2xyz<double, double>;
                    } else if (solution_options[1].compare("xyz2blh") == 0) {
                        transformation = &geo_f::xyz2blh<double, double>;
                    } else if (solution_options[1].compare("blh2ruv") == 0) {
                        transformation = &geo_f::blh2ruv<double, double>;
                    } else if (solution_options[1].compare("xyz2uvw") == 0) {
                        transformation = &geo_f::xyz2uvw<double, double>;
                    } else if (solution_options[1].compare("blh2uvw") == 0) {
                        transformation = &geo_f::blh2uvw<double, double>;
                    } else if (solution_options[1].compare("uvw2blh") == 0) {
                        transformation = &geo_f::uvw2blh<double, double>;
                    } else if (solution_options[1].compare("uvw2xyz") == 0) {
                        transformation = &geo_f::uvw2xyz<double, double>;
                    } else if (solution_options[1].compare("xyz2ruv") == 0) {
                        transformation = &geo_f::xyz2ruv<double, double>;
                    } else {
                        std::cerr << "Unknown transformation requestion. The second argument is not recognized." << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }

                    std::ofstream ostream_out;
                    ostream_out.open(output_file);

                    if (ostream_out.is_open() ){
                        for (unsigned int i = 0; i < Pts.coords.n_rows; ++i)
                        {
                            vector<double> transformed_vec = transformation(
                                                                arma::as_scalar(Pts.coords(i , 0 )),
                                                                arma::as_scalar(Pts.coords(i , 1 )),
                                                                arma::as_scalar(Pts.coords(i , 2 )),
                                                                ell
                                                            );

                            ostream_out << Pts.pointsID[i]
                                << " " << setw(16) << setprecision(12) << transformed_vec[0]
                                << " " << setw(16) << setprecision(12) << transformed_vec[1]
                                << " " << setw(16) << setprecision(12) << transformed_vec[2] << std::endl;
                        }

                        ostream_out.close();
                    } else {
                        std::cerr << "Unable to save the computation results to file \""
                                << output_file << "\". Please check your privilages\n"
                                << "to access the given path and folder." << std::endl;
                        return ERROR_UNHANDLED_EXCEPTION;
                    }
                    
                    return SUCCESS;
                } else if (solution_options[0].compare("compute_slope") == 0) {
                    // Compute slope on the DEM
                    Isgemgeoid terrain; 
                    Isgemgeoid slope;

                    if (vm.count("file")) {
                        terrain.load_isgem_model(file_name);
                        slope = terrain.compute_slope( ell );
                        slope.set_print_coeff(true);

                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            ostream_out << slope;
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }

                        return SUCCESS;
                    } else {
                        std::cerr << "--file/-f no input file with coordinates given. Program ends here" << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }
                } else if (solution_options[0].compare("convert2isg") == 0) {
                    Isgemgeoid model;

                    if (vm.count("file")) {
                        if (vm.count("extra-data") && extra_data.size() != 7) {
                            std::cout << "-c convert -s convert2isg not enough arguments for --extra option. 7 arguments are needed!\n"
                                      << "see @ Isgemgeoid::load_model_from_ascii() function." << std::endl;
                        } 

                        model.load_model_from_ascii( file_name, // path
                                                     std::stol(extra_data[0]), // format
                                                     extra_data[1].c_str(),
                                                     extra_data[2].c_str(),
                                                     extra_data[3].c_str(),
                                                     extra_data[4].c_str(),
                                                     extra_data[5].c_str(),
                                                     std::stod(extra_data[6]));
                        model.set_print_coeff(true);

                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            ostream_out << model;
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }

                        return SUCCESS;
                    } else {
                        std::cerr << "--file/-f no input DEM modek in ascii given. Program ends here" << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }
                } else if (solution_options[0].compare("isg2ascii") == 0) {
                    Isgemgeoid model;

                    if (vm.count("file")) {
                        model.load_isgem_model(file_name);
                        model.export2ascii( output_file, false );
                        
                        return SUCCESS;
                    } else {
                        std::cerr << "--file/-f no input DEM modek in ascii given. Program ends here" << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }
                } else if (solution_options[0].compare("gfc2hdf5")==0) {
                    Ggfm<double> gfield_model;

                    if (vm.count("algorithm")) {
                        if ( !(algorithm_option.compare("standard") == 0 ||
                             algorithm_option.compare("stan_mod") == 0 ||
                             algorithm_option.compare("tmfcm") == 0 ||
                             algorithm_option.compare("tmfcm_2nd") == 0)) {
                                 algorithm_option = "stan_mod";
                            }
                    } else {
                        algorithm_option = "stan_mod";
                    }

                    // === Loading the model ===
                    if( vm.count("model"))
                    {
                        std::string model_format; bool overwrite_zero_term;
                        // first read all of the additional parameters
                        model_format = (my_parser::do_vector_contains<std::string>(model_parameters, "hdf5")) ? "hdf5" : "default";
                        overwrite_zero_term = (my_parser::do_vector_contains<std::string>(model_parameters, "keep_zt")) ? false : true;

                        // Path to the model should always be the first argument
                        std::cout << ".... Loading the GGFM model: "
                                << model_parameters[0] << std::endl;


                        if (model_format.compare("hdf5") == 0) {
                            gfield_model.load_hdf5_model( model_parameters[0], algorithm_option);
                        } else {
                            gfield_model.icgem_loadggm(model_parameters[0], algorithm_option, overwrite_zero_term);
                        }

                        std::cout << "Model successfully loaded." << std::endl;

                        gfield_model.save2hdf5(output_file);
                    } else {
                        std::cerr << "The argument \"--model\" is mandatory for converting the gfc2hdf5." << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }
                } else {
                    std::cerr << "Uknown --solution option for the conversion type." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }
            // ------------------------------------------------------------------------------------------------------------
            // Interpolate isgem format geoid , either resample the grid or interpolate the value for single point solution
            // ------------------------------------------------------------------------------------------------------------
            } else if ( computation_type[0].compare("interpolate") == 0 ) {
            // If no computation type is set the runtime error pops up
                if (vm.count("solution")) {
                    // common variables
                    Isgemgeoid model;

                    if (!vm.count("algorithm")) {
                        std::cerr << "No algorithm was chosen for the interpolation. Default option \"nearest\" will be used" << std::endl;
                        algorithm_option = "nearest";
                    }

                    if (!vm.count("model")) {
                        std::cerr << "The argument \"--model\" is mandatory for -c interpolate option." << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }

                    model.load_isgem_model(model_parameters[0]);

                    if (solution_options[0].compare("grid") == 0) {
                        if (!vm.count("boundaries")) {
                            std::cerr << "--boundaries/-b does not have enough arguments. Need 6 arguments: {bmin, bmax, lmin, lmax,  db, dl} for interpolation option." << std::endl
                                    << "Program ends here." << std::endl;
                            return ERROR_IN_COMMAND_LINE;
                        }

                        Isgemgeoid interpolated_model = model.resample_model(
                            boundaries[0],  // phi min
                            boundaries[1],  // phi max
                            boundaries[2],  // lam min
                            boundaries[3],  // lam max
                            boundaries[4],  // delta phi
                            boundaries[5],  // delta lam
                            algorithm_option// iterpolation algorithm
                        );

                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            interpolated_model.set_print_coeff(true);

                            ostream_out << setw(17) << setprecision(12) << interpolated_model;
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                        return SUCCESS;
                    } else if (solution_options[0].compare("sp") == 0) {
                         Points Pts;

                        if (vm.count("file")) {
                            Pts.load_from_file(file_name, "#");
                        } else {
                            std::cerr << "--file/-f no input file with coordinates given. Program ends here" << std::endl;
                            return ERROR_IN_COMMAND_LINE;
                        }

                        std::cout << "Interpolating data from model \"" << model.get_name() << "\"" << std::endl
                                  << "List of points                \"" << file_name << "\"" << std::endl
                                  << "Total number of points        \"" << Pts.coords.n_rows << std::endl;

                        arma::vec interpolated_values = arma::vec( Pts.coords.n_rows );

                        // iterpolation loop
                        for (arma::uword i = 0; i < Pts.coords.n_rows; ++i ) {
                            bool is_in_bounds;

                            double value = model.interpolate_value( Pts.coords(i,0) ,
                                                                    Pts.coords(i,1) ,
                                                                    algorithm_option ,
                                                                    &is_in_bounds );

                            interpolated_values(i) = (is_in_bounds) ? value : -9999.999;
                        }

                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            for (arma::uword i = 0; i < Pts.coords.n_rows; ++i)
                            {
                                ostream_out << Pts.pointsID[i] << "  " 
                                            << setw(18) << setprecision(12)
                                            << arma::as_scalar(Pts.coords(i,0)) << "  "
                                            << arma::as_scalar(Pts.coords(i,1)) << "  "
                                            << interpolated_values(i) << std::endl;
                            }

                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                        return SUCCESS;
                    } else {
                        std::cerr << "Unknown --solution/-s for interpolation." << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }
                } else {
                    std::cerr << "Uknown --solution option for the \"interpolate\" type." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }
            // -----------
            // Get subgrid
            // -----------
            } else if ( computation_type[0].compare("getsubgrid") == 0) {
                std::cout << "Cutting the grids into subgrids...." << std::endl;
                std::cout << "No boundaries check available yet" << std::endl;

                if (!vm.count("model")) {
                    std::cerr << "--model/-m is mandatory option for \"combine_data\" --computation-type. Program can not serve anymore." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }

                if (!vm.count("boundaries") || boundaries.size() != 4) {
                    std::cerr << "--boundaries/-b does not have enough arguments. Need 4 arguments: {bmin, bmax, lmin, lmax} for \"getsubgrid\" option." << std::endl
                            << "Program ends here." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }

                Isgemgeoid model; model.load_isgem_model(file_name);

                unsigned int n_row, n_col;
                vector<double> header = model.get_header<double>(n_row, n_col);

                unsigned int b_low = static_cast<unsigned>((boundaries[0] - header[0]) / header[4]);
                unsigned int b_max = static_cast<unsigned>((boundaries[1] - header[0]) / header[4]);
                unsigned int l_low = static_cast<unsigned>((boundaries[2] - header[2]) / header[5]);
                unsigned int l_max = static_cast<unsigned>((boundaries[3] - header[2]) / header[5]);

                Isgemgeoid subgrid = model.cutoff_rows_columns(b_low, b_max, l_low, l_max);
                subgrid.set_print_coeff(true);

                std::ofstream ostream_out;
                ostream_out.open(output_file);

                if (ostream_out.is_open() ){
                    ostream_out << subgrid;
                    ostream_out.close();
                } else {
                    std::cerr << "Unable to save the computation results to file \""
                            << output_file << "\". Please check your privilages\n"
                            << "to access the given path and folder." << std::endl;
                    return ERROR_UNHANDLED_EXCEPTION;
                }
                return SUCCESS;
            // -----------------------------------------------------------------------------------------------------------
            // Combine multiple "rasters" into one big one
            // data are first interpolated from the first grid, if it fails then from second grid, then from third etc.
            // -----------------------------------------------------------------------------------------------------------
            } else if ( computation_type[0].compare("combine_data") == 0) {
                std::cout << "PhysGeo data combination was created to fullfill certain task." << std::endl
                          << "Use it on your own responsibility. Only Isgem models supported." << std::endl;

                if (!vm.count("algorithm")) {
                    std::cerr << "No algorithm was chosen for the interpolation. Default option \"nearest\" will be used" << std::endl;
                    algorithm_option = "nearest";
                }

                if (!vm.count("boundaries") || boundaries.size() != 6) {
                    std::cerr << "--boundaries/-b does not have enough arguments. Need 6 arguments: {bmin, bmax, lmin, lmax,  db, dl} for \"combine_data\" option." << std::endl
                            << "Program ends here." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }

                if (!vm.count("model")) {
                    std::cerr << "--model/-m is mandatory option for \"combine_data\" --computation-type. Program can not serve anymore." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }

                long long int numrows = static_cast<long long int>( (boundaries[1] - boundaries[0]) / boundaries[5] +1);
                long long int numcols = static_cast<long long int>( (boundaries[3] - boundaries[2]) / boundaries[6] +1);

                Isgemgeoid * models = new Isgemgeoid[model_parameters.size()];

                for ( size_t k = 0; k < model_parameters.size(); ++k) {
                    std::cout << "Loading Isgemgeoid model \"";

                    models[k].load_isgem_model( model_parameters[k] );

                    std::cout << models[k] << "\". Model loaded." << std::endl;
                }

                arma::mat interpolated(numrows, numcols);
                Progressbar p_bar( static_cast<int>(numrows) );
                p_bar.set_name( "my_parser: \"combine_data\""  );

                #pragma omp parallel for shared(interpolated, boundaries, models , numcols)
                for (long long int i = 0; i < numrows; ++i) {
                    #pragma omp critical
                    { p_bar.update(); }

                    double bdeg = boundaries[0] + static_cast<double>(i) * boundaries[5];
                    std::string from_model;

                    for ( long long int j = 0;  j < numcols; ++j) { 
                        double from_grid = 0.000;
                        bool success;
                        double ldeg = boundaries[2] + static_cast<double>(j) * (boundaries[6]); // lmin + (j*dl)

                        for (unsigned long k = 0; k < model_parameters.size(); ++k) {
                            try {
                                from_grid = models[k].interpolate_value(bdeg, ldeg, algorithm_option, &success);
                            } catch (...) {
                                unsigned nr, nc;
                                std::vector<double> hdinfo = models[k].get_header<double>( nr, nc );

                                #pragma omp critical
                                {
                                    std::cout << "----------------------------------------------\n";
                                    std::cout << "Problems with model := " << models[k].get_name() << std::endl
                                        << "lat_min   := " << setprecision(10) << hdinfo[0] << std::endl
                                        << "lat_max   := " << setprecision(10) << hdinfo[1] << std::endl
                                        << "lon_min   := " << setprecision(10) << hdinfo[2] << std::endl
                                        << "lon_max   := " << setprecision(10) << hdinfo[3] << std::endl
                                        << "delta lat := " << setprecision(10) << hdinfo[4] << std::endl
                                        << "delta lon := " << setprecision(10) << hdinfo[5] << std::endl
                                        << "Trying to interpolate value for (" << bdeg <<","<<ldeg<<")" << std::endl;
                                }
                                continue;
                            }

                            if (success ) {
                                from_model = models[k].get_name();
                                break;
                            }
                        }

                        interpolated(i,j) = (success) ? from_grid : -9999.999;
                    }
                }

                std::ofstream ostream_out;
                ostream_out.open(output_file);

                if (ostream_out.is_open() ){
                    interpolated = arma::flipud(interpolated);

                    ostream_out << "begin_of_head ================================================" << std::endl
                            << "model name : combined_models_with_PhysGeo_combine_function"     << std::endl
                            << "model type : unknown " << std::endl
                            << "units : unknown" << std::endl
                            << "reference :  unknown " << std::endl
                            << "lat min : " << setw(20) << setprecision(12) << boundaries[0] << std::endl
                            << "lat max : " << setw(20) << setprecision(12) << boundaries[1] << std::endl
                            << "lon min : " << setw(20) << setprecision(12) << boundaries[2] << std::endl
                            << "lon max : " << setw(20) << setprecision(12) << boundaries[3] << std::endl
                            << "delta lat : " << setw(20) << setprecision(12) << boundaries[5] << std::endl
                            << "delta lon : " << setw(20) << setprecision(12) << boundaries[6] << std::endl
                            << "nrows : " << numrows << std::endl
                            << "ncols : " << numcols << std::endl
                            << "nodata :     -9999.999" << std::endl
                            << " ISG format : 1.0 " << std::endl
                            << "end_of_head ==================================================" << std::endl;

                    ostream_out << setw(21) << setprecision(11) << scientific;
                    interpolated.raw_print(ostream_out);
                    
                    ostream_out.close();
                } else {
                    std::cerr << "Unable to save the computation results to file \""
                            << output_file << "\". Please check your privilages\n"
                            << "to access the given path and folder." << std::endl;
                    return ERROR_UNHANDLED_EXCEPTION;
                }
                return SUCCESS;
            // ------------------------------------------------------------------------ //
            // Geoid/Quasigeoid related computations                                    //
            // ------------------------------------------------------------------------ //
            } else if ( computation_type[0].compare("geoid") == 0 ) {
                // Commonly defined variables
                bool extend = false; // default value is false
                double sphrad = 6.3710087714e+6; // reference sphere radius
                double intrad = DEG2RAD * sphrad; // integration radius
                bool check_radius = false;
                bool inner_area = true;
                bool boundaries_set = false;
                bool remove_bouguer = false;
                unsigned int nmin;
                unsigned int nmax;
                bool throw_away;

                if (!vm.count("model")) {
                    std::cerr << "--model/-m is mandatory option for \"geoid\" --computation-type. Program can not serve anymore." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }

                if (!vm.count("parameters")) {
                    std::cerr << "--parameters/-p is mandatory option for \"geoid\" --computation-type. Program can not serve anymore." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }

                if (!vm.count("solution")) {
                    std::cerr << "--solution/-s is mandatory option for \"geoid\" --computation-type. Program can not serve anymore." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }

                if (vm.count("extend-data") && extend_data.compare("true") == 0)
                {
                    extend = true;
                }

                if (vm.count("radius")) {
                    if (radius_options.size() != 0) {
                        if (radius_options.size() == 1) {
                            sphrad = my_parser::convert_std_string_to_double(radius_options[0], throw_away);
                        } else {
                            sphrad = my_parser::convert_std_string_to_double(radius_options[0], throw_away);
                            intrad = my_parser::convert_std_string_to_double(radius_options[1], check_radius);
                        }
                    }
                }

                if (vm.count("boundaries")) {
                    if ( boundaries.size() != 2) {
                        std::cerr << "Option --boundaries, -b for  \"geoid\" is required to have 2 parameters or not to be used." << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }
                }

                if (solution_options[0].compare("grid") == 0) {
                    if (parameters[0].compare("stokes") == 0 ) {
                        std::cout << "Computing the Stokes' integral for \"" << model_parameters[0] << "\"" << std::endl
                                  << "Input parameters                   : " << std::endl
                                  << "Reference sphere radius            : " << sphrad << std::endl
                                  << "Integration radius                 : " << intrad << std::endl
                                  << "Extend data option                 : " << extend << std::endl
                                  << "Check the integration radius       : " << check_radius << std::endl
                                  << "Remove Bouguer shell option        : " << remove_bouguer << std::endl;

                        Isgemgeoid faye_anom; faye_anom.load_isgem_model(model_parameters[0]);
                        Isgemgeoid terrain; terrain.load_isgem_model( model_parameters[1] );

                        Isgemgeoid tpot = faye_anom.stokes_integral(sphrad,
                                            intrad ,
                                            check_radius,
                                            extend ,
                                            remove_bouguer,
                                            terrain,
                                            ell,
                                            geo_f::stokes_kernel<double>,
                                            geo_f::int_stokes_kernel_cyl<double>,
                                            boundaries);

                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            tpot.set_print_coeff(true);
                            ostream_out << tpot;
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                        return SUCCESS;              
                    } else if (parameters[0].compare("stokes_g1") == 0) {
                        std::cout << "Computing the G1 (Molodenski) term for faye anomaly for \"" << model_parameters[0] << "\"" << std::endl
                                  << "Input parameters                   : " << std::endl
                                  << "Terrain model                      : " << model_parameters[0] << std::endl
                                  << "Term T0 dist. potential            : " << model_parameters[1] << std::endl
                                  << "Reference sphere radius            : " << sphrad << std::endl
                                  << "Integration radius                 : " << intrad << std::endl
                                  << "Extend data option                 : " << extend << std::endl
                                  << "Check the integration radius       : " << check_radius << std::endl;

                        if (!boundaries.empty()) {
                            std::cout << "Latitude limits are set to: " << std::endl
                                      << "Lat_min := " << boundaries[0] << std::endl
                                      << "Lat_max := " << boundaries[1] << std::endl;
                        }


                        Isgemgeoid faye_anom; faye_anom.load_isgem_model( model_parameters[0]);
                        Isgemgeoid terrain;     terrain.load_isgem_model( model_parameters[1]);
                        Isgemgeoid tpot0;         tpot0.load_isgem_model( model_parameters[2]);

                        Isgemgeoid g1t = faye_anom.molodensky_G1term( sphrad, // param sphrad - radius of the reference sphere
                                                                    intrad, // param intradius - integration radius
                                                                    check_radius, // param check_radius - check the integration radius
                                                                    extend, // extend_data - because the convolution and the fact than we are dealing with the sphere the extention of the data is
                                                                    terrain,
                                                                    tpot0 , // terrain model in ellispoidal coordinates
                                                                    ell, //rotational ellipsoid
                                                                    boundaries);

                        std::cout << "\nComputation is done!" << std::endl;

                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            g1t.set_print_coeff(true);
                            ostream_out << g1t;
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                        return SUCCESS; 
                    } else if (parameters[0].compare("hotine") == 0) {
                        std::cout << "Computing the Hotine integral (gravity disturbace) for \"" << model_parameters[0] << "\"" << std::endl
                                  << "Input parameters                   : " << std::endl
                                  << "Reference sphere radius            : " << sphrad << std::endl
                                  << "Integration radius                 : " << intrad << std::endl
                                  << "Check the integration radius       : " << check_radius << std::endl
                                  << "Extend data option                 : " << extend << std::endl;

                        if (!boundaries.empty()) {
                            std::cout << "Latitude limits are set to: " << std::endl
                                      << "Lat_min := " << boundaries[0] << std::endl
                                      << "Lat_max := " << boundaries[1] << std::endl;
                        }
                        
                        Isgemgeoid gdist; gdist.load_isgem_model( model_parameters[0]);

                        Isgemgeoid tpot = gdist.hotine_integral(sphrad, // param sphrad - radius of the reference sphere
                                                                intrad, // param intradius - integration radius
                                                                check_radius, // param check_radius - check the integration radius
                                                                extend, // extend_data - because the convolution and the fact than we are dealing with the sphere the extention of the data is
                                                                ell, //rotational ellipsoid
                                                                geo_f::hotine_kernel<double>,
                                                                geo_f::int_hotine_kernel_cyl<double>,
                                                                boundaries);

                        std::cout << "\nComputation is done!" << std::endl;

                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            tpot.set_print_coeff(true);
                            ostream_out << tpot;
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                        return SUCCESS; 
                    } else if (parameters[0].compare("stokes_spectral") == 0) {
                        std::cerr << "WARNING! This computation type was not tested. Used it on your own risk.\n";
                        std::cout << "Computing the Stokes' integral in a spectral form for \""
                                  << model_parameters[0] << "\"" << std::endl
                                  << "Input parameters                   : " << std::endl
                                  << "Reference sphere radius            : " << sphrad << std::endl
                                  << "Integration radius                 : " << intrad << std::endl
                                  << "Check the integration radius       : " << check_radius << std::endl
                                  << "Extend data option                 : " << extend << std::endl;

                        if (!boundaries.empty()) {
                            std::cout << "Latitude limits are set to: " << std::endl
                                      << "Lat_min := " << boundaries[0] << std::endl
                                      << "Lat_max := " << boundaries[1] << std::endl;
                        }

                        if (vm.count("kernel")) {
                            if ( kernel_options.size() != 2) {
                                std::cerr << "Incorrectly passed parameters into \"--kernel,-k\" option." << std::endl 
                                          << "When computing the \"stokes_spectral\" computation type." << std::endl;
                            } else {
                                nmin = my_parser::convert_std_string_to_unsigned(kernel_options[0], throw_away);
                                nmax = my_parser::convert_std_string_to_unsigned(kernel_options[1], throw_away);
                            }
                        } else {
                            nmin = 0; nmax = 2190;
                        }

                        std::cout << "Kernel spectral form computed from '" << nmin << "' to '" << nmax << "'." << std::endl;

                        Isgemgeoid gravity_model; gravity_model.load_isgem_model( model_parameters[0] );
                        Isgemgeoid tpot = gravity_model.kernel_spectral_form( sphrad, // param sphrad - radius of the reference sphere
                                                intrad, // param intradius - integration radius
                                                check_radius, // param check_radius - check the integration radius
                                                extend, // extend_data - because the convolution and the fact than we are dealing with the sphere the extention of the data is
                                                ell, //rotational ellipsoid
                                                nmin,
                                                nmax,
                                                "stokes",
                                                boundaries);

                        std::cout << "\nComputation is done!" << std::endl;

                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            tpot.set_print_coeff(true);
                            ostream_out << tpot;
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                        return SUCCESS;
                    } else if (parameters[0].compare("hotine_spectral") == 0) {
                        std::cerr << "WARNING! This computation type was not tested. Used it on your own risk.\n";
                        std::cout << "Computing the Hotine's integral in a spectral form for \""
                                  << model_parameters[0] << "\"" << std::endl
                                  << "Input parameters                   : " << std::endl
                                  << "Reference sphere radius            : " << sphrad << std::endl
                                  << "Integration radius                 : " << intrad << std::endl
                                  << "Check the integration radius       : " << check_radius << std::endl
                                  << "Extend data option                 : " << extend << std::endl;

                        if (!boundaries.empty()) {
                            std::cout << "Latitude limits are set to: " << std::endl
                                      << "Lat_min := " << boundaries[0] << std::endl
                                      << "Lat_max := " << boundaries[1] << std::endl;
                        }

                        if (vm.count("kernel")) {
                            if (kernel_options.size() != 2) {
                                std::cerr << "Incorrectly passed parameters into \"--kernel,-k\" option." << std::endl 
                                          << "When computing the \"hotine_spectral\" computation type." << std::endl;
                            } else {
                                nmin = my_parser::convert_std_string_to_unsigned(kernel_options[0], throw_away);
                                nmax = my_parser::convert_std_string_to_unsigned(kernel_options[1], throw_away);
                            }
                        } else {
                            nmin = 0; nmax = 2190;
                        }

                        std::cout << "Kernel spectral form computed from '" << nmin << "' to '" << nmax << "'." << std::endl;

                        Isgemgeoid gravity_model; gravity_model.load_isgem_model( model_parameters[0] );
                        Isgemgeoid tpot = gravity_model.kernel_spectral_form( sphrad, // param sphrad - radius of the reference sphere
                                                                  intrad, // param intradius - integration radius
                                                                  check_radius, // param check_radius - check the integration radius
                                                                  extend, // extend_data - because the convolution and the fact than we are dealing with the sphere the extention of the data is
                                                                  ell, //rotational ellipsoid
                                                                  nmin,
                                                                  nmax,
                                                                  "hotine",
                                                                  boundaries);

                        std::cout << "\nComputation is done!" << std::endl;

                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            tpot.set_print_coeff(true);
                            ostream_out << tpot;
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                        return SUCCESS;
                    } else if (parameters[0].compare("hotine_g1") == 0) {
                        std::cout << "Computing the Stokes' integral in a spectral form for \""
                                  << model_parameters[0] << "\"" << std::endl
                                  << "Input parameters                   : " << std::endl
                                  << "Reference sphere radius            : " << sphrad << std::endl
                                  << "Integration radius                 : " << intrad << std::endl
                                  << "Check the integration radius       : " << check_radius << std::endl
                                  << "Extend data option                 : " << extend << std::endl;

                        if (!boundaries.empty()) {
                            std::cout << "Latitude limits are set to: " << std::endl
                                      << "Lat_min := " << boundaries[0] << std::endl
                                      << "Lat_max := " << boundaries[1] << std::endl;
                        }

                        // Loading the gravity disturbace
                        Isgemgeoid gdist;     gdist.load_isgem_model( model_parameters[0]);
                        Isgemgeoid terrain; terrain.load_isgem_model( model_parameters[1]);

                        Isgemgeoid tpot = gdist.gdist_molodenskyG1(  sphrad, // param sphrad - radius of the reference sphere
                                                                    intrad, // param intradius - integration radius
                                                                    check_radius, // param check_radius - check the integration radius
                                                                    extend, // extend_data - because the convolution and the fact than we are dealing with the sphere the extention of the data is
                                                                    terrain , // terrain model in ellispoidal coordinates
                                                                    ell, //rotational ellipsoid
                                                                    boundaries);

                        std::cout << "\nComputation is done!" << std::endl;

                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            tpot.set_print_coeff(true);
                            ostream_out << tpot;
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                        return SUCCESS;
                    } else if (parameters[0].compare("hotine_kernel_only") == 0) {
                        double dphi = 0.25;
                        double dlam = 0.25;

                        std::cout << "Computing the Hotine integral (only the kernel) for \"" << model_parameters[0] << "\"\n";
                        std::cout << "Delta phi := " << dphi << std::endl
                                  << "Delta lam := " << dlam << std::endl;

                        Isgemgeoid gdist; gdist.load_isgem_model( model_parameters[0] );

                        Isgemgeoid tpot = gdist.hotine_kernel_integral( dphi, dlam,
                                                                ell, //rotational ellipsoid
                                                                geo_f::hotine_kernel<double>,
                                                                geo_f::int_hotine_kernel_cyl<double>);


                        std::cout << "\nComputation is done!" << std::endl;

                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            tpot.set_print_coeff(true);
                            ostream_out << tpot;
                            ostream_out.close();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                        return SUCCESS;
                    } else if (parameters[0].compare("quasigeoid") == 0) {
                        std::cout << "Computing the quasigeoid from Hotine integral (gravity disturbace) for \""
                                  << model_parameters[0] << "\"" << std::endl
                                  << "Input parameters                   : " << std::endl
                                  << "Reference sphere radius            : " << sphrad << std::endl
                                  << "Integration radius                 : " << intrad << std::endl
                                  << "Check the integration radius       : " << check_radius << std::endl
                                  << "Extend data option                 : " << extend << std::endl
                                  << "Remove Bouguer shell option        : " << remove_bouguer << std::endl;

                        if (!boundaries.empty()) {
                            std::cout << "Latitude limits are set to: " << std::endl
                                      << "Lat_min := " << boundaries[0] << std::endl
                                      << "Lat_max := " << boundaries[1] << std::endl;
                        }

                        // Loading the gravity disturbace
                        Isgemgeoid gdist;     gdist.load_isgem_model( model_parameters[0]);
                        Isgemgeoid terrain; terrain.load_isgem_model( model_parameters[1]);

                        std::cout << "Computing the disturbing potential for spherical approximation (tpot)." << std::endl;
                        Isgemgeoid tpot = gdist.stokes_integral(sphrad,
                                                                intrad ,
                                                                check_radius,
                                                                extend ,
                                                                remove_bouguer,
                                                                terrain,
                                                                ell,
                                                                geo_f::stokes_kernel<double>,
                                                                geo_f::int_stokes_kernel_cyl<double>,
                                                                boundaries);

                        std::cout << "Computations done. Saving the disturbing potential results to the file.\n";
                        {
                            std::string preffix, suffix;
                            split_file_name(output_file, preffix, suffix);
                            std::ofstream ostream_out;
                            ostream_out.open( preffix + "-tpot"  + suffix);

                            if (ostream_out.is_open() ){
                                tpot.set_print_coeff(true);
                                ostream_out << tpot;
                                ostream_out.close();
                            } else {
                                std::cerr << "Unable to save the computation results to file \""
                                        << output_file << "\". Please check your privilages\n"
                                        << "to access the given path and folder." << std::endl;
                                return ERROR_UNHANDLED_EXCEPTION;
                            }
                        }

                        std::cout << "Computing the 'gdist_prime' parameter."  << std::endl;
                        Isgemgeoid gdist_prime = tpot.convert_T0_to_deltag_prime( sphrad , gdist );
                        tpot.reset();

                        std::cout << "Computing the 'gdist1_prime' parameter."  << std::endl;
                        Isgemgeoid gdist1 = gdist_prime.gdist_molodenskyG1(  
                                                        sphrad, // param sphrad - radius of the reference sphere
                                                        intrad, // param intradius - integration radius
                                                        check_radius, // param check_radius - check the integration radius
                                                        extend, // extend_data - because the convolution and the fact than we are dealing with the sphere the extention of the data is
                                                        terrain , // terrain model in ellispoidal coordinates
                                                        ell, //rotational ellipsoid
                                                        boundaries);

                        std::cout << "Computations done. Saving the 'gdist1_prime' results to the file.\n";
                        {
                            std::string preffix, suffix;
                            split_file_name(output_file, preffix, suffix);
                            std::ofstream ostream_out;
                            ostream_out.open( preffix + "-gdist1"  + suffix);

                            if (ostream_out.is_open() ){
                                gdist1.set_print_coeff(true);
                                ostream_out << gdist1;
                                ostream_out.close();
                            } else {
                                std::cerr << "Unable to save the computation results to file \""
                                        << output_file << "\". Please check your privilages\n"
                                        << "to access the given path and folder." << std::endl;
                                return ERROR_UNHANDLED_EXCEPTION;
                            }
                        }

                        std::cout << "Computing the disturbing potential for spherical approximation (tpot1)." << std::endl;
                        Isgemgeoid tpot1 = gdist1.stokes_integral(sphrad,
                                            intrad ,
                                            check_radius,
                                            extend ,
                                            remove_bouguer,
                                            terrain,
                                            ell,
                                            geo_f::stokes_kernel<double>,
                                            geo_f::int_stokes_kernel_cyl<double>,
                                            boundaries);

                        std::cout << "Computations done. Saving the 'tpot1' results to the file.\n";
                        {
                            std::string preffix, suffix;
                            split_file_name(output_file, preffix, suffix);
                            std::ofstream ostream_out;
                            ostream_out.open( preffix + "-tpot1"  + suffix);

                            if (ostream_out.is_open() ){
                                tpot1.set_print_coeff(true);
                                ostream_out << tpot1;
                                ostream_out.close();
                            } else {
                                std::cerr << "Unable to save the computation results to file \""
                                        << output_file << "\". Please check your privilages\n"
                                        << "to access the given path and folder." << std::endl;
                                return ERROR_UNHANDLED_EXCEPTION;
                            }
                        }

                        gdist1.reset();
                        tpot1.reset();

                        return SUCCESS;
                    } else if (parameters[0].compare("bouguer") == 0) {
                        // Calculate the efffect of the Bouguer plate or shell
                        // as a limited option or \" full scale \" Bouguer shell
                        // Spherical or plannar approximation  is available
                        if (parameters.size() < 4 ) {
                            std::cout << "-c geoid -s grid -p bouguer need at least 4 arguments for -p option.\n"
                                << "First is bouguer and then \"{sphere,plane}\" argument, third is \n"
                                << "\"{limited,unlimited}\" and the last necessary is gravity quantity\n"
                                << "\"{gpot_v,gravity_r,marussi_rr}\"." << std::endl;

                            return ERROR_IN_COMMAND_LINE;
                        }

                        std::cout   << "Computing the Bouguer plate/shell correction :" << std::endl
                                    << "Geometry           : " << parameters[1] << std::endl
                                    << "Limits             : " << parameters[2] << std::endl
                                    << "Parameter          : " << parameters[3] << std::endl
                                    << "Integration radius : " << intrad << std::endl
                                    << "Sphere radius      : " << sphrad << std::endl;

                        Isgemgeoid terrain; terrain.load_isgem_model(  model_parameters[0]);
                        Isgemgeoid bouguermodel = terrain.bouguer_shell(parameters[1], // geometry
                                                                        parameters[2], // limit
                                                                        parameters[3], // quantity
                                                                        sphrad,
                                                                        intrad);
                        std::ofstream ostream_out;
                        ostream_out.open(output_file);

                        if (ostream_out.is_open() ){
                            bouguermodel.set_print_coeff(true);

                            ostream_out << bouguermodel;
                            ostream_out.close();

                            bouguermodel.reset();
                        } else {
                            std::cerr << "Unable to save the computation results to file \""
                                    << output_file << "\". Please check your privilages\n"
                                    << "to access the given path and folder." << std::endl;
                            return ERROR_UNHANDLED_EXCEPTION;
                        }
                        return SUCCESS;
                    } else {
                        std::cerr << "--solution/-s is mandatory for the \"geoid\" computation. Can not serve any more!" << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }
                } else if (solution_options[0].compare("sp") == 0) {
                    Points Pts;

                    if (vm.count("file")) {
                        Pts.load_from_file(file_name, "#");
                    } else {
                        std::cerr << "--file/-f no input file with coordinates given. Program ends here" << std::endl;
                        return ERROR_IN_COMMAND_LINE;
                    }

                    std::cerr << "Unfinished... sorry" << std::endl;
                    return ERROR_UNHANDLED_EXCEPTION;
                } else {
                    std::cerr << "Uknown input for \"--computation-type,-c\". Program ends here." << std::endl;
                    return ERROR_IN_COMMAND_LINE;
                }
            } else {
            // Check if the computation type parameter is set. If it is not
            // there is no point to continue the program. 
                std::cerr << "\"--computation-type,-c\" is a mandatory option when the computations should be performed. Program terminates here." << std::endl;
                return ERROR_IN_COMMAND_LINE;
            }
        }
    }   // If everything fails throws an exception
    catch (std::exception& e) 
    {
        std::cerr << "Unhandled Exception reached the top of main: " 
        << e.what() << " , application will now exit" << std::endl;

        return ERROR_UNHANDLED_EXCEPTION; 
    }

    return SUCCESS;
}

//==============================================================================================//
// REST OF THE FUNCTIONS                                                                        //
//==============================================================================================//

bool my_parser::check_boundaries_vector()
{
    if ( boundaries.size() == 7) {
        return true;
    } 

    std::string line, word;
    unsigned int noi = 0, i;
    boundaries.resize(7);

    while(true) {
        std::cout << "Incorrect number of values passed by --boundaries/-b. Need 7 arguments in this order:" << std::endl
                  << "\"bmin bmax lmin lmax height db dl\". Type \"q\" to exit the program." << std::endl;
        
        std::getline(std::cin, line);

        std::istringstream is(line);
        i = 0;

        while( is >> word ) {
            try {
                boundaries[i++] = std::stod(word);
            } catch (...) {
                break;
            }
        }

        noi++;
        if ( i==7 ) {
            // check and swaps bmin, bmax, lmin, lmax when are in wrong order
            if (boundaries[1] < boundaries[0])
            {
                my_parser::swap_values(boundaries[1] , boundaries[0]);
            }

            if (boundaries[2] < boundaries[3])
            {
                my_parser::swap_values(boundaries[2] , boundaries[3]);
            }

            return true;
        } else if ( noi==3 || line.compare("q") == 0) {
            return false;
        }
    }

    return false;
}

unordered_multimap<string, vector<double> > my_parser::load_points_form_file(const string &path)
{
    // PtID b l h <free space>

    ifstream f_in ; f_in.open( path );

    unordered_multimap<string, vector<double> > points;
    if ( f_in.is_open() ) {
        string line, word;
        vector<string> vvec;

        while ( getline( f_in , line  ) ) {
            vector<double> pts_vec(4);

            istringstream is (line);
            while ( is >> word ) {
                vvec.push_back( word );
            }

            pts_vec[0] = std::stod ( vvec[1].c_str() );
            pts_vec[1] = std::stod ( vvec[2].c_str() );
            pts_vec[2] = std::stod ( vvec[3].c_str() );

            points.insert({vvec[0].c_str() , pts_vec });

            vvec.clear();
        }

        f_in.close();
    } else {
        std::cerr << "Function \"load_points_form_file\": Could not open the file! " << std::endl;
    }
    return points;
}

void my_parser::swap_values(double &x, double &y)
{
    double swapval = x;
    x = y;
    y = swapval;
}

void my_parser::generate_physgeo_info()
{
    std::cout << "\nThe program \"PhysGeo\" was created dor the needs of the doctoral thesis:" <<std::endl
              << "\"Geophysical methods of integration of the local vertical datums into World Height System\"." << std::endl
              << "\nThe latest source code can be downloaded from the GITLAB repository:" << std::endl
              << GITLAB_REPOSITORY << std::endl
              << "\nCitation: BUDAY, Michal. Geophysical methods of integration of the local vertical datums into World Height System." << std::endl
              << "Brno [2022-05-19]. Available at: http://hdl.handle.net/11012/196817" << std::endl
              << "\nProgram covers the computations for tho Global Gravity Field Models, second option" << std::endl
              << "is the Forward gravity field modelling, third options are some computations related to" << std::endl
              << "operations with Stokes' and Hotine's integrals (retaled to computaions for geoid undolations" << std::endl
              << "or height anomaly for quasigeoid." << std::endl
              << "All of these computations have some supported functions in the last module. Related to" << std::endl
              << "data interpolation, data combination, data handling, transformations for the coordinates between" << std::endl
              << "various coordina systems (rectangular, spherical, ellipsoid coordinates, etc.)" << std::endl;
}

void my_parser::generate_ggfm_module_help()
{
    // #TODO this stupid stuff
    std::cout << "Well.... not yet!" << std::endl;
}

void my_parser::generate_fgfm_module_help()
{
    // #TODO this stupid stuff
    std::cout << "Well.... not yet!" << std::endl;
}

void my_parser::generate_stokes_and_hotine_module_help()
{
    // #TODO this stupid stuff
    std::cout << "Well.... not yet!" << std::endl;
}

void my_parser::split_file_name(const std::string& filename, std::string& preffix, std::string& suffix)
{
    boost::filesystem::path p{filename};
    suffix = p.extension().string();

    size_t filename_size = filename.length();
    size_t suffix_size = suffix.length();
    preffix = filename.substr(0,filename_size - suffix_size);
};


void my_parser::get_license()
{
    std::ifstream licence_file(PATH_TO_LICENCE_FILE);

    if (licence_file.is_open())
    {
        std::string line_of_text;

        while( std::getline(licence_file, line_of_text))
        {
            std::cout << line_of_text << std::endl;
        }
    }
    else {
        std::cerr << "Can not locate ile \"LICENCE.txt\" from project directory.\n" << std::endl
                  << "See MIT licence." << std::endl;
        
    }

    licence_file.close();
}

bool my_parser::handle_model_format(const std::string& format, std::string &confirmed)
{
    confirmed.clear();

    if (format.compare("isgem") == 0) {
        confirmed = "isgem";
    } else if (format.compare("ascii") == 0) {
        confirmed = "ascii";
    } else if (format.compare("csv") == 0) {
        confirmed = "csv";
    } else {
        confirmed = "isgem"; // default value;
        return false;
    }

    return true;
};

std::vector<double> my_parser::parse_boundaries( const std::string & args )
{
    std::vector<double> boundaries;
    
    std::istringstream is(args);
    
    std::string val;
    
    while ( is >> val )
    {
        bool is_success;
        double converted = convert_std_string_to_double(val, is_success);
        
        if (is_success) 
        {
            boundaries.push_back(converted);
        }
    }
    return boundaries;

}

double my_parser::convert_std_string_to_double( const std::string& str, bool & success)
{
    double num = 0.0;   
    try {
        num = std::stod(str);
        success = true;
    } catch (...) {
        success = false;
    }

    return num;
}

unsigned my_parser::convert_std_string_to_unsigned( const std::string& str, bool & success)
{
    unsigned num ;
    try {
        num = static_cast<unsigned>(std::stoul(str));
        success = true;
    } catch (...) {
        success = false;
    }

    return num;
}