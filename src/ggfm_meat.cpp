#include "../include/ggfm_meat.h"

bool ggfm_idx::return_rowcol(const arma::uword &e,
                             const arma::uword &nrows,
                             const arma::uword &ncols,
                             arma::uword &row,
                             arma::uword &col) {

    if ( e < nrows * ncols ) {
        col = e / nrows;
        row = e - col * nrows;
        return true;
    } else {
        col = 0;
        row = 0;
        return false;
    }
}

bool ggfm_idx::return_rowcol2(const arma::uword &e,
                              const arma::mat &amat,
                              arma::uword &row,
                              arma::uword &col) {

    if ( e < amat.n_elem ) {
        col = e / amat.n_rows;
        row = e - col * amat.n_rows;
        return true;
    } else {
        col = 0;
        row = 0;
        return false;
    }
}
