#include "../include/bouguer.h"

bouguer::Bouguer::Bouguer()
{

}

bouguer::Bouguer::Bouguer(const double &n_rho, const double &n_G) {
    this->G = n_G;
    this->rho = n_rho;
}

double bouguer::Bouguer::gpot_v_limited_plate(const double &R,
                                              const double &h,
                                              const double &z,
                                              const int &pcase) {
    double G_rho_pi = this->rho * this->G * M_PI;

    double lh = std::sqrt(  R*R + (h+z)*(h+z));
    double l0 = std::sqrt(R*R+z*z);

    double result = 0.0;
    if ( pcase == 1 ) {
        //  Point is above the bouguer layer \f$ P_1 ( z>0) \f$
        result = (R*R * std::log( (lh+h+z)/(z+l0) ) - h*h + h*(lh-2.*z) + z*(lh-l0));
    } else if ( pcase == 2 ) {
        // Point is at the top edge of the layer \f$ P_2 ( z=0) \f$
        result = (R*R * std::log( (std::sqrt(h*h+R*R)+h) / (R) ) - h*h + h*std::sqrt(h*h+R*R) );
    } else if ( pcase == 3 ) {
        // Point is inside the layer \f$ P_3 ( 0<z<-h) \f$
        result = (R*R * std::log( (lh+h+z)/(z+l0) ) - h*h + h*(lh-2.*z) - 2.*z*z + z*(lh-l0));
    } else if ( pcase == 4 ) {
        result = R*R * std::log( R / (std::sqrt(h*h+R*R) - h) + h*h + h*std::sqrt(h*h + R*R) );
        // Point is at the lower edge of the Bouguer layer \f$ P_4 ( z=-h) \f$
    } else if ( pcase == 5 ) {
        // Point is under the layer  \f$ P_5 ( z<-h) \f$
        result = (R*R * std::log( (lh+h+z)/(z+l0) )  +h*h + h*(lh+2.*z) + z*(lh-l0) );
    } else {
        // error
        std::string errmsg = "Bouguer::gpot_v_limited_plate() invalid pcase option.\n";
        throw std::runtime_error(errmsg);
    }
    return  G_rho_pi * result;
}

double bouguer::Bouguer::gpot_v_unlimited_plate(const double &h,
                                                const double &z,
                                                const int &pcase) {
    return (h+z+pcase)/0.; // inf
}

double bouguer::Bouguer::gravity_r_unlimited_plate(const double &h,
                                                   const double &z,
                                                   const int &pcase) {

    double G_rho_pi = 2. * this->G * this->rho * M_PI;
    double result = 0.0;
    if ( pcase == 1 ) {
        //  Point is above the bouguer layer \f$ P_1 ( z>0) \f$
        result = -h;
    } else if ( pcase == 2 ) {
        // Point is at the top edge of the layer \f$ P_2 ( z=0) \f$
        result = -h;
    } else if ( pcase == 3 ) {
        // Point is inside the layer \f$ P_3 ( 0<z<-h) \f$
        result =  -(h+2.*z);
    } else if ( pcase == 4 ) {
        result = h;
        // Point is at the lower edge of the Bouguer layer \f$ P_4 ( z=-h) \f$
    } else if ( pcase == 5 ) {
        // Point is under the layer  \f$ P_5 ( z<-h) \f$
        result = h;
    } else {
        // error
        std::string errmsg = "Bouguer::gravity_r_unlimited_plate() invalid pcase option.\n";
        throw std::runtime_error(errmsg);
    }
    return  G_rho_pi * result;
}

double bouguer::Bouguer::marussi_rr_unlimited_plate(const double &h,
                                                    const double &z,
                                                    const int &pcase) {

    double G_rho_pi = this->rho * this->G * M_PI;


    double result = 0.0;
    if ( pcase == 1 ) {
        //  Point is above the bouguer layer \f$ P_1 ( z>0) \f$
        result = 0.0;
    } else if ( pcase == 2 ) {
        // Point is at the top edge of the layer \f$ P_2 ( z=0) \f$
        result = h/0.;
    } else if ( pcase == 3 ) {
        // Point is inside the layer \f$ P_3 ( 0<z<-h) \f$
        result = -4.;
    } else if ( pcase == 4 ) {
        result = z/0.;
        // Point is at the lower edge of the Bouguer layer \f$ P_4 ( z=-h) \f$
    } else if ( pcase == 5 ) {
        // Point is under the layer  \f$ P_5 ( z<-h) \f$
        result = 0.0;
    } else {
        // error
        std::string errmsg = "Bouguer::marussi_rr_inlimited_plate() invalid pcase option.\n";
        throw std::runtime_error(errmsg);
    }
    return  G_rho_pi * result;
}

double bouguer::Bouguer::gravity_r_limited_plate(const double &R,
                                                 const double &h,
                                                 const double &z,
                                                 const int &pcase) {
    double G_rho_pi = -2.*this->rho * this->G * M_PI;

    double result = 0.0;
    if ( pcase == 1 ) {
        //  Point is above the bouguer layer \f$ P_1 ( z>0) \f$
        result = -std::sqrt(h*h+ 2.*h*z + R*R + z*z ) + h + std::sqrt(R*R+h*h);
    } else if ( pcase == 2 ) {
        // Point is at the top edge of the layer \f$ P_2 ( z=0) \f$
        result = -std::sqrt(h*h+R*R) + h + R;
    } else if ( pcase == 3 ) {
        // Point is inside the layer \f$ P_3 ( 0<z<-h) \f$
        result = -std::sqrt(h*h+2.*h*z+R*R+z*z) + h + std::sqrt(R*R+h*h) + 2.*z;
    } else if ( pcase == 4 ) {
        result = std::sqrt(h*h+R*R) - h - R;
        // Point is at the lower edge of the Bouguer layer \f$ P_4 ( z=-h) \f$
    } else if ( pcase == 5 ) {
        // Point is under the layer  \f$ P_5 ( z<-h) \f$
        result = -std::sqrt(h*h + 2.*h*z + R*R + z*z) - h + std::sqrt(R*R + z*z);
    } else {
        // error
        std::string errmsg = "Bouguer::gpot_v_plate() invalid pcase option.\n";
        throw std::runtime_error(errmsg);
    }
    return  G_rho_pi * result;
}

double bouguer::Bouguer::marussi_rr_limited_plate(const double &R, const double &h, const double &z, const int &pcase) {
    double G_rho_pi = 2.*this->rho * this->G * M_PI;

    double result = 0.0;
    if ( pcase == 1 ) {
        //  Point is above the bouguer layer \f$ P_1 ( z>0) \f$
        result = (h+z)/std::sqrt( (h+z)*(h+z) + R*R ) - z / std::sqrt(R*R+z*z);
    } else if ( pcase == 2 ) {
        // Point is at the top edge of the layer \f$ P_2 ( z=0) \f$
        result = h / std::sqrt(R*R+h*h);
    } else if ( pcase == 3 ) {
        // Point is inside the layer \f$ P_3 ( 0<z<-h) \f$
        result = (h+z)/std::sqrt( (h+z)*(h+z) + R*R ) - z / std::sqrt(R*R+z*z) -2.;
    } else if ( pcase == 4 ) {
        result = h / std::sqrt(R*R+h*h);
        // Point is at the lower edge of the Bouguer layer \f$ P_4 ( z=-h) \f$
    } else if ( pcase == 5 ) {
        // Point is under the layer  \f$ P_5 ( z<-h) \f$
        result = (h+z)/std::sqrt( (h+z)*(h+z) + R*R ) - z / std::sqrt(R*R+z*z);
    } else {
        // error
        std::string errmsg = "Bouguer::marussi_rr_limited_plate() invalid pcase option.\n";
        throw std::runtime_error(errmsg);
    }
    return  G_rho_pi * result;
}

double bouguer::Bouguer::gpot_v_limited_shell(const double &r1,
                                              const double &r2,
                                              const double &r,
                                              const double &intradius) {

    double G_rho_pi = 2.*this->rho * this->G * M_PI;
    double r_mean = .5 * (r1+r2);
    double psi0 = intradius/r_mean; // geocentric angle in radians

    double l1 = std::sqrt( r*r +r1*r1 -2.*r*r1 *std::cos( psi0 ) );
    double l2 = std::sqrt( r*r +r2*r2 -2.*r*r2 *std::cos( psi0 ) );

    double result = ((r1-r)*std::abs(r-r1)*(r+2.*r1) + (r-r2)*std::abs(r-r2)*(r+2.*r2))/(6.*r)
            + 0.5*r*r*std::sin(psi0)*std::sin(psi0)*std::cos(psi0)
            *std::log( (r2+l2-r*std::cos(psi0)) / (r1+l1-r*std::cos(psi0) ) )
            +  (l2*l2*l2 - l1*l1*l1)/(3.*r)
            +.5*std::cos(psi0)*(l2*(r2-r*std::cos(psi0)) - l1*(r1-r*std::cos(psi0)));

    return  G_rho_pi * result;
}

double bouguer::Bouguer::gpot_v_unlimited_shell(const double &r1,
                                                const double &r2,
                                                const double &r) // position of the point P , so for z=h r = R+h
{
    double G_rho_pi = M_PI * this->G * this->rho / (3.*r);

    return  G_rho_pi*( (r*r+r2*r-2.*r2*r2)*std::abs(r-r2)
                       -(r*r+r1*r-2.*r1*r1)*std::abs(r-r1)
                       -2.*r1*r1*r1-3.*r*r1*r1+2.*r2*r2*r2+3.*r*r2*r2);
}

double bouguer::Bouguer::gravity_r_limited_shell(const double &r1,
                                                 const double &r2,
                                                 const double &r,
                                                 const double &intradius) {

    double G_rho_pi = this->rho * this->G * M_PI / 3.;
    double r_mean = .5 * (r1+r2);
    double psi0 = intradius/r_mean; // geocentric angle in radians

    double l1 = std::sqrt( r*r +r1*r1 -2.*r*r1 *std::cos( psi0 ) );
    double l2 = std::sqrt( r*r +r2*r2 -2.*r*r2 *std::cos( psi0 ) );

    double result = ( (r*r+r2*r-2.*r2*r2 )*bouguer::sgn(r-r2) - (r*r+r1*r-2.*r1*r1)*bouguer::sgn(r-r1))/r
            +(std::abs(r-r2)*(r*r+2.*r2*r2) - std::abs(r-r1)*(r*r+2.*r1*r1))/(r*r)
            +3.*r*r*std::sin(psi0)*std::sin(psi0)*std::cos(psi0)
            *( ((r1+l1)*std::cos(psi0)-r)/(l1*(-r*std::cos(psi0)+r1+l1))
               -((r2+l2)*std::cos(psi0)-r)/(l2*(-r*std::cos(psi0)+r2+l2))
               )
            +3.*std::cos(psi0)
            *(  (r*std::cos(psi0) - r1)*(r-r1*std::cos(psi0))/l1
                -(r*std::cos(psi0) - r2)*(r-r2*std::cos(psi0))/l2
                )
            +3.*std::cos(psi0)*std::cos(psi0)*(l1-l2)
            +6.*( l1*(r1*std::cos(psi0)-r) + l2*(r-r2*std::cos(psi0)) )/r
            +2.*(l1*l1*l1-l2*l2*l2)/(r*r)
            +6.*r*std::sin(psi0)*std::sin(psi0)*std::cos(psi0)
            *std::log( (-r*std::cos(psi0)+r2+l2)/(-r*std::cos(psi0)+r1+l1) );
    return  G_rho_pi * result;
}

double bouguer::Bouguer::gravity_r_unlimited_shell(const double &r1,
                                                   const double &r2,
                                                   const double &r) // position of the point P , so for z=h r = R+h
{
    double G_rho_pi = M_PI*this->G*this->rho/(3.*r*r);

    return G_rho_pi*( (r*r+r2*r-2.*r2*r2)*bouguer::sgn(r-r2)*r
                      -(r*r+r1*r-2.*r1*r1)*bouguer::sgn(r-r1)*r
                      +(r*r+2.*r2*r2)*std::abs(r-r2)
                      -(r*r+2.*r1*r1)*std::abs(r-r1) - 2.*(r2*r2*r2 - r1*r1*r1)
                      );
}

double bouguer::Bouguer::marussi_rr_unlimited_shell(const double &r1,
                                                    const double &r2,
                                                    const double &r) {

    double G_rho_pi = -2.*M_PI*this->G*this->rho/(3.*r*r*r);

    return G_rho_pi*(
                -2.*r1*r1*std::abs(r-r1) + 2.*r2*r2*std::abs(r-r2)
                +3.*r*r*r*( bouguer::sgn(r-r1) - bouguer::sgn(r-r2) )
                +2.*r1*r1*r* bouguer::sgn(r-r1) - 2.*r2*r2*r*bouguer::sgn(r-r2)
                +2.*r1*r1*r1-2.*r2*r2*r2
                );
}

void bouguer::Bouguer::set_new_Newton(const double n_G) {
    this->G = n_G;
}
