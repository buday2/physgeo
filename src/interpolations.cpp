#include "../include/interpolations.h"

// void free_matrix(double **A, int lines, int cols)
void interpol::free_matrix(double** A, int lines) {
  for (int i = 0; i < lines; i++) {
    delete A[i];
    A[i] = NULL;
  }
  delete A;
}

double interpol::Lagrange_Int(
    map<double, double> InputData,
    // double Lagrange_Int(map<double, double> InputData,
    double T,
    unsigned int n) {
  /* Source from:
   * http://mathworld.wolfram.com/LagrangeInterpolatingPolynomial.html
   *
   * InputData (t_i , ft_i)
   * T - Interpolated value
   * n - degree of the polynomial
   */

  // Alternative selection of closest data
  const double EPS = __EPS__;
  const double DoubleMAX = 1.7976931348623158e+308;

  InputData[T] = DoubleMAX;  // -max value for double

  unsigned int i = 0;
  for (map<double, double>::iterator It = InputData.begin();
       It != InputData.end(); It++) {
    if ((It->first) == T && (It->second) == DoubleMAX) {
      break;
    } else
      i++;
  }

  vector<double> TIME, FTIME;

  for (map<double, double>::iterator It = InputData.begin();
       It != InputData.end(); It++) {
    TIME.push_back(It->first);
    FTIME.push_back(It->second);
  }

  if (T < *min_element(TIME.begin(), TIME.end()) ||
      T > *max_element(TIME.begin(), TIME.end())) {
    cout << "Extrapolation is not allowed!" << endl;
    return DoubleMAX;  // returns double max value
  }

  if (i <= n - 1) {
    cout << "Degree of interpolation polynomial is too high!" << endl;
    return DoubleMAX;
  }

  double *choosenT, *choosenFT;
  choosenT = new double[n + 1];
  choosenFT = new double[n + 1];

  choosenT[0] = TIME[i - 1];
  choosenT[1] = TIME[i + 1];
  choosenFT[0] = FTIME[i - 1];
  choosenFT[1] = FTIME[i + 1];

  int k = 2, l = 2, m = 2;  // number of selected values
  while (m != (int)n + 1) {
    if (((int)i) - k < 0) {
      choosenT[m] = TIME[i + m];
      choosenFT[m] = FTIME[i + m];
      m++;
    } else if (i + l >= FTIME.size()) {
      choosenT[m] = TIME[i - m];
      choosenFT[m] = FTIME[i - m];
      m++;
    } else {
      if (abs(TIME[i - k] - T + EPS) >= abs(TIME[i + l] - T)) {
        choosenT[m] = TIME[i + l];
        choosenFT[m] = FTIME[i + l];
        l++;
        m++;
      } else if (abs(TIME[i - k] - T + EPS) < abs(TIME[i + l] - T)) {
        choosenT[m] = TIME[i - k];
        choosenFT[m] = FTIME[i - k];
        k++;
        m++;
      }
    }
  }

  double *Li, *L;
  Li = new double[n + 1];
  L = new double[n + 1];

  unsigned int a;
  for (unsigned int I = 0; I < n + 1; I++) {
    a = 0;
    for (unsigned int J = 0; J < n + 1; J++) {
      if (J != I) {
        Li[a] = (T - choosenT[J]) / (choosenT[I] - choosenT[J]);
        a++;
      } else
        continue;
    }
    L[I] = Li[0];
    for (unsigned int J = 1; J < a; J++) {
      L[I] *= Li[J];
    }
  }

  double IV = 0;  // IV - interpolated value

  for (unsigned int I = 0; I < n + 1; I++) {
    IV += L[I] * choosenFT[I];
  }

  delete[] choosenT;
  delete[] choosenFT;
  delete[] L;
  delete[] Li;

  return IV;
}

double interpol::Newton_Int(map<double, double> InputData,
                            // double Newton_Int( map<double, double> InputData,
                            double T,
                            unsigned int n) {
  /* Source:
   * http://mathworld.wolfram.com/NewtonsDividedDifferenceInterpolationFormula.html
   * ---------------------------------------- */
  /* InputData (t_i , ft_i)
   * T - Interpolated value
   * n - degree of the polynomial             */

  const double EPS = 2.220446049250313081E-16;
  const double DoubleMAX = 1.7976931348623158e+308;

  InputData[T] = DoubleMAX;  // -max value for double

  unsigned int i = 0;
  for (map<double, double>::iterator It = InputData.begin();
       It != InputData.end(); It++) {
    if ((It->first) == T && (It->second) == DoubleMAX) {
      break;
    } else
      i++;
  }

  vector<double> TIME, FTIME;

  for (map<double, double>::iterator It = InputData.begin();
       It != InputData.end(); It++) {
    TIME.push_back(It->first);
    FTIME.push_back(It->second);
  }

  if (T < *min_element(TIME.begin(), TIME.end()) ||
      T > *max_element(TIME.begin(), TIME.end())) {
    cout << "Extrapolation is not allowed!" << endl;
    return DoubleMAX;  // returns double max value
  }

  if (i <= n - 1) {
    cout << "Degree of interpolation polynomial is too high!" << endl;
    return DoubleMAX;
  }

  double *choosenT, *choosenFT;
  choosenT = new double[n + 1];
  choosenFT = new double[n + 1];

  choosenT[0] = TIME[i - 1];
  choosenT[1] = TIME[i + 1];
  choosenFT[0] = FTIME[i - 1];
  choosenFT[1] = FTIME[i + 1];

  int k = 2, l = 2, m = 2;  // number of selected values
  while (m != (int)n + 1) {
    if (((int)i) - k < 0) {
      choosenT[m] = TIME[i + m];
      choosenFT[m] = FTIME[i + m];
      m++;
    } else if (i + l >= FTIME.size()) {
      choosenT[m] = TIME[i - m];
      choosenFT[m] = FTIME[i - m];
      m++;
    } else {
      if (abs(TIME[i - k] - T + EPS) >= abs(TIME[i + l] - T)) {
        choosenT[m] = TIME[i + l];
        choosenFT[m] = FTIME[i + l];
        l++;
        m++;
      } else if (abs(TIME[i - k] - T + EPS) < abs(TIME[i + l] - T)) {
        choosenT[m] = TIME[i - k];
        choosenFT[m] = FTIME[i - k];
        k++;
        m++;
      }
    }
  }
  // Ordering chosen values
  map<double, double> OrderedData;
  for (unsigned int i = 0; i < n + 1; i++) {
    OrderedData[choosenT[i]] = choosenFT[i];
  }

  delete[] choosenT;
  delete[] choosenFT;

  double *TT, *FT;
  TT = new double[n + 1];  // non vector class!!!
  FT = new double[n + 1];

  int ii = 0;
  for (map<double, double>::iterator It = OrderedData.begin();
       It != OrderedData.end(); It++) {
    TT[ii] = (It->first);
    FT[ii] = (It->second);
    ii++;
  }
  /*          |=>  Divided Difference:
   * xi  fi   |   f[xi,xi+1]      f[xi,x+1,x+2]   ...
   * ---------|--------------------------------------
   * x0  f0   |
   *          |   f[x0,x1]
   * x1  f1   |                   f[x0,x1,x2]
   *          |   f[x1,x2]                        f[x0,x1,...,x3]
   * x2  f2   |                   f[x1,x2,x3]
   *          |   f[x2,x3]
   * .   .    .
   * .   .    .
   * .   .    .
   *
   * xn  fn */

  double** DD; /* =>  Divided Difference: */
  DD = new double*[n];
  for (unsigned int i = 0; i < n; i++) {
    DD[i] = new double[n];
  }

  for (unsigned int i = 0; i < n; i++) {
    for (unsigned int j = 0; j < n - i; j++) {
      if (i == 0) {
        DD[i][j] = (FT[j + 1] - FT[j]) / (TT[j + 1] - TT[j]);
      } else {
        DD[i][j] = (DD[i - 1][j + 1] - DD[i - 1][j]) / (TT[i + j + 1] - TT[j]);
      }
    }
  }
  /* POLYNOMIAL CONSTRUCTION */ /* IS WRONG */
  double FT_inter, Inter;
  FT_inter = FT[0];
  for (unsigned int i = 0; i < n; i++) {
    Inter = 1;
    for (unsigned int j = 0; j <= i; j++) {
      Inter *= T - TT[j];
    }
    FT_inter += DD[i][0] * Inter;
  }
  return FT_inter;
}

void interpol::order_by_col(arma::mat& dat, unsigned int col) {
  // void order_by_col ( arma::mat& dat, unsigned int col ){
  // order data asceding according to first column in matrix
  if (col >= dat.n_cols) {
    col = 0;
  }

  unsigned int go_n = 1, n = 0;

  while (go_n != 0) {
    if ((n < dat.n_rows - 1)) {
      if ((dat(n, col) > dat(n + 1, col)) /*&& ( dat(n,1) < dat(n+1,1)) */) {
        dat.swap_rows(n, n + 1);
        go_n++;
      }
    }
    n++;
    if (n == dat.n_rows && go_n == 1) {
      go_n = 0;
    } else if (n == dat.n_rows && go_n != 1) {
      go_n = 1;
      n = 0;
    } else if (n == 100 * dat.n_rows) {
      break;
    }
  }
};

arma::mat interpol::find_closest_points3D(double x,
                                          double y,
                                          double z,
                                          arma::mat xyz,
                                          unsigned int i) {
  // arma::mat find_closest_points3D( double x, double y, double z, arma::mat
  // xyz, unsigned int i){
  /* Finding i-number of closest points      */

  xyz.col(0) = xyz.col(0) - x;
  xyz.col(1) = xyz.col(1) - y;
  xyz.col(2) = xyz.col(2) - z;

  arma::colvec l;
  l = sqrt(xyz.col(0) % xyz.col(0) + xyz.col(1) % xyz.col(1) +
           xyz.col(2) % xyz.col(2));
  xyz.col(0) = xyz.col(0) + x;
  xyz.col(1) = xyz.col(1) + y;
  xyz.col(2) = xyz.col(2) + z;
  arma::mat data = arma::join_rows(xyz, l);

  // interpol::order_by_col(data, 3);
  order_by_col(data, 3);

  arma::mat close_pt = arma::zeros<arma::mat>(i, 3);

#pragma omp parallel shared(close_pt, data)
  {
    for (unsigned int k = 0; k < i; k++) {
      close_pt.row(k) = data.submat(k, 0, k, 2);
    }
  }

  return close_pt;
}

double interpol::length_on_sphere(double b1,
                                  double b2,
                                  double l1,
                                  double l2,
                                  double r) {
  // double length_on_sphere (double b1, double b2, double l1, double l2, double
  // r){
  /* dist on sphere with radius r between two points p(b1,l1) and q(p2,l2)
     b_i, l_i in deg format
  */
  double d2r =
      3.14159265358979323851280895940618620443274267017841339111328125 / 180.;

  b1 *= d2r;
  b2 *= d2r;
  l1 *= d2r;
  l2 *= d2r;

  return abs(r * acos(sin(b1) * sin(b2) + cos(b1) * cos(b2) * cos(l2 - l1)));
};

double interpol::close_neighbour_method(double phi,
                                        double lambda,
                                        arma::mat xyz,
                                        double maxdist,
                                        unsigned int maxpt) {
  /* phi, lambda - point of interest
     blh - general data
     xyz - blh data in 3d cartesian coordinates
     maxdist - maximal allowed distance
     maxpt - maximum number of points
  */

  /* Method of closest point */
  /* Calculate mean radius for point */
  double PI = 3.14159265358979323851280895940618620443274267017841339111328125L;
  double phi_rad = phi * (PI / 180.); /* converting to radians */
  double lambda_rad = lambda * (PI / 180.);
  double m, n, r_nm, ee, a, x, y, z;
  vector<double> blh_sel;
  arma::rowvec tempvec;
  struct geo_f::ellipsoid<double> wgs84 = geo_f::get_wgs84<double>();

  a = 6378137.000;
  ee = pow(0.0818191908426215, 2.0);

  m = (a * (1.0 - ee)) / pow(1.0 - ee * pow(sin(phi_rad), 2.0), 3.0 / 2.0);
  n = a / sqrt(1.0 - ee * pow(sin(phi_rad), 2.0));

  x = n * (cos(phi_rad) * cos(lambda_rad));
  y = n * (cos(phi_rad) * sin(lambda_rad));
  z = n * (1.0 - ee) * sin(phi_rad);

  r_nm = sqrt(m * n);
  /* Calculate mean radius for point - END */

  arma::mat close_pts = interpol::find_closest_points3D(x, y, z, xyz, maxpt);

  arma::mat blh_selected = arma::zeros<arma::mat>(close_pts.n_rows, 3);
  for (unsigned int k = 0; k < blh_selected.n_rows; k++) {
    blh_sel = geo_f::xyz2blh(close_pts(k, 0), close_pts(k, 1), close_pts(k, 2),
                             wgs84);
    tempvec = arma::conv_to<arma::rowvec>::from(blh_sel);
    blh_selected.row(k) = tempvec;

    blh_sel.clear();
    tempvec.reset();
  }

  arma::colvec dist(blh_selected.n_rows);

  //    #pragma omp parallel shared (dist, blh_selected, phi, lambda, r_nm)
  //    {
  //        #pragma omp schedule
  for (unsigned int i = 0; i < blh_selected.n_rows; i++) {
    dist(i) = length_on_sphere(phi, blh_selected(i, 0), blh_selected(i, 1),
                               lambda, r_nm);
  };
  //    }

  arma::mat temp = arma::join_rows(blh_selected, dist);
  interpol::order_by_col(temp, 3);

  if (temp(0, 3) > maxdist) {
    cout << "For point b: " << phi << " and l: " << lambda
         << " no close point was found.";
    cout << "Returning \"inf\"" << endl;
    return (1. / 0.0);
  };

  unsigned int i = 0;
  double pv = 0.0;
  double p = 0.0;
  double ptemp;
  while (temp(i, 3) <= maxdist && i < temp.n_rows) {
    ptemp = 1. / pow(temp(i, 3) + EPS, 2.);
    p += ptemp;
    pv += ptemp * temp(i, 2);
    i++;
    if (i == maxpt)
      break;
  };

  cout << "return:" << pv / p << endl;
  return (pv / p);
};

arma::mat interpol::load_arma_mat(const char* path) {
  // arma::mat load_arma_mat (const char* path ){
  /* load matrix from file */
  arma::rowvec row_temp;
  ifstream f(path);
  vector<string> v;
  vector<double> vec_double;
  string line, word;
  unsigned int i = 0;
  unsigned int j;

  while (getline(f, line)) {
    ++i;
    if (i == 1) {
      istringstream is(line);
      while (is >> word)
        v.push_back(word);
      j = v.size();
      v.clear();
    } else {
      continue;
    }
  }

  f.clear();
  f.seekg(0, f.beg);

  arma::mat matfromfile = arma::zeros<arma::mat>(i, j);

  i = 0;
  while (getline(f, line)) {
    istringstream is(line);
    while (is >> word)
      vec_double.push_back(stod(word));

    row_temp = arma::conv_to<arma::rowvec>::from(vec_double);
    matfromfile.row(i) = row_temp;
    row_temp.reset();
    vec_double.clear();
    i++;
  }
  return matfromfile;
};
