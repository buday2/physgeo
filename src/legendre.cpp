#include "../include/legendre.h"

arma::cube legendre::legendre(unsigned int l_max, const arma::vec &data)
{
    unsigned int i, m, l;
    unsigned int n = static_cast<unsigned int>( data.n_elem );

    arma::cube P = arma::zeros<arma::cube>(l_max+1, l_max+1, data.n_elem);

    // start with P(0,0) = 1.0;
    for(i = 0; i < n; i++) {
        P(0, 0, i) = 1.0;
    }

    arma::vec y(data.n_elem);
    for(i = 0; i < data.n_elem; i++) {
        y(i) = sqrt(1.0 - data(i)*data(i));
    }

    // using #2 build up diagonal.
    for(m = 1; m <= l_max; m++) {
        double c1 = (1 - 2.0*m);
        for(i = 0; i < n; i++) {
            P(m,m,i) = c1 * y(i) * P(m-1,m-1,i);
        }
    }

      // using #3 build off-diagonal entries.
      for(m=0; m < l_max; m++) {
          for(i = 0; i < n; i++) {
            P(m+1, m, i) = (2*m+1) * data(i) * P(m, m, i);
          }
      }

      // Now we can fill out remaining entries using relation #1.
      for(m =  0; m <= l_max; m++) {
        for(l = m+2; l <= l_max; l++)
        {
          double c1 = (2.0*l-1.0) / (l-m);
          double c2 = (l+m-1.0) / (l-m);
          for(i = 0; i < n; i++)
            P(l, m, i) = c1 * data(i) * P(l-1, m, i) - c2 * P(l-2, m, i);
        }
      }
      return P;
}

arma::cube legendre::normalized_legendre(unsigned int l_max, const arma::vec &x)
{
  unsigned int i, m, l;
  unsigned int n = static_cast<unsigned int>( x.n_elem );

  arma::cube P = arma::zeros<arma::cube>(l_max+1, l_max+1, x.n_elem);

  // start with P(0,0) = 1.0;
  for(i = 0; i < n; i++) {
    P(0, 0, i) = 1.0/sqrt(4.0*M_PI);
  }

  // using #2 build up diagonal.\f$ \sqrt(1-x^2) = \sqrt((1-x)*(1+x)) \f$
  for(m = 1; m <= l_max; m++)  {
    double mm = static_cast<double>( m );
    double c1 =  sqrt( (2.*mm+1.) / (2.*mm) );
    for(i = 0; i < n; i++) {
        P(m, m, i) = - c1 * sqrt(1-x(i)*x(i)) * P(m-1, m-1, i);
    }
  }

  // using #3 build off-diagonal entries.
  for(m=0; m < l_max; m++) {
    double c1 = sqrt(2.0*m+3.0);
    for(i = 0; i < n; i++) {
      P(m+1, m, i) = c1 * x(i) * P(m, m, i);
    }
  }

  // Now we can fill out remaining entries using relation #1.
  for(m =  0; m <= l_max; m++) {
    for(l = m+2; l <= l_max; l++)
    {
      double c1 = sqrt( ((2.0*l+1)*(2.0*l-1)) / ((l+m)*(l-m)));
      double c2 = sqrt( (2.0*l+1)*(l-m-1.0)*(l+m-1.0) / ((2.0*l-3)*(l-m)*(l+m)));
      for(i = 0; i < n; i++)
        P(l, m, i) = c1 * x(i) * P(l-1, m, i) - c2 * P(l-2, m, i);
    }
  }

  return P;
}

arma::vec legendre::legendre(unsigned int n_max, const double &x)
{
    arma::vec pvec = arma::zeros<arma::vec>( n_max+1 );

    if ( n_max == 0 ) {
        pvec(0) = 1.;
    } else if ( n_max == 1 ) {
        pvec(0) = 1.0;
        pvec(1) = x;
    } else {
        pvec(0) = 1.0;
        pvec(1) = x;

        for (unsigned int i = 2; i <= n_max ; i++ ){
            double m = static_cast<double>(i);
            pvec(i) = ((2.*m-1.)*x*pvec(i-1) - (m-1.)*pvec(i-2))/m;
        }
    }
    return pvec;
}



double legendre::sum_kernel(const arma::vec &p_n,
                            const string &kernel_type,
                            const unsigned &n_min,
                            const unsigned &n_max)
{
    unsigned int pn_size = p_n.n_elem;

    if ( p_n.is_empty() ) {
        cerr << "Funciton parmaeter p_n in legendre::sum_kernel() is empty!" << endl;
        return 0.0;
    } else {
        unsigned int min_idx = 2;
        unsigned int max_idx = pn_size - 1;
        if ( kernel_type != "stokes"  ) {
            // hotine kernel
            min_idx = ( n_min >= pn_size ) ? pn_size-1 : n_min ;
            max_idx = ( n_max < pn_size ) ? n_max : pn_size-1 ;
        } else {
            // stokes kernel
            if ( n_min >= 2 && n_min < pn_size ) {
                min_idx = n_min;
            } else if ( n_min < 2 ) {
                min_idx = 2;
            } else {
                min_idx = pn_size-1;
            }
            max_idx = ( n_max < pn_size ) ? n_max : pn_size-1 ;
        }

        double sum_kernel = 0.0;
        // #pragma omp parallel for
        for ( unsigned i = min_idx; i <= max_idx; i++) {
            double n = static_cast<double>(i);
            double q;

            if ( kernel_type != "stokes" ) {
                // hotine kernel
                q = (2.*n+1.)/(n+1.); // f$ \sum \limits_{n=0}^{\infty} \frac{2n+1}{n+1} P_n (\cos \psi) \f$
            } else {
                // stokes kernel
                q = (2.*n+1.)/(n-1.); // f$ \sum \limits_{n=2}^{\infty} \frac{2n+1}{n-1} P_n (\cos \psi) \f$
            }

            sum_kernel += q*p_n(i);
        }

        return sum_kernel;
    }
}
