#include "../include/gaussquad.h"

Gaussquad::Gaussquad( unsigned n , const string& path )
{
    ifstream f; f.open( path  );
    bool fexist;

    if ( !f.is_open() ) {
        cout << "File with Bessel zeros \"" << path << "\"not found." << endl;
        fexist = false ;
    } else {
        fexist = true;
    }
    f.close();

    if ( fexist ) {
        besselJ0.load ( arma::hdf5_name( path , "bessel0") );      // alpha = 0
        besselJalpha.load(  arma::hdf5_name( path , "bessel01") ); // alpha 0.1

        if ( n > besselJ0.n_elem ) {
            // In besselJ0 there are 20 000 values stored
            unsigned m_elem = besselJ0.n_elem;
            besselJ0.resize( n );

            for (unsigned i = m_elem; i < n; i++) {
                besselJ0(i) = besselJ0(i-1) + M_PI;
            }
        }

        if ( n > besselJalpha.n_elem ) {
            // In besselJ0 there are 20 000 values stored
            unsigned m_elem = besselJalpha.n_elem;
            besselJalpha.resize( n );

            for (unsigned i = m_elem; i < n; i++) {
                besselJalpha(i) = besselJalpha(i-1) + M_PI;
            }
        }

        nw = Gaussquad::legen_zeros( n,false) ;
    } else {
        string msg_error = "File with Bessel zeros not found. Can not compute roots of the Legendre polynomials";
        throw logic_error( string( msg_error ));
    }
}

double Gaussquad::int1d(double (*fx)(double), double a, double b)
{
    double integral = 0.;
    double alpha = (b-a)/2.;
    //double beta = (b+a)/2.;
    for ( unsigned i = 0; i < nw.n_rows; i++) {
        double wi = arma::as_scalar( nw(i,1) );
        double xi = arma::as_scalar( nw(i,0) );
        integral += wi * fx( a + alpha * ( 1. + xi ) );
    }

    return alpha*integral;
}

double Gaussquad::int2d(double (*fxy)(double, double), double x1, double x2, double y1, double y2)
{
    double iintegral = 0.;

    double dx12_2 = (x2-x1)/2.;
    double dy12_2 = (y2-y1)/2.;

    for (unsigned i = 0; i < nw.n_rows; i++) {
        double wi = arma::as_scalar( nw(i,1) );
        double xi = arma::as_scalar( nw(i,0) );
        for (unsigned j = 0; j < nw.n_rows; j++) {
            double wj = arma::as_scalar( nw(j,1) );
            double eta = arma::as_scalar( nw(j,0) );

            iintegral += wi*wj*fxy(  x1 + dx12_2 * (1.+xi) , y1 + dy12_2 * (1.+eta));
        }
    }

    return (iintegral*dx12_2*dy12_2);
}

void Gaussquad::view()
{
    cout << scientific << setw(16) << setprecision(8);
    nw.raw_print(cout, "Nodes and weights:");

}

arma::vec Gaussquad::legendreP(arma::vec xvec, unsigned n)
{
    arma::vec pn_i  = arma::ones<arma::vec>( xvec.n_elem );
    arma::vec pn_ii = xvec;
    arma::vec pn_iii;

    if ( n == 0 ) {
        return pn_i ;
    } else if  (n == 1 ) {
        return pn_ii;
    } else {
        for ( unsigned i = 2 ; i <= n ; i++ ) {
            // \f$  P_{i+1}^n (x) = \frac{ (2i - 1) \cdot x * P_i^n(x) - (i-1) P_{i-1}^n(x) }{i}  \f$
            pn_iii = (( 2.*i-1 ) * xvec % pn_ii - (i-1)*pn_i) / ( i ); // % element wise multiplication of two objects

            pn_i = pn_ii;
            pn_ii = pn_iii;
        }
    }
    return pn_iii;
}

arma::vec Gaussquad::dlegendreP(arma::vec xvec, unsigned n)
{
    arma::vec dpn_i  = arma::zeros<arma::vec>( xvec.n_elem );
    arma::vec dpn_ii = arma::ones<arma::vec>( xvec.n_elem );

    arma::vec pn_i = arma::ones<arma::vec>( xvec.n_elem );
    arma::vec  pn_ii = xvec;

    arma::vec pn_iii, dpn_iii;

    if ( n == 0 ) {
        return dpn_i;
    } else if  (n == 1 ) {
        return dpn_ii;
    } else {
        for ( unsigned i = 2 ; i <= n ; i++ ) {
            pn_iii = (( 2.*i-1 ) * xvec % pn_ii - (i-1)*pn_i) / ( i );
            dpn_iii = (2*i-1) * pn_ii + dpn_i;

            pn_i = pn_ii;
            pn_ii = pn_iii;
            dpn_i = dpn_ii;
            dpn_ii = dpn_iii;
        }
        return dpn_iii;
    }
}

arma::mat Gaussquad::legen_zeros(unsigned n, bool alg, double alpha, double beta)
{
    //    n - degree of the legendre polynomials
    //    bessel0 - vector with precomputed roots of the Bessel polynomials
    //    of the first kind. \f$ J_\alpha (x) \f$
    //    alg - False for standard alg, True for Olver's approximation
    //    alpha - coefficient
    //    beta - coefficient


    //    so that we need only evaluate Jacobi polynomials on the right half of the interval, i.e.,
    //    x ∈ [0, 1), θ ∈ (0, π/2].


    //    #TODO#1# - not using symmetry yet
    //    #TODO#2# - add order degree than 20 000

    arma::vec xk_vec = arma::zeros<arma::vec>(n);
    arma::vec weights = arma::zeros<arma::vec>(n);

    arma::mat nodes( n,2 );

    double nn = static_cast<double>(n);
    double xk;

    if ( alg ) {
        // Olver's approximation
        for ( unsigned k = 1 ; k <= n ; k++ ) {
            double rho = nn + ( alpha + beta + 1. )/2.;
            double phi_k = ( static_cast<double>(k) + alpha/2. - 1./4. )*M_PI / rho;

            // \f$ \psi_k = \frac{J_{\alpha, k }  }{\rho}\f$
            double psi_k = besselJalpha(k-1) / rho; // besseljalpha

            double mu = sqrt(rho*rho + (1. - alpha*alpha - 3.*beta*beta)/12. );



            if ( phi_k < M_PI/3. ) { // #  for x > 1/2 (i.e., \f$  \theta < \pi/3 \f$ ) .
                //Bessel like regions
                //Pittaluga’s approximation
                xk = cos( psi_k + (alpha*alpha - .25)* (psi_k / tan(psi_k) -1.)/(2.*rho*rho*psi_k)
                         -(alpha*alpha-beta*beta) * tan( phi_k/2. ) / (4.*rho*rho) );

            } else {
                //Nodes by Gatteshi
                xk = cos( besselJalpha(k-1)/mu * (1. - (4. - alpha*alpha - 15.*beta*beta)/(720.*pow(mu,4.))
                                * ( pow(besselJalpha(k-1) , 2.) / 2. + alpha*alpha - 1. )));
            }

            xk_vec(k-1) = xk;
        }
    } else {
        //Standard model
        for ( unsigned k = 1 ; k <= n ; k++ ) {
            // bar_k  = n - k + 1;
            //old school Newton's method
            double theta_k = ( static_cast<double>(k)-.25)*M_PI/(nn+.5); // (k − 1/4) π/(n + 1/2),
            double j0_k = besselJ0(k-1);

            double psi_k = j0_k / ( nn + 1/2);

            if ( theta_k < M_PI/3. ) { // (3.6) for x > 1/2 (i.e., \f$  \theta < \pi/3 \f$ ) .
                //Bessel like regions
                xk = cos( psi_k + ( psi_k / tan(psi_k) -1. )/(8. * psi_k * pow( nn + .5 , 2.) ));
            } else {
                //Trigonometric like regions \f$  \pi/3 \leq \theta < \pi/2 \f$
                double ctg_theta = 1./tan( theta_k );
                xk = cos( theta_k +  ctg_theta /(8.*pow( nn + .5 , 2.)) );
            }
            xk_vec(k-1) = xk;
        }
    }
    // iterative solution

    arma::vec xkkvec = xk_vec;
    arma::vec delta_x, dpn_xvec, pn_xvec;
    unsigned it = 0;

    do {
        pn_xvec = Gaussquad::legendreP( xk_vec , n );
        dpn_xvec = Gaussquad::dlegendreP( xk_vec , n );

        xkkvec = xk_vec - pn_xvec /  dpn_xvec ; // Newton's iteration method
                                                // for polynomial roots

        delta_x = abs( xkkvec - xk_vec );


        it+=1;
        xk_vec = xkkvec;

        if ( it == 99 )
            break;
    } while ( arma::max( delta_x ) > __EPS__  );
    weights = 2./ ((1. -  xk_vec % xk_vec ) % ( dpn_xvec % dpn_xvec));

    nodes.col(0) = xk_vec;
    nodes.col(1) = weights;

    return nodes;// , weights; << put data into matrix
}



double Gaussquad::poly_construction(double xi, arma::colvec xvec)
{
    double fx = 0.;
    if ( !xvec.is_empty() ) {
        for (unsigned int i=0; i< xvec.n_elem; i++){
            fx += pow(xi, static_cast<double>(i) )* arma::as_scalar(xvec(i));
        }
    }
    return fx;
}


arma::colvec Gaussquad::polynom_dotproduct (arma::colvec p1, arma::colvec p2){
    /*
     *
     *
     *
     */
    unsigned int p_1, p_2;
    p_1 = p1.n_rows;
    p_2 = p2.n_rows;
    arma::colvec p_product = arma::zeros<arma::mat>(p_1+p_2-1,1);

    for (unsigned int i = 0; i< p_1; i++){
        for (unsigned int j=0; j<p_2; j++){
        p_product(i+j) += p1(i)*p2(j);
        }
    }
    return p_product;
}


double Gaussquad::pol_dotproduct(arma::colvec xvec, arma::colvec yvec)
{
    /* Generally is no possible to multiply vectors of different
     * dimensions. So I need to reshape (resize) the smaller one.
     */
    int xvec_el = xvec.n_elem;
    int yvec_el = yvec.n_elem;

    if ( xvec_el < yvec_el ) {
        xvec.reshape( yvec_el , 1 );
    } else if ( xvec_el > yvec_el ) {
        yvec.reshape( xvec_el , 1 );
    }

    arma::colvec dotprod = xvec % yvec;
    return  arma::accu(dotprod);

}

arma::colvec Gaussquad::add_polynoms(arma::colvec xvec, arma::colvec yvec)
{
    /* Generally is no possible to add vectors of different
     * dimensions. So I need to reshape (resize) the smaller one.
     */
    int xvec_el = xvec.n_elem;
    int yvec_el = yvec.n_elem;

    if ( xvec_el < yvec_el ) {
        xvec.reshape( yvec_el , 1 );
    } else if ( xvec_el > yvec_el ) {
        yvec.reshape( xvec_el , 1 );
    }

    return (  xvec + yvec );
}

arma::colvec Gaussquad::substract_polynoms(arma::colvec xvec, arma::colvec yvec)
{
    int xvec_el = xvec.n_elem;
    int yvec_el = yvec.n_elem;

    if ( xvec_el < yvec_el ) {
        xvec.reshape( yvec_el , 1 );
    } else if ( xvec_el > yvec_el ) {
        yvec.reshape( xvec_el , 1 );
    }

    return (  xvec - yvec );
}

arma::colvec Gaussquad::legendre_pol(unsigned int m)
{
    /* Recurent formula for Legendre polynomials
     * P_{n+1}(x) = \frac{2n+1}{n+1} \cdot x P_n (x) - \frac{n}{n-1} P_{n-1} (x)
     */

    arma::colvec p_0 = arma::ones<arma::colvec>(1);
    arma::colvec p_1 = arma::zeros<arma::colvec>(2);
    arma::colvec p_x = arma::zeros<arma::colvec>(2);

    p_0(0) = 1.; //  := 1
    p_1(1) = 1.; //  := x
    p_x = p_1;

    if ( m == 0 ){
        return p_0;
    } else if (m == 1 ){
        return p_1;
    };

    arma::colvec p_nii, p_ni, p_n;   /* p_nii = P_{n+1}(x)
                                * p_ni = P_n (x)
                                * p_n = P_{n-1}(x) */
    arma::colvec p_dotproduct;
    double a1,a2,nn;
    for (unsigned int n = 2; n <= m ; n++ ){
        nn = static_cast<double>(n);
        a1 = (2.*nn-1.)/(nn);    // A little different from original formula
        a2 = -((nn-1.)/nn); // but it starts with n-1 degree

        if ( n== 2){
            p_n = p_0;
            p_ni = p_1;

            p_dotproduct =  Gaussquad::polynom_dotproduct(p_x, (p_ni*a1));
            p_nii = Gaussquad::add_polynoms(p_dotproduct, (a2*p_n));
        }
        else {
            p_dotproduct =  Gaussquad::polynom_dotproduct(p_x, (p_ni*a1));
            p_nii = Gaussquad::add_polynoms(p_dotproduct, (a2*p_n));
        }
        p_n = p_ni;
        p_ni = p_nii;

        p_dotproduct.reset();
        p_nii.reset();
    }
    p_nii = p_ni;

    return p_nii;
}

arma::colvec Gaussquad::poly_derivative(arma::colvec xvec, unsigned n)
{
    /* n - order of derivative
     * polynom of degree n := x' = [a0 a1 a2 .... an]
     *
     * := a0 + a1 *  x + a2 * x^2 + .... + an * x^n
     */
    unsigned int j = xvec.n_rows;
    if ( xvec.is_empty() ){
        cout << "Nothing to differentiate." << endl;
        arma::colvec nodiff;
        return nodiff;
    } else if ( n == 0 ){
        arma::colvec nodiff;
        return nodiff;
    }

    if ( n+1 >= j ){
        xvec.zeros();
        arma::colvec nodiff;
        return nodiff;
    };

    arma::mat diff_mat = arma::zeros<arma::mat>(j, j);


    for (unsigned int i = 0; i<j-1; i++){
        diff_mat(i,i+1) = static_cast<double>(i+1); /* construction of derivate matrix */
    };

    arma::mat higher_diff = arma::eye<arma::mat>(j, j);

    for (unsigned int i=1; i <= n; i++ ){
        higher_diff = higher_diff*diff_mat;
    };

    arma::colvec diff_xvec = higher_diff*xvec;

    return diff_xvec;
}

arma::vec Gaussquad::transform_nodes(double c, double d)
{
    arma::vec xnew = c + (d-c)/(b-a) * ( nw.col(0) - a );

    return xnew;
}

arma::vec Gaussquad::return_weights()
{
    arma::vec w =  nw.col(1);
    return w;
}


arma::mat Gaussquad::nodes_weights(unsigned n)
{
    arma::colvec l_n = Gaussquad::legendre_pol(n);
    arma::colvec dl_n = Gaussquad::poly_derivative(l_n, 1);

    l_n = arma::reverse(l_n);

    arma::cx_vec xi_cvec = arma::roots(l_n);
    arma::colvec xi_vec = arma::real(xi_cvec);
    arma::colvec wi_vec = arma::zeros<arma::colvec>( xi_vec.n_elem );

    for ( unsigned i = 0; i < xi_vec.n_elem; i++) {
        double xi = arma::as_scalar( xi_vec(i) );
        double dl_xval = Gaussquad::poly_construction( xi , dl_n );

        wi_vec(i) = 2./( (1.-xi*xi)*dl_xval*dl_xval );
    }

    arma::mat nod_weights = arma::join_rows( xi_vec, wi_vec );

    return nod_weights;
}