#include "../include/tesseroid.h"

inline bool f_exists(const char* fname) {
  if (access(fname, F_OK) != -1) {
    return true;
  } else {
    return false;
  }
}

Tesseroid::Tesseroid() {
    // Constructor
}
Tesseroid::~Tesseroid() {
    // destructor
}

void Tesseroid::copy_header(const tess::header &hinfo)
{
    this->model_name = hinfo.model_name;
    this->model_type = hinfo.model_type;
    this->units = hinfo.units;
    this->ref_ell = hinfo.ref_ell;
    this->isg_format = hinfo.isg_format;
    this->lat_min = hinfo.lat_min;
    this->lat_max = hinfo.lat_max;
    this->lon_min = hinfo.lon_min;
    this->lon_max = hinfo.lon_max;
    this->nodata = hinfo.nodata;
    this->delta_lat = hinfo.delta_lat;
    this->delta_lon = hinfo.delta_lon;
    this->nrows = hinfo.nrows;
    this->ncols = hinfo.ncols;
}

bool Tesseroid::compare_header(const tess::header &hinfo, const string &function)
{
    bool isthesame = true;
    if ( this->model_name.compare( hinfo.model_name ) != 0 ) {
        cout << "bool Tesseroid::compare_header() called in  function " + function + " the \"model_name\" is different!" << endl;
        isthesame = false;
    }

    if ( this->model_type.compare( hinfo.model_type  ) != 0 ) {
        cout << "bool Tesseroid::compare_header() called in  function " + function + " the \"model_type\" is different!" << endl;
        isthesame = false;
    }

    if ( this->units.compare( hinfo.units ) != 0 ) {
        cout << "bool Tesseroid::compare_header() called in  function " + function + " the \"units\" is different!" << endl;
        isthesame = false;
    }

    if ( this->ref_ell.compare( hinfo.ref_ell ) != 0 ) {
        cout << "bool Tesseroid::compare_header() called in  function " + function + " the \"ref_ell\" is different!" << endl;
        isthesame = false;
    }

    if ( this->isg_format.compare( hinfo.isg_format ) != 0 ) {
        cout << "bool Tesseroid::compare_header() called in  function " + function + " the \"isg_format\" is different!" << endl;
        isthesame = false;
    }

    if ( abs( this->lat_min - hinfo.lat_min) > 1.0e-12 ) {
        cout << "bool Tesseroid::compare_header() called in  function " + function + " the \"lat_min\" is different!" << endl;
        isthesame = false;
    }

    if ( abs( this->lat_max - hinfo.lat_max) > 1.0e-12 ) {
        cout << "bool Tesseroid::compare_header() called in  function " + function + " the \"lat_max\" is different!" << endl;
        isthesame = false;
    }

    if ( abs( this->lon_min - hinfo.lon_min) > 1.0e-12 ) {
        cout << "bool Tesseroid::compare_header() called in  function " + function + " the \"lon_min\" is different!" << endl;
        isthesame = false;
    }

    if ( abs( this->lon_max - hinfo.lon_max) > 1.0e-12 ) {
        cout << "bool Tesseroid::compare_header() called in  function " + function + " the \"lon_max\" is different!" << endl;
        isthesame = false;
    }

    if ( abs( this->delta_lat - hinfo.delta_lat) > 1.0e-12 ) {
        cout << "bool Tesseroid::compare_header() called in  function " + function + " the \"delta_lat\" is different!" << endl;
        isthesame = false;
    }

    if ( abs( this->delta_lon - hinfo.delta_lon) > 1.0e-12 ) {
        cout << "bool Tesseroid::compare_header() called in  function " + function + " the \"delta_lon\" is different!" << endl;
        isthesame = false;
    }

    return isthesame;
}

unsigned int Tesseroid::conv2index(double xval) //
{
    if ( xval >= 0.0 ) {
        return static_cast<unsigned int>( round(xval) );
    } else {
        string error_msg = "Input number is negative! Can not obtain index as unsinged int.";
        throw logic_error( string( error_msg ));
    }
}

int Tesseroid::conv2index2(double xval)
{
    return static_cast<int>( floor(xval ) );
}

arma::mat Tesseroid::load_mat_from_ascii(const string &path,
                                         unsigned int format,
                                         unsigned int load_col,
                                         tess::header &hinfo,
                                         bool &success,
                                         bool rewrite_header,
                                         double nodata_value)
{
    arma::mat loaded_mat;

    ifstream input( path );
    if ( input.is_open() ) {
      arma::mat data;
      data.load(input, arma::raw_ascii);
      arma::colvec b_vec, l_vec, hu_vec, hl_vec;

      /* reading input file based on format type */
      if (format == 0) {
        b_vec = data.col(1);
        l_vec = data.col(2);
        hu_vec = data.col(3);
        hl_vec = data.col(4);
      } else if (format == 1) {
        b_vec = data.col(2);
        l_vec = data.col(1);
        hu_vec = data.col(3);
        hl_vec = data.col(4);
      } else if (format == 2) {
        b_vec = data.col(1);
        l_vec = data.col(2);
        hu_vec = data.col(3);
        hl_vec.copy_size(hu_vec);
        hl_vec.fill(0.);
      } else if (format == 3) {
        b_vec = data.col(0);
        l_vec = data.col(1);
        hu_vec = data.col(2);
        hl_vec = data.col(3);
      } else if (format == 4) {
        b_vec = data.col(1);
        l_vec = data.col(0);
        hu_vec = data.col(2);
        hl_vec = data.col(3);
      } else if (format == 5) {
        b_vec = data.col(0);
        l_vec = data.col(1);
        hu_vec = data.col(2);
        hl_vec.copy_size(hu_vec);
        hl_vec.fill(0.);
      } else if (format == 6) {
        b_vec = data.col(1);
        l_vec = data.col(0);
        hu_vec = data.col(2);
        hl_vec.copy_size(hu_vec);
        hl_vec.fill(0.);
      }

      data.reset();
      data.set_size(b_vec.n_rows, 4);

      data.col(0) = b_vec;
      data.col(1) = l_vec;
      data.col(2) = hu_vec;
      data.col(3) = hl_vec;

      b_vec.reset();
      l_vec.reset();
      hu_vec.reset();
      hl_vec.reset();

      double bmax, bmin, lmin, lmax, bstep, lstep;

      /* extracting basic information */
      map<double, double> latitude, longitude;

      for (unsigned int i = 0; i < data.n_rows; i++) {
        latitude[ arma::as_scalar(data(i, 0))] = 1.;
        longitude[arma::as_scalar(data(i, 1))] = 1.;
      }

      map<double, double>::iterator it, iit;
      iit = latitude.begin();
      iit++;

      double latavg = 0.0;
      for (it = latitude.begin(); it != latitude.end(); it++) {
        latavg += abs(iit->first - it->first);

        iit++;
        if (iit == latitude.end())
          break;
      }

      bstep = latavg / ( static_cast<double>(latitude.size() - 1));

      iit = longitude.begin();
      iit++;
      double lonavg = 0.0;
      for (it = longitude.begin(); it != longitude.end(); it++) {
        lonavg += abs(iit->first - it->first);

        iit++;
        if (iit == longitude.end())
          break;
      }
      lstep = lonavg / ( static_cast<double>(longitude.size() - 1));

      bmax = data.col(0).max();
      bmin = data.col(0).min();
      lmax = data.col(1).max();
      lmin = data.col(1).min();

      unsigned nr = static_cast<unsigned>(latitude.size() );
      unsigned nc = static_cast<unsigned>(longitude.size());

      // Storing info to header struct
      hinfo.lat_min = bmin;
      hinfo.lat_max = bmax;
      hinfo.lon_min = lmin;
      hinfo.lon_max = lmax;
      hinfo.nodata  = nodata_value;
      hinfo.delta_lat = bstep;
      hinfo.delta_lon = lstep;
      hinfo.nrows = nr;
      hinfo.ncols = nc;

      loaded_mat.fill(nodata_value);
      unsigned int m, n;

      ///# Could be parallelized easily !!
      for (unsigned int i = 0; i < data.n_rows; i++) {
        n = Tesseroid::conv2index((data(i, 0) - lat_min) / delta_lon);
        m = Tesseroid::conv2index((data(i, 1) - lon_min) / delta_lat);

        if ( load_col == 0 ) {
            loaded_mat(m, n) = data(i, 2);
        } else {
            loaded_mat(m, n) = data(i, 3);
        }

      }

      if ( rewrite_header ) {
          this->copy_header( hinfo );
      }

      success = true;
      return loaded_mat;
    } else {
      cerr << "Tesseroid::load_isgem_model(): file " << path << " does not exist. Can't continue! " << endl;
      success = false;
      return loaded_mat;
    }
}

arma::mat Tesseroid::load_isgem_model(const string &path,
                                      tess::header &hinfo,
                                      bool &success,
                                      bool rewrite_header)
{
    ifstream fmodel( path );
    arma::mat layer;
    if ( fmodel.is_open() ) {
        vector<string> v; // vector with words in loaded lines
        string line, word;
        string data_reading; data_reading.assign("header");

        int rowsn;
        while ( getline (fmodel, line ) ) {
            if ( data_reading.compare("header") == 0 ) {
                istringstream is ( line );

                while ( is >> word ) {
                    v.push_back( word );
                } // spliting <string> line to separate words

                if ( line.find("model name") != string::npos  || // line.find("model name") != string::npos ||  line.find("model_name") != string::npos
                     line.find("model_name") != string::npos  ) {

                    hinfo.model_name.clear();
                    for ( unsigned int i = 3; i < v.size(); i++ ) {
                        hinfo.model_name.append( v[i] );
                        hinfo.model_name.append( " " );// + (" ").c_str();
                    }
                } else if ( line.find("model type") != string::npos ||
                            line.find("model_type") != string::npos ) {
                    hinfo.model_type =(v[3]).c_str();
                } else if ( line.find("units") != string::npos  ) {
                    hinfo.units = v[2].c_str();
                } else if ( line.find("reference") != string::npos ) {
                    hinfo.ref_ell = (v[2]).c_str();
                } else if ( line.find( "lat min" ) != string::npos ||
                            line.find( "lat_min" ) != string::npos) {
                    hinfo.lat_min = stod((v[ v.size() - 1 ]).c_str());
                } else if ( line.find( "lat max" ) != string::npos  ||
                            line.find( "lat_max" ) != string::npos) { 
                    hinfo.lat_max= stod((v[ v.size() - 1 ]).c_str());
                } else if ( line.find( "lon min" ) != string::npos  ||
                            line.find( "lon_min" ) != string::npos ) {
                    hinfo.lon_min = stod((v[v.size() - 1]).c_str());
                } else if ( line.find( "lon max" ) != string::npos ||
                            line.find( "lon_max" ) != string::npos) {
                    hinfo.lon_max = stod((v[v.size() - 1]).c_str());
                } else if ( line.find( "delta lon" ) != string::npos ||
                            line.find( "delta_lon" ) != string::npos ) {
                    hinfo.delta_lon = stod((v[v.size() - 1]).c_str());
                } else if ( line.find( "delta lat" ) != string::npos ||
                            line.find( "delta_lat" ) != string::npos ) {
                    hinfo.delta_lat = stod((v[v.size() - 1]).c_str());
                } else if ( line.find("nrows") != string::npos ) {
                    rowsn =  stoi( v[v.size() - 1] );
                    hinfo.nrows = static_cast<unsigned>(rowsn);
                } else if ( line.find( "ncols" ) != string::npos ) {
                    hinfo.ncols = static_cast<unsigned>( stoi( v[v.size() - 1] ));
                } else if ( line.find( "nodata" ) != string::npos ) {
                    hinfo.nodata = stod(v[v.size() - 1].c_str());
                } else if ( line.find( "ISG format" ) != string::npos ) {
                    hinfo.isg_format = v[v.size() - 1].c_str();
                } else if ( line.find("end_of_head") != string::npos) {
                    data_reading = "coefficients";

                    try  {
                        layer.load( fmodel, arma::raw_ascii); // is there a way do it without flipping?
                        layer =  arma::flipud( layer );

                        if ( hinfo.nrows != layer.n_rows ) {
                            cout << "Tesseroid::load_isgem_model() , the header info \"nrows\" is different data data size!" << endl
                                 << "Header \"nrows\" : " << hinfo.nrows << " , data \"nrows\" : " << layer.n_rows << endl;
                             hinfo.nrows = layer.n_rows;
                        }

                        if ( hinfo.ncols != layer.n_cols ) {
                            cout << "Tesseroid::load_isgem_model() , the header info \"nrows\" is different data data size!" << endl
                                 << "Header \"nrows\" : " << hinfo.ncols << " , data \"nrows\" : " << layer.n_cols << endl;
                            hinfo.ncols = layer.n_cols;
                        }
                    } catch (...) {
                         cerr << "Unable to load data while calling function "
                              << "\"bool Isgemgeoid::load_isgem_model ( const string& path )\"."
                              << "Check the file for errors!" << endl;
                         success = false;
                         layer.reset();
                         return layer;
                    }
                }

                v.clear();
                word.clear();
                line.clear();
            }
        }

        if (rewrite_header) {
            this->copy_header( hinfo );
        }

        fmodel.close();

        success = true;
        return  layer;
    } else {
        cerr << "File \""<< path << "\" can not be opened!" << endl;
        fmodel.close();
        success = false;

        layer.reset();
        return layer;
    }
}

bool Tesseroid::check_layers_size()
{
    if ( this->upper_lay.n_rows != this->lower_lay.n_rows || this->upper_lay.n_cols != this->lower_lay.n_cols ){
        cout << "Size of the matrix data (upper vs lower) layer is different. Upperlay dim ("
             << this->upper_lay.n_rows << "," << this->upper_lay.n_cols << ") , Lowerlay dim ("
             << this->lower_lay.n_rows << "," << this->lower_lay.n_cols << ")." << endl;

        return false;
    } else {
        return true;
    }
}

bool Tesseroid::load_model_from_ascii(  const char* path,
                                        unsigned int format,
                                        const char* f_model_name ,
                                        const char* f_model_type ,
                                        const char* f_units ,
                                        const char* f_ref_ell ,
                                        const char* f_isg_format ,
                                        double nodata_value ) {
    // Header info:

    model_name = f_model_name; ///< Name of the DMR model
    model_type = f_model_type; ///< Type of the model, digital terrain/relief/elevation model
    units = f_units;      ///< Units
    ref_ell = f_ref_ell;    ///< reference ellipsoid
    isg_format = f_isg_format; ///< Version of the isgem format.
    nodata = nodata_value;          ///< NoData value
    /* data in file are in following format:
      input in raw ascii format
  */

  if (f_exists(path)) {
    arma::mat data;
    data.load(path, arma::raw_ascii);

    arma::colvec b_vec, l_vec, hu_vec, hl_vec;

    /* reading input file based on format type */
    if (format == 0) {
      b_vec = data.col(1);
      l_vec = data.col(2);
      hu_vec = data.col(3);
      hl_vec = data.col(4);
    } else if (format == 1) {
      b_vec = data.col(2);
      l_vec = data.col(1);
      hu_vec = data.col(3);
      hl_vec = data.col(4);
    } else if (format == 2) {
      b_vec = data.col(1);
      l_vec = data.col(2);
      hu_vec = data.col(3);
      hl_vec.copy_size(hu_vec);
      hl_vec.fill(0.0);
    } else if (format == 3) {
      b_vec = data.col(0);
      l_vec = data.col(1);
      hu_vec = data.col(2);
      hl_vec = data.col(3);
    } else if (format == 4) {
      b_vec = data.col(1);
      l_vec = data.col(0);
      hu_vec = data.col(2);
      hl_vec = data.col(3);
    } else if (format == 5) {
      b_vec = data.col(0);
      l_vec = data.col(1);
      hu_vec = data.col(2);
      hl_vec.copy_size(hu_vec);
      hl_vec.fill(0.);
    } else if (format == 6) {
      b_vec = data.col(1);
      l_vec = data.col(0);
      hu_vec = data.col(2);
      hl_vec.copy_size(hu_vec);
      hl_vec.fill(0.);
    }

    data.reset();
    data.set_size(b_vec.n_rows, 4);

    data.col(0) = b_vec;
    data.col(1) = l_vec;
    data.col(2) = hu_vec;
    data.col(3) = hl_vec;

    b_vec.reset();
    l_vec.reset();
    hu_vec.reset();

    /* extracting basic information */
    map<double, double> latitude, longitude;

    for (unsigned int i = 0; i < data.n_rows; i++) {
      latitude[arma::as_scalar(data(i, 0))] = 1.;
      longitude[arma::as_scalar(data(i, 1))] = 1.;
    }

    map<double, double>::iterator it, iit;
    iit = latitude.begin();
    iit++;

    double latavg = 0.0;
    for (it = latitude.begin(); it != latitude.end(); it++) {
      latavg += abs(iit->first - it->first);

      iit++;
      if (iit == latitude.end())
        break;
    }

    delta_lat = latavg / ( static_cast<double>(latitude.size() - 1));

    iit = longitude.begin();
    iit++;
    double lonavg = 0.0;
    for (it = longitude.begin(); it != longitude.end(); it++) {
      lonavg += abs(iit->first - it->first);

      iit++;
      if (iit == longitude.end())
        break;
    }

    delta_lon = lonavg / ( static_cast<double>(longitude.size() - 1));

    lat_max = data.col(0).max();
    lat_min = data.col(0).min();
    lon_max = data.col(1).max();
    lon_min = data.col(1).min();
    nodata = 9999.999;

    nrows = static_cast<unsigned>(latitude.size() );
    ncols = static_cast<unsigned>(longitude.size());

    if (upper_lay.is_empty()) {
      upper_lay.set_size(nrows, ncols);
    } else {
      upper_lay.reset();
      upper_lay.set_size(nrows, ncols);
    }

    if (lower_lay.is_empty()) {
      lower_lay.set_size(nrows, ncols);
    } else {
      lower_lay.reset();
      lower_lay.set_size(nrows, ncols);
    }

    upper_lay.fill(nodata);
    lower_lay.fill(nodata);

    unsigned int m, n;
    ///# Could be parallelized easily !!
    for (unsigned int i = 0; i < data.n_rows; i++) {
      n = Tesseroid::conv2index((data(i, 0) - lat_min) / delta_lon);
      m = Tesseroid::conv2index((data(i, 1) - lon_min) / delta_lat);

      upper_lay(n, m) = data(i, 2);
      lower_lay(n, m) = data(i, 3);
    }

    return true;
  } else {
    cerr << "File " << path << " does not exist. Can't continue! " << endl;
    return false;
  }
}

bool Tesseroid::export2csv( const string& path, bool ptid ) {
    ofstream f_out( path );

    if ( f_out.is_open() ) {

        if (ptid) {
            f_out << "ptid, phi, lam, zeta" << endl;
        } else {
            f_out << "phi, lam, zeta" << endl;
        }

        unsigned int pid = 1;
        double phi = lat_min, lam;
        for (unsigned int i = 0; i < upper_lay.n_rows; i++ ) {
            //double lam = lon_min;
            for ( unsigned int j = 0; j < upper_lay.n_cols; j++ ) {
                lam = lon_min + static_cast<double>(j)*delta_lon;
                if ( arma::as_scalar( upper_lay(i,j) ) == nodata ) {
                    continue;
                } else {
                    if ( ptid ) {
                        f_out << setw(12) << setprecision(9) <<  pid++ << " " << phi << " " << lam << " " << arma::as_scalar(upper_lay(i,j)) << endl;
                    } else {
                        f_out << setw(12) << setprecision(9) << phi << "," << lam << "," << arma::as_scalar(upper_lay(i,j)) << endl;
                    }
                }
            }
            phi += delta_lat;
        }
        f_out.close();
        return true;
    } else {
        cerr << "File \"" << path << "\" can not be created!" << endl;
        f_out.close();
        return false;
    }
}

bool Tesseroid::load_isgem_model ( const string& path ){
    tess::header hinfo;
    bool succees, rheader = false;

    arma::mat ulay = Tesseroid::load_isgem_model( path, hinfo, succees, rheader);

    if ( succees ) {
        this->upper_lay = ulay;
        this->lower_lay = arma::zeros<arma::mat>( arma::size( this->upper_lay ) );

        Tesseroid::copy_header(hinfo);
        return true;
    } else {
        cout << "bool Tesseroid::load_isgem_model() not successful!\n";
        return false;
    }
}

void Tesseroid::set_print_coeff ( bool in_print_coeff ) {
    print_coeff = in_print_coeff;
}

bool Tesseroid::set_lower_layer ( const string& path ) {
    vector<double> header_info;

//    lower_lay = Tesseroid::load_mat_from_ascii(  );
    Tesseroid lowlay; lowlay.load_isgem_model( path );

    return true;
}

void Tesseroid::print_model(/* const char* header_only */) {
  cout << setw(15) << setprecision(9);
  cout << "Model name :                    " << model_name << endl;
  cout << "Model type:                     " << model_type << endl;
  cout << "Units :                         " << units << endl;
  cout << "Reference ellipsoid :           " << ref_ell
       << endl;  ///< reference ellipsoid
  cout << "Minimum value of the latitude:  " << lat_min
       << endl;  ///< Minimum value of the latitude
  cout << "Maximum value of the latitude:  " << lat_max
       << endl;  ///< Maximum value of the latitude
  cout << "Minimum value of the longitude: " << lon_min
       << endl;  ///< Minimum value of the longitude
  cout << "Maximum value of the longitude: " << lon_max
       << endl;  ///< Maximum value of the longitude
  cout << "No data value :                 " << nodata
       << endl;  ///< NoData value
  cout << "Latitude step:                  " << delta_lat
       << endl;  ///< Step in the direction
  cout << "Longitude step:                 " << delta_lon << endl;
  cout << "Number of columns:              " << nrows
       << endl;  ///< Number of columns
  cout << "Number of rows:                 " << ncols
       << endl;  ///< Number of rows
}

bool Tesseroid::load_layer(const string &path,
                           const string &data_format,
                           const int &layer, // 0 upper, 1 lower
                           const int &col_order,
                           bool overwrite)
{
    ifstream flin ( path );

    tess::header hinfo;
    bool success, rwrite = false;
    arma::mat layer_dat;

    if ( flin.is_open() ) {
        flin.close();
        if ( data_format.compare("isgem") == 0  ) {
            layer_dat = Tesseroid::load_isgem_model(path,
                                                    hinfo,
                                                    success,
                                                    rwrite);
        } else if ( data_format.compare( "csv" ) == 0 ) {
            cout << "Tesseroid::load_layer() , foramt csv not finished!\n";
            return false;
        } else if ( data_format.compare("ascii") == 0 ) {
            layer_dat = Tesseroid::load_mat_from_ascii( path,
                                                        col_order,
                                                        0,
                                                        hinfo,
                                                        success,
                                                        rwrite, hinfo.nodata );

        } else {
            cerr << "Tesseroid::load_layer() unknown input format!" << endl;
            return false;
        }


        if ( this->lower_set || this->upper_set ) {
            success = this->compare_header( hinfo, "bool Tesseroid::load_layer()" );
        }

        if ( overwrite ) {
            this->copy_header( hinfo );
        }

        if (layer == 1 ) {
            this->lower_lay = layer_dat;
            this->lower_set = true;

            return true;
        } else { // upper layer is default
            this->upper_lay = layer_dat;
            this->upper_set = true;

            return true;
        }
    } else {
        cerr << "Tesseroid::load_layer() file does not exist!" << endl;
        flin.is_open();
        return false;
    }
}


arma::mat Tesseroid::grav_on_grid ( bool check_radius,
                                    bool rrun_is_up,
                                    double radius,
                                    double kappa,
                                    double rho,
                                    struct geo_f::ellipsoid<double> ell,
                                    double (Tesseroid::*gravf)(double,double,double,double,double,double,
                                                               double,double,double,double,double)) {
    /* Computes up to 10 components of the gravity field on grid data (spherical
     tess,
     * but integration is computed after transforming the integral kernel to
     cartesian
     * system).
     *
     * goes point by point on grid
     * functions vialable in the <initializer_list> func_list are:
     * potential_u - computes gravity potential in the units m^2 s^{-2}
     * gravity_r   - computes the first derivative of the gravity potential
     *               with respect to 'r' coordinate
     * gravity_phi - \frac{\partial U }{\partial \varphi}
     * gravity_lambda - \frac{\partial U }{\partial \lambda}
     * marussi_rr  - computes the second derivative of the gravity potential
                     with respect to the r coordinate
     * marussi_rb   -\frac{\partial U^2 }{\partial^2 r \varphi}
     * marussi_rl   -\frac{\partial U^2 }{\partial^2 r \lambda}
     * marussi_bb   ...
     * marussi_bl
     * marussi_ll
     */

    arma::mat results = arma::zeros<arma::mat>(this->nrows, this->ncols);
    Progressbar p_bar ( static_cast<int>( upper_lay.n_rows ));
    p_bar.set_name( "Tesseroid::grav_on_grid" );

    //#pragma omp parallel shared(results, upper_lay, lower_lay, check_radius, radius, kappa, rho, ell, p_bar)
    {
        for (unsigned int n = 0; n < upper_lay.n_rows; n++) {
          #pragma omp critical
          {  p_bar.update();  }


          double brun = lat_min + static_cast<double>(n) * delta_lat;

          #pragma omp parallel for shared( n, brun, results, upper_lay , lower_lay, check_radius)
          for (unsigned int m = 0; m < upper_lay.n_cols; m++) {
            //Timer T("I have just calculated one point!" );
            double phi_run = brun * DEG2RAD; // conversion to radians
            double rrun;
            double lrun = lon_min + static_cast<double>(m) * delta_lon;

            if ( rrun_is_up ) {
                rrun = arma::as_scalar(upper_lay(n, m));
            } else {
                rrun = arma::as_scalar(lower_lay(n, m));
            }

            double n_rad =
                ell.a_axis / sqrt(1. - ell.eccentricity * sin(phi_run) * sin(phi_run));
            double m_rad = (ell.a_axis * (1. - ell.eccentricity)) /
                           pow(1. - ell.eccentricity * pow(sin(phi_run), 2.0), 3. / 2.);

            double r_mean = sqrt( n_rad * m_rad );
            // Check radius code - exclude distant points from the computation
            if ( check_radius ) {
              double delta_b = RAD2DEG * radius / (m_rad) ;  // return in deg format
              double delta_l = RAD2DEG * radius / (n_rad * cos(phi_run));  // return in deg format

              int dcol, drow = static_cast<int>(floor(delta_b / delta_lat));
              if ( abs( abs(phi_run) - M_PI_2) <= .5*DEG2RAD ) {
                dcol = drow;
              } else {
                dcol = static_cast<int>(round(delta_l / delta_lon));
              }

              int leftb, rightb, upb, lowb, coln, rown;
              rown = static_cast<int>( floor((brun - lat_min) / delta_lat));
              coln = static_cast<int>( floor((lrun - lon_min) / delta_lon));

              leftb = ((coln - dcol) >= 0) ? (coln - dcol) : 0;
              leftb = (leftb < 0) ? 0 : leftb;
              rightb = ((coln + dcol) < static_cast<int>(ncols) ) ? (coln + dcol) : static_cast<int>(ncols - 1);

              lowb = ((rown - drow) >= 0) ? (rown - drow) : 0;
              lowb = (lowb < 0) ? 0 : lowb;
              upb = ((rown + drow) < static_cast<int>(nrows) ) ? (rown + drow) : static_cast<int>(nrows - 1);

              for ( int i = lowb; i <= upb ; i++) {
                for ( int j = leftb; j <= rightb ; j++) {
                    double b1, b2, l1, l2, rm_low, rm_up;

                    rm_low = arma::as_scalar( this->lower_lay(i,j) );
                    rm_up =  arma::as_scalar( this->upper_lay(i,j) );

                    if (isnan(rm_low) || isnan(rm_up) || isinf( rm_low ) || isinf(rm_up) )
                      continue;

                    b1 = lat_min + static_cast<double>(i) * delta_lat - delta_lat/2.;
                    b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

                    l1 = lon_min + static_cast<double>(j) * delta_lon - delta_lon/2.;
                    l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;


                    double b0 = .5 * (b1 + b2) * DEG2RAD; // convert to radians
                    double l0 = .5 * (l1 + l2) * DEG2RAD; // convert to radians
                    double r0 = .5 * (rm_low + rm_up);
                    double sph_len = tess_t::spherical_lenght( rrun + r_mean , phi_run, lrun*DEG2RAD, // little mess with DEG to RAD conversion
                                                               r0 + r_mean , b0 , l0 );               // phi_run already in radians, see above

                    if (  sqrt(2.)*sph_len > radius ||
                          isnan(rm_low) || isnan(rm_up)  ) {
                        continue;
                    }

                    results(n, m) +=  (this->*gravf)(rrun   + r_mean, brun, lrun,
                                                     rm_low + r_mean, b1, l1,
                                                     rm_up  + r_mean, b2, l2, kappa, rho);

                }
              }
            } else {
              for (unsigned int i = 0; i < upper_lay.n_rows - 1; i++) {
                for (unsigned int j = 0; j < lower_lay.n_cols - 1; j++) {

                  double b1, b2, l1, l2, rm_low, rm_up;

                  rm_low = arma::as_scalar( lower_lay(i,j) );
                  rm_up =  arma::as_scalar( upper_lay(i,j) );

                  if (isnan(rm_low) || isnan(rm_up) || isinf( rm_low ) || isinf(rm_up) )
                    continue;

                  b1 = lat_min + static_cast<double>(i) * delta_lat - delta_lat/2.;
                  b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

                  l1 = lon_min + static_cast<double>(j) * delta_lon - delta_lon/2.;
                  l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;


                  results(n, m) +=  (this->*gravf)(rrun   + r_mean, brun, lrun,
                                                   rm_low + r_mean, b1, l1,
                                                   rm_up  + r_mean, b2, l2, kappa, rho);
                }
              }
            }
          }
        //#pragma omp barrier
        }

    } // end #pragma omp parallel shared(grid, u, pc_nm_taylor, ps_nm_taylor, min_n, max_n )
    return results;
}

arma::mat Tesseroid::grav_on_grid(bool check_radius,
                                  bool rrun_is_up,
                                  double radius,
                                  double kappa,
                                  const Isgemgeoid &rho,
                                  geo_f::ellipsoid<double> ell,
                                  double (Tesseroid::*gravf)(double, double, double, double, double, double, double, double, double, double, double))
{
    arma::mat results = arma::zeros<arma::mat>( this->nrows, this->ncols);
    //============================================
    unsigned n,m;
    vector<double> headrho = rho.get_header<double>(n,m);

    // check if the Isgemgeoid rhe HEADER is identical with this->HEADER
    if ( abs(this->lat_min - headrho[0] ) > 1.0e-12 ) {
        cerr << "Tesseroid::grav_on_grid() inconsistent size of input rho (density) distribution and the size of the DEM model loaded in Tesseroid class!" << endl;
        cerr << "lat_min check failed!" << endl;
        return results;
    }

    if ( abs(this->lat_max - headrho[1] ) > 1.0e-12 ) {
        cerr << "Tesseroid::grav_on_grid() inconsistent size of input rho (density) distribution and the size of the DEM model loaded in Tesseroid class!" << endl;
        cerr << "lat_max check failed!" << endl;
        return results;
    }

    if ( abs(this->lon_min - headrho[2] ) > 1.0e-12 ) {
        cerr << "Tesseroid::grav_on_grid() inconsistent size of input rho (density) distribution and the size of the DEM model loaded in Tesseroid class!" << endl;
        cerr << "lon_min check failed!" << endl;
        return results;
    }

    if ( abs(this->lon_max - headrho[3] ) > 1.0e-12 ) {
        cerr << "Tesseroid::grav_on_grid() inconsistent size of input rho (density) distribution and the size of the DEM model loaded in Tesseroid class!" << endl;
        cerr << "lon_max check failed!" << endl;
        return results;
    }

    if ( abs(this->delta_lat - headrho[4] ) > 1.0e-12 ) {
        cerr << "Tesseroid::grav_on_grid() inconsistent size of input rho (density) distribution and the size of the DEM model loaded in Tesseroid class!" << endl;
        cerr << "delta_lat check failed!" << endl;
        return results;
    }

    if ( abs(this->delta_lon - headrho[5] ) > 1.0e-12 ) { // 1.0e-12 approx 0.000 001 sec (DEG) to DMS, 2.778e-11 degrees
        cerr << "Tesseroid::grav_on_grid() inconsistent size of input rho (density) distribution and the size of the DEM model loaded in Tesseroid class!" << endl;
        cerr << "delta_lon check failed!" << endl;
        return results;
    }
    // **END** check if the Isgemgeoid rhe HEADER is identical with this->HEADER
    Progressbar p_bar ( static_cast<int>( upper_lay.n_rows ));
    p_bar.set_name( "Tesseroid::grav_on_grid" );
    for (unsigned int n = 0; n < upper_lay.n_rows; n++) {
      #pragma omp critical
      {  p_bar.update();  } // Just to see the progress of the function


      double brun = lat_min + static_cast<double>(n) * delta_lat;

      #pragma omp parallel for shared( n, brun, results, upper_lay , lower_lay, check_radius, rho)
      for (unsigned int m = 0; m < upper_lay.n_cols; m++) {
        //Timer T("I have just calculated one point!" );
        double phi_run = brun * DEG2RAD; // conversion to radians
        double rrun;
        double lrun = lon_min + static_cast<double>(m) * delta_lon;

        if ( rrun_is_up ) {
            rrun = arma::as_scalar(upper_lay(n, m));
        } else {
            rrun = arma::as_scalar(lower_lay(n, m));
        }

        double n_rad =
            ell.a_axis / sqrt(1. - ell.eccentricity * sin(phi_run) * sin(phi_run));
        double m_rad = (ell.a_axis * (1. - ell.eccentricity)) /
                       pow(1. - ell.eccentricity * pow(sin(phi_run), 2.0), 3. / 2.);

        double r_mean = sqrt( n_rad * m_rad );
        // Check radius code - exclude distant points from the computation
        if ( check_radius ) {
          double delta_b = RAD2DEG * radius / (m_rad) ;  // return in deg format
          double delta_l = RAD2DEG * radius / (n_rad * cos(phi_run));  // return in deg format

          int dcol, drow = static_cast<int>(floor(delta_b / delta_lat));
          if ( abs( abs(phi_run) - M_PI_2) <= .5*DEG2RAD ) {
            dcol = drow;
          } else {
            dcol = static_cast<int>(round(delta_l / delta_lon));
          }

          int leftb, rightb, upb, lowb, coln, rown;
          rown = static_cast<int>(floor((brun - lat_min) / delta_lat ));
          coln = static_cast<int>(floor((lrun - lon_min) / delta_lon ));

          leftb = ((coln - dcol) >= 0) ? (coln - dcol) : 0;
          leftb = (leftb < 0) ? 0 : leftb;
          rightb = ((coln + dcol) < static_cast<int>(ncols) ) ? (coln + dcol) : static_cast<int>(ncols - 1);

          lowb = ((rown - drow) >= 0) ? (rown - drow) : 0;
          lowb = (lowb < 0) ? 0 : lowb;
          upb = ((rown + drow) < static_cast<int>(nrows) ) ? (rown + drow) : static_cast<int>(nrows - 1);

          for ( int i = lowb; i <= upb ; i++) {
            for ( int j = leftb; j <= rightb ; j++) {
                double b1, b2, l1, l2, rm_low, rm_up;

//                if ( i == static_cast<int>(n) && j == static_cast<int>(m) ) {
//                    continue;
//                }

                rm_low = arma::as_scalar( lower_lay(i,j) );
                rm_up =  arma::as_scalar( upper_lay(i,j) );

                if (isnan(rm_low) || isnan(rm_up) || isinf( rm_low ) || isinf(rm_up) )
                  continue;

                b1 = lat_min + static_cast<double>(i) * delta_lat - delta_lat/2.; // WRONG !!!!!
                b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

                l1 = lon_min + static_cast<double>(j) * delta_lon - delta_lon/2.;
                l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;


                double b0 = .5 * (b1 + b2) * DEG2RAD; // convert to radians
                double l0 = .5 * (l1 + l2) * DEG2RAD; // convert to radians
                double r0 = .5 * (rm_low + rm_up);
                double sph_len = tess_t::spherical_lenght( rrun + r_mean , phi_run, lrun*DEG2RAD, // little mess with DEG to RAD conversion
                                                           r0 + r_mean , b0 , l0 );               // phi_run already in radians, see above

                if (  sqrt(2.)*sph_len > radius ||
                      isnan(rm_low) || isnan(rm_up)  ) {
                    continue;
                }

                // obtaining rho from Isgem rho model
                double density = rho.get_value(i,j);
                double retval = (this->*gravf)(rrun   + r_mean, brun, lrun,
                                               rm_low + r_mean, b1, l1,
                                               rm_up  + r_mean, b2, l2, kappa, density);

                results(n, m) += retval;
            }
          }
        } else {
          for (unsigned int i = 0; i < upper_lay.n_rows - 1; i++) {
            for (unsigned int j = 0; j < lower_lay.n_cols - 1; j++) {

              double b1, b2, l1, l2, rm_low, rm_up;

//              if ( i == n && j == m ) {
//                  continue;
//              }

              rm_low = arma::as_scalar( lower_lay(i,j) );
              rm_up =  arma::as_scalar( upper_lay(i,j) );

              if (isnan(rm_low) || isnan(rm_up) || isinf( rm_low ) || isinf(rm_up) )
                continue;

              b1 = lat_min + static_cast<double>(i) * delta_lat - delta_lat/2.;
              b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

              l1 = lon_min + static_cast<double>(j) * delta_lon - delta_lon/2.;
              l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;

              // obtaining rho from Isgem rho model
              double density = rho.get_value(i,j);

              results(n, m) +=  (this->*gravf)(rrun   + r_mean, brun, lrun,
                                               rm_low + r_mean, b1, l1,
                                               rm_up  + r_mean, b2, l2, kappa, density);
            }
          }
        }
      }
    }
    return results;
}


void Tesseroid::grav_crd_list ( arma::mat &crdmat, // modified version of grav_on_grid function
                                bool check_radius,
                                double radius,
                                double kappa,
                                double rho,
                                struct geo_f::ellipsoid<double> ell,
                                double (Tesseroid::*gravf)(double,double,double,double,double,double,
                                                           double,double,double,double,double)) {
    /* Computes up to 10 components of the gravity field on grid data (spherical
     tess,
     * but integration is computed after transforming the integral kernel to
     cartesian
     * system).
     *
     * goes point by point on grid
     * functions vialable in the <initializer_list> func_list are:
     * potential_u - computes gravity potential in the units m^2 s^{-2}
     * gravity_r   - computes the first derivative of the gravity potential
     *               with respect to 'r' coordinate
     * gravity_phi - \frac{\partial U }{\partial \varphi}
     * gravity_lambda - \frac{\partial U }{\partial \lambda}
     * marussi_rr  - computes the second derivative of the gravity potential
                     with respect to the r coordinate
     * marussi_rb   -\frac{\partial U^2 }{\partial^2 r \varphi}
     * marussi_rl   -\frac{\partial U^2 }{\partial^2 r \lambda}
     * marussi_bb   ...
     * marussi_bl
     * marussi_ll
     */


    ///< TODO #001# pole areas of the earth are not very good
    ///< TODO #002# only possible input now is b l h

    if ( crdmat.is_empty() ) {
        cerr << "Can not continue with the computation! " << endl;
    }

    arma::colvec results = arma::zeros<arma::colvec>(crdmat.n_rows);


    Progressbar p_bar( static_cast< int >( crdmat.n_rows ) );
    p_bar.set_name("Tesseroid::grav_crd_list");

    #pragma omp parallel for shared( results, upper_lay , lower_lay, check_radius)
    for (unsigned long long n = 0; n < crdmat.n_rows; n++) {
        double brun = arma::as_scalar(crdmat(n,0));
        double phi_run = brun * DEG2RAD; // conversion to radians
        double lrun = arma::as_scalar(crdmat(n,1));
        double rrun = arma::as_scalar(crdmat(n,2));

        double n_rad =
            ell.a_axis / sqrt(1. - ell.eccentricity * sin(phi_run) * sin(phi_run));
        double m_rad = (ell.a_axis * (1. - ell.eccentricity)) /
                       pow(1. - ell.eccentricity * pow(sin(phi_run), 2.0), 3. / 2.);

        double r_mean = sqrt( n_rad * m_rad );
        // Check radius code - exclude distant points from the computation
        if ( check_radius ) {
          double delta_b = RAD2DEG * radius / (m_rad) ;  // return in deg format
          double delta_l = RAD2DEG * radius / (n_rad * cos(phi_run));  // return in deg format

          int dcol, drow = static_cast<int>(round(delta_b / delta_lat));
          if ( abs( abs(phi_run) - M_PI_2) <= .5*DEG2RAD ) {
            dcol = drow;
          } else {
            dcol = static_cast<int>(round(delta_l / delta_lon));
          }

          int leftb, rightb, upb, lowb, coln, rown;
          rown = static_cast<int>( round((brun - lat_min) / delta_lat));
          coln = static_cast<int>( round((lrun - lon_min) / delta_lon));

          leftb = ((coln - dcol) >= 0) ? (coln - dcol) : 0;
          leftb = (leftb < 0) ? 0 : leftb;
          rightb = ((coln + dcol) < static_cast<int>(ncols) ) ? (coln + dcol) : static_cast<int>(ncols - 1);

          lowb = ((rown - drow) >= 0) ? (rown - drow) : 0;
          lowb = (lowb < 0) ? 0 : lowb;
          upb = ((rown + drow) < static_cast<int>(nrows) ) ? (rown + drow) : static_cast<int>(nrows - 1);

          for ( int i = lowb; i <= upb ; i++) {
            for ( int j = leftb; j <= rightb ; j++) {
                double b1, b2, l1, l2, rm_low, rm_up;

                rm_low = arma::as_scalar( lower_lay(i,j) );
                rm_up =  arma::as_scalar( upper_lay(i,j) );

                if (isnan(rm_low) || isnan(rm_up) || isinf( rm_low ) || isinf(rm_up) )
                  continue;

                b1 = lat_min + static_cast<double>(i) * delta_lat - delta_lat/2.;
                b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

                l1 = lon_min + static_cast<double>(j) * delta_lon - delta_lon/2.;
                l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;


                double b0 = .5 * (b1 + b2) * DEG2RAD; // convert to radians
                double l0 = .5 * (l1 + l2) * DEG2RAD; // convert to radians
                double r0 = .5 * (rm_low + rm_up);
                double sph_len = tess_t::spherical_lenght( rrun + r_mean , phi_run, lrun*DEG2RAD, // little mess with DEG to RAD conversion
                                                           r0 + r_mean , b0 , l0 );               // phi_run already in radians, see above

                if (  1.05*sph_len > radius  ) {
                    continue;
                }

                double add2res =  (this->*gravf)(rrun   + r_mean, brun, lrun,
                                                 rm_low + r_mean, b1, l1,
                                                 rm_up  + r_mean, b2, l2, kappa, rho);

                if (  isnan(add2res) || isinf(add2res) ) {
                    continue;
                }
                results(n) += add2res; // add to result
            }
          }
        } else {
          for (unsigned int i = 0; i < upper_lay.n_rows - 1; i++) {
            for (unsigned int j = 0; j < lower_lay.n_cols - 1; j++) {
              double b1, b2, l1, l2, rm_low, rm_up;

              rm_low = arma::as_scalar( lower_lay(i,j) );
              rm_up =  arma::as_scalar( upper_lay(i,j) );

              if (isnan(rm_low) || isnan(rm_up) || isinf( rm_low ) || isinf(rm_up) )
                continue;

              b1 = lat_min + static_cast<double>(i) * delta_lat - delta_lat/2.;
              b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

              l1 = lon_min + static_cast<double>(j) * delta_lon - delta_lon/2.;
              l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;


              double add2res =  (this->*gravf)(rrun   + r_mean, brun, lrun,
                                               rm_low + r_mean, b1, l1,
                                               rm_up  + r_mean, b2, l2, kappa, rho);

              if (  isnan(add2res) || isinf(add2res) ) {
                  continue;
              }
              results(n) += add2res; // add to result
            }
          }
        }

    #pragma omp critical
    {  p_bar.update();  } // see the progress

    }

    crdmat = arma::join_rows(crdmat, results );
}

void Tesseroid::grav_crd_list(arma::mat &crdmat,
                              bool check_radius,
                              double radius,
                              double kappa,
                              const Isgemgeoid &rho,
                              geo_f::ellipsoid<double> ell,
                              double (Tesseroid::*gravf)(double,
                                                         double,
                                                         double,
                                                         double,
                                                         double,
                                                         double,
                                                         double,
                                                         double,
                                                         double,
                                                         double,
                                                         double)
                              ) {
    /* Computes up to 10 components of the gravity field on grid data (spherical
     tess,
     * but integration is computed after transforming the integral kernel to
     cartesian
     * system).
     *
     * goes point by point on grid
     * functions vialable in the <initializer_list> func_list are:
     * potential_u - computes gravity potential in the units m^2 s^{-2}
     * gravity_r   - computes the first derivative of the gravity potential
     *               with respect to 'r' coordinate
     * gravity_phi - \frac{\partial U }{\partial \varphi}
     * gravity_lambda - \frac{\partial U }{\partial \lambda}
     * marussi_rr  - computes the second derivative of the gravity potential
                     with respect to the r coordinate
     * marussi_rb   -\frac{\partial U^2 }{\partial^2 r \varphi}
     * marussi_rl   -\frac{\partial U^2 }{\partial^2 r \lambda}
     * marussi_bb   ...
     * marussi_bl
     * marussi_ll
     */


    ///< TODO #001# pole areas of the earth are not very good
    ///< TODO #002# only possible input now is b l h

    if ( crdmat.is_empty() ) {
        cerr << "Can not continue with the computation! " << endl;
    }

    arma::colvec results = arma::zeros<arma::colvec>(crdmat.n_rows);

    Progressbar p_bar( static_cast<int>( crdmat.n_rows ));
    p_bar.set_name( "Tesseroid::grav_crd_list" );

    #pragma omp parallel for shared( results, upper_lay , lower_lay, check_radius)
    for (unsigned int n = 0; n < crdmat.n_rows; n++) {
        double brun = arma::as_scalar(crdmat(n,0));


        double phi_run = brun * DEG2RAD; // conversion to radians
        double lrun = arma::as_scalar(crdmat(n,1));
        double rrun = arma::as_scalar(crdmat(n,2));

        double n_rad =
            ell.a_axis / sqrt(1. - ell.eccentricity * sin(phi_run) * sin(phi_run));
        double m_rad = (ell.a_axis * (1. - ell.eccentricity)) /
                       pow(1. - ell.eccentricity * pow(sin(phi_run), 2.0), 3. / 2.);

        double r_mean = sqrt( n_rad * m_rad );
        // Check radius code - exclude distant points from the computation
        if ( check_radius ) {
          double delta_b = RAD2DEG * radius / (m_rad) ;  // return in deg format
          double delta_l = RAD2DEG * radius / (n_rad * cos(phi_run));  // return in deg format

          int dcol, drow = static_cast<int>(round(delta_b / delta_lat));
          if ( abs( abs(phi_run) - M_PI_2) <= .5*DEG2RAD ) {
            dcol = drow;
          } else {
            dcol = static_cast<int>(round(delta_l / delta_lon));
          }

          int leftb, rightb, upb, lowb, coln, rown;
          rown = static_cast<int>( round((brun - lat_min) / delta_lat));
          coln = static_cast<int>( round((lrun - lon_min) / delta_lon));

          leftb = ((coln - dcol) >= 0) ? (coln - dcol) : 0;
          leftb = (leftb < 0) ? 0 : leftb;
          rightb = ((coln + dcol) < static_cast<int>(ncols) ) ? (coln + dcol) : static_cast<int>(ncols - 1);

          lowb = ((rown - drow) >= 0) ? (rown - drow) : 0;
          lowb = (lowb < 0) ? 0 : lowb;
          upb = ((rown + drow) < static_cast<int>(nrows) ) ? (rown + drow) : static_cast<int>(nrows - 1);
          for ( int i = lowb; i <= upb ; i++) {
            for ( int j = leftb; j <= rightb ; j++) {
                double b1, b2, l1, l2, rm_low, rm_up;

                rm_low = arma::as_scalar( lower_lay(i,j) );
                rm_up =  arma::as_scalar( upper_lay(i,j) );

                if (isnan(rm_low) || isnan(rm_up) || isinf( rm_low ) || isinf(rm_up) )
                  continue;

                b1 = lat_min + static_cast<double>(i) * delta_lat - delta_lat/2.;
                b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

                l1 = lon_min + static_cast<double>(j) * delta_lon - delta_lon/2.;
                l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;


                double b0 = .5 * (b1 + b2) * DEG2RAD; // convert to radians
                double l0 = .5 * (l1 + l2) * DEG2RAD; // convert to radians
                double r0 = .5 * (rm_low + rm_up);
                double sph_len = tess_t::spherical_lenght( rrun + r_mean , phi_run, lrun*DEG2RAD, // little mess with DEG to RAD conversion
                                                           r0 + r_mean , b0 , l0 );               // phi_run already in radians, see above

                if (  sqrt(2.)*sph_len > radius ||
                      isnan(rm_low) || isnan(rm_up)  ) {
                    continue;
                }


                // obtaining rho from Isgem rho model
                double density = rho.get_value(i,j);

                results(n) +=  (this->*gravf)(rrun   + r_mean, brun, lrun,
                                                 rm_low + r_mean, b1, l1,
                                                 rm_up  + r_mean, b2, l2, kappa, density);

            }
          }
        } else {
          for (unsigned int i = 0; i < upper_lay.n_rows - 1; i++) {
            for (unsigned int j = 0; j < lower_lay.n_cols - 1; j++) {
              double b1, b2, l1, l2, rm_low, rm_up;

              rm_low = arma::as_scalar( lower_lay(i,j) );
              rm_up =  arma::as_scalar( upper_lay(i,j) );

              if (isnan(rm_low) || isnan(rm_up) || isinf( rm_low ) || isinf(rm_up) )
                continue;

              b1 = lat_min + static_cast<double>(i) * delta_lat - delta_lat/2.;
              b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

              l1 = lon_min + static_cast<double>(j) * delta_lon - delta_lon/2.;
              l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;

              // obtaining rho from Isgem rho model
              double density = rho.get_value(i,j);
              results(n) +=  (this->*gravf)(rrun   + r_mean, brun, lrun,
                                               rm_low + r_mean, b1, l1,
                                               rm_up  + r_mean, b2, l2, kappa, density);
            }
          }
        }

    #pragma omp critical
    {  p_bar.update();  } // see the progress

    }

    crdmat = arma::join_rows(crdmat, results );
}


arma::cube Tesseroid::gravity_field(
    bool check_radius,
    double radius,
    double kappa,
    double rho,
    struct geo_f::ellipsoid<double> ell,
    initializer_list<double (*)(double,
                                double,
                                double,
                                double,
                                double,
                                double,
                                double,
                                double,
                                double,
                                double,
                                double)> func_list) {
  /* Computes up to 10 components of the gravity field on grid data (spherical
   tess,
   * but integration is computed after transforming the integral kernel to
   cartesian
   * system).
   *
   * goes point by point on grid
   * functions vialable in the <initializer_list> func_list are:
   * potential_u - computes gravity potential in the units m^2 s^{-2}
   * gravity_r   - computes the first derivative of the gravity potential
   *               with respect to 'r' coordinate
   * gravity_phi - \frac{\partial U }{\partial \varphi}
   * gravity_lambda - \frac{\partial U }{\partial \lambda}
   * marussi_rr  - computes the second derivative of the gravity potential
                   with respect to the r coordinate
   * marussi_rb   -\frac{\partial U^2 }{\partial^2 r \varphi}
   * marussi_rl   -\frac{\partial U^2 }{\partial^2 r \lambda}
   * marussi_bb   ...
   * marussi_bl
   * marussi_ll
   */
  // arma::mat results(nrows,ncols); // returning variable
       arma::cube results = arma::zeros<arma::cube>(nrows, ncols, func_list.size());

  double brun;//, lrun, rrun;
  for (unsigned int n = 0; n < upper_lay.n_rows; n++) {
    brun = lat_min + static_cast<double>(n) * delta_lat;

    #pragma omp parallel for shared( brun, results, func_list )
    for (unsigned int m = 0; m < upper_lay.n_cols; m++) {
      double lrun = lon_min + static_cast<double>(m)  * delta_lon;
      double rrun = arma::as_scalar(upper_lay(n, m));

      if (check_radius) {
        double phi = brun * DEG2RAD;

        double n_rad =
            ell.a_axis / sqrt(1. - ell.eccentricity * sin(phi) * sin(phi));
        double m_rad = (ell.a_axis * (1. - ell.eccentricity)) /
                       pow(1. - ell.eccentricity * pow(sin(phi), 2.0), 3. / 2.);

        double r_mean = sqrt( n_rad * m_rad );
        double delta_b =
            RAD2DEG * radius / (n_rad * cos(phi));  // return in deg format
        double delta_l = RAD2DEG * radius / m_rad;  // return in deg format

        int drow = static_cast<int>(round(delta_b / delta_lat));
        int dcol = static_cast<int>(round(delta_l / delta_lon));

        int leftb, rightb, upb, lowb, coln, rown;
        rown = static_cast<int>(round((brun - lat_min) / delta_lat));
        coln = static_cast<int>(round((lrun - lon_min) / delta_lon));

        leftb = ((coln - dcol) >= 0) ? (coln - dcol) : 0;
        leftb = (leftb < 0) ? 0 : leftb;
        rightb = ((coln + dcol) < static_cast<int>(ncols) ) ? (coln + dcol) : static_cast<int>(ncols - 1);

        lowb = ((rown - drow) >= 0) ? (rown - drow) : 0;
        lowb = (lowb < 0) ? 0 : lowb;
        upb = ((rown + drow) < static_cast<int>(nrows)) ? (rown + drow) : static_cast<int>(nrows - 1);

        double b_smat = lat_min + leftb * delta_lat;
        double l_smat = lon_min + lowb * delta_lon;

        arma::mat upsubmat, lowsubmat;

        upsubmat =
            upper_lay.submat(arma::span(lowb, upb), arma::span(leftb, rightb));
        lowsubmat =
            lower_lay.submat(arma::span(lowb, upb), arma::span(leftb, rightb));

        for (unsigned int i = 0; i < upsubmat.n_rows - 1; i++) {
          for (unsigned int j = 0; j < upsubmat.n_cols - 1; j++) {
            arma::mat rlow, rup;

            rlow = lowsubmat.submat(arma::span(i, i + 1), arma::span(j, j + 1));
            rup = upsubmat.submat(arma::span(i, i + 1), arma::span(j, j + 1));

            double b1, b2, l1, l2, rm_low, rm_up;
            rm_low = handle_no_data(rlow);
            rm_up = handle_no_data(rup);

            if (isnan(rm_low) || isnan(rm_up))
              continue;

            b1 = b_smat + static_cast<double>(i) * delta_lat - delta_lat/2.;
            b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

            l1 = l_smat + static_cast<double>(j) * delta_lon - delta_lon/2. ;
            l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;

            int k = 0;
            for (auto* gravfun : func_list) {
              results(n, m, k) += gravfun(rrun  + r_mean, brun, lrun,
                                          rm_low+ r_mean, b1, l1,
                                          rm_up + r_mean, b2, l2, kappa, rho);
              k++;
            }
          }
        }
      } else {
        // No
        for (unsigned int i = 0; i < upper_lay.n_rows - 1; i++) {
          //#pragma omp parallel for shared( brun, results, func_list ) private( lrun, rrun  )
          for (unsigned int j = 0; j < lower_lay.n_cols - 1; j++) {
            arma::mat rlow, rup;
            double phi = brun*DEG2RAD;

            double n_rad =
                ell.a_axis / sqrt(1. - ell.eccentricity * sin(phi) * sin(phi));
            double m_rad = (ell.a_axis * (1. - ell.eccentricity)) /
                           pow(1. - ell.eccentricity * pow(sin(phi), 2.0), 3. / 2.);

            double r_mean = sqrt( n_rad * m_rad );

            rlow = lower_lay.submat(arma::span(i, i + 1), arma::span(j, j + 1));
            rup = upper_lay.submat(arma::span(i, i + 1), arma::span(j, j + 1));

            double b1, b2, l1, l2, rm_low, rm_up;
            rm_low = handle_no_data(rlow);
            rm_up = handle_no_data(rup);

            if (isnan(rm_low) || isnan(rm_up))
              continue;

            b1 = lat_min + static_cast<double>(i) * delta_lat - delta_lat/2.;
            b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

            l1 = lon_min + static_cast<double>(j) * delta_lon - delta_lon/2. ;
            l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;

            int k = 0;
            for (auto* gravfun : func_list) {
              results(n, m, k) += gravfun(rrun  + r_mean, brun, lrun,
                                          rm_low+ r_mean, b1, l1,
                                          rm_up + r_mean, b2, l2, kappa, rho);
              k++;
            }
          }
        }
      }
    }
  }
  return results;
}

// void Tesseroid::gravity_field( multimap<string, vector<double> > &inpts,
//                                bool check_radius, double radius, double
//                                kappa, double rho,
//                                struct geo_f::ellipsoid<double> ell,
//                                initializer_list <double (*)
//                                (double,double,double,double,double,double,
//                                double,double,double,double,double) >
//                                func_list
//                                ){
//    /* Computes up to 10 components of the gravity field on grid data
//    (spherical tess,
//     * but integration is computed after transforming the integral kernel to
//     cartesian
//     * system).
//     *
//     * goes point by point on grid
//     * functions vialable in the <initializer_list> func_list are:
//     * potential_u - computes gravity potential in the units m^2 s^{-2}
//     * gravity_r   - computes the first derivative of the gravity potential
//     *               with respect to 'r' coordinate
//     * gravity_phi - \frac{\partial U }{\partial \varphi}
//     * gravity_lambda - \frac{\partial U }{\partial \lambda}
//     * marussi_rr  - computes the second derivative of the gravity potential
//                     with respect to the r coordinate
//     * marussi_rb   -\frac{\partial U^2 }{\partial^2 r \varphi}
//     * marussi_rl   -\frac{\partial U^2 }{\partial^2 r \lambda}
//     * marussi_bb   ...
//     * marussi_bl
//     * marussi_ll
//     */
//
//    double rrun, brun , lrun, res;
//    multimap<string, vector<double> >::iterator it;
//    for ( it = inpts.begin(); it!= inpts.end(); it++){
//        brun = it->second[0];
//        lrun = it->second[1];
//        rrun = it->second[2];
//
//        for ( auto* gravf: func_list){
//            it->second.push_back( grav_from_tessgrid(rrun, brun, lrun,
//            check_radius,
//                                        radius, kappa, rho, ell, gravf ) );
//        }
//    }
//}

double Tesseroid::grav_from_tessgrid(double rrun,
                                     double brun,
                                     double lrun,
                                     bool check_radius,
                                     double radius,
                                     double kappa,
                                     double rho,
                                     struct geo_f::ellipsoid<double> ell,
                                     double (&gravf)(double,
                                                     double,
                                                     double,
                                                     double,
                                                     double,
                                                     double,
                                                     double,
                                                     double,
                                                     double,
                                                     double,
                                                     double)) {
  /* Compute on of the 10 components from DEM
   * > depends on grafv function reference
   * > ignoring tesseroids with no data
   *
   * Input check_radius - if only tess within distance smaller than radius are
   * used
   *             radius - the maximum radius that is allowed for computation
   *             kappa  - Newton's gravity constant
   *             rho    - density
   *
   *             {rrun, brun, lrun} = spherical coordinates of the point of
   * interest
   *   units:|-> {m , deg, deg}, running integration point
   *
   * gravf = {potential_u, gravity_i, marussi_ij} , 10 components in total
   * i,j = {phi/b , lambda/l, r}
   */
  arma::mat rlow, rup;
  double /*mean_r,*/ rm_low, rm_up;
  double b1, b2, l1, l2; /* boundaries of the selected tesseroid */

  /* Moving on the grid */
  double res = 0.0; /* result */

  if (check_radius) {
    double phi = brun * DEG2RAD;

    double n_rad =
        ell.a_axis / sqrt(1. - ell.eccentricity * sin(phi) * sin(phi));
    double m_rad = (ell.a_axis * (1. - ell.eccentricity)) /
                   pow(1. - ell.eccentricity * pow(sin(phi), 2.0), 3. / 2.);

    double r_mean = sqrt( n_rad * m_rad );

    double delta_b =
        RAD2DEG * radius / (n_rad * cos(phi));  // return in deg format
    double delta_l = RAD2DEG * radius / m_rad;  // return in deg format

    int drow = static_cast<int>(round(delta_b / delta_lat));
    int dcol = static_cast<int>(round(delta_l / delta_lon));

    int leftb, rightb, upb, lowb, coln, rown;

    rown = static_cast<int>( round((brun - lat_min) / delta_lat));
    coln = static_cast<int>( round((lrun - lon_min) / delta_lon));

    leftb = ((coln - dcol) >= 0) ? (coln - dcol) : 0;
    leftb = (leftb < 0) ? 0 : leftb;
    rightb = ((coln + dcol) < static_cast<int>(ncols)) ? (coln + dcol) : static_cast<int>(ncols) - 1;

    lowb = ((rown - drow) >= 0) ? (rown - drow) : 0;
    lowb = (lowb < 0) ? 0 : lowb;
    upb = ((rown + drow) < static_cast<int>(nrows)) ? (rown + drow) : static_cast<int>(nrows) - 1;

    arma::mat upsubmat, lowsubmat;

    upsubmat =
        upper_lay.submat(arma::span(lowb, upb), arma::span(leftb, rightb));
    lowsubmat =
        lower_lay.submat(arma::span(lowb, upb), arma::span(leftb, rightb));

    for (unsigned int i = 0; i < upsubmat.n_rows - 1; i++) {
      #pragma omp parallel for shared( brun, lrun, rrun , res )
      for (unsigned int j = 0; j < upsubmat.n_cols - 1; j++) {
        //Timer T("onept");
        rlow = lowsubmat.submat(arma::span(i, i + 1), arma::span(j, j + 1));
        rup = upsubmat.submat(arma::span(i, i + 1), arma::span(j, j + 1));

        rm_low = handle_no_data(lowsubmat);
        rm_up = handle_no_data(upsubmat);

        // add the mean radius ( $\f r = \sqrt { M \cdot N } \f$ ) for the running point of the integration


        if (isnan(rm_low) || isnan(rm_up))
          continue;

        rm_low += r_mean;
        rm_up  += r_mean;

        b1 = lat_min + static_cast<double>(i) * delta_lat - delta_lat/2.;
        b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

        l1 = lon_min + static_cast<double>(j) * delta_lon - delta_lon/2.;
        l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;

        res +=
            gravf(brun, lrun, rrun, kappa, rho, rm_low, b1, l1, rm_up, b2, l2);
      }
    }
  } else {
      double phi = brun * DEG2RAD;

      double n_rad =
          ell.a_axis / sqrt(1. - ell.eccentricity * sin(phi) * sin(phi));
      double m_rad = (ell.a_axis * (1. - ell.eccentricity)) /
                     pow(1. - ell.eccentricity * pow(sin(phi), 2.0), 3. / 2.);

      double r_mean = sqrt( n_rad * m_rad );

    for (unsigned int i = 0; i < upper_lay.n_rows - 1; i++) {
      double ii = static_cast<double>(i);
      #pragma omp parallel for shared( brun, res, lrun, rrun , ii,i )
      for (unsigned int j = 0; j < upper_lay.n_cols - 1; j++) {
        double jj = static_cast<double>(j);
        rlow = lower_lay.submat(arma::span(i, i + 1), arma::span(j, j + 1));
        rup = upper_lay.submat(arma::span(i, i + 1), arma::span(j, j + 1));

        rm_low = handle_no_data(rlow);
        rm_up = handle_no_data(rup);

        if (isnan(rm_low) || isnan(rm_up))
          continue;

        rm_low += r_mean;
        rm_up  += r_mean;

        b1 = lat_min + ii * delta_lat - delta_lat/2.;
        b2 = b1 + delta_lat;  // lat_min + (double) (1+i) * dlat;

        l1 = lon_min + jj * delta_lon - delta_lon/2.;
        l2 = l1 + delta_lon;  // lon_min + (double) (j+1) * dlon;

        res +=
            gravf(brun, lrun, rrun, kappa, rho, rm_low, b1, l1, rm_up, b2, l2);
      }
    }
  }

  return res;
}

double Tesseroid::m_ijk(
    double r,
    double phi,
    double lambda,
    double r1,
    double phi1,
    double lambda1,
    double r2,
    double phi2,
    double lambda2,
    double kappa,
    double rho,
    double ratio,
    double (&k_ijk) (double, double, double, double, double, double),
    double (&alpha_ij)(double, double, double, double, double, double),
    double (&beta1_ijk)(double, double, double, double, double, double),
    double (&beta2_ijk)(double, double, double, double, double, double),
    double (&gamma1_ijk)(double, double, double, double, double, double),
    double (&gamma2_ijk)(double, double, double, double, double, double),
    double (&gamma3_ijk)(double, double, double, double, double, double)) {
  /*  input:
      r1, r2, phi1, phi2, lambda1, lambda2 - are the boundaries for tesseroid
     (OMEGA)
      for which the following conditions have to be met:
          r1 < r2
          phi1 < phi2
          lambda1 < lambda2

      r, phi, lambda - point P in which the characteristics are computed;
      P is not inside integration domain OMEGA
         but can be on the boundary of the spherical volume element dOMEGA

      if P is inside the function will automatically split it into 8
     subtesseroids

      all angle units are in radians
      units of length are in metres

      This function can be used for computation (by changing the reference
     functions):
          - gravity potential
          - 1st derivative of gravity potential
          - 2nd derivative of gravity potential
  */

  double r0, phi0, lambda0, d_r, d_phi, d_lambda, s, mm_ij;

  /* geoemtric center of the tesseroid */
  r0      = .5 * (r1 + r2);
  phi0    = .5 * (phi1 + phi2);
  lambda0 = .5 * (lambda1 + lambda2);

  /* lenght on the sphere */
  s = tess_t::length_sphere(r, phi, lambda, r0, phi0, lambda0);

  if ( s <= __EPS__ || isnan(s) || isinf(s) ) {
      return 0.0 ; // addressing the singularities
  }

  /* size of the tesseroid */
  d_r = abs(r1 - r2);
  d_phi = r2*acos( sin(phi1)*sin(phi2)+cos(phi1)*cos(phi2)  ) ; //abs(phi1 - phi2);
  d_lambda = r2*acos( sin(phi0)*sin(phi0) + cos(phi0)*cos(phi0)*cos( lambda2 - lambda1 )); //abs(lambda1 - lambda2);

  if ( d_r < 1.0e-15 || d_phi < 1.0e-15 || d_lambda < 1.0e-15 ) {
      return 0.;
  }
  if (r > r1 && r < r2 && phi > phi1 && phi < phi2 && lambda > lambda1 &&
      lambda < lambda2) {
    /* check if the point P is inside the set OMEGA.
       if it's inside then the tesseroid is splitted  */

    //<< stl vector memory allocation is not thread safe with openmp >>///
//    vector<double> rlist = {r1, r, r2};
//    vector<double> philist = {phi1, phi, phi2};
//    vector<double> lambdalist = {lambda1, lambda, lambda2};

    my_vector rlist, philist, lambdalist;

    rlist[0] =               r1; rlist[1] = r;       rlist[2] = r2;
    philist[0] = phi1;       philist[1] = phi;       philist[2] = phi2;
    lambdalist[0] = lambda1; lambdalist[1] = lambda; lambdalist[2] = lambda2;

    mm_ij = 0.0;
    for (unsigned int i = 0; i < 2; i++) {
      for (unsigned int j = 0; j < 2; j++) {
        for (unsigned int k = 0; k < 2; k++) {
          mm_ij += m_ijk(r, phi, lambda, rlist[i], philist[j], lambdalist[k],
                         rlist[i + 1], philist[j + 1], lambdalist[k + 1], kappa,
                         rho, ratio, k_ijk, alpha_ij, beta1_ijk, beta2_ijk, gamma1_ijk,
                         gamma2_ijk, gamma3_ijk);
        }
      }
    }
  } else if ( (  (s < ratio * d_phi )    ||
                 (s < ratio * d_lambda ) ||
                 (s < ratio * d_r) )
                  &&  d_phi*d_lambda*d_r>= 1.0e+4 ) {
    //    /* To minimize approximation error the subdivision of tess models
    //    * is adapted. The criterium is \psi_c < 5 \cdot \Delta_hor
    //    * \Delta_hor = max{\Delta \varphi, \Delta \lambda}

    //    * 1st attempt is to split the tess into 8 smaller
    //    */
            my_vector rlist, philist, lambdalist;
            if (s < ratio *d_r) {
                rlist[0] = r1 ; rlist[1] = (r1+r2)/2. ; rlist[2] = r2 ;
            } else {
                rlist[0] = r1 ; rlist[1] = r2;
            }

            if ( s < ratio * d_phi ) {
                philist[0] = (phi1);
                philist[1] = ( (phi1+phi2)/2.);
                philist[2] = ( phi2 );
            } else {
                philist[0] =( phi1 ); philist[1] = (phi2);
            }

            if ( s < ratio* d_lambda ) {
                lambdalist[0] = (lambda1);
                lambdalist[1] = ( (lambda1+lambda2)/2.);
                lambdalist[2] = ( lambda2 );
            } else {
                lambdalist[0] =( lambda1 ); lambdalist[1] = (lambda2);
            }

            mm_ij = 0.0;
            for (int i = 0; i < rlist.size() - 1; i++) {
              for (int j = 0; j < philist.size() -1; j++) {
                for (int k = 0; k < lambdalist.size() -1 ; k++) {
                  mm_ij += m_ijk(r, phi, lambda, rlist[i], philist[j], lambdalist[k],
                                 rlist[i + 1], philist[j + 1], lambdalist[k + 1], kappa,
                                 rho, ratio, k_ijk, alpha_ij, beta1_ijk, beta2_ijk, gamma1_ijk,
                                 gamma2_ijk, gamma3_ijk);
                }
              }
            }
  } else {
    double dd_r      = r2-r1; // in meetres
    double dd_phi    =  (phi2 - phi1); // in radians
    double dd_lambda = (lambda2 - lambda1); // in radians

    double tau = r0*r0*cos(phi0);
    mm_ij = dd_r * dd_phi * dd_lambda * kappa * rho / (s * s * s ) *
            ( tau * k_ijk( r, phi, lambda, r0, phi0, lambda0 )
              + (1./24.)*( dd_r * dd_r *(2. * cos(phi0) *
                              alpha_ij(r, phi, lambda, r0, phi0, lambda0) +
                          2. * r0 * cos(phi0) *
                              beta1_ijk(r, phi, lambda, r0, phi0, lambda0) +
                          r0 * r0 * cos(phi0) *
                              gamma1_ijk(r, phi, lambda, r0, phi0, lambda0))
                  + dd_phi * dd_phi * (-r0 * r0 * cos(phi0) *
                     alpha_ij(r, phi, lambda, r0, phi0, lambda0) -
                 r0 * r0 * sin(phi0) *
                     beta2_ijk(r, phi, lambda, r0, phi0, lambda0) +
                 r0 * r0 * cos(phi0) *
                     gamma2_ijk(r, phi, lambda, r0, phi0, lambda0))
                  +  dd_lambda * dd_lambda *
                  (r0 * r0 * cos(phi0) *
                   gamma3_ijk(r, phi, lambda, r0, phi0, lambda0))
                  ));

    if ( isnan(mm_ij) || isinf(mm_ij)  ) {
      return 0.0;
    }
  }
  return mm_ij;
} /* m_ijk */

double Tesseroid::potential_u(
    double r,
    double phi,
    double lambda, /* running point of the integration */
    double r1,
    double phi1,
    double lambda1, /* boundaries of the tesseroid */
    double r2,
    double phi2,
    double lambda2,
    double kappa,
    double rho) { /* kappa =  newton gravity constant, rho - density */
  /* returns gravity potential caused by spherical tesseroid */
  double b, b1, b2, l, l1, l2;

  /* conversion from degrees in deg format into radians */
  b = phi * DEG2RAD;
  b1 = phi1 * DEG2RAD;
  b2 = phi2 * DEG2RAD;
  l = lambda * DEG2RAD;
  l1 = lambda1 * DEG2RAD;
  l2 = lambda2 * DEG2RAD;

  return (m_ijk(r, b, l, r1, b1, l1, r2, b2, l2, kappa, rho, TESS_POT_RATIO,
                tess_t::length_sphere_sq,
                tess_t::length_sphere_sq, tess_t::beta_1, tess_t::beta_2,
                tess_t::gamma_1, tess_t::gamma_2, tess_t::gamma_3));
} /* potential_u */

double Tesseroid::gravity_r(
    double r,
    double phi,
    double lambda, /* running point of the integration */
    double r1,
    double phi1,
    double lambda1, /* boundaries of the tesseroid */
    double r2,
    double phi2,
    double lambda2,
    double kappa,
    double rho) { /* kappa =  newton gravity constant, rho - density */
  /* returns gravity potential derivative by r caused by spherical tesseroid
     gravity_r = \frac{\partial U}{\partial r}
  */
  double b, b1, b2, l, l1, l2;

  /* conversion from degrees in deg format into radians */
  b = phi * DEG2RAD;
  b1 = phi1 * DEG2RAD;
  b2 = phi2 * DEG2RAD;
  l = lambda * DEG2RAD;
  l1 = lambda1 * DEG2RAD;
  l2 = lambda2 * DEG2RAD;
  return (m_ijk(r, b, l, r1, b1, l1, r2, b2, l2, kappa, rho, TESS_GX_RATIO,
                tess_t::dx1, tess_t::dx1, tess_t::beta_11, tess_t::beta_12,
                tess_t::gamma_11, tess_t::gamma_12, tess_t::gamma_13));
} /* gravity_r */

double Tesseroid::gravity_phi(
    double r,
    double phi,
    double lambda, /* running point of the integration */
    double r1,
    double phi1,
    double lambda1, /* boundaries of the tesseroid */
    double r2,
    double phi2,
    double lambda2,
    double kappa,
    double rho) { /* kappa =  newton gravity constant, rho - density */
  /* returns gravity potential derivative by phi caused by spherical tesseroid
     gravity_r = \frac{\partial U}{\phi r}
  */
  double b, b1, b2, l, l1, l2;

  /* conversion from degrees in deg format into radians */
  b = phi * DEG2RAD;
  b1 = phi1 * DEG2RAD;
  b2 = phi2 * DEG2RAD;
  l = lambda * DEG2RAD;
  l1 = lambda1 * DEG2RAD;
  l2 = lambda2 * DEG2RAD;

  return (-m_ijk(r, b, l, r1, b1, l1, r2, b2, l2, kappa, rho, TESS_GX_RATIO,
                tess_t::dx2, tess_t::dx2,
                tess_t::beta_21, tess_t::beta_22, tess_t::gamma_21,
                tess_t::gamma_22, tess_t::gamma_23));
} /* gravity_phi */

double Tesseroid::gravity_lambda(
    double r,
    double phi,
    double lambda, /* running point of the integration */
    double r1,
    double phi1,
    double lambda1, /* boundaries of the tesseroid */
    double r2,
    double phi2,
    double lambda2,
    double kappa,
    double rho) { /* kappa =  newton gravity constant, rho - density */
  /* returns gravity potential derivative by lambda caused by spherical
     tesseroid
     gravity_r = \frac{\partial U}{\lambda r}
  */
  double b, b1, b2, l, l1, l2;

  /* conversion from degrees in deg format into radians */
  b = phi * DEG2RAD;
  b1 = phi1 * DEG2RAD;
  b2 = phi2 * DEG2RAD;
  l = lambda * DEG2RAD;
  l1 = lambda1 * DEG2RAD;
  l2 = lambda2 * DEG2RAD;

  return (-m_ijk(r, b, l, r1, b1, l1, r2, b2, l2, kappa, rho, TESS_GX_RATIO,
                tess_t::dx3, tess_t::dx3,
                tess_t::beta_31, tess_t::beta_32, tess_t::gamma_31,
                tess_t::gamma_32, tess_t::gamma_33));
} /* gravity_lambda */

double Tesseroid::marussi_rr(
    double r,
    double phi,
    double lambda, /* running point of the integration */
    double r1,
    double phi1,
    double lambda1, /* boundaries of the tesseroid */
    double r2,
    double phi2,
    double lambda2,
    double kappa,
    double rho) { /* kappa =  newton gravity constant, rho - density */
  /* Returns the contribution of the spherical resseroid to Marussi tensor
     for derivate respecting cordinates: r,r
  */
  double b, b1, b2, l, l1, l2;

  /* conversion from degrees in deg format into radians */
  b = phi * DEG2RAD;
  b1 = phi1 * DEG2RAD;
  b2 = phi2 * DEG2RAD;
  l = lambda * DEG2RAD;
  l1 = lambda1 * DEG2RAD;
  l2 = lambda2 * DEG2RAD;

  return (m_ijk(r, b, l, r1, b1, l1, r2, b2, l2, kappa, rho, TESS_GXX_RATIO,
                tess_t::n11_pq, tess_t::alpha_11,
                tess_t::beta_111, tess_t::beta_112, tess_t::gamma_111,
                tess_t::gamma_112, tess_t::gamma_113));
} /* marussi_rr */

double Tesseroid::marussi_rb(
    double r,
    double phi,
    double lambda, /* running point of the integration */
    double r1,
    double phi1,
    double lambda1, /* boundaries of the tesseroid */
    double r2,
    double phi2,
    double lambda2,
    double kappa,
    double rho) { /* kappa =  newton gravity constant, rho - density */
  /* Returns the contribution of the spherical resseroid to Marussi tensor
     for derivate respecting cordinates: r,phi
  */
  double b, b1, b2, l, l1, l2;

  /* conversion from degrees in deg format into radians */
  b = phi * DEG2RAD;
  b1 = phi1 * DEG2RAD;
  b2 = phi2 * DEG2RAD;
  l = lambda * DEG2RAD;
  l1 = lambda1 * DEG2RAD;
  l2 = lambda2 * DEG2RAD;

  return (m_ijk(r, b, l, r1, b1, l1, r2, b2, l2, kappa, rho, TESS_GXX_RATIO,
                tess_t::n12_pq, tess_t::alpha_12,
                tess_t::beta_121, tess_t::beta_122, tess_t::gamma_121,
                tess_t::gamma_122, tess_t::gamma_123));
} /* marussi_rb */

double Tesseroid::marussi_rl(
    double r,
    double phi,
    double lambda, /* running point of the integration */
    double r1,
    double phi1,
    double lambda1, /* boundaries of the tesseroid */
    double r2,
    double phi2,
    double lambda2,
    double kappa,
    double rho) { /* kappa =  newton gravity constant, rho - density */
  /* Returns the contribution of the spherical resseroid to Marussi tensor
     for derivate respecting cordinates: r,lambda
  */
  double b, b1, b2, l, l1, l2;

  /* conversion from degrees in deg format into radians */
  b = phi * DEG2RAD;
  b1 = phi1 * DEG2RAD;
  b2 = phi2 * DEG2RAD;
  l = lambda * DEG2RAD;
  l1 = lambda1 * DEG2RAD;
  l2 = lambda2 * DEG2RAD;

  return (m_ijk(r, b, l, r1, b1, l1, r2, b2, l2, kappa, rho, TESS_GXX_RATIO,
                tess_t::n13_pq, tess_t::alpha_13,
                tess_t::beta_113, tess_t::beta_123, tess_t::gamma_131,
                tess_t::gamma_132, tess_t::gamma_133));
} /* marussi_rl */

double Tesseroid::marussi_bb(
    double r,
    double phi,
    double lambda, /* running point of the integration */
    double r1,
    double phi1,
    double lambda1, /* boundaries of the tesseroid */
    double r2,
    double phi2,
    double lambda2,
    double kappa,
    double rho) { /* kappa =  newton gravity constant, rho - density */
  /* Returns the contribution of the spherical resseroid to Marussi tensor
     for derivate respecting cordinates: phi, phi
  */
  double b, b1, b2, l, l1, l2;

  /* conversion from degrees in deg format into radians */
  b = phi * DEG2RAD;
  b1 = phi1 * DEG2RAD;
  b2 = phi2 * DEG2RAD;
  l = lambda * DEG2RAD;
  l1 = lambda1 * DEG2RAD;
  l2 = lambda2 * DEG2RAD;

  return (m_ijk(r, b, l, r1, b1, l1, r2, b2, l2, kappa, rho,TESS_GXX_RATIO ,
                tess_t::n22_pq, tess_t::alpha_22,
                tess_t::beta_221, tess_t::beta_222, tess_t::gamma_221,
                tess_t::gamma_222, tess_t::gamma_223));
} /* marussi_bb */

double Tesseroid::marussi_bl(
    double r,
    double phi,
    double lambda, /* running point of the integration */
    double r1,
    double phi1,
    double lambda1, /* boundaries of the tesseroid */
    double r2,
    double phi2,
    double lambda2,
    double kappa,
    double rho) { /* kappa =  newton gravity constant, rho - density */
  /* Returns the contribution of the spherical resseroid to Marussi tensor
     for derivate respecting cordinates: phi, lambda
  */
  double b, b1, b2, l, l1, l2;

  /* conversion from degrees in deg format into radians */
  b = phi * DEG2RAD;
  b1 = phi1 * DEG2RAD;
  b2 = phi2 * DEG2RAD;
  l = lambda * DEG2RAD;
  l1 = lambda1 * DEG2RAD;
  l2 = lambda2 * DEG2RAD;

  return (m_ijk(r, b, l, r1, b1, l1, r2, b2, l2, kappa, rho, TESS_GXX_RATIO,
                tess_t::n23_pq, tess_t::alpha_23,
                tess_t::beta_231, tess_t::beta_232, tess_t::gamma_231,
                tess_t::gamma_232, tess_t::gamma_233));
} /* marussi_bb */

double Tesseroid::marussi_ll(
    double r,
    double phi,
    double lambda, /* running point of the integration */
    double r1,
    double phi1,
    double lambda1, /* boundaries of the tesseroid */
    double r2,
    double phi2,
    double lambda2,
    double kappa,
    double rho) { /* kappa =  newton gravity constant, rho - density */
  /* Returns the contribution of the spherical resseroid to Marussi tensor
     for derivate respecting cordinates: lambda, lambda
  */
  double b, b1, b2, l, l1, l2;

  /* conversion from degrees in deg format into radians */
  b = phi * DEG2RAD;
  b1 = phi1 * DEG2RAD;
  b2 = phi2 * DEG2RAD;
  l = lambda * DEG2RAD;
  l1 = lambda1 * DEG2RAD;
  l2 = lambda2 * DEG2RAD;

  return (m_ijk(r, b, l, r1, b1, l1, r2, b2, l2, kappa, rho, TESS_GXX_RATIO,
                tess_t::n33_pq, tess_t::alpha_33,
                tess_t::beta_331, tess_t::beta_332, tess_t::gamma_331,
                tess_t::gamma_332, tess_t::gamma_333));
} /* marussi_ll */

double Tesseroid::handle_no_data(arma::mat A) {
  double res = 0.;
  int numel = 0;

  arma::mat::iterator a = A.begin();
  arma::mat::iterator b = A.end();

  for (arma::mat::iterator i = a; i != b; ++i) {
    if (*i == nodata) {
      continue;
    } else {
      res += *i;
      numel++;
    }
  }
  return (res / static_cast<double>(numel));
}

double Tesseroid::interpolate_value( double phi, double lambda) {
    // krieger method is applied
    if ( ( phi <= lat_max ) && ( phi >= lat_min ) &&
         ( lambda <= lon_max ) && ( lambda >= lon_min  )) {

        double lwi = 0.0,li,  wi,  b,l, sum_weights = 0.0;

        double m,n,r_nm,ee,a;

        a = WGS84_A; // parameters of the WGS84 ellipsoid
        ee= WGS84_E*WGS84_E; // e^2

        m = (a*(1.0 - ee))/pow( 1.0 - ee * pow (sin(phi*DEG2RAD),2.0), 3.0/2.0); // meridian mean radius
        n = a/sqrt( 1.0 - ee * pow (sin(phi*DEG2RAD), 2.0)); // mean radius in

        r_nm = sqrt (m*n);

        unsigned int rown = static_cast<unsigned int>(floor((phi - lat_min) / delta_lat ));
        unsigned int coln = static_cast<unsigned int>(floor((lambda - lon_min) / delta_lon));

        for (unsigned int i = 0; i < 2; i++) { // lengths[2*i+j]*lengths[2*i+j] = ...;
            for (unsigned int j =0 ; j < 2; j++) {
                b = lat_min + static_cast<double>(rown + i) * delta_lat;
                l = lon_min + static_cast<double>(coln + j) * delta_lon;


                li = geo_f::len_on_sphere(r_nm, phi, lambda, b,l);
                wi = 1./(li+__EPS__);

                lwi         += wi* arma::as_scalar( upper_lay(rown + i, coln + j))  ;
                sum_weights += wi;
            }
        }

        double result = lwi/sum_weights;

        if ( isnan(result) || isinf(result) ) {
            return arma::as_scalar( upper_lay(rown,coln) );
        } else {
            return  upper_lay(rown,coln) ;
        }
    } else {
        string error_msg = "Calling function \"Isgemgeoid::interpolate_value()\" the value requested for interpolation is out of the bounds." ;
        throw logic_error( string(error_msg) );
    }
}

bool Tesseroid::do_check()
{
    bool isgood  =true;
    if ( this->upper_set && this->lower_set ) {
        // both layers are set
        cout << "DEM model Tesseroid::do_check() " << this->model_name << " is set (upper and lower layer)!" << endl;
        isgood = this->check_layers_size();
    } else if ( this->upper_set && this->lower_set==false ) {
        this->lower_lay = arma::zeros<arma::mat>( arma::size( this->upper_lay ) );
        cout << "DEM model Tesseroid::do_check() " << this->model_name << " is set (upper) and lower is set to zero mat!" << endl;
    } else if ( this->upper_set==false && this->lower_set==false ) {
        cout << "DEM model Tesseroid::do_check() " << this->model_name << " none of the layers is set!" << endl;
        isgood = false;
    } else {
        this->upper_lay = arma::zeros<arma::mat>( arma::size( this->lower_lay ));
        cout << "DEM model Tesseroid::do_check() " << this->model_name << " is set (lower) and upper is set to zero mat!" << endl;
    }

    return isgood;
}
