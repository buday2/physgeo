#include "../include/tessquad.h"



double Tessquad::potential_u(double rp, double bp, double lp)
{
    double cos_psi = sin(bp)*sin(phi) + cos(phi)*cos(bp)*cos(lp - lam);
    //double c_phi = cos(phi)*sin(bp) - sin(phi)*cos(bp)*cos(lp-lam); ///< \frac{ \partial \cos \psi}{\partial \varphi}
    //double c_lam = cos(phi)*cos(bp)*sin(lp-lam);  ///< \frac{ \partial \cos \psi}{\partial \lambda}

    double l = sqrt(r*r+rp*rp-2.*r*rp*cos_psi );
    double domega = rp*rp*cos(bp); ///< \mathrm{d} \Omega = r^2 \cos \varphi \mathrm{d} r \mathrm{d} \varphi mathrm{d} \lambda


    return domega/(l);
}

double Tessquad::gravity_r(double rp, double bp, double lp)
{
    double cos_psi = sin(bp)*sin(phi) + cos(phi)*cos(bp)*cos(lp - lam);
    ///double c_phi = cos(phi)*sin(bp) - sin(phi)*cos(bp)*cos(lp-lam); ///< \frac{ \partial \cos \psi}{\partial \varphi}
    //double c_lam = cos(phi)*cos(bp)*sin(lp-lam);  ///< \frac{ \partial \cos \psi}{\partial \lambda}

    double l = sqrt(r*r+rp*rp-2.*r*rp*cos_psi );
    double domega = rp*rp*cos(bp); ///< \mathrm{d} \Omega = r^2 \cos \varphi \mathrm{d} r \mathrm{d} \varphi mathrm{d} \lambda


    return (rp * cos_psi - r)*domega/(l*l*l);
}

double Tessquad::gravity_phi(double rp, double bp, double lp)
{
    double cos_psi = sin(bp)*sin(phi) + cos(phi)*cos(bp)*cos(lp - lam);
    double c_phi = cos(phi)*sin(bp) - sin(phi)*cos(bp)*cos(lp-lam); ///< \frac{ \partial \cos \psi}{\partial \varphi}
    //double c_lam = cos(phi)*cos(bp)*sin(lp-lam);  ///< \frac{ \partial \cos \psi}{\partial \lambda}

    double l = sqrt(r*r+rp*rp-2.*r*rp*cos_psi );    
    double domega = rp*rp*cos(bp); ///< \mathrm{d} \Omega = r^2 \cos \varphi \mathrm{d} r \mathrm{d} \varphi mathrm{d} \lambda


    return r*rp*c_phi*domega/(l*l*l);
}

double Tessquad::gravity_lambda(double rp, double bp, double lp)
{
    double cos_psi = sin(bp)*sin(phi) + cos(phi)*cos(bp)*cos(lp - lam);
    //double c_phi = cos(phi)*sin(bp) - sin(phi)*cos(bp)*cos(lp-lam); ///< \frac{ \partial \cos \psi}{\partial \varphi}
    double c_lam = cos(phi)*cos(bp)*sin(lp-lam);  ///< \frac{ \partial \cos \psi}{\partial \lambda}

    double l = sqrt(r*r+rp*rp-2.*r*rp*cos_psi );
    double domega = rp*rp*cos(bp); ///< \mathrm{d} \Omega = r^2 \cos \varphi \mathrm{d} r \mathrm{d} \varphi mathrm{d} \lambda


    return r*rp*c_lam*domega/(l*l*l);
}

double Tessquad::marussi_rr(double rp, double bp, double lp)
{
    double cos_psi = sin(bp)*sin(phi) + cos(phi)*cos(bp)*cos(lp - lam);
    ///double c_phi = cos(phi)*sin(bp) - sin(phi)*cos(bp)*cos(lp-lam); ///< \frac{ \partial \cos \psi}{\partial \varphi}
    //double c_lam = cos(phi)*cos(bp)*sin(lp-lam);  ///< \frac{ \partial \cos \psi}{\partial \lambda}

    double l = sqrt(r*r+rp*rp-2.*r*rp*cos_psi );
    double domega = rp*rp*cos(bp); ///< \mathrm{d} \Omega = r^2 \cos \varphi \mathrm{d} r \mathrm{d} \varphi mathrm{d} \lambda


    return domega/(l*l*l)*( (3.*pow( rp*cos_psi - r ,2.))/(l*l)  -1.)  ;
}

double Tessquad::marussi_bb(double rp, double bp, double lp)
{
    double cos_psi = sin(bp)*sin(phi) + cos(phi)*cos(bp)*cos(lp - lam);
    double c_phi = cos(phi)*sin(bp) - sin(phi)*cos(bp)*cos(lp-lam); ///< \frac{ \partial \cos \psi}{\partial \varphi}
    //double c_lam = cos(phi)*cos(bp)*sin(lp-lam);  ///< \frac{ \partial \cos \psi}{\partial \lambda}

    double l = sqrt(r*r+rp*rp-2.*r*rp*cos_psi );
    double domega = rp*rp*cos(bp); ///< \mathrm{d} \Omega = r^2 \cos \varphi \mathrm{d} r \mathrm{d} \varphi mathrm{d} \lambda


    return  r*rp*domega/(l*l*l)*( (3.*r*rp*c_phi*c_phi)/(l*l)  - cos_psi )  ;
}

double Tessquad::marussi_ll(double rp, double bp, double lp)
{
    double cos_psi = sin(bp)*sin(phi) + cos(phi)*cos(bp)*cos(lp - lam);
    //double c_phi = cos(phi)*sin(bp) - sin(phi)*cos(bp)*cos(lp-lam); ///< \frac{ \partial \cos \psi}{\partial \varphi}
    double c_lam = cos(phi)*cos(bp)*sin(lp-lam);  ///< \frac{ \partial \cos \psi}{\partial \lambda}

    double l = sqrt(r*r+rp*rp-2.*r*rp*cos_psi );
    double domega = rp*rp*cos(bp); ///< \mathrm{d} \Omega = r^2 \cos \varphi \mathrm{d} r \mathrm{d} \varphi mathrm{d} \lambda


    return  r*rp*domega/(l*l*l)*( (3.*r*rp*c_lam*c_lam)/(l*l)  - cos(phi)*cos(bp)*cos(lp-lam) )  ;
}

double Tessquad::marussi_rb(double rp, double bp, double lp)
{
    double cos_psi = sin(bp)*sin(phi) + cos(phi)*cos(bp)*cos(lp - lam);
    double c_phi = cos(phi)*sin(bp) - sin(phi)*cos(bp)*cos(lp-lam); ///< \frac{ \partial \cos \psi}{\partial \varphi}
    //double c_lam = cos(phi)*cos(bp)*sin(lp-lam);  ///< \frac{ \partial \cos \psi}{\partial \lambda}

    double l = sqrt(r*r+rp*rp-2.*r*rp*cos_psi );
    double domega = rp*rp*cos(bp); ///< \mathrm{d} \Omega = r^2 \cos \varphi \mathrm{d} r \mathrm{d} \varphi mathrm{d} \lambda


    return  r*c_phi*domega/(l*l*l)*( (3.*r*(rp*cos_psi - r))/(l*l)  +1. )  ;
}

double Tessquad::marussi_rl(double rp, double bp, double lp)
{
    double cos_psi = sin(bp)*sin(phi) + cos(phi)*cos(bp)*cos(lp - lam);
    //double c_phi = cos(phi)*sin(bp) - sin(phi)*cos(bp)*cos(lp-lam); ///< \frac{ \partial \cos \psi}{\partial \varphi}
    double c_lam = cos(phi)*cos(bp)*sin(lp-lam);  ///< \frac{ \partial \cos \psi}{\partial \lambda}

    double l = sqrt(r*r+rp*rp-2.*r*rp*cos_psi );
    double domega = rp*rp*cos(bp); ///< \mathrm{d} \Omega = r^2 \cos \varphi \mathrm{d} r \mathrm{d} \varphi mathrm{d} \lambda


    return  r*c_lam*domega/(l*l*l)*( (3.*r*(rp*cos_psi - r))/(l*l)  +1. )  ;
}

double Tessquad::marussi_bl(double rp, double bp, double lp)
{
    double cos_psi = sin(bp)*sin(phi) + cos(phi)*cos(bp)*cos(lp - lam);
    double c_phi = cos(phi)*sin(bp) - sin(phi)*cos(bp)*cos(lp-lam); ///< \frac{ \partial \cos \psi}{\partial \varphi}
    //double c_lam = cos(phi)*cos(bp)*sin(lp-lam);  ///< \frac{ \partial \cos \psi}{\partial \lambda}

    double l = sqrt(r*r+rp*rp-2.*r*rp*cos_psi );
    double domega = rp*rp*cos(bp); ///< \mathrm{d} \Omega = r^2 \cos \varphi \mathrm{d} r \mathrm{d} \varphi mathrm{d} \lambda


    return r*rp*cos(bp)*sin(lp-lam)*domega/(l*l*l)*( (3.*r*rp*c_phi*cos(phi))/(l*l) -sin(phi)) ;
}

double Tessquad::int2d(double (Tessquad::*gravfxy)(double, double, double),     double b1, double b2,
                                                                    double l1,  double l2, double r0,
                                                                    double rho, double kappa, double tess_ratio)
{
    double b0 = (b1 + b2)/2.;
    double l0 = (l1 + l2)/2.;

    double cos_psi = sin(b0)*sin(phi) + cos(phi)*cos(b0)*cos(l0 - lam);
    double l = sqrt(r*r+r0*r0-2.*r*r0*cos_psi );

    /* size of the tesseroid */
    double d_phi = r0*acos( sin(b1)*sin(b2)+cos(b1)*cos(b2)  ) ; //abs(phi1 - phi2);
    double d_lambda = r0*acos( sin(b0)*sin(b0) + cos(b0)*cos(b0)*cos( l2 - l1 )); //abs(lambda1 - lambda2);

    double iint_sum = 0.;
    if (  phi > b1 && phi < b2 && lam > l1 && lam < l2  ) {
        my_vector  philist, lambdalist;

        philist[0]    = b1;    philist[1] = phi;       philist[2] = b2;
        lambdalist[0] = l1; lambdalist[1] = lam;    lambdalist[2] = l2;

        for ( int i = 0; i < philist.size() - 1; i++) {
          for ( int j = 0; j < lambdalist.size() -1; j++) {
              iint_sum += Tessquad::int2d( gravfxy , philist[i], philist[i+1], lambdalist[j], lambdalist[j+1], r0, rho, kappa, tess_ratio  );
          }
        }
    } else if ( ((l < tess_ratio * d_phi )    ||
                (l < tess_ratio * d_lambda )) && d_phi*d_lambda >= 1.0e+03 ) {

        my_vector  philist, lambdalist;

        if ( l < tess_ratio * d_phi ) {
            philist[0] = ( b1 );
            philist[1] = ((b1+b2)/2.);
            philist[2] = ( b2 );
        } else {
            philist[0] =( b1 ); philist[1] = (b2);
        }

        if ( l < tess_ratio* d_lambda ) {
            lambdalist[0] = ( l1 );
            lambdalist[1] = ( (l1+l2 )/2.);
            lambdalist[2] = ( l2 );
        } else {
            lambdalist[0] =( l1 ); lambdalist[1] = ( l2 );
        }

        iint_sum = 0.0;
        for ( int j = 0; j < philist.size() -1; j++) {
            for ( int k = 0; k < lambdalist.size() -1 ; k++) {
                iint_sum += Tessquad::int2d( gravfxy , philist[j], philist[j+1],
                                                         lambdalist[k], lambdalist[k+1], r0, rho, kappa, tess_ratio);
                        }
        }
    } else {
        double db12_2 = (b2-b1)/2.;
        double dl12_2 = (l2-l1)/2.;

        for (unsigned i = 0; i < nw.n_rows; i++) {
            double phi_node = arma::as_scalar( nw(i,0) );
            double phi_weight = arma::as_scalar( nw( i,1) );
            for (unsigned j = 0 ; j< nw.n_rows; j++ ) {
                double lam_node = arma::as_scalar( nw(j,0) );
                double lam_weight = arma::as_scalar( nw( j,1) );

                iint_sum += lam_weight*phi_weight*(this->*gravfxy)( r0 ,  b1 + db12_2 * (1.+phi_node) , l1 + dl12_2 * (1.+lam_node) );
            }
        }
        iint_sum *= db12_2 * dl12_2;
    }
    return iint_sum*rho*kappa;
}

double Tessquad::int3d(double (Tessquad::*gravfxyz)(double, double,double), double r1, double r2,
                                                                            double b1, double b2,
                                                                            double l1, double l2,
                                                                            double rho, double kappa, double tess_ratio)
{
    double iiint_sum = 0.;
    double r0 = (r1 + r2)/2.;
    double b0 = (b1 + b2)/2.;
    double l0 = (l1 + l2)/2.;

    double cos_psi = sin(b0)*sin(phi) + cos(phi)*cos(b0)*cos(l0 - lam);
    double l = sqrt(r*r+r0*r0-2.*r*r0*cos_psi );

    /* size of the tesseroid */
    double d_r = abs(r1 - r2);
    double d_phi = r2*acos( sin(b1)*sin(b2)+cos(b1)*cos(b2)  ) ; //abs(phi1 - phi2);
    double d_lambda = r2*acos( sin(b0)*sin(b0) + cos(b0)*cos(b0)*cos( l2 - l1 )); //abs(lambda1 - lambda2);

    if (  phi > b1 && phi < b2 && lam > l1 && lam < l2 && r > r1 && r < r2  ) {
        my_vector  rlist, philist, lambdalist;

        rlist[0]      = r1;      rlist[1] = r;           rlist[2] = r2;
        philist[0]    = b1;    philist[1] = phi;       philist[2] = b2;
        lambdalist[0] = l1; lambdalist[1] = lam;    lambdalist[2] = l2;

        iiint_sum = 0.0;
        for ( int k = 0; k < rlist.size() -1; k++ ) {
            for ( int i = 0; i < philist.size() - 1; i++) {
              for ( int j = 0; j < lambdalist.size() -1; j++) {
                  iiint_sum += Tessquad::int3d( gravfxyz ,rlist[k], rlist[k+1] ,
                                                philist[i], philist[i+1],
                                                lambdalist[j], lambdalist[j+1], rho, kappa, tess_ratio);
              }
            }
        }
    } else if (((l < tess_ratio * d_phi )    ||
                (l < tess_ratio * d_lambda ) ||
                (l < tess_ratio * d_r) ) && ( d_phi * d_lambda * d_r >= 1.0e+04 ) )  {

//        cout << "Splitting: " << num_of_splits++ << ", "
//            << setw(18) << setprecision(12) << l << " << " << d_phi*tess_ratio
//            << " << " << d_lambda * tess_ratio << " << " << d_r * tess_ratio << endl;

        my_vector rlist, philist, lambdalist;
        if ( l < tess_ratio *d_r) {
            rlist[0] = r1 ; rlist[1] = (r1+r2)/2. ; rlist[2] = r2 ;
        } else {
            rlist[0] = r1 ; rlist[1] = r2;
        }

        if ( l < tess_ratio * d_phi ) {
            philist[0] = ( b1 );
            philist[1] = ((b1+b2)/2.);
            philist[2] = ( b2 );
        } else {
            philist[0] = b1 ; philist[1] = b2;
        }

        if ( l < tess_ratio* d_lambda ) {
            lambdalist[0] = ( l1 );
            lambdalist[1] = ( (l1+l2 )/2.);
            lambdalist[2] = ( l2 );
        } else {
            lambdalist[0] = l1 ; lambdalist[1] = l2 ;
        }

        iiint_sum = 0.0;
        for ( int i = 0; i < rlist.size() - 1; i++) {
          for ( int j = 0; j < philist.size() -1; j++) {
            for ( int k = 0; k < lambdalist.size() -1 ; k++) {
                iiint_sum += Tessquad::int3d( gravfxyz ,rlist[i], rlist[i+1] ,
                                              philist[j], philist[j+1],
                                              lambdalist[k], lambdalist[k+1], rho,kappa, tess_ratio);
            }
          }
        }
    } else {
        double db12_2 = (b2-b1)/2.;
        double dl12_2 = (l2-l1)/2.;
        double dr12_2 = (r2-r1)/2.;

        for (unsigned i = 0; i < nw.n_rows; i++) {
            double phi_node = arma::as_scalar( nw(i,0) );
            double phi_weight = arma::as_scalar( nw( i,1) );
            for (unsigned j = 0 ; j< nw.n_rows; j++ ) {
                double lam_node = arma::as_scalar( nw(j,0) );
                double lam_weight = arma::as_scalar( nw( j,1) );
                for ( unsigned k = 0; k < nw.n_rows; k++ ) {
                    double r_node = arma::as_scalar( nw(k,0) );
                    double r_weight = arma::as_scalar( nw( k,1) );

                    iiint_sum += r_weight*lam_weight*phi_weight *(this->*gravfxyz)( r1 + dr12_2 * (1.*r_node),
                                                                                  b1 + db12_2 * (1.+phi_node) ,
                                                                                  l1 + dl12_2 * (1.+lam_node) );
                }
            }
        }
        iiint_sum *= db12_2 * dl12_2 * dr12_2 * rho * kappa ;
    }
    return iiint_sum;
}

Tessquad::Tessquad(double c_r , double c_brad, double c_lrad,/* double c_hel,*/unsigned n )
{
    phi = c_brad;
    lam = c_lrad;
    //hel = c_hel;
    r = c_r;

    if ( n == 2 ) {
    nw  << -0.5773502691896257 << 1.0<< arma::endr
     << 0.5773502691896257 << 1.0 << arma::endr;
    } else if ( n ==3 ) {
    nw  << 0.0 << 0.8888888888888888<< arma::endr
     << -0.7745966692414834 << 0.5555555555555556<< arma::endr
     << 0.7745966692414834 << 0.5555555555555556 << arma::endr;
    } else if ( n ==4 ) {
    nw  << -0.3399810435848563 << 0.6521451548625461<< arma::endr
     << 0.3399810435848563 << 0.6521451548625461<< arma::endr
     << -0.8611363115940526 << 0.3478548451374538<< arma::endr
     << 0.8611363115940526 << 0.3478548451374538 << arma::endr;
    } else if ( n ==5 ) {
    nw  << 0.0 << 0.5688888888888889<< arma::endr
     << -0.5384693101056831 << 0.4786286704993665<< arma::endr
     << 0.5384693101056831 << 0.4786286704993665<< arma::endr
     << -0.906179845938664 << 0.2369268850561891<< arma::endr
     << 0.906179845938664 << 0.2369268850561891 << arma::endr;
    } else if ( n ==6 ) {
    nw  << 0.6612093864662645 << 0.3607615730481386<< arma::endr
     << -0.6612093864662645 << 0.3607615730481386<< arma::endr
     << -0.2386191860831969 << 0.467913934572691<< arma::endr
     << 0.2386191860831969 << 0.467913934572691<< arma::endr
     << -0.932469514203152 << 0.1713244923791704<< arma::endr
     << 0.932469514203152 << 0.1713244923791704 << arma::endr;
    } else if ( n ==7 ) {
    nw  << 0.0 << 0.4179591836734694<< arma::endr
     << 0.4058451513773972 << 0.3818300505051189<< arma::endr
     << -0.4058451513773972 << 0.3818300505051189<< arma::endr
     << -0.7415311855993945 << 0.2797053914892766<< arma::endr
     << 0.7415311855993945 << 0.2797053914892766<< arma::endr
     << -0.9491079123427585 << 0.1294849661688697<< arma::endr
     << 0.9491079123427585 << 0.1294849661688697 << arma::endr;
    } else if ( n ==8 ) {
    nw  << -0.1834346424956498 << 0.362683783378362<< arma::endr
     << 0.1834346424956498 << 0.362683783378362<< arma::endr
     << -0.525532409916329 << 0.3137066458778873<< arma::endr
     << 0.525532409916329 << 0.3137066458778873<< arma::endr
     << -0.7966664774136267 << 0.2223810344533745<< arma::endr
     << 0.7966664774136267 << 0.2223810344533745<< arma::endr
     << -0.9602898564975363 << 0.1012285362903763<< arma::endr
     << 0.9602898564975363 << 0.1012285362903763 << arma::endr;
    } else if ( n ==9 ) {
    nw  << 0.0 << 0.3302393550012598<< arma::endr
     << -0.8360311073266358 << 0.1806481606948574<< arma::endr
     << 0.8360311073266358 << 0.1806481606948574<< arma::endr
     << -0.9681602395076261 << 0.0812743883615744<< arma::endr
     << 0.9681602395076261 << 0.0812743883615744<< arma::endr
     << -0.3242534234038089 << 0.3123470770400029<< arma::endr
     << 0.3242534234038089 << 0.3123470770400029<< arma::endr
     << -0.6133714327005904 << 0.2606106964029354<< arma::endr
     << 0.6133714327005904 << 0.2606106964029354 << arma::endr;
    } else if ( n ==10 ) {
    nw  << -0.1488743389816312 << 0.2955242247147529<< arma::endr
     << 0.1488743389816312 << 0.2955242247147529<< arma::endr
     << -0.4333953941292472 << 0.2692667193099963<< arma::endr
     << 0.4333953941292472 << 0.2692667193099963<< arma::endr
     << -0.6794095682990244 << 0.219086362515982<< arma::endr
     << 0.6794095682990244 << 0.219086362515982<< arma::endr
     << -0.8650633666889845 << 0.1494513491505806<< arma::endr
     << 0.8650633666889845 << 0.1494513491505806<< arma::endr
     << -0.9739065285171717 << 0.0666713443086881<< arma::endr
     << 0.9739065285171717 << 0.0666713443086881 << arma::endr;
    } else if ( n ==11 ) {
    nw  << 0.0 << 0.2729250867779006<< arma::endr
     << -0.269543155952345 << 0.2628045445102467<< arma::endr
     << 0.269543155952345 << 0.2628045445102467<< arma::endr
     << -0.5190961292068118 << 0.2331937645919905<< arma::endr
     << 0.5190961292068118 << 0.2331937645919905<< arma::endr
     << -0.7301520055740494 << 0.1862902109277343<< arma::endr
     << 0.7301520055740494 << 0.1862902109277343<< arma::endr
     << -0.8870625997680953 << 0.1255803694649046<< arma::endr
     << 0.8870625997680953 << 0.1255803694649046<< arma::endr
     << -0.978228658146057 << 0.0556685671161737<< arma::endr
     << 0.978228658146057 << 0.0556685671161737 << arma::endr;
    } else if ( n ==12 ) {
    nw  << -0.1252334085114689 << 0.2491470458134028<< arma::endr
     << 0.1252334085114689 << 0.2491470458134028<< arma::endr
     << -0.3678314989981802 << 0.2334925365383548<< arma::endr
     << 0.3678314989981802 << 0.2334925365383548<< arma::endr
     << -0.5873179542866175 << 0.2031674267230659<< arma::endr
     << 0.5873179542866175 << 0.2031674267230659<< arma::endr
     << -0.7699026741943047 << 0.1600783285433462<< arma::endr
     << 0.7699026741943047 << 0.1600783285433462<< arma::endr
     << -0.9041172563704749 << 0.1069393259953184<< arma::endr
     << 0.9041172563704749 << 0.1069393259953184<< arma::endr
     << -0.9815606342467192 << 0.0471753363865118<< arma::endr
     << 0.9815606342467192 << 0.0471753363865118 << arma::endr;
    } else if ( n ==13 ) {
    nw  << 0.0 << 0.2325515532308739<< arma::endr
     << -0.2304583159551348 << 0.2262831802628972<< arma::endr
     << 0.2304583159551348 << 0.2262831802628972<< arma::endr
     << -0.4484927510364469 << 0.2078160475368885<< arma::endr
     << 0.4484927510364469 << 0.2078160475368885<< arma::endr
     << -0.6423493394403402 << 0.1781459807619457<< arma::endr
     << 0.6423493394403402 << 0.1781459807619457<< arma::endr
     << -0.8015780907333099 << 0.1388735102197872<< arma::endr
     << 0.8015780907333099 << 0.1388735102197872<< arma::endr
     << -0.9175983992229779 << 0.0921214998377285<< arma::endr
     << 0.9175983992229779 << 0.0921214998377285<< arma::endr
     << -0.9841830547185881 << 0.0404840047653159<< arma::endr
     << 0.9841830547185881 << 0.0404840047653159 << arma::endr;
    } else if ( n ==14 ) {
    nw  << -0.1080549487073437 << 0.2152638534631578<< arma::endr
     << 0.1080549487073437 << 0.2152638534631578<< arma::endr
     << -0.3191123689278897 << 0.2051984637212956<< arma::endr
     << 0.3191123689278897 << 0.2051984637212956<< arma::endr
     << -0.5152486363581541 << 0.1855383974779378<< arma::endr
     << 0.5152486363581541 << 0.1855383974779378<< arma::endr
     << -0.6872929048116855 << 0.1572031671581935<< arma::endr
     << 0.6872929048116855 << 0.1572031671581935<< arma::endr
     << -0.827201315069765 << 0.1215185706879032<< arma::endr
     << 0.827201315069765 << 0.1215185706879032<< arma::endr
     << -0.9284348836635735 << 0.0801580871597602<< arma::endr
     << 0.9284348836635735 << 0.0801580871597602<< arma::endr
     << -0.9862838086968123 << 0.0351194603317519<< arma::endr
     << 0.9862838086968123 << 0.0351194603317519 << arma::endr;
    } else if ( n ==15 ) {
    nw  << 0.0 << 0.2025782419255613<< arma::endr
     << -0.2011940939974345 << 0.1984314853271116<< arma::endr
     << 0.2011940939974345 << 0.1984314853271116<< arma::endr
     << -0.3941513470775634 << 0.1861610000155622<< arma::endr
     << 0.3941513470775634 << 0.1861610000155622<< arma::endr
     << -0.5709721726085388 << 0.1662692058169939<< arma::endr
     << 0.5709721726085388 << 0.1662692058169939<< arma::endr
     << -0.7244177313601701 << 0.1395706779261543<< arma::endr
     << 0.7244177313601701 << 0.1395706779261543<< arma::endr
     << -0.8482065834104272 << 0.1071592204671719<< arma::endr
     << 0.8482065834104272 << 0.1071592204671719<< arma::endr
     << -0.937273392400706 << 0.0703660474881081<< arma::endr
     << 0.937273392400706 << 0.0703660474881081<< arma::endr
     << -0.9879925180204854 << 0.0307532419961173<< arma::endr
     << 0.9879925180204854 << 0.0307532419961173 << arma::endr;
    } else if ( n ==16 ) {
    nw  << -0.0950125098376374 << 0.1894506104550685<< arma::endr
     << 0.0950125098376374 << 0.1894506104550685<< arma::endr
     << -0.2816035507792589 << 0.1826034150449236<< arma::endr
     << 0.2816035507792589 << 0.1826034150449236<< arma::endr
     << -0.4580167776572274 << 0.1691565193950025<< arma::endr
     << 0.4580167776572274 << 0.1691565193950025<< arma::endr
     << -0.6178762444026438 << 0.1495959888165767<< arma::endr
     << 0.6178762444026438 << 0.1495959888165767<< arma::endr
     << -0.755404408355003 << 0.1246289712555339<< arma::endr
     << 0.755404408355003 << 0.1246289712555339<< arma::endr
     << -0.8656312023878318 << 0.0951585116824928<< arma::endr
     << 0.8656312023878318 << 0.0951585116824928<< arma::endr
     << -0.9445750230732326 << 0.0622535239386479<< arma::endr
     << 0.9445750230732326 << 0.0622535239386479<< arma::endr
     << -0.9894009349916499 << 0.0271524594117541<< arma::endr
     << 0.9894009349916499 << 0.0271524594117541 << arma::endr;
    } else if ( n ==17 ) {
    nw  << 0.0 << 0.1794464703562065<< arma::endr
     << -0.1784841814958479 << 0.1765627053669926<< arma::endr
     << 0.1784841814958479 << 0.1765627053669926<< arma::endr
     << -0.3512317634538763 << 0.16800410215645<< arma::endr
     << 0.3512317634538763 << 0.16800410215645<< arma::endr
     << -0.5126905370864769 << 0.1540457610768103<< arma::endr
     << 0.5126905370864769 << 0.1540457610768103<< arma::endr
     << -0.6576711592166907 << 0.1351363684685255<< arma::endr
     << 0.6576711592166907 << 0.1351363684685255<< arma::endr
     << -0.7815140038968014 << 0.111883847193404<< arma::endr
     << 0.7815140038968014 << 0.111883847193404<< arma::endr
     << -0.8802391537269859 << 0.0850361483171792<< arma::endr
     << 0.8802391537269859 << 0.0850361483171792<< arma::endr
     << -0.9506755217687678 << 0.0554595293739872<< arma::endr
     << 0.9506755217687678 << 0.0554595293739872<< arma::endr
     << -0.9905754753144174 << 0.0241483028685479<< arma::endr
     << 0.9905754753144174 << 0.0241483028685479 << arma::endr;
    } else if ( n ==18 ) {
    nw  << -0.0847750130417353 << 0.1691423829631436<< arma::endr
     << 0.0847750130417353 << 0.1691423829631436<< arma::endr
     << -0.2518862256915055 << 0.1642764837458327<< arma::endr
     << 0.2518862256915055 << 0.1642764837458327<< arma::endr
     << -0.4117511614628426 << 0.1546846751262652<< arma::endr
     << 0.4117511614628426 << 0.1546846751262652<< arma::endr
     << -0.5597708310739475 << 0.1406429146706507<< arma::endr
     << 0.5597708310739475 << 0.1406429146706507<< arma::endr
     << -0.6916870430603532 << 0.1225552067114785<< arma::endr
     << 0.6916870430603532 << 0.1225552067114785<< arma::endr
     << -0.8037049589725231 << 0.1009420441062872<< arma::endr
     << 0.8037049589725231 << 0.1009420441062872<< arma::endr
     << -0.8926024664975557 << 0.0764257302548891<< arma::endr
     << 0.8926024664975557 << 0.0764257302548891<< arma::endr
     << -0.9558239495713977 << 0.0497145488949698<< arma::endr
     << 0.9558239495713977 << 0.0497145488949698<< arma::endr
     << -0.9915651684209309 << 0.0216160135264833<< arma::endr
     << 0.9915651684209309 << 0.0216160135264833 << arma::endr;
    } else if ( n ==19 ) {
    nw  << 0.0 << 0.1610544498487837<< arma::endr
     << -0.1603586456402254 << 0.1589688433939543<< arma::endr
     << 0.1603586456402254 << 0.1589688433939543<< arma::endr
     << -0.3165640999636298 << 0.1527660420658597<< arma::endr
     << 0.3165640999636298 << 0.1527660420658597<< arma::endr
     << -0.4645707413759609 << 0.1426067021736066<< arma::endr
     << 0.4645707413759609 << 0.1426067021736066<< arma::endr
     << -0.600545304661681 << 0.1287539625393362<< arma::endr
     << 0.600545304661681 << 0.1287539625393362<< arma::endr
     << -0.7209661773352294 << 0.111566645547334<< arma::endr
     << 0.7209661773352294 << 0.111566645547334<< arma::endr
     << -0.8227146565371428 << 0.09149002162245<< arma::endr
     << 0.8227146565371428 << 0.09149002162245<< arma::endr
     << -0.9031559036148179 << 0.0690445427376412<< arma::endr
     << 0.9031559036148179 << 0.0690445427376412<< arma::endr
     << -0.96020815213483 << 0.0448142267656996<< arma::endr
     << 0.96020815213483 << 0.0448142267656996<< arma::endr
     << -0.9924068438435844 << 0.0194617882297265<< arma::endr
     << 0.9924068438435844 << 0.0194617882297265 << arma::endr;
    } else if ( n ==20 ) {
    nw  << -0.0765265211334973 << 0.1527533871307258<< arma::endr
     << 0.0765265211334973 << 0.1527533871307258<< arma::endr
     << -0.2277858511416451 << 0.1491729864726037<< arma::endr
     << 0.2277858511416451 << 0.1491729864726037<< arma::endr
     << -0.3737060887154195 << 0.142096109318382<< arma::endr
     << 0.3737060887154195 << 0.142096109318382<< arma::endr
     << -0.5108670019508271 << 0.1316886384491766<< arma::endr
     << 0.5108670019508271 << 0.1316886384491766<< arma::endr
     << -0.636053680726515 << 0.1181945319615184<< arma::endr
     << 0.636053680726515 << 0.1181945319615184<< arma::endr
     << -0.7463319064601508 << 0.1019301198172404<< arma::endr
     << 0.7463319064601508 << 0.1019301198172404<< arma::endr
     << -0.8391169718222188 << 0.0832767415767048<< arma::endr
     << 0.8391169718222188 << 0.0832767415767048<< arma::endr
     << -0.912234428251326 << 0.0626720483341091<< arma::endr
     << 0.912234428251326 << 0.0626720483341091<< arma::endr
     << -0.9639719272779138 << 0.0406014298003869<< arma::endr
     << 0.9639719272779138 << 0.0406014298003869<< arma::endr
     << -0.9931285991850949 << 0.0176140071391521<< arma::endr
     << 0.9931285991850949 << 0.0176140071391521 << arma::endr;
    } else if ( n ==21 ) {
    nw  << 0.0 << 0.1460811336496904<< arma::endr
     << -0.1455618541608951 << 0.14452440398997<< arma::endr
     << 0.1455618541608951 << 0.14452440398997<< arma::endr
     << -0.2880213168024011 << 0.1398873947910731<< arma::endr
     << 0.2880213168024011 << 0.1398873947910731<< arma::endr
     << -0.4243421202074388 << 0.1322689386333375<< arma::endr
     << 0.4243421202074388 << 0.1322689386333375<< arma::endr
     << -0.5516188358872198 << 0.1218314160537285<< arma::endr
     << 0.5516188358872198 << 0.1218314160537285<< arma::endr
     << -0.6671388041974123 << 0.1087972991671484<< arma::endr
     << 0.6671388041974123 << 0.1087972991671484<< arma::endr
     << -0.7684399634756779 << 0.0934444234560339<< arma::endr
     << 0.7684399634756779 << 0.0934444234560339<< arma::endr
     << -0.8533633645833173 << 0.0761001136283793<< arma::endr
     << 0.8533633645833173 << 0.0761001136283793<< arma::endr
     << -0.9200993341504008 << 0.0571344254268572<< arma::endr
     << 0.9200993341504008 << 0.0571344254268572<< arma::endr
     << -0.9672268385663063 << 0.0369537897708525<< arma::endr
     << 0.9672268385663063 << 0.0369537897708525<< arma::endr
     << -0.9937521706203895 << 0.0160172282577743<< arma::endr
     << 0.9937521706203895 << 0.0160172282577743 << arma::endr;
    } else if ( n ==22 ) {
    nw  << -0.0697392733197222 << 0.139251872855632<< arma::endr
     << 0.0697392733197222 << 0.139251872855632<< arma::endr
     << -0.2078604266882213 << 0.1365414983460152<< arma::endr
     << 0.2078604266882213 << 0.1365414983460152<< arma::endr
     << -0.3419358208920842 << 0.1311735047870624<< arma::endr
     << 0.3419358208920842 << 0.1311735047870624<< arma::endr
     << -0.469355837986757 << 0.1232523768105124<< arma::endr
     << 0.469355837986757 << 0.1232523768105124<< arma::endr
     << -0.5876404035069116 << 0.1129322960805392<< arma::endr
     << 0.5876404035069116 << 0.1129322960805392<< arma::endr
     << -0.6944872631866827 << 0.100414144442881<< arma::endr
     << 0.6944872631866827 << 0.100414144442881<< arma::endr
     << -0.7878168059792081 << 0.0859416062170677<< arma::endr
     << 0.7878168059792081 << 0.0859416062170677<< arma::endr
     << -0.8658125777203002 << 0.0697964684245205<< arma::endr
     << 0.8658125777203002 << 0.0697964684245205<< arma::endr
     << -0.926956772187174 << 0.0522933351526833<< arma::endr
     << 0.926956772187174 << 0.0522933351526833<< arma::endr
     << -0.9700604978354287 << 0.0337749015848142<< arma::endr
     << 0.9700604978354287 << 0.0337749015848142<< arma::endr
     << -0.9942945854823992 << 0.0146279952982722<< arma::endr
     << 0.9942945854823992 << 0.0146279952982722 << arma::endr;
    } else if ( n ==23 ) {
    nw  << 0.0 << 0.1336545721861062<< arma::endr
     << -0.1332568242984661 << 0.1324620394046966<< arma::endr
     << 0.1332568242984661 << 0.1324620394046966<< arma::endr
     << -0.264135680970345 << 0.1289057221880822<< arma::endr
     << 0.264135680970345 << 0.1289057221880822<< arma::endr
     << -0.3903010380302908 << 0.1230490843067295<< arma::endr
     << 0.3903010380302908 << 0.1230490843067295<< arma::endr
     << -0.5095014778460075 << 0.1149966402224114<< arma::endr
     << 0.5095014778460075 << 0.1149966402224114<< arma::endr
     << -0.6196098757636461 << 0.1048920914645414<< arma::endr
     << 0.6196098757636461 << 0.1048920914645414<< arma::endr
     << -0.7186613631319502 << 0.0929157660600352<< arma::endr
     << 0.7186613631319502 << 0.0929157660600352<< arma::endr
     << -0.8048884016188399 << 0.0792814117767189<< arma::endr
     << 0.8048884016188399 << 0.0792814117767189<< arma::endr
     << -0.8767523582704416 << 0.0642324214085258<< arma::endr
     << 0.8767523582704416 << 0.0642324214085258<< arma::endr
     << -0.9329710868260161 << 0.0480376717310847<< arma::endr
     << 0.9329710868260161 << 0.0480376717310847<< arma::endr
     << -0.9725424712181152 << 0.0309880058569794<< arma::endr
     << 0.9725424712181152 << 0.0309880058569794<< arma::endr
     << -0.9947693349975522 << 0.0134118594871418<< arma::endr
     << 0.9947693349975522 << 0.0134118594871418 << arma::endr;
    } else if ( n ==24 ) {
    nw  << -0.0640568928626056 << 0.1279381953467522<< arma::endr
     << 0.0640568928626056 << 0.1279381953467522<< arma::endr
     << -0.1911188674736163 << 0.1258374563468283<< arma::endr
     << 0.1911188674736163 << 0.1258374563468283<< arma::endr
     << -0.3150426796961634 << 0.1216704729278034<< arma::endr
     << 0.3150426796961634 << 0.1216704729278034<< arma::endr
     << -0.4337935076260451 << 0.1155056680537256<< arma::endr
     << 0.4337935076260451 << 0.1155056680537256<< arma::endr
     << -0.5454214713888396 << 0.1074442701159656<< arma::endr
     << 0.5454214713888396 << 0.1074442701159656<< arma::endr
     << -0.6480936519369755 << 0.0976186521041139<< arma::endr
     << 0.6480936519369755 << 0.0976186521041139<< arma::endr
     << -0.7401241915785544 << 0.0861901615319533<< arma::endr
     << 0.7401241915785544 << 0.0861901615319533<< arma::endr
     << -0.820001985973903 << 0.0733464814110803<< arma::endr
     << 0.820001985973903 << 0.0733464814110803<< arma::endr
     << -0.8864155270044011 << 0.0592985849154368<< arma::endr
     << 0.8864155270044011 << 0.0592985849154368<< arma::endr
     << -0.9382745520027328 << 0.0442774388174198<< arma::endr
     << 0.9382745520027328 << 0.0442774388174198<< arma::endr
     << -0.9747285559713095 << 0.0285313886289337<< arma::endr
     << 0.9747285559713095 << 0.0285313886289337<< arma::endr
     << -0.9951872199970213 << 0.0123412297999872<< arma::endr
     << 0.9951872199970213 << 0.0123412297999872 << arma::endr;
    } else if ( n ==25 ) {
    nw  << 0.0 << 0.1231760537267154<< arma::endr
     << -0.1228646926107104 << 0.12224244299031<< arma::endr
     << 0.1228646926107104 << 0.12224244299031<< arma::endr
     << -0.2438668837209884 << 0.1194557635357848<< arma::endr
     << 0.2438668837209884 << 0.1194557635357848<< arma::endr
     << -0.3611723058093879 << 0.1148582591457116<< arma::endr
     << 0.3611723058093879 << 0.1148582591457116<< arma::endr
     << -0.473002731445715 << 0.1085196244742637<< arma::endr
     << 0.473002731445715 << 0.1085196244742637<< arma::endr
     << -0.577662930241223 << 0.1005359490670506<< arma::endr
     << 0.577662930241223 << 0.1005359490670506<< arma::endr
     << -0.6735663684734684 << 0.0910282619829637<< arma::endr
     << 0.6735663684734684 << 0.0910282619829637<< arma::endr
     << -0.7592592630373576 << 0.080140700335001<< arma::endr
     << 0.7592592630373576 << 0.080140700335001<< arma::endr
     << -0.833442628760834 << 0.0680383338123569<< arma::endr
     << 0.833442628760834 << 0.0680383338123569<< arma::endr
     << -0.8949919978782753 << 0.0549046959758352<< arma::endr
     << 0.8949919978782753 << 0.0549046959758352<< arma::endr
     << -0.9429745712289743 << 0.0409391567013063<< arma::endr
     << 0.9429745712289743 << 0.0409391567013063<< arma::endr
     << -0.9766639214595175 << 0.0263549866150321<< arma::endr
     << 0.9766639214595175 << 0.0263549866150321<< arma::endr
     << -0.9955569697904981 << 0.0113937985010263<< arma::endr
     << 0.9955569697904981 << 0.0113937985010263 << arma::endr;
    } else if ( n ==26 ) {
    nw  << -0.0592300934293132 << 0.1183214152792623<< arma::endr
     << 0.0592300934293132 << 0.1183214152792623<< arma::endr
     << -0.1768588203568902 << 0.1166604434852966<< arma::endr
     << 0.1768588203568902 << 0.1166604434852966<< arma::endr
     << -0.2920048394859569 << 0.1133618165463197<< arma::endr
     << 0.2920048394859569 << 0.1133618165463197<< arma::endr
     << -0.4030517551234863 << 0.1084718405285766<< arma::endr
     << 0.4030517551234863 << 0.1084718405285766<< arma::endr
     << -0.5084407148245057 << 0.1020591610944254<< arma::endr
     << 0.5084407148245057 << 0.1020591610944254<< arma::endr
     << -0.6066922930176181 << 0.0942138003559141<< arma::endr
     << 0.6066922930176181 << 0.0942138003559141<< arma::endr
     << -0.6964272604199573 << 0.0850458943134852<< arma::endr
     << 0.6964272604199573 << 0.0850458943134852<< arma::endr
     << -0.7763859488206789 << 0.0746841497656597<< arma::endr
     << 0.7763859488206789 << 0.0746841497656597<< arma::endr
     << -0.845445942788498 << 0.0632740463295748<< arma::endr
     << 0.845445942788498 << 0.0632740463295748<< arma::endr
     << -0.9026378619843071 << 0.0509758252971478<< arma::endr
     << 0.9026378619843071 << 0.0509758252971478<< arma::endr
     << -0.9471590666617142 << 0.0379623832943628<< arma::endr
     << 0.9471590666617142 << 0.0379623832943628<< arma::endr
     << -0.978385445956471 << 0.0244178510926319<< arma::endr
     << 0.978385445956471 << 0.0244178510926319<< arma::endr
     << -0.9958857011456169 << 0.010551372617343<< arma::endr
     << 0.9958857011456169 << 0.010551372617343 << arma::endr;
    } else if ( n ==27 ) {
    nw  << 0.0 << 0.114220867378957<< arma::endr
     << -0.11397258560953 << 0.1134763461089651<< arma::endr
     << 0.11397258560953 << 0.1134763461089651<< arma::endr
     << -0.2264593654395368 << 0.1112524883568452<< arma::endr
     << 0.2264593654395368 << 0.1112524883568452<< arma::endr
     << -0.3359939036385089 << 0.1075782857885332<< arma::endr
     << 0.3359939036385089 << 0.1075782857885332<< arma::endr
     << -0.4411482517500269 << 0.1025016378177458<< arma::endr
     << 0.4411482517500269 << 0.1025016378177458<< arma::endr
     << -0.5405515645794569 << 0.0960887273700285<< arma::endr
     << 0.5405515645794569 << 0.0960887273700285<< arma::endr
     << -0.6329079719464952 << 0.0884231585437569<< arma::endr
     << 0.6329079719464952 << 0.0884231585437569<< arma::endr
     << -0.7170134737394237 << 0.0796048677730578<< arma::endr
     << 0.7170134737394237 << 0.0796048677730578<< arma::endr
     << -0.7917716390705082 << 0.0697488237662456<< arma::endr
     << 0.7917716390705082 << 0.0697488237662456<< arma::endr
     << -0.8562079080182945 << 0.0589835368598336<< arma::endr
     << 0.8562079080182945 << 0.0589835368598336<< arma::endr
     << -0.9094823206774911 << 0.0474494125206151<< arma::endr
     << 0.9094823206774911 << 0.0474494125206151<< arma::endr
     << -0.9509005578147051 << 0.0352970537574197<< arma::endr
     << 0.9509005578147051 << 0.0352970537574197<< arma::endr
     << -0.9799234759615012 << 0.0226862315961806<< arma::endr
     << 0.9799234759615012 << 0.0226862315961806<< arma::endr
     << -0.9961792628889886 << 0.0097989960512944<< arma::endr
     << 0.9961792628889886 << 0.0097989960512944 << arma::endr;
    } else if ( n ==28 ) {
    nw  << -0.0550792898840343 << 0.1100470130164752<< arma::endr
     << 0.0550792898840343 << 0.1100470130164752<< arma::endr
     << -0.1645692821333808 << 0.1087111922582941<< arma::endr
     << 0.1645692821333808 << 0.1087111922582941<< arma::endr
     << -0.2720616276351781 << 0.1060557659228464<< arma::endr
     << 0.2720616276351781 << 0.1060557659228464<< arma::endr
     << -0.3762515160890787 << 0.1021129675780608<< arma::endr
     << 0.3762515160890787 << 0.1021129675780608<< arma::endr
     << -0.4758742249551183 << 0.0969306579979299<< arma::endr
     << 0.4758742249551183 << 0.0969306579979299<< arma::endr
     << -0.5697204718114017 << 0.0905717443930328<< arma::endr
     << 0.5697204718114017 << 0.0905717443930328<< arma::endr
     << -0.656651094038865 << 0.0831134172289012<< arma::endr
     << 0.656651094038865 << 0.0831134172289012<< arma::endr
     << -0.7356108780136318 << 0.0746462142345688<< arma::endr
     << 0.7356108780136318 << 0.0746462142345688<< arma::endr
     << -0.8056413709171791 << 0.0652729239669996<< arma::endr
     << 0.8056413709171791 << 0.0652729239669996<< arma::endr
     << -0.8658925225743951 << 0.0551073456757167<< arma::endr
     << 0.8658925225743951 << 0.0551073456757167<< arma::endr
     << -0.9156330263921321 << 0.0442729347590042<< arma::endr
     << 0.9156330263921321 << 0.0442729347590042<< arma::endr
     << -0.9542592806289382 << 0.0329014277823044<< arma::endr
     << 0.9542592806289382 << 0.0329014277823044<< arma::endr
     << -0.9813031653708727 << 0.0211321125927713<< arma::endr
     << 0.9813031653708727 << 0.0211321125927713<< arma::endr
     << -0.9964424975739544 << 0.0091242825930945<< arma::endr
     << 0.9964424975739544 << 0.0091242825930945 << arma::endr;
    } else if ( n ==29 ) {
    nw  << 0.0 << 0.1064793817183142<< arma::endr
     << -0.1062782301326792 << 0.1058761550973209<< arma::endr
     << 0.1062782301326792 << 0.1058761550973209<< arma::endr
     << -0.2113522861660011 << 0.1040733100777294<< arma::endr
     << 0.2113522861660011 << 0.1040733100777294<< arma::endr
     << -0.3140316378676399 << 0.101091273759915<< arma::endr
     << 0.3140316378676399 << 0.101091273759915<< arma::endr
     << -0.4131528881740086 << 0.0969638340944086<< arma::endr
     << 0.4131528881740086 << 0.0969638340944086<< arma::endr
     << -0.5075929551242276 << 0.0917377571392588<< arma::endr
     << 0.5075929551242276 << 0.0917377571392588<< arma::endr
     << -0.5962817971382278 << 0.0854722573661725<< arma::endr
     << 0.5962817971382278 << 0.0854722573661725<< arma::endr
     << -0.6782145376026865 << 0.0782383271357638<< arma::endr
     << 0.6782145376026865 << 0.0782383271357638<< arma::endr
     << -0.7524628517344771 << 0.0701179332550513<< arma::endr
     << 0.7524628517344771 << 0.0701179332550513<< arma::endr
     << -0.8181854876152524 << 0.0612030906570791<< arma::endr
     << 0.8181854876152524 << 0.0612030906570791<< arma::endr
     << -0.8746378049201028 << 0.0515948269024979<< arma::endr
     << 0.8746378049201028 << 0.0515948269024979<< arma::endr
     << -0.9211802329530587 << 0.0414020625186828<< arma::endr
     << 0.9211802329530587 << 0.0414020625186828<< arma::endr
     << -0.9572855957780877 << 0.0307404922020936<< arma::endr
     << 0.9572855957780877 << 0.0307404922020936<< arma::endr
     << -0.9825455052614132 << 0.0197320850561227<< arma::endr
     << 0.9825455052614132 << 0.0197320850561227<< arma::endr
     << -0.9966794422605966 << 0.0085169038787464<< arma::endr
     << 0.9966794422605966 << 0.0085169038787464 << arma::endr;
    } else if ( n ==30 ) {
    nw  << -0.0514718425553177 << 0.1028526528935588<< arma::endr
     << 0.0514718425553177 << 0.1028526528935588<< arma::endr
     << -0.1538699136085835 << 0.1017623897484055<< arma::endr
     << 0.1538699136085835 << 0.1017623897484055<< arma::endr
     << -0.2546369261678899 << 0.0995934205867953<< arma::endr
     << 0.2546369261678899 << 0.0995934205867953<< arma::endr
     << -0.3527047255308781 << 0.0963687371746443<< arma::endr
     << 0.3527047255308781 << 0.0963687371746443<< arma::endr
     << -0.4470337695380892 << 0.0921225222377861<< arma::endr
     << 0.4470337695380892 << 0.0921225222377861<< arma::endr
     << -0.5366241481420199 << 0.086899787201083<< arma::endr
     << 0.5366241481420199 << 0.086899787201083<< arma::endr
     << -0.6205261829892429 << 0.0807558952294202<< arma::endr
     << 0.6205261829892429 << 0.0807558952294202<< arma::endr
     << -0.6978504947933158 << 0.0737559747377052<< arma::endr
     << 0.6978504947933158 << 0.0737559747377052<< arma::endr
     << -0.7677774321048262 << 0.0659742298821805<< arma::endr
     << 0.7677774321048262 << 0.0659742298821805<< arma::endr
     << -0.8295657623827684 << 0.0574931562176191<< arma::endr
     << 0.8295657623827684 << 0.0574931562176191<< arma::endr
     << -0.8825605357920527 << 0.0484026728305941<< arma::endr
     << 0.8825605357920527 << 0.0484026728305941<< arma::endr
     << -0.9262000474292743 << 0.0387991925696271<< arma::endr
     << 0.9262000474292743 << 0.0387991925696271<< arma::endr
     << -0.9600218649683075 << 0.0287847078833234<< arma::endr
     << 0.9600218649683075 << 0.0287847078833234<< arma::endr
     << -0.9836681232797472 << 0.018466468311091<< arma::endr
     << 0.9836681232797472 << 0.018466468311091<< arma::endr
     << -0.9968934840746495 << 0.0079681924961666<< arma::endr
     << 0.9968934840746495 << 0.0079681924961666 << arma::endr;
    } else if ( n ==31 ) {
    nw  << 0.0 << 0.0997205447934265<< arma::endr
     << -0.0995553121523415 << 0.0992250112266723<< arma::endr
     << 0.0995553121523415 << 0.0992250112266723<< arma::endr
     << -0.1981211993355706 << 0.0977433353863287<< arma::endr
     << 0.1981211993355706 << 0.0977433353863287<< arma::endr
     << -0.2947180699817016 << 0.0952902429123195<< arma::endr
     << 0.2947180699817016 << 0.0952902429123195<< arma::endr
     << -0.3883859016082329 << 0.0918901138936415<< arma::endr
     << 0.3883859016082329 << 0.0918901138936415<< arma::endr
     << -0.4781937820449025 << 0.0875767406084779<< arma::endr
     << 0.4781937820449025 << 0.0875767406084779<< arma::endr
     << -0.5632491614071493 << 0.0823929917615893<< arma::endr
     << 0.5632491614071493 << 0.0823929917615893<< arma::endr
     << -0.6427067229242603 << 0.0763903865987766<< arma::endr
     << 0.6427067229242603 << 0.0763903865987766<< arma::endr
     << -0.7157767845868532 << 0.0696285832354104<< arma::endr
     << 0.7157767845868532 << 0.0696285832354104<< arma::endr
     << -0.7817331484166249 << 0.0621747865610284<< arma::endr
     << 0.7817331484166249 << 0.0621747865610284<< arma::endr
     << -0.8399203201462674 << 0.0541030824249169<< arma::endr
     << 0.8399203201462674 << 0.0541030824249169<< arma::endr
     << -0.8897600299482711 << 0.0454937075272011<< arma::endr
     << 0.8897600299482711 << 0.0454937075272011<< arma::endr
     << -0.9307569978966481 << 0.0364322739123855<< arma::endr
     << 0.9307569978966481 << 0.0364322739123855<< arma::endr
     << -0.9625039250929497 << 0.0270090191849794<< arma::endr
     << 0.9625039250929497 << 0.0270090191849794<< arma::endr
     << -0.9846859096651525 << 0.0173186207903106<< arma::endr
     << 0.9846859096651525 << 0.0173186207903106<< arma::endr
     << -0.997087481819477 << 0.0074708315792488<< arma::endr
     << 0.997087481819477 << 0.0074708315792488 << arma::endr;
    } else if ( n ==32 ) {
    nw  << -0.0483076656877383 << 0.0965400885147278<< arma::endr
     << 0.0483076656877383 << 0.0965400885147278<< arma::endr
     << -0.1444719615827965 << 0.0956387200792749<< arma::endr
     << 0.1444719615827965 << 0.0956387200792749<< arma::endr
     << -0.2392873622521371 << 0.0938443990808046<< arma::endr
     << 0.2392873622521371 << 0.0938443990808046<< arma::endr
     << -0.3318686022821277 << 0.0911738786957639<< arma::endr
     << 0.3318686022821277 << 0.0911738786957639<< arma::endr
     << -0.4213512761306353 << 0.0876520930044038<< arma::endr
     << 0.4213512761306353 << 0.0876520930044038<< arma::endr
     << -0.5068999089322294 << 0.0833119242269467<< arma::endr
     << 0.5068999089322294 << 0.0833119242269467<< arma::endr
     << -0.5877157572407623 << 0.0781938957870703<< arma::endr
     << 0.5877157572407623 << 0.0781938957870703<< arma::endr
     << -0.6630442669302152 << 0.0723457941088485<< arma::endr
     << 0.6630442669302152 << 0.0723457941088485<< arma::endr
     << -0.7321821187402897 << 0.0658222227763618<< arma::endr
     << 0.7321821187402897 << 0.0658222227763618<< arma::endr
     << -0.7944837959679424 << 0.0586840934785355<< arma::endr
     << 0.7944837959679424 << 0.0586840934785355<< arma::endr
     << -0.84936761373257 << 0.0509980592623762<< arma::endr
     << 0.84936761373257 << 0.0509980592623762<< arma::endr
     << -0.8963211557660521 << 0.0428358980222267<< arma::endr
     << 0.8963211557660521 << 0.0428358980222267<< arma::endr
     << -0.9349060759377397 << 0.0342738629130214<< arma::endr
     << 0.9349060759377397 << 0.0342738629130214<< arma::endr
     << -0.9647622555875064 << 0.0253920653092621<< arma::endr
     << 0.9647622555875064 << 0.0253920653092621<< arma::endr
     << -0.9856115115452684 << 0.0162743947309057<< arma::endr
     << 0.9856115115452684 << 0.0162743947309057<< arma::endr
     << -0.9972638618494816 << 0.0070186100094701<< arma::endr
     << 0.9972638618494816 << 0.0070186100094701 << arma::endr;
    } else if ( n ==33 ) {
    nw  << 0.0 << 0.09376844616021<< arma::endr
     << -0.0936310658547334 << 0.0933564260655961<< arma::endr
     << 0.0936310658547334 << 0.0933564260655961<< arma::endr
     << -0.1864392988279916 << 0.0921239866433168<< arma::endr
     << 0.1864392988279916 << 0.0921239866433168<< arma::endr
     << -0.277609097152497 << 0.0900819586606386<< arma::endr
     << 0.277609097152497 << 0.0900819586606386<< arma::endr
     << -0.3663392577480734 << 0.0872482876188443<< arma::endr
     << 0.3663392577480734 << 0.0872482876188443<< arma::endr
     << -0.4518500172724507 << 0.0836478760670387<< arma::endr
     << 0.4518500172724507 << 0.0836478760670387<< arma::endr
     << -0.5333899047863476 << 0.0793123647948867<< arma::endr
     << 0.5333899047863476 << 0.0793123647948867<< arma::endr
     << -0.610242345836379 << 0.0742798548439541<< arma::endr
     << 0.610242345836379 << 0.0742798548439541<< arma::endr
     << -0.6817319599697428 << 0.0685945728186567<< arma::endr
     << 0.6817319599697428 << 0.0685945728186567<< arma::endr
     << -0.7472304964495622 << 0.0623064825303175<< arma::endr
     << 0.7472304964495622 << 0.0623064825303175<< arma::endr
     << -0.8061623562741665 << 0.0554708466316636<< arma::endr
     << 0.8061623562741665 << 0.0554708466316636<< arma::endr
     << -0.8580096526765041 << 0.0481477428187117<< arma::endr
     << 0.8580096526765041 << 0.0481477428187117<< arma::endr
     << -0.9023167677434336 << 0.0404015413316696<< arma::endr
     << 0.9023167677434336 << 0.0404015413316696<< arma::endr
     << -0.9386943726111684 << 0.032300358632329<< arma::endr
     << 0.9386943726111684 << 0.032300358632329<< arma::endr
     << -0.9668229096899927 << 0.0239155481017495<< arma::endr
     << 0.9668229096899927 << 0.0239155481017495<< arma::endr
     << -0.9864557262306425 << 0.0153217015129347<< arma::endr
     << 0.9864557262306425 << 0.0153217015129347<< arma::endr
     << -0.9974246942464552 << 0.0066062278475874<< arma::endr
     << 0.9974246942464552 << 0.0066062278475874 << arma::endr;
    } else if ( n ==34 ) {
    nw  << -0.0455098219531025 << 0.0909567403302599<< arma::endr
     << 0.0455098219531025 << 0.0909567403302599<< arma::endr
     << -0.136152357259183 << 0.0902030443706407<< arma::endr
     << 0.136152357259183 << 0.0902030443706407<< arma::endr
     << -0.2256666916164495 << 0.0887018978356939<< arma::endr
     << 0.2256666916164495 << 0.0887018978356939<< arma::endr
     << -0.3133110813394632 << 0.0864657397470358<< arma::endr
     << 0.3133110813394632 << 0.0864657397470358<< arma::endr
     << -0.3983592777586459 << 0.0835130996998457<< arma::endr
     << 0.3983592777586459 << 0.0835130996998457<< arma::endr
     << -0.480106545190327 << 0.0798684443397718<< arma::endr
     << 0.480106545190327 << 0.0798684443397718<< arma::endr
     << -0.5578755006697467 << 0.0755619746600319<< arma::endr
     << 0.5578755006697467 << 0.0755619746600319<< arma::endr
     << -0.6310217270805285 << 0.0706293758142557<< arma::endr
     << 0.6310217270805285 << 0.0706293758142557<< arma::endr
     << -0.6989391132162629 << 0.0651115215540764<< arma::endr
     << 0.6989391132162629 << 0.0651115215540764<< arma::endr
     << -0.761064876629873 << 0.0590541358275245<< arma::endr
     << 0.761064876629873 << 0.0590541358275245<< arma::endr
     << -0.8168842279009336 << 0.0525074145726781<< arma::endr
     << 0.8168842279009336 << 0.0525074145726781<< arma::endr
     << -0.8659346383345645 << 0.0455256115233533<< arma::endr
     << 0.8659346383345645 << 0.0455256115233533<< arma::endr
     << -0.9078096777183244 << 0.0381665937963875<< arma::endr
     << 0.9078096777183244 << 0.0381665937963875<< arma::endr
     << -0.9421623974051071 << 0.0304913806384461<< arma::endr
     << 0.9421623974051071 << 0.0304913806384461<< arma::endr
     << -0.9687082625333443 << 0.022563721985495<< arma::endr
     << 0.9687082625333443 << 0.022563721985495<< arma::endr
     << -0.9872278164063095 << 0.014450162748595<< arma::endr
     << 0.9872278164063095 << 0.014450162748595<< arma::endr
     << -0.997571753790842 << 0.0062291405559087<< arma::endr
     << 0.997571753790842 << 0.0062291405559087 << arma::endr;
    } else if ( n ==35 ) {
    nw  << 0.0 << 0.0884867949071043<< arma::endr
     << -0.0883713432756593 << 0.0881405304302755<< arma::endr
     << 0.0883713432756593 << 0.0881405304302755<< arma::endr
     << -0.1760510611659896 << 0.0871044469971835<< arma::endr
     << 0.1760510611659896 << 0.0871044469971835<< arma::endr
     << -0.262352941209296 << 0.0853866533920991<< arma::endr
     << 0.262352941209296 << 0.0853866533920991<< arma::endr
     << -0.3466015544308139 << 0.0830005937288566<< arma::endr
     << 0.3466015544308139 << 0.0830005937288566<< arma::endr
     << -0.4281375415178142 << 0.0799649422423243<< arma::endr
     << 0.4281375415178142 << 0.0799649422423243<< arma::endr
     << -0.5063227732414887 << 0.0763034571554421<< arma::endr
     << 0.5063227732414887 << 0.0763034571554421<< arma::endr
     << -0.5805453447497645 << 0.0720447947725601<< arma::endr
     << 0.5805453447497645 << 0.0720447947725601<< arma::endr
     << -0.6502243646658904 << 0.0672222852690869<< arma::endr
     << 0.6502243646658904 << 0.0672222852690869<< arma::endr
     << -0.7148145015566287 << 0.0618736719660802<< arma::endr
     << 0.7148145015566287 << 0.0618736719660802<< arma::endr
     << -0.7738102522869126 << 0.0560408162123701<< arma::endr
     << 0.7738102522869126 << 0.0560408162123701<< arma::endr
     << -0.8267498990922254 << 0.0497693704013535<< arma::endr
     << 0.8267498990922254 << 0.0497693704013535<< arma::endr
     << -0.8732191250252224 << 0.0431084223261702<< arma::endr
     << 0.8732191250252224 << 0.0431084223261702<< arma::endr
     << -0.9128542613593176 << 0.0361101158634634<< arma::endr
     << 0.9128542613593176 << 0.0361101158634634<< arma::endr
     << -0.9453451482078273 << 0.0288292601088943<< arma::endr
     << 0.9453451482078273 << 0.0288292601088943<< arma::endr
     << -0.9704376160392298 << 0.0213229799114836<< arma::endr
     << 0.9704376160392298 << 0.0213229799114836<< arma::endr
     << -0.9879357644438514 << 0.0136508283483615<< arma::endr
     << 0.9879357644438514 << 0.0136508283483615<< arma::endr
     << -0.9977065690996003 << 0.0058834334204431<< arma::endr
     << 0.9977065690996003 << 0.0058834334204431 << arma::endr;
    } else if ( n ==36 ) {
    nw  << -0.0430181984737086 << 0.0859832756703948<< arma::endr
     << 0.0430181984737086 << 0.0859832756703948<< arma::endr
     << -0.1287361038093848 << 0.0853466857393386<< arma::endr
     << 0.1287361038093848 << 0.0853466857393386<< arma::endr
     << -0.2135008923168656 << 0.0840782189796619<< arma::endr
     << 0.2135008923168656 << 0.0840782189796619<< arma::endr
     << -0.2966849953440283 << 0.0821872667043397<< arma::endr
     << 0.2966849953440283 << 0.0821872667043397<< arma::endr
     << -0.3776725471196892 << 0.0796878289120716<< arma::endr
     << 0.3776725471196892 << 0.0796878289120716<< arma::endr
     << -0.4558639444334203 << 0.0765984106458707<< arma::endr
     << 0.4558639444334203 << 0.0765984106458707<< arma::endr
     << -0.5306802859262452 << 0.0729418850056531<< arma::endr
     << 0.5306802859262452 << 0.0729418850056531<< arma::endr
     << -0.6015676581359806 << 0.0687453238357364<< arma::endr
     << 0.6015676581359806 << 0.0687453238357364<< arma::endr
     << -0.668001236585521 << 0.0640397973550155<< arma::endr
     << 0.668001236585521 << 0.0640397973550155<< arma::endr
     << -0.7294891715935565 << 0.0588601442453248<< arma::endr
     << 0.7294891715935565 << 0.0588601442453248<< arma::endr
     << -0.7855762301322066 << 0.0532447139777599<< arma::endr
     << 0.7855762301322066 << 0.0532447139777599<< arma::endr
     << -0.8358471669924753 << 0.047235083490266<< arma::endr
     << 0.8358471669924753 << 0.047235083490266<< arma::endr
     << -0.8799298008903972 << 0.0408757509236449<< arma::endr
     << 0.8799298008903972 << 0.0408757509236449<< arma::endr
     << -0.9174977745156591 << 0.0342138107703072<< arma::endr
     << 0.9174977745156591 << 0.0342138107703072<< arma::endr
     << -0.9482729843995076 << 0.0272986214985688<< arma::endr
     << 0.9482729843995076 << 0.0272986214985688<< arma::endr
     << -0.972027691049698 << 0.0201815152977355<< arma::endr
     << 0.972027691049698 << 0.0201815152977355<< arma::endr
     << -0.9885864789022122 << 0.0129159472840656<< arma::endr
     << 0.9885864789022122 << 0.0129159472840656<< arma::endr
     << -0.9978304624840858 << 0.005565719664245<< arma::endr
     << 0.9978304624840858 << 0.005565719664245 << arma::endr;
    } else if ( n ==37 ) {
    nw  << 0.0 << 0.0837683609931389<< arma::endr
     << -0.0836704089547699 << 0.0834745736258628<< arma::endr
     << 0.0836704089547699 << 0.0834745736258628<< arma::endr
     << -0.166753930239852 << 0.0825952722364373<< arma::endr
     << 0.166753930239852 << 0.0825952722364373<< arma::endr
     << -0.2486677927913657 << 0.081136624508465<< arma::endr
     << 0.2486677927913657 << 0.081136624508465<< arma::endr
     << -0.328837429883707 << 0.0791088618375294<< arma::endr
     << 0.328837429883707 << 0.0791088618375294<< arma::endr
     << -0.4067005093183261 << 0.0765262075705292<< arma::endr
     << 0.4067005093183261 << 0.0765262075705292<< arma::endr
     << -0.4817108778032055 << 0.0734067772484882<< arma::endr
     << 0.4817108778032055 << 0.0734067772484882<< arma::endr
     << -0.5533423918615817 << 0.0697724515557003<< arma::endr
     << 0.5533423918615817 << 0.0697724515557003<< arma::endr
     << -0.6210926084089244 << 0.0656487228727513<< arma::endr
     << 0.6210926084089244 << 0.0656487228727513<< arma::endr
     << -0.6844863091309593 << 0.061064516523226<< arma::endr
     << 0.6844863091309593 << 0.061064516523226<< arma::endr
     << -0.7430788339819653 << 0.0560519879982749<< arma::endr
     << 0.7430788339819653 << 0.0560519879982749<< arma::endr
     << -0.7964592005099023 << 0.0506462976548246<< arma::endr
     << 0.7964592005099023 << 0.0506462976548246<< arma::endr
     << -0.844252987340556 << 0.0448853646624372<< arma::endr
     << 0.844252987340556 << 0.0448853646624372<< arma::endr
     << -0.8861249621554861 << 0.0388096025019345<< arma::endr
     << 0.8861249621554861 << 0.0388096025019345<< arma::endr
     << -0.9217814374124638 << 0.0324616398475215<< arma::endr
     << 0.9217814374124638 << 0.0324616398475215<< arma::endr
     << -0.9509723432620948 << 0.0258860369905589<< arma::endr
     << 0.9509723432620948 << 0.0258860369905589<< arma::endr
     << -0.9734930300564858 << 0.019129044489084<< arma::endr
     << 0.9734930300564858 << 0.019129044489084<< arma::endr
     << -0.9891859632143192 << 0.0122387801003076<< arma::endr
     << 0.9891859632143192 << 0.0122387801003076<< arma::endr
     << -0.9979445824779136 << 0.0052730572794979<< arma::endr
     << 0.9979445824779136 << 0.0052730572794979 << arma::endr;
    } else if ( n ==38 ) {
    nw  << -0.0407851479045782 << 0.0815250292803858<< arma::endr
     << 0.0407851479045782 << 0.0815250292803858<< arma::endr
     << -0.1220840253378674 << 0.0809824937705971<< arma::endr
     << 0.1220840253378674 << 0.0809824937705971<< arma::endr
     << -0.2025704538921167 << 0.0799010332435278<< arma::endr
     << 0.2025704538921167 << 0.0799010332435278<< arma::endr
     << -0.2817088097901653 << 0.078287844658211<< arma::endr
     << 0.2817088097901653 << 0.078287844658211<< arma::endr
     << -0.358972440479435 << 0.0761536635484464<< arma::endr
     << 0.358972440479435 << 0.0761536635484464<< arma::endr
     << -0.4338471694323765 << 0.0735126925847435<< arma::endr
     << 0.4338471694323765 << 0.0735126925847435<< arma::endr
     << -0.5058347179279311 << 0.070382507066899<< arma::endr
     << 0.5058347179279311 << 0.070382507066899<< arma::endr
     << -0.5744560210478071 << 0.0667839379791404<< arma::endr
     << 0.5744560210478071 << 0.0667839379791404<< arma::endr
     << -0.6392544158296817 << 0.0627409333921331<< arma::endr
     << 0.6392544158296817 << 0.0627409333921331<< arma::endr
     << -0.6997986803791844 << 0.0582803991469972<< arma::endr
     << 0.6997986803791844 << 0.0582803991469972<< arma::endr
     << -0.7556859037539707 << 0.0534320199103323<< arma::endr
     << 0.7556859037539707 << 0.0534320199103323<< arma::endr
     << -0.8065441676053168 << 0.0482280618607587<< arma::endr
     << 0.8065441676053168 << 0.0482280618607587<< arma::endr
     << -0.8520350219323621 << 0.0427031585046744<< arma::endr
     << 0.8520350219323621 << 0.0427031585046744<< arma::endr
     << -0.8918557390046322 << 0.0368940815940247<< arma::endr
     << 0.8918557390046322 << 0.0368940815940247<< arma::endr
     << -0.9257413320485844 << 0.0308395005451751<< arma::endr
     << 0.9257413320485844 << 0.0308395005451751<< arma::endr
     << -0.9534663309335296 << 0.0245797397382324<< arma::endr
     << 0.9534663309335296 << 0.0245797397382324<< arma::endr
     << -0.9748463285901535 << 0.0181565777096132<< arma::endr
     << 0.9748463285901535 << 0.0181565777096132<< arma::endr
     << -0.9897394542663855 << 0.0116134447164687<< arma::endr
     << 0.9897394542663855 << 0.0116134447164687<< arma::endr
     << -0.9980499305356876 << 0.0050028807496393<< arma::endr
     << 0.9980499305356876 << 0.0050028807496393 << arma::endr;
    } else if ( n ==39 ) {
    nw  << 0.0 << 0.0795276221394429<< arma::endr
     << -0.0794438046087555 << 0.0792762225683685<< arma::endr
     << 0.0794438046087555 << 0.0792762225683685<< arma::endr
     << -0.1583853399978378 << 0.0785236132873712<< arma::endr
     << 0.1583853399978378 << 0.0785236132873712<< arma::endr
     << -0.2363255124618358 << 0.077274552544682<< arma::endr
     << 0.2363255124618358 << 0.077274552544682<< arma::endr
     << -0.3127715592481859 << 0.0755369373228361<< arma::endr
     << 0.3127715592481859 << 0.0755369373228361<< arma::endr
     << -0.3872401639715615 << 0.0733217534142686<< arma::endr
     << 0.3872401639715615 << 0.0733217534142686<< arma::endr
     << -0.4592605123091361 << 0.0706430059706088<< arma::endr
     << 0.4592605123091361 << 0.0706430059706088<< arma::endr
     << -0.5283772686604374 << 0.0675176309662313<< arma::endr
     << 0.5283772686604374 << 0.0675176309662313<< arma::endr
     << -0.594153454957278 << 0.0639653881386824<< arma::endr
     << 0.594153454957278 << 0.0639653881386824<< arma::endr
     << -0.656173213432011 << 0.0600087360885962<< arma::endr
     << 0.656173213432011 << 0.0600087360885962<< arma::endr
     << -0.7140444358945347 << 0.0556726903409163<< arma::endr
     << 0.7140444358945347 << 0.0556726903409163<< arma::endr
     << -0.7674012429310635 << 0.0509846652921294<< arma::endr
     << 0.7674012429310635 << 0.0509846652921294<< arma::endr
     << -0.8159062974301431 << 0.0459743011089166<< arma::endr
     << 0.8159062974301431 << 0.0459743011089166<< arma::endr
     << -0.8592529379999062 << 0.0406732768479338<< arma::endr
     << 0.8592529379999062 << 0.0406732768479338<< arma::endr
     << -0.8971671192929929 << 0.0351151114981313<< arma::endr
     << 0.8971671192929929 << 0.0351151114981313<< arma::endr
     << -0.9294091484867383 << 0.0293349559839034<< arma::endr
     << 0.9294091484867383 << 0.0293349559839034<< arma::endr
     << -0.9557752123246522 << 0.0233693848321782<< arma::endr
     << 0.9557752123246522 << 0.0233693848321782<< arma::endr
     << -0.9760987093334711 << 0.0172562290937249<< arma::endr
     << 0.9760987093334711 << 0.0172562290937249<< arma::endr
     << -0.990251536854686 << 0.0110347889391646<< arma::endr
     << 0.990251536854686 << 0.0110347889391646<< arma::endr
     << -0.9981473830664329 << 0.0047529446916351<< arma::endr
     << 0.9981473830664329 << 0.0047529446916351 << arma::endr;
    } else if ( n ==40 ) {
    nw  << -0.0387724175060508 << 0.0775059479784248<< arma::endr
     << 0.0387724175060508 << 0.0775059479784248<< arma::endr
     << -0.1160840706752552 << 0.077039818164248<< arma::endr
     << 0.1160840706752552 << 0.077039818164248<< arma::endr
     << -0.1926975807013711 << 0.0761103619006262<< arma::endr
     << 0.1926975807013711 << 0.0761103619006262<< arma::endr
     << -0.2681521850072537 << 0.0747231690579683<< arma::endr
     << 0.2681521850072537 << 0.0747231690579683<< arma::endr
     << -0.3419940908257585 << 0.0728865823958041<< arma::endr
     << 0.3419940908257585 << 0.0728865823958041<< arma::endr
     << -0.413779204371605 << 0.0706116473912868<< arma::endr
     << 0.413779204371605 << 0.0706116473912868<< arma::endr
     << -0.4830758016861787 << 0.0679120458152339<< arma::endr
     << 0.4830758016861787 << 0.0679120458152339<< arma::endr
     << -0.5494671250951282 << 0.064804013456601<< arma::endr
     << 0.5494671250951282 << 0.064804013456601<< arma::endr
     << -0.6125538896679802 << 0.0613062424929289<< arma::endr
     << 0.6125538896679802 << 0.0613062424929289<< arma::endr
     << -0.6719566846141796 << 0.0574397690993916<< arma::endr
     << 0.6719566846141796 << 0.0574397690993916<< arma::endr
     << -0.7273182551899271 << 0.0532278469839368<< arma::endr
     << 0.7273182551899271 << 0.0532278469839368<< arma::endr
     << -0.7783056514265194 << 0.0486958076350722<< arma::endr
     << 0.7783056514265194 << 0.0486958076350722<< arma::endr
     << -0.8246122308333117 << 0.0438709081856733<< arma::endr
     << 0.8246122308333117 << 0.0438709081856733<< arma::endr
     << -0.8659595032122595 << 0.038782167974472<< arma::endr
     << 0.8659595032122595 << 0.038782167974472<< arma::endr
     << -0.9020988069688743 << 0.0334601952825478<< arma::endr
     << 0.9020988069688743 << 0.0334601952825478<< arma::endr
     << -0.9328128082786765 << 0.0279370069800234<< arma::endr
     << 0.9328128082786765 << 0.0279370069800234<< arma::endr
     << -0.9579168192137917 << 0.022245849194167<< arma::endr
     << 0.9579168192137917 << 0.022245849194167<< arma::endr
     << -0.9772599499837743 << 0.0164210583819079<< arma::endr
     << 0.9772599499837743 << 0.0164210583819079<< arma::endr
     << -0.990726238699457 << 0.0104982845311528<< arma::endr
     << 0.990726238699457 << 0.0104982845311528<< arma::endr
     << -0.9982377097105593 << 0.0045212770985332<< arma::endr
     << 0.9982377097105593 << 0.0045212770985332 << arma::endr;
    } else if ( n ==41 ) {
    nw  << 0.0 << 0.0756955356472984<< arma::endr
     << -0.075623258989163 << 0.0754787470927158<< arma::endr
     << 0.075623258989163 << 0.0754787470927158<< arma::endr
     << -0.1508133548639922 << 0.0748296231762215<< arma::endr
     << 0.1508133548639922 << 0.0748296231762215<< arma::endr
     << -0.2251396056334228 << 0.0737518820272235<< arma::endr
     << 0.2251396056334228 << 0.0737518820272235<< arma::endr
     << -0.2981762773418249 << 0.0722516968610231<< arma::endr
     << 0.2981762773418249 << 0.0722516968610231<< arma::endr
     << -0.3695050226404815 << 0.0703376606208175<< arma::endr
     << 0.3695050226404815 << 0.0703376606208175<< arma::endr
     << -0.4387172770514071 << 0.0680207367608768<< arma::endr
     << 0.4387172770514071 << 0.0680207367608768<< arma::endr
     << -0.5054165991994061 << 0.0653141964535274<< arma::endr
     << 0.5054165991994061 << 0.0653141964535274<< arma::endr
     << -0.5692209416102159 << 0.0622335425809663<< arma::endr
     << 0.5692209416102159 << 0.0622335425809663<< arma::endr
     << -0.6297648390721963 << 0.0587964209498719<< arma::endr
     << 0.6297648390721963 << 0.0587964209498719<< arma::endr
     << -0.6867015020349513 << 0.0550225192425787<< arma::endr
     << 0.6867015020349513 << 0.0550225192425787<< arma::endr
     << -0.7397048030699261 << 0.0509334542946175<< arma::endr
     << 0.7397048030699261 << 0.0509334542946175<< arma::endr
     << -0.7884711450474093 << 0.0465526483690143<< arma::endr
     << 0.7884711450474093 << 0.0465526483690143<< arma::endr
     << -0.8327212004013613 << 0.0419051951959097<< arma::endr
     << 0.8327212004013613 << 0.0419051951959097<< arma::endr
     << -0.8722015116924414 << 0.037017716703508<< arma::endr
     << 0.8722015116924414 << 0.037017716703508<< arma::endr
     << -0.9066859447581012 << 0.0319182117316993<< arma::endr
     << 0.9066859447581012 << 0.0319182117316993<< arma::endr
     << -0.9359769874978539 << 0.0266358992071104<< arma::endr
     << 0.9359769874978539 << 0.0266358992071104<< arma::endr
     << -0.9599068917303463 << 0.0212010633687796<< arma::endr
     << 0.9599068917303463 << 0.0212010633687796<< arma::endr
     << -0.9783386735610834 << 0.0156449384078186<< arma::endr
     << 0.9783386735610834 << 0.0156449384078186<< arma::endr
     << -0.9911671096990163 << 0.0099999387739059<< arma::endr
     << 0.9911671096990163 << 0.0099999387739059<< arma::endr
     << -0.9983215885747715 << 0.0043061403581649<< arma::endr
     << 0.9983215885747715 << 0.0043061403581649 << arma::endr;
    } else if ( n ==42 ) {
    nw  << -0.0369489431653518 << 0.0738642342321729<< arma::endr
     << 0.0369489431653518 << 0.0738642342321729<< arma::endr
     << -0.1106450272085199 << 0.0734608134534675<< arma::endr
     << 0.1106450272085199 << 0.0734608134534675<< arma::endr
     << -0.1837368065648546 << 0.0726561752438041<< arma::endr
     << 0.1837368065648546 << 0.0726561752438041<< arma::endr
     << -0.2558250793428791 << 0.071454714265171<< arma::endr
     << 0.2558250793428791 << 0.071454714265171<< arma::endr
     << -0.3265161244654115 << 0.0698629924925942<< arma::endr
     << 0.3265161244654115 << 0.0698629924925942<< arma::endr
     << -0.395423852042975 << 0.0678897033765219<< arma::endr
     << 0.395423852042975 << 0.0678897033765219<< arma::endr
     << -0.4621719120704219 << 0.065545624364909<< arma::endr
     << 0.4621719120704219 << 0.065545624364909<< arma::endr
     << -0.5263957499311923 << 0.0628435580450026<< arma::endr
     << 0.5263957499311923 << 0.0628435580450026<< arma::endr
     << -0.5877445974851093 << 0.0597982622275867<< arma::endr
     << 0.5877445974851093 << 0.0597982622275867<< arma::endr
     << -0.6458833888692478 << 0.0564263693580184<< arma::endr
     << 0.6458833888692478 << 0.0564263693580184<< arma::endr
     << -0.7004945905561712 << 0.0527462956991741<< arma::endr
     << 0.7004945905561712 << 0.0527462956991741<< arma::endr
     << -0.7512799356894805 << 0.0487781407928032<< arma::endr
     << 0.7512799356894805 << 0.0487781407928032<< arma::endr
     << -0.7979620532554874 << 0.0445435777719659<< arma::endr
     << 0.7979620532554874 << 0.0445435777719659<< arma::endr
     << -0.8402859832618169 << 0.0400657351806923<< arma::endr
     << 0.8402859832618169 << 0.0400657351806923<< arma::endr
     << -0.8780205698121727 << 0.0353690710975921<< arma::endr
     << 0.8780205698121727 << 0.0353690710975921<< arma::endr
     << -0.9109597249041275 << 0.0304792406996035<< arma::endr
     << 0.9109597249041275 << 0.0304792406996035<< arma::endr
     << -0.9389235573549882 << 0.025422959526113<< arma::endr
     << 0.9389235573549882 << 0.025422959526113<< arma::endr
     << -0.9617593653382045 << 0.0202278695690526<< arma::endr
     << 0.9617593653382045 << 0.0202278695690526<< arma::endr
     << -0.9793425080637482 << 0.0149224436973575<< arma::endr
     << 0.9793425080637482 << 0.0149224436973575<< arma::endr
     << -0.9915772883408609 << 0.0095362203017485<< arma::endr
     << 0.9915772883408609 << 0.0095362203017485<< arma::endr
     << -0.9983996189900625 << 0.0041059986046491<< arma::endr
     << 0.9983996189900625 << 0.0041059986046491 << arma::endr;
    } else if ( n ==43 ) {
    nw  << 0.0 << 0.072215751693799<< arma::endr
     << -0.0721529908745862 << 0.072027501971422<< arma::endr
     << 0.0721529908745862 << 0.072027501971422<< arma::endr
     << -0.1439298095107133 << 0.0714637342525141<< arma::endr
     << 0.1439298095107133 << 0.0714637342525141<< arma::endr
     << -0.2149562448605182 << 0.070527387765085<< arma::endr
     << 0.2149562448605182 << 0.070527387765085<< arma::endr
     << -0.2848619980329136 << 0.0692233441936567<< arma::endr
     << 0.2848619980329136 << 0.0692233441936567<< arma::endr
     << -0.3532826128643038 << 0.0675584022293652<< arma::endr
     << 0.3532826128643038 << 0.0675584022293652<< arma::endr
     << -0.4198613760292693 << 0.0655412421263228<< arma::endr
     << 0.4198613760292693 << 0.0655412421263228<< arma::endr
     << -0.4842511767857347 << 0.0631823804493961<< arma::endr
     << 0.4842511767857347 << 0.0631823804493961<< arma::endr
     << -0.5461163166600848 << 0.0604941152499913<< arma::endr
     << 0.5461163166600848 << 0.0604941152499913<< arma::endr
     << -0.605134259639601 << 0.0574904619569105<< arma::endr
     << 0.605134259639601 << 0.0574904619569105<< arma::endr
     << -0.6609973137514982 << 0.0541870803188818<< arma::endr
     << 0.6609973137514982 << 0.0541870803188818<< arma::endr
     << -0.7134142352689571 << 0.0506011927843902<< arma::endr
     << 0.7134142352689571 << 0.0506011927843902<< arma::endr
     << -0.7621117471949551 << 0.0467514947543466<< arma::endr
     << 0.7621117471949551 << 0.0467514947543466<< arma::endr
     << -0.8068359641369386 << 0.0426580571979821<< arma::endr
     << 0.8068359641369386 << 0.0426580571979821<< arma::endr
     << -0.8473537162093151 << 0.0383422221941327<< arma::endr
     << 0.8473537162093151 << 0.0383422221941327<< arma::endr
     << -0.8834537652186168 << 0.0338264920868603<< arma::endr
     << 0.8834537652186168 << 0.0338264920868603<< arma::endr
     << -0.9149479072061387 << 0.0291344132614985<< arma::endr
     << 0.9149479072061387 << 0.0291344132614985<< arma::endr
     << -0.9416719568476378 << 0.0242904566138388<< arma::endr
     << 0.9416719568476378 << 0.0242904566138388<< arma::endr
     << -0.96348661301408 << 0.0193199014236839<< arma::endr
     << 0.96348661301408 << 0.0193199014236839<< arma::endr
     << -0.9802782209802553 << 0.0142487564315765<< arma::endr
     << 0.9802782209802553 << 0.0142487564315765<< arma::endr
     << -0.9919595575932442 << 0.0091039966374014<< arma::endr
     << 0.9919595575932442 << 0.0091039966374014<< arma::endr
     << -0.9984723322425078 << 0.0039194902538441<< arma::endr
     << 0.9984723322425078 << 0.0039194902538441 << arma::endr;
    } else if ( n ==44 ) {
    nw  << -0.0352892369641354 << 0.0705491577893541<< arma::endr
     << 0.0352892369641354 << 0.0705491577893541<< arma::endr
     << -0.1056919017086533 << 0.0701976854735582<< arma::endr
     << 0.1056919017086533 << 0.0701976854735582<< arma::endr
     << -0.1755680147755168 << 0.0694964918615726<< arma::endr
     << 0.1755680147755168 << 0.0694964918615726<< arma::endr
     << -0.2445694569282013 << 0.0684490702693667<< arma::endr
     << 0.2445694569282013 << 0.0684490702693667<< arma::endr
     << -0.3123524665027858 << 0.0670606389062937<< arma::endr
     << 0.3123524665027858 << 0.0670606389062937<< arma::endr
     << -0.3785793520147072 << 0.0653381148791814<< arma::endr
     << 0.3785793520147072 << 0.0653381148791814<< arma::endr
     << -0.4429201745254115 << 0.0632900797332039<< arma::endr
     << 0.4429201745254115 << 0.0632900797332039<< arma::endr
     << -0.5050543913882023 << 0.060926736701562<< arma::endr
     << 0.5050543913882023 << 0.060926736701562<< arma::endr
     << -0.5646724531854708 << 0.0582598598775955<< arma::endr
     << 0.5646724531854708 << 0.0582598598775955<< arma::endr
     << -0.6214773459035758 << 0.0553027355637281<< arma::endr
     << 0.6214773459035758 << 0.0553027355637281<< arma::endr
     << -0.6751860706661224 << 0.0520700960917045<< arma::endr
     << 0.6751860706661224 << 0.0520700960917045<< arma::endr
     << -0.725531053660717 << 0.048578046448352<< arma::endr
     << 0.725531053660717 << 0.048578046448352<< arma::endr
     << -0.7722614792487559 << 0.04484398408197<< arma::endr
     << 0.7722614792487559 << 0.04484398408197<< arma::endr
     << -0.815144539645135 << 0.0408865123103462<< arma::endr
     << 0.815144539645135 << 0.0408865123103462<< arma::endr
     << -0.8539665950047104 << 0.0367253478138089<< arma::endr
     << 0.8539665950047104 << 0.0367253478138089<< arma::endr
     << -0.8885342382860432 << 0.0323812228120698<< arma::endr
     << 0.8885342382860432 << 0.0323812228120698<< arma::endr
     << -0.9186752599841758 << 0.027875782821281<< arma::endr
     << 0.9186752599841758 << 0.027875782821281<< arma::endr
     << -0.9442395091181941 << 0.0232314819020192<< arma::endr
     << 0.9442395091181941 << 0.0232314819020192<< arma::endr
     << -0.9650996504224931 << 0.0184714817368147<< arma::endr
     << 0.9650996504224931 << 0.0184714817368147<< arma::endr
     << -0.981151833077914 << 0.01361958675558<< arma::endr
     << 0.981151833077914 << 0.01361958675558<< arma::endr
     << -0.9923163921385159 << 0.0087004813675248<< arma::endr
     << 0.9923163921385159 << 0.0087004813675248<< arma::endr
     << -0.9985402006367742 << 0.0037454048031128<< arma::endr
     << 0.9985402006367742 << 0.0037454048031128 << arma::endr;
    } else if ( n ==45 ) {
    nw  << 0.0 << 0.069041824829232<< arma::endr
     << -0.0689869801631442 << 0.0688773169776613<< arma::endr
     << 0.0689869801631442 << 0.0688773169776613<< arma::endr
     << -0.137645205983253 << 0.0683845773786697<< arma::endr
     << 0.137645205983253 << 0.0683845773786697<< arma::endr
     << -0.2056474897832637 << 0.0675659541636075<< arma::endr
     << 0.2056474897832637 << 0.0675659541636075<< arma::endr
     << -0.2726697697523776 << 0.0664253484498425<< arma::endr
     << 0.2726697697523776 << 0.0664253484498425<< arma::endr
     << -0.3383926542506022 << 0.0649681957507234<< arma::endr
     << 0.3383926542506022 << 0.0649681957507234<< arma::endr
     << -0.4025029438585419 << 0.0632014400738199<< arma::endr
     << 0.4025029438585419 << 0.0632014400738199<< arma::endr
     << -0.4646951239196351 << 0.0611335008310665<< arma::endr
     << 0.4646951239196351 << 0.0611335008310665<< arma::endr
     << -0.5246728204629161 << 0.0587742327188417<< arma::endr
     << 0.5246728204629161 << 0.0587742327188417<< arma::endr
     << -0.5821502125693532 << 0.0561348787597865<< arma::endr
     << 0.5821502125693532 << 0.0561348787597865<< arma::endr
     << -0.6368533944532233 << 0.053228016731269<< arma::endr
     << 0.6368533944532233 << 0.053228016731269<< arma::endr
     << -0.6885216807712006 << 0.050067499237952<< arma::endr
     << 0.6885216807712006 << 0.050067499237952<< arma::endr
     << -0.7369088489454904 << 0.0466683877183734<< arma::endr
     << 0.7369088489454904 << 0.0466683877183734<< arma::endr
     << -0.7817843125939062 << 0.043046880709165<< arma::endr
     << 0.7817843125939062 << 0.043046880709165<< arma::endr
     << -0.8229342205020863 << 0.0392202367293025<< arma::endr
     << 0.8229342205020863 << 0.0392202367293025<< arma::endr
     << -0.8601624759606642 << 0.035206692201609<< arma::endr
     << 0.8601624759606642 << 0.035206692201609<< arma::endr
     << -0.8932916717532418 << 0.0310253749345155<< arma::endr
     << 0.8932916717532418 << 0.0310253749345155<< arma::endr
     << -0.9221639367190004 << 0.0266962139675777<< arma::endr
     << 0.9221639367190004 << 0.0266962139675777<< arma::endr
     << -0.9466416909956291 << 0.0222398475505787<< arma::endr
     << 0.9466416909956291 << 0.0222398475505787<< arma::endr
     << -0.9666083103968947 << 0.0176775352579376<< arma::endr
     << 0.9666083103968947 << 0.0176775352579376<< arma::endr
     << -0.9819687150345405 << 0.0130311049915828<< arma::endr
     << 0.9819687150345405 << 0.0130311049915828<< arma::endr
     << -0.9926499984472037 << 0.0083231892962182<< arma::endr
     << 0.9926499984472037 << 0.0083231892962182<< arma::endr
     << -0.9986036451819367 << 0.0035826631552836<< arma::endr
     << 0.9986036451819367 << 0.0035826631552836 << arma::endr;
    } else if ( n ==46 ) {
    nw  << -0.033772190016052 << 0.0675186858490365<< arma::endr
     << 0.033772190016052 << 0.0675186858490365<< arma::endr
     << -0.1011624753055842 << 0.0672106136006782<< arma::endr
     << 0.1011624753055842 << 0.0672106136006782<< arma::endr
     << -0.1680911794671035 << 0.0665958747684549<< arma::endr
     << 0.1680911794671035 << 0.0665958747684549<< arma::endr
     << -0.2342529222062698 << 0.0656772742677812<< arma::endr
     << 0.2342529222062698 << 0.0656772742677812<< arma::endr
     << -0.29934582270187 << 0.0644590034671391<< arma::endr
     << 0.29934582270187 << 0.0644590034671391<< arma::endr
     << -0.3630728770209957 << 0.0629466210643945<< arma::endr
     << 0.3630728770209957 << 0.0629466210643945<< arma::endr
     << -0.4251433132828284 << 0.0611470277246505<< arma::endr
     << 0.4251433132828284 << 0.0611470277246505<< arma::endr
     << -0.4852739183881646 << 0.0590684345955463<< arma::endr
     << 0.4852739183881646 << 0.0590684345955463<< arma::endr
     << -0.5431903302618026 << 0.0567203258439912<< arma::endr
     << 0.5431903302618026 << 0.0567203258439912<< arma::endr
     << -0.5986282897127152 << 0.0541134153858568<< arma::endr
     << 0.5986282897127152 << 0.0541134153858568<< arma::endr
     << -0.6513348462019977 << 0.051259598007143<< arma::endr
     << 0.6513348462019977 << 0.051259598007143<< arma::endr
     << -0.7010695120204057 << 0.0481718951017122<< arma::endr
     << 0.7010695120204057 << 0.0481718951017122<< arma::endr
     << -0.7476053596156661 << 0.0448643952773181<< arma::endr
     << 0.7476053596156661 << 0.0448643952773181<< arma::endr
     << -0.7907300570752742 << 0.0413521901096787<< arma::endr
     << 0.7907300570752742 << 0.0413521901096787<< arma::endr
     << -0.8302468370660661 << 0.0376513053573861<< arma::endr
     << 0.8302468370660661 << 0.0376513053573861<< arma::endr
     << -0.865975394866858 << 0.0337786279991069<< arma::endr
     << 0.865975394866858 << 0.0337786279991069<< arma::endr
     << -0.897752711533942 << 0.0297518295522028<< arma::endr
     << 0.897752711533942 << 0.0297518295522028<< arma::endr
     << -0.9254337988067539 << 0.02558928639713<< arma::endr
     << 0.9254337988067539 << 0.02558928639713<< arma::endr
     << -0.9488923634460898 << 0.0213099987541365<< arma::endr
     << 0.9488923634460898 << 0.0213099987541365<< arma::endr
     << -0.9680213918539919 << 0.0169335140078362<< arma::endr
     << 0.9680213918539919 << 0.0169335140078362<< arma::endr
     << -0.9827336698041669 << 0.0124798837709887<< arma::endr
     << 0.9827336698041669 << 0.0124798837709887<< arma::endr
     << -0.9929623489061744 << 0.0079698982297246<< arma::endr
     << 0.9929623489061744 << 0.0079698982297246<< arma::endr
     << -0.998663042133818 << 0.003430300868107<< arma::endr
     << 0.998663042133818 << 0.003430300868107 << arma::endr;
    } else if ( n ==47 ) {
    nw  << 0.0 << 0.0661351296236555<< arma::endr
     << -0.0660869239163557 << 0.0659905335888105<< arma::endr
     << 0.0660869239163557 << 0.0659905335888105<< arma::endr
     << -0.1318848665545149 << 0.0655573777665497<< arma::endr
     << 0.1318848665545149 << 0.0655573777665497<< arma::endr
     << -0.1971061102791118 << 0.0648375562389457<< arma::endr
     << 0.1971061102791118 << 0.0648375562389457<< arma::endr
     << -0.2614654592149745 << 0.063834216605717<< arma::endr
     << 0.2614654592149745 << 0.063834216605717<< arma::endr
     << -0.3246814863377359 << 0.0625517462209217<< arma::endr
     << 0.3246814863377359 << 0.0625517462209217<< arma::endr
     << -0.3864777640846672 << 0.0609957530087396<< arma::endr
     << 0.3864777640846672 << 0.0609957530087396<< arma::endr
     << -0.4465840731048557 << 0.0591730409423389<< arma::endr
     << 0.4465840731048557 << 0.0591730409423389<< arma::endr
     << -0.5047375838635779 << 0.0570915802932315<< arma::endr
     << 0.5047375838635779 << 0.0570915802932315<< arma::endr
     << -0.5606840059346642 << 0.0547604727815302<< arma::endr
     << 0.5606840059346642 << 0.0547604727815302<< arma::endr
     << -0.6141786999563736 << 0.0521899117800571<< arma::endr
     << 0.6141786999563736 << 0.0521899117800571<< arma::endr
     << -0.6649877473903327 << 0.0493911377473612<< arma::endr
     << 0.6649877473903327 << 0.0493911377473612<< arma::endr
     << -0.7128889734090643 << 0.0463763890865059<< arma::endr
     << 0.7128889734090643 << 0.0463763890865059<< arma::endr
     << -0.7576729184454386 << 0.0431588486484795<< arma::endr
     << 0.7576729184454386 << 0.0431588486484795<< arma::endr
     << -0.799143754167742 << 0.039752586122531<< arma::endr
     << 0.799143754167742 << 0.039752586122531<< arma::endr
     << -0.8371201398999021 << 0.0361724965841749<< arma::endr
     << 0.8371201398999021 << 0.0361724965841749<< arma::endr
     << -0.8714360157968963 << 0.0324342355151848<< arma::endr
     << 0.8714360157968963 << 0.0324342355151848<< arma::endr
     << -0.9019413294385253 << 0.0285541507006434<< arma::endr
     << 0.9019413294385253 << 0.0285541507006434<< arma::endr
     << -0.9285026930123607 << 0.0245492116596588<< arma::endr
     << 0.9285026930123607 << 0.0245492116596588<< arma::endr
     << -0.9510039692577085 << 0.0204369381476684<< arma::endr
     << 0.9510039692577085 << 0.0204369381476684<< arma::endr
     << -0.9693467873265645 << 0.0162353331464331<< arma::endr
     << 0.9693467873265645 << 0.0162353331464331<< arma::endr
     << -0.9834510030716237 << 0.0119628484643123<< arma::endr
     << 0.9834510030716237 << 0.0119628484643123<< arma::endr
     << -0.9932552109877686 << 0.0076386162958488<< arma::endr
     << 0.9932552109877686 << 0.0076386162958488<< arma::endr
     << -0.9987187285842121 << 0.003287453842528<< arma::endr
     << 0.9987187285842121 << 0.003287453842528 << arma::endr;
    } else if ( n ==48 ) {
    nw  << -0.0323801709628694 << 0.0647376968126839<< arma::endr
     << 0.0323801709628694 << 0.0647376968126839<< arma::endr
     << -0.0970046992094627 << 0.0644661644359501<< arma::endr
     << 0.0970046992094627 << 0.0644661644359501<< arma::endr
     << -0.1612223560688917 << 0.0639242385846482<< arma::endr
     << 0.1612223560688917 << 0.0639242385846482<< arma::endr
     << -0.2247637903946891 << 0.063114192286254<< arma::endr
     << 0.2247637903946891 << 0.063114192286254<< arma::endr
     << -0.2873624873554556 << 0.0620394231598927<< arma::endr
     << 0.2873624873554556 << 0.0620394231598927<< arma::endr
     << -0.3487558862921608 << 0.0607044391658939<< arma::endr
     << 0.3487558862921608 << 0.0607044391658939<< arma::endr
     << -0.4086864819907167 << 0.0591148396983956<< arma::endr
     << 0.4086864819907167 << 0.0591148396983956<< arma::endr
     << -0.4669029047509584 << 0.0572772921004032<< arma::endr
     << 0.4669029047509584 << 0.0572772921004032<< arma::endr
     << -0.523160974722233 << 0.0551995036999842<< arma::endr
     << 0.523160974722233 << 0.0551995036999842<< arma::endr
     << -0.5772247260839727 << 0.0528901894851937<< arma::endr
     << 0.5772247260839727 << 0.0528901894851937<< arma::endr
     << -0.6288673967765136 << 0.0503590355538545<< arma::endr
     << 0.6288673967765136 << 0.0503590355538545<< arma::endr
     << -0.6778723796326639 << 0.0476166584924905<< arma::endr
     << 0.6778723796326639 << 0.0476166584924905<< arma::endr
     << -0.7240341309238146 << 0.0446745608566943<< arma::endr
     << 0.7240341309238146 << 0.0446745608566943<< arma::endr
     << -0.7671590325157404 << 0.0415450829434647<< arma::endr
     << 0.7671590325157404 << 0.0415450829434647<< arma::endr
     << -0.8070662040294426 << 0.0382413510658307<< arma::endr
     << 0.8070662040294426 << 0.0382413510658307<< arma::endr
     << -0.8435882616243935 << 0.0347772225647704<< arma::endr
     << 0.8435882616243935 << 0.0347772225647704<< arma::endr
     << -0.8765720202742479 << 0.0311672278327981<< arma::endr
     << 0.8765720202742479 << 0.0311672278327981<< arma::endr
     << -0.9058791367155696 << 0.0274265097083569<< arma::endr
     << 0.9058791367155696 << 0.0274265097083569<< arma::endr
     << -0.9313866907065543 << 0.0235707608393244<< arma::endr
     << 0.9313866907065543 << 0.0235707608393244<< arma::endr
     << -0.9529877031604309 << 0.0196161604573555<< arma::endr
     << 0.9529877031604309 << 0.0196161604573555<< arma::endr
     << -0.9705915925462473 << 0.0155793157229438<< arma::endr
     << 0.9705915925462473 << 0.0155793157229438<< arma::endr
     << -0.9841245837228269 << 0.0114772345792345<< arma::endr
     << 0.9841245837228269 << 0.0114772345792345<< arma::endr
     << -0.9935301722663508 << 0.0073275539012763<< arma::endr
     << 0.9935301722663508 << 0.0073275539012763<< arma::endr
     << -0.9987710072524261 << 0.0031533460523058<< arma::endr
     << 0.9987710072524261 << 0.0031533460523058 << arma::endr;
    } else if ( n ==49 ) {
    nw  << 0.0 << 0.0634632814047906<< arma::endr
     << -0.0634206849826868 << 0.0633355092964917<< arma::endr
     << 0.0634206849826868 << 0.0633355092964917<< arma::endr
     << -0.126585997269672 << 0.0629527074651957<< arma::endr
     << 0.126585997269672 << 0.0629527074651957<< arma::endr
     << -0.1892415924618136 << 0.0623164173200573<< arma::endr
     << 0.1892415924618136 << 0.0623164173200573<< arma::endr
     << -0.2511351786125773 << 0.0614292009791929<< arma::endr
     << 0.2511351786125773 << 0.0614292009791929<< arma::endr
     << -0.3120175321197488 << 0.060294630953152<< arma::endr
     << 0.3120175321197488 << 0.060294630953152<< arma::endr
     << -0.3716435012622849 << 0.0589172757600273<< arma::endr
     << 0.3716435012622849 << 0.0589172757600273<< arma::endr
     << -0.4297729933415765 << 0.0573026815301875<< arma::endr
     << 0.4297729933415765 << 0.0573026815301875<< arma::endr
     << -0.486171941452492 << 0.0554573496748036<< arma::endr
     << 0.486171941452492 << 0.0554573496748036<< arma::endr
     << -0.5406132469917261 << 0.053388710708259<< arma::endr
     << 0.5406132469917261 << 0.053388710708259<< arma::endr
     << -0.5928776941089007 << 0.0511050943301446<< arma::endr
     << 0.5928776941089007 << 0.0511050943301446<< arma::endr
     << -0.6427548324192377 << 0.0486156958878282<< arma::endr
     << 0.6427548324192377 << 0.0486156958878282<< arma::endr
     << -0.6900438244251321 << 0.0459305393555959<< arma::endr
     << 0.6900438244251321 << 0.0459305393555959<< arma::endr
     << -0.7345542542374027 << 0.0430604369812596<< arma::endr
     << 0.7345542542374027 << 0.0430604369812596<< arma::endr
     << -0.7761068943454467 << 0.040016945766373<< arma::endr
     << 0.7761068943454467 << 0.040016945766373<< arma::endr
     << -0.8145344273598555 << 0.0368123209630007<< arma::endr
     << 0.8145344273598555 << 0.0368123209630007<< arma::endr
     << -0.8496821198441658 << 0.0334594667916222<< arma::endr
     << 0.8496821198441658 << 0.0334594667916222<< arma::endr
     << -0.8814084455730089 << 0.0299718846205838<< arma::endr
     << 0.8814084455730089 << 0.0299718846205838<< arma::endr
     << -0.9095856558280733 << 0.026363618927066<< arma::endr
     << 0.9095856558280733 << 0.026363618927066<< arma::endr
     << -0.9341002947558101 << 0.0226492015874467<< arma::endr
     << 0.9341002947558101 << 0.0226492015874467<< arma::endr
     << -0.9548536586741372 << 0.0188435958530895<< arma::endr
     << 0.9548536586741372 << 0.0188435958530895<< arma::endr
     << -0.9717622009015554 << 0.0149621449356247<< arma::endr
     << 0.9717622009015554 << 0.0149621449356247<< arma::endr
     << -0.984757895914213 << 0.0110205510315936<< arma::endr
     << 0.984757895914213 << 0.0110205510315936<< arma::endr
     << -0.9937886619441678 << 0.0070350995900865<< arma::endr
     << 0.9937886619441678 << 0.0070350995900865<< arma::endr
     << -0.9988201506066354 << 0.0030272789889229<< arma::endr
     << 0.9988201506066354 << 0.0030272789889229 << arma::endr;
    } else if ( n ==50 ) {
    nw  << -0.0310983383271889 << 0.0621766166553473<< arma::endr
     << 0.0310983383271889 << 0.0621766166553473<< arma::endr
     << -0.0931747015600861 << 0.0619360674206832<< arma::endr
     << 0.0931747015600861 << 0.0619360674206832<< arma::endr
     << -0.1548905899981459 << 0.0614558995903167<< arma::endr
     << 0.1548905899981459 << 0.0614558995903167<< arma::endr
     << -0.2160072368760418 << 0.0607379708417702<< arma::endr
     << 0.2160072368760418 << 0.0607379708417702<< arma::endr
     << -0.276288193779532 << 0.0597850587042655<< arma::endr
     << 0.276288193779532 << 0.0597850587042655<< arma::endr
     << -0.3355002454194373 << 0.0586008498132224<< arma::endr
     << 0.3355002454194373 << 0.0586008498132224<< arma::endr
     << -0.3934143118975651 << 0.0571899256477284<< arma::endr
     << 0.3934143118975651 << 0.0571899256477284<< arma::endr
     << -0.4498063349740388 << 0.0555577448062125<< arma::endr
     << 0.4498063349740388 << 0.0555577448062125<< arma::endr
     << -0.5044581449074642 << 0.0537106218889962<< arma::endr
     << 0.5044581449074642 << 0.0537106218889962<< arma::endr
     << -0.5571583045146501 << 0.0516557030695811<< arma::endr
     << 0.5571583045146501 << 0.0516557030695811<< arma::endr
     << -0.6077029271849502 << 0.0494009384494663<< arma::endr
     << 0.6077029271849502 << 0.0494009384494663<< arma::endr
     << -0.6558964656854394 << 0.0469550513039484<< arma::endr
     << 0.6558964656854394 << 0.0469550513039484<< arma::endr
     << -0.7015524687068222 << 0.0443275043388033<< arma::endr
     << 0.7015524687068222 << 0.0443275043388033<< arma::endr
     << -0.7444943022260685 << 0.0415284630901477<< arma::endr
     << 0.7444943022260685 << 0.0415284630901477<< arma::endr
     << -0.7845558329003993 << 0.0385687566125877<< arma::endr
     << 0.7845558329003993 << 0.0385687566125877<< arma::endr
     << -0.821582070859336 << 0.0354598356151462<< arma::endr
     << 0.821582070859336 << 0.0354598356151462<< arma::endr
     << -0.8554297694299461 << 0.032213728223578<< arma::endr
     << 0.8554297694299461 << 0.032213728223578<< arma::endr
     << -0.8859679795236131 << 0.0288429935805352<< arma::endr
     << 0.8859679795236131 << 0.0288429935805352<< arma::endr
     << -0.9130785566557919 << 0.0253606735700124<< arma::endr
     << 0.9130785566557919 << 0.0253606735700124<< arma::endr
     << -0.936656618944878 << 0.0217802431701248<< arma::endr
     << 0.936656618944878 << 0.0217802431701248<< arma::endr
     << -0.9566109552428079 << 0.0181155607134894<< arma::endr
     << 0.9566109552428079 << 0.0181155607134894<< arma::endr
     << -0.972864385106692 << 0.0143808227614856<< arma::endr
     << 0.972864385106692 << 0.0143808227614856<< arma::endr
     << -0.9853540840480058 << 0.010590548383651<< arma::endr
     << 0.9853540840480058 << 0.010590548383651<< arma::endr
     << -0.9940319694320907 << 0.0067597991957454<< arma::endr
     << 0.9940319694320907 << 0.0067597991957454<< arma::endr
     << -0.998866404420071 << 0.0029086225531551<< arma::endr
     << 0.998866404420071 << 0.0029086225531551 << arma::endr;
    } else if ( n ==51 ) {
    nw  << 0.0 << 0.0609989248412059<< arma::endr
     << -0.0609611001505787 << 0.0608854648448563<< arma::endr
     << 0.0609611001505787 << 0.0608854648448563<< arma::endr
     << -0.1216954210188888 << 0.0605455069347378<< arma::endr
     << 0.1216954210188888 << 0.0605455069347378<< arma::endr
     << -0.1819770269570775 << 0.0599803157775033<< arma::endr
     << 0.1819770269570775 << 0.0599803157775033<< arma::endr
     << -0.2415816664477987 << 0.0591919939229615<< arma::endr
     << 0.2415816664477987 << 0.0591919939229615<< arma::endr
     << -0.3002876063353319 << 0.0581834739825921<< arma::endr
     << 0.3002876063353319 << 0.0581834739825921<< arma::endr
     << -0.3578764566884095 << 0.0569585077202587<< arma::endr
     << 0.3578764566884095 << 0.0569585077202587<< arma::endr
     << -0.4141339832263039 << 0.0555216520957387<< arma::endr
     << 0.4141339832263039 << 0.0555216520957387<< arma::endr
     << -0.468850904286041 << 0.0538782523130456<< arma::endr
     << 0.468850904286041 << 0.0538782523130456<< arma::endr
     << -0.5218236693661858 << 0.0520344219366971<< arma::endr
     << 0.5218236693661858 << 0.0520344219366971<< arma::endr
     << -0.5728552163513039 << 0.0499970201500574<< arma::endr
     << 0.5728552163513039 << 0.0499970201500574<< arma::endr
     << -0.6217557046007233 << 0.0477736262406231<< arma::endr
     << 0.6217557046007233 << 0.0477736262406231<< arma::endr
     << -0.66834322117537 << 0.0453725114076501<< arma::endr
     << 0.66834322117537 << 0.0453725114076501<< arma::endr
     << -0.7124444575770367 << 0.0428026079978801<< arma::endr
     << 0.7124444575770367 << 0.0428026079978801<< arma::endr
     << -0.7538953544853755 << 0.0400734762854965<< arma::endr
     << 0.7538953544853755 << 0.0400734762854965<< arma::endr
     << -0.7925417120993812 << 0.0371952689232603<< arma::endr
     << 0.7925417120993812 << 0.0371952689232603<< arma::endr
     << -0.8282397638230649 << 0.0341786932041883<< arma::endr
     << 0.8282397638230649 << 0.0341786932041883<< arma::endr
     << -0.8608567111822923 << 0.03103497129016<< arma::endr
     << 0.8608567111822923 << 0.03103497129016<< arma::endr
     << -0.8902712180295274 << 0.0277757985941625<< arma::endr
     << 0.8902712180295274 << 0.0277757985941625<< arma::endr
     << -0.9163738623097802 << 0.0244133005737814<< arma::endr
     << 0.9163738623097802 << 0.0244133005737814<< arma::endr
     << -0.9390675440029623 << 0.0209599884017032<< arma::endr
     << 0.9390675440029623 << 0.0209599884017032<< arma::endr
     << -0.9582678486139082 << 0.0174287147234011<< arma::endr
     << 0.9582678486139082 << 0.0174287147234011<< arma::endr
     << -0.9739033680193239 << 0.0138326340064778<< arma::endr
     << 0.9739033680193239 << 0.0138326340064778<< arma::endr
     << -0.9859159917359029 << 0.0101851912978217<< arma::endr
     << 0.9859159917359029 << 0.0101851912978217<< arma::endr
     << -0.9942612604367526 << 0.0065003377832526<< arma::endr
     << 0.9942612604367526 << 0.0065003377832526<< arma::endr
     << -0.9989099908489035 << 0.0027968071710899<< arma::endr
     << 0.9989099908489035 << 0.0027968071710899 << arma::endr;
    } else if ( n ==52 ) {
    nw  << -0.0299141097973388 << 0.0598103657452919<< arma::endr
     << 0.0299141097973388 << 0.0598103657452919<< arma::endr
     << -0.0896352446489006 << 0.0595962601712482<< arma::endr
     << 0.0896352446489006 << 0.0595962601712482<< arma::endr
     << -0.1490355086069492 << 0.059168815466043<< arma::endr
     << 0.1490355086069492 << 0.059168815466043<< arma::endr
     << -0.2079022641563661 << 0.0585295617718139<< arma::endr
     << 0.2079022641563661 << 0.0585295617718139<< arma::endr
     << -0.2660247836050018 << 0.0576807874525268<< arma::endr
     << 0.2660247836050018 << 0.0576807874525268<< arma::endr
     << -0.3231950034348078 << 0.0566255309023686<< arma::endr
     << 0.3231950034348078 << 0.0566255309023686<< arma::endr
     << -0.3792082691160937 << 0.0553675696693027<< arma::endr
     << 0.3792082691160937 << 0.0553675696693027<< arma::endr
     << -0.4338640677187617 << 0.0539114069327573<< arma::endr
     << 0.4338640677187617 << 0.0539114069327573<< arma::endr
     << -0.4869667456980961 << 0.052262255383907<< arma::endr
     << 0.4869667456980961 << 0.052262255383907<< arma::endr
     << -0.5383262092858274 << 0.0504260185663424<< arma::endr
     << 0.5383262092858274 << 0.0504260185663424<< arma::endr
     << -0.5877586049795791 << 0.0484092697440749<< arma::endr
     << 0.5877586049795791 << 0.0484092697440749<< arma::endr
     << -0.6350869776952459 << 0.0462192283727848<< arma::endr
     << 0.6350869776952459 << 0.0462192283727848<< arma::endr
     << -0.6801419042271677 << 0.0438637342590004<< arma::endr
     << 0.6801419042271677 << 0.0438637342590004<< arma::endr
     << -0.7227620997499832 << 0.0413512195005603<< arma::endr
     << 0.7227620997499832 << 0.0413512195005603<< arma::endr
     << -0.7627949951937449 << 0.038690678310424<< arma::endr
     << 0.7627949951937449 << 0.038690678310424<< arma::endr
     << -0.8000972834304684 << 0.0358916348350972<< arma::endr
     << 0.8000972834304684 << 0.0358916348350972<< arma::endr
     << -0.8345354323267345 << 0.0329641090897188<< arma::endr
     << 0.8345354323267345 << 0.0329641090897188<< arma::endr
     << -0.8659861628460676 << 0.0299185811471439<< arma::endr
     << 0.8659861628460676 << 0.0299185811471439<< arma::endr
     << -0.8943368905344953 << 0.026765953746504<< arma::endr
     << 0.8943368905344953 << 0.026765953746504<< arma::endr
     << -0.9194861289164246 << 0.0235175135539845<< arma::endr
     << 0.9194861289164246 << 0.0235175135539845<< arma::endr
     << -0.9413438536413591 << 0.0201848915079808<< arma::endr
     << 0.9413438536413591 << 0.0201848915079808<< arma::endr
     << -0.9598318269330866 << 0.0167800233963007<< arma::endr
     << 0.9598318269330866 << 0.0167800233963007<< arma::endr
     << -0.9748838842217445 << 0.013315114982341<< arma::endr
     << 0.9748838842217445 << 0.013315114982341<< arma::endr
     << -0.9864461956515499 << 0.0098026345794628<< arma::endr
     << 0.9864461956515499 << 0.0098026345794628<< arma::endr
     << -0.9944775909292161 << 0.0062555239629733<< arma::endr
     << 0.9944775909292161 << 0.0062555239629733<< arma::endr
     << -0.9989511111039503 << 0.0026913169500471<< arma::endr
     << 0.9989511111039503 << 0.0026913169500471 << arma::endr;
    } else if ( n ==53 ) {
    nw  << 0.0 << 0.0587187941511644<< arma::endr
     << -0.0586850543002595 << 0.0586175862327203<< arma::endr
     << 0.0586850543002595 << 0.0586175862327203<< arma::endr
     << -0.1171678090719551 << 0.058314311362256<< arma::endr
     << 0.1171678090719551 << 0.058314311362256<< arma::endr
     << -0.1752466621553257 << 0.0578100149917132<< arma::endr
     << 0.1752466621553257 << 0.0578100149917132<< arma::endr
     << -0.2327214037242726 << 0.0571064355362672<< arma::endr
     << 0.2327214037242726 << 0.0571064355362672<< arma::endr
     << -0.2893939064516262 << 0.0562059983817397<< arma::endr
     << 0.2893939064516262 << 0.0562059983817397<< arma::endr
     << -0.3450688084957224 << 0.0551118075239336<< arma::endr
     << 0.3450688084957224 << 0.0551118075239336<< arma::endr
     << -0.399554186953953 << 0.053827634868731<< arma::endr
     << 0.399554186953953 << 0.053827634868731<< arma::endr
     << -0.4526622194618458 << 0.0523579072298727<< arma::endr
     << 0.4526622194618458 << 0.0523579072298727<< arma::endr
     << -0.5042098316571334 << 0.0507076910692927<< arma::endr
     << 0.5042098316571334 << 0.0507076910692927<< arma::endr
     << -0.5540193282770679 << 0.0488826750326991<< arma::endr
     << 0.5540193282770679 << 0.0488826750326991<< arma::endr
     << -0.6019190057137693 << 0.0468891503407503<< arma::endr
     << 0.6019190057137693 << 0.0468891503407503<< arma::endr
     << -0.64774374391651 << 0.0447339891036728<< arma::endr
     << 0.64774374391651 << 0.0447339891036728<< arma::endr
     << -0.6913355756013667 << 0.04242462063452<< arma::endr
     << 0.6913355756013667 << 0.04242462063452<< arma::endr
     << -0.7325442308075103 << 0.0399690058435404<< arma::endr
     << 0.7325442308075103 << 0.0399690058435404<< arma::endr
     << -0.7712276549255324 << 0.0373756098034829<< arma::endr
     << 0.7712276549255324 << 0.0373756098034829<< arma::endr
     << -0.8072524984168955 << 0.0346533725835342<< arma::endr
     << 0.8072524984168955 << 0.0346533725835342<< arma::endr
     << -0.8404945765458014 << 0.0318116784590193<< arma::endr
     << 0.8404945765458014 << 0.0318116784590193<< arma::endr
     << -0.8708392975582413 << 0.0288603236178237<< arma::endr
     << 0.8708392975582413 << 0.0288603236178237<< arma::endr
     << -0.8981820578754266 << 0.0258094825107575<< arma::endr
     << 0.8981820578754266 << 0.0258094825107575<< arma::endr
     << -0.9224286030428122 << 0.0226696730570702<< arma::endr
     << 0.9224286030428122 << 0.0226696730570702<< arma::endr
     << -0.9434953534644419 << 0.0194517211076369<< arma::endr
     << 0.9434953534644419 << 0.0194517211076369<< arma::endr
     << -0.9613096946231363 << 0.0161667252566875<< arma::endr
     << 0.9613096946231363 << 0.0161667252566875<< arma::endr
     << -0.9758102337149845 << 0.0128260261442404<< arma::endr
     << 0.9758102337149845 << 0.0128260261442404<< arma::endr
     << -0.9869470350233716 << 0.0094412022849403<< arma::endr
     << 0.9869470350233716 << 0.0094412022849403<< arma::endr
     << -0.9946819193080071 << 0.0060242762269487<< arma::endr
     << 0.9946819193080071 << 0.0060242762269487<< arma::endr
     << -0.9989899477763282 << 0.002591683720567<< arma::endr
     << 0.9989899477763282 << 0.002591683720567 << arma::endr;
    } else if ( n ==54 ) {
    nw  << -0.0288167481993418 << 0.057617536707147<< arma::endr
     << 0.0288167481993418 << 0.057617536707147<< arma::endr
     << -0.0863545182632482 << 0.0574261370541121<< arma::endr
     << 0.0863545182632482 << 0.0574261370541121<< arma::endr
     << -0.1436054273162561 << 0.0570439735587946<< arma::endr
     << 0.1436054273162561 << 0.0570439735587946<< arma::endr
     << -0.2003792936062136 << 0.056472315730626<< arma::endr
     << 0.2003792936062136 << 0.056472315730626<< arma::endr
     << -0.2564875200699973 << 0.05571306256059<< arma::endr
     << 0.2564875200699973 << 0.05571306256059<< arma::endr
     << -0.3117437208344682 << 0.054768736213058<< arma::endr
     << 0.3117437208344682 << 0.054768736213058<< arma::endr
     << -0.3659643403721912 << 0.0536424736475536<< arma::endr
     << 0.3659643403721912 << 0.0536424736475536<< arma::endr
     << -0.4189692632552045 << 0.0523380161982987<< arma::endr
     << 0.4189692632552045 << 0.0523380161982987<< arma::endr
     << -0.4705824124813823 << 0.0508596971461881<< arma::endr
     << 0.4705824124813823 << 0.0508596971461881<< arma::endr
     << -0.520632334385933 << 0.0492124273245289<< arma::endr
     << 0.520632334385933 << 0.0492124273245289<< arma::endr
     << -0.5689527681952095 << 0.047401678806445<< arma::endr
     << 0.5689527681952095 << 0.047401678806445<< arma::endr
     << -0.6153831983311274 << 0.0454334667282767<< arma::endr
     << 0.6153831983311274 << 0.0454334667282767<< arma::endr
     << -0.6597693876319831 << 0.043314329309597<< arma::endr
     << 0.6597693876319831 << 0.043314329309597<< arma::endr
     << -0.7019638897191729 << 0.041051306136645<< arma::endr
     << 0.7019638897191729 << 0.041051306136645<< arma::endr
     << -0.7418265388091844 << 0.0386519147821025<< arma::endr
     << 0.7418265388091844 << 0.0386519147821025<< arma::endr
     << -0.779224915346254 << 0.0361241258403836<< arma::endr
     << 0.779224915346254 << 0.0361241258403836<< arma::endr
     << -0.8140347859135678 << 0.0334763364643726<< arma::endr
     << 0.8140347859135678 << 0.0334763364643726<< arma::endr
     << -0.846140515970773 << 0.0307173424978707<< arma::endr
     << 0.846140515970773 << 0.0307173424978707<< arma::endr
     << -0.8754354540655689 << 0.0278563093105959<< arma::endr
     << 0.8754354540655689 << 0.0278563093105959<< arma::endr
     << -0.9018222862847016 << 0.0249027414672088<< arma::endr
     << 0.9018222862847016 << 0.0249027414672088<< arma::endr
     << -0.9252133598666515 << 0.0218664514228531<< arma::endr
     << 0.9252133598666515 << 0.0218664514228531<< arma::endr
     << -0.9455309751649958 << 0.0187575276214694<< arma::endr
     << 0.9455309751649958 << 0.0187575276214694<< arma::endr
     << -0.9627076457859236 << 0.0155863030359241<< arma::endr
     << 0.9627076457859236 << 0.0155863030359241<< arma::endr
     << -0.9766863288579032 << 0.0123633281288476<< arma::endr
     << 0.9766863288579032 << 0.0123633281288476<< arma::endr
     << -0.9874206373973435 << 0.0090993694555094<< arma::endr
     << 0.9874206373973435 << 0.0090993694555094<< arma::endr
     << -0.9948751170183389 << 0.00580561101524<< arma::endr
     << 0.9948751170183389 << 0.00580561101524<< arma::endr
     << -0.999026666867341 << 0.0024974818357616<< arma::endr
     << 0.999026666867341 << 0.0024974818357616 << arma::endr;
    } else if ( n ==55 ) {
    nw  << 0.0 << 0.0566029764445604<< arma::endr
     << -0.0565727538183368 << 0.056512318249772<< arma::endr
     << 0.0565727538183368 << 0.056512318249772<< arma::endr
     << -0.1129642880593293 << 0.0562406340710844<< arma::endr
     << 0.1129642880593293 << 0.0562406340710844<< arma::endr
     << -0.1689939636468732 << 0.0557887941952841<< arma::endr
     << 0.1689939636468732 << 0.0557887941952841<< arma::endr
     << -0.2244823006478455 << 0.0551582460025087<< arma::endr
     << 0.2244823006478455 << 0.0551582460025087<< arma::endr
     << -0.2792515532008065 << 0.0543510093299111<< arma::endr
     << 0.2792515532008065 << 0.0543510093299111<< arma::endr
     << -0.3331262788900239 << 0.0533696700016055<< arma::endr
     << 0.3331262788900239 << 0.0533696700016055<< arma::endr
     << -0.3859339007409794 << 0.0522173715456321<< arma::endr
     << 0.3859339007409794 << 0.0522173715456321<< arma::endr
     << -0.4375052600371746 << 0.050897805124494<< arma::endr
     << 0.4375052600371746 << 0.050897805124494<< arma::endr
     << -0.4876751581874741 << 0.0494151977115517<< arma::endr
     << 0.4876751581874741 << 0.0494151977115517<< arma::endr
     << -0.5362828859083433 << 0.0477742985512007<< arma::endr
     << 0.5362828859083433 << 0.0477742985512007<< arma::endr
     << -0.5831727380260321 << 0.0459803639462838<< arma::endr
     << 0.5831727380260321 << 0.0459803639462838<< arma::endr
     << -0.6281945122499282 << 0.0440391404216066<< arma::endr
     << 0.6281945122499282 << 0.0440391404216066<< arma::endr
     << -0.6712039903198264 << 0.0419568463177188<< arma::endr
     << 0.6712039903198264 << 0.0419568463177188<< arma::endr
     << -0.7120633999866378 << 0.0397401518743372<< arma::endr
     << 0.7120633999866378 << 0.0397401518743372<< arma::endr
     << -0.7506418563480219 << 0.0373961578679655<< arma::endr
     << 0.7506418563480219 << 0.0373961578679655<< arma::endr
     << -0.7868157811276224 << 0.0349323728735899<< arma::endr
     << 0.7868157811276224 << 0.0349323728735899<< arma::endr
     << -0.8204692985593209 << 0.0323566892261858<< arma::endr
     << 0.8204692985593209 << 0.0323566892261858<< arma::endr
     << -0.8514946066171545 << 0.029677357765161<< arma::endr
     << 0.8514946066171545 << 0.029677357765161<< arma::endr
     << -0.8797923224198955 << 0.0269029614563963<< arma::endr
     << 0.8797923224198955 << 0.0269029614563963<< arma::endr
     << -0.905271800744 << 0.0240423880097256<< arma::endr
     << 0.905271800744 << 0.0240423880097256<< arma::endr
     << -0.9278514247207917 << 0.0211048016680165<< arma::endr
     << 0.9278514247207917 << 0.0211048016680165<< arma::endr
     << -0.9474588680412107 << 0.0180996145207291<< arma::endr
     << 0.9474588680412107 << 0.0180996145207291<< arma::endr
     << -0.9640313285931352 << 0.0150364583335118<< arma::endr
     << 0.9640313285931352 << 0.0150364583335118<< arma::endr
     << -0.9775157355039892 << 0.0119251607198486<< arma::endr
     << 0.9775157355039892 << 0.0119251607198486<< arma::endr
     << -0.9878689411988892 << 0.0087757461070585<< arma::endr
     << 0.9878689411988892 << 0.0087757461070585<< arma::endr
     << -0.9950579778474119 << 0.0055986322665608<< arma::endr
     << 0.9950579778474119 << 0.0055986322665608<< arma::endr
     << -0.9990614195648185 << 0.0024083236199798<< arma::endr
     << 0.9990614195648185 << 0.0024083236199798 << arma::endr;
    } else if ( n ==56 ) {
    nw  << -0.0277970352872754 << 0.0555797463065144<< arma::endr
     << 0.0277970352872754 << 0.0555797463065144<< arma::endr
     << -0.0833051868224354 << 0.0554079525032451<< arma::endr
     << 0.0833051868224354 << 0.0554079525032451<< arma::endr
     << -0.1385558468103762 << 0.0550648959017624<< arma::endr
     << 0.1385558468103762 << 0.0550648959017624<< arma::endr
     << -0.1933782386352753 << 0.0545516368708894<< arma::endr
     << 0.1933782386352753 << 0.0545516368708894<< arma::endr
     << -0.2476029094343372 << 0.0538697618657145<< arma::endr
     << 0.2476029094343372 << 0.0538697618657145<< arma::endr
     << -0.3010622538672207 << 0.0530213785240108<< arma::endr
     << 0.3010622538672207 << 0.0530213785240108<< arma::endr
     << -0.3535910321749545 << 0.0520091091517414<< arma::endr
     << 0.3535910321749545 << 0.0520091091517414<< arma::endr
     << -0.4050268809270913 << 0.0508360826177985<< arma::endr
     << 0.4050268809270913 << 0.0508360826177985<< arma::endr
     << -0.4552108148784596 << 0.0495059246830476<< arma::endr
     << 0.4552108148784596 << 0.0495059246830476<< arma::endr
     << -0.5039877183843817 << 0.0480227467936003<< arma::endr
     << 0.5039877183843817 << 0.0480227467936003<< arma::endr
     << -0.5512068248555346 << 0.0463911333730019<< arma::endr
     << 0.5512068248555346 << 0.0463911333730019<< arma::endr
     << -0.5967221827706634 << 0.0446161276526923<< arma::endr
     << 0.5967221827706634 << 0.0446161276526923<< arma::endr
     << -0.6403931068070069 << 0.0427032160846671<< arma::endr
     << 0.6403931068070069 << 0.0427032160846671<< arma::endr
     << -0.6820846126944704 << 0.0406583113847445<< arma::endr
     << 0.6820846126944704 << 0.0406583113847445<< arma::endr
     << -0.7216678344501881 << 0.0384877342592477<< arma::endr
     << 0.7216678344501881 << 0.0384877342592477<< arma::endr
     << -0.7590204227051289 << 0.0361981938723152<< arma::endr
     << 0.7590204227051289 << 0.0361981938723152<< arma::endr
     << -0.7940269228938664 << 0.0337967671156118<< arma::endr
     << 0.7940269228938664 << 0.0337967671156118<< arma::endr
     << -0.8265791321428817 << 0.0312908767473104<< arma::endr
     << 0.8265791321428817 << 0.0312908767473104<< arma::endr
     << -0.8565764337627486 << 0.0286882684738227<< arma::endr
     << 0.8565764337627486 << 0.0286882684738227<< arma::endr
     << -0.8839261083278276 << 0.025996987058392<< arma::endr
     << 0.8839261083278276 << 0.025996987058392<< arma::endr
     << -0.9085436204206555 << 0.0232253515625653<< arma::endr
     << 0.9085436204206555 << 0.0232253515625653<< arma::endr
     << -0.9303528802474963 << 0.0203819298824026<< arma::endr
     << 0.9303528802474963 << 0.0203819298824026<< arma::endr
     << -0.9492864795619627 << 0.0174755129114009<< arma::endr
     << 0.9492864795619627 << 0.0174755129114009<< arma::endr
     << -0.9652859019054901 << 0.0145150892780215<< arma::endr
     << 0.9652859019054901 << 0.0145150892780215<< arma::endr
     << -0.9783017091402564 << 0.0115098243403834<< arma::endr
     << 0.9783017091402564 << 0.0115098243403834<< arma::endr
     << -0.9882937155401615 << 0.0084690631633079<< arma::endr
     << 0.9882937155401615 << 0.0084690631633079<< arma::endr
     << -0.9952312260810697 << 0.0054025222460153<< arma::endr
     << 0.9952312260810697 << 0.0054025222460153<< arma::endr
     << -0.9990943438014656 << 0.0023238553757732<< arma::endr
     << 0.9990943438014656 << 0.0023238553757732 << arma::endr;
    } else if ( n ==57 ) {
    nw  << 0.0 << 0.054634328756584<< arma::endr
     << -0.0546071510016468 << 0.0545528036047619<< arma::endr
     << 0.0546071510016468 << 0.0545528036047619<< arma::endr
     << -0.1090513328087878 << 0.0543084714524986<< arma::endr
     << 0.1090513328087878 << 0.0543084714524986<< arma::endr
     << -0.1631700625912643 << 0.0539020614832986<< arma::endr
     << 0.1631700625912643 << 0.0539020614832986<< arma::endr
     << -0.216801828796124 << 0.0533347865848192<< arma::endr
     << 0.216801828796124 << 0.0533347865848192<< arma::endr
     << -0.2697865731618387 << 0.0526083397291774<< arma::endr
     << 0.2697865731618387 << 0.0526083397291774<< arma::endr
     << -0.3219661683953786 << 0.0517248889205178<< arma::endr
     << 0.3219661683953786 << 0.0517248889205178<< arma::endr
     << -0.3731848900865944 << 0.0506870707249274<< arma::endr
     << 0.3731848900865944 << 0.0506870707249274<< arma::endr
     << -0.4232898814515639 << 0.0494979824020197<< arma::endr
     << 0.4232898814515639 << 0.0494979824020197<< arma::endr
     << -0.4721316095179757 << 0.0481611726616877<< arma::endr
     << 0.4721316095179757 << 0.0481611726616877<< arma::endr
     << -0.5195643113911876 << 0.0466806310736415<< arma::endr
     << 0.5195643113911876 << 0.0466806310736415<< arma::endr
     << -0.5654464292692367 << 0.0450607761613812<< arma::endr
     << 0.5654464292692367 << 0.0450607761613812<< arma::endr
     << -0.6096410329087154 << 0.0433064422162152<< arma::endr
     << 0.6096410329087154 << 0.0433064422162152<< arma::endr
     << -0.6520162282809769 << 0.0414228648708011<< arma::endr
     << 0.6520162282809769 << 0.0414228648708011<< arma::endr
     << -0.6924455511995178 << 0.0394156654754801<< arma::endr
     << 0.6924455511995178 << 0.0394156654754801<< arma::endr
     << -0.7308083447445233 << 0.0372908343244173<< arma::endr
     << 0.7308083447445233 << 0.0372908343244173<< arma::endr
     << -0.7669901193594502 << 0.0350547127823126<< arma::endr
     << 0.7669901193594502 << 0.0350547127823126<< arma::endr
     << -0.8008828945472183 << 0.0327139743663716<< arma::endr
     << 0.8008828945472183 << 0.0327139743663716<< arma::endr
     << -0.8323855211504391 << 0.030275604842694<< arma::endr
     << 0.8323855211504391 << 0.030275604842694<< arma::endr
     << -0.8614039832620469 << 0.0277468814021802<< arma::endr
     << 0.8614039832620469 << 0.0277468814021802<< arma::endr
     << -0.8878516788822214 << 0.0251353509909181<< arma::endr
     << 0.8878516788822214 << 0.0251353509909181<< arma::endr
     << -0.9116496785213912 << 0.0224488078907764<< arma::endr
     << 0.9116496785213912 << 0.0224488078907764<< arma::endr
     << -0.9327269610671017 << 0.0196952706994885<< arma::endr
     << 0.9327269610671017 << 0.0196952706994885<< arma::endr
     << -0.9510206264478768 << 0.0168829590234416<< arma::endr
     << 0.9510206264478768 << 0.0168829590234416<< arma::endr
     << -0.9664760851718867 << 0.0140202707907536<< arma::endr
     << 0.9664760851718867 << 0.0140202707907536<< arma::endr
     << -0.9790472267094688 << 0.011115763732336<< arma::endr
     << 0.9790472267094688 << 0.011115763732336<< arma::endr
     << -0.988696577650222 << 0.0081781600678212<< arma::endr
     << 0.988696577650222 << 0.0081781600678212<< arma::endr
     << -0.9953955236784303 << 0.0052165334747188<< arma::endr
     << 0.9953955236784303 << 0.0052165334747188<< arma::endr
     << -0.9991255656252629 << 0.0022437538722507<< arma::endr
     << 0.9991255656252629 << 0.0022437538722507 << arma::endr;
    } else if ( n ==58 ) {
    nw  << -0.0268470123659424 << 0.0536811198633348<< arma::endr
     << 0.0268470123659424 << 0.0536811198633348<< arma::endr
     << -0.0804636302141427 << 0.0535263433040583<< arma::endr
     << 0.0804636302141427 << 0.0535263433040583<< arma::endr
     << -0.1338482505954668 << 0.053217236446579<< arma::endr
     << 0.1338482505954668 << 0.053217236446579<< arma::endr
     << -0.1868469518357613 << 0.0527546905263708<< arma::endr
     << 0.1868469518357613 << 0.0527546905263708<< arma::endr
     << -0.2393069249661535 << 0.0521400391836698<< arma::endr
     << 0.2393069249661535 << 0.0521400391836698<< arma::endr
     << -0.2910769143111092 << 0.0513750546182857<< arma::endr
     << 0.2910769143111092 << 0.0513750546182857<< arma::endr
     << -0.3420076535979953 << 0.0504619424799531<< arma::endr
     << 0.3420076535979953 << 0.0504619424799531<< arma::endr
     << -0.3919522963307531 << 0.0494033355089624<< arma::endr
     << 0.3919522963307531 << 0.0494033355089624<< arma::endr
     << -0.4407668391868396 << 0.0482022859454177<< arma::endr
     << 0.4407668391868396 << 0.0482022859454177<< arma::endr
     << -0.4883105372167185 << 0.0468622567290263<< arma::endr
     << 0.4883105372167185 << 0.0468622567290263<< arma::endr
     << -0.5344463096488475 << 0.0453871115148198<< arma::endr
     << 0.5344463096488475 << 0.0453871115148198<< arma::endr
     << -0.579041135130225 << 0.0437811035336403<< arma::endr
     << 0.579041135130225 << 0.0437811035336403<< arma::endr
     << -0.6219664352630792 << 0.0420488633295821<< arma::endr
     << 0.6219664352630792 << 0.0420488633295821<< arma::endr
     << -0.6630984453321253 << 0.0401953854098678<< arma::endr
     << 0.6630984453321253 << 0.0401953854098678<< arma::endr
     << -0.7023185711539082 << 0.0382260138458584<< arma::endr
     << 0.7023185711539082 << 0.0382260138458584<< arma::endr
     << -0.7395137310200423 << 0.0361464268670873<< arma::endr
     << 0.7395137310200423 << 0.0361464268670873<< arma::endr
     << -0.7745766817496528 << 0.033962620493416<< arma::endr
     << 0.7745766817496528 << 0.033962620493416<< arma::endr
     << -0.8074063279130882 << 0.0316808912538093<< arma::endr
     << 0.8074063279130882 << 0.0316808912538093<< arma::endr
     << -0.8379080133393734 << 0.0293078180441605<< arma::endr
     << 0.8379080133393734 << 0.0293078180441605<< arma::endr
     << -0.8659937940748075 << 0.0268502431819819<< arma::endr
     << 0.8659937940748075 << 0.0268502431819819<< arma::endr
     << -0.8915826920220302 << 0.024315252724964<< arma::endr
     << 0.8915826920220302 << 0.024315252724964<< arma::endr
     << -0.9146009285643525 << 0.0217101561401462<< arma::endr
     << 0.9146009285643525 << 0.0217101561401462<< arma::endr
     << -0.9349821375882593 << 0.0190424654618934<< arma::endr
     << 0.9349821375882593 << 0.0190424654618934<< arma::endr
     << -0.9526675575188691 << 0.016319874234971<< arma::endr
     << 0.9526675575188691 << 0.016319874234971<< arma::endr
     << -0.9676062025029241 << 0.0135502371129888<< arma::endr
     << 0.9676062025029241 << 0.0135502371129888<< arma::endr
     << -0.9797550146943503 << 0.0107415535328788<< arma::endr
     << 0.9797550146943503 << 0.0107415535328788<< arma::endr
     << -0.9890790082484426 << 0.0079019738499987<< arma::endr
     << 0.9890790082484426 << 0.0079019738499987<< arma::endr
     << -0.9955514765972909 << 0.0050399816126502<< arma::endr
     << 0.9955514765972909 << 0.0050399816126502<< arma::endr
     << -0.9991552004073866 << 0.0021677232496275<< arma::endr
     << 0.9991552004073866 << 0.0021677232496275 << arma::endr;
    } else if ( n ==59 ) {
    nw  << 0.0 << 0.0527980126219904<< arma::endr
     << -0.05277348408831 << 0.0527244338591279<< arma::endr
     << 0.05277348408831 << 0.0527244338591279<< arma::endr
     << -0.1053998790163442 << 0.0525039026478287<< arma::endr
     << 0.1053998790163442 << 0.0525039026478287<< arma::endr
     << -0.157732505587858 << 0.0521370336483754<< arma::endr
     << 0.157732505587858 << 0.0521370336483754<< arma::endr
     << -0.2096255033920365 << 0.0516248493908915<< arma::endr
     << 0.2096255033920365 << 0.0516248493908915<< arma::endr
     << -0.2609342373428117 << 0.0509687774253939<< arma::endr
     << 0.2609342373428117 << 0.0509687774253939<< arma::endr
     << -0.3115157008030137 << 0.0501706463429969<< arma::endr
     << 0.3115157008030137 << 0.0501706463429969<< arma::endr
     << -0.3612289141697948 << 0.049232680679362<< arma::endr
     << 0.3612289141697948 << 0.049232680679362<< arma::endr
     << -0.409935317810419 << 0.0481574947146064<< arma::endr
     << 0.409935317810419 << 0.0481574947146064<< arma::endr
     << -0.4574991582532667 << 0.046948085186962<< arma::endr
     << 0.4574991582532667 << 0.046948085186962<< arma::endr
     << -0.503787866557718 << 0.0456078229405098<< arma::endr
     << 0.503787866557718 << 0.0456078229405098<< arma::endr
     << -0.5486724278083964 << 0.0441404435302974<< arma::endr
     << 0.5486724278083964 << 0.0441404435302974<< arma::endr
     << -0.5920277407040302 << 0.0425500368110676<< arma::endr
     << 0.5920277407040302 << 0.0425500368110676<< arma::endr
     << -0.6337329662388501 << 0.0408410355386867<< arma::endr
     << 0.6337329662388501 << 0.0408410355386867<< arma::endr
     << -0.6736718645049372 << 0.03901820301616<< arma::endr
     << 0.6736718645049372 << 0.03901820301616<< arma::endr
     << -0.7117331186771977 << 0.0370866198188709<< arma::endr
     << 0.7117331186771977 << 0.0370866198188709<< arma::endr
     << -0.7478106452786403 << 0.0350516696364001<< arma::endr
     << 0.7478106452786403 << 0.0350516696364001<< arma::endr
     << -0.7818038898623609 << 0.0329190242710453<< arma::endr
     << 0.7818038898623609 << 0.0329190242710453<< arma::endr
     << -0.8136181072882116 << 0.0306946278361117<< arma::endr
     << 0.8136181072882116 << 0.0306946278361117<< arma::endr
     << -0.8431646258168722 << 0.0283846802005348<< arma::endr
     << 0.8431646258168722 << 0.0283846802005348<< arma::endr
     << -0.8703610942928822 << 0.0259956197312985<< arma::endr
     << 0.8703610942928822 << 0.0259956197312985<< arma::endr
     << -0.8951317117434721 << 0.0235341053937134<< arma::endr
     << 0.8951317117434721 << 0.0235341053937134<< arma::endr
     << -0.9174074387881552 << 0.0210069982884372<< arma::endr
     << 0.9174074387881552 << 0.0210069982884372<< arma::endr
     << -0.9371261903534539 << 0.01842134275361<< arma::endr
     << 0.9371261903534539 << 0.01842134275361<< arma::endr
     << -0.9542330093769511 << 0.0157843473130815<< arma::endr
     << 0.9542330093769511 << 0.0157843473130815<< arma::endr
     << -0.9686802216817816 << 0.0131033663063452<< arma::endr
     << 0.9686802216817816 << 0.0131033663063452<< arma::endr
     << -0.9804275739567156 << 0.0103858855009959<< arma::endr
     << 0.9804275739567156 << 0.0103858855009959<< arma::endr
     << -0.989442365133731 << 0.0076395294534876<< arma::endr
     << 0.989442365133731 << 0.0076395294534876<< arma::endr
     << -0.995699640383246 << 0.0048722391682653<< arma::endr
     << 0.995699640383246 << 0.0048722391682653<< arma::endr
     << -0.9991833539092947 << 0.0020954922845412<< arma::endr
     << 0.9991833539092947 << 0.0020954922845412 << arma::endr;
    } else if ( n ==60 ) {
    nw  << -0.0259597723012478 << 0.0519078776312206<< arma::endr
     << 0.0259597723012478 << 0.0519078776312206<< arma::endr
     << -0.0778093339495366 << 0.0517679431749102<< arma::endr
     << 0.0778093339495366 << 0.0517679431749102<< arma::endr
     << -0.129449135396945 << 0.0514884515009809<< arma::endr
     << 0.129449135396945 << 0.0514884515009809<< arma::endr
     << -0.1807399648734254 << 0.0510701560698556<< arma::endr
     << 0.1807399648734254 << 0.0510701560698556<< arma::endr
     << -0.2315435513760293 << 0.0505141845325094<< arma::endr
     << 0.2315435513760293 << 0.0505141845325094<< arma::endr
     << -0.2817229374232617 << 0.0498220356905502<< arma::endr
     << 0.2817229374232617 << 0.0498220356905502<< arma::endr
     << -0.3311428482684482 << 0.0489955754557568<< arma::endr
     << 0.3311428482684482 << 0.0489955754557568<< arma::endr
     << -0.379670056576798 << 0.0480370318199712<< arma::endr
     << 0.379670056576798 << 0.0480370318199712<< arma::endr
     << -0.4271737415830784 << 0.0469489888489122<< arma::endr
     << 0.4271737415830784 << 0.0469489888489122<< arma::endr
     << -0.4735258417617071 << 0.0457343797161145<< arma::endr
     << 0.4735258417617071 << 0.0457343797161145<< arma::endr
     << -0.5186014000585697 << 0.0443964787957871<< arma::endr
     << 0.5186014000585697 << 0.0443964787957871<< arma::endr
     << -0.5622789007539445 << 0.0429388928359356<< arma::endr
     << 0.5622789007539445 << 0.0429388928359356<< arma::endr
     << -0.6044405970485104 << 0.0413655512355848<< arma::endr
     << 0.6044405970485104 << 0.0413655512355848<< arma::endr
     << -0.644972828489477 << 0.0396806954523808<< arma::endr
     << 0.644972828489477 << 0.0396806954523808<< arma::endr
     << -0.6837663273813555 << 0.0378888675692434<< arma::endr
     << 0.6837663273813555 << 0.0378888675692434<< arma::endr
     << -0.7207165133557304 << 0.0359948980510845<< arma::endr
     << 0.7207165133557304 << 0.0359948980510845<< arma::endr
     << -0.7557237753065856 << 0.0340038927249464<< arma::endr
     << 0.7557237753065856 << 0.0340038927249464<< arma::endr
     << -0.7886937399322641 << 0.0319212190192963<< arma::endr
     << 0.7886937399322641 << 0.0319212190192963<< arma::endr
     << -0.8195375261621458 << 0.0297524915007889<< arma::endr
     << 0.8195375261621458 << 0.0297524915007889<< arma::endr
     << -0.8481719847859296 << 0.0275035567499248<< arma::endr
     << 0.8481719847859296 << 0.0275035567499248<< arma::endr
     << -0.8745199226468983 << 0.0251804776215212<< arma::endr
     << 0.8745199226468983 << 0.0251804776215212<< arma::endr
     << -0.898510310810046 << 0.0227895169439978<< arma::endr
     << 0.898510310810046 << 0.0227895169439978<< arma::endr
     << -0.9200784761776275 << 0.0203371207294573<< arma::endr
     << 0.9200784761776275 << 0.0203371207294573<< arma::endr
     << -0.9391662761164232 << 0.0178299010142077<< arma::endr
     << 0.9391662761164232 << 0.0178299010142077<< arma::endr
     << -0.9557222558399961 << 0.0152746185967848<< arma::endr
     << 0.9557222558399961 << 0.0152746185967848<< arma::endr
     << -0.9697017887650528 << 0.012678166476816<< arma::endr
     << 0.9697017887650528 << 0.012678166476816<< arma::endr
     << -0.9810672017525982 << 0.010047557182288<< arma::endr
     << 0.9810672017525982 << 0.010047557182288<< arma::endr
     << -0.9897878952222218 << 0.0073899311633455<< arma::endr
     << 0.9897878952222218 << 0.0073899311633455<< arma::endr
     << -0.9958405251188381 << 0.0047127299269536<< arma::endr
     << 0.9958405251188381 << 0.0047127299269536<< arma::endr
     << -0.9992101232274361 << 0.0020268119688738<< arma::endr
     << 0.9992101232274361 << 0.0020268119688738 << arma::endr;
    } else if ( n ==61 ) {
    nw  << 0.0 << 0.0510811194407862<< arma::endr
     << -0.0510589067079743 << 0.0510144870386973<< arma::endr
     << 0.0510589067079743 << 0.0510144870386973<< arma::endr
     << -0.1019846065622741 << 0.0508147636688183<< arma::endr
     << 0.1019846065622741 << 0.0508147636688183<< arma::endr
     << -0.1526442402308153 << 0.0504824703867974<< arma::endr
     << 0.1526442402308153 << 0.0504824703867974<< arma::endr
     << -0.2029056425180585 << 0.0500184741081783<< arma::endr
     << 0.2029056425180585 << 0.0500184741081783<< arma::endr
     << -0.2526376871690535 << 0.0494239853467356<< arma::endr
     << 0.2526376871690535 << 0.0494239853467356<< arma::endr
     << -0.3017106289630307 << 0.0487005550564115<< arma::endr
     << 0.3017106289630307 << 0.0487005550564115<< arma::endr
     << -0.3499964422040668 << 0.0478500705850956<< arma::endr
     << 0.3499964422040668 << 0.0478500705850956<< arma::endr
     << -0.3973691547257566 << 0.0468747507508091<< arma::endr
     << 0.3973691547257566 << 0.0468747507508091<< arma::endr
     << -0.4437051765385316 << 0.045777140053146<< arma::endr
     << 0.4437051765385316 << 0.045777140053146<< arma::endr
     << -0.4888836222622521 << 0.0445601020350835<< arma::endr
     << 0.4888836222622521 << 0.0445601020350835<< arma::endr
     << -0.5327866265029253 << 0.0432268118124961<< arma::endr
     << 0.5327866265029253 << 0.0432268118124961<< arma::endr
     << -0.5752996513508306 << 0.0417807477908885<< arma::endr
     << 0.5752996513508306 << 0.0417807477908885<< arma::endr
     << -0.6163117851979217 << 0.0402256825909982<< arma::endr
     << 0.6163117851979217 << 0.0402256825909982<< arma::endr
     << -0.6557160320950709 << 0.0385656732070082<< arma::endr
     << 0.6557160320950709 << 0.0385656732070082<< arma::endr
     << -0.6934095908944912 << 0.0368050504231548<< arma::endr
     << 0.6934095908944912 << 0.0368050504231548<< arma::endr
     << -0.7292941234494651 << 0.0349484075165334<< arma::endr
     << 0.7292941234494651 << 0.0349484075165334<< arma::endr
     << -0.7632760111723123 << 0.0330005882759074<< arma::endr
     << 0.7632760111723123 << 0.0330005882759074<< arma::endr
     << -0.7952665992823597 << 0.0309666743683974<< arma::endr
     << 0.7952665992823597 << 0.0309666743683974<< arma::endr
     << -0.8251824281086599 << 0.0288519720881834<< arma::endr
     << 0.8251824281086599 << 0.0288519720881834<< arma::endr
     << -0.8529454508476635 << 0.0266619985241509<< arma::endr
     << 0.8529454508476635 << 0.0266619985241509<< arma::endr
     << -0.8784832372148811 << 0.0244024671875442<< arma::endr
     << 0.8784832372148811 << 0.0244024671875442<< arma::endr
     << -0.9017291624740011 << 0.022079273148319<< arma::endr
     << 0.9017291624740011 << 0.022079273148319<< arma::endr
     << -0.9226225813829553 << 0.0196984777461012<< arma::endr
     << 0.9226225813829553 << 0.0196984777461012<< arma::endr
     << -0.9411089866813611 << 0.0172662929876137<< arma::endr
     << 0.9411089866813611 << 0.0172662929876137<< arma::endr
     << -0.9571401519129841 << 0.0147890658849379<< arma::endr
     << 0.9571401519129841 << 0.0147890658849379<< arma::endr
     << -0.9706742588331829 << 0.0122732635078121<< arma::endr
     << 0.9706742588331829 << 0.0122732635078121<< arma::endr
     << -0.981676011284037 << 0.0097254618303561<< arma::endr
     << 0.981676011284037 << 0.0097254618303561<< arma::endr
     << -0.990116745232517 << 0.0071523549917491<< arma::endr
     << 0.990116745232517 << 0.0071523549917491<< arma::endr
     << -0.9959745998151203 << 0.0045609240060124<< arma::endr
     << 0.9959745998151203 << 0.0045609240060124<< arma::endr
     << -0.9992355976313635 << 0.0019614533616703<< arma::endr
     << 0.9992355976313635 << 0.0019614533616703 << arma::endr;
    } else if ( n ==62 ) {
    nw  << -0.0251292914218206 << 0.0502480003752563<< arma::endr
     << 0.0251292914218206 << 0.0502480003752563<< arma::endr
     << -0.0753243954962343 << 0.0501210695690433<< arma::endr
     << 0.0753243954962343 << 0.0501210695690433<< arma::endr
     << -0.1253292236158968 << 0.0498675285949524<< arma::endr
     << 0.1253292236158968 << 0.0498675285949524<< arma::endr
     << -0.1750174592490156 << 0.0494880179196993<< arma::endr
     << 0.1750174592490156 << 0.0494880179196993<< arma::endr
     << -0.2242635856041655 << 0.0489834962205178<< arma::endr
     << 0.2242635856041655 << 0.0489834962205178<< arma::endr
     << -0.2729432026967263 << 0.0483552379634777<< arma::endr
     << 0.2729432026967263 << 0.0483552379634777<< arma::endr
     << -0.320933341594194 << 0.0476048301841012<< arma::endr
     << 0.320933341594194 << 0.0476048301841012<< arma::endr
     << -0.3681127750465645 << 0.0467341684784155<< arma::endr
     << 0.3681127750465645 << 0.0467341684784155<< arma::endr
     << -0.4143623237171261 << 0.0457454522145702<< arma::endr
     << 0.4143623237171261 << 0.0457454522145702<< arma::endr
     << -0.4595651572401134 << 0.0446411789771244<< arma::endr
     << 0.4595651572401134 << 0.0446411789771244<< arma::endr
     << -0.503607089344756 << 0.0434241382580474<< arma::endr
     << 0.503607089344756 << 0.0434241382580474<< arma::endr
     << -0.5463768663002511 << 0.0420974044103851<< arma::endr
     << 0.5463768663002511 << 0.0420974044103851<< arma::endr
     << -0.5877664479530873 << 0.0406643288824174<< arma::endr
     << 0.5877664479530873 << 0.0406643288824174<< arma::endr
     << -0.6276712806468852 << 0.0391285317519631<< arma::endr
     << 0.6276712806468852 << 0.0391285317519631<< arma::endr
     << -0.6659905613354794 << 0.03749389258228<< arma::endr
     << 0.6659905613354794 << 0.03749389258228<< arma::endr
     << -0.702627492222297 << 0.0357645406227681<< arma::endr
     << 0.702627492222297 << 0.0357645406227681<< arma::endr
     << -0.7374895252831567 << 0.0339448443794105<< arma::endr
     << 0.7374895252831567 << 0.0339448443794105<< arma::endr
     << -0.7704885960554193 << 0.0320394005816247<< arma::endr
     << 0.7704885960554193 << 0.0320394005816247<< arma::endr
     << -0.8015413461039764 << 0.0300530225739899<< arma::endr
     << 0.8015413461039764 << 0.0300530225739899<< arma::endr
     << -0.8305693336040049 << 0.0279907281633146<< arma::endr
     << 0.8305693336040049 << 0.0279907281633146<< arma::endr
     << -0.857499231512071 << 0.0258577269540247<< arma::endr
     << 0.857499231512071 << 0.0258577269540247<< arma::endr
     << -0.8822630128318973 << 0.0236594072086828<< arma::endr
     << 0.8822630128318973 << 0.0236594072086828<< arma::endr
     << -0.9047981225210935 << 0.02140132227767<< arma::endr
     << 0.9047981225210935 << 0.02140132227767<< arma::endr
     << -0.9250476356362037 << 0.0190891766585732<< arma::endr
     << 0.9250476356362037 << 0.0190891766585732<< arma::endr
     << -0.9429604013923285 << 0.0167288117901773<< arma::endr
     << 0.9429604013923285 << 0.0167288117901773<< arma::endr
     << -0.958491172973927 << 0.0143261918238065<< arma::endr
     << 0.958491172973927 << 0.0143261918238065<< arma::endr
     << -0.9716007233716518 << 0.0118873901170105<< arma::endr
     << 0.9716007233716518 << 0.0118873901170105<< arma::endr
     << -0.9822559490972367 << 0.0094185794284204<< arma::endr
     << 0.9822559490972367 << 0.0094185794284204<< arma::endr
     << -0.9904299711892903 << 0.006926041901831<< arma::endr
     << 0.9904299711892903 << 0.006926041901831<< arma::endr
     << -0.9961022963162671 << 0.0044163334569309<< arma::endr
     << 0.9961022963162671 << 0.0044163334569309<< arma::endr
     << -0.999259859308777 << 0.0018992056795137<< arma::endr
     << 0.999259859308777 << 0.0018992056795137 << arma::endr;
    } else if ( n ==63 ) {
    nw  << 0.0 << 0.049472366623931<< arma::endr
     << -0.0494521871161596 << 0.0494118330399182<< arma::endr
     << 0.0494521871161596 << 0.0494118330399182<< arma::endr
     << -0.0987833564469453 << 0.0492303804237476<< arma::endr
     << 0.0987833564469453 << 0.0492303804237476<< arma::endr
     << -0.147872786357872 << 0.048928452820512<< arma::endr
     << 0.147872786357872 << 0.048928452820512<< arma::endr
     << -0.1966003467915067 << 0.0485067890978838<< arma::endr
     << 0.1966003467915067 << 0.0485067890978838<< arma::endr
     << -0.2448467932459534 << 0.0479664211379951<< arma::endr
     << 0.2448467932459534 << 0.0479664211379951<< arma::endr
     << -0.2924940585862514 << 0.0473086713122689<< arma::endr
     << 0.2924940585862514 << 0.0473086713122689<< arma::endr
     << -0.3394255419745844 << 0.0465351492453837<< arma::endr
     << 0.3394255419745844 << 0.0465351492453837<< arma::endr
     << -0.3855263942122479 << 0.0456477478762926<< arma::endr
     << 0.3855263942122479 << 0.0456477478762926<< arma::endr
     << -0.4306837987951116 << 0.0446486388259414<< arma::endr
     << 0.4306837987951116 << 0.0446486388259414<< arma::endr
     << -0.4747872479948044 << 0.0435402670830276<< arma::endr
     << 0.4747872479948044 << 0.0435402670830276<< arma::endr
     << -0.5177288132900333 << 0.0423253450208158<< arma::endr
     << 0.5177288132900333 << 0.0423253450208158<< arma::endr
     << -0.559403409486285 << 0.0410068457596664<< arma::endr
     << 0.559403409486285 << 0.0410068457596664<< arma::endr
     << -0.5997090518776252 << 0.0395879958915441<< arma::endr
     << 0.5997090518776252 << 0.0395879958915441<< arma::endr
     << -0.6385471058213654 << 0.0380722675843496<< arma::endr
     << 0.6385471058213654 << 0.0380722675843496<< arma::endr
     << -0.6758225281149861 << 0.0364633700854573<< arma::endr
     << 0.6758225281149861 << 0.0364633700854573<< arma::endr
     << -0.7114440995848458 << 0.0347652406453559<< arma::endr
     << 0.7114440995848458 << 0.0347652406453559<< arma::endr
     << -0.7453246483178474 << 0.0329820348837793<< arma::endr
     << 0.7453246483178474 << 0.0329820348837793<< arma::endr
     << -0.7773812629903724 << 0.0311181166222198<< arma::endr
     << 0.7773812629903724 << 0.0311181166222198<< arma::endr
     << -0.8075354957734567 << 0.0291780472082805<< arma::endr
     << 0.8075354957734567 << 0.0291780472082805<< arma::endr
     << -0.8357135543195029 << 0.0271665743590979<< arma::endr
     << 0.8357135543195029 << 0.0271665743590979<< arma::endr
     << -0.8618464823641238 << 0.025088620553345<< arma::endr
     << 0.8618464823641238 << 0.025088620553345<< arma::endr
     << -0.8858703285078534 << 0.0229492710048899<< arma::endr
     << 0.8858703285078534 << 0.0229492710048899<< arma::endr
     << -0.9077263027785316 << 0.0207537612580391<< arma::endr
     << 0.9077263027785316 << 0.0207537612580391<< arma::endr
     << -0.9273609206218432 << 0.0185074644601613<< arma::endr
     << 0.9273609206218432 << 0.0185074644601613<< arma::endr
     << -0.9447261340410098 << 0.0162158784103383<< arma::endr
     << 0.9447261340410098 << 0.0162158784103383<< arma::endr
     << -0.9597794497589419 << 0.0138846126161156<< arma::endr
     << 0.9597794497589419 << 0.0138846126161156<< arma::endr
     << -0.97248403469757 << 0.01151937607688<< arma::endr
     << 0.97248403469757 << 0.01151937607688<< arma::endr
     << -0.9828088105937273 << 0.0091259686763267<< arma::endr
     << 0.9828088105937273 << 0.0091259686763267<< arma::endr
     << -0.9907285468921895 << 0.0067102917659601<< arma::endr
     << 0.9907285468921895 << 0.0067102917659601<< arma::endr
     << -0.9962240127779701 << 0.0042785083468638<< arma::endr
     << 0.9962240127779701 << 0.0042785083468638<< arma::endr
     << -0.9992829840291237 << 0.0018398745955771<< arma::endr
     << 0.9992829840291237 << 0.0018398745955771 << arma::endr;
    } else /*if ( n ==64 )*/ {
    nw  << -0.0243502926634244 << 0.0486909570091397<< arma::endr
     << 0.0243502926634244 << 0.0486909570091397<< arma::endr
     << -0.072993121787799 << 0.0485754674415034<< arma::endr
     << 0.072993121787799 << 0.0485754674415034<< arma::endr
     << -0.1214628192961206 << 0.048344762234803<< arma::endr
     << 0.1214628192961206 << 0.048344762234803<< arma::endr
     << -0.1696444204239928 << 0.0479993885964583<< arma::endr
     << 0.1696444204239928 << 0.0479993885964583<< arma::endr
     << -0.2174236437400071 << 0.0475401657148303<< arma::endr
     << 0.2174236437400071 << 0.0475401657148303<< arma::endr
     << -0.2646871622087674 << 0.04696818281621<< arma::endr
     << 0.2646871622087674 << 0.04696818281621<< arma::endr
     << -0.311322871990211 << 0.0462847965813144<< arma::endr
     << 0.311322871990211 << 0.0462847965813144<< arma::endr
     << -0.3572201583376681 << 0.0454916279274181<< arma::endr
     << 0.3572201583376681 << 0.0454916279274181<< arma::endr
     << -0.4022701579639916 << 0.0445905581637566<< arma::endr
     << 0.4022701579639916 << 0.0445905581637566<< arma::endr
     << -0.4463660172534641 << 0.0435837245293235<< arma::endr
     << 0.4463660172534641 << 0.0435837245293235<< arma::endr
     << -0.489403145707053 << 0.0424735151236536<< arma::endr
     << 0.489403145707053 << 0.0424735151236536<< arma::endr
     << -0.5312794640198946 << 0.0412625632426235<< arma::endr
     << 0.5312794640198946 << 0.0412625632426235<< arma::endr
     << -0.571895646202634 << 0.0399537411327203<< arma::endr
     << 0.571895646202634 << 0.0399537411327203<< arma::endr
     << -0.6111553551723933 << 0.0385501531786156<< arma::endr
     << 0.6111553551723933 << 0.0385501531786156<< arma::endr
     << -0.6489654712546573 << 0.03705512854024<< arma::endr
     << 0.6489654712546573 << 0.03705512854024<< arma::endr
     << -0.6852363130542333 << 0.0354722132568824<< arma::endr
     << 0.6852363130542333 << 0.0354722132568824<< arma::endr
     << -0.7198818501716109 << 0.0338051618371416<< arma::endr
     << 0.7198818501716109 << 0.0338051618371416<< arma::endr
     << -0.7528199072605319 << 0.0320579283548516<< arma::endr
     << 0.7528199072605319 << 0.0320579283548516<< arma::endr
     << -0.7839723589433414 << 0.0302346570724025<< arma::endr
     << 0.7839723589433414 << 0.0302346570724025<< arma::endr
     << -0.8132653151227975 << 0.0283396726142595<< arma::endr
     << 0.8132653151227975 << 0.0283396726142595<< arma::endr
     << -0.8406292962525803 << 0.0263774697150547<< arma::endr
     << 0.8406292962525803 << 0.0263774697150547<< arma::endr
     << -0.8659993981540928 << 0.0243527025687109<< arma::endr
     << 0.8659993981540928 << 0.0243527025687109<< arma::endr
     << -0.8893154459951141 << 0.0222701738083833<< arma::endr
     << 0.8893154459951141 << 0.0222701738083833<< arma::endr
     << -0.9105221370785028 << 0.0201348231535302<< arma::endr
     << 0.9105221370785028 << 0.0201348231535302<< arma::endr
     << -0.9295691721319396 << 0.0179517157756973<< arma::endr
     << 0.9295691721319396 << 0.0179517157756973<< arma::endr
     << -0.9464113748584028 << 0.0157260304760247<< arma::endr
     << 0.9464113748584028 << 0.0157260304760247<< arma::endr
     << -0.9610087996520538 << 0.0134630478967186<< arma::endr
     << 0.9610087996520538 << 0.0134630478967186<< arma::endr
     << -0.973326827789911 << 0.0111681394601311<< arma::endr
     << 0.973326827789911 << 0.0111681394601311<< arma::endr
     << -0.983336253884626 << 0.0088467598263639<< arma::endr
     << 0.983336253884626 << 0.0088467598263639<< arma::endr
     << -0.9910133714767443 << 0.0065044579689784<< arma::endr
     << 0.9910133714767443 << 0.0065044579689784<< arma::endr
     << -0.9963401167719553 << 0.0041470332605625<< arma::endr
     << 0.9963401167719553 << 0.0041470332605625<< arma::endr
     << -0.9993050417357722 << 0.0017832807216964<< arma::endr
     << 0.9993050417357722 << 0.0017832807216964 << arma::endr;
    }

    degree = n;
}

