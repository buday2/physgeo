#include "../include/geodetic_sphere.h"

//geo_sphere::geodetic_sphere()
//{
//    //ctor
//}
//
//geo_sphere::~geodetic_sphere()
//{
//    //dtor
//}

void geo_sphere::geodetic1( dms phi1 , dms lambda1, dms alpha1, double s1, double radius,
                   dms& phi2, dms& lambda2, dms& alpha2){

    double b1 = phi1.dms2rad();
    double l1 = lambda1.dms2rad();
    double a1 = alpha1.dms2rad();
    double sr = s1/radius;

    double b2 = asin(sin(b1)*cos(sr)+ cos(b1)*sin(sr)*cos(a1)); //rad
    double deltal = geo_sphere::signum( sin(a1) )* abs(acos((cos(sr)-sin(b1)*sin(b2))/(cos(b1)*cos(b2))));

    double a = (cos(b1)*sin(a1)/(cos(b2)));
    double b = -1.*cos(a1)*cos(deltal) + sin(a1)*sin(deltal)*sin(b1);

    double a2 = 2.*M_PI - atan2(a,b);

    geo_sphere::get_right_interval( a1 );
    geo_sphere::get_right_interval( a2 );

    phi2.rad2dms( b2 );
    lambda2.rad2dms( l1+deltal );
    alpha2.rad2dms( a2 );
}


void  geo_sphere::geodetic2(dms phi1,dms lambda1, dms phi2, dms lambda2,
                                 double R, dms &alpha1, dms &alpha2, double &s1){
    //converting units to rad
    double b1 = phi1.dms2rad(); // rad
    double l1 = lambda1.dms2rad(); // rad
    double b2 = phi2.dms2rad(); // rad
    double l2 = lambda2.dms2rad(); // rad

    // cumputations
    double dlambda = l2 - l1 ;// rad
    s1 = acos(sin(b1)*sin(b2)+cos(b1)*cos(b2)*cos(dlambda)) ;// rad
    double a = atan2(cos((b1-b2)/2.),sin((b2+b1)/2.)*tan(dlambda/2.)); // rad
    double b = atan2(sin((b1-b2)/2.),cos((b2+b1)/2.)*tan(dlambda/2)); // rad

//    double a = atan2(cos((b2-b1)/2)/tan(dlambda/2),sin((b2+b1)/2)); // rad
//    double b = atan2(sin((b2-b1)/2)/tan(dlambda/2),cos((b2+b1)/2)); // rad

    double a1 = (a+b); //rad
    double a2 = 2.*M_PI - (a-b); //rad
    // Conditions azimuth \f$ \in [0 , 2 \pi] \f$
    geo_sphere::get_right_interval( a1 );
    geo_sphere::get_right_interval( a2 );

    // results
    alpha1.rad2dms(a1); //DMS
    alpha2.rad2dms(a2); //DMS
    s1 *= R; // [m]
}

void geo_sphere::geodetic1(const double &phi1,
                           const double &lambda1,
                           const double &alpha1,
                           const double &s1,
                           const double &radius,
                           double &phi2,
                           double &lambda2,
                           double &alpha2)
{
    double b1 = phi1 * DEG2RAD;
    double l1 = lambda1 * DEG2RAD;
    double a1 = alpha1 * DEG2RAD;
    double sr = s1/radius;

    double b2 = asin(sin(b1)*cos(sr)+ cos(b1)*sin(sr)*cos(a1)); //rad
    double deltal = signum( sin(a1) )* abs(acos((cos(sr)-sin(b1)*sin(b2))/(cos(b1)*cos(b2))));

    double a = (cos(b1)*sin(a1)/(cos(b2)));
    double b = -1.*cos(a1)*cos(deltal) + sin(a1)*sin(deltal)*sin(b1);

    double a2 = 2.*M_PI - atan2(a,b);

    get_right_interval( a1 );
    get_right_interval( a2 );

    phi2 =  b2 * RAD2DEG;
    lambda2 = RAD2DEG*(l1+deltal );
    alpha2 = a2  * RAD2DEG;
}

void geo_sphere::geodetic2(const double &phi1,
                           const double &lambda1,
                           const double &phi2,
                           const double &lambda2,
                           const double &R,
                           double &alpha1,
                           double &alpha2,
                           double &s1)
{
    double b1 = phi1 * DEG2RAD; // rad
    double l1 = lambda1 * DEG2RAD; // rad
    double b2 = phi2 * DEG2RAD; // rad
    double l2 = lambda2 * DEG2RAD; // rad

    // cumputations
    double dlambda = l2 - l1 ;// rad
    s1 = acos(sin(b1)*sin(b2)+cos(b1)*cos(b2)*cos(dlambda)) ;// rad
    double a = atan2(cos((b1-b2)/2.),sin((b2+b1)/2.)*tan(dlambda/2.)); // rad
    double b = atan2(sin((b1-b2)/2.),cos((b2+b1)/2.)*tan(dlambda/2)); // rad

//    double a = atan2(cos((b2-b1)/2)/tan(dlambda/2),sin((b2+b1)/2)); // rad
//    double b = atan2(sin((b2-b1)/2)/tan(dlambda/2),cos((b2+b1)/2)); // rad

    double a1 = (a+b); //rad
    double a2 = 2.*M_PI - (a-b); //rad
    // Conditions azimuth €<0,2%pi>
    get_right_interval( a1 );
    get_right_interval( a2 );

    // results
    alpha1 = RAD2DEG*a1; //DEG
    alpha2 = RAD2DEG*a2; //DEG
    s1 *= R; // [m]
}
