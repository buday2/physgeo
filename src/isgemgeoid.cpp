﻿#include "../include/isgemgeoid.h"


inline bool f_exists(const char* fname) {
  if (access(fname, F_OK) != -1) {
    return true;
  } else {
    return false;
  }
}


Isgemgeoid::Isgemgeoid() {

}

Isgemgeoid::~Isgemgeoid() {
    // Virtual destructor
    this->geoid_model.reset();
}

void Isgemgeoid::reset()
{
    this->geoid_model.reset();
}

bool Isgemgeoid::load_isgem_model ( const string& path ) {
    ifstream fmodel( path );

    if ( fmodel.is_open() ) {
        vector<string> v; // vector with words in loaded lines
        string line, word;
        string data_reading; data_reading.assign("header");

        //int rowsn, colsn;
        while ( getline (fmodel, line ) ) {
            if ( data_reading.compare("header") == 0 ) {
                istringstream is ( line );

                while ( is >> word ) {
                    v.push_back( word );
                } // spliting <string> line to separate words

                if ( line.find("model name") != string::npos  || // line.find("model name") != string::npos ||  line.find("model_name") != string::npos
                     line.find("model_name") != string::npos  ) {
                    for ( unsigned int i = 3; i < v.size(); i++ ) {
                        model_name.append( v[i] );
                        model_name.append( " " );// + (" ").c_str();
                    }
                } else if ( line.rfind("model type") != string::npos ) {
                    this->model_type =(v[3]).c_str();
                } else if ( line.rfind("units") != string::npos  ) {
                    this->units = v[2].c_str();
                } else if ( line.rfind("reference") != string::npos ) {
                    this->ref_ell = (v[2]).c_str();
                } else if ( line.rfind( "lat min" ) != string::npos ||
                            line.rfind( "lat_min" ) != string::npos) {
                    this->lat_min = stod((v[ v.size() - 1 ]).c_str());
                } else if ( line.rfind( "lat max" ) != string::npos  ||
                            line.rfind( "lat_max" ) != string::npos) {
                    this->lat_max= stod((v[ v.size() - 1 ]).c_str());
                } else if ( line.rfind( "lon min" ) != string::npos  ||
                            line.rfind( "lon_min" ) != string::npos ) {
                    this->lon_min = stod((v[v.size() - 1]).c_str());
                } else if ( line.rfind( "lon max" ) != string::npos ||
                            line.rfind( "lon_max" ) != string::npos) {
                    this->lon_max = stod((v[v.size() - 1]).c_str());
                } else if ( line.rfind( "delta lon" ) != string::npos ||
                            line.rfind( "delta_lon" ) != string::npos ) {
                    this->delta_lon = stod((v[v.size() - 1]).c_str());
                } else if ( line.rfind( "delta lat" ) != string::npos ||
                            line.rfind( "delta_lat" ) != string::npos ) {
                    this->delta_lat = stod((v[v.size() - 1]).c_str());
                } else if ( line.rfind("nrows") != string::npos ) {
                    nrows = static_cast<unsigned int>( stoi( v[v.size() - 1] ) );

                    //rowsn = nrows-1;
                } else if ( line.rfind( "ncols" ) != string::npos ) {
                    ncols =  static_cast<unsigned int>( stoi( v[v.size() - 1] ) );
                } else if ( line.rfind( "nodata" ) != string::npos ) {
                    nodata = stod(v[v.size() - 1].c_str());
                } else if ( line.rfind( "ISG format" ) != string::npos ) {
                    isg_format = v[v.size() - 1].c_str();
                } else if ( line.rfind("end_of_head") != string::npos) {
                    data_reading = "coefficients";

                    try  {
                        geoid_model.load( fmodel, arma::raw_ascii); // TODO!!! is there a way do it without flipping?
                        geoid_model =  arma::flipud( geoid_model );

                    } catch (...) {
                         cerr << "Unable to load data while calling function "
                              << "\"bool Isgemgeoid::load_isgem_model ( const string& path )\"."
                              << "Check the file for errors!" << endl;
                    }
                }
                v.clear();
                word.clear();
                line.clear();
            }
        }
        fmodel.close();
        return true;
    } else {
        cerr << "File \""<< path << "\" can not be opened!" << endl;
        return false;
    }

}

bool Isgemgeoid::save2hdf5( const string& path , const string& matrix_name) {
    try {
        arma::colvec header;
        header << lat_min << arma::endr
               << lat_max << arma::endr
               << lon_min << arma::endr
               << lon_max << arma::endr
               << nodata  << arma::endr
               << delta_lat << arma::endr
               << delta_lon << arma::endr
               << nrows << arma::endr
               << ncols << arma::endr;

        geoid_model.save( arma::hdf5_name(path, matrix_name, arma::hdf5_opts::append ) );
        header.save(arma::hdf5_name(path, matrix_name + "_header", arma::hdf5_opts::append));

        return true;
    } catch(...) {
        cout << "Problems with function: \"bool Isgemgeoid::save2hdf5( const string& , const string& )\" . " << endl;
        cerr << "Could not open file: " << path << " . Operation aborted!" << endl;
        return false;
    }
}

bool Isgemgeoid::read_hdf5( const string& path ) {
    ifstream fmodel( path );

    if ( fmodel.is_open() ) {
        vector<string> v; // vector with words in loaded lines
        string line, word;
        string data_reading; data_reading.assign("header");

        while ( data_reading.compare("header") == 0) {
            getline (fmodel, line );

            istringstream is ( line );

            while ( is >> word ) {
                v.push_back( word );
            } // spliting <string> line to separate words

            if ( line.find("model name") != string::npos  ||
                 line.find("model_name") != string::npos  ) {
                for ( unsigned int i = 3; i < v.size(); i++ ) {
                    model_name.append( v[i] );
                    model_name.append( " " );// + (" ").c_str();
                }
            } else if ( line.find("model type") != string::npos ) {
                model_type =(v[3]).c_str();
            } else if ( line.find("units") != string::npos  ) {
                units = v[2].c_str();
            } else if ( line.find("reference") != string::npos ) {
                ref_ell = (v[2]).c_str();
            } else if ( line.find( "lat min" ) != string::npos ||
                        line.find( "lat_min" ) != string::npos) {
                lat_min = stod((v[ v.size() - 1 ]).c_str());
            } else if ( line.find( "lat max" ) != string::npos  ||
                        line.find( "lat_max" ) != string::npos) {
                lat_max= stod((v[ v.size() - 1 ]).c_str());
            } else if ( line.find( "lon min" ) != string::npos  ||
                        line.find( "lon_min" ) != string::npos ) {
                lon_min = stod((v[v.size() - 1]).c_str());
            } else if ( line.find( "lon max" ) != string::npos ||
                        line.find( "lon_max" ) != string::npos) {
                lon_max = stod((v[v.size() - 1]).c_str());
            } else if ( line.find( "delta lon" ) != string::npos ||
                        line.find( "delta_lon" ) != string::npos ) {
                delta_lon = stod((v[v.size() - 1]).c_str());
            } else if ( line.find( "delta lat" ) != string::npos ||
                        line.find( "delta_lat" ) != string::npos ) {
                delta_lat = stod((v[v.size() - 1]).c_str());
            } else if ( line.find("nrows") != string::npos ) {
                nrows = static_cast<unsigned>( stoi( v[v.size() - 1] ));
            } else if ( line.find( "ncols" ) != string::npos ) {
                ncols = static_cast<unsigned>(stoi( v[v.size() - 1] ) );
            } else if ( line.find( "nodata" ) != string::npos ) {
                nodata = stod(v[v.size() - 1].c_str());
            } else if ( line.find( "ISG format" ) != string::npos ) {
                isg_format = v[v.size() - 1].c_str();
            } else if ( line.find("end_of_head") != string::npos) {
                data_reading = "coefficients";
            }
        }
        fmodel.close();

        geoid_model.reset();
        geoid_model.load( arma::hdf5_name(path, "gmodel") );

        return true;
    } else {
        cerr << "Function \"bool Isgemgeoid::read_hdf5( const string& path ) \" failed to load data" << endl;
        cerr << "File \""<< path << "\" can not be opened!" << endl;
        fmodel.close();
        return false;
    }
}


bool Isgemgeoid::load_model_from_ascii( const string& path,
                            unsigned int format ,
                            const char* f_model_name,
                            const char* f_model_type,
                            const char* f_units,
                            const char* f_ref_ell,
                            const char* f_isg_format,
                            double nodata_value) {
    bool f_ex = f_exists( path.c_str() );

    if ( f_ex ) {

        model_name = f_model_name;
        model_type = f_model_type;
        units      = f_units;
        ref_ell    = f_ref_ell;
        isg_format = f_isg_format;

        arma::mat data;
        data.load(path, arma::raw_ascii);

        arma::colvec b_vec, l_vec, hu_vec;

        /* reading input file based on format type */
        /*
        * b - geodetic latitude
        * l - geodetic longitude
        * hu_ell - upper layer of the "tesser"
        * hl_ell - lower layer

        * 0 - PointID b l hu_el
        * 1 - PointID l b hu_el
        * 2 - PointID b l hu_ell
        * 3 - b l hu_ell
        * 4 - l b hu_ell
        * 5 - b l hu_ell
        * 6 - l b hu_ell
        */
        if (format == 0) {
          b_vec = data.col(1);
          l_vec = data.col(2);
          hu_vec = data.col(3);
        } else if (format == 1) {
          b_vec = data.col(2);
          l_vec = data.col(1);
          hu_vec = data.col(3);
        } else if (format == 2) {
          b_vec = data.col(1);
          l_vec = data.col(2);
          hu_vec = data.col(3);
        } else if (format == 3) {
          b_vec = data.col(0);
          l_vec = data.col(1);
          hu_vec = data.col(2);
        } else if (format == 4) {
          b_vec = data.col(1);
          l_vec = data.col(0);
          hu_vec = data.col(2);
        } else if (format == 5) {
          b_vec = data.col(0);
          l_vec = data.col(1);
          hu_vec = data.col(2);
        } else if (format == 6) {
          b_vec = data.col(1);
          l_vec = data.col(0);
          hu_vec = data.col(2);
        }

        data.reset();
        data.set_size(b_vec.n_rows, 3);

        data.col(0) = b_vec;
        data.col(1) = l_vec;
        data.col(2) = hu_vec;

        b_vec.reset();
        l_vec.reset();
        hu_vec.reset();

        /* extracting basic information */
        map<double, double> latitude, longitude;

        for (unsigned int i = 0; i < data.n_rows; i++) {
          latitude[arma::as_scalar(data(i, 0))] = 1.;
          longitude[arma::as_scalar(data(i, 1))] = 1.;
        }

        map<double, double>::iterator it, iit;
        iit = latitude.begin();
        iit++;

        double latavg = 0.0;
        for (it = latitude.begin(); it != latitude.end(); it++) {
          latavg += abs(iit->first - it->first);

          iit++;
          if (iit == latitude.end())
            break;
        }

        delta_lat = latavg / static_cast<double>( latitude.size() - 1) ;

        iit = longitude.begin();
        iit++;
        double lonavg = 0.0;
        for (it = longitude.begin(); it != longitude.end(); it++) {
          lonavg += abs(iit->first - it->first);

          iit++;
          if (iit == longitude.end())
            break;
        }

        delta_lon = lonavg / static_cast<double>(longitude.size() - 1);

        lat_max = data.col(0).max();
        lat_min = data.col(0).min();
        lon_max = data.col(1).max();
        lon_min = data.col(1).min();
        nodata = nodata_value;

        nrows = latitude.size();
        ncols = longitude.size();

        if (geoid_model.is_empty()) {
          geoid_model.set_size(nrows, ncols);
        } else {
          geoid_model.reset();
          geoid_model.set_size(nrows, ncols);
        }

        geoid_model.fill(nodata);


        unsigned int m, n;
        ///# Could be parallelized easily !!
        for (unsigned int i = 0; i < data.n_rows; i++) {
          m = Isgemgeoid::conv2index((data(i, 0) - lat_min) / delta_lat);
          n = Isgemgeoid::conv2index((data(i, 1) - lon_min) / delta_lon);

          geoid_model(m, n) = data(i, 2);
        }

        return true;
    } else {
        cerr << "File \"" << path << "\" can not be opened!" << endl;
        return false;
    }
}

bool Isgemgeoid::export2ascii( const string& path, bool ptid ) {
    ofstream f_out( path );

    if ( f_out.is_open() ) {
        unsigned int pid = 1;
        double phi = lat_min, lam;
        for (unsigned long long int i = 0; i < geoid_model.n_rows; i++ ) {
            //double lam = lon_min;
            for ( unsigned long long int j = 0; j < geoid_model.n_cols; j++ ) {
                lam = this->lon_min +  static_cast<double>(j)*this->delta_lon;
                if ( arma::as_scalar( geoid_model(i,j) ) == nodata ) {
                    continue;
                } else {
                    if ( ptid ) {
                        f_out << setw(12) << setprecision(9) <<  pid++ << " " << phi << " " << lam << " " << arma::as_scalar(geoid_model(i,j)) << endl;
                    } else {
                        f_out << setw(12) << setprecision(9) << phi << " " << lam << " " << arma::as_scalar(geoid_model(i,j)) << endl;
                    }
                }
            }
            phi += this->delta_lat;
        }
        f_out.close();
        return true;
    } else {
        cerr << "File \"" << path << "\" can not be created!" << endl;
        f_out.close();
        return false;
    }
}

bool Isgemgeoid::export2_esri_grid(const string &path, bool center  )
{
    ofstream f_out( path );

    if ( f_out.is_open() ) {
        if ( center ) {
            f_out << "ncols        " << this->geoid_model.n_cols << endl
                  << "nrows        " << this->geoid_model.n_rows << endl
                  << "xllcenter    " << this->lon_min  << endl
                  << "yllcenter    " << this->lat_min  << endl
                  << "cellsize     " << this->delta_lat << endl
                  << "NODATA_value " << this->nodata   << endl;
        } else {
            f_out << "ncols        " << this->geoid_model.n_cols << endl
                  << "nrows        " << this->geoid_model.n_rows << endl
                  << "xllcorner    " << this->lon_min - this->delta_lon/2. << endl
                  << "yllcorner    " << this->lat_min - this->delta_lat/2. << endl
                  << "cellsize     " << this->delta_lat << endl
                  << "NODATA_value " << this->nodata   << endl;
        }

        for ( arma::uword i = this->geoid_model.n_rows -1 ; i == 0 ; i-- ) {
            for ( arma::uword j = 0 ; i < this->geoid_model.n_cols ; j++ ) {
                f_out << arma::as_scalar(  this->geoid_model(i,j)) << "  ";
            }
            f_out << endl;
        }

        f_out.close();
        return true;
    } else {
        cerr << "File \"" << path << "\" can not be created!" << endl;
        f_out.close();
        return false;
    }
}

//bool Isgemgeoid::load_esri_grid ( const string& path ) {
//    bool conter_value;

//    ifstream fmodel( path );
//    if ( fmodel.is_open() ) {
//        vector<string> v; // vector with words in loaded lines
//        string line, word;
//        string data_reading; data_reading.assign("header");

//        while ( data_reading.compare("header") == 0) {
//            getline (fmodel, line );

//            istringstream is ( line );

//            while ( is >> word ) {
//                v.push_back( word );
//            } // spliting <string> line to separate words

//            if ( line.find("model name") != string::npos  || // line.find("model name") != string::npos ||  line.find("model_name") != string::npos
//                 line.find("model_name") != string::npos  ) {
//                for ( unsigned int i = 3; i < v.size(); i++ ) {
//                    model_name.append( v[i] );
//                    model_name.append( " " );// + (" ").c_str();
//                }
//            } else if ( line.find("model type") != string::npos ) {
//                model_type =(v[3]).c_str();
//            } else if ( line.find("units") != string::npos  ) {
//                units = v[2].c_str();
//            } else if ( line.find("reference") != string::npos ) {
//                ref_ell = (v[2]).c_str();
//            } else if ( line.find( "lat min" ) != string::npos ||
//                        line.find( "lat_min" ) != string::npos) {
//                lat_min = stod((v[ v.size() - 1 ]).c_str());
//            } else if ( line.find( "lat max" ) != string::npos  ||
//                        line.find( "lat_max" ) != string::npos) {
//                lat_max= stod((v[ v.size() - 1 ]).c_str());
//            } else if ( line.find( "lon min" ) != string::npos  ||
//                        line.find( "lon_min" ) != string::npos ) {
//                lon_min = stod((v[v.size() - 1]).c_str());
//            } else if ( line.find( "lon max" ) != string::npos ||
//                        line.find( "lon_max" ) != string::npos) {
//                lon_max = stod((v[v.size() - 1]).c_str());
//            } else if ( line.find( "delta lon" ) != string::npos ||
//                        line.find( "delta_lon" ) != string::npos ) {
//                delta_lon = stod((v[v.size() - 1]).c_str());
//            } else if ( line.find( "delta lat" ) != string::npos ||
//                        line.find( "delta_lat" ) != string::npos ) {
//                delta_lat = stod((v[v.size() - 1]).c_str());
//            } else if ( line.find("nrows") != string::npos ) {
//                nrows = static_cast<unsigned>( stoi( v[v.size() - 1] ));
//            } else if ( line.find( "ncols" ) != string::npos ) {
//                ncols = static_cast<unsigned>(stoi( v[v.size() - 1] ) );
//            } else if ( line.find( "nodata" ) != string::npos ) {
//                nodata = stod(v[v.size() - 1].c_str());
//            } else if ( line.find( "ISG format" ) != string::npos ) {
//                isg_format = v[v.size() - 1].c_str();
//            } else if ( line.find("end_of_head") != string::npos) {
//                data_reading = "coefficients";
//            }
//        }
//        fmodel.close();

//        return true;
//    } else {
//        cerr << "Function \"bool Isgemgeoid::read_hdf5( const string& path ) \" failed to load data" << endl;
//        cerr << "File \""<< path << "\" can not be opened!" << endl;
//        fmodel.close();
//        return false;
//    }

//}

double Isgemgeoid::get_value( int i , int j  ) {
    if ( static_cast<unsigned>(i) > nrows || static_cast<unsigned>(j) > ncols ) {
        //return this->nodata; // if the data are out of the bounds
        string errmsg = "Isgemgeoid::get_value(i,j) indexes out of the bounds. i= " + to_string(i) + " ,j= " + to_string(j) +".\n";
        throw runtime_error(errmsg);

    } else {
        return arma::as_scalar( geoid_model(i,j) );
    }
}

double Isgemgeoid::get_value(int i, int j) const
{
    if (  static_cast<unsigned>(i) > nrows || static_cast<unsigned>(j) > ncols ) {
        //return this->nodata; // if the data are out of the bounds
        string errmsg = "Isgemgeoid::get_value(i,j) indexes out of the bounds. i= " + to_string(i) + " ,j= " + to_string(j) +".\n";
        throw runtime_error(errmsg);

    } else {
        return arma::as_scalar( geoid_model(i,j) );
    }
}

//Isgemgeoid &Isgemgeoid::operator=(const Isgemgeoid &model)
//{
//    if (this != &model ) {
//        this->model_name = model.model_name;
//        this->model_type = model.model_type;
//        this->units = model.units;
//        this->ref_ell = model.ref_ell;
//        this->isg_format = model.isg_format;
//        this->print_coeff = model.print_coeff;
//        this->lat_min = model.lat_min;
//        this->lat_max = model.lat_max;
//        this->lon_min = model.lon_min;
//        this->lon_max = model.lon_max;
//        this->nodata = model.nodata;
//        this->sphererad = model.sphererad;
//        this->delta_lat = model.delta_lat;
//        this->delta_lon = model.delta_lon;
//        this->nrows = model.nrows;
//        this->ncols = model.ncols;
//        this->geoid_model = model.geoid_model;
//    }
//    return  *this;
//}

//Isgemgeoid &Isgemgeoid::operator+(const double &cval)
//{
//    this->geoid_model += cval;
//    return *this;
//}
//Isgemgeoid &Isgemgeoid::operator-(const double &cval)
//{
//    this->geoid_model -= cval;
//    return *this;
//}
//Isgemgeoid &Isgemgeoid::operator*(const double &cval)
//{
//    this->geoid_model *= cval;
//    return *this;
//}
//Isgemgeoid &Isgemgeoid::operator/(const double &cval)
//{
//    this->geoid_model /= cval;
//    return *this;
//}


double Isgemgeoid::get_value( unsigned int i , unsigned int j  ) {
    if ( i > nrows || j > ncols ) {
        //return this->nodata; // if the data are out of the bounds
        string errmsg = "Isgemgeoid::get_value(i,j) indexes out of the bounds. i= " + to_string(i) + " ,j= " + to_string(j) +".\n";
        throw runtime_error(errmsg);

    } else {
        return arma::as_scalar( geoid_model(i,j) );
    }
}

double Isgemgeoid::get_value(unsigned int i, unsigned int j) const
{
    if ( i > nrows || j > ncols ) {
        //return this->nodata; // if the data are out of the bounds
        string errmsg = "Isgemgeoid::get_value(i,j) indexes out of the bounds. i= " + to_string(i) + " ,j= " + to_string(j) +".\n";
        throw runtime_error(errmsg);

    } else {
        return arma::as_scalar( geoid_model(i,j) );
    }
}

double Isgemgeoid::get_value( double phi, double lambda) {
    if ( ( phi >= lat_min && phi <= lat_max ) &&
         ( lambda >= lon_min && lambda <= lon_max) ) {

        unsigned int i = Isgemgeoid::conv2index((phi - lat_min) / delta_lat);
        unsigned int j = Isgemgeoid::conv2index((lambda- lon_min) / delta_lon);

        return arma::as_scalar( geoid_model(i,j) );
    } else {
        return nodata;
    }
}

void Isgemgeoid::get_index( const double &phi,
                            const double &lambda,
                            unsigned int &i,
                            unsigned int &j) const {

    int ii ,jj;
    ii = static_cast<int>( floor( ( phi    - this->lat_min )/ this->delta_lat ) );
    jj = static_cast<int>( floor( ( lambda - this->lon_min )/ this->delta_lon ) );


    if ( !(phi >= lat_min && phi <= lat_max && lambda >= lon_min && lambda <= lon_max))
    {
        if ( ii < 0 && jj < 0 ) {
            i = 0;
            j = 0;
        } else if ( ii < 0 && jj >= static_cast<int>(ncols) ) {
            i = 0;
            j = ncols-1;
        } else if ( ii >= static_cast<int>(nrows) && jj < 0 ) {
            i = nrows - 1;
            j = 0;
        } else {
            i = nrows - 1;
            j = ncols -1;
        }
    } else {
        if ( ii < static_cast<int>( this->geoid_model.n_rows ) ) {
            i = static_cast<unsigned int>(ii);
        } else {
            i = static_cast<unsigned int>( this->geoid_model.n_rows -1 ) ;
        }

        if ( jj < static_cast<int>( this->geoid_model.n_cols ) ) {
            j = static_cast<unsigned int>(jj);
        } else {
            j = static_cast<unsigned int>( this->geoid_model.n_cols -1 ) ;
        }
    }
}

void Isgemgeoid::get_coordinates (const unsigned int &i,
                                   const unsigned int &j,
                                   double &phi,
                                   double &lam ) const {
    phi = this->lat_min + static_cast<double>(i) * this->delta_lat;
    lam = this->lon_min + static_cast<double>(j) * this->delta_lon;
}

void Isgemgeoid::set_print_coeff ( bool in_print_coeff) {
    print_coeff = in_print_coeff;
}

double Isgemgeoid::interpolate_value( const double& phi,
                                      const double& lambda,
                                      const string& alg ,
                                      bool *isinbound  )
{
    // krieger method is applied
    if ( ( phi <= lat_max + __EPS__ ) && ( phi >= lat_min -__EPS__ ) &&
         ( lambda <= lon_max + __EPS__ ) && ( lambda >= lon_min - __EPS__  )) {

        double bcenter, lcenter, bdif,ldif, w ,pw, surface_triangle, surface, weight;
        unsigned rowidx, colidx;

        Isgemgeoid::get_index(phi, lambda,rowidx, colidx );
        Isgemgeoid::get_coordinates(rowidx, colidx, bcenter , lcenter );


        if ( rowidx == static_cast<unsigned int>( this->geoid_model.n_rows ) - 1  ) {
            --rowidx;
        }

        if ( colidx == static_cast<unsigned int>( this->geoid_model.n_cols ) - 1  ) {
            --colidx;
        }

        lcenter += delta_lon/2.;
        bcenter += delta_lat/2.;

        // === Selecting spherical triangle ===
        bdif = phi - bcenter;
        ldif = lambda - lcenter;

        arma::vec phi_list(4);
        arma::vec lam_list(4);
        arma::vec fvalues(3); // function values, values obtained from the grid

        if ( bdif < 0 && ldif < 0 ) {
            // bottom left, bottom right, upper left (bl, br, ul)
            phi_list(0) = ( bcenter - delta_lat/2. );
            phi_list(1) = ( bcenter - delta_lat/2. );
            phi_list(2) = ( bcenter + delta_lat/2. );
            phi_list(3) = ( bcenter - delta_lat/2. );

            lam_list(0) = ( lcenter - delta_lon/2. );
            lam_list(1) = ( lcenter + delta_lon/2. );
            lam_list(2) = ( lcenter - delta_lon/2. );
            lam_list(3) = ( lcenter - delta_lon/2. );

            fvalues(0) = arma::as_scalar( geoid_model(rowidx+1, colidx) );
            fvalues(1) = arma::as_scalar( geoid_model(rowidx, colidx) );
            fvalues(2) = arma::as_scalar( geoid_model(rowidx, colidx+1) );

        } else if (bdif >= 0 && ldif < 0 ) {
            // bl, ul, ur
            phi_list(0) = ( bcenter - delta_lat/2. );
            phi_list(1) = ( bcenter + delta_lat/2. );
            phi_list(2) = ( bcenter + delta_lat/2. );
            phi_list(3) = ( bcenter - delta_lat/2. );

            lam_list(0) = ( lcenter - delta_lon/2. );
            lam_list(1) = ( lcenter - delta_lon/2. );
            lam_list(2) = ( lcenter + delta_lon/2. );
            lam_list(3) = ( lcenter - delta_lon/2. );

            fvalues(0) = arma::as_scalar( geoid_model(rowidx+1, colidx+1) );
            fvalues(1) = arma::as_scalar( geoid_model(rowidx, colidx) );
            fvalues(2) = arma::as_scalar( geoid_model(rowidx+1, colidx) );
        } else if ( bdif >= 0 && ldif >= 0 ) {
            // br, ul, ur
            phi_list(0) = ( bcenter - delta_lat/2. );
            phi_list(1) = ( bcenter + delta_lat/2. );
            phi_list(2) = ( bcenter + delta_lat/2. );
            phi_list(3) = ( bcenter - delta_lat/2. );

            lam_list(0) = ( lcenter + delta_lon/2. );
            lam_list(1) = ( lcenter - delta_lon/2. );
            lam_list(2) = ( lcenter + delta_lon/2. );
            lam_list(3) = ( lcenter + delta_lon/2. );

            fvalues(0) = arma::as_scalar( geoid_model(rowidx+1, colidx+1) );
            fvalues(1) = arma::as_scalar( geoid_model(rowidx, colidx+1) );
            fvalues(2) = arma::as_scalar( geoid_model(rowidx+1, colidx) );
        } else {
            // bl, br, ur
            phi_list(0) = ( bcenter - delta_lat/2. );
            phi_list(1) = ( bcenter - delta_lat/2. );
            phi_list(2) = ( bcenter + delta_lat/2. );
            phi_list(3) = ( bcenter - delta_lat/2. );

            lam_list(0) = ( lcenter - delta_lon/2. );
            lam_list(1) = ( lcenter + delta_lon/2. );
            lam_list(2) = ( lcenter + delta_lon/2. );
            lam_list(3) = ( lcenter - delta_lon/2. );

            fvalues(0) = arma::as_scalar( geoid_model(rowidx+1, colidx+1) );
            fvalues(1) = arma::as_scalar( geoid_model(rowidx, colidx) );
            fvalues(2) = arma::as_scalar( geoid_model(rowidx, colidx+1) );
        }

        for (arma::uword x = 0 ; x < 3; x++ ) {
            if ( (arma::as_scalar( fvalues(x) ) - this->nodata) <= __EPS__ ) {
                if ( isinbound != nullptr ) {
                    *isinbound = false;
                }
                return this->nodata;
            }
        }

        if ( alg == "sphtriangle" ) {
            double s1, s2, s3;
            s1 = geo_f::length_sphere( phi_list(0), lam_list(0),  phi_list(1), lam_list(1));
            s2 = geo_f::length_sphere( phi_list(1), lam_list(1),  phi_list(2), lam_list(2));
            s3 = geo_f::length_sphere( phi_list(2), lam_list(2),  phi_list(0), lam_list(0));

            surface_triangle = geo_f::lhuilierTheorem(s1, s2, s3);

            if (abs( surface_triangle ) < 1.0e-30  ) {
                // In case that the surface of a triangle is equal to zero, we are dealing
                // with the polar regions. Changing the algorithm to \"closes_neighbour\"
                double closest_neighbour = Isgemgeoid::interpolate_value( phi, lambda, "closest_neighbour", isinbound);

//                if ( isinbound != nullptr ) {
//                    *isinbound = true;
//                }

                return closest_neighbour;
            }

            if ( !((phi >= bcenter - delta_lat/2.) && ( phi <= bcenter + delta_lat/2. )
                 && (lambda >= lcenter - delta_lon/2.) && ( lambda <= lcenter + delta_lon/2. )) ) {
                // In case that the point is outside the bounds , the zero value is returned and false statement for bool "isinbound"
                if ( isinbound != nullptr ) {
                    *isinbound = false;
                }
                return this->nodata;
            }

            w = 0.;
            pw = 0.;
            for ( unsigned i = 0 ; i < 3 ; i++ ) { // last point in vec is the same as the first one
                s1 = geo_f::length_sphere( phi_list(i), lam_list(i),  phi_list(i+1), lam_list(i+1));
                s2 = geo_f::length_sphere( phi_list(i), lam_list(i),  phi, lambda);
                s3 = geo_f::length_sphere( phi, lambda,   phi_list(i+1), lam_list(i+1));

                surface = geo_f::lhuilierTheorem(s1, s2, s3); // in radians, of given triangle
                weight = surface/surface_triangle;

                w  += weight;
                pw += weight * fvalues(i);
            }
            double result = pw/w;

            if ( isnan( result ) || isinf( result ) || abs(this->nodata - result) < __EPS__ ) {
                result = this->nodata;

                if ( isinbound != nullptr ) {
                    *isinbound = false;
                }
            } else {

                if ( isinbound != nullptr ) {
                    *isinbound = true;
                }
            }
            return result;
        } else /* if(  alg == "nearest" ) */ { // closes neighbour method
            double s1;
            w = 0.;
            pw = 0.;

            for ( unsigned i = 0 ; i < 3 ; i++ ) { // last point in vec is the same as the first one
                s1 = geo_f::length_sphere( phi, lambda , phi_list(i), lam_list(i),1.0 );
                weight = 1./(s1+__EPS__);

                w  += weight;
                pw += weight * fvalues(i);

            }
            double result = pw/w;

            if ( isnan(result) || isinf(result) ) {
                if ( isinbound != nullptr ) {
                    *isinbound = false;
                }
                return arma::as_scalar( geoid_model(rowidx,colidx) );
            } else {
                if ( isinbound != nullptr ) {
                    *isinbound = true;
                }
                return  result ;
            }
        }
    } else {
        if ( isinbound != nullptr ) {
            *isinbound = false;
        }

        return this->nodata;
    }
}

double Isgemgeoid::interpolate_value(const double& phi,
                                     const double& lambda,
                                     const string &alg,
                                     bool *isinbound) const {
    // krieger method is applied
    if ( ( phi <= lat_max ) && ( phi >= lat_min ) &&
         ( lambda <= lon_max ) && ( lambda >= lon_min  )) {

        double bcenter, lcenter, bdif,ldif, w ,pw, surface_triangle, surface, weight;
        unsigned rowidx, colidx;

        Isgemgeoid::get_index(phi, lambda,rowidx, colidx );
        Isgemgeoid::get_coordinates(rowidx, colidx, bcenter , lcenter );

        if ( rowidx == static_cast<unsigned int>( this->geoid_model.n_rows ) - 1  ) {
            --rowidx;
        }

        if ( colidx == static_cast<unsigned int>( this->geoid_model.n_cols ) - 1  ) {
            --colidx;
        }
        bcenter += delta_lat/2.;
        lcenter += delta_lon/2.;
        // === Selecting spherical triangle ===
        bdif = phi - bcenter;
        ldif = lambda - lcenter;

        arma::vec phi_list(4);
        arma::vec lam_list(4);
        arma::vec fvalues(3); // function values, values obtained from the grid

        if ( bdif < 0 && ldif < 0 ) {
            // bottom left, bottom right, upper left (bl, br, ul)
            phi_list(0) = ( bcenter - delta_lat/2. );
            phi_list(1) = ( bcenter - delta_lat/2. );
            phi_list(2) = ( bcenter + delta_lat/2. );
            phi_list(3) = ( bcenter - delta_lat/2. );

            lam_list(0) = ( lcenter - delta_lon/2. );
            lam_list(1) = ( lcenter + delta_lon/2. );
            lam_list(2) = ( lcenter - delta_lon/2. );
            lam_list(3) = ( lcenter - delta_lon/2. );

            fvalues(0) = arma::as_scalar( geoid_model(rowidx+1, colidx) );
            fvalues(1) = arma::as_scalar( geoid_model(rowidx, colidx) );
            fvalues(2) = arma::as_scalar( geoid_model(rowidx, colidx+1) );

        } else if (bdif >= 0 && ldif < 0 ) {
            // bl, ul, ur
            phi_list(0) = ( bcenter - delta_lat/2. );
            phi_list(1) = ( bcenter + delta_lat/2. );
            phi_list(2) = ( bcenter + delta_lat/2. );
            phi_list(3) = ( bcenter - delta_lat/2. );

            lam_list(0) = ( lcenter - delta_lon/2. );
            lam_list(1) = ( lcenter - delta_lon/2. );
            lam_list(2) = ( lcenter + delta_lon/2. );
            lam_list(3) = ( lcenter - delta_lon/2. );

            fvalues(0) = arma::as_scalar( geoid_model(rowidx+1, colidx+1) );
            fvalues(1) = arma::as_scalar( geoid_model(rowidx, colidx) );
            fvalues(2) = arma::as_scalar( geoid_model(rowidx+1, colidx) );
        } else if ( bdif >= 0 && ldif >= 0 ) {
            // br, ul, ur
            phi_list(0) = ( bcenter - delta_lat/2. );
            phi_list(1) = ( bcenter + delta_lat/2. );
            phi_list(2) = ( bcenter + delta_lat/2. );
            phi_list(3) = ( bcenter - delta_lat/2. );

            lam_list(0) = ( lcenter + delta_lon/2. );
            lam_list(1) = ( lcenter - delta_lon/2. );
            lam_list(2) = ( lcenter + delta_lon/2. );
            lam_list(3) = ( lcenter + delta_lon/2. );

            fvalues(0) = arma::as_scalar( geoid_model(rowidx+1, colidx+1) );
            fvalues(1) = arma::as_scalar( geoid_model(rowidx, colidx+1) );
            fvalues(2) = arma::as_scalar( geoid_model(rowidx+1, colidx) );
        } else {
            // bl, br, ur
            phi_list(0) = ( bcenter - delta_lat/2. );
            phi_list(1) = ( bcenter - delta_lat/2. );
            phi_list(2) = ( bcenter + delta_lat/2. );
            phi_list(3) = ( bcenter - delta_lat/2. );

            lam_list(0) = ( lcenter - delta_lon/2. );
            lam_list(1) = ( lcenter + delta_lon/2. );
            lam_list(2) = ( lcenter + delta_lon/2. );
            lam_list(3) = ( lcenter - delta_lon/2. );

            fvalues(0) = arma::as_scalar( geoid_model(rowidx+1, colidx+1) );
            fvalues(1) = arma::as_scalar( geoid_model(rowidx, colidx) );
            fvalues(2) = arma::as_scalar( geoid_model(rowidx, colidx+1) );
        }

        if ( alg == "sphtriangle" ) {
            double s1, s2, s3;
            s1 = geo_f::length_sphere( phi_list(0), lam_list(0),  phi_list(1), lam_list(1));
            s2 = geo_f::length_sphere( phi_list(1), lam_list(1),  phi_list(2), lam_list(2));
            s3 = geo_f::length_sphere( phi_list(2), lam_list(2),  phi_list(0), lam_list(0));

            surface_triangle = geo_f::lhuilierTheorem(s1, s2, s3);

//            if (abs( surface_triangle ) < 1.0e-30  ) {
//                // In case that the surface of a triangle is equal to zero, we are dealing
//                // with the polar regions. Changing the algorithm to \"closes_neighbour\"
//                double closest_neighbour = Isgemgeoid::interpolate_value( phi, lambda, "closest_neighbour");

//                if ( isinbound != nullptr ) {
//                    *isinbound = true;
//                }

//                return closest_neighbour;
//            }

            if ( !((phi >= bcenter - delta_lat/2.) && ( phi <= bcenter + delta_lat/2. )
                 && (lambda >= lcenter - delta_lon/2.) && ( lambda <= lcenter + delta_lon/2. )) ) {
                // In case that the point is outside the bounds , the zero value is returned and false statement for bool "isinbound"
                if ( isinbound != nullptr ) {
                    *isinbound = false;
                }
                return this->nodata;
            }

            w = 0.;
            pw = 0.;
            for ( unsigned i = 0 ; i < 3 ; i++ ) { // last point in vec is the same as the first one
                s1 = geo_f::length_sphere( phi_list(i), lam_list(i),  phi_list(i+1), lam_list(i+1));
                s2 = geo_f::length_sphere( phi_list(i), lam_list(i),  phi, lambda);
                s3 = geo_f::length_sphere( phi, lambda,   phi_list(i+1), lam_list(i+1));

                surface = geo_f::lhuilierTheorem(s1, s2, s3); // in radians, of given triangle
                weight = surface/surface_triangle;

                w  += weight;
                pw += weight * fvalues(i);
            }
            double result = pw/w;

            if ( isnan( result ) || isinf( result ) || abs(this->nodata - result) < __EPS__ ) {
                result = this->nodata;

                if ( isinbound != nullptr ) {
                    *isinbound = false;
                }
            } else {
                if ( isinbound != nullptr ) {
                    *isinbound = true;
                }
            }
            return result;
        } else { // closes neighbour method
            double s1;

            w = 0.;
            pw = 0.;
            for ( unsigned i = 0 ; i < 3 ; i++ ) { // last point in vec is the same as the first one
                s1 = geo_f::length_sphere( phi, lambda , phi_list(i), lam_list(i) );
                weight = 1./(s1+__EPS__);

                w  += weight;
                pw += weight * fvalues(i);
            }

            double result = pw/w;

            if ( isnan(result) || isinf(result) ) {
                return arma::as_scalar( geoid_model(rowidx,colidx) );
            } else {
                return  geoid_model(rowidx,colidx) ;
            }
        }
    } else {
        if ( isinbound != nullptr ) {
            *isinbound = false;
        }
        return 0.0;
    }
}

arma::mat Isgemgeoid::interpolate_grid(const arma::vec &phi_vec, const arma::vec &lam_vec)
{
    unsigned n = phi_vec.n_elem;
    unsigned m = lam_vec.n_elem;

    arma::mat interpolated_grid_data(n,m);

    for (unsigned i = 0; i < n; i++) {
        //#pragma omp parallel for shared( phi_vec, lam_vec, i )
        for (unsigned j = 0; j < m ; j++ ) {
            interpolated_grid_data(i,j) = Isgemgeoid::interpolate_value( phi_vec(i)*RAD2DEG, lam_vec(j)*RAD2DEG, "sphtriangle" );
        }
    }

    return interpolated_grid_data;
}

double Isgemgeoid::mean_value_row(size_t row_number)
{
    if ( row_number >= 0 && row_number < this->geoid_model.n_rows ) {
        return arma::as_scalar( arma::mean( this->geoid_model.row( row_number )) );
    } else {
        string error_msg = "Isgemgeoid::mean_value_row() row_number is out of the bounds." ;
        throw logic_error( string(error_msg) );
    }
}

double Isgemgeoid::mean_value_row(size_t row_number) const
{
    if ( row_number >= 0 && row_number < this->geoid_model.n_rows ) {
        return arma::as_scalar( arma::mean( this->geoid_model.row( row_number )) );
    } else {
        string error_msg = "Isgemgeoid::mean_value_row() row_number is out of the bounds." ;
        throw logic_error( string(error_msg) );
    }
}

double Isgemgeoid::handle_no_data(arma::mat A) {
  double res = 0.;
  int numel = 0;

  arma::mat::iterator a = A.begin();
  arma::mat::iterator b = A.end();

  for (arma::mat::iterator i = a; i != b; ++i) {
    if (*i == nodata) {
      continue;
    } else {
      res += *i;
      numel++;
    }
  }
  return (res / ( static_cast<double>(numel)) );
}

unsigned int Isgemgeoid::conv2index(double xval)
{
    if ( xval >= 0.0 ) {
        return static_cast<unsigned int>( round(xval) );
    } else {
        string error_msg = "Input number is negative! Can not obtain index as unsinged int.";
        throw logic_error( string( error_msg ));
    }
}

void Isgemgeoid::swap_minmax( double &min, double &max) {
    double temp_val =  min;

    if ( min > max ) {
        min = max;
        max = temp_val;
    }

}

bool Isgemgeoid::g2qg_separation( const char* protocol,
                                  ofstream f_out,
                                  arma::mat dg_pbo,
                                  arma::mat delta,
                                  arma::mat v_gt,
                                  arma::mat v_pt,
                                  arma::mat dg_bo) {
    f_out << protocol << endl;
    f_out << dg_pbo << endl;
    f_out << delta << endl;
    f_out << v_gt << endl;
    f_out << v_pt << endl;
    f_out << dg_bo << endl;

    return false;
}

Isgemgeoid Isgemgeoid::compute_slope(geo_f::ellipsoid<double> ell)
{
    Isgemgeoid slope;

    // Copying the header info
    slope.ncols = this->ncols;
    slope.nrows = this->nrows;
    slope.units = "unitless";
    slope.lat_max = this->lat_max;
    slope.lat_min = this->lat_min;
    slope.lon_max = this->lon_max;
    slope.lon_min = this->lon_min;

    slope.delta_lat = this->delta_lat;         ///< Distance between the two parallels in the model.
    slope.delta_lon = this->delta_lon;         ///< Distance between the two meridians in the
    slope.nodata    = this->nodata;

    slope.model_name = this->model_name; ///< Name of the model
    slope.model_type = "Slope_from_" + this->model_type;
    slope.ref_ell = this->ref_ell;       ///< Reference ellipsoid.
    slope.isg_format = this->isg_format; ///< Version of the isgem format.

    slope.geoid_model = arma::zeros<arma::mat>(slope.nrows ,slope.ncols);


    double dz_dx , dz_dy, dx_cell_size, dy_cell_size ; // dx = db, dz = dl;
    for ( unsigned n = 0; n < this->nrows; n++ ) {
        double b = this->lat_min + static_cast<double>(n)*(this->delta_lat);  // current geodetic latitude

        double n_rad = ell.get_n_radius(b);
        double m_rad = ell.get_m_radius(b);

        dx_cell_size = m_rad * ( this->delta_lat * DEG2RAD );
        dy_cell_size = n_rad * ( this->delta_lon * DEG2RAD ) * cos( b*DEG2RAD );
        for (unsigned m = 0; m < this->ncols; m++) {
            arma::mat grid33; // grid around the point, except the boundary the size is 3x3 cells

            //////// =================== CORNERS ==========================================================
            if ( n==0 && m==0 ) { // upper left corner
                 grid33 = this->geoid_model( arma::span( 0,1 ) , arma::span( 0,1 ));

                 cout << grid33 << endl;

                 dz_dx = arma::as_scalar( (2.*grid33(0,0) + grid33(0,1))
                                         -(2.*grid33(1,0) + grid33(1,1))) / (6.*dx_cell_size);

                 dz_dy = arma::as_scalar( (2.*grid33(0,0) + grid33(1,0))
                                         -(2.*grid33(0,1) + grid33(1,1))) / (6.*dy_cell_size);
            } else if ( n==0 && m==this->ncols-1 ) { // upper right corner
                grid33 = this->geoid_model( arma::span( 0,1 ) , arma::span(  this->ncols-2,this->ncols-1 ));

                dz_dx = arma::as_scalar( (grid33(0,0) + 2.*grid33(0,1))
                                        -(grid33(1,0) + 2.*grid33(1,1))) / (6.*dx_cell_size);

                dz_dy = arma::as_scalar( (grid33(0,0) + 2.*grid33(1,0))
                                        -(grid33(0,1) + 2.*grid33(1,1))) / (6.*dy_cell_size);

            } else if ( n==this->nrows-1 && m==0  ) { // lower left corner
                grid33 = this->geoid_model( arma::span( this->nrows-2,this->nrows-1 ), arma::span( 0,1 ));


                dz_dx = arma::as_scalar( (2.*grid33(0,0) + grid33(0,1))
                                        -(2.*grid33(1,0) + grid33(1,1))) / (6.*dx_cell_size);

                dz_dy = arma::as_scalar( (grid33(0,0) + 2.*grid33(1,0))
                                        -(grid33(0,1) + 2.*grid33(1,1))) / (6.*dy_cell_size);
            } else if ( n==this->nrows-1 && m==this->ncols-1 ) { // lower right corner
                grid33 = this->geoid_model( arma::span( this->nrows-2,this->nrows-1 ), arma::span(this->ncols-2,this->ncols-1 ));

                dz_dx = arma::as_scalar( (grid33(0,0) + 2.*grid33(0,1))
                                        -(grid33(1,0) + 2.*grid33(1,1))) / (6.*dx_cell_size);

                dz_dy = arma::as_scalar( (grid33(0,0) + 2.*grid33(0,1))
                                        -(grid33(0,1) + 2.*grid33(1,1))) / (6.*dy_cell_size);
            //////// =================== BOUNDARIES========================================================
            } else if ( n == 0 && m > 0 && m < this->ncols-1 ) { // upper boundary
                grid33 = this->geoid_model( arma::span( 0,1 ) , arma::span( m-1,m+1 ));

                dz_dx = arma::as_scalar( (grid33(0,0)+2.*grid33(0,1) + grid33(0,2))
                                       - (grid33(1,0)+2.*grid33(1,1) + grid33(1,2)) ) / ( 8.*dx_cell_size );

                dz_dy = arma::as_scalar( (2.*grid33(0,0)+grid33(1,0) )
                                       - (2.*grid33(0,2)+grid33(1,2) ) ) / ( 6.*dy_cell_size );

            } else if ( m == 0 && n > 0 && n < this->nrows-1 ) { // left boundary
                grid33 = this->geoid_model( arma::span(n-1,n+1) , arma::span(0,1) ) ;

                dz_dx = arma::as_scalar(  (2.*grid33(0,0)+grid33(0,1) )
                                         -(2.*grid33(2,0)+grid33(2,1) )) / ( 6.*dx_cell_size );

                dz_dy = arma::as_scalar( (grid33(0,0)+2.*grid33(1,0) + grid33(2,0))
                                       - (grid33(0,1)+2.*grid33(1,1) + grid33(2,1)) ) / ( 8.*dy_cell_size );

            } else if ( m == this->ncols -1 && n > 0 && n < this->nrows-1 ) { // right boundary
                grid33 = this->geoid_model( arma::span(n-1,n+1) , arma::span(this->ncols-2,this->ncols-1) ) ;

                dz_dx = arma::as_scalar( -1.*(2.*grid33(0,0)+grid33(0,1) )
                                            +(2.*grid33(2,0)+grid33(2,1) )) / ( 6.*dx_cell_size );

                dz_dy = arma::as_scalar( -1.*(grid33(0,0)+2.*grid33(1,0) + grid33(2,0))
                                            +(grid33(0,1)+2.*grid33(1,1) + grid33(2,1)) ) / ( 8.*dy_cell_size );

            } else if ( m > 0 && m < this->ncols -1 && n == this->nrows-1 ) { // lower voundary
                grid33 = this->geoid_model( arma::span( this->nrows-2,this->nrows-1 ) , arma::span( m-1,m+1 ));

                dz_dx = arma::as_scalar( -1.*(grid33(0,0)+2.*grid33(0,1) + grid33(0,2))
                                            +(grid33(1,0)+2.*grid33(1,1) + grid33(1,2)) ) / ( 8.*dx_cell_size );

                dz_dy = arma::as_scalar( -1.*( grid33(0,0)+2.*grid33(1,0) )
                                            +( grid33(0,2)+2.*grid33(1,2) ) ) / ( 6.*dy_cell_size );
            //////// ==================== INSIDE ==============================================================
            } else {
                grid33 = this->geoid_model( arma::span( n-1,n+1 ) , arma::span( m-1,m+1 ));

                dz_dx = arma::as_scalar( (grid33(0,0)+2.*grid33(0,1) + grid33(0,2))
                                       - (grid33(2,0)+2.*grid33(2,1) + grid33(2,2)) ) / ( 8.*dx_cell_size );

                dz_dy = arma::as_scalar( (grid33(0,0)+2.*grid33(1,0) + grid33(2,0))
                                       - (grid33(0,2)+2.*grid33(1,2) + grid33(2,2)) ) / ( 8.*dy_cell_size );
            }
            slope.geoid_model(n,m) = sqrt( dz_dx*dz_dx + dz_dy*dz_dy );
        }
    }

    return  slope;
}


Isgemgeoid Isgemgeoid::gdist_molodenskyG1( const double& sphrad ,
                                           const double& intradius ,
                                           const bool& check_radius,
                                           const bool& extend_data,
                                           const Isgemgeoid& terrain ,
                                           geo_f::ellipsoid<double>& ell,
                                           const vector<double> & phi_bound)
{
    cout << "Function \"Isgemgeoid::molodensky_G1term()\" :" << endl
         << "Gdist raster size   " << this->geoid_model.n_rows << " " << this->geoid_model.n_cols << endl
         << "Terrain raster size " << terrain.geoid_model.n_rows << " " << terrain.geoid_model.n_cols << endl;

    // Running integration
    double avrg_phi,avrg_nrad,avrg_mrad,delta_b,delta_l;
    long long unsigned dcol, drow;
    long long int dr, dc;

    if ( check_radius )
    { //# If checking the radius of the integration
        avrg_phi = .5 * (this->lat_min + this->lat_max);
        //double avrg_rad = ell.get_mean_radius( avrg_phi );
        avrg_nrad = ell.get_n_radius( avrg_phi );
        avrg_mrad = ell.get_n_radius( avrg_phi );

        delta_b = RAD2DEG * intradius / (avrg_mrad) ;                           // return in deg format
        delta_l = RAD2DEG * intradius / (avrg_nrad * cos(avrg_phi * DEG2RAD));  // return in deg format

        dcol = static_cast<long long unsigned>(round(delta_l / delta_lon));
        drow = static_cast<long long unsigned>(round(delta_b / delta_lat));

        dr = static_cast<long long int>(drow);
        dc = static_cast<long long int>(dcol);

        if ( dr > static_cast<long long int>( this->geoid_model.n_rows ) ) {
            dr = static_cast<long long int>( this->geoid_model.n_rows );
        }

        if ( dc > static_cast<long long int>( this->geoid_model.n_cols ) ) {
            dc = static_cast<long long int>( this->geoid_model.n_cols );
        }

    } else { //# No limits to the integration radius
        dr = static_cast<long long int>( this->geoid_model.n_rows );
        dc = static_cast<long long int>( this->geoid_model.n_cols );
    }

    arma::uword nc, half, ridx, lidx;
    nc = this->geoid_model.n_cols;

    if ( extend_data ) {
         cout << "Extending the gravity data from ("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")" << " to ";

        half = nc / 2 - 1;
        arma::mat leftmat = this->geoid_model.cols( 0 , half );
        arma::mat rightmat= this->geoid_model.cols( half+1 , nc -1 );

        ridx = rightmat.n_cols;
        lidx =  leftmat.n_cols;

        this->geoid_model = arma::join_rows( rightmat , this->geoid_model );
        this->geoid_model = arma::join_rows( this->geoid_model , leftmat );

        cout << "("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")."  << endl;
    } else {
        ridx = static_cast<arma::uword>( dc + 1 );
        lidx = static_cast<arma::uword>( dc + 1 );
    }

    arma::mat conv_1d = arma::zeros<arma::mat>( arma::size(  this->geoid_model ) );
    long long int conv_nrows = static_cast<long long int>(conv_1d.n_rows);
    // Calculate all grid or just a small portion of it
    long long int i_low = 0, i_up = conv_nrows;
    if ( phi_bound.empty() ) {
        i_low = 0;
        i_up = conv_nrows-1;
    } else {
        // phi_bound should have two values [phi_min, phi_max]
        i_low = static_cast<long long int>( floor( (phi_bound[0] - this->lat_min )/this->delta_lat)  );
        i_up = static_cast<long long int>( ceil(( phi_bound[1] - this->lat_min)/this->delta_lat));

        if ( i_low < 0 ) {
            i_low = 0;
        } else if ( i_low >= conv_nrows ) {
            i_low = conv_nrows;
        }

        if ( i_up < 0 ) {
            i_up = 0;
        } else if ( i_up >= conv_nrows ) {
            i_up = conv_nrows-1;
        }
    }

    // ----------------------------------------------------------------------------
    // Numerical integration based on convolution algorithm (faster FFT would help)
    // Loop calculations
    // ----------------------------------------------------------------------------
    Progressbar pc_bar( static_cast<int>(i_up - i_low + 1) );
    pc_bar.set_name( "Isgemgeoid:gravity_disturbance_g1_Integral()"  );
    //
    {
        for ( long long int i = i_low ; i <= i_up; i++ ) {
            #pragma omp critical
            { pc_bar.update(); }

            double b0 = this->lat_min + this->delta_lat * static_cast<double>(i);
            double avrg_nrad,avrg_mrad,delta_b,delta_l;
            long long int dr, dc;

            if ( check_radius  )
            { //# If checking the radius of the integration
                avrg_nrad = ell.get_n_radius( b0 );
                avrg_mrad = ell.get_n_radius( b0 );

                delta_b = RAD2DEG * intradius / (avrg_mrad) ;                     // return in deg format
                delta_l = RAD2DEG * intradius / (avrg_nrad * cos(b0 * DEG2RAD));  // return in deg format

                dc = static_cast<long long int>(round(delta_l / delta_lon));
                dr = static_cast<long long int>(round(delta_b / delta_lat));

                if ( dr > static_cast<long long int>( this->geoid_model.n_rows ) ) {
                    dr = static_cast<long long int>( this->geoid_model.n_rows );
                }

                if ( extend_data ) {
                    if ( dc > static_cast<long long int>( this->geoid_model.n_cols )/2 ) {
                        dc = static_cast<long long int>( this->geoid_model.n_cols )/2;
                    }
                } else {
                    if ( dc > static_cast<long long int>( this->geoid_model.n_cols ) ) {
                        dc = static_cast<long long int>( this->geoid_model.n_cols );
                    }
                }

            } else { //# No limits to the integration radius
                dr = static_cast<long long int>( this->geoid_model.n_rows );
                dc = static_cast<long long int>( this->geoid_model.n_cols );
            }

            // number rows l = low, u = upper , number cols r = right, l = left
            arma::uword nr_l , nr_u , nc_r , nc_l;
            // picking up the indexes for the sigma matrix ( matrix with the kernel values based on $\Delta \phi , \Delta \lambda$
            if ( !check_radius   ) { // integration throughout the whole grid
                // vertical direction
                nr_l = static_cast<arma::uword>(i);
                nr_u = static_cast<arma::uword>( conv_nrows -i );

                // horizontal direction
                if ( extend_data ) {
                    nc_r = ridx;
                    nc_l = lidx;
                } else {
                    nc_r = static_cast<arma::uword>( dc + 1 );
                    nc_l = nc_r;
                }
            } else { // Check the integration radius (it will speed up the computaiton)
                // vertical direction ( phi_i)
                if ( i < dr ) {
                    nr_l = static_cast<arma::uword>( i );
                    nr_u = static_cast<arma::uword>( dr + 1 );
                } else if ( i>= dr && i < conv_nrows - dr ) {
                    nr_l = static_cast<arma::uword>( dr + 1 );
                    nr_u = static_cast<arma::uword>( dr );
                } else /* ( i >= conv_nrows + dr && i < conv_nrows ) */ {
                    nr_l = ( dr + 1 <= i ) ?  static_cast<arma::uword>( dr + 1 ) : static_cast<arma::uword>(i) ;
                    nr_u = static_cast<arma::uword>( conv_nrows - i - 1 );
                }
                //horizontal direciton (lambda)
                nc_r = static_cast<arma::uword>( dc + 1 );
                nc_l = nc_r;
            }

            arma::mat sigma = geo_f::l03_mat  (  b0, 0.0,
                                                 this->delta_lat,
                                                 this->delta_lon ,
                                                 sphrad, intradius,
                                                 nr_l , //nr_l, //  const arma::uword &drow,
                                                 nr_u , //nr_u,
                                                 nc_r,//   const arma::uword &dc_l,
                                                 nc_l,//   const arma::uword &dc_r, );
                                                 check_radius,
                                                 true);

            long long int s = i ; // indexes for rows od SIGMA matrix
            long long int low_i = ( i - dr < 0                   ) ? 0          : i - dr;
            long long int up_i  = ( i + dr >= conv_nrows - 1 ) ? conv_nrows - 1 : i + dr; // indexes for rows of delta_g matrix

           // interresults are stored into vector "insert_line"
           arma::rowvec insert_line = arma::zeros<arma::rowvec>(  conv_1d.n_cols );
           if ( !check_radius  ) {
           #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
               {
               #pragma omp for reduction(+ :insert_line)
               //for ( long long int j = low_i; j <= up_i; j++ ) {
               for ( long long int j = 0; j < conv_nrows; j++) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi \mathrm{d} \Omega

                    arma::rowvec hl = arma::conv( terrain.geoid_model.row(j) %  this->geoid_model.row(j) , sigma.row(j) ,"same" );
                    arma::rowvec hr = terrain.geoid_model.row(j) % arma::conv( this->geoid_model.row(j) , sigma.row(j) ,"same") ;

                    insert_line += surface*(hl - hr);
                }
                #pragma omp barrier
                } //  #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
                conv_1d.row(i) = insert_line;
           } else {
            #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            {
                #pragma omp for reduction(+ :insert_line)
                for ( long long int j = low_i; j <= up_i; j++ ) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi d\Omega

                    arma::rowvec hl = arma::conv( terrain.geoid_model.row(j) % this->geoid_model.row(j) , sigma.row(low_s) ,"same" );
                    arma::rowvec hr = terrain.geoid_model.row(j) % arma::conv( this->geoid_model.row(j) , sigma.row(low_s) ,"same") ;

                    insert_line += surface*(hl - hr);
                }
                #pragma omp barrier
            } // #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            conv_1d.row(i) = insert_line;
           }
        }
    }

    cout << "\n";
    arma::mat tpot;
    if ( extend_data ) {
        tpot = conv_1d.cols(  ridx  , ridx + nc - 1  );
    } else {
        tpot = conv_1d;
    }

    if ( phi_bound.empty() ) {
        tpot = tpot.rows( i_low , i_up );
    }

    // Saving the result
    Isgemgeoid G1_model;
    G1_model.set_header( "Hotine_integral_for_Molodensky_solution" , "grid_data", "unknown",Isgemgeoid::ref_ell,"1.0",true);
    G1_model.geoid_model = tpot * sphrad * sphrad / (2. * M_PI);
    G1_model.delta_lon = Isgemgeoid::delta_lon;
    G1_model.delta_lat = Isgemgeoid::delta_lat;

    if ( phi_bound.empty() ) {
         G1_model.lat_min = this->lat_min + this->delta_lat * static_cast<double>(i_low);
         G1_model.lat_max = this->lat_min + this->delta_lat * static_cast<double>(i_up);
    } else {
        G1_model.lat_min = Isgemgeoid::lat_min;
        G1_model.lat_max = Isgemgeoid::lat_max;
    }
    G1_model.lon_min = Isgemgeoid::lon_min;
    G1_model.lon_max = Isgemgeoid::lon_max;
    G1_model.nodata = Isgemgeoid::nodata;
    G1_model.nrows = tpot.n_rows ;
    G1_model.ncols = tpot.n_cols ;
    G1_model.ref_ell = ell.name;
    G1_model.sphererad = sphrad;

    return G1_model;
}

Isgemgeoid Isgemgeoid::convert_T0_to_deltag_prime(const double &sphrad,
                                                  Isgemgeoid gdist)
{
    // this->geoid_model is tpot value
    Isgemgeoid gdist_prime;

    if ( this->geoid_model.n_rows == gdist.geoid_model.n_rows  &&
         this->geoid_model.n_cols == gdist.geoid_model.n_cols ) {
         bool header_OK = true;

         string warning = "";

         if ( abs( this->lat_min - gdist.lat_min  ) > __EPS__ ) {
             warning = warning + "Function Isgemgeoid::convert_T0_to_deltag_prime() inconsistent data layout for tpot and gdist raster ( lat_min ).\n";
             header_OK =  false;
         } else if (abs( this->lat_max - gdist.lat_max   ) > __EPS__ ) {
             warning = warning +  "Function Isgemgeoid::convert_T0_to_deltag_prime() inconsistent data layout for tpot and gdist raster ( lat_max ).\n";
             header_OK =  false;
         } else if  ( abs( this->lon_min - gdist.lon_min  ) > __EPS__ ) {
             warning = warning +  "Function Isgemgeoid::convert_T0_to_deltag_prime() inconsistent data layout for tpot and gdist raster ( lon_min ).\n";
             header_OK =  false;
         } else if  ( abs( this->lon_max - gdist.lon_max  ) > __EPS__ ) {
             warning = warning +  "Function Isgemgeoid::convert_T0_to_deltag_prime() inconsistent data layout for tpot and gdist raster ( lon_max ).\n";
             header_OK =  false;
         } else if  ( abs( this->delta_lat - gdist.delta_lat  ) > __EPS__ ) {
             warning = warning + "Function Isgemgeoid::convert_T0_to_deltag_prime() inconsistent data layout for tpot and gdist raster ( delta_lat ).\n";
             header_OK =  false;
         } else if  ( abs( this->delta_lon - gdist.delta_lon   ) > __EPS__ ) {
             warning = warning + "Function Isgemgeoid::convert_T0_to_deltag_prime() inconsistent data layout for tpot and gdist raster ( delta_lon ).\n";
             header_OK =  false;
         }

         if ( header_OK == false   ) {
             cout << warning << endl;
         }

         gdist_prime.geoid_model = gdist.geoid_model - this->geoid_model/ ( 2. * sphrad );
         gdist_prime.set_header( "Hotine_integral_for_Molodensky_solution" , "grid_data", "unknown",Isgemgeoid::ref_ell,"1.0",true);
         gdist_prime.delta_lon = this->delta_lon;
         gdist_prime.delta_lat = this->delta_lat;
         gdist_prime.lat_min = this->lat_min;
         gdist_prime.lat_max = this->lat_max;
         gdist_prime.lon_min = this->lon_min;
         gdist_prime.lon_max = this->lon_max;
         gdist_prime.nodata = this->nodata;
         gdist_prime.nrows = this->geoid_model.n_rows;
         gdist_prime.ncols = this->geoid_model.n_cols;
         gdist_prime.ref_ell = this->ref_ell;
         gdist_prime.sphererad = sphrad;

         return gdist_prime;
    } else {
        string err_msg = "Isgemgeoid::convert_T0_to_deltag_prime() can not do the math with the models";
        throw runtime_error( err_msg );
    }
}

Isgemgeoid Isgemgeoid::molodensky_G1term ( const double& sphrad ,
                                           const double& intradius ,
                                           const bool& check_radius,
                                           const bool& extend_data,
                                           const Isgemgeoid& terrain,
                                           const Isgemgeoid& tpot0 ,
                                           geo_f::ellipsoid<double>& ell,
                                           const std::vector<double>& phi_bound) { // change to const std::vector<double>&
    // intradius - integration radius
    // sphrad - radius of the refeerence sphere
    // returning new model?
    // \f$ G_1 = \frac{R^2}{2\pi} \iint_\sigma \frac{H - H_p}{l^3} \left( \Delta g + \frac{3 \gamma}{2R} \zeta_0 \right) \mathrm{d} \sigma \f$
    // \f$ H, H_P \f$ Molodensky normal heights
    // \f$ l \f$ Euclidian distance between the computation points
    // \$ \gamma \f$ normal gravity
    // \f \zeta_0 \f$ approximate height anomaly computed from T_0
    // removeFreeAir is removing the Bouguer Shell in spherical way \f$ 4 \pi G \rho h \f$

    cout << "Matrixes size := " << endl
         << "faye " << this->geoid_model.n_rows << " " << this->geoid_model.n_cols << endl
         << "terr " << tpot0.geoid_model.n_rows << " " << tpot0.geoid_model.n_cols << endl;


    //*=========================== Solution with convolution ==================================*//
    cout << "Function \"Isgemgeoid::faye_molodenskyG1()\" :" << endl
         << "Gdist raster size   " << this->geoid_model.n_rows << " " << this->geoid_model.n_cols << endl
         << "Terrain raster size " << tpot0.geoid_model.n_rows << " " << tpot0.geoid_model.n_cols << endl;

    if ( !this->geoid_model.is_empty() ) {
        this->geoid_model = this->geoid_model + (3./(2.*sphrad)) * tpot0.geoid_model;
    }

    // Running integration
    double avrg_phi,avrg_nrad,avrg_mrad,delta_b,delta_l;
    long long unsigned dcol, drow;
    long long int dr, dc;

    if ( check_radius )
    { //# If checking the radius of the integration
        avrg_phi = .5 * (this->lat_min + this->lat_max);
        //double avrg_rad = ell.get_mean_radius( avrg_phi );
        avrg_nrad = ell.get_n_radius( avrg_phi );
        avrg_mrad = ell.get_n_radius( avrg_phi );

        delta_b = RAD2DEG * intradius / (avrg_mrad) ;                           // return in deg format
        delta_l = RAD2DEG * intradius / (avrg_nrad * cos(avrg_phi * DEG2RAD));  // return in deg format

        dcol = static_cast<long long unsigned>(round(delta_l / delta_lon));
        drow = static_cast<long long unsigned>(round(delta_b / delta_lat));

        dr = static_cast<long long int>(drow);
        dc = static_cast<long long int>(dcol);

        if ( dr > static_cast<long long int>( this->geoid_model.n_rows ) ) {
            dr = static_cast<long long int>( this->geoid_model.n_rows );
        }

        if ( dc > static_cast<long long int>( this->geoid_model.n_cols ) ) {
            dc = static_cast<long long int>( this->geoid_model.n_cols );
        }

    } else { //# No limits to the integration radius
        dr = static_cast<long long int>( this->geoid_model.n_rows );
        dc = static_cast<long long int>( this->geoid_model.n_cols );
    }

    arma::uword nc, half, ridx, lidx;
    nc = this->geoid_model.n_cols;

    if ( extend_data ) {
         cout << "Extending the gravity data from ("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")" << " to ";

        half = nc / 2 - 1;
        arma::mat leftmat = this->geoid_model.cols( 0 , half );
        arma::mat rightmat= this->geoid_model.cols( half+1 , nc -1 );

        ridx = rightmat.n_cols;
        lidx =  leftmat.n_cols;

        this->geoid_model = arma::join_rows( rightmat , this->geoid_model );
        this->geoid_model = arma::join_rows( this->geoid_model , leftmat );

        cout << "("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")."  << endl;
    } else {
        ridx = static_cast<arma::uword>( dc + 1 );
        lidx = static_cast<arma::uword>( dc + 1 );
    }

    arma::mat conv_1d = arma::zeros<arma::mat>( arma::size(  this->geoid_model ) );
    long long int conv_nrows = static_cast<long long int>(conv_1d.n_rows);
    // Calculate all grid or just a small portion of it
    long long int i_low = 0, i_up = conv_nrows;
    if ( phi_bound.empty() ) {
        i_low = 0;
        i_up = conv_nrows-1;
    } else {
        // phi_bound should have two values [phi_min, phi_max]
        i_low = static_cast<long long int>( floor( (phi_bound[0] - this->lat_min )/this->delta_lat)  );
        i_up = static_cast<long long int>( ceil(( phi_bound[1] - this->lat_min)/this->delta_lat));

        if ( i_low < 0 ) {
            i_low = 0;
        } else if ( i_low >= conv_nrows ) {
            i_low = conv_nrows;
        }

        if ( i_up < 0 ) {
            i_up = 0;
        } else if ( i_up >= conv_nrows ) {
            i_up = conv_nrows-1;
        }
    }

    // ----------------------------------------------------------------------------
    // Numerical integration based on convolution algorithm (faster FFT would help)
    // Loop calculations
    // ----------------------------------------------------------------------------
    Progressbar pc_bar( static_cast<int>(i_up - i_low + 1) );
    pc_bar.set_name( "Isgemgeoid:gravity_disturbance_g1_Integral()"  );
    //
    {
        for ( long long int i = i_low ; i <= i_up; i++ ) {
            #pragma omp critical
            { pc_bar.update(); }

            double b0 = this->lat_min + this->delta_lat * static_cast<double>(i);
            double avrg_nrad,avrg_mrad,delta_b,delta_l;
            long long int dr, dc;

            if ( check_radius  )
            { //# If checking the radius of the integration
                avrg_nrad = ell.get_n_radius( b0 );
                avrg_mrad = ell.get_n_radius( b0 );

                delta_b = RAD2DEG * intradius / (avrg_mrad) ;                     // return in deg format
                delta_l = RAD2DEG * intradius / (avrg_nrad * cos(b0 * DEG2RAD));  // return in deg format

                dc = static_cast<long long int>(round(delta_l / delta_lon));
                dr = static_cast<long long int>(round(delta_b / delta_lat));

                if ( dr > static_cast<long long int>( this->geoid_model.n_rows ) ) {
                    dr = static_cast<long long int>( this->geoid_model.n_rows );
                }

                if ( extend_data ) {
                    if ( dc > static_cast<long long int>( this->geoid_model.n_cols )/2 ) {
                        dc = static_cast<long long int>( this->geoid_model.n_cols )/2;
                    }
                } else {
                    if ( dc > static_cast<long long int>( this->geoid_model.n_cols ) ) {
                        dc = static_cast<long long int>( this->geoid_model.n_cols );
                    }
                }

            } else { //# No limits to the integration radius
                dr = static_cast<long long int>( this->geoid_model.n_rows );
                dc = static_cast<long long int>( this->geoid_model.n_cols );
            }

            // number rows l = low, u = upper , number cols r = right, l = left
            arma::uword nr_l , nr_u , nc_r , nc_l;
            // picking up the indexes for the sigma matrix ( matrix with the kernel values based on $\Delta \phi , \Delta \lambda$
            if ( !check_radius   ) { // integration throughout the whole grid
                // vertical direction
                nr_l = static_cast<arma::uword>(i);
                nr_u = static_cast<arma::uword>( conv_nrows -i );

                // horizontal direction
                if ( extend_data ) {
                    nc_r = ridx;
                    nc_l = lidx;
                } else {
                    nc_r = static_cast<arma::uword>( dc + 1 );
                    nc_l = nc_r;
                }
            } else { // Check the integration radius (it will speed up the computaiton)
                // vertical direction ( phi_i)
                if ( i < dr ) {
                    nr_l = static_cast<arma::uword>( i );
                    nr_u = static_cast<arma::uword>( dr + 1 );
                } else if ( i>= dr && i < conv_nrows - dr ) {
                    nr_l = static_cast<arma::uword>( dr + 1 );
                    nr_u = static_cast<arma::uword>( dr );
                } else /* ( i >= conv_nrows + dr && i < conv_nrows ) */ {
                    nr_l = ( dr + 1 <= i ) ?  static_cast<arma::uword>( dr + 1 ) : static_cast<arma::uword>(i) ;
                    nr_u = static_cast<arma::uword>( conv_nrows - i - 1 );
                }
                //horizontal direciton (lambda)
                nc_r = static_cast<arma::uword>( dc + 1 );
                nc_l = nc_r;
            }

            arma::mat sigma = geo_f::l03_mat  (  b0, 0.0,
                                                 this->delta_lat,
                                                 this->delta_lon ,
                                                 sphrad, intradius,
                                                 nr_l , //nr_l, //  const arma::uword &drow,
                                                 nr_u , //nr_u,
                                                 nc_r,//   const arma::uword &dc_l,
                                                 nc_l,//   const arma::uword &dc_r, );
                                                 check_radius,
                                                 true);

            long long int s = i ; // indexes for rows od SIGMA matrix
            long long int low_i = ( i - dr < 0                   ) ? 0          : i - dr;
            long long int up_i  = ( i + dr >= conv_nrows - 1 ) ? conv_nrows - 1 : i + dr; // indexes for rows of delta_g matrix

           // interresults are stored into vector "insert_line"
           arma::rowvec insert_line = arma::zeros<arma::rowvec>(  conv_1d.n_cols );
           if ( !check_radius  ) {
           #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
               {
               #pragma omp for reduction(+ :insert_line)
               //for ( long long int j = low_i; j <= up_i; j++ ) {
               for ( long long int j = 0; j < conv_nrows; j++) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi \mathrm{d} \Omega

                    arma::rowvec hl = arma::conv( terrain.geoid_model.row(j) %  this->geoid_model.row(j) , sigma.row(j) ,"same" );
                    arma::rowvec hr = terrain.geoid_model.row(j) % arma::conv( this->geoid_model.row(j) , sigma.row(j) ,"same") ;

                    insert_line += surface*(hl - hr);
                }
                #pragma omp barrier
                } //  #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
                conv_1d.row(i) = insert_line;
           } else {
            #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            {
                #pragma omp for reduction(+ :insert_line)
                for ( long long int j = low_i; j <= up_i; j++ ) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi d\Omega

                    arma::rowvec hl = arma::conv( terrain.geoid_model.row(j) % this->geoid_model.row(j) , sigma.row(low_s) ,"same" );
                    arma::rowvec hr = terrain.geoid_model.row(j) % arma::conv( this->geoid_model.row(j) , sigma.row(low_s) ,"same") ;

                    insert_line += surface*(hl - hr);
                }
                #pragma omp barrier
            } // #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            conv_1d.row(i) = insert_line;
           }
        }
    }

    cout << "\n";
    arma::mat tpot;
    if ( extend_data ) {
        tpot = conv_1d.cols(  ridx  , ridx + nc - 1  );
    } else {
        tpot = conv_1d;
    }

    if ( phi_bound.empty() ) {
        tpot = tpot.rows( i_low , i_up );
    }

    // Saving the result
    Isgemgeoid G1_model;
    G1_model.set_header( "Hotine_integral_for_Molodensky_solution" , "grid_data", "unknown",Isgemgeoid::ref_ell,"1.0",true);
    G1_model.geoid_model = tpot * sphrad * sphrad / (2. * M_PI);
    G1_model.delta_lon = Isgemgeoid::delta_lon;
    G1_model.delta_lat = Isgemgeoid::delta_lat;

    if ( phi_bound.empty()) {
         G1_model.lat_min = this->lat_min + this->delta_lat * static_cast<double>(i_low);
         G1_model.lat_max = this->lat_min + this->delta_lat * static_cast<double>(i_up);
    } else {
        G1_model.lat_min = Isgemgeoid::lat_min;
        G1_model.lat_max = Isgemgeoid::lat_max;
    }
    G1_model.lon_min = Isgemgeoid::lon_min;
    G1_model.lon_max = Isgemgeoid::lon_max;
    G1_model.nodata = Isgemgeoid::nodata;
    G1_model.nrows = tpot.n_rows ;
    G1_model.ncols = tpot.n_cols ;
    G1_model.ref_ell = ell.name;
    G1_model.sphererad = sphrad;

    return G1_model;
}

void Isgemgeoid::stokes_integral(const double &sphrad,
                                 const double &intradius,
                                 arma::mat &coords,
                                 geo_f::ellipsoid<double> ell)
{
    cout << sphrad << " <<Stokes integral>> " << intradius << " .....endl" << endl;

    arma::colvec stokes_computed( coords.n_rows );

    for ( arma::uword i= 0; i < coords.n_rows ; i++ ) {
        double bpt = coords(i,0); // phi in deg
        double lpt = coords(i,1); // lam in deg

        // only points in range
        double m_rad = ell.get_m_radius( bpt ); // metre
        double n_rad = ell.get_n_radius( bpt ); // metre
        //double radius = sqrt(m_rad*n_rad);

        double phi_run = bpt * DEG2RAD; // phi_run in radias

        double delta_b = RAD2DEG * intradius / (m_rad) ;  // return in deg format
        double delta_l = RAD2DEG * intradius / (n_rad * cos(phi_run));  // return in deg format

        int dcol, drow = static_cast<int>(round(delta_b / delta_lat));
        if ( abs( abs(phi_run) - M_PI_2) <= .5*DEG2RAD ) {
          dcol = drow;
        } else {
          dcol = static_cast<int>(round(delta_l / delta_lon));
        }

        long int leftb, rightb, upb, lowb, coln, rown;
        rown = static_cast<long int>( round((bpt - this->lat_min) / this->delta_lat))+1;
        coln = static_cast<long int>( round((lpt - this->lon_min) / this->delta_lon))+1;

        leftb = ((coln - dcol) >= 0) ? (coln - dcol) : 0;
        leftb = (leftb < 0) ? 0 : leftb;
        rightb = ((coln + dcol) < static_cast<long int>(ncols) ) ? (coln + dcol) : static_cast<long int>(ncols - 1);

        lowb = ((rown - drow) >= 0) ? (rown - drow) : 0;
        lowb = (lowb < 0) ? 0 : lowb;
        upb = ((rown + drow) < static_cast<long int>(nrows) ) ? (rown + drow) : static_cast<long int>(nrows - 1);;

        double subres = 0.0;//subresults
        for ( long int n = lowb ; n <= upb; n++ ) {
            double phi_m = lat_min + static_cast<double>(n) * this->delta_lat; // deg
            for  ( long int m = leftb; m <= rightb; m++) {
                double lam_m = lon_min + static_cast<double>(m) * this->delta_lon; // deg
                double surface = 2. * cos(DEG2RAD * phi_m) * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;

                double psi = geo_f::length_sphere(bpt, lpt, phi_m, lam_m, 1.0);

                double l = 2.*sphrad*sin(psi/2.);

                if ( 1.05*l >= intradius ){
                    continue;
                }

                double addval =  this->geoid_model(n,m) * surface * geo_f::stokes_kernel<double>(psi);
                if ( isnan(addval) || isinf(addval) ) {

                    continue;
                }
                subres += addval;
            }
        }

        stokes_computed(i) = subres * sphrad / (4. * M_PI);
    }

    coords = arma::join_rows(coords ,  stokes_computed );
}



Isgemgeoid Isgemgeoid::stokes_integral(const double &sphrad, // [m]
                                       const double &intradius, // [m]
                                       const bool& check_radius,
                                       const bool& extend_data,
                                       const bool &remove_bouguer_shell,
                                       Isgemgeoid terrain,
                                       geo_f::ellipsoid<double> ell,
                                       double (&kernel_fun)(double),
                                       double (&kernel_fun_int)(double),
                                       const vector<double> &phi_bound)
{
    // intradius - integration radius
    // sphrad - radius of the refeerence sphere
    // returning new model?
    // \f$ G_1 = \frac{R^2}{2\pi} \iint_\sigma \frac{H - H_p}{l^3} \left( \Delta g + \frac{3 \gamma}{2R} \zeta_0 \right) \mathrm{d} \sigma \f$
    // \f$ H, H_P \f$ Molodensky normal heights
    // \f$ l \f$ Euclidian distance between the computation points
    // \$ \gamma \f$ normal gravity
    // \f \zeta_0 \f$ approximate height anomaly computed from T_0

    if ( remove_bouguer_shell ) { // spherical approximation is however -4\pi
        this->geoid_model /*faye*/ -= 2. * M_PI * 2670 * terrain.geoid_model * arma::datum::G; //#TODO!!#
    }

    arma::mat tpot = arma::zeros<arma::mat>( arma::size( this->geoid_model ));


    ///<<<--------------------------------------------------------------------------------------------->>>///
    /* Stokes integral solved by FFT. Basically Stokes integral is converted to convolution integral based
     * on fact, that integral kernel is \f$ S(\psi) \f$ is function  of a distance between the points.
     *
     * Let the height anomaly be give by eq:
     * \f$ \zeta(\Phi, \Lambda ) = \frac{R}{4 \pi \gamma} = \int \limits_{-\pi/2}^{\pi/2} \mathrm{d} \phi \int \limits_0^{2 \pi} S(\psi) ( \Delta g (\Phi, \Lambda )  + G_1 (\Phi, \Lambda )  ) \cos \phi \mathrm{d} \lambda \f$ ,
     * where \f$ \Delta g (\Phi, \Lambda )  \f$ is Free-air anomaly on the Earth surface.
     *
     * Important quantities :
     * \f$ \psi = \arccos [ \cos \Phi \cos \phi + \sin \Phi \sin \phi \cos ( \Lambda - \lambda) ] \f$
     * \f$ S(\psi) = S( \Psi , \phi, \Lambda - \lambda ) \f$ ,
     *
     * where \f$ P(\Phi, \Lambda) \f$ is running point of the integration and
     * \f$ Q(\phi, \lambda) \f$ is an arbitrary point of the grid.
     *
     * Then without the loss of the inner integral in \f$ \zeta \f$ function becomes the convolution integral. The integral solution
     * is given by the sums of all parallels \f$ \phi = \phi_1 , \phi_2, \ldots, \phi_n \f$
     *
     * \f$ \zeta_\Phi ( \Lambda ) = \frac{R \Delta \phi \Delta \lambda}{4 \pi \gamma} F^{-1} \left[ \sum \limits_{\phi = \phi_1}^\phi_n F(S_{\Phi \phi} (\Lambda - \lambda) F(\Delta g_\phi (\lambda) \cos \phi ) \right]
     *
     * Singularity in \f$ \psi = 0  \rightqarrow \zeta_{0,0} = \frac{R}{\gamma} \sqrt{ \frac{\Delta \phi \Delta \lambda \cos \phi}{\pi} } \Delta g_{0,0} \f$
     *
     *
     */

    // Running integration
    double avrg_phi,avrg_nrad,avrg_mrad,delta_b,delta_l;
    long long unsigned dcol, drow;
    long long int dr, dc;

    if ( check_radius )
    { //# If checking the radius of the integration
        avrg_phi = .5 * (this->lat_min + this->lat_max);
        //double avrg_rad = ell.get_mean_radius( avrg_phi );
        avrg_nrad = ell.get_n_radius( avrg_phi );
        avrg_mrad = ell.get_n_radius( avrg_phi );

        delta_b = RAD2DEG * intradius / (avrg_mrad) ;                           // return in deg format
        delta_l = RAD2DEG * intradius / (avrg_nrad * cos(avrg_phi * DEG2RAD));  // return in deg format

        dcol = static_cast<long long unsigned>(round(delta_l / delta_lon));
        drow = static_cast<long long unsigned>(round(delta_b / delta_lat));

        dr = static_cast<long long int>(drow);
        dc = static_cast<long long int>(dcol);

        if ( dr > static_cast<long long int>( this->geoid_model.n_rows ) ) {
            dr = static_cast<long long int>( this->geoid_model.n_rows );
        }

        if ( dc > static_cast<long long int>( this->geoid_model.n_cols ) ) {
            dc = static_cast<long long int>( this->geoid_model.n_cols );
        }

    } else { //# No limits to the integration radius
        dr = static_cast<long long int>( this->geoid_model.n_rows );
        dc = static_cast<long long int>( this->geoid_model.n_cols );
    }

    arma::uword nc, half, ridx, lidx;
    nc = this->geoid_model.n_cols;

    if ( extend_data ) {
         cout << "Extending the gravity data from ("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")" << " to ";

        half = nc / 2 - 1;
        arma::mat leftmat = this->geoid_model.cols( 0 , half );
        arma::mat rightmat= this->geoid_model.cols( half+1 , nc -1 );

        ridx = rightmat.n_cols;
        lidx =  leftmat.n_cols;

        this->geoid_model = arma::join_rows( rightmat , this->geoid_model );
        this->geoid_model = arma::join_rows( this->geoid_model , leftmat );

        cout << "("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")."  << endl;
    } else {
        ridx = static_cast<arma::uword>( dc + 1 );
        lidx = static_cast<arma::uword>( dc + 1 );
    }

    arma::mat conv_1d = arma::zeros<arma::mat>( arma::size(  this->geoid_model ) );
    long long int conv_nrows = static_cast<long long int>(conv_1d.n_rows);
    // Calculate all grid or just a small portion of it
    long long int i_low = 0, i_up = conv_nrows;
    if ( phi_bound.empty()) {
        i_low = 0;
        i_up = conv_nrows-1;
    } else {
        // phi_bound should have two values [phi_min, phi_max]
        i_low = static_cast<long long int>( floor( (phi_bound[0] - this->lat_min )/this->delta_lat)  );
        i_up = static_cast<long long int>( ceil(( phi_bound[1] - this->lat_min)/this->delta_lat));

        if ( i_low < 0 ) {
            i_low = 0;
        } else if ( i_low >= conv_nrows ) {
            i_low = conv_nrows;
        }

        if ( i_up < 0 ) {
            i_up = 0;
        } else if ( i_up >= conv_nrows ) {
            i_up = conv_nrows-1;
        }
    }


    // ----------------------------------------------------------------------------
    // Numerical integration based on convolution algorithm (faster FFT would help)
    // Loop calculations
    Progressbar pc_bar( static_cast<int>(i_up - i_low + 1) );
    pc_bar.set_name( "Isgemgeoid::stokes_Integral()"  );
    //
    {
        for ( long long int i = i_low ; i <= i_up; i++ ) {
            #pragma omp critical
            { pc_bar.update(); }

            double b0 = this->lat_min + this->delta_lat * static_cast<double>(i);
            double avrg_nrad,avrg_mrad,delta_b,delta_l;
            long long int dr, dc;

            if ( check_radius  )
            { //# If checking the radius of the integration
                avrg_nrad = ell.get_n_radius( b0 );
                avrg_mrad = ell.get_n_radius( b0 );

                delta_b = RAD2DEG * intradius / (avrg_mrad) ;                     // return in deg format
                delta_l = RAD2DEG * intradius / (avrg_nrad * cos(b0 * DEG2RAD));  // return in deg format

                dc = static_cast<long long int>(round(delta_l / delta_lon));
                dr = static_cast<long long int>(round(delta_b / delta_lat));

                if ( dr > static_cast<long long int>( this->geoid_model.n_rows ) ) {
                    dr = static_cast<long long int>( this->geoid_model.n_rows );
                }

                if ( extend_data ) {
                    if ( dc > static_cast<long long int>( this->geoid_model.n_cols )/2 ) {
                        dc = static_cast<long long int>( this->geoid_model.n_cols )/2;
                    }
                } else {
                    if ( dc > static_cast<long long int>( this->geoid_model.n_cols ) ) {
                        dc = static_cast<long long int>( this->geoid_model.n_cols );
                    }
                }

            } else { //# No limits to the integration radius
                dr = static_cast<long long int>( this->geoid_model.n_rows );
                dc = static_cast<long long int>( this->geoid_model.n_cols );
            }

            // number rows l = low, u = upper , number cols r = right, l = left
            arma::uword nr_l , nr_u , nc_r , nc_l;
            // picking up the indexes for the sigma matrix ( matrix with the kernel values based on $\Delta \phi , \Delta \lambda$
            if ( !check_radius   ) { // integration throughout the whole grid
                // vertical direction
                nr_l = static_cast<arma::uword>(i);
                nr_u = static_cast<arma::uword>( conv_nrows -i );

                // horizontal direction
                if ( extend_data ) {
                    nc_r = ridx;
                    nc_l = lidx;
                } else {
                    nc_r = static_cast<arma::uword>( dc + 1 );
                    nc_l = nc_r;
                }
            } else { // Check the integration radius (it will speed up the computaiton)
                // vertical direction ( phi_i)
                if ( i < dr ) {
                    nr_l = static_cast<arma::uword>( i );
                    nr_u = static_cast<arma::uword>( dr + 1 );
                } else if ( i>= dr && i < conv_nrows - dr ) {
                    nr_l = static_cast<arma::uword>( dr + 1 );
                    nr_u = static_cast<arma::uword>( dr );
                } else /* ( i >= conv_nrows + dr && i < conv_nrows ) */ {
                    nr_l = ( dr + 1 <= i ) ?  static_cast<arma::uword>( dr + 1 ) : static_cast<arma::uword>(i) ;
                    nr_u = static_cast<arma::uword>( conv_nrows - i - 1 );
                }
                //horizontal direciton (lambda)
                nc_r = static_cast<arma::uword>( dc + 1 );
                nc_l = nc_r;
            }

            arma::mat sigma = geo_f::kernelmat2( b0, 0.0,
                                                 this->delta_lat,
                                                 this->delta_lon ,
                                                 sphrad, intradius,
                                                 nr_l, //  const arma::uword &drow,
                                                 nr_u,
                                                 nc_r,//   const arma::uword &dc_l,
                                                 nc_l,//   const arma::uword &dc_r, );
                                                 check_radius,
                                                 kernel_fun);

            long long int s = i ; // indexes for rows od SIGMA matrix
            long long int low_i = ( i - dr < 0                   ) ? 0          : i - dr;
            long long int up_i  = ( i + dr >= conv_nrows - 1 ) ? conv_nrows - 1 : i + dr; // indexes for rows of delta_g matrix

           arma::rowvec insert_line = arma::zeros<arma::rowvec>(  conv_1d.n_cols );

           if ( !check_radius  ) {
           #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
               {
               #pragma omp for reduction(+ :insert_line)
               //for ( long long int j = low_i; j <= up_i; j++ ) {
               for ( long long int j = 0; j < conv_nrows; j++) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi d\Omega

                    insert_line += arma::conv( surface*this->geoid_model.row(j) , sigma.row(j) , "same" );
                }
                #pragma omp barrier
                } //  #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
                conv_1d.row(i) = insert_line;
           } else {
            #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            {
                #pragma omp for reduction(+ :insert_line)
                for ( long long int j = low_i; j <= up_i; j++ ) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi d\Omega

                    insert_line += arma::conv( surface*this->geoid_model.row(j) , sigma.row(low_s) , "same" );
                }
                #pragma omp barrier
            } // #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            conv_1d.row(i) = insert_line;
           }
        }

    }

//     Contribution of the P=Q point, e.g. the running point of the integration is the same as
//     as the point for which the quantity is calculated

    cout << "\n";
    pc_bar.reset();
    pc_bar.set_niter( static_cast<int>(i_up - i_low) );

    #pragma omp parallel shared ( conv_1d )
    {
        #pragma omp for
        for ( arma::uword i = static_cast<arma::uword>(i_low) ;
                          i <= static_cast<arma::uword>(i_up) ; i++ ) {
            double brun = this->lat_min + static_cast<double>( this->delta_lat );
            double zero_effect = 0.;

            if ( abs( brun - 90.0  ) < __EPS__ ) { // north and south pole
                zero_effect = 2. * M_PI * ( kernel_fun_int( DEG2RAD * this->delta_lat )
                                           -kernel_fun_int( 0.0 ));
            } else {
                zero_effect = geo_f::zero_area_effect( brun, 0.0 , this->delta_lat, this->delta_lon , kernel_fun_int);
            }

            conv_1d.row(i) = conv_1d.row(i) + zero_effect * this->geoid_model.row(i);

            #pragma omp critical
            { pc_bar.update(); }
        }
    }

    cout << "\n";
    if ( extend_data ) {
        tpot = conv_1d.cols(  ridx  , ridx + nc - 1  ) * sphrad / (4*M_PI) ;
    } else {
        tpot = conv_1d * sphrad / (4*M_PI) ;
    }

    if ( phi_bound.empty() ) {
        tpot = tpot.rows( i_low , i_up );
    }

    Isgemgeoid stokes_model;

    stokes_model.set_header( "Stokes_integral_for_molodensky" , "grid_data", "unknown",Isgemgeoid::ref_ell,"1.0",true);
    stokes_model.geoid_model = tpot;// * sphrad / (4*M_PI) ;
    stokes_model.delta_lon = Isgemgeoid::delta_lon;
    stokes_model.delta_lat = Isgemgeoid::delta_lat;
    if ( phi_bound.empty()) {
         stokes_model.lat_min = this->lat_min + this->delta_lat * static_cast<double>(i_low);
         stokes_model.lat_max = this->lat_min + this->delta_lat * static_cast<double>(i_up);
    } else {
        stokes_model.lat_min = Isgemgeoid::lat_min;
        stokes_model.lat_max = Isgemgeoid::lat_max;
    }
    stokes_model.lon_min = Isgemgeoid::lon_min;
    stokes_model.lon_max = Isgemgeoid::lon_max;
    stokes_model.nodata = Isgemgeoid::nodata;
    stokes_model.nrows = Isgemgeoid::nrows;
    stokes_model.ncols = Isgemgeoid::ncols;

    return stokes_model;
}


Isgemgeoid Isgemgeoid::kernel_spectral_form( const double& sphrad,
                                             const double& intradius,
                                             const bool& check_radius,
                                             const bool& extend_data,
                                             geo_f::ellipsoid<double> ell,
                                             const unsigned& nmin,
                                             const unsigned& nmax,
                                             const string& kernel_type,
                                             const vector<double> &phi_bound)
{
    arma::mat tpot = arma::zeros<arma::mat>( arma::size( this->geoid_model ));

   //<<<---------------------------------------------------------------------------------------------
    /* Hotine integral solved by FFT. Basically Hotine integral is converted to convolution integral based
     * on fact, that integral kernel is \f$ H(\psi) \f$ is function  of a distance between the points.
     */

    // Running integration
    double avrg_phi,avrg_nrad,avrg_mrad,delta_b,delta_l;
    long long unsigned dcol, drow;
    long long int dr, dc;

    if ( check_radius )
    { //# If checking the radius of the integration
        avrg_phi = .5 * (this->lat_min + this->lat_max);
        avrg_nrad = ell.get_n_radius( avrg_phi );
        avrg_mrad = ell.get_n_radius( avrg_phi );

        delta_b = RAD2DEG * intradius / (avrg_mrad) ;                           // return in deg format
        delta_l = RAD2DEG * intradius / (avrg_nrad * cos(avrg_phi * DEG2RAD));  // return in deg format

        dcol = static_cast<long long unsigned>(round(delta_l / delta_lon));
        drow = static_cast<long long unsigned>(round(delta_b / delta_lat));

        dr = static_cast<long long int>(drow);
        dc = static_cast<long long int>(dcol);

        if ( dr > static_cast<long long int>( this->geoid_model.n_rows ) ) {
            dr = static_cast<long long int>( this->geoid_model.n_rows );
        }

        if ( dc > static_cast<long long int>( this->geoid_model.n_cols ) ) {
            dc = static_cast<long long int>( this->geoid_model.n_cols );
        }

    } else { //# No limits to the integration radius
        dr = static_cast<long long int>( this->geoid_model.n_rows );
        dc = static_cast<long long int>( this->geoid_model.n_cols );
    }

    arma::uword nc, half, ridx, lidx;
    nc = this->geoid_model.n_cols;

    if ( extend_data ) {
         cout << "Extending the gravity data from ("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")" << " to ";

        half = nc / 2 - 1;
        arma::mat leftmat = this->geoid_model.cols( 0 , half );
        arma::mat rightmat= this->geoid_model.cols( half+1 , nc -1 );

        ridx = rightmat.n_cols;
        lidx =  leftmat.n_cols;

        this->geoid_model = arma::join_rows( rightmat , this->geoid_model );
        this->geoid_model = arma::join_rows( this->geoid_model , leftmat );

        cout << "("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")."  << endl;
    } else {
        ridx = static_cast<arma::uword>( dc + 1 );
        lidx = static_cast<arma::uword>( dc + 1 );
    }

    arma::mat conv_1d = arma::zeros<arma::mat>( arma::size(  this->geoid_model ) );
    long long int conv_nrows = static_cast<long long int>(conv_1d.n_rows);
    // Calculate all grid or just a small portion of it
    long long int i_low = 0, i_up = conv_nrows;
    if ( phi_bound.empty() ) {
        i_low = 0;
        i_up = conv_nrows-1;
    } else {
        // phi_bound should have two values [phi_min, phi_max]
        i_low = static_cast<long long int>( floor( (phi_bound[0] - this->lat_min )/this->delta_lat)  );
        i_up = static_cast<long long int>( ceil(( phi_bound[1] - this->lat_min)/this->delta_lat));

        if ( i_low < 0 ) {
            i_low = 0;
        } else if ( i_low >= conv_nrows ) {
            i_low = conv_nrows;
        }

        if ( i_up < 0 ) {
            i_up = 0;
        } else if ( i_up >= conv_nrows ) {
            i_up = conv_nrows-1;
        }
    }

    // ----------------------------------------------------------------------------
    // Numerical integration based on convolution algorithm (faster FFT would help)
    // Loop calculations
    Progressbar pc_bar( static_cast<int>(i_up - i_low + 1) );
    pc_bar.set_name( "Isgemgeoid::kernel_spectral_form(), \"" + kernel_type + "\"."  );
    {
        for ( long long int i = i_low ; i <= i_up; i++ ) {
            #pragma omp critical
            { pc_bar.update(); }

            double b0 = this->lat_min + this->delta_lat * static_cast<double>(i);
            double avrg_nrad,avrg_mrad,delta_b,delta_l;
            long long int dr, dc;

            if ( check_radius  )
            { //# If checking the radius of the integration
                avrg_nrad = ell.get_n_radius( b0 );
                avrg_mrad = ell.get_n_radius( b0 );

                delta_b = RAD2DEG * intradius / (avrg_mrad) ;                     // return in deg format
                delta_l = RAD2DEG * intradius / (avrg_nrad * cos(b0 * DEG2RAD));  // return in deg format

                dc = static_cast<long long int>(round(delta_l / delta_lon));
                dr = static_cast<long long int>(round(delta_b / delta_lat));

                if ( dr > static_cast<long long int>( this->geoid_model.n_rows ) ) {
                    dr = static_cast<long long int>( this->geoid_model.n_rows );
                }

                if ( extend_data ) {
                    if ( dc > static_cast<long long int>( this->geoid_model.n_cols )/2 ) {
                        dc = static_cast<long long int>( this->geoid_model.n_cols )/2;
                    }
                } else {
                    if ( dc > static_cast<long long int>( this->geoid_model.n_cols ) ) {
                        dc = static_cast<long long int>( this->geoid_model.n_cols );
                    }
                }

            } else { //# No limits to the integration radius
                dr = static_cast<long long int>( this->geoid_model.n_rows );
                dc = static_cast<long long int>( this->geoid_model.n_cols );
            }

            // number rows l = low, u = upper , number cols r = right, l = left
            arma::uword nr_l , nr_u , nc_r , nc_l;
            // picking up the indexes for the sigma matrix ( matrix with the kernel values based on $\Delta \phi , \Delta \lambda$
            if ( !check_radius   ) { // integration throughout the whole grid
                // vertical direction
                nr_l = static_cast<arma::uword>(i);
                nr_u = static_cast<arma::uword>( conv_nrows -i );

                // horizontal direction
                if ( extend_data ) {
                    nc_r = ridx;
                    nc_l = lidx;
                } else {
                    nc_r = static_cast<arma::uword>( dc + 1 );
                    nc_l = nc_r;
                }
            } else { // Check the integration radius (it will speed up the computaiton)
                // vertical direction ( phi_i)
                if ( i < dr ) {
                    nr_l = static_cast<arma::uword>( i );
                    nr_u = static_cast<arma::uword>( dr + 1 );
                } else if ( i>= dr && i < conv_nrows - dr ) {
                    nr_l = static_cast<arma::uword>( dr + 1 );
                    nr_u = static_cast<arma::uword>( dr );
                } else /* ( i >= conv_nrows + dr && i < conv_nrows ) */ {
                    nr_l = ( dr + 1 <= i ) ?  static_cast<arma::uword>( dr + 1 ) : static_cast<arma::uword>(i) ;
                    nr_u = static_cast<arma::uword>( conv_nrows - i - 1 );
                }
                //horizontal direciton (lambda)
                nc_r = static_cast<arma::uword>( dc + 1 );
                nc_l = nc_r;
            }

            arma::mat sigma = geo_f::kernel_legen_sum(  b0, 0.0,
                                                     this->delta_lat,
                                                     this->delta_lon ,
                                                     sphrad, intradius,
                                                     nr_l, //  const arma::uword &drow,
                                                     nr_u,
                                                     nc_r,//   const arma::uword &dc_l,
                                                     nc_l,//   const arma::uword &dc_r, );
                                                     check_radius,
                                                     kernel_type,
                                                     nmin,
                                                     nmax,
                                                     true);

           long long int s = i ; // indexes for rows od SIGMA matrix
           long long int low_i = ( i - dr < 0                   ) ? 0          : i - dr;
           long long int up_i  = ( i + dr >= conv_nrows - 1 ) ? conv_nrows - 1 : i + dr; // indexes for rows of delta_g matrix

           arma::rowvec insert_line = arma::zeros<arma::rowvec>(  conv_1d.n_cols );


           if ( !check_radius  ) {
           #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
               {
               #pragma omp for reduction(+ :insert_line)
               //for ( long long int j = low_i; j <= up_i; j++ ) {
               for ( long long int j = 0; j < conv_nrows; j++) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi d\Omega

                    insert_line += arma::conv( surface*this->geoid_model.row(j) , sigma.row(j) , "same" );;
                }
                #pragma omp barrier
                } //  #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
                conv_1d.row(i) = insert_line;
           } else {
            #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            {
                #pragma omp for reduction(+ :insert_line)
                for ( long long int j = low_i; j <= up_i; j++ ) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi d\Omega

                    insert_line += arma::conv( surface*this->geoid_model.row(j) , sigma.row(low_s) , "same" );
                }
                #pragma omp barrier
            } // #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            conv_1d.row(i) = insert_line;
           }
        }

    }

    cout << "\n";

    if ( extend_data ) {
        tpot = conv_1d.cols(  ridx  , ridx + nc - 1  ) * sphrad / (4*M_PI) ;
    } else {
        tpot = conv_1d * sphrad / (4*M_PI) ;
    }

    if ( phi_bound.empty() ) {
        tpot = tpot.rows( i_low , i_up );
    }

    // Saving the result
    Isgemgeoid kernel_model;
    kernel_model.set_header( "kernel_spectral_form(), \"" + kernel_type + "\"" , "grid_data", "unknown",Isgemgeoid::ref_ell,"1.0",true);
    kernel_model.geoid_model = tpot;// * sphrad / (4*M_PI) ;
    kernel_model.delta_lon = Isgemgeoid::delta_lon;
    kernel_model.delta_lat = Isgemgeoid::delta_lat;

    if ( phi_bound.empty() ) {
         kernel_model.lat_min = this->lat_min + this->delta_lat * static_cast<double>(i_low);
         kernel_model.lat_max = this->lat_min + this->delta_lat * static_cast<double>(i_up);
    } else {
        kernel_model.lat_min = Isgemgeoid::lat_min;
        kernel_model.lat_max = Isgemgeoid::lat_max;
    }
    kernel_model.lon_min = Isgemgeoid::lon_min;
    kernel_model.lon_max = Isgemgeoid::lon_max;
    kernel_model.nodata = Isgemgeoid::nodata;
    kernel_model.nrows = tpot.n_rows ;
    kernel_model.ncols = tpot.n_cols ;
    kernel_model.ref_ell = ell.name;
    kernel_model.sphererad = sphrad;

    return kernel_model;
}

Isgemgeoid Isgemgeoid::hotine_integral(const double &sphrad, // [m]
                                       const double &intradius, // [m]
                                       const bool& check_radius,
                                       const bool& extend_data,
                                       geo_f::ellipsoid<double> ell,
                                       double (&kernel_fun)(double),
                                       double (&kernel_fun_int)(double),
                                       const vector<double> &phi_bound)
{

    arma::mat tpot = arma::zeros<arma::mat>( arma::size( this->geoid_model ));

   //<<<---------------------------------------------------------------------------------------------
    /* Hotine integral solved by FFT. Basically Hotine integral is converted to convolution integral based
     * on fact, that integral kernel is \f$ H(\psi) \f$ is function  of a distance between the points.
     */

    // Running integration
    double avrg_phi,avrg_nrad,avrg_mrad,delta_b,delta_l;
    long long unsigned dcol, drow;
    long long int dr, dc;

    if ( check_radius )
    { //# If checking the radius of the integration
        avrg_phi = .5 * (this->lat_min + this->lat_max);
        //double avrg_rad = ell.get_mean_radius( avrg_phi );
        avrg_nrad = ell.get_n_radius( avrg_phi );
        avrg_mrad = ell.get_n_radius( avrg_phi );

        delta_b = RAD2DEG * intradius / (avrg_mrad) ;                           // return in deg format
        delta_l = RAD2DEG * intradius / (avrg_nrad * cos(avrg_phi * DEG2RAD));  // return in deg format

        dcol = static_cast<long long unsigned>(round(delta_l / delta_lon));
        drow = static_cast<long long unsigned>(round(delta_b / delta_lat));

        dr = static_cast<long long int>(drow);
        dc = static_cast<long long int>(dcol);

        if ( dr > static_cast<long long int>( this->geoid_model.n_rows ) ) {
            dr = static_cast<long long int>( this->geoid_model.n_rows );
        }

        if ( dc > static_cast<long long int>( this->geoid_model.n_cols ) ) {
            dc = static_cast<long long int>( this->geoid_model.n_cols );
        }

    } else { //# No limits to the integration radius
        dr = static_cast<long long int>( this->geoid_model.n_rows );
        dc = static_cast<long long int>( this->geoid_model.n_cols );
    }

    arma::uword nc, half, ridx, lidx;
    nc = this->geoid_model.n_cols;

    if ( extend_data ) {
         cout << "Extending the gravity data from ("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")" << " to ";

        half = nc / 2 - 1;
        arma::mat leftmat = this->geoid_model.cols( 0 , half );
        arma::mat rightmat= this->geoid_model.cols( half+1 , nc -1 );

        ridx = rightmat.n_cols;
        lidx =  leftmat.n_cols;

        this->geoid_model = arma::join_rows( rightmat , this->geoid_model );
        this->geoid_model = arma::join_rows( this->geoid_model , leftmat );

        cout << "("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")."  << endl;
    } else {
        ridx = static_cast<arma::uword>( dc + 1 );
        lidx = static_cast<arma::uword>( dc + 1 );
    }

    arma::mat conv_1d = arma::zeros<arma::mat>( arma::size(  this->geoid_model ) );
    long long int conv_nrows = static_cast<long long int>(conv_1d.n_rows);
    // Calculate all grid or just a small portion of it
    long long int i_low = 0, i_up = conv_nrows;
    if ( phi_bound.empty() ) {
        i_low = 0;
        i_up = conv_nrows-1;
    } else {
        // phi_bound should have two values [phi_min, phi_max]
        i_low = static_cast<long long int>( floor( (phi_bound[0] - this->lat_min )/this->delta_lat)  );
        i_up = static_cast<long long int>( ceil(( phi_bound[1] - this->lat_min)/this->delta_lat));

        if ( i_low < 0 ) {
            i_low = 0;
        } else if ( i_low >= conv_nrows ) {
            i_low = conv_nrows;
        }

        if ( i_up < 0 ) {
            i_up = 0;
        } else if ( i_up >= conv_nrows ) {
            i_up = conv_nrows-1;
        }
    }

    // ----------------------------------------------------------------------------
    // Numerical integration based on convolution algorithm (faster FFT would help)
    // Loop calculations
    Progressbar pc_bar( static_cast<int>(i_up - i_low + 1) );
    pc_bar.set_name( "Isgemgeoid::Hotine_integral()"  );
    {
        for ( long long int i = i_low ; i <= i_up; i++ ) {
            #pragma omp critical
            { pc_bar.update(); }

            double b0 = this->lat_min + this->delta_lat * static_cast<double>(i);
            double avrg_nrad,avrg_mrad,delta_b,delta_l;
            long long int dr, dc;

            if ( check_radius  )
            { //# If checking the radius of the integration
                avrg_nrad = ell.get_n_radius( b0 );
                avrg_mrad = ell.get_n_radius( b0 );

                delta_b = RAD2DEG * intradius / (avrg_mrad) ;                     // return in deg format
                delta_l = RAD2DEG * intradius / (avrg_nrad * cos(b0 * DEG2RAD));  // return in deg format

                dc = static_cast<long long int>(round(delta_l / delta_lon));
                dr = static_cast<long long int>(round(delta_b / delta_lat));

                if ( dr > static_cast<long long int>( this->geoid_model.n_rows ) ) {
                    dr = static_cast<long long int>( this->geoid_model.n_rows );
                }

                if ( extend_data ) {
                    if ( dc > static_cast<long long int>( this->geoid_model.n_cols )/2 ) {
                        dc = static_cast<long long int>( this->geoid_model.n_cols )/2;
                    }
                } else {
                    if ( dc > static_cast<long long int>( this->geoid_model.n_cols ) ) {
                        dc = static_cast<long long int>( this->geoid_model.n_cols );
                    }
                }

            } else { //# No limits to the integration radius
                dr = static_cast<long long int>( this->geoid_model.n_rows );
                dc = static_cast<long long int>( this->geoid_model.n_cols );
            }

            // number rows l = low, u = upper , number cols r = right, l = left
            arma::uword nr_l , nr_u , nc_r , nc_l;
            // picking up the indexes for the sigma matrix ( matrix with the kernel values based on $\Delta \phi , \Delta \lambda$
            if ( !check_radius   ) { // integration throughout the whole grid
                // vertical direction
                nr_l = static_cast<arma::uword>(i);
                nr_u = static_cast<arma::uword>( conv_nrows -i );

                // horizontal direction
                if ( extend_data ) {
                    nc_r = ridx;
                    nc_l = lidx;
                } else {
                    nc_r = static_cast<arma::uword>( dc + 1 );
                    nc_l = nc_r;
                }
            } else { // Check the integration radius (it will speed up the computaiton)
                // vertical direction ( phi_i)
                if ( i < dr ) {
                    nr_l = static_cast<arma::uword>( i );
                    nr_u = static_cast<arma::uword>( dr + 1 );
                } else if ( i>= dr && i < conv_nrows - dr ) {
                    nr_l = static_cast<arma::uword>( dr + 1 );
                    nr_u = static_cast<arma::uword>( dr );
                } else /* ( i >= conv_nrows + dr && i < conv_nrows ) */ {
                    nr_l = ( dr + 1 <= i ) ?  static_cast<arma::uword>( dr + 1 ) : static_cast<arma::uword>(i) ;
                    nr_u = static_cast<arma::uword>( conv_nrows - i - 1 );
                }
                //horizontal direciton (lambda)
                nc_r = static_cast<arma::uword>( dc + 1 );
                nc_l = nc_r;
            }

            arma::mat sigma = geo_f::kernelmat2(  b0, 0.0,
                                                 this->delta_lat,
                                                 this->delta_lon ,
                                                 sphrad, intradius,
                                                 nr_l, //  const arma::uword &drow,
                                                 nr_u,
                                                 nc_r,//   const arma::uword &dc_l,
                                                 nc_l,//   const arma::uword &dc_r, );
                                                 check_radius,
                                                 kernel_fun);

           long long int s = i ; // indexes for rows od SIGMA matrix
           long long int low_i = ( i - dr < 0                   ) ? 0          : i - dr;
           long long int up_i  = ( i + dr >= conv_nrows - 1 ) ? conv_nrows - 1 : i + dr; // indexes for rows of delta_g matrix

           arma::rowvec insert_line = arma::zeros<arma::rowvec>(  conv_1d.n_cols );


           if ( !check_radius  ) {
           #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
               {
               #pragma omp for reduction(+ :insert_line)
               //for ( long long int j = low_i; j <= up_i; j++ ) {
               for ( long long int j = 0; j < conv_nrows; j++) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi d\Omega

                    insert_line += arma::conv( surface*this->geoid_model.row(j) , sigma.row(j) , "same" );
                    //insert_line += arma::conv( surface*this->geoid_model.row(j) , sigma.row(low_s) , "same" );
                }
                #pragma omp barrier
                } //  #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
                conv_1d.row(i) = insert_line;
           } else {
            #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            {
                #pragma omp for reduction(+ :insert_line)
                for ( long long int j = low_i; j <= up_i; j++ ) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi d\Omega

                    insert_line += arma::conv( surface*this->geoid_model.row(j) , sigma.row(low_s) , "same" );
                    //insert_line += arma::conv( surface*this->geoid_model.row(j) , sigma.row(low_s) , "same" );
                }
                #pragma omp barrier
            } // #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            conv_1d.row(i) = insert_line;
           }
        }

    }

//     Contribution of the P=Q point, e.g. the running point of the integration is the same as
//     as the point for which the quantity is calculated

    cout << "\n";
    pc_bar.reset();
    pc_bar.set_niter( static_cast<int>(i_up - i_low) );

    #pragma omp parallel shared ( conv_1d )
    {
        #pragma omp for
        for ( arma::uword i = static_cast<arma::uword>(i_low) ;
                          i <= static_cast<arma::uword>(i_up) ; i++ ) {
            double brun = this->lat_min + static_cast<double>( this->delta_lat );
            double zero_effect = 0.;

            if ( abs( brun - 90.0  ) < __EPS__ ) { // north and south pole
                zero_effect = 2. * M_PI * ( kernel_fun_int( DEG2RAD * this->delta_lat )
                                           -kernel_fun_int( 0.0 ));
            } else {
                zero_effect = geo_f::zero_area_effect( brun, 0.0 , this->delta_lat, this->delta_lon , kernel_fun_int);
            }

            conv_1d.row(i) = conv_1d.row(i) + zero_effect * this->geoid_model.row(i);

            #pragma omp critical
            { pc_bar.update(); }
        }
    }

    cout << "\n";
    if ( extend_data ) {
        tpot = conv_1d.cols(  ridx  , ridx + nc - 1  ) * sphrad / (4*M_PI) ;
    } else {
        tpot = conv_1d * sphrad / (4*M_PI) ;
    }

    if ( phi_bound.empty() ) {
        tpot = tpot.rows( i_low , i_up );
    }

    // Saving the result
    Isgemgeoid hotine_model;
    hotine_model.set_header( "Hotine_integral_for_Molodensky_solution" , "grid_data", "unknown",Isgemgeoid::ref_ell,"1.0",true);
    hotine_model.geoid_model = tpot;// * sphrad / (4*M_PI) ;
    hotine_model.delta_lon = Isgemgeoid::delta_lon;
    hotine_model.delta_lat = Isgemgeoid::delta_lat;

    if ( phi_bound.empty() ) {
         hotine_model.lat_min = this->lat_min + this->delta_lat * static_cast<double>(i_low);
         hotine_model.lat_max = this->lat_min + this->delta_lat * static_cast<double>(i_up);
    } else {
        hotine_model.lat_min = Isgemgeoid::lat_min;
        hotine_model.lat_max = Isgemgeoid::lat_max;
    }
    hotine_model.lon_min = Isgemgeoid::lon_min;
    hotine_model.lon_max = Isgemgeoid::lon_max;
    hotine_model.nodata = Isgemgeoid::nodata;
    hotine_model.nrows = tpot.n_rows ;
    hotine_model.ncols = tpot.n_cols ;
    hotine_model.ref_ell = ell.name;
    hotine_model.sphererad = sphrad;

    return hotine_model;
}

Isgemgeoid Isgemgeoid::hotine_kernel_integral(const double &dphi,
                                              const double &dlam,
                                              geo_f::ellipsoid<double> ell,
                                              double (&kernel_fun)(double),
                                              double (&kernel_fun_int)(double))
{
    bool extend_data = true;
    bool check_radius = false;

    unsigned long long numberrows = static_cast<unsigned long long>( round( 180./abs(dphi) )) +1;
    unsigned long long numbercols = static_cast<unsigned long long>( round((360. - dlam)/abs(dlam) )) +1;

    this->lat_min = -90.0; ///< Minimum value of geodetic latitude.
    this->lat_max = +90.0; ///< Maximum value of geodetic latitude.
    this->lon_min =  +0.0; ///< Minimum value of geodetic longitude.
    this->delta_lat = 180./static_cast<double>( numberrows ); ///< Distance between the two parallels in the model.
    this->delta_lon = 360./static_cast<double>( numbercols ); ///< Distance between the two meridians in the model.
    this->lon_max = 360. - this->delta_lon;                   ///< Maximum value of geodetic longitude.
    this->nrows = static_cast<unsigned int>( numberrows ); ///< Number of the rows
    this->ncols = static_cast<unsigned int>( numbercols ); ///< Number of the columns

    this->geoid_model.ones( numberrows, numbercols);

    arma::mat tpot = arma::zeros<arma::mat>( arma::size( this->geoid_model ));
    //---------------------------------------------------------------------------------------------
    /* Hotine integral solved by FFT. Basically Hotine integral is converted to convolution integral based
     * on fact, that integral kernel is \f$ H(\psi) \f$ is function  of a distance between the points.
     */
    arma::uword nc, half, ridx, lidx;
    nc = this->geoid_model.n_cols;
    if ( extend_data ) {
         cout << "Extending the gravity data from ("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")" << " to ";

        half = nc / 2 - 1;
        arma::mat leftmat = this->geoid_model.cols( 0 , half );
        arma::mat rightmat= this->geoid_model.cols( half+1 , nc -1 );

        ridx = rightmat.n_cols;
        lidx =  leftmat.n_cols;

        this->geoid_model = arma::join_rows( rightmat , this->geoid_model );
        this->geoid_model = arma::join_rows( this->geoid_model , leftmat );

        cout << "("<<geoid_model.n_rows<< "," << geoid_model.n_cols << ")."  << endl;
    }

    arma::mat conv_1d = arma::zeros<arma::mat>( arma::size(  this->geoid_model ) );

    long long int conv_nrows = static_cast<long long int>(conv_1d.n_rows);

    Progressbar pc_bar( static_cast<int>(conv_nrows) );
    pc_bar.set_name( "Isgemgeoid::Hotine_integral()"  );
    {
        for ( long long int i = 0 ; i < conv_nrows; i++ ) {
            #pragma omp critical
            { pc_bar.update(); }

            double b0 = this->lat_min + this->delta_lat * static_cast<double>(i);
            long long int dr, dc;

            dr = static_cast<long long int>( this->geoid_model.n_rows );
            dc = static_cast<long long int>( this->geoid_model.n_cols );


            arma::uword nr_l , nr_u , nc_r , nc_l; // number rows l = low, u = upper , number cols r = right, l = left
            // picking up the indexes for the sigma matrix ( matrix with the kernel values based on $\Delta \phi , \Delta \lambda$
            if ( !check_radius ) { // integration throughout the whole grid
                // vertical direction
                nr_l = static_cast<arma::uword>(i);
                nr_u = static_cast<arma::uword>( conv_nrows -i );

                // horizontal direction
                if ( extend_data ) {
                    nc_r = ridx;
                    nc_l = lidx;
                } else {
                    nc_r = static_cast<arma::uword>( dc + 1 );
                    nc_l = nc_r;
                }
            } else { // Check the integration radius (it will speed up the computaiton)
                // vertical direction ( phi_i)
                if ( i < dr ) {
                    nr_l = static_cast<arma::uword>( i );
                    nr_u = static_cast<arma::uword>( dr + 1 );
                } else if ( i>= dr && i < conv_nrows - dr ) {
                    nr_l = static_cast<arma::uword>( dr + 1 );
                    nr_u = static_cast<arma::uword>( dr );
                } else /* ( i >= conv_nrows + dr && i < conv_nrows ) */ {
                    nr_l = ( dr + 1 <= i ) ?  static_cast<arma::uword>( dr + 1 ) : static_cast<arma::uword>(i) ;
                    nr_u = static_cast<arma::uword>( conv_nrows - i - 1 );
                }
                //horizontal direciton (lambda)
                nc_r = static_cast<arma::uword>( dc + 1 );
                nc_l = nc_r;
            }

            arma::mat sigma = geo_f::kernelmat2(  b0, 0.0,
                                                 this->delta_lat,
                                                 this->delta_lon ,
                                                 6378136.3, 2.*M_PI* 6378136.3,
                                                 nr_l, //  const arma::uword &drow,
                                                 nr_u,
                                                 nc_r,//   const arma::uword &dc_l,
                                                 nc_l,//   const arma::uword &dc_r, );
                                                 false,
                                                 kernel_fun);

           long long int s = i ; // indexes for rows od SIGMA matrix
           long long int low_i = ( i - dr < 0                   ) ? 0          : i - dr;
           long long int up_i  = ( i + dr >= conv_nrows - 1 ) ? conv_nrows - 1 : i + dr; // indexes for rows of delta_g matrix

           arma::rowvec insert_line = arma::zeros<arma::rowvec>(  conv_1d.n_cols );

           if ( !check_radius ) {
           #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
               {
               #pragma omp for reduction(+ :insert_line)
               //for ( long long int j = low_i; j <= up_i; j++ ) {
               for ( long long int j = 0; j < conv_nrows; j++) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi d\Omega

                    insert_line += arma::conv( surface*this->geoid_model.row(j) , sigma.row(j) , "same" );
                }
                #pragma omp barrier
                } //  #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
                conv_1d.row(i) = insert_line;
           } else {
            #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            {
                #pragma omp for reduction(+ :insert_line)
                for ( long long int j = low_i; j <= up_i; j++ ) {
                    long long int low_s = ( s-i < 0  ) ? 0 : s-i;
                    low_s += ( j - low_i );

                    double cos_phi = cos ( DEG2RAD*(this->lat_min +  this->delta_lat * static_cast<double>(j)) );
                    double surface = 2. * cos_phi * sin( DEG2RAD * this->delta_lat / 2. ) * DEG2RAD * this->delta_lon;  // \iint_\omega \cos \phi d\Omega

                    insert_line += arma::conv( surface*this->geoid_model.row(j) , sigma.row(low_s) , "same" );
                }
                #pragma omp barrier
            } // #pragma omp parallel shared( sigma , geoid_model,i, conv_1d , low_i , up_i )
            conv_1d.row(i) = insert_line;
           }
        }

    }

//     Contribution of the P=Q point, e.g. the running point of the integration is the same as
//     as the point for which the quantity is calculated

    cout << "\n";
    pc_bar.reset();
    pc_bar.set_niter( static_cast<int>( this->geoid_model.n_rows ) );

#pragma omp parallel shared ( conv_1d )
{
    #pragma omp for
    for ( arma::uword i = 0; i < this->geoid_model.n_rows ; i++ ) {
        double brun = this->lat_min + static_cast<double>( this->delta_lat );
        double zero_effect = 0.;

        if ( abs( brun - 90.0  ) < __EPS__ ) { // north and south pole
            zero_effect = 2. * M_PI * ( kernel_fun_int( DEG2RAD * this->delta_lat )
                                       -kernel_fun_int( 0.0 ));
        } else {
            zero_effect = geo_f::zero_area_effect( brun, 0.0 , this->delta_lat, this->delta_lon , kernel_fun_int);
        }

        conv_1d.row(i) = conv_1d.row(i) + zero_effect * this->geoid_model.row(i);

        #pragma omp critical
        { pc_bar.update(); }
    }

}
    cout << "\n";

    tpot = conv_1d.cols(  ridx  , ridx + nc - 1  ) ;

    // Saving the result
    Isgemgeoid hotine_model;
    hotine_model.set_header( "Hotine_integral_for_Molodensky_solution" , "grid_data", "unknown",Isgemgeoid::ref_ell,"1.0",true);
    hotine_model.geoid_model = tpot;// * sphrad / (4*M_PI) ;
    hotine_model.delta_lon = Isgemgeoid::delta_lon;
    hotine_model.delta_lat = Isgemgeoid::delta_lat;
    hotine_model.lat_min = Isgemgeoid::lat_min;
    hotine_model.lat_max = Isgemgeoid::lat_max;
    hotine_model.lon_min = Isgemgeoid::lon_min;
    hotine_model.lon_max = Isgemgeoid::lon_max;
    hotine_model.nodata = Isgemgeoid::nodata;
    hotine_model.nrows = Isgemgeoid::nrows;
    hotine_model.ncols = Isgemgeoid::ncols;
    hotine_model.ref_ell = ell.name;
    hotine_model.sphererad = 6378136.3;

    return hotine_model;
}

bool Isgemgeoid::convert_tpot2qg(Isgemgeoid terrain,
                                 const string &tide_from,
                                 const string &tide_to,
                                 const double& k,
                                 const double& h,
                                 geo_f::ellipsoid<double> ell,
                                 double deltaT)
{
    unsigned int tpot_nc, tpot_nr;
    vector<double> tpot_header = this->get_header<double>( tpot_nr , tpot_nc );

    unsigned int terr_nc, terr_nr;
    vector<double> terr_header = terrain.get_header<double>( terr_nr , terr_nc );

    if (tpot_nc==terr_nc && tpot_nr==terr_nr   ) {
        bool header_OK = true;

        if ( abs(tpot_header[0] - terr_header[0] ) > __EPS__ ) {
            cerr << "Function Isgemgeoid::convert_tpot2qg() inconsistent data layout for tpot and terrain raster.\n";
            header_OK= false;
        } else if ( abs(tpot_header[1] - terr_header[1] ) > __EPS__ ) {
            cerr << "Function Isgemgeoid::convert_tpot2qg() inconsistent data layout for tpot and terrain raster.\n";
            header_OK= false;
        } else if  ( abs(tpot_header[2] - terr_header[2] ) > __EPS__ ) {
            cerr << "Function Isgemgeoid::convert_tpot2qg() inconsistent data layout for tpot and terrain raster.\n";
            header_OK= false;
        } else if  ( abs(tpot_header[3] - terr_header[3] ) > __EPS__ ) {
            cerr << "Function Isgemgeoid::convert_tpot2qg() inconsistent data layout for tpot and terrain raster.\n";
            header_OK= false;
        } else if  ( abs(tpot_header[4] - terr_header[4] ) > __EPS__ ) {
            cerr << "Function Isgemgeoid::convert_tpot2qg() inconsistent data layout for tpot and terrain raster.\n";
            header_OK= false;
        } else if  ( abs(tpot_header[5] - terr_header[5] ) > __EPS__ ) {
            cerr << "Function Isgemgeoid::convert_tpot2qg() inconsistent data layout for tpot and terrain raster.\n";
            header_OK= false;
        }

        Progressbar Pbar( static_cast<int>( this->geoid_model.n_rows ) );
        Pbar.set_name( "Isgemgeoid::convert_tpot2qg" );

        for (  arma::uword i = 0 ; i < this->geoid_model.n_rows; i++ ) {
            double bdeg = this->lat_min + static_cast<double>(i) * this->delta_lat;
            vector<double> ruv = geo_f::blh2ruv( bdeg , 0. , 0. , ell);

            #pragma omp critical
            {
                Pbar.update();
            }

            #pragma omp parallel shared (geoid_model, terrain, bdeg, ruv , i)
            {
                #pragma omp for
                for (  arma::uword j = 0 ; j < this->geoid_model.n_cols; j++ ){
                    double tpot = arma::as_scalar( this->geoid_model(i,j) ) + deltaT;
                    double hell = arma::as_scalar( terrain.geoid_model(i,j));

                    double zeta = geo_f::convert_tpot2qg<double>( bdeg , tpot, hell, ell   ); //geo_f::convert_tpot2qg( bdeg , tpot, hell, ell );
                    double tideZeta = geo_f::height_tide_transform( zeta, ruv[1], tide_from, tide_to, k ,h );

                    this->geoid_model(i,j) = tideZeta;
                }
            } // #pragma omp parallel shared ()
        }
        return  header_OK;
    } else {
        return false;
    }
}

Isgemgeoid Isgemgeoid::bouguer_shell( const string& geometry ,
                                      const string& limit,
                                      const string& quantity,
                                      const double& sphrad,
                                      const double& intradius)
{
    arma::mat free_air = arma::zeros<arma::mat>( arma::size( this->geoid_model) );

    bouguer::Bouguer Boug_anom( 2670.0 , NG_CONST );
    // pointers in spherical approximation
    double (bouguer::Bouguer::*sph_unlimited)( const double& ,
                                               const double& ,
                                               const double& );
    double (bouguer::Bouguer::*sph_limited)( const double& ,
                                             const double& ,
                                             const double& ,
                                             const double&);

    // plannar approximation
    double (bouguer::Bouguer::*plan_lim)( const double&,
                                          const double&,
                                          const double&,
                                          const int&);

    double (bouguer::Bouguer::*plan_unlim)( const double&,
                                            const double&,
                                            const int&);

    if ( limit == "limited" ) {
        if ( quantity == "gpot_v"  ) {
            sph_limited = &bouguer::Bouguer::gpot_v_limited_shell;
            plan_lim = &bouguer::Bouguer::gpot_v_limited_plate;
        } else if ( quantity == "gravity_r" ) {
            sph_limited = &bouguer::Bouguer::gravity_r_limited_shell;
            plan_lim = &bouguer::Bouguer::gravity_r_limited_plate;
        } else if ( quantity == "marussi_rr" ) {
            sph_limited = &bouguer::Bouguer::marussi_rr_limited_shell;
            plan_lim = &bouguer::Bouguer::marussi_rr_limited_plate;
        } else {
            string errmsg = "Isgemgeoid::bouguer_shell unknown gravity quantity required\n";
            throw runtime_error( errmsg );
        }
    } else {
        // unlimited geometry is default
        if ( quantity == "gpot_v"  ) {
            sph_unlimited = &bouguer::Bouguer::gpot_v_unlimited_shell;
            plan_unlim = &bouguer::Bouguer::gpot_v_unlimited_plate;
        } else if ( quantity == "gravity_r" ) {
            sph_unlimited = &bouguer::Bouguer::gravity_r_unlimited_shell;
            plan_unlim = &bouguer::Bouguer::gravity_r_unlimited_plate;
        } else if ( quantity == "marussi_rr" ) {
            sph_unlimited = &bouguer::Bouguer::marussi_rr_unlimited_shell;
            plan_unlim = &bouguer::Bouguer::marussi_rr_unlimited_plate;
        } else {
            string errmsg = "Isgemgeoid::bouguer_shell unknown gravity quantity required\n";
            throw runtime_error( errmsg );
        }
    }

    Progressbar p_bar( static_cast<int>( this->geoid_model.n_rows ) );
    p_bar.set_name( "Isgemgeoid::bouguer_shell()"  );

    for ( arma::uword i = 0 ;i < this->geoid_model.n_rows ; i++ ) {
        #pragma omp critical
        { p_bar.update(); }

        #pragma omp parallel for shared( geoid_model, free_air )
        for ( arma::uword j = 0; j < this->geoid_model.n_cols ; j++) {
            double result = 0.;
            double height = arma::as_scalar( this->geoid_model(i,j) );

            if ( limit == "limited" ) {
                if ( geometry == "sphere" ) {
                    result = (Boug_anom.*sph_limited)( sphrad,
                                                       sphrad + height,
                                                       sphrad + height+0.00001,
                                                       intradius);
                } else if ( geometry == "plane" ) {
                    result = (Boug_anom.*plan_lim)( intradius,
                                                    height,
                                                    height+0.00001,
                                                    2);
                } else {
                    string errmsg = "Isgemgeoid::bouguer_shell unknown geometry request.\n";
                    throw runtime_error( errmsg );
                }
            } else {
                if ( geometry == "sphere" ) {
                    result = (Boug_anom.*sph_unlimited)( sphrad,
                                                         sphrad + height,
                                                         sphrad + height+0.00001);
                } else if ( geometry == "plane" ) {
                    result =(Boug_anom.*plan_unlim)( height,
                                                     height+0.00001,
                                                     2);
                } else {
                    string errmsg = "Isgemgeoid::bouguer_shell unknown geometry request.\n";
                    throw runtime_error( errmsg );
                }
            }
            free_air(i,j) = result;
        } // for
    } // for

    Isgemgeoid bouguer_model;
    bouguer_model.set_header( "Bouguer_anomaly_" + limit + "_" + geometry ,
                              "grid_data",
                              quantity ,
                              Isgemgeoid::ref_ell,"1.0",true);
    bouguer_model.geoid_model = free_air;// * sphrad / (4*M_PI) ;
    bouguer_model.delta_lon = Isgemgeoid::delta_lon;
    bouguer_model.delta_lat = Isgemgeoid::delta_lat;
    bouguer_model.lat_min = Isgemgeoid::lat_min;
    bouguer_model.lat_max = Isgemgeoid::lat_max;
    bouguer_model.lon_min = Isgemgeoid::lon_min;
    bouguer_model.lon_max = Isgemgeoid::lon_max;
    bouguer_model.nodata = Isgemgeoid::nodata;
    bouguer_model.nrows = Isgemgeoid::nrows;
    bouguer_model.ncols = Isgemgeoid::ncols;
    bouguer_model.ref_ell = Isgemgeoid::ref_ell;
    bouguer_model.sphererad = sphrad;

    return  bouguer_model;
}

Isgemgeoid Isgemgeoid::cutoff_rows_columns(unsigned &nmin, unsigned &nmax, unsigned &mmin,  unsigned &mmax)
{
    if ( nmax >= this->geoid_model.n_rows ) {
        nmax = this->geoid_model.n_rows - 1;
    }

    if ( mmax >= this ->geoid_model.n_cols ) {
        mmax = this->geoid_model.n_cols - 1;
    }

    Isgemgeoid cutoff_model;
    cutoff_model.model_name = this->model_name; ///< Name of the model
    cutoff_model.model_type = this->model_type; ///< Type of the model. E.g. geoid/quasigeoid and the
                            ///< type of geoid such as gravimetric, hybrid, GNSS/leveling.
    cutoff_model.units=  this-> units;      ///< Units used by model (meetres, feets, ...)
    cutoff_model.ref_ell = this->ref_ell;    ///< Reference ellipsoid.
    cutoff_model.isg_format = this->isg_format; ///< Version of the isgem format.
    cutoff_model.geoid_model = this->geoid_model( arma::span( nmin , nmax ) , arma::span(mmin, mmax));
    cutoff_model.delta_lon = this->delta_lon;
    cutoff_model.delta_lat = this->delta_lat;
    cutoff_model.lat_min = this->lat_min + static_cast<double>(nmin) * this->delta_lat;
    cutoff_model.lat_max = this->lat_min + static_cast<double>(nmax) * this->delta_lat;
    cutoff_model.lon_min = this->lon_min + static_cast<double>(mmin) * this->delta_lon;
    cutoff_model.lon_max = this->lon_min + static_cast<double>(mmax) * this->delta_lon;
    cutoff_model.nodata = Isgemgeoid::nodata;
    cutoff_model.nrows = cutoff_model.geoid_model.n_rows;
    cutoff_model.ncols = cutoff_model.geoid_model.n_cols;
    cutoff_model.ref_ell = Isgemgeoid::ref_ell;
    cutoff_model.sphererad = this->sphererad;

    return cutoff_model;
}

void Isgemgeoid::set_header(const string &n_model_name,
                            const string &n_model_type,
                            const string &n_units,
                            const string &n_ref_ell,
                            const string &n_isg_format,
                            const bool &n_print_coeff)
{
     this->model_name = n_model_name;
     this->model_type = n_model_type;
     this->units      = n_units;
     this->ref_ell    = n_ref_ell;
     this->isg_format = n_isg_format;
     this->print_coeff= n_print_coeff;
}

Isgemgeoid Isgemgeoid::resample_model ( double& n_lat_min,
                                       double& n_lat_max,
                                       double& n_lon_min,
                                       double& n_lon_max,
                                       double& n_dlat,
                                       double& n_dlon,
                                       string algorithm ) {
    // boundary check, can not return model bigger than the original one
    swap_minmax( n_lat_min , n_lat_max );
    swap_minmax( n_lon_min , n_lon_max );

    if ( n_lat_min < lat_min ) {
        n_lat_min = lat_min;
    }

    if ( n_lat_max > lat_max ) {
        n_lat_max = lat_max;
    }

    if ( n_lon_min < lon_min ) {
        n_lon_min = lon_min;
    }

    if ( n_lat_max > lat_max ) {
        n_lat_max = lat_max;
    }

    Isgemgeoid Newmodel;
    unsigned int i = static_cast<unsigned>(round( ( n_lat_max - n_lat_min )/n_dlat ) ) + 1;
    unsigned int j = static_cast<unsigned>(round( ( n_lon_max - n_lon_min )/n_dlon ) ) + 1;
    // Copying the header information
    Newmodel.model_name = model_name;   // Name of the model
    Newmodel.model_type = model_type;   // Type of the model. E.g. geoid/quasigeoid and the
                                        // type of geoid such as gravimetric, hybrid, GNSS/leveling.
    Newmodel.units = units;             // Units used by model (meetres, feets, ...)
    Newmodel.ref_ell = ref_ell;         // Reference ellipsoid.
    Newmodel.isg_format = isg_format;   // Version of the isgem format.
    Newmodel.print_coeff = print_coeff; // Value that is used for function @see operator<<
    Newmodel.nodata = nodata;
    Newmodel.delta_lat = n_dlat;
    Newmodel.delta_lon = n_dlon;
    Newmodel.lat_max = static_cast<double>(i)*n_dlat + n_lat_min;
    Newmodel.lat_min = n_lat_min;
    Newmodel.lon_max = static_cast<double>(j)*n_dlon + n_lon_min;
    Newmodel.lon_min =  n_lon_min;
    Newmodel.nrows = i;
    Newmodel.ncols = j;
    Newmodel.geoid_model.resize(i,j);
    Newmodel.geoid_model.fill( nodata );

    // Actual resampling of the data
    Progressbar Rbar( static_cast<int>( i ) );
    Rbar.set_name("Isgemgeoid::resample()");
    //#pragma omp parallel
    {
        for ( unsigned int m = 0; m < i ; m++ ) {
            #pragma omp critical
            {  Rbar.update();  }
            double b_m = static_cast<double>(m)*Newmodel.delta_lat + Newmodel.lat_min;
            #pragma omp parallel for shared(i, b_m , Newmodel)
            for ( unsigned int n = 0; n < j ; n++ ) {
                double l_m = static_cast<double>(n) * Newmodel.delta_lon + Newmodel.lon_min;
                bool is_in_bound;

                double ival = this->interpolate_value(b_m, l_m, algorithm , &is_in_bound );// interpolated value

                if ( is_in_bound ) {
                    Newmodel.geoid_model(m,n) = ival ;
                } else {
                    continue;
                }
            }
        #pragma omp barrier
        }
    }

    return Newmodel;
}

