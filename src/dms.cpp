#include "../include/dms.h"

dms::dms(){
    // ctor
}

dms::dms(double s_deg, double s_min, double s_sec) {
  // ctor
  sign = (s_deg >= 0) ? 1 : -1;
  deg = abs(s_deg);
  minut = abs(s_min);
  sec = abs(s_sec);
}

dms::dms(double degformat) {
  // ctor
  sign = (degformat >= 0) ? 1 : -1;
  deg = floor(abs(degformat));
  minut = floor((abs(degformat) - deg) * 60);
  sec = (abs(degformat) - deg - minut / 60) * 3600;
}


double dms::dms2deg() {
  return ((double)sign) * (abs(deg) + minut / 60. + sec / 3600.);
}

double dms::dms2rad() {
  return ((double)sign) *
         ((abs(deg) + minut / 60. + sec / 3600.) * (M_PI / 180.));
}

double dms::dms2grad() {
  return ((double)sign) * (abs(deg) + minut / 60. + sec / 3600.) * (10. / 9.);
}

void dms::deg2dms(double degformat) {
  sign = (degformat >= 0) ? 1 : -1;
  deg = floor(abs(degformat));
  minut = floor((abs(degformat) - deg) * 60);
  sec = (abs(degformat) - deg - minut / 60) * 3600;
}

void dms::rad2dms(
    double
        rad) /*, int &s_sign, double &s_deg, double &s_min, double &s_sec);*/ {
  double degformat = rad * 180. / M_PI;

  sign = (degformat >= 0) ? 1 : -1;
  deg = floor(abs(degformat));
  minut = floor((abs(degformat) - deg) * 60);
  sec = (abs(degformat) - deg - minut / 60) * 3600;
}

///////////////////////////
// OVERLOADING OPERATORS //
///////////////////////////
dms dms::operator+(const dms& angle2) const {
    dms angle1;

    double deg1 = sign*( abs(deg) + minut/60. + sec/3600.0);
    double deg2 = angle2.sign*(abs(angle2.deg) + (angle2.minut) / 60. +
                                  (angle2.sec) / 3600.);

    angle1.deg2dms(deg1+deg2);
    return angle1;
}

dms dms::operator+=(const dms& angle2) const {
    dms angle1;

    double deg1 = sign*( abs(deg) + minut/60. + sec/3600.0);
    double deg2 = angle2.sign*(abs(angle2.deg) + (angle2.minut) / 60. +
                                  (angle2.sec) / 3600.);

    angle1.deg2dms(deg1+deg2);
    return angle1;
}

dms dms::operator-(const dms& angle2) const {
    dms angle1;

    double deg1 = sign*( abs(deg) + minut/60. + sec/3600.0);
    double deg2 = angle2.sign*(abs(angle2.deg) + (angle2.minut) / 60. +
                                  (angle2.sec) / 3600.);

    angle1.deg2dms(deg1-deg2);
    return angle1;
}

dms dms::operator*(const dms& angle2) const {
  dms angle1;

  double deg1 = sign*( abs(deg) + minut/60. + sec/3600.0);
  double deg2 = angle2.sign*(abs(angle2.deg) + (angle2.minut) / 60. +
                                (angle2.sec) / 3600.);

  angle1.deg2dms(deg1*deg2);
  return angle1;
}

dms dms::operator/(const dms& angle2) const {
    dms angle1;

    double deg1 = sign*( abs(deg) + minut/60. + sec/3600.0);
    double deg2 = angle2.sign*(abs(angle2.deg) + (angle2.minut) / 60. +
                                  (angle2.sec) / 3600.);

    angle1.deg2dms(deg1/deg2);
    return angle1;
}


