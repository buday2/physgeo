#include "../include/geodetic_functions.h"

double geo_f::len_on_sphere(double radius, double b1, double l1, double b2, double l2) {
    b1 *= DEG2RAD;
    b2 *= DEG2RAD;
    l1 *= DEG2RAD;
    l2 *= DEG2RAD;

    double psi = acos( sin(b1)*sin(b2) + cos(b1)*cos(b2)*cos(l2-l1) );

    if ( abs(b1-b2) <= __EPS__ && abs(l1-l2) <= __EPS__ ) {
        psi = 0.0;
    } else if (  sin(b1)*sin(b2) + cos(b1)*cos(b2)*cos(l2-l1) >= 1.00000000000000000000000000000000000 ) {
        psi = 0.0;
    }

    return radius*psi;
}


vector<double> geo_f::bl2jtsk( double b, double l ) {
    /* (phi,lambda) >> (U,V) >> (S,D) >> (\rho, \vaerpsilon) >> (Y,X)
    * (phi,lambda) - geodetic coordinates on Bessel's ellipsoid
    * (U,V)        - spherical coordinates on reference sphere
    * (S,D)        - cartographic coordinates on reference sphere with shifted pole
    * (\rho, \vaerpsilon) - polar coordinates on the cone
    * (Y,X)        - rectangular coordinates in the plane (JTSK coordinates)
    *
    * coordinates of the shifted pole  \phi_Q      = 48°15'
                                    \lambda_Q      = 42°30'

    * input : xn, yn, zn - rectangular coordinates on the Bessel ellipsoid
    * output: y,x        - rectangular coordinates in the plane (jtsk)
           h          - height
    */
    const double rho_0 = 1298039.004638987;
    const double s_0   = 78.50000*DEG2RAD ;
    const double n     = .9799247046208300;
    const double alpha = 1.000597498371542;
    const double k     = /*1.003419163966575;*/.996592486900000;
    const double e     = 0.81696831215303e-1;
    const double vk    = (42. + 31./60. + 31.41725/3600.)*DEG2RAD;
    const double uk    = (59. + 42./60. + 42.6968885/3600.)*DEG2RAD;
    //const double u0    = (49. + 27./60. + 32.84625/3600.)*DEG2RAD;

    // (phi,lambda) >> (U,V)
    double phi = b*DEG2RAD;
    double u = 0.2e1 * atan( pow(tan(phi/ 2. + M_PI_4 )
                                 * pow( (1. - e * sin(phi))
                                      / (1. + e * sin(phi)), e / 2.), alpha) / k)
               - M_PI_2;
    double v = alpha * DEG2RAD * (l + 17+36./60.+46.002/3600.); // metodika navrhuje 17°40'

    // (U,V) >> (S,D)
    double s = asin(sin(u) * sin(uk) + cos(u) * cos(uk) * cos(vk - v));
    double d = asin(cos(u) * sin(vk - v) / cos(s));

    //(S,D) >> (\rho, \vaerpsilon)
    double rho = rho_0 * pow(tan(s_0 /2. + M_PI_4) / tan(s / 2. + M_PI_4), n);
    double epsilon = n * d;

    // (\rho, \vaerpsilon) >> (Y,X)
    double x = rho * cos(epsilon);
    double y = rho * sin(epsilon);

    vector<double> yx(2);
    yx[0] = y;
    yx[1] = x;

    return yx;
}

vector<double> geo_f::jtsk2bl( double y, double x) {
    /* (phi,lambda) >> (U,V) >> (S,D) >> (\rho, \vaerpsilon) >> (Y,X)
    * (phi,lambda) - geodetic coordinates on Bessel's ellipsoid
    * (U,V)        - spherical coordinates on reference sphere
    * (S,D)        - cartographic coordinates on reference sphere with shifted pole
    * (\rho, \vaerpsilon) - polar coordinates on the cone
    * (Y,X)        - rectangular coordinates in the plane (JTSK coordinates)
    *
    * coordinates of the shifted pole  \phi_Q      = 48°15'
                                    \lambda_Q   = 42°30'

    * input : xn, yn, zn - rectangular coordinates on the Bessel ellipsoid
    * output: y,x        - rectangular coordinates in the plane (jtsk)
           h          - height
    */

//    const double n     = .979924704600000;
//    const double alpha = 1.00059749837200;
//    const double k     = .996592486900000;
//    const double e     = 0.81696831215303e-1;
//    const double vk    = (42. + 31./60. + 31.41725/3600.)*DEG2RAD;
//    const double uk    = (59. + 42./60. + 42.69690/3600.)*DEG2RAD;
    //const double u0    = (49. + 27./60. + 32.84625/3600.)*DEG2RAD; //    (49. + 27./60. + 35.8462/.3600)*DEG2RAD;
    const double rho_0 = 1298039.004638987;
    const double s_0   = 78.50000*DEG2RAD ;
    const double n     = .9799247046208300;
    const double alpha = 1.000597498371542;
    const double k     = /*1.003419163966575;*/.996592486900000;
    const double e     = 0.81696831215303e-1;
    const double vk    = (42. + 31./60. + 31.41725/3600.)*DEG2RAD;
    const double uk    = (59. + 42./60. + 42.6968885/3600.)*DEG2RAD;
    const double u0    = (49. + 27./60. + 32.84625/3600.)*DEG2RAD;
    // ((\rho, \vaerpsilon) << (Y,X)
    double rho = sqrt(x*x+y*y);
    double epsilon = atan(y/x);

    // (S,D) << (\rho, \vaerpsilon)
    double d = epsilon/n ;
    //double s = 2.*atan(0.17199720955619340213e8 * pow(rho, -0.2220270644e10 / 0.2175698055e10)) - M_PI_2;
    double s = 2.*atan( pow( rho_0/rho , 1./n  ) * tan( s_0/2. + M_PI_4  ) ) - M_PI_2;

    // (U,V) << (S,D)
    double u = asin( sin(uk)*sin(s) - cos(uk)*cos(s)*cos(d));
    double delta_v = asin( sin(d)*cos(s)/cos(u));

    // (phi,lambda) << (U,V)
    double lambda = (vk - delta_v)/alpha - .3074009723405860142863407 ;
    double phi_i = u, phi_ii = u, dphi=9999;
    int loop_cnt = 0;

    while ( abs( dphi ) >= .000001/3600*DEG2RAD ) {
        phi_ii = 2.* atan( pow( k * tan( u/2 + M_PI_4 ), 1./alpha )
                         * pow( (1.-e*sin(phi_i))/
                                (1.+e*sin(phi_i)), -e/2. )) - M_PI_2;
        dphi = phi_ii - phi_i;
        phi_i = phi_ii;

        if (loop_cnt > 10 )
            break;

        loop_cnt++; // approx 5 iterations
    }

    vector<double> bl(2);
    bl[0] = phi_ii*RAD2DEG;
    bl[1] = lambda*RAD2DEG;

    return bl;
}


//vector<double> geo_f::bl2jtsk05( double b, double l ) {
//    // Modified version
//    /* (phi,lambda) >> (U,V) >> (S,D) >> (\rho, \vaerpsilon) >> (Y,X)
//    * (phi,lambda) - geodetic coordinates on Bessel's ellipsoid
//    * (U,V)        - spherical coordinates on reference sphere
//    * (S,D)        - cartographic coordinates on reference sphere with shifted pole
//    * (\rho, \vaerpsilon) - polar coordinates on the cone
//    * (Y,X)        - rectangular coordinates in the plane (JTSK coordinates)
//    *
//    * coordinates of the shifted pole  \phi_Q      = 48°15'
//                                    \lambda_Q   = 42°30'

//    * input : xn, yn, zn - rectangular coordinates on the Bessel ellipsoid
//    * output: y,x        - rectangular coordinates in the plane (jtsk)
//           h          - height
//    */
//    const double rho_0 = 1298039.004638987;
//    const double s_0   = 78.50000*DEG2RAD ;
//    const double n     = .9799247046208300;
//    const double alpha = 1.000597498371542;
//    const double k     = /*1.003419163966575;*/.996592486900000;
//    //const double k1    = .9999;
//    const double e     = 0.81696831215303e-1;
//    const double vk    = (42. + 31./60. + 31.41725/3600.)*DEG2RAD;
//    const double uk    = (59. + 42./60. + 42.6968885/3600.)*DEG2RAD;
//    //const double u0    = (49. + 27./60. + 32.84625/3600.)*DEG2RAD;

//    // (phi,lambda) >> (U,V)
//    double phi = b*DEG2RAD;
//    double u = 0.2e1 * atan( pow(tan(phi/ 2. + M_PI_4 )
//                                 * pow( (1. - e * sin(phi))
//                                      / (1. + e * sin(phi)), e / 2.), alpha) / k)
//               - M_PI_2;
//    double v = alpha * DEG2RAD * (l + 17.+40./60.); // metodika navrhuje 17°40'

//    // (U,V) >> (S,D)
//    double s = asin(sin(u) * sin(uk) + cos(u) * cos(uk) * cos(vk - v));
//    double d = asin(cos(u) * sin(vk - v) / cos(s));

//    //(S,D) >> (\rho, \vaerpsilon)
//    double rho = rho_0 * pow(tan(s_0 /2. + M_PI_4) / tan(s / 2. + M_PI_4), n);
//    double epsilon = n * d;

//    // (\rho, \vaerpsilon) >> (Y,X)
//    double x = rho * cos(epsilon);
//    double y = rho * sin(epsilon);

//    // Cubic transformation
//    //=======================
//    const double a1  =  0.2946529277e-01;
//    const double a2  =  0.2515965696e-01;
//    const double a3  =  0.1193845912e-06;
//    const double a4  = -0.4668270147e-06;
//    const double a5  =  0.9233980362e-11;
//    const double a6  =  0.1523735715e-11;
//    const double a7  =  0.1696780024e-17;
//    const double a8  =  0.4408314235e-17;
//    const double a9  = -0.8331083518e-23;
//    const double a10 = -0.3689471323e-23;

//    double x_red = x - 1089000, y_red = y - 654000, dy, dx;

//    dy = a2+a3*y_red + a4*x_red + 2.*a5*y_red*x_red + a6*(x_red*x_red - y_red*y_red)
//            +a8*x_red*(x_red*x_red - 3.*y_red*y_red) + a7*y_red(3.*x_red*x_red - y_red*y_red)
//            -4.*a10*x_red*y_red*(x_red*x_red - y_red*y_red)
//            +a9*( pow( x_red, 4.) + pow( y_red , 4. ) - 6.*pow( x_red*y_red, 2.  ) );
//    dx = a1+a3*x_red-a4*y_red - 2.*a6*x_red*y_red + a5*( x_red*x_red - y_red*y_red )
//            +a7*x_red*(x_red*x_red-3.*y_red*y_red) - a8*y_red(3.*x_red*x_red-y_red*y_red )
//            +4.*a9*y_red*x_red(x_red*x_red-y_red*y_red)
//            +10.*( pow( x_red, 4.) + pow( y_red , 4. ) - 6.*pow( x_red*y_red, 2.  ) );


//    vector<double> yx(2);s
//    yx[0] = y - dy;
//    yx[1] = x + dy;

//    return yx;
//}

double geo_f::bilinearInterpolation(double x, double y, double x1, double x2, double y1, double y2, double f11, double f12, double f21, double f22)
{
    double d0 = (x1-x2)*(y1-y2);

    double a0 = (f11*x2*y2 - f12*x2*y1 - f21*x1*y2 + f22*x1*y1);
    double a1 = (-f11*y2   + f12*y1    + f21*y2    - f22*y1   );
    double a2 = (-f11*x2   + f12*x2    + f21*x1    - f22*x1   );
    double a3 = (f11*x2*y2 - f12*x2*y1 - f21*x1*y2 + f22*x1*y1);


    return (a0 + a1*x + a2*y + a3*x*y)/d0;
}

double geo_f::length_sphere(double b1, double l1, double b2, double l2, double rsphere)
{
    if ( abs(b1-b2) <= __EPS__ && abs(l1-l2) <= __EPS__ ) {
        return 0.0;
    }

    // all inputs in deg format
    double phi1 = b1 * DEG2RAD;
    double phi2 = b2 * DEG2RAD;
    double lam1 = l1 * DEG2RAD;
    double lam2 = l2 * DEG2RAD;

    double cosphi = sin(phi1)*sin(phi2) + cos(phi1)*cos(phi2)*cos( lam1 - lam2 );

    if ( cosphi >= 1 ) {
        return 0;
    } else {
        return ( rsphere*acos(cosphi) );
    }
}

double geo_f::lhuilierTheorem(double a, double b, double c, double rsphere)
{
    /*
    a,b,c lengths of the triangle sides
    r radius of a reference sphere
    */

    a /= rsphere;
    b /= rsphere;
    c /= rsphere;

    double s = .5*(a+b+c); // semiperimeter
    double spherical_exces = 4. * atan( sqrt(  tan(s/2.)*tan((s-a)/2.)* tan((s-b)/2.)* tan((s-c)/2.) ));

    if ( isnan(spherical_exces) || isinf(spherical_exces) ) {
        spherical_exces = 0.0; // because of very thin spherical triangles
    }

    return rsphere*rsphere*spherical_exces; //in radias
}




arma::mat geo_f::compute_stokes_mat(const double &phi0,
                                    const double &lam0,
                                    const double &dlat,
                                    const double &dlon,
                                    const double &sphererad,
                                    const double &intrad,
                                    const int &drow,
                                    const int &dcol)
{
    arma::mat psi_mat = arma::zeros<arma::mat>( 2*drow +1 , 2*dcol+1 ) ;

    //---------------------------------------------------------------------------------------------
    // Computing Stokes kernel grid matrix
    // because of the cyclical convolution the matrix si extended in the and by 50 percent by zeros
    for ( int i = 0; i < 2*drow+1; i++ ) {
        for ( int j = 0; j < 2*dcol+1; j++) {
            if ( i == drow && j == dcol ) {
                psi_mat(i,j) = 0;
                continue;
            } else {
                double bi = phi0 + static_cast<double>(i - drow) * dlat;
                double li = lam0 + static_cast<double>(j - dcol) * dlon;

                double psi = geo_f::length_sphere(phi0,lam0, bi,li, 1.0 );

                if ( 1.05*(sin(psi/2.) * 2.* sphererad) >= intrad ) {
                    continue;
                }

                psi_mat(i,j) = geo_f::stokes_kernel<double>(psi);
            }
        }
    }

    return  psi_mat;
}

arma::mat geo_f::kernelmat (  const double &phi0,
                              const double &lam0,
                              const double &dlat,
                              const double &dlon,
                              const double &sphererad,
                              const double &intrad,
                              const arma::uword &drow,
                              const arma::uword &dc_l,
                              const arma::uword &dc_r,
                              const bool& check_radius,
                              double (&kernel_fun) (double))
{

    arma::uword dcol = dc_l + dc_r;
    arma::mat kmat = arma::zeros<arma::mat>( 2*drow+1 , dcol );

    if ( dcol % 2 == 0 ) {
        for ( arma::uword i = 0; i < 2*drow+1 ; i++ ){ // phi index
            double delta_i = (static_cast<double>(i) - static_cast<double>(drow));
            double bi = phi0 + delta_i * dlat;

            for ( arma::uword j = 0 ; j < dcol ; j++){ // lambda index
                double delta_j = (static_cast<double>(j) - static_cast<double>(dc_l));
                double li = lam0 + delta_j * dlat;
                double psi = geo_f::length_sphere(phi0,lam0, bi,li, 1.0 );

                if ( i == drow && j == dc_l  ) {
                    continue;
                }

                if ( check_radius ) {
                    if ( 1.05*(sin(psi/2.) * 2.* sphererad) >= intrad ) {
                        continue;
                    }
                }

                if ( abs(bi) > 90 || abs( li - lam0 ) > 180 + dlon ) {
                    continue;
                }

                double kval = kernel_fun(psi);

                if( isnan(kval) || isinf(kval) )
                    continue;

                kmat(i,j) = kval;
            }
        }
    } else {
        for ( arma::uword i = 0; i < 2*drow+1 ; i++ ){ // phi index
            double delta_i = (static_cast<double>(i) - static_cast<double>(drow));
            double bi = phi0 + delta_i * dlat;

            for ( arma::uword j = 0 ; j < dcol ; j++){ // lambda index
                double delta_j = (static_cast<double>(j) - static_cast<double>(dc_r));
                double li = lam0 + delta_j * dlat;
                double psi = geo_f::length_sphere(phi0,lam0, bi,li, 1.0 );

                if ( abs(bi) > 90 || abs( li - lam0 ) > 180 + dlon ) {
                    continue;
                }

                if ( i == drow && j == dc_r  ) {
                    continue;
                }

                if ( check_radius ) {
                    if ( 1.05*(sin(psi/2.) * 2.* sphererad) >= intrad ) {
                        continue;
                    }
                }

                double kval = kernel_fun(psi);

                if( isnan(kval) || isinf(kval) )
                    continue;

                kmat(i,j) = kval;
            }
        }
    }
    return kmat;
}


arma::mat geo_f::kernelmat2 ( const double &phi0,
                              const double &lam0,
                              const double &dlat,
                              const double &dlon,
                              const double &sphererad,
                              const double &intrad,
                              const arma::uword &dr_l, // low
                              const arma::uword &dr_u,
                              const arma::uword &dc_l,
                              const arma::uword &dc_r,
                              const bool& check_radius,
                              double (&kernel_fun) (double),
                              bool inner_area )
{

    arma::uword dcol = dc_l + dc_r;
    arma::uword drow = dr_u + dr_l;
    arma::mat kmat = arma::zeros<arma::mat>( drow , dcol );

    for ( arma::uword i = 0; i < drow ; i++ ){ // phi index
        double delta_i = (static_cast<double>(i) - static_cast<double>(dr_l));
        double bi = phi0 + delta_i * dlat;

        for ( arma::uword j = 0 ; j < dcol ; j++){ // lambda index
            double delta_j = (static_cast<double>(j) - static_cast<double>(dc_l));
            double li = lam0 + delta_j * dlat;
            double psi = geo_f::length_sphere(phi0,lam0, bi,li, 1.0 );

            if ( inner_area ) {
                if ( i == dr_l && j == dc_l  ) {
                    continue;
                }

                if ( check_radius ) {
                    if ( 1.05*(sin(psi/2.) * 2.* sphererad) >= intrad ) {
                        continue;
                    }
                }

                if ( abs(bi) > 90 || abs( li - lam0 ) > 180 + dlon ) {
                    continue;
                }
            } else {
                if ( check_radius ) {
                    if ( 1.05*(sin(psi/2.) * 2.* sphererad) < intrad ) {
                        continue;
                    }
                }
            }

            double kval = kernel_fun(psi);

            if( isnan(kval) || isinf(kval) )
                continue;

            kmat(i,j) = kval;
        }
    }
    return kmat;
}

arma::mat geo_f::kernel_legen_sum ( const double &phi0,
                                    const double &lam0,
                                    const double &dlat,
                                    const double &dlon,
                                    const double &sphererad,
                                    const double &intrad,
                                    const arma::uword &dr_l, // low
                                    const arma::uword &dr_u,
                                    const arma::uword &dc_l,
                                    const arma::uword &dc_r,
                                    const bool& check_radius,
                                    const string& kernel_type,
                                    const unsigned& nmin,
                                    const unsigned& nmax,
                                    bool inner_area )
{

    arma::uword dcol = dc_l + dc_r;
    arma::uword drow = dr_u + dr_l;
    arma::mat kmat = arma::zeros<arma::mat>( drow , dcol );

    for ( arma::uword i = 0; i < drow ; i++ ){ // phi index
        double delta_i = (static_cast<double>(i) - static_cast<double>(dr_l));
        double bi = phi0 + delta_i * dlat;

        #pragma omp critical

        #pragma omp parallel shared( delta_i , bi,i, kmat , sphererad , intrad )
        {
            #pragma omp for
            for ( arma::uword j = 0 ; j < dcol ; j++){ // lambda index
                double delta_j = (static_cast<double>(j) - static_cast<double>(dc_l));
                double li = lam0 + delta_j * dlat;
                double psi = geo_f::length_sphere(phi0,lam0, bi,li, 1.0 );

                if ( inner_area ) {
                    if ( check_radius ) {
                        if ( 1.05*(sin(psi/2.) * 2.* sphererad) >= intrad ) {
                            continue;
                        }
                    }

                    if ( abs(bi) > 90 || abs( li - lam0 ) > 180 + dlon ) {
                        continue;
                    }
                } else {
                    if ( check_radius ) {
                        if ( 1.05*(sin(psi/2.) * 2.* sphererad) < intrad ) {
                            continue;
                        }
                    }
                }

                arma::vec p_n = legendre::legendre(nmax, cos(psi)); // legendre of P_n ( \cos \psi )
                double kval = legendre::sum_kernel( p_n , kernel_type, nmin, nmax );

                if( isnan(kval) || isinf(kval) )
                    continue;

                kmat(i,j) = kval;
            }
        }
    }
    return kmat;
}

arma::mat geo_f::l03_mat ( const double &phi0,
                           const double &lam0,
                           const double &dlat,
                           const double &dlon,
                           const double &sphererad,
                           const double &intrad,
                           const arma::uword &dr_l, // low
                           const arma::uword &dr_u,
                           const arma::uword &dc_l,
                           const arma::uword &dc_r,
                           const bool& check_radius,
                           bool inner_area )
{

    arma::uword dcol = dc_l + dc_r;
    arma::uword drow = dr_u + dr_l;
    arma::mat kmat = arma::zeros<arma::mat>( drow , dcol );

    for ( arma::uword i = 0; i < drow ; i++ ){ // phi index
        double delta_i = (static_cast<double>(i) - static_cast<double>(dr_l));
        double bi = phi0 + delta_i * dlat;

        for ( arma::uword j = 0 ; j < dcol ; j++){ // lambda index
            double delta_j = (static_cast<double>(j) - static_cast<double>(dc_l));
            double li = lam0 + delta_j * dlat;
            double psi = geo_f::length_sphere(phi0,lam0, bi,li, 1.0 );

            double l0 = sin(psi/2.) * 2.* sphererad;
            if ( inner_area ) {
                if ( i == dr_l && j == dc_l  ) {
                    continue;
                }



                if ( check_radius ) {
                    if ( 1.05*l0 >= intrad ) {
                        continue;
                    }
                }

                if ( abs(bi) > 90 || abs( li - lam0 ) > 180 + dlon ) {
                    continue;
                }
            } else {
                if ( check_radius ) {
                    if ( 1.05*(sin(psi/2.) * 2.* sphererad) < intrad ) {
                        continue;
                    }
                }
            }


            double kval = 1./ (l0*l0*l0);

            if( isnan(kval) || isinf(kval) )
                continue;

            kmat(i,j) = kval;
        }
    }
    return kmat;
}

//arma::mat geo_f::kernelmat2 (const double &phi0,
//                              const double &lam0,
//                              const double &dlat,
//                              const double &dlon,
//                              const double &sphererad,
//                              const double &intrad,
//                              const arma::uword &dr_l, // low
//                              const arma::uword &dr_u,
//                              const arma::uword &dc_l,
//                              const arma::uword &dc_r,
//                              const bool& check_radius,
//                              double (&kernel_fun) (double))
//{

//    arma::uword dcol = dc_l + dc_r;
//    arma::uword drow = dr_u + dr_l;
//    arma::mat kmat = arma::zeros<arma::mat>( drow , dcol );

//    if ( dcol % 2 == 0 ) {
//        for ( arma::uword i = 0; i < drow ; i++ ){ // phi index
//            double delta_i = (static_cast<double>(i) - static_cast<double>(dr_l));
//            double bi = phi0 + delta_i * dlat;

//            for ( arma::uword j = 0 ; j < dcol ; j++){ // lambda index
//                double delta_j = (static_cast<double>(j) - static_cast<double>(dc_l));
//                double li = lam0 + delta_j * dlat;
//                double psi = geo_f::length_sphere(phi0,lam0, bi,li, 1.0 );

////                if ( i == drow && j == dc_l  ) {
////                    continue;
////                }

//                if ( check_radius ) {
//                    if ( 1.05*(sin(psi/2.) * 2.* sphererad) >= intrad ) {
//                        continue;
//                    }
//                }

//                if ( abs(bi) > 90 || abs( li - lam0 ) > 180 + dlon ) {
//                    continue;
//                }

//                double kval = kernel_fun(psi);

//                if( isnan(kval) || isinf(kval) )
//                    continue;

//                kmat(i,j) = kval;
//            }
//        }
//    } else {
//        for ( arma::uword i = 0; i < drow ; i++ ){ // phi index
//            double delta_i = (static_cast<double>(i) - static_cast<double>(dr_l));
//            double bi = phi0 + delta_i * dlat;

//            for ( arma::uword j = 0 ; j < dcol ; j++){ // lambda index
//                double delta_j = (static_cast<double>(j) - static_cast<double>(dc_r));
//                double li = lam0 + delta_j * dlat;
//                double psi = geo_f::length_sphere(phi0,lam0, bi,li, 1.0 );

//                if ( abs(bi) > 90 || abs( li - lam0 ) > 180 + dlon ) {
//                    continue;
//                }

////                if ( i == drow && j == dc_r  ) {
////                    continue;
////                }

//                if ( check_radius ) {
//                    if ( 1.05*(sin(psi/2.) * 2.* sphererad) >= intrad ) {
//                        continue;
//                    }
//                }

//                double kval = kernel_fun(psi);

//                if( isnan(kval) || isinf(kval) )
//                    continue;

//                kmat(i,j) = kval;
//            }
//        }
//    }
//    return kmat;
//}


arma::mat geo_f::compute_lmatrix(const double &phi0,
                                 const double &lam0,
                                 const double &dlat,
                                 const double &dlon,
                                 const double &sphererad,
                                 const double &intrad,
                                 const int &drow,
                                 const int &dcol)
{
    arma::mat l_mat = arma::zeros<arma::mat>( 2*drow +1 , 2*dcol+1 ) ;
    for ( int i = 0; i < 2*drow+1; i++ ) {
        for ( int j = 0; j < 2*dcol+1; j++) {
            if ( i == drow && j == dcol ) {
                l_mat(i,j) = 0;
                continue;
            } else {
                double bi = phi0 + static_cast<double>(i - drow) * dlat;
                double li = lam0 + static_cast<double>(j - dcol) * dlon;

                double psi0 = geo_f::length_sphere(phi0,lam0, bi,li, 1.0 );
                double l0 = 2. * sphererad * sin ( psi0 / 2. );

                if ( 1.05* l0  >= intrad ) {
                    continue;
                }

                l_mat(i,j) = 1. /  (l0*l0*l0);
            }
        }
    }
    return l_mat;
}

double geo_f::zero_area_effect(const double &phi0,
                               const double &lam0,
                               const double &dphi,
                               const double &dlam,
                               double (&kernel_int_fun)(double))
{
    double b0 = phi0 * DEG2RAD;//    double b  = dphi * DEG2RAD;
    double l0 = lam0 * DEG2RAD;//    double l  = dlam * DEG2RAD;

    double db = atan( tan( DEG2RAD ) * sin( dphi * DEG2RAD ));
    double dl = atan( tan( DEG2RAD ) * sin( dlam * DEG2RAD ));

    long int int_b = static_cast<long int>( DEG2RAD * ( 2.* dphi ) / db );
    long int int_l = static_cast<long int>( DEG2RAD * ( 2.* dlam ) / dl );

    double theta = 2.*M_PI / ( 100. * static_cast<double>(int_b + int_l));
    double dtheta = theta;

    long int n       = 0;
    double res       = 0.0;
    // \f$ psi = 0 \f$ value of the integral
    double zint = kernel_int_fun(0.0);

    // numerical integration
    while ( theta <= 2. * M_PI + dtheta ) {
       double b1, b2, l1, l2, bm, lm;
       double psi, alpha_01_1, alpha_01_2, s1, alpha_02_1, alpha_02_2, s2;

       double theta1 = theta;
       double theta2 = theta - dtheta;

       double rphi1 = ( abs(tan(theta1)) <= abs(dphi/dlam) ) ? dlam / abs(cos( theta1 )) : dphi / abs(sin( theta1 ));
       double rphi2 = ( abs(tan(theta2)) <= abs(dphi/dlam) ) ? dlam / abs(cos( theta2 )) : dphi / abs(sin( theta2 ));

       b1 = DEG2RAD*( rphi1 * sin( theta1 ) + phi0);
       l1 = DEG2RAD*( rphi1 * cos( theta1 ) + lam0);
       b2 = DEG2RAD*( rphi2 * sin( theta2 ) + phi0);
       l2 = DEG2RAD*( rphi2 * cos( theta2 ) + lam0);

       bm = .5 * (b1 + b2) ; // in RAD
       lm = .5 * (l1 + l2) ; // in RAD

       psi = acos( sin( b0 ) * sin( bm ) + cos( b0 ) * cos( bm ) * cos((l0 - lm)));

       geo_sphere::geodetic2(phi0, lam0, b1*RAD2DEG, l1*RAD2DEG, 1. , alpha_01_1, alpha_01_2, s1);
       geo_sphere::geodetic2(phi0, lam0, b2*RAD2DEG, l2*RAD2DEG, 1. , alpha_02_1, alpha_02_2, s2);

       double da = alpha_02_1-alpha_01_1 ; // delta alpha
       if ( da < 0.0 ) {
           da += 360.0;
       }

       double s_int = ( kernel_int_fun( psi ) - zint ) * da * DEG2RAD;

       res   += s_int;
       theta += dtheta;
       n++;
    }
    return res;
}


double geo_f::height_tide_transform(const double &h_n,
                                    const double &udeg,
                                    const string &from,
                                    const string &to,
                                    const double &k,
                                    const double &h)
{
    double dH = 0.0;
    double dWg = -0.198*(1.5 * sin(DEG2RAD*udeg)*sin(DEG2RAD*udeg) - 0.5);
    if ( from.compare(to) == 0 ) {
        dH = 0.0;
    } else if ((from.compare("zero tide") == 0 || from.compare("zero_tide") == 0) &&
            (to.compare("mean tide") == 0 || to.compare("mean_tide") == 0)) {
        dH = -dWg;
    } else if ((from.compare("zero tide") == 0 ||
                from.compare("zero_tide") == 0) &&
               (to.compare("tide free") == 0 || to.compare("tide_free") == 0)) {
        dH = (k-h)*dWg;
    } else if ((from.compare("mean tide") == 0 ||
                from.compare("mean_tide") == 0) &&
               (to.compare("zero tide") == 0 || to.compare("zero_tide") == 0)) {
        dH = dWg;
    } else if ((from.compare("mean tide") == 0 ||
                from.compare("mean_tide") == 0) &&
               (to.compare("tide free") == 0 || to.compare("tide_free") == 0)) {
        dH = (1.+k-h)*dWg;
    } else if ((from.compare("tide free") == 0 ||
                from.compare("tide_free") == 0) &&
               (to.compare("zero tide") == 0 || to.compare("zero_tide") == 0)) {
        dH = -(k-h)*dWg;
    } else if ((from.compare("tide free") == 0 ||
                from.compare("tide_free") == 0) &&
               (to.compare("mean tide") == 0 || to.compare("mean_tide") == 0)) {
        dH = -(1.+k-h)*dWg;
    } else {
        dH = 0.0;
    }

    return h_n + dH;
}
