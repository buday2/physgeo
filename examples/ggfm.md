# Glogal Gravity Field Models ( -c ggfm ) or (--ctype ggfm)

## Computation options parameters **--parameters,-p**
- the parameter of the Earth's gravity field to calculate, more than one option 
- is actually allowed ( -q gpot_w gpot_t )

- Computational options for GGFMs
    - 'gpot_v' (gravity potential)
    - 'gpot_w' (gravitational potential)
    - 'gpot_t' (disturbing potential)
    - 'def_vert_eta' (deflection of the vertical - east-west)
    - 'def_vert_xi' (deflection of the vertical - north-south)
    - 'def_vert' (deflection of the vertical)
    - 'grav_anom_sa' (gravity anomaly in spherical form)
    - 'grav_dist_sa' (gravity disturbance in spherical form)
    - 'height_anomaly' (height anomaly)
    - 'grav_gy'
    - 'grav_gx'
    - 'grav_gz'
    - 'terrain' (grid options only)

## Using algorithm for fully normalized assoc. Legen. poly. (**--algorithm,-a** option) 
- 'stan_mod' - standard method of computation, numerically stable up to degree/order around 720
- 'standard' - same as 'stan_mod'
- 'tmfcm' - modified method of computation, numerically stable up to degree/order around 2700
- 'tmfcm_2nd' - modified method of computation, numerically stable up to degree/order around 2700

- the second option is to used 128bit <long double> to extend the numerical stability up to
- ultra-high degrees/orders, but it is slower. Not recommended for models upto degree/order 2700.
- In order to use <long double>, the ```-c ggfm ld``` must be called, **ld = long double**

## solutionType (option **--solution,-s**) stands for the option between
- 'sp' - single point solution option
- 'grid' - the grid is output, but option **--boundaries,-b** is needed
- 'ongrid' - performs the computation on DEM using the gradient approach, DEM must be in ISGEM model

## (**--degree-range,-d**) if the cut for degree is required
- ```-d minumum degree maximum degree```
- otherwise the maximum possible range is used (derived from the header)

## set the grid boundaries for "-s grid" option
- **--boundaries,-b** "bmin bmax lmin lmax height deltab deltal"
- all values in DEG format, except h argument
- passed as single string argument, parser automatically handles it and splits the string into numerical arguments
- **bmin, bmax** is latitude range
- **lmin, lmax** is longitude range
- **deltab** step in latitude direction
- **deltal** step in longitude direction
- **h** reference ellipsoidal height


## path to the GGFM model (option -m/--model)
- default format is International Centre for Global Earth Models (ICGEM) style
- downloaded from webpage [Icgem GGFM models](http://icgem.gfz-potsdam.de/tom_longtime) in gfc format
- but if the option is called ```-m $path2ggfm hdf5```, the **hdf5** format can be used, see the source code
- in ```ggfm.h ``` file, still the header is needed

## path to file with points, if single point option is called (**--file,-f**)
- file in format PTID BDEG LDEG HELL (+more columns, but they are ignored)

## output option (**--output,-o**)
- if it is not used, default name is ***result.txt*** for all computations

## Permanent Tide system managment is option (**--tide-system,-n**)
- if it is not used, the tide system of the model's tide system is used
- options are "mean_tide", "tide_free", "zero_tide"

## ellipsoidGeometry is the reference ellipsoid for the computations (**--ellipsoid,-e**)
- wgs84 is default
- grs80 or bessel ellipsoid
- if the user needs his own, must be defined separately (see ```geodetic_funcions.h``` and ```my_parser.cpp```)

## Love number for C20 term transformation  (option **--love-number,-l**)
- if it is not set, the value $0.29525$ is used

## EXAMPLES 
### single point solution (-s sp)
```{: .language-bash}
path2points="data/CZEPOS.txt" # points of the permanent GNSS network in Czechia
path2model="model/ggm_GRACE16.gfc"
output="CZEPOS_gpotW.txt"

./PhysGeo -c ggfm -a tmfcm -s sp -m $path2ggfm -q gpot_w -f ${path2points}\
-n zero_tide -o ${output}
```

- same computation but the GGFM is in hdf5 format with a separate header file, the program automatically
- searches for the header file

```{: .language-bash}
path2model="model/ggm_GRACE16.hdf5"

./PhysGeo -c ggfm -a tmfcm -s sp -m $path2ggfm hdf5 -q gpot_w -f ${path2points} \
-n zero_tide -o ${output}
```

- grid option (**--solution,-s** grid)
- grid with the constant ellispiodal height (-b option)

```{: .language-bash}
output="world.txt"
./PhysGeo -c ggfm -a tmfcm -s grid -m $path2ggfm hdf5 -q height_anomaly \
-b "-90 90 0 360 100 5 5" -n zero_tide -o ${output}
```

- computes the grid around the globe in 5 DEG step and ellipsoidal height 100
- ongrid option (-s ongrid) - gradient approach

```{: .language-bash}
DEM="data/ETOPO1.isg"
./PhysGeo -c ggfm -s ongrid -f ${DEM} -m ${path2model} -a tmfcm -q grav_anom_sa \
-n zero_tide -o ETOPO1_Delta_g.isg 
```

