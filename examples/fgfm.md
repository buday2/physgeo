
# Forward gravity field modelling  ( -c tess ) or (--ctype tess)

## Computation parameeters options (**--parameters,-p** option)
- 'gpot' - gravity potential $V$
- 'grav_r' - first radial derivative $V^{\prime}_{r}$
- 'grav_b' - $V^{\prime}_{B}$
- 'grav_l' - $V^{\prime}_{L}$
- 'marussi_rr' - paMateřská společnost (Konsolidováno)rt of the Marussi tensor $V^{\prime \prime}_{rr}$
- 'marussi_rb' - part of the Marussi tensor $V^{\prime \prime}_{rB}$
- 'marussi_rl' - part of the Marussi tensor $V^{\prime \prime}_{rL}$
- 'marussi_bb' - part of the Marussi tensor $V^{\prime \prime}_{BB}$
- 'marussi_ll' - part of the Marussi tensor $V^{\prime \prime}_{LL}$
- 'marussi_bl' - part of the Marussi tensor $V^{\prime \prime}_{BL}$

## solutionType (option **--solution,-s**) stands for the option between
- **sp** - single point solution option
- **grid** - the grid is output (computation is performed on the given DEM)

## density value (**--density,-d** option)
- in this case, d stands dor density
- default value is $2670 kg/m^3$
- or also ISGEM format density model can be used
- but the model has to have the same resolution and the boundaries
as the DEM

```{: .language-bash}
-d ${density} isgem , first argument is the path
                      second argument is format of the density model
```

## integration radius (**--radius-r** option)
- **-r** radius of the integration , 111320 approx 1 degree (default), input in **m**

## **-model,-m** path to DEM
- of two models are given, the first path serves as a model
- of the upper layer, second model represents lower layer
- both layers must have the same spatial resolution, boundaries, number
of columns, rows, etc.

### -n option
# additional information to the DEM
# such as format of the inputs

## output option (**--output,-o**)
- if it is not used, default name is **result.txt** for all computations

## Example for residual terrain modelling
```{: .language-bash}
upper_lay="GMTED2010_150_mean_ME_ell_height.isg"
lower_lay="DEM_GMTED-terrain.txt"
density="eu_density.isg"
# models are however not part of this example
./PhysGeo -c tess -s grid -r 18520 -q gpot -m ${upper_lay} ${lower_lay} \
-n isgem -d ${density} isgem
```

```{: .language-bash}
# using ETOPO1 for grav_r computaions for CZEPOS
model="data/ETOPO1.isg"
points="data/CZEPOS.txt"
./PhysGeo -c tess -q grav_r -s sp -f ${points} -m ${model} \
-r 125000 -o CZEPOS_gravr_from_ETOPI1.txt
```