# Solving Stokes or Hotine integrals via convolution ( -c geoid ) 

## Computation options (-q option)
- **hotine** - solving Hotine's integral, using grav. dist. sa
- **stokes** - solving Stokes' integral (gravity anomalies)
- **hotine_g1** - first term after the gravity disturbance member in fixed GBVP
- **stokes_g1** - first term after the gravity anomaly member in fixed GBVP

## -x extend data (needed if the integration is around the whole globe)
- because the integrals are solved by convolution, for the whole globe
- the data must be extended, that means the "western" hemisphere is copied 
- behind the eastern and vice versa. The extended grid then looks (E W E W)
- the results are then cut to original resolution
- **--exatend-data,-x** true

## **--radius,-r** option 
- can be used to set the radius of the reference sphere and the integration
- radius, the default sphere radius is $6.3710087714e+6$ and the integration
- radius is 1 DEG based on the ref. sphere radius. The arguments are provided as
- ```-r SPHRADIUS INTRADIUS``` or ```-r SPHRADIUS```

## **--boundaries,-b** option
- if the integration around the globe is needed and only results for certain geo.
- latitudes is needed, phi_min phi_max in DEG format can be used
- ```-b "40 51.35"``` as an example, the output grid will be in those boundaries

## output option (**--output,-o**)
- if it is not used, default name is **result.txt** for all computations

## **--model,-m** option 
- stands for model or models, based on the **--parameters,-p** option (hotine_g1 requires 2 models)
- only ISGEM models are allowed

```{: .language-bash}
gdist="gdist_EUrope.txt"
terrain="wgs84_ell_EUrope.txt" #  -r 6378136.3  121440 

./PhysGeo -c geoid -q hotine_g1 -s grid -m ${gdist} ${terrain}  \
-r 6378136.3  121440  -o hotine_g1_int_Europe.txt
```

# for whole world and the area of the Czech Republic
```{: .language-bash}
gdist="gdist_MT_EIGEN-10min_grid.txt" # can be easily computed using the ggfm section, step 10' x 10' for example
./PhysGeo -c geoid -p hotine -s grid -x true -m ${gdist} -b "47.5 51.5" \
-o zeta0_EIGEN6C4_MT.txt
 ```
