// Including STL
#include <iomanip>
#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <string>

// Include My Own Library parser and timer   
#include "include/my_parser.h"

int main(int argc, char *argv[]) {
    /*--------------------------------------------*/
    my_parser application = my_parser();
    return application.execute( argc, argv );
    /*--------------------------------------------*/
}
