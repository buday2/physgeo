#ifndef TOPO_CORRECTIONS_H
#define TOPO_CORRECTIONS_H

#include <iomanip>
#include <iostream>
#include <cmath>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <fstream>

// include my own libraries
#include "geodetic_functions.h"
#include "physical_constants.h"

// Using 'ARMADILLO' Library
#include <armadillo>

using namespace std;

const double PI = 3.14159265358979323851280895940618620443274267017841339111328125L;

namespace topo_c {
    template<typename Any>
    inline Any u (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  ( x*y*log(z+r)+y*z*log(x+r)+z*x*log(y+r) \
                - x*x/2.0*atan(y*z/(x*r)) \
                - y*y/2.0*atan(z*x/(y*r)) \
                - z*z/2.0*atan(x*y/(z*r)));
    }
    /* ==============================================================
                           FIRST DERIVATIVES
     ============================================================== */
    template<typename Any>
    inline Any u_x (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  ( x*log(y+r) + y*log(x+r) - z*atan(y*x/(z*r)) );
    }

    template<typename Any>
    inline Any u_y (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  ( y*log(z+r) + z*log(y+r) - x*atan(z*y/(x*r)) );
    }

    template<typename Any>
    inline Any u_z (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  ( z*log(x+r) + x*log(z+r) - y*atan(z*x/(y*r)) );
    }

    /* ==============================================================
                        SECOND DERIVATIVES
     ============================================================== */

    template<typename Any>
    inline Any u_xx (Any x, Any y, Any z){
        return  ( -1.0*atan(y*z/(x*sqrt(x*x+y*y+z*z))));
    }

    template<typename Any>
    inline Any u_yy (Any x, Any y, Any z){
        return  ( -1.0*atan(x*z/(y*sqrt(x*x+y*y+z*z))));
    };

    template<typename Any>
    inline Any u_zz (Any x, Any y, Any z){
        return  ( -1.0*atan(y*x/(z*sqrt(x*x+y*y+z*z))));
    }

    template<typename Any>
    inline Any u_xz (Any x, Any y, Any z){
        return  ( log(y + sqrt(x*x+y*y+z*z)) );
    }

    template<typename Any>
    inline Any u_yz (Any x, Any y, Any z){
        return  ( log(x + sqrt(x*x+y*y+z*z)) );
    }

    template<typename Any>
    inline Any u_xy (Any x, Any y, Any z){
        return  ( log(z + sqrt(x*x+y*y+z*z)) );
    }
    /* ==============================================================
                        THIRD DERIVATIVES
     ============================================================== */
    template<typename Any>
    inline Any u_xxx (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  (  (y*z/r)*( 1.0/(x*x+z*z)+1.0/(x*x+y*y)) );
    }

    template<typename Any>
    inline Any u_yyy (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  (  (x*z/r)*( 1.0/(y*y+z*z)+1.0/(x*x+y*y)) );
    };

    template<typename Any>
    inline Any u_zzz (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  (  (x*y/r)*( 1.0/(y*y+z*z)+1.0/(x*x+z*z)) );
    };

    template<typename Any>
    inline Any u_xyz (Any x, Any y, Any z){
        return  ( 1.0/sqrt(x*x+y*y+z*z) );
    };

    template<typename Any>
    inline Any u_xxy (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  ( (z*x/r)*( -1.0/(x*x+y*y)) );
    };

    template<typename Any>
    inline Any u_xxz (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  ( (y*x/r)*( -1.0/(x*x+z*z)) );
    };

    template<typename Any>
    inline Any u_yyx (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  ( (y*z/r)*( -1.0/(x*x+y*y)) );
    };

    template<typename Any>
    inline Any u_yyz (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  ( (y*x/r)*( -1.0/(z*z+y*y)) );
    };

    template<typename Any>
    inline Any u_zzx (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  ( (y*z/r)*( -1.0/(z*z+x*x)) );
    };

    template<typename Any>
    inline Any u_zzy (Any x, Any y, Any z){
        Any r = sqrt(x*x+y*y+z*z);
        return  ( (y*x/r)*( -1.0/(z*z+y*y)) );
    };
    /* ==============================================================
                          END OF DERIVATIVES
     ============================================================== */
    template<typename Any>
    inline Any integrate_u (Any x1, Any x2, Any y1, Any y2, Any z1, Any z2, const char* deriv){
        /*
         *
         *
         */
        Any x[2] = {x1, x2};
        Any y[2] = {y1, y2};
        Any z[2] = {z1, z2};

        Any integral = (Any) 0;
        Any pm_sign; // plus/minus sign

        if ( (strcmp( deriv, "")== 0)  || (strcmp( deriv, "none")== 0)  || (strcmp( deriv, "-")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "x")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_x(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "y")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_y(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "z")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_z(x[i],y[j],z[k]);
                    }
                }
            } // End  of the first derivatives
        } else if ( (strcmp( deriv, "xy")== 0) || ( strcmp( deriv, "yx")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xy(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( (strcmp( deriv, "xz")== 0) || ( strcmp( deriv, "zx")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xz(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( (strcmp( deriv, "yz")== 0) || ( strcmp( deriv, "zy")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yz(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "xx")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xx(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "yy")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yy(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "zz")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_zz(x[i],y[j],z[k]);
                    }
                }
            } // End  of the second derivatives
        } else if ( strcmp( deriv, "xxx")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xxx(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "yyy")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yyy(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "zzz")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_zzz(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "xyz")== 0) || (strcmp( deriv, "xzy")== 0)
                   || (strcmp( deriv, "yzx")== 0) || (strcmp( deriv, "yxz")== 0)
                   || (strcmp( deriv, "zxy")== 0) || (strcmp( deriv, "zyx")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xyz(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "xxy")== 0) || (strcmp( deriv, "xyx")== 0)
                   || (strcmp( deriv, "yxx")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xxy(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "yyx")== 0) || (strcmp( deriv, "yxy")== 0)
                   || (strcmp( deriv, "xyy")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yyx(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "xxz")== 0) || (strcmp( deriv, "xzx")== 0)
                   || (strcmp( deriv, "zxx")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xxz(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "yyz")== 0) || (strcmp( deriv, "yzy")== 0)
                   || (strcmp( deriv, "zyy")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yyz(x[i],y[j],z[k]);;
                    }
                }
            }
        } else if (   (strcmp( deriv, "zzx")== 0) || (strcmp( deriv, "zxz")== 0)
                   || (strcmp( deriv, "xzz")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yy(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "zzy")== 0) || (strcmp( deriv, "zyz")== 0)
                   || (strcmp( deriv, "yzz")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yy(x[i],y[j],z[k]);
                    }
                }
            }
        };
        return integral;
    }


    // End of quick macros, functions for integration
    // Functions for data loading

    struct  dtm{
        /* digital terrain model */
        const char* model_name;
        const char* model_type;
        const char* units;
        const char* ref_ell;
        double lat_min;
        double lat_max;
        double lon_min;
        double lon_max;
        double nodata;
        double delta_lat;
        double delta_lon;
        bool full_grid;
        double b_cen; /* latitude of the center of the block */
        double l_cen; /* longitude -||- */
        double h_cen; /* ellipsoidal height  -//- */
        unsigned int nrows;
        unsigned int ncols;
        arma::mat grid_data; /* b l h - format */
    };

    struct dtm load_dtm_from_file ( const char* path, const char* ellipsoid, int format ){
        /* load digital terrain model in format of b,l,h
         * b - latitude
         * l - longitude
         * h - ellipsoidal  height
         *
         * format of input file : 1 - id b l h
                                  2 - id l b h
                                  3 - b l h
                                  4 - l b h

                                  1 is default setting
        */
        struct dtm terrain_model;

        arma::mat data; data.load( path, arma::raw_ascii);

        arma::colvec  b_vec, l_vec, h_vec;

        /* reading input file based on format type */
        if ( format == 2 ){
            b_vec = data.col(2);
            l_vec = data.col(1);
            h_vec = data.col(3);
        } else if ( format == 3 ){
            b_vec = data.col(0);
            l_vec = data.col(1);
            h_vec = data.col(2);
        } else if ( format == 4){
            b_vec = data.col(1);
            l_vec = data.col(0);
            h_vec = data.col(2);
        } else {
            b_vec = data.col(1);
            l_vec = data.col(2);
            h_vec = data.col(3);
        }

        data.set_size( b_vec.n_rows,3 ) ;
        data.col(0) = b_vec;
        data.col(1) = l_vec;
        data.col(2) = h_vec;

        b_vec.reset(); l_vec.reset(); h_vec.reset();

        /* extracting basic information */
        map<double, double> latitude, longitude;

        for (unsigned int i = 0; i < data.n_rows ; i++){
            latitude[ arma::as_scalar(data( i,0 )) ] = 1.;
            longitude[ arma::as_scalar(data( i,1 )) ] = 1.;
        };

        map<double,double >::iterator it, iit;
        iit = latitude.begin(); iit++;

        double latavg = 0.0;
        for ( it=latitude.begin(); it!=latitude.end(); it++){
            latavg += abs( iit->first - it->first );

            iit++;
            if ( iit == latitude.end() ) break;
        }

        double delta_lat = latavg/((double) latitude.size()-1 );

        iit = longitude.begin(); iit++;
        double lonavg = 0.0;
        for ( it=longitude.begin(); it!=longitude.end(); it++){
            lonavg += abs( iit->first - it->first );

            iit++;
            if ( iit == longitude.end() ) break;
        }

        double delta_lon = lonavg/((double) longitude.size()-1 );

        double lat_max = data.col(0).max();
        double lat_min = data.col(0).min();
        double lon_max = data.col(1).max();
        double lon_min = data.col(1).min();
        double nodata  = 9999.999;
        /*
        double delta_lat = delta_lat;
        double delta_lon = delta_lon;
        */
        unsigned int nrows = latitude.size();
        unsigned int ncols = longitude.size();

        arma::mat grid_data(nrows, ncols); grid_data.fill(nodata);
        unsigned int m,n;

        for (unsigned int i = 0 ; i < data.n_rows; i++){
            m = (unsigned int) round( (data(i,0 ) - lat_min)/delta_lon );
            n = (unsigned int) round( (data(i,1 ) - lon_min)/delta_lat );

            grid_data(m,n) = data(i,2);
        }


        /* digital terrain model */
        terrain_model.model_name = "noname";
        terrain_model.model_type = "terrain";
        terrain_model.units      = "metres";
        terrain_model.ref_ell    = ellipsoid;
        terrain_model.lat_min    = lat_min;
        terrain_model.lat_max    = lat_max;
        terrain_model.lon_min    = lon_min;
        terrain_model.lon_max    = lon_max;
        terrain_model.nodata     = 9999.999;
        terrain_model.delta_lat  = delta_lat;
        terrain_model.delta_lon  = delta_lon;
        terrain_model.full_grid  = true;
        terrain_model.nrows      = nrows;
        terrain_model.ncols      = ncols;
        terrain_model.grid_data  = grid_data; /* b l h - format */

        return terrain_model;
    };

    void  print_dtm_to_file ( struct dtm terrain_model, const char* path ){
        /* print to file in ISGEM format */
        ofstream f; f.open( path, ios::out);

        f << "begin_of_head ==============================================="           << endl;
        f << "model name : " << terrain_model.model_name                               << endl;
        f << "model type : " << terrain_model.model_type                               << endl;
        f << "units      : " << terrain_model.units                                    << endl;
        f << "reference  : " << terrain_model.ref_ell                                  << endl;
        f << "lat min    = " << setw(10) << setprecision(6) << terrain_model.lat_min   << endl;
        f << "lat max    = " << setw(10) << setprecision(6) << terrain_model.lat_max   << endl;
        f << "lon min    = " << setw(10) << setprecision(6) << terrain_model.lon_min   << endl;
        f << "lon max    = " << setw(10) << setprecision(6) << terrain_model.lon_max   << endl;
        f << "delta lat  = " << setw(10) << setprecision(6) << terrain_model.delta_lat << endl;
        f << "delta lon  = " << setw(10) << setprecision(6) << terrain_model.delta_lon << endl;
        f << "nrows      = " << setw(10) << terrain_model.nrows                        << endl;
        f << "ncols      = " << setw(10) << terrain_model.ncols                        << endl;
        f << "nodata     = " << setw(10) << setprecision(6) << terrain_model.nodata    << endl;
        f << "ISG format = " << setw(10) << "1.0"                                      << endl;
        f << "end_of_head ================================================="           << endl;

        for ( unsigned int i = 0; i < terrain_model.nrows; i++){
            for (unsigned int j = 0; j < terrain_model.ncols; j++){
                f << setw(10) << setprecision(6) << terrain_model.grid_data(i,j) << " ";
            }
            f << endl;
        };
        f.close();
    }

    double get_value_from_dtm ( struct dtm terrain_model, unsigned int rown ,
                                unsigned int coln,  double& b, double& l  ){
        b = terrain_model.lat_min+rown*terrain_model.delta_lat;
        l = terrain_model.lon_min+coln*terrain_model.delta_lon;

        return terrain_model.grid_data(rown,coln) ;
    };

    inline void get_coord_from_dtm ( struct dtm terrain_model, unsigned int rown ,
                                unsigned int coln,  double& b, double& l  ){
        b = terrain_model.lat_min+rown*terrain_model.delta_lat;
        l = terrain_model.lon_min+coln*terrain_model.delta_lon;
    };

    void get_index_from_dtm (struct dtm terrain_model, double b, double l, unsigned int& i, unsigned int& j ){
        i = (unsigned int) round( (b - terrain_model.lat_min)/terrain_model.delta_lon );
        j = (unsigned int) round( (l - terrain_model.lon_min)/terrain_model.delta_lat );
    }

    struct dtm get_subgrid_from_dtm ( struct dtm terrain_model, struct geo_f::ellipsoid<double> ell,
                                unsigned int rown, unsigned int coln, double radius ){
        /* extract close points from dtm */
        /* TODO - this function is full of holes */
        double phi, lambda;
        get_coord_from_dtm( terrain_model, rown,coln, phi, lambda);

        phi *= PI/180.; /* converting to rad */
        double n = ell.a_axis/sqrt( 1. - ell.eccentricity*sin(phi)*sin(phi) );
        double m = (ell.a_axis*(1. - ell.eccentricity))/pow( 1. - ell.eccentricity * pow (sin(phi),2.0), 3./2.);

        double dlon = radius/(n*cos(phi))*(180./PI);    /* in deg format */
        double dlat = radius/m *(180./PI);              /* in deg format */

        unsigned int drow = (unsigned int) round(dlon/terrain_model.delta_lon) ;
        unsigned int dcol = (unsigned int) round(dlat/terrain_model.delta_lat) ;

        int leftb, rightb, upb, lowb;

        leftb   = ( (coln-dcol) >= 0 ) ? (coln-dcol) : 0;
        leftb   = ( leftb < 0 ) ? 0 : leftb;
        rightb  = ( (coln+dcol) < terrain_model.ncols ) ? (coln+dcol) : terrain_model.ncols-1;

        lowb     = ( (rown-drow) >= 0 ) ? (rown-drow) : 0;
        lowb     = ( lowb < 0 ) ? 0 : lowb;
        upb    = ( (rown+drow) < terrain_model.nrows) ? (rown+drow) : terrain_model.nrows-1;

        arma::mat grid_selection = terrain_model.grid_data.submat( arma::span(lowb, upb), arma::span(leftb, rightb) );


        bool full_grid;
        if ( ( (unsigned int) (leftb+rightb)/2 != coln ) || ((unsigned int) (upb+lowb)/2!= rown ) ){
            full_grid = false;
        } else {
            full_grid = true;
        }

        //double bmin, bmax, lmin, lmax;

        struct dtm submodel;
        submodel.model_name = "noname";
        submodel.model_type = "terrain";
        submodel.units      = "metres";
        submodel.ref_ell    = (ell.name).c_str();
        submodel.lat_min    = terrain_model.lat_min + terrain_model.delta_lat
                                * ( (double) lowb  );
        submodel.lat_max    = terrain_model.lat_min + terrain_model.delta_lat
                                * ( (double) upb   );
        submodel.lon_min    = terrain_model.lon_min + terrain_model.delta_lon
                                * ( (double) leftb  );
        submodel.lon_max    = terrain_model.lon_min + terrain_model.delta_lon
                                * ( (double)  rightb );
        submodel.nodata     = 9999.999;
        submodel.delta_lat  = terrain_model.delta_lat;
        submodel.delta_lon  = terrain_model.delta_lon;
        submodel.full_grid  = full_grid;
        submodel.nrows      = grid_selection.n_rows;
        submodel.ncols      = grid_selection.n_cols;
        submodel.grid_data  = grid_selection; /* b l h - format */

        submodel.b_cen = phi*180./PI;
        submodel.l_cen = lambda;
        submodel.h_cen = arma::as_scalar(terrain_model.grid_data(rown, coln));

        return submodel;
    };

    void convert_subgrid2neu ( struct dtm subgrid, struct geo_f::ellipsoid<double> ell,
                              arma::mat& nloc, arma::mat& eloc, arma::mat& vloc ){
        /* converting subgrid to local topocentric coordinate system
         * matrices nloc, eloc, vloc are the output from this function
         */

        // TODO (mbuday#1#): nodata item not implemented yet

        /* Differential relations between x,y,z <-> b,l,h */
        nloc.reset();         eloc.reset();        vloc.reset();

        vector<double> xyz_cen = geo_f::blh2xyz(subgrid.b_cen, subgrid.l_cen, subgrid.h_cen, ell  );
//        arma::colvec xyzcen(3); xyzcen << xyz_cen[0] << arma::endr
//                                       << xyz_cen[1] << arma::endr
//                                       << xyz_cen[2] << arma::endr;

        double phi    = subgrid.b_cen * (PI/180.); /* converting to rad */
        double lambda = subgrid.l_cen * (PI/180.); /* converting to rad */
        //double h      = subgrid.h_cen;

//        double n = ell.a_axis/sqrt( 1. - ell.eccentricity*sin(phi)*sin(phi) );
//        double m = (ell.a_axis*(1. - ell.eccentricity))/pow( 1. - ell.eccentricity * pow (sin(phi),2.0), 3./2.);

        arma::mat rot_neu;

        rot_neu << -sin(phi)*cos(lambda) << -sin(phi)*sin(lambda) << cos(phi) << arma::endr
                << -sin(lambda)          << cos(lambda)           << 0.0      << arma::endr
                << cos(phi)*cos(lambda)  << cos(phi)*sin(lambda)  << sin(phi) << arma::endr;

        /*
        // Not working as i expected
        arma::mat jacob;

        jacob << -1.*(m+h)*sin(phi)*cos(lambda) << -1.*(n+h)*cos(phi)*sin(lambda)
                                                << cos(phi)*cos(lambda) << arma::endr
              << -1.*(m+h)*sin(phi)*sin(lambda) << (n+h)*cos(phi)*cos(lambda)
                                                << cos(phi)*sin(lambda) << arma::endr
              << (m+h)*cos(phi) << 0.0          << sin(phi)             << arma::endr;

        arma::mat blh2neu = rot_neu * jacob;
        */
        /* (dx , dy , dz ).t() = jacob * (db, dl, dh).t()
         * ( n , e  , v  ).t() = rot_neu * (dx , dy , dz ).t()
         *                     = rot_neu * jacob * (db, dl, dh).t()
         *                     = blh2neu * (db, dl, dh).t()
         */

        /* creating db, dl, dh for the whole grid */
        arma::mat dxdydz( 3 ,subgrid.ncols * subgrid.nrows );
        arma::colvec temp;

        unsigned int mm = subgrid.grid_data.n_rows;
        unsigned int nn = subgrid.grid_data.n_cols;

        vector<double> xyz;
        //#pragma omp shared(subgrid,temp,xyz_cen)
        {
            for ( unsigned int i = 0; i<mm; i++ ){
                #pragma omp parallel for private(xyz,temp)
                for (unsigned int j = 0; j<nn; j++ ){
                    xyz = geo_f::blh2xyz( subgrid.lat_min + ((double) i* subgrid.delta_lat),
                                          subgrid.lon_min + ((double) j* subgrid.delta_lon),
                                          arma::as_scalar(subgrid.grid_data(i,j)), ell);

                    temp << xyz[0] - xyz_cen[0]  << arma::endr
                         << xyz[1] - xyz_cen[1]  << arma::endr
                         << xyz[2] - xyz_cen[2]  << arma::endr  ;

    //                temp << ((subgrid.lat_min + ((double) i* subgrid.delta_lat))*(PI/180.)  - phi)  << arma::endr  /* dphi in rad   */
    //                     << ((subgrid.lon_min + ((double) j* subgrid.delta_lon))*(PI/180) - lambda) << arma::endr  /* dlambda in rad*/
    //                     << arma::as_scalar(subgrid.grid_data(i,j) ) - h                  << arma::endr; /* dh      */

                    dxdydz.col(i*nn+j) = temp;
                    temp.reset();
                }
            };
        }

        arma::mat neu = rot_neu*dxdydz;

        nloc.resize( mm,nn );
        eloc.resize( mm,nn );
        vloc.resize( mm,nn );

        for ( unsigned int i = 0; i<mm; i++ ){
            for ( unsigned int  j = 0; j<nn; j++ ){
                nloc(i,j) = neu(0,i*nn+j);
                eloc(i,j) = neu(1,i*nn+j);
                vloc(i,j) = neu(2,i*nn+j);
            }
        }


        dxdydz.reset();
    }; /* END - convert_subgrid2neu */

    double compute_topographic_contribution (arma::mat nloc, arma::mat eloc, arma::mat vloc,
                                             struct geo_f::ellipsoid<double> ell, double b,
                                             double radius, double depth, const char* deriv){
        /* Compute the gravitational potential and its derivatives for the prism
         *
         * matrices nloc, eloc, vloc are obtained from dtm
         * ell      - rotational ellipsoid on which the original geodetic coordinates are defined
         * radius   - maximum allowed distance from the origin of the local topocentric coordinate system
         * depth    - depth of the rectification plane
         */

        arma::mat n_submat, e_submat, v_submat;
        double n_avg, e_avg, v_avg, dist4beg ; /*dist4beg = distance from beginning */
        double x1,x2,y1,y2,z1,z2, density;
        double mean_r = geo_f::meanradius( b , ell );

        double sum = 0.0;

        #pragma omp parallel shared(nloc,eloc,vloc,radius,depth,deriv )
        {
            for (  unsigned int i = 0 ; i < nloc.n_rows -1 ; i++ ){
                #pragma omp parallel for private(n_submat, e_submat, v_submat,n_avg, e_avg, v_avg, dist4beg,x1,x2,y1,y2,z1,z2, density) reduction(+:sum)
                for (  unsigned int j = 0; j < nloc.n_cols -1; j++  ){
                    // TODO: nodata item not implemented yet
                    n_submat = nloc.submat( arma::span(i, i+1), arma::span(j,j+1) );
                    e_submat = eloc.submat( arma::span(i, i+1), arma::span(j,j+1) );
                    v_submat = vloc.submat( arma::span(i, i+1), arma::span(j,j+1) );

                    n_avg = arma::accu( n_submat )/4.;
                    e_avg = arma::accu( e_submat )/4.;
                    v_avg = arma::accu( v_submat )/4.;

                    dist4beg = sqrt ( n_avg*n_avg + e_avg*e_avg ); /* distance between the center of
                                                                      the trapezoid and the origin of the neu */
                    if ( dist4beg <= radius ){
                        x1 = (n_submat(1,0) + n_submat(1,1) ) /2.;
                        x2 = (n_submat(0,0) + n_submat(0,1) ) /2.;
                        y1 = (n_submat(0,1) + n_submat(1,1) ) /2.;
                        y2 = (n_submat(0,0) + n_submat(1,0) ) /2.;

                        z1 = -1.*dist4beg*dist4beg/(2*mean_r) - depth;
                        z2 = v_avg;

                        if ( z2 < z1 ){
                            density = 0.0;
                        } else {
                            density = MEAN_DENSITY*NG_CONST;
                        }

                        sum += density * integrate_u<double>(x1,x2,y1,y2,z1,z2, /*const char*/ deriv );
                    }
                }
            }
        }

        return sum;
    };

    double compute_topographic_contribution (arma::mat nloc, arma::mat eloc, arma::mat vloc,
                                             struct geo_f::ellipsoid<double> ell, double b,
                                             double radius, double depth, const char* direction,
                                             const char* deriv){
        /* Compute the gravitational potential and its derivatives for the prism
         *
         * matrices nloc, eloc, vloc are obtained from dtm
         * ell      - rotational ellipsoid on which the original geodetic coordinates are defined
         * radius   - maximum allowed distance from the origin of the local topocentric coordinate system
         * depth    - depth of the rectification plane, or height in this scenario
         * direction - from surface to geoid/quasi-geoid or the opposite direction
                     - from surface is the default option
                     - the opposite direction is called with: direction = "geoid2surf"
         */

        arma::mat n_submat, e_submat, v_submat;
        double n_avg, e_avg, v_avg, dist4beg ; /*dist4beg = distance from beginning */
        double x1,x2,y1,y2,z1,z2, density;
        double mean_r = geo_f::meanradius( b , ell );

        double sum = 0.0;

        density = MEAN_DENSITY*NG_CONST;
        #pragma omp parallel shared(nloc,eloc,vloc,radius,depth,deriv )
        {
            for ( unsigned int i = 0 ; i < nloc.n_rows -1 ; i++ ){
                #pragma omp parallel for private(n_submat, e_submat, v_submat,n_avg, e_avg, v_avg, dist4beg,x1,x2,y1,y2,z1,z2, density) reduction(+:sum)
                for ( unsigned int j = 0; j < nloc.n_cols -1; j++  ){
                    n_submat = nloc.submat( arma::span(i, i+1), arma::span(j,j+1) );
                    e_submat = eloc.submat( arma::span(i, i+1), arma::span(j,j+1) );
                    v_submat = vloc.submat( arma::span(i, i+1), arma::span(j,j+1) );

                    n_avg = arma::accu( n_submat )/4.;
                    e_avg = arma::accu( e_submat )/4.;
                    v_avg = arma::accu( v_submat )/4.;

                    dist4beg = sqrt ( n_avg*n_avg + e_avg*e_avg ); /* distance between the center of
                                                                      the trapezoid and the origin of the neu */
                    if ( dist4beg <= radius ){
                        x1 = (n_submat(1,0) + n_submat(1,1) ) /2.;
                        x2 = (n_submat(0,0) + n_submat(0,1) ) /2.;
                        y1 = (n_submat(0,1) + n_submat(1,1) ) /2.;
                        y2 = (n_submat(0,0) + n_submat(1,0) ) /2.;

                        if ( strcmp(direction, "geoid2surf") == 0 ){
                            z2 = -1.*dist4beg*dist4beg/(2*mean_r) - depth;
                            z1 = v_avg;
                        } else {
                            z1 = -1.*dist4beg*dist4beg/(2*mean_r) - depth;
                            z2 = v_avg;
                        }

                        if ( z2 < z1 ){
                            density = 0.0;
                        } else {
                            density = MEAN_DENSITY*NG_CONST;
                        }

                        sum += density * integrate_u<double>(x1,x2,y1,y2,z1,z2, deriv );
                    }
                }
            }
        }
        return sum;
    };

    arma::colvec basic_stats( arma::mat res)/*, arma::colvec& stats )*/{
        /* res = {b l h tc} */
        arma::colvec stats = arma::zeros<arma::colvec>(9);

        stats(0) = res.col(0).min(); /* b_min*/
        stats(1) = res.col(0).max(); /* b_max */
        stats(2) = res.col(1).min(); /* l_min */
        stats(3) = res.col(1).max(); /* l_max */
        stats(4) = res.col(2).min(); /* h_min */
        stats(5) = res.col(2).max(); /* h_max */
        stats(6) = res.col(3).min(); /* tc_min */
        stats(7) = res.col(3).max(); /* tc_max */
        stats(8) = (double) res.n_rows; /* number of computed points */
        return stats;
    };

    void interpolate_missing_dtm_data ( struct dtm terrainmodel){
        cout << "Function not written yet! BAZINGA!!! " << endl;
    };

    void print2file( arma::mat res, const char* name , const char* deriv){
        arma::colvec stat = basic_stats(res);

        ofstream f; f.open( name, ios::out );

        f << "# Computed topographic contribution from DTM. Used derivation "
          << deriv << endl;
        f << "# b_min " << setw(9) << setprecision(6) << arma::as_scalar(stat(0)) << ", ";
        f << "b_max " << setw(9) << setprecision(6) << arma::as_scalar(stat(1)) << ", ";
        f << "l_min " << setw(9) << setprecision(6) << arma::as_scalar(stat(2)) << ", ";
        f << "l_max " << setw(9) << setprecision(6) << arma::as_scalar(stat(3)) << ", ";
        f << "h_min " << setw(9) << setprecision(6) << arma::as_scalar(stat(4)) << ", ";
        f << "h_max " << setw(9) << setprecision(6) << arma::as_scalar(stat(5)) << endl;
        f << "# tc_min " << setw(9) << setprecision(6) << arma::as_scalar(stat(6)) << ", ";
        f << "tc_max " << setw(9) << setprecision(6) << arma::as_scalar(stat(7)) << ", ";
        f << " number of computed points" << setw(9) << setprecision(6)
                                          << (int) arma::as_scalar(stat(8)) << endl;

        for ( unsigned int i = 0; i < res.n_rows; i++){
            f << setw(12) << setprecision(9) << arma::as_scalar( res(i,0) ) << " ";
            f << setw(12) << setprecision(9) << arma::as_scalar( res(i,1) ) << " ";
            f << setw(12) << setprecision(9) << arma::as_scalar( res(i,2) ) << " ";
            f << setw(12) << setprecision(9) << arma::as_scalar( res(i,3) ) << endl;
        };
        f.close();
    };


//    double compute_topo_contribution_from_dtm( double phi, double lambda, double height,
//                                                 struct dtm digtermod,
//                                                 const char* input, const char* output,
//                                                 int format, geo_f::ellipsoid<double> ell,
//                                                 double radius, double depth,
//                                                 const char* derivative ){
//        /* input - path to the dtm model
//         * format of the dtm in file    1 - id b l h
//                                        2 - id l b h
//                                        3 - b l h
//                                        4 - l b h
//         * ell          - geocentric rotation ellipsoid
//         * radius       - radius of the circle of the area of interest
//         * depth        - depth of the "rectification" plane
//         * derivative   - options
//         *              >> ""/"none"/"-" - no derivation ( units of the g. potential )
//                        >> "x"           - derivation of the potential in x axis
//                        >> "y"           - derivation of the potential in y axis
//                                >> in the local topocentric coordinates d/dx a d/dy
//                                   are practically characteristics of the deflection
//                                   of the vertical
//                        >> "xy"/"yx"/"xx"/"yy/..." - second derivative
//                        >> "xxx"/"xxy"/"xyx"/..../"zzz" - third derivative
//         *
//         */
//        //struct dtm digtermod= load_dtm_from_file( input, (ell.name).c_str(), format );
//        struct dtm submod;
//        double res, b, l , h; /* result */
//        unsigned int ii,jj;
//        arma::mat nloc, vloc, eloc;
//        arma::mat r;
//        arma::rowvec temp;
//
//        get_index_from_dtm (digtermod, phi, lambda, ii, jj);
//        struct dtm = get_subgrid_from_dtm ( digtermod, ell,
//                                         unsigned int rown, unsigned int coln, radius );
//
//        submod = get_subgrid_from_dtm( digtermod, ell, ii, jj, radius );
//
//        submod.b_cen = phi;
//        submod.l_cen = lambda;
//        submod.h_cen = height;
//
//        if ( submod.full_grid == false)
//            return 1./0.;
//        else {
//            convert_subgrid2neu( submod, ell, nloc,eloc,vloc);
//            res = compute_topographic_contribution(nloc,eloc,vloc,ell,phi,
//                                                    radius,height,derivative);
//
//            return res;
//        }
//    };

    arma::mat compute_topo_contribution_from_dtm( const char* input, const char* output,
                                                 int format, geo_f::ellipsoid<double> ell,
                                                 double radius, const char* direction,
                                                 const char* derivative ){
        /* input - path to the dtm model
         * format of the dtm in file    1 - id b l h
                                        2 - id l b h
                                        3 - b l h
                                        4 - l b h
         * ell          - geocentric rotation ellipsoid
         * radius       - radius of the circle of the area of interest
         * depth        - depth of the "rectification" plane
         * direction    - the topographic contribution is computed from
                          a) from the Earth's surface to geoid
                          b) the opposite way

                        variant "a" is by default, for variant b : direction="geoid2surf"
         * derivative   - options
         *              >> ""/"none"/"-" - no derivation ( units of the g. potential )
                        >> "x"           - derivation of the potential in x axis
                        >> "y"           - derivation of the potential in y axis
                                >> in the local topocentric coordinates d/dx a d/dy
                                   are practically characteristics of the deflection
                                   of the vertical
                        >> "xy"/"yx"/"xx"/"yy/..." - second derivative
                        >> "xxx"/"xxy"/"xyx"/..../"zzz" - third derivative
         *
         */
        struct dtm digtermod= load_dtm_from_file( input, (ell.name).c_str(), format );
        struct dtm submod;
        double res, b, l , h; /* result */
        arma::mat nloc, vloc, eloc;
        arma::mat r;
        arma::rowvec temp;

        //#pragma omp shared(digtermod,ell,radius,depth,derivative,r)
        {
            //#pragma omp for collapse(2) private(submod, nloc,vloc,eloc,res,h,temp)
            for ( unsigned int i = 0; i < digtermod.nrows; i++){
                for (unsigned int j = 0; j < digtermod.ncols; j++){
                    submod = get_subgrid_from_dtm ( digtermod, ell, i,j, radius);
                    if ( submod.full_grid == false)
                        goto skippoint;

                    convert_subgrid2neu( submod, ell, nloc,eloc,vloc);
                    h   = get_value_from_dtm( digtermod, i , j, b, l );

                    res = compute_topographic_contribution(nloc,eloc,vloc,ell,submod.b_cen,
                                                           radius,h,direction,derivative);


                    temp << b << l << h << res << arma::endr;

                    r = arma::join_cols( r, temp );

                    temp.reset();
                    skippoint:{};

                }
            }
        }

        print2file( r , output, derivative);
        return r;
    };

///* ===================================================================================== */
///* OLD AND REPLACED BY NEWER ALGORITHMS                                                  */
///* ===================================================================================== */
//    arma::mat load_matrix (const char* path, const char* data_order ){
//        /* path - path to the file you want to load as matrix
//         * data_order - order of data {blh, lbh, else}
//                        default output is in b l h order,
//                        'lbh' option just switch 1st and 2nd column
//         */
//        arma::mat data; data.load(path, arma::raw_ascii);
//
//        if ( strcmp(data_order, "lbh")==0 ){
//            arma::colvec col1,col2;
//            col1 = data.col(0);
//            col2 = data.col(1);
//
//            data.col(0) = col2;
//            data.col(1) = col1;
//        } else {};
//        return data;
//    }
//
//    arma::mat load_matrix (const char* path ){
//        arma:mat data;
//        data = load_matrix(path, "else");
//    };
//
//    template <typename Any>
//    arma::mat blh2xyz (arma::mat blh, struct geo_f::ellipsoid<Any> ell){
//        // creating mat consisting of x,y,z coordinates in columns
//        // from mat consisting of b,l,h in columns
//        typedef vector<Any> stdvec; stdvec temp;
//        arma::mat xyz; xyz.copy size(blh);
//
//        for (unsigned int i=0; i<blh.n_rows; i++ ){
//            temp = geo_f::blh2xyz(blh(i,0),blh(i,1),blh(i,2), ell);
//            xyz.col(i) = arma::conv_to< rowvec >::from(temp);
//            temp.clear();
//        }
//        return xyz;
//    };
//
//    template <typename Any>
//    arma::mat topographic_corrections (arma::mat xyz,Any radius, const char* derivative,
//                                       Any rho, Any kappa,
//                                       struct  geo_f::ellipsoid<Any> ell){
//        /* Input:
//         * xyz - matrix with dimension n x 3, xyz in R^n x R^3
//         *       columns  containing rectangular coordinates [x, y, z]
//         * radius - maximum radius/length for computation of topographic corrections
//         * derivative (variation of 1/r equation)
//         * rho - mean density
//         * kappa - newton gravity constant
//         * ellipsoid - needed for coordinate transformation, from xyz to blh
//                       then b,l values are used for transformation of dx, dy, dz
//                       to local topocentric coordinate system
//         */
//        Any PI = (Any) 3.14159265358979323851280895940618620443274267017841339111328125L;
//        vector<Any> blh;
//        Any b,l;
//        arma::mat neu;
//        arma::mat rot = arma::zeros<arma::mat>(3,3);
//
//        /* testing if xyz is empty matrix */
//        if ( xyz.is_empty() ){
//            cout << "ERROR: Input matrix \"xyz\" is empty !!" << endl;
//            goto endfunction;
//        }
//
//
//        /* testing xyz dimension */
//        if ( xyz.n_rows > xyz.n_cols || xyz.n_cols == 3 ){
//            continue;
//        } else if ( xyz.n_rows > xyz.n_cols || xyz.n_rows == 3 ){
//            xyz = xyz.t();
//        }
//
//        for (unsigned int i=0; i<xyz.n_rows; i++){
//            blh = geo_f::xyz2blh(xyz(i,0),xyz(i,1),xyz(i,2),ell); // converting xyz -> blh
//
//            b = blh[0]*(PI/180.0);
//            l = blh[1]*(PI/180.0);
//            /* rotation matrix to local topocentric coordinate system */
//            rot << -sin(b)*cos(l) << -sin(b)*sin(l) << cos(b)   << arma::endr
//                << -sin(l)        << cos(l)         << 0.0      << arma::endr
//                << cos(b)*cos(l)  << cos(b)*sin(l)  << sin(b)   << arma::endr;
//
//            for ( unsigned int j = 0; j< xyz.n_rows; j++){
//                /* select all points that are closer to i-th point
//                   than radius value */
//                if ( i == j ){
//                    continue;
//                } else {
//                    if ( (xyz.row(i)-xyz.row(j))*(xyz.row(i)-xyz.row(j)).t() <= radius ){
//                        arma::join_rows(neu, xyz.row(j));
//                    } else {
//                        continue;
//                    }
//                }
//            }                       // end of finding close points
//            neu.col(0) - xyz(i,0);  // reduction to selected point
//            neu.col(1) - xyz(i,1);  // -||-
//            neu.col(2) - xyz(i,2);  // -||-
//
//            neu = rot*neu.t();      // transformation to local topocentric coordinate system
//            arma::sort(neu,1);
//
//            for ( unsigned int j = 0; j<neu.rows; j++){
//
//            }
//
//            neu.reset(); // set the number of elements to zero
//        }
//
//        endfunction:
//    }


}       // end namespace topo_c


#endif // TOPO_CORRECTIONS_H
