#ifndef GEODETIC_FUNCTIONS_H
#define GEODETIC_FUNCTIONS_H

#include <cmath>
#include <complex>
#include <vector>
#include <iostream>
#include <iomanip>
#include <map>
#include <iterator>
#include <cstdio>
#include <string>
#include <cstring>

// My own libraries
#include "physical_constants.h"
#include "geodetic_sphere.h"
#include "legendre.h"

// Armadillo Matrix Library
#include <armadillo>


using namespace std;

namespace geo_f {

    template <typename Any>
    struct ellipsoid {      // rotational
        string name;        // E.g. WGS84
        Any a_axis;         // semi-major axis
        Any b_axis;         // semi-minor axis
        Any eccentricity;   // First numerical eccentricity squared!! := e^2
        Any gm;             // geocentric gravitational constant
        Any omega;          // speed of rotation
        Any j20;            // second stokes zonal coefficient [unitless]
        Any g_e;            // normal gravity acceleration on equator [ms^-2]
        Any g_p;            // normal gravity acceleration on poles [m s^-2]
        Any f;              // flattening

        /**
         * @brief get_normal_gravity
         * @param phi - geodetic latitude in DEG format
         * @return normal gravity value on the surface of the ellipsoid in \f$ m \cdot s^{-2} \f$
         */
        Any get_normal_gravity( const Any& phi ) {
            Any k = (b_axis * g_p ) / ( a_axis * g_e ) - 1.;
            Any brad = phi * DEG2RAD ;

            return g_e * ( (1 + k * sin(brad)*sin(brad) ) / sqrt( 1. - eccentricity* sin(brad)*sin(brad) ) );
        }

        Any get_U0 () {
            Any ee = eccentricity;
            return  (gm/(sqrt(ee)*a_axis))*atan(sqrt(ee)/(1.0-f))+(1.0/3.0)*omega*omega*a_axis*a_axis;
        }

        Any get_mean_radius( Any phi ) {
            Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
            /* B,L have to be in DEG format   */
            Any B = phi*(PI/180);                  /* converting to radians */
            Any m,n,r_nm;

            m = (a_axis*(1.0 - eccentricity))/pow( 1.0 - eccentricity * pow (sin(B),2.0), 3.0/2.0);
            n = a_axis/sqrt( 1.0 - eccentricity * pow (sin(B), 2.0));

            r_nm = sqrt (m*n);
            return r_nm;
        }

        Any get_n_radius ( Any phi ) {
            Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
            /* B,L have to be in DEG format   */
            Any B = phi*(PI/180);                  /* converting to radians */

            return a_axis/sqrt( 1.0 - eccentricity * pow (sin(B), 2.0));
        }

        Any get_m_radius ( Any phi ) {
            Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
            /* B,L have to be in DEG format   */
            Any B = phi*(PI/180);                  /* converting to radians */

            return (a_axis*(1.0 - eccentricity))/pow( 1.0 - eccentricity * pow (sin(B),2.0), 3.0/2.0);
        }

        Any get_normal_gravity ( const Any& phi, const Any& h ) {
            Any gamma_0 = this->get_normal_gravity(phi);

            Any g = gamma_0*(1.-(2.-(b_axis/a_axis) + pow( omega*a_axis,2.)*b_axis/ gm  \
                                 -2.*((a_axis-b_axis)/a_axis)*pow(sin(phi*DEG2RAD ),2.0) )      \
                            *h/a_axis+pow(h/a_axis,2.0)
                            );
            return g;
        }
    };

    template <typename Any>
    struct level_ellipsoid{     // rotational ellipsoid
        string name;            // probably name is needed :D
        string tide_system;     // zero, mean, tide free
        Any a_axis;             // sami-major axis
        Any b_axis;
        Any eccentricity;        // First numerical eccentricity squared!! := e^2
        Any f;                  // flattening
        Any gm;                 // geocentric gravity constant
        Any w0;                 // geopotential on geoid surface
        Any omega;              // speed of rotation
        Any j20;                // second stokes zonal coefficient [unitless]
                                //          \-> fully normalized
        Any g_e;                // normal gravity acceleration on eqautor [ms^-2]
        Any g_p;                // normal gravity acceleration on poles [m s^-2]
    };

    template <typename Any>
    struct triaxial_ell{
        string name;
        Any a_axis;
        Any b_axis;
        Any c_axis;
        Any ecc_x;   // eccentricity^2 := (c^2-a^2)/a^2
        Any ecc_e;   // e_e^2 = (b^2-a^2)/a^2
        Any gm;      // geocentric gravitational constant
        Any omega;   // speed of rotation
    };

    inline char* str2char( string str){
        char* ch = new char[str.size() + 1];
        copy(str.begin(), str.end(), ch);
        ch[str.size()] = '\0'; // terminating 0
        return ch;
    }

    template<typename Any>
    void show_ellipsoid ( struct ellipsoid<Any> ell){
        cout << "\nPrinting parameters of rotational ellipsoid: " << endl;
        cout << setw(15) << "Name :=        ";
        cout << ell.name << endl;
        cout << setw(15) << "a_axis :=      " << setprecision(12) << setw(18) << ell.a_axis << " [m]" << endl;
        cout << setw(15) << "b_axis :=      " << setprecision(12) << setw(18) << ell.b_axis << " [m]" << endl;
        cout << setw(15) << "e^2    :=      " << setprecision(12) << setw(18) << ell.eccentricity << " [unitless]" << endl;
        cout << setw(15) << "ggm    :=      " << setprecision(12) << setw(18) << ell.gm << " [m^3/s^2]" << endl;
        cout << setw(15) << "omgea  :=      " << setprecision(12) << setw(18) << ell.omega << " [rad/s]" << endl;
        cout << setw(15) << "j_20   :=      " << setprecision(12) << setw(18) << ell.j20 << " [unitless]" << endl;
        cout << setw(15) << "1/f    :=      " << setprecision(12) << setw(18) << 1.0/ell.f << " [unitless]" << endl;
        cout << setw(15) << "g_equat:=      " << setprecision(12) << setw(18) << ell.g_e << " [m/s^2]" << endl;
        cout << setw(15) << "g_poles:=      " << setprecision(12) << setw(18) << ell.g_p << " [m/s^2]" << endl;
    }

    template<typename Any>
    void show_ellipsoid ( struct level_ellipsoid<Any> ell){
        cout << "\nPrinting parameters of level ellipsoid: " << endl;
        cout << setw(15) << "Name :=        ";
        cout << setw(18) << ell.name << endl;
        cout << setw(15) << "Tide system := " << setw(18) << ell.tide_system << endl;
        cout << setw(15) << "a_axis :=      " << setprecision(12) << setw(18) << ell.a_axis << " [m]" << endl;
        cout << setw(15) << "b_axis :=      " << setprecision(12) << setw(18) << ell.b_axis << " [m]" << endl;
        cout << setw(15) << "e^2    :=      " << setprecision(12) << setw(18) << ell.eccentricity << " [unitless]" << endl;
        cout << setw(15) << "ggm    :=      " << setprecision(12) << setw(18) << ell.gm << " [m^3/s^2]" << endl;
        cout << setw(15) << "omgea  :=      " << setprecision(12) << setw(18) << ell.omega << " [rad/s]" << endl;
        cout << setw(15) << "j_20   :=      " << setprecision(12) << setw(18) << ell.j20 << " [unitless]" << endl;
        cout << setw(15) << "1/f    :=      " << setprecision(12) << setw(18) << 1.0/ell.f << " [unitless]" << endl;
        cout << setw(15) << "W_0    :=      " << setprecision(12) << setw(18) << ell.w0 << " [m^2/s^2]" << endl;
        cout << setw(15) << "g_equat:=      " << setprecision(12) << setw(18) << ell.g_e << " [m/s^2]" << endl;
        cout << setw(15) << "g_poles:=      " << setprecision(12) << setw(18) << ell.g_p << " [m/s^2]" << endl;
    };


    template<typename Any>
    void show_ellipsoid ( struct triaxial_ell<Any> ell){
        cout << "\nPrinting parameters of triaxial ellipsoid: " << endl;
        cout << setw(15) << "     Name := ";
        cout << ell.name << endl;
        cout << setw(15) << "a_axis :=      " << setprecision(12) << setw(18) << ell.a_axis << " [m]" << endl;
        cout << setw(15) << "b_axis :=      " << setprecision(12) << setw(18) << ell.b_axis << " [m]" << endl;
        cout << setw(15) << "c_axis :=      " << setprecision(12) << setw(18) << ell.c_axis << " [m]" << endl;
        cout << setw(15) << "e_e^2  :=      " << setprecision(12) << setw(18) << ell.ecc_e << " [unitless]" << endl;
        cout << setw(15) << "e_x^2  :=      " << setprecision(12) << setw(18) << ell.ecc_x << " [unitless]" << endl;
        cout << setw(15) << "ggm    :=      " << setprecision(12) << setw(18) << ell.gm << " [m^3/s^2]" << endl;
        cout << setw(15) << "omgea  :=      " << setprecision(12) << setw(18) << ell.omega << " [rad/s]" << endl;
    }
    
    template<typename Any>
    void print_ell2file (struct ellipsoid<Any> ell, const char* fname,  const char* rewrite){
        /* print parameters of rotation ellipsoid to file, adding to file
         * not deleting and creating the new one.
         * place - print at the end of file or at the beginnig7
         * {"end", "begin"}, end is default
         */
        ofstream file;
        string report = (string("reports/")+(fname)).c_str();

        if ( strcmp(rewrite, "a")== 0){
            file.open( report, ios::app);
        } else {
            file.open( report, ios::ate);
        }

        file << "\n============================================" << endl;
        file << "Printing parameters of rotational ellipsoid: " << endl;
        file << setw(15) << "Name :=        ";
        file << ell.name << endl;
        file << setw(15) << "a_axis :=      " << setprecision(12) << setw(18) << ell.a_axis << " [m]" << endl;
        file << setw(15) << "b_axis :=      " << setprecision(12) << setw(18) << ell.b_axis << " [m]" << endl;
        file << setw(15) << "e^2    :=      " << setprecision(12) << setw(18) << ell.eccentricity << " [unitless]" << endl;
        file << setw(15) << "ggm    :=      " << setprecision(12) << setw(18) << ell.gm << " [m^3/s^2]" << endl;
        file << setw(15) << "omgea  :=      " << setprecision(12) << setw(18) << ell.omega << " [rad/s]" << endl;
        file << setw(15) << "j_20   :=      " << setprecision(12) << setw(18) << ell.j20 << " [unitless]" << endl;
        file << setw(15) << "1/f    :=      " << setprecision(12) << setw(18) << 1.0/ell.f << " [unitless]" << endl;
        file << setw(15) << "g_equat:=      " << setprecision(12) << setw(18) << ell.g_e << " [m/s^2]" << endl;
        file << setw(15) << "g_poles:=      " << setprecision(12) << setw(18) << ell.g_p << " [m/s^2]" << endl;
        file << endl;
        file.close();
    }

    template<typename Any>
    void print_ell2file (struct ellipsoid<Any> ell, const char* fname){
        const char* rewrite = "r";
        print_ell2file(ell, fname, rewrite);
    }

    template<typename Any>
    void print_ell2file (struct level_ellipsoid<Any> ell, const char* fname, const char* rewrite){
        /* print parameters of rotation ellipsoid to file, adding to file
         * not deleting and creating the new one ("a").
         * rewrite "r", r is default
         */
        ofstream file;
        string report = (string("reports/")+(fname)).c_str();

        if ( strcmp(rewrite, "a")== 0){
            file.open( report, ios::app);
        } else {
            file.open( report, ios::ate);
        }

        file << "\n============================================" << endl;
        file << "Printing parameters of level ellipsoid: " << endl;
        file << setw(15) << "Name :=        ";
        file << setw(18) << ell.name << endl;
        file << setw(15) << "Tide system := " << setw(18) << ell.tide_system << endl;
        file << setw(15) << "a_axis :=      " << setprecision(12) << setw(18) << ell.a_axis << " [m]" << endl;
        file << setw(15) << "b_axis :=      " << setprecision(12) << setw(18) << ell.b_axis << " [m]" << endl;
        file << setw(15) << "e^2    :=      " << setprecision(12) << setw(18) << ell.eccentricity << " [unitless]" << endl;
        file << setw(15) << "ggm    :=      " << setprecision(12) << setw(18) << ell.gm << " [m^3/s^2]" << endl;
        file << setw(15) << "omgea  :=      " << setprecision(12) << setw(18) << ell.omega << " [rad/s]" << endl;
        file << setw(15) << "j_20   :=      " << setprecision(12) << setw(18) << ell.j20 << " [unitless]" << endl;
        file << setw(15) << "1/f    :=      " << setprecision(12) << setw(18) << 1.0/ell.f << " [unitless]" << endl;
        file << setw(15) << "W_0    :=      " << setprecision(12) << setw(18) << ell.w0 << " [m^2/s^2]" << endl;
        file << setw(15) << "g_equat:=      " << setprecision(12) << setw(18) << ell.g_e << " [m/s^2]" << endl;
        file << setw(15) << "g_poles:=      " << setprecision(12) << setw(18) << ell.g_p << " [m/s^2]" << endl;
        file << endl;
        file.close();
    }

    template<typename Any>
    void print_ell2file (struct level_ellipsoid<Any> ell, const char* fname){
        const char* rewrite = "r";
        print_ell2file(ell, fname, rewrite);
    }

    template<typename Any>
    void print_ell2file (struct triaxial_ell<Any> ell, const char* fname,  const char* rewrite){
        /* print parameters of rotation ellipsoid to file, adding to file
         * not deleting and creating the new one.
         * place - print at the end of file or at the beginnig7
         * {"end", "begin"}, end is default
         */
        ofstream file;
        string report = (string("reports/")+(fname)).c_str();

        if ( strcmp(rewrite, "a")== 0){
            file.open( report, ios::app);
        } else {
            file.open( report, ios::ate);
        }

        file << "\n============================================" << endl;
        file << "Printing parameters of triaxial ellipsoid: " << endl;
        file << setw(15) << "Name :=        ";
        file << ell.name << endl;
        file << setw(15) << "a_axis :=      " << setprecision(12) << setw(18) << ell.a_axis << " [m]" << endl;
        file << setw(15) << "b_axis :=      " << setprecision(12) << setw(18) << ell.b_axis << " [m]" << endl;
        file << setw(15) << "c_axis :=      " << setprecision(12) << setw(18) << ell.c_axis << " [m]" << endl;
        file << setw(15) << "e_e^2  :=      " << setprecision(12) << setw(18) << ell.ecc_e << " [unitless]" << endl;
        file << setw(15) << "e_x^2  :=      " << setprecision(12) << setw(18) << ell.ecc_x << " [unitless]" << endl;
        file << setw(15) << "ggm    :=      " << setprecision(12) << setw(18) << ell.gm << " [m^3/s^2]" << endl;
        file << setw(15) << "omgea  :=      " << setprecision(12) << setw(18) << ell.omega << " [rad/s]" << endl;
        file << endl;
        file.close();
    }

    template<typename Any>
    void print_ell2file (struct triaxial_ell<Any> ell, const char* fname){
        const char* rewrite = "r";
        print_ell2file(ell, fname, rewrite);
    }

    template<typename Any>
    ellipsoid<Any> level2normal_ell ( level_ellipsoid<Any> l_ell){
        struct ellipsoid <Any> ellips;
        ellips.name = l_ell.name;
        ellips.a_axis = l_ell.a_axis;
        ellips.b_axis = l_ell.b_axis;
        ellips.eccentricity =  l_ell.eccentricity;
        ellips.gm =   l_ell.gm;
        ellips.omega = l_ell.omega;
        ellips.j20 =  l_ell.j20;
        ellips.g_e =  l_ell.g_e;
        ellips.g_p =  l_ell.g_p;
        ellips.f =   l_ell.f;

        return ellips;
    }

    template<typename Any>
    ellipsoid<Any> get_wgs84(){
        struct geo_f::ellipsoid<Any> wgs84;
        wgs84.a_axis = WGS84_A;
        wgs84.b_axis = WGS84_B;
        wgs84.eccentricity = WGS84_E*WGS84_E;
        wgs84.gm = WGS84_GM;
        wgs84.omega = WGS84_WE;
        wgs84.g_p = WGS84_GP;
        wgs84.g_e = WGS84_GE;
        wgs84.f = WGS84_F;
        wgs84.j20 = WGS84_C20;

        wgs84.name = string("World geodetic system 1984");
        return wgs84;
    }

    /**
     * @return struct grs80 ellipsoid
     * @see ellipsoid
     */
    template<typename Any>
    ellipsoid<Any> get_grs80(){
        struct geo_f::ellipsoid<Any> grs80;
        grs80.a_axis = 6378137.0 ;// m
        grs80.b_axis = 6356752.3141 ; //m
        grs80.f = 1./298.25722210;
        grs80.eccentricity = 2.*grs80.f - grs80.f*grs80.f;
        //bessel.eccentricity = (bessel.a_axis*bessel.a_axis - bessel.b_axis *bessel.b_axis )/(bessel.a_axis*bessel.a_axis);
        grs80.gm = 3986005e+08;
        grs80.omega = 7292115e-11;
        grs80.g_p = 9.8321863685;
        grs80.g_e = 9.7803267715;
        grs80.j20 = -108263.0e-8 / sqrt(5.);

        grs80.name = string("GRS 1980");
        return grs80;
    }

    /**
     * @return struct Bessel ellipsoid
     * @see ellipsoid
     */
    template<typename Any>
    ellipsoid<Any> get_bessel(){
        struct geo_f::ellipsoid<Any> bessel;
        bessel.a_axis = 6377397.15500 ;// m
        bessel.b_axis = 6356078.9629 ; //m
        bessel.f = 1. / 299.1528128;
        bessel.eccentricity = 2.*bessel.f - bessel.f*bessel.f;
        //bessel.eccentricity = (bessel.a_axis*bessel.a_axis - bessel.b_axis *bessel.b_axis )/(bessel.a_axis*bessel.a_axis);
        bessel.gm = WGS84_GM;
        bessel.omega = WGS84_WE;
        bessel.g_p = WGS84_GP;
        bessel.g_e = WGS84_GE;
        bessel.j20 = WGS84_C20;
        bessel.name = string("Bessel ellipsoid");
        return bessel;
    }

    template <typename Any, typename Typ>
    Any meanradius (Any B, struct ellipsoid<Typ> ell){
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
        /* B,L have to be in DEG format   */
        B = B*(PI/180);                  /* converting to radians */
        Any m,n,r_nm,ee,a;

        a = static_cast<Any>( ell.a_axis);
        ee= static_cast<Any>( ell.eccentricity );

        m = (a*(1.0 - ee))/pow( 1.0 - ee * pow (sin(B),2.0), 3.0/2.0);
        n = a/sqrt( 1.0 - ee * pow (sin(B), 2.0));

        r_nm = sqrt (m*n);
        return r_nm;
    }

    template <typename Any, typename Typ>
    Any compute_normal_potential(Any u, Any w, struct ellipsoid<Typ> ell){
        /* Computation of normal potential U due Molodensky's theory
           W(x_M,y_M,z_M) = U(u_N,v_N.w_N)
           Molodenskij,Jeremejev, Jurkina 1960)
           input u,v in deg format, w unitless
        */
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
        u *= PI/180.0; /* converting to radians from DEG */
        Any q, P20, U1, U2, U3, U;
        Any a, ee, _omega_ , gm;
        a = static_cast<Any>( ell.a_axis);
        ee= static_cast<Any>( ell.eccentricity );
        _omega_ = static_cast<Any>( ell.omega );
        gm = static_cast<Any>( ell.gm );
        /* Computing normal potential of point N on plumb line
         *
           W(x_M,y_M,z_M) = U(u_N,v_n,w_N) =
           = \frac{GM}{ae} \left(
           \textrm{arccot} \sinh w_N + \frac{1}{3} e q \cdot
                                \left[
                                (3 \sinh^2 w_N + 1) \textrm{arccot} \sinh w_N
                               -3 \sinh w_N \right]
           \cdot \left[
           \frac{3-2e^2}{e^2} \arctan \frac{e}{\sqrt{1-e^2}} - 3 \frac{\sqrt{1-e^2}}{e}
                   \right]^{-1} \cdot P_2^{(0)}(\cos u_N)
                  + \frac{1}{3} qe^3 \cosh^2 w_N \cdot
                   [1 - P_2^{(0)}(\cos u_N)]                  \right)
           q = \frac{\omega^2  a^3}{GM}
           P_2^{(0)}(\cos u_N) = \frac{3 \cos^2 u_N -1}{2}
        */
        q = (pow( _omega_ ,(Any)  2.0) * pow( a,(Any) 3.0))/gm;
        P20 = (3.0*pow ( cos(u) ,(Any) 2.0 ) -1.0 )/2.0;
        U1 = (3.0*sinh(w)*sinh (w)+ 1.0 ) * (PI/2.0 - atan(sinh (w))) - 3.0*sinh(w);
        U2 = (((3.0-2.0*ee )/ ee ) * atan(sqrt(ee/(1.0-ee)))-3.0*sqrt((1.0 - ee)/ee));
        U3 =  1.0/3.0 *q*pow(sqrt(ee) ,(Any) 3.0 ) *cosh(w)*cosh(w) * (1.0-P20);
        U = gm/(sqrt(ee) * a) \
                * (PI/2.0 - atan(sinh(w)) + (1.0/3.0)* sqrt(ee) \
                * q *  (U1/U2) * P20 + U3);
        return U;
    }

    template<typename Any, typename Typ>
    Any compute_normal_potential(Any x, Any y , Any z, struct ellipsoid<Typ> ell){
        /*  Computation of normal potential U
            extracted from old piece of fortran source code
            input x,y,z in meters
        */
        Any r, d, a, c , ee , e , u1, u21, u2,shw ,ash, P20,m;
        a = static_cast<Any>( ell.a_axis );
        ee =static_cast<Any>(ell.eccentricity);
        e = sqrt(ee);
        c = sqrt(a*a - a*a*(1.-ee));

        r = sqrt( x*x + y*y + z*z);
        d = pow(r*r -c*c, 2.0) + 4.*c*c*z*z;
        shw = sqrt( (r*r-c*c+sqrt(d))/(2.*c*c)  ) ; // := sinh(w)
        ash = atan(1./shw);
        P20=1.5*( z*z/(c*c*shw*shw) ) - 0.5;
        m=(1.0+3.0*(a*a*(1.-ee))/(c*c))*atan(c/(a*sqrt(1.-ee)))-3.0*(a*sqrt(1.-ee))/(c);

        u1=((Any) ell.gm)*ash/(c) + ((Any) ell.omega*ell.omega)*(x*x+y*y)/2.0;
        u21=ash*(3.0*shw*shw+1.0)-3.0*shw;
        u2=((Any) ell.omega*ell.omega)*ell.a_axis*ell.a_axis*u21*P20/(3.0*m);
        return (u1+u2);
    }

    template <typename Any, typename Typ>
    Any compute_normal_gravity (Any u, Any w, struct ellipsoid<Typ> ell){
        /* \gamma(N) = \gamma(H_q,GM,W_0,\omega.J2)
         * derivative of function U(u,w) in the direction of the normal (opposite
         * direction as gradient )
         */
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
        u *= PI/180.0;
        Any h_u, h_v, h_w, P20, m, gamma, du_u, du_w;

        /* reading ellipsoid parameters */
        Any a, ee, _omega_ , _gm_;
        a = static_cast<Any>( ell.a_axis);
        ee= static_cast<Any>( ell.eccentricity );
        _omega_ = static_cast<Any>( ell.omega );
        _gm_ = static_cast<Any>( ell.gm );

        Any __e = sqrt(a*a-a*a*(1.0-ee)); // square of linear eccentricity

        h_u = a*sqrt(ee)*sqrt(cosh(w)*cosh(w)-sin(u)*sin(u));    // lame coeff
        h_w = h_u;                                               // lame coeff
        h_v = a*sqrt(ee)*sin(u)*cosh(w);                         // lame coeff

        P20 = (3.0*pow ( cos(u) ,(Any) 2.0 ) -1.0 )/2.0;         // legen. poly 20

        m = ((3. - 2.*ee)/ ee) \
        * atan(sqrt(ee / (1. - ee))) - 3.*sqrt((1.0 - ee) / ee );

        du_w = -1.0*_gm_/(__e*cosh(w)) + pow( _omega_*a,(Any) 2.0)/(3.0*m) \
             * ( 6*sinh(w)*cosh(w)*(PI/2.0 - atan(sinh(w))) - (6.0*sinh(w)*sinh(w)+4.0) \
             / cosh(w)) * P20 + 2.0/3.0*_omega_*_omega_*__e*__e*sinh(w)*cosh(w)*(1.0-P20);
        du_u = pow(_omega_*__e*cosh(w),(Any) 2.0)*sin(u)*cos(u) - pow( _omega_*a,(Any) 2.0)/(2.0*m) \
             * ( (3.0*sinh(w)*sinh (w)+ 1.0 ) * (PI/2.0 - atan(sinh (w))) - 3.0*sinh(w) ) \
             * sin(2*u);

        gamma = du_w/h_w + .5* pow( du_u/h_u ,(Any) 2.0 )/(du_w/h_w);
        return gamma;
    }

    template <typename Any, typename Typ >
    inline Any normal_ell_gravity (Any phi, struct ellipsoid<Typ> ell){
        // based on Somigliana equation:
        // normal gravity on ellipsoid surface
        // phi in deg format
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
        phi *= PI/180.0;
        Any k =  (Any)(sqrt(1.0 - ell.eccentricity)*ell.g_p)/ell.g_e -1.0;
        Any gamma = (Any) ell.g_e*(1.0 +k*sin(phi)*sin(phi));
        gamma /= sqrt(1.0 - ell.eccentricity*sin(phi)*sin(phi));
        return gamma;
    }

    template <typename Any, typename Typ>
    inline Any normal_gravity_hellmert( Any phi, Any h, struct ellipsoid<Typ> ell){
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
        phi *= PI/180.0;
        return 9.80616 - 0.025928*cos(2.*phi) + 6.9e-5*pow(cos(2.*phi),2.) -3.086e-6*h;
    }

    template<typename Any, typename Typ>
    Any compute_av_normal_gravity (Any phi, Any h, struct ellipsoid<Typ> ell ){
        /* compute average normal gravity
         * Formula of Heiskanen and Moritz 1967
         */
        Any deg2rad = (Any) 3.14159265358979323851280895940618620443274267017841339111328125L/180.0;
        Any gamma = normal_ell_gravity(phi, ell);
        Any a_axis, b_axis;
        a_axis = ell.a_axis;
        b_axis = a_axis*sqrt(1.-ell.eccentricity);

        Any g__ = gamma*(1.-(2.-(b_axis/a_axis) + pow( ell.omega*a_axis,2.)*b_axis/ ell.gm  \
                             -2.*((a_axis-b_axis)/a_axis)*pow(sin(phi*deg2rad ),2.0) )      \
                        *h/a_axis+pow(h/a_axis,2.0)
                        );
        return g__;
    }

    template <typename Any, typename Typ >
    vector<Any> xyz2blh (Any X, Any Y, Any Z, struct ellipsoid<Typ> ell){
        /* Converting cartesian perpendicular coordinates X,Y,Z
         * to geodetic latitude, longtitude, ellipsoidal height
         * relations between xyz <-> in function blh2xyz
         */
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
        Any B,L,H, Bi,Hi, NN, Ni, p, dB, dH;
        Any a, ee;

        a  = static_cast<Any>( ell.a_axis );
        ee = static_cast<Any>( ell.eccentricity );

        L = atan2(Y,X);
        if ( L < 0){
            L += 2.0*PI;
        }
        // Iterative method of computation
        p = sqrt(X*X + Y*Y);
        B = atan(Z/(p*(1.0- ee)));
        NN = a/sqrt(1.0 - ee * pow(sin(B), (Any) 2.0));
        H = p/cos(B) - NN;

        dB = 10; dH = 10;
        int i = 0;
        while ( ( dB > 1e-12 && dH > 1e-6 ) || i != 50){
            Bi = atan((Z*(NN+H))/(p*(H+NN*(1.0-ee))));
            Ni = a/sqrt(1.0 - ee * pow(sin(Bi),(Any) 2.0));
            Hi = p/cos(Bi) - Ni;

            dB = abs(B-Bi); dH = abs(H-Hi);
            B = Bi; H = Hi; NN = Ni;
            i++;
        }

        vector<Any> blh = { B*180.0/PI,  L*180.0/PI, H};
        return blh;
    }

    template <typename Any, typename Typ >
    vector<Any> blh2ruv (Any b, Any l, Any h, struct ellipsoid<Typ> ell){
        /* Converting geodetic coordinates B, L, H
         * to 3D polar coordinates r, u, v
         * r - geocentric length
         * u - polar angle measured from plane defined by axis x,y
         * v - identical with lambda
         */
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
        Any x,y,z, n;
        Any r,u,v;

        v = l;
        b *= PI/180.0; /* converting from deg to radians */
        l *= PI/180.0; /* converting from deg to radians */

        n = ( (Any) ell.a_axis )/sqrt(1.0 -( (Any) ell.eccentricity) * pow(sin(b), (Any) 2.0));

        x = (n+h)*cos(b) * cos(l); // X - coordinate
        y = (n+h)*cos(b) * sin(l); // Y - coordinate
        z = (n*(1.0-( (Any) ell.eccentricity)) + h)*sin(b); // Z - coordinate

        r = sqrt(x*x+y*y+z*z);
        u = (RAD2DEG)*atan(z/sqrt(x*x+y*y));

        vector<Any> ruv = {r,u,v};
        return ruv;
    }

    template <typename Any, typename Typ >
    vector<Any> blh2xyz (Any b, Any l, Any h, struct ellipsoid<Typ> ell){
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);

        b *= PI/180.0; /* converting from deg to radians */
        l *= PI/180.0; /* converting from deg to radians */

        Any _n_ = ( (Any) ell.a_axis )/sqrt(1.0 -( (Any) ell.eccentricity) * pow(sin(b), (Any) 2.0));

        vector<Any> xyz = {(_n_+h)*cos(b) * cos(l),
                           (_n_+h)*cos(b) * sin(l),
                           (_n_*(1.0-( (Any) ell.eccentricity)) + h)*sin(b) };


        return xyz;
    }

    template <typename Any, typename Typ >
    vector<Any> blh2uvw (Any __b, Any __l, Any __h, struct ellipsoid<Typ> ell){
        vector<Any> uvw,xyz; /* output*/
        xyz = blh2xyz(__b,__l,__h,ell);
        uvw = xyz2uvw(xyz[0],xyz[1],xyz[2],ell);
        return uvw;
    }

    template <typename Any, typename Typ >
    vector<Any> xyz2uvw (Any X, Any Y, Any Z, struct ellipsoid<Typ> ell){
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
        /* curvilinear coordinates u,v,w
         * X = a e \sin u \cos v \cosh w
         * Y = a e \sin u \sin v \cosh w
         * Z = a e \cos u \sinh w
         */
        Any a, ee, p,r,e,u,v,w,dd1,dd2,dd;
        a = static_cast<Any>( ell.a_axis );
        ee = static_cast<Any>(ell.eccentricity );
        e = sqrt(ee);
        v = atan2(Y, X);
        p = sqrt(X*X + Y*Y);
        r = sqrt(X*X + Y*Y + Z*Z);

        dd1 =  1.0/(2.0*pow(a*e,(Any) 2.0)) \
                *(  pow(a*e,(Any) 2.0) + r*r + sqrt(pow(pow(a*e,(Any) 2.0) + r*r,(Any) 2.0) \
                - 4.0*pow(a*e,(Any) 2.0)*p*p));

        dd2 = 1.0/(2.0*pow(a*e,(Any) 2.0)) \
                *(  pow(a*e,(Any) 2.0) + r*r - sqrt(pow(pow(a*e,(Any) 2.0) + r*r,(Any) 2.0) \
                - 4.0*pow(a*e,(Any) 2.0)*p*p));

        if ( (abs(dd1) > 1.0) && (dd2 == 1.0) ){
            dd = dd2 - 1.0e-10;
        } else if ( (abs(dd1) > 1.0) && (dd2 != 1.0) ){
            dd = dd2;
        } else if( (dd1 < 1.0) && (dd2 < 0.0) ) {
            dd = dd1;
        } else {
            dd = dd1;
        }
//        cout << "dd:=" << dd << endl;
        Any f = Z/(a*e*sqrt(1.0 - dd));
        if (Z > 0.0){
            w = asinh(f);
            u = asin(sqrt(dd));
        } else if ( Z < 0.0){
            u = PI - asin(sqrt(dd));
            w = asinh(-1.0*f);
        } else {
            u = asin(sqrt(dd-1.0e-10));
            w = acosh( 1.0/e);
        }

        u /= PI/180.0; v /= PI/180.0;

        vector<Any> uvw = { u , v , w};
        return uvw; // in vector<> format [u ,v ,w]
    }

    template <typename Any, typename Typ>
    vector<Any> xyz2ruv (Any x, Any y, Any z, struct ellipsoid<Typ> ell){
        /* returns : geocentric length, latitude (u) and longtitude in deg format */
        Any PI =static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);

        Any r, u,v;

        r = sqrt(x*x+y*y+z*z);
        u = (180./PI)* asin( z/r );
        v = (180./PI) * atan2(y,x);

        v = ( v < 0 ) ? v+360. : v;


        vector<Any> ruv= {r,u,v};
        return ruv;
    }

    template <typename Any, typename Typ>
    vector<Any> xyz2ruv_2 (Any x, Any y, Any z, struct ellipsoid<Typ> ell){
        /* returns : geocentric length, co-latitude (u) and longtitude in deg format */
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
        Any r,u,v;

        r = sqrt(x*x+y*y+z*z);
        u = (180./PI)* acos( z/r );
        v = (180./PI) * atan2(y,x);

        v = ( v < 0 ) ? v+360. : v;

        vector<Any> ruv= {r,u,v};
        return ruv;
    }

    template <typename Any, typename Typ >
    vector<Any> uvw2xyz (Any u, Any v, Any w, struct ellipsoid<Typ> ell){
        /* curvilinear coordinates u,v,w
         * X = a e  \sin u \cos v \cosh w
         * Y = a e \sin u \sin v \cosh w
         * Z = a e \cos u \sinh w */
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
        Any a, ee, e;
         // xyz.push_back(
        a  = static_cast<Any>(ell.a_axis);
        ee = static_cast<Any>(ell.eccentricity);

        e = sqrt(ee);
        vector<Any> xyz = {a*e*sin(u*PI/180.0)*cos(v*PI/180.0)*cosh(w),
                           a*e*sin(u*PI/180.0)*sin(v*PI/180.0)*cosh(w),
                           a*e*cos(u*PI/180.0)*sinh(w) };
        return xyz;
    }

    template <typename Any, typename Typ>
    vector<Any> uvw2blh (Any u, Any v, Any w, struct ellipsoid<Typ> ell){
        vector<Any> temp_xyz, blh;
        temp_xyz = uvw2xyz(u,v,w,ell);
        blh = xyz2blh(temp_xyz[0],temp_xyz[1],temp_xyz[2],ell);
        return blh;
    }

    template <typename Any>
    /**
     * @brief compute_level_ell
     * @param a0 - a major axis of refer. ell of geo model
     * @param gm - geocentric gravitational constant
     * @param w0 - geopotential value on geoid
     * @param omega - angular speed of the Earth's rotation
     * @param j_20 - second stokes zonal coefficient fully normalized
     * @param name - name of the ellipsoid
     * @param tide_system - needed because second zonal stokec coefficient
     * @return
     */
    level_ellipsoid<Any> compute_level_ell (Any a0, Any gm, Any w0, Any omega, \
                                            Any j_20, string name, const char* tide_system) {
        //for numeric errors less then 1e-7  m 6 iterations are needed
        Any a = gm/w0;
        // Any a0 = 6378136.3; -> for egm2008
        Any  alpha0 = 1.0/298.25231; /* IAGSC3 1995 */
        Any  p, pom, ee /* ee = e^2 */,alpha, da ,dalpha,q,a1, e_prime /*second numerical eccentricity */ ;

        j_20 = j_20*sqrt(5.0);

        q = pow(omega, 2 ) * pow(a0 , 3 )/ gm;
        ee =( 2.0*alpha0  - pow(alpha0,(Any) 2.0) ) ;

        int iter = 1;
        do{
            alpha = .5*alpha0*alpha0 - 1.5* (a0*a0/(a*a))*j_20 + 2.0/15.0*q*(a*a*a/(a0*a0*a0)) \
                  * pow(ee,3.0/2.0)/((3.0-2.0*ee)/ee * atan(sqrt (ee/(1.0-ee))) \
                                     -3.0*sqrt((1.0-ee)/ee));
            ee =( 2.0*alpha  - pow(alpha,(Any) 2.0) ) ;
            a1 = (gm/w0) * ((1.0/sqrt(ee)) * atan(sqrt(ee)/(1.0-alpha)) \
                +(1.0/3.0)*pow(omega,(Any) 2.0)*pow(a,(Any) 3.0)/gm);

            da = a-a1; dalpha = alpha-alpha0;
            alpha0 = alpha; a = a1;
            iter += 1;
            if ( iter == 20 ){
                cout << "Ups,something is wrong with computing level ellipsoid. ";
                cout << "Maximum number of allowed iterations exceeded !!\nCheck your inputs !!" << endl;
                break;
            }
        } while ( (abs(da) >= 1.e-6) ) ;

        ee =( 2.0*alpha0  - pow(alpha0,(Any) 2.0) ) ;

        /* Checking computations - dont delete */ /*
        Any w0__, w0____;
        w0__ = (gm/(sqrt(ee)*a))*atan(sqrt(ee)/(1.0-alpha))+(1.0/3.0)*omega*omega*a*a;
        w0____ = gm/a*(1.0 + alpha/3.0+.4*(alpha*alpha)+ 46.0/105.0*pow(alpha,3.0)+ \
                         q*pow(a/a0,3.0)*(1.0/3.0-2.0/15.0*alpha-3.0/35.0*(alpha*alpha)) \
                        +j_20*alpha*(a0*a0/(a*a))* \
                            (.4+27.0*alpha/35.0)  );

        cout << setprecision(12) << setw(12) << "w0_input:= " << _w0_ << endl;
        cout << setprecision(12) << setw(12) << "w0_2:= " << w0__ << endl;
        cout << setprecision(12) << setw(12) << "w0_2':= " <<w0____  << endl; */
        /* Control computation based on "Pizzeti theorem" */

        /* compute normal gravity - have new formulas, check it later */
        /*
        struct ellipsoid<Any> ell;
        ell.a_axis = a;
        ell.b_axis = (Any) a*sqrt(1.0-ee);
        ell.eccentricity = ee;
        ell.omega = omega;
        ell.gm = gm;

        vector<Any> uvw_eq, uvw_pol;
        Any g_e, g_p;
        uvw_eq = blh2uvw(0.0,0.0,0.0,ell);
        uvw_pol = blh2uvw(90.0,0.0,0.0,ell);
        */
        /*
        g_e = -1.0*compute_normal_gravity(uvw_eq[0], uvw_eq[2], ell);
        g_p = -1.0*compute_normal_gravity(uvw_pol[0], uvw_pol[2], ell); */

        /** Jurkina's modification of Pizzetti's theorem */
        Any g_e, g_p;
        e_prime = sqrt(ee/(1.0-ee));
        q = (3.0/pow(e_prime,2.0) +1.0)*atan(e_prime)-3./e_prime; // CHECK IT
        g_p = gm/(a*a*(sqrt(1.0-ee)))-(2./3.)*omega*omega*a*(1.0-e_prime/q*(1.0-1./e_prime*atan(e_prime)));
        g_e = gm/(a*a)-(2./3.)*omega*omega*a*(sqrt(1.-ee)+(4./3.)*sqrt(ee)/q*(1.0-1./e_prime*atan(e_prime)));

        /* END compute normal gravity */
        struct level_ellipsoid<Any> level_ell;
        level_ell.name  = name;
        level_ell.tide_system = tide_system;
        level_ell.a_axis = static_cast<Any>( a );
        level_ell.f = static_cast<Any>( alpha );
        level_ell.b_axis = static_cast<Any>( a*sqrt(1.0-ee) );
        level_ell.gm = gm;
        level_ell.omega = omega;
        level_ell.j20 = j_20/sqrt(5.0); // fully norm.
        level_ell.w0 = w0;
        level_ell.eccentricity = ee;
        level_ell.g_e = g_e;
        level_ell.g_p = g_p;

        return level_ell;
    }

    template <typename Any>
    level_ellipsoid<Any> compute_level_ell (Any a0, Any _gm_, Any _w0_, Any omega, \
                                            Any j_20, string name){
        struct level_ellipsoid<Any> ell;
        const char* ts = str2char("Not specified") ;
        ell = (a0,_gm_,_w0_,omega,j_20,name,ts);
        return ell;
    }

    template <typename Any>
    level_ellipsoid<Any> compute_level_ell (Any a0, Any _gm_, Any _w0_, Any omega, \
                                            Any j_20){
        /* with no name assigned */
        level_ellipsoid<Any> level_ell;
        string name__ = "Nameless";
        const char* ts = str2char("Not specified") ;
        level_ell = compute_level_ell(a0,_gm_,_w0_,omega,j_20,name__,ts);
        return level_ell;
    }

    template <typename Any>
    Any tide_system_transf (Any c_20, Any radius, Any k,const char* from,const char* to ){
        /* tide sytems: "zero tide", "mean tide, "tide free"
           c_20 is input from ggm
           k - love number (0.3 for EGM96, .29525 for EGM2008
           radius - reference radius of the gravity field model
           !! simplified version of algorithm
           based on mean value theorem and WGS84 ellipsoid

           mean gravity = 9.806224173 from wgs84 ellipsoid
           mean radius  = 6.36754098217714754616796613117603325027137630257097929e+06
           a_axis = 6378137.000
           GM = 3986005.0e+8
        */
        Any _c20_, m; // m= \frac{-1}{r \sqrt{4 \pi} \cdot .}
        Any PI = static_cast<Any>( 3.14159265358979323851280895940618620443274267017841339111328125L);
        //m = (Any) -1.382520964080156800e-08;
        //m = (Any) -1.390366816361558784e-08;
        m = static_cast<Any>(1.0/(radius*sqrt(4.*PI))*(-0.31455));

        if ( ((strcmp(from, "zero tide") == 0) || (strcmp(from, "zero_tide") == 0)) &&
             ((strcmp(to, "mean tide")  == 0 ) || (strcmp(to, "mean_tide")  == 0 )) ) {
            _c20_ = c_20 + m;
        } else if ( ((strcmp(from,"zero tide") == 0) || (strcmp(from,"zero_tide") == 0)) &&
                    ((strcmp(to, "tide free")  == 0) || (strcmp(to,"tide_free") == 0)) ){
            _c20_ = c_20 -k*m;
        } else if ( ((strcmp(from,"mean tide") == 0) || (strcmp(from,"mean_tide") == 0)) &&
                    ((strcmp(to, "zero tide")  == 0) || (strcmp(to,"zero_tide") == 0)) ){
            _c20_ = c_20 - m;
        } else if ( ((strcmp(from,"mean tide") == 0) || (strcmp(from,"mean_tide") == 0)) &&
                    ((strcmp(to, "tide free")  == 0) || (strcmp(to,"tide_free") == 0)) ){
            _c20_ = c_20 -( (Any) 1.0L + k) * m ;
        } else if ( ((strcmp(from,"tide free") == 0) || (strcmp(from,"tide_free") == 0)) &&
                    ((strcmp(to, "zero tide")  == 0) || (strcmp(to,"zero_tide") == 0)) ){
            _c20_ = c_20 +k*m;
        } else if ( ((strcmp(from,"tide free") == 0) || (strcmp(from,"tide_free") == 0)) &&
                    ((strcmp(to, "mean tide")  == 0) || (strcmp(to,"mean_tide") == 0)) ){
            _c20_ = c_20  - ( (Any) 1.0L + k )*m;
        } else {
            return c_20;
        }
        return _c20_;
    }

    /**
     * @brief height_tide_transform - returns the transformed height from one system to another
     * @param h_n - height value
     * @param udeg - spherical latitude
     * @param from - from tide system
     * @param to - to tide system
     * @param k - Love number
     * @param h - Love number
     * @return transformed height
     */
    double height_tide_transform (const double &h_n ,
                                  const double &udeg,
                                   const string& from,
                                   const string& to,
                                   const double& k ,
                                   const double& h );

    template <typename Any>
    inline void level_ell_tstransform ( struct level_ellipsoid<Any>& ell, Any love,const char* tots){
        /* Transforming the fully normalized C20 (J20) Stoke's coeff of the level ell.
         * It's from level ellipsoid's tide system to specified tide system. The love number (love
         * variable) is needed for correct ts transformation. Input in 2 possible forms
         * for every tide system
         * "zero tide" or "zero_tide", "mean tide" or "mean_tide", "tide free" or "tide_free"
         */
         Any c20_tr, c20_ell;

         c20_ell = ell.j20;
         const char* fromts = (ell.tide_system).c_str();
         c20_tr = tide_system_transf(c20_ell, ell.a_axis, love, fromts, tots);

         ell.j20 = c20_tr;
         ell.tide_system = tots;

         cout << "Transformation of the a_axis, etc. not finished yet" << endl;
    }

    template <typename Any, typename Tany>
    vector<Any> blh2xyz_triaxial (Any b, Any l, Any h,
                                  struct triaxial_ell<Tany> ellipsoid){
        /* x = (v+h)*\cos b \cos l
           y = (v (1-e_e^2) + h) \cos b \sin l
           z = (v (1 - e_x^2) +h) \sin b

           v = a/sqrt( 1 -e_x^2 \sin^2 b -e_e^2 \cos^2 b \sin^2 l)

           b,l in deg format
           h in metres
        */
        Any PI = (Any) 3.14159265358979323851280895940618620443274267017841339111328125L;

        b *= PI/180.0; /* converting to radians */
        l *= PI/180.0; /* converting to radians */

        Any v, ee_x, ee_e, a_ax;
        a_ax = static_cast<Any>(ellipsoid.a_axis);
        ee_x = static_cast<Any>(ellipsoid.ecc_x);
        ee_e = static_cast<Any>( ellipsoid.ecc_e);

        v = a_ax/sqrt(1.0 - ee_x*sin(b)*sin(b) - ee_e*cos(b)*cos(b)*sin(l)*sin(l));

        vector<Any> xyz = {(v+h)*cos(b)*cos(l),// X - coordinate
                           (v*(1.0-ee_e)+h)*cos(b)*sin(l),  // Y - coordinate
                           (v*(1.0-ee_x)+h)*sin(b) }; // Z - coordinate
        return xyz;
    }

    template <typename Any>
    vector<double> xyz2blh_triaxial (double x, double y, double z,
                                  struct triaxial_ell<Any> ellipsoid, string method){
//        /* Cartesian to geodetic coordinates conversion on a triaxial ellipsoid
//           Marcin Ligas                         Journal of Geodesy(2012) 86:249-256 */

//        /* ARMADILLO IS NEEDED !!!!! */
//        /* method = { case1, case2, case3};, case1 == default */
//        /* FIRST := finding the point on the ellipsoid */
        using namespace arma;

        Any PI = (Any) 3.14159265358979323851280895940618620443274267017841339111328125L;
        double __e, __f, __g, a_ax, b_ax, c_ax, b, l, h, z_ei, ee_e, ee_x;
        int noi; noi=0;                     /* noi == number of iterations */
        double dd, r, x_ei, y_ei;    //, y_eii, z_ei, z_eii;

        mat jac = zeros<mat>(3,3);
        colvec fx = zeros<mat>(3,1);
        colvec delta;

        a_ax = static_cast<double>( ellipsoid.a_axis );
        b_ax = static_cast<double>( ellipsoid.b_axis );
        c_ax = static_cast<double>( ellipsoid.c_axis );
        ee_x = static_cast<double>( ellipsoid.ecc_x );
        ee_e = static_cast<double>( ellipsoid.ecc_e );

        r = sqrt(x*x+y*y+z*z);

        __e = 1.0/(a_ax*a_ax);
        __f = 1.0/(b_ax*b_ax);
        __g = 1.0/(c_ax*c_ax);

        x_ei = a_ax*x/r;
        y_ei = b_ax*y/r;
        z_ei = c_ax*z/r;

        do{
            if ( method == "case2"){
                jac << 2.0*__e*x_ei << 2*__f*y_ei << 2.0*__g*z_ei << endr
                    << __f*y_ei - (y_ei-y)*__e
                        << (x_ei -x)*__f - __e*x_ei
                            << 0.0 << endr
                    << 0.0
                        << __g*z_ei - (z_ei -z)*__f
                            << (y_ei-y)*__g-__f*y_ei << endr;

                fx << __e*x_ei*x_ei + __f*y_ei*y_ei + __g*z_ei*z_ei - 1.0 << endr
                   << (x_ei -x)*__f*y_ei - (y_ei-y)*__e*x_ei            << endr
                   << (y_ei -y)*__g*z_ei - (z_ei-z)*__f*y_ei            << endr;

            } else if ( method == "case3" ){
                jac << 2.0*__e*x_ei << 2.0*__f*y_ei << 2.0*__g*z_ei << endr
                    << __g*z_ei-(z_ei-z)*__e
                        << 0.0
                            << (x_ei -x)*__g-__e*x_ei << endr
                    << 0.0
                        << __g*z_ei-(z_ei-z)*__f
                            << (y_ei - y)*__g -__f*y_ei << endr;

                fx << __e*x_ei*x_ei + __f*y_ei*y_ei + __g*z_ei*z_ei - 1.0 << endr
                   << (x_ei -x)*__g*z_ei - (z_ei-z)*__e*x_ei            << endr
                   << (y_ei -y)*__g*z_ei - (z_ei-z)*__f*y_ei            << endr;

            } else {
                jac << 2.0*__e*x_ei << 2.0*__f*y_ei << 2.0*__g*z_ei << endr
                    << __f*y_ei-(y_ei-y)*__e
                        << (x_ei-x)*__f -__e*x_ei
                            << 0.0 << endr
                    << __g*z_ei-(z_ei-z)*__e
                        << 0.0
                            << (x_ei -x)*__g-__e*x_ei << endr;

                fx << __e*x_ei*x_ei + __f*y_ei*y_ei + __g*z_ei*z_ei - 1.0 << endr
                   << (x_ei -x)*__f*y_ei - (y_ei-y)*__e*x_ei            << endr
                   << (x_ei -x)*__g*z_ei - (z_ei-z)*__e*x_ei            << endr;
            }

            delta = -jac*fx;

            x_ei += delta(0);
            y_ei += delta(1);
            z_ei += delta(2);

            dd = sqrt( arma::as_scalar(delta.t()*delta ));

            jac.fill(0);
            fx.fill(0);

            noi++;
        } while ( dd >= .0005 /*[m]*/ || noi != 50);

        h = sqrt( pow(x_ei-x,2.0) + pow(y_ei-y,2.0) + pow(z_ei-z,2.0) ); // height above ellipsoid
        b = atan( ( (1.0-ee_e)/(1.0-ee_x) )*z_ei/sqrt( pow(1.0-ee_e,2.0)*x_ei*x_ei +y_ei*y_ei ) ); // b - rad
        l = atan2( y_ei, (1-ee_e)*x_ei);

        l = ( l < 0 ) ? l += 180 : l;

        b *= 180.0/PI; /* rad2deg */
        l *= 180.0/PI;

        vector<double> blh = {b,l,h};
        return blh;
    }

    template <typename Any>
    vector<double> xyz2blh_triaxial (double x, double y, double z,
                                     struct triaxial_ell<Any> ell){
        string method = "case1";
        vector<double> blh;
        blh = geo_f::xyz2blh_triaxial(x,y,z, ell, method);
        return blh;
    }

    template<typename Any, typename Typ>
    Any convert_phi2u (Any phi, Any h_ell, struct ellipsoid<Any> ell){

        Any phi_rad = phi*DEG2RAD;
        Any n = ell.a_axis/sqrt(1.0 - ell.eccentricity*sin(phi_rad)*sin(phi_rad));
        return ( RAD2DEG*atan( (n*(1.-ell.eccentricity)+h_ell)/(n+h_ell)*tan(phi_rad)) );
    }


    /**
     * @brief sokes_kernel - return value of the Stoke's Kernel given by equation
     * \f$  \frac{1}{\sin( \frac{\psi}{2} )} - 6\sin( \frac{\psi}{2} ) + 1 - 5 \cos(\psi) - 3\cos(\psi) * \ln(\sin( \frac{\psi}{2} ) +\sin^2( \frac{\psi}{2} ))  \f$
     * @param psi - geocentric angle/spherical distance in radians
     * @return value of the Stokes Kernel
     */
    template<typename T>
    T stokes_kernel(T psi ) {
        T s = sin(psi/2);
        return 1/s - 4 - 6*s + 10*s*s - (3 - 6*s*s) * log(s+s*s);
        //return ( 1./sin(psi/2.) - 6.*sin(psi/2.0) + 1. - 5.*cos(psi) - 3.*cos(psi) * log(sin(psi/2.) + pow( sin(psi/2.),2. )) );
    } // 1./sin(psi/2.)


    template <typename eT = double>
    /**
     * @brief stokes_kernel_cyl - return the value os the Stoke's kernel in cylidrical coordinates
     * \f$ F(\psi) = \sin ( \psi ) S (\psi ) \f$
     * \f$ F(\psi) = 2 \cos \frac{\psi}{2} - 6 \sin  \frac{\psi}{2} \sin \psi  + \sin \psi  - \frac{5}{2} \sin 2 \psi - \frac{3}{2} \ln \left( \sin  \frac{\psi}{2} + \sin^2  \frac{\psi}{2}  \right )^{\sin 2 \psi} \f$
     * @param psi - spherical distance in radians ( geocentric angle \f$ \psi \in [0,\pi] \f$ )
     * @return
     */
    eT stokes_kernel_cyl ( eT psi ) {
        eT s = sin(psi/2.);
        eT s2 = sin(2.*psi);
        return 2.*cos(psi/2.) -6.*sin(psi)*s + sin(psi)-2.5*s2 -1.5*log( pow( s+s*s ,s2 ) );
    }

    template<typename eT = double>
    /**
     * @brief hotine_kernel
     * \f$ H(\psi) = \frac{1}{\sin\frac{\psi}{2}} - \ln \left( 1 + \frac{1}{\sin\frac{\psi}{2}}  \right) \f$
     * @param psi - spherical distance in radians ( geocentric angle \f$ \psi \in [0,\pi] \f$ )
     * @return
     */
    eT hotine_kernel ( eT psi ) {
        eT s = 1./sin(psi/2.);

        return s - log(1. + s);
    }

    template <typename eT = double>
    /**
     * @brief hotine_kernel_cyl
     * \f$ G(\psi) = H( \psi) \cdot \sin \psi = 2 \cos \frac{\psi}{2} - \sin \psi \ln \left( 1 + \frac{1}{\sin\frac{\psi}{2}}  \right) \f$
     * @param psi- spherical distance in radians ( geocentric angle \f$ \psi \in [0,\pi] \f$ )
     * @return value of the hotine's kernel in cylyndrical coordinates, if the number is too small returns the limit value 0+
     */
    eT hotine_kernel_cyl ( eT psi ) {
        eT resutl =  2.*cos(psi/2) - sin(psi) * log( 1. + sin(psi/2.));

        if ( isnan(resutl) || isinf(resutl) ) {
            return 2.0;
        } else {
            return resutl;
        }
    }

    template<typename eT=double>
    /**
     * @brief int_stokes_kernel_cyl , \f$ \int \sin \psi S( \psi) \mathrm{d} \psi \f$, the reuslt is
         * \f$ G(\psi) = \frac{1}{8} \left( -8 \cos \psi + \cos 2 \psi \left[ 6 \ln \left( \sin \frac{\psi}{2} \left( 1 + \sin \frac{\psi}{2} \right ) \right ) +7 \right ] - 4 \sin \frac{\psi}{2} + 12 \sin \frac{3\psi}{2} -6\ln \left( \sin \frac{\psi}{2} \left( 1 + \sin \frac{\psi}{2} \right ) \right ) +10 \right) \f$
         * for the \psi = 0 \f$  the \f$G(\psi=0) \f$ can be evaluated from \f \lim_{x \rightarrow0+} G(\psi) = \frac{9}{8} \f$
     * @param psi - spherical distance in radians ( geocentric angle \f$ \psi \in [0,\pi] \f$ )
     * @return
     */
    eT int_stokes_kernel_cyl ( eT psi )
    {
        eT s = sin(psi/2);
        eT result = (-8.*cos(psi) + cos(2.*psi)*(6.*log(s+s*s) +7.) -4.*s+ 12.*sin(1.5*psi) - 6.*log(s+s*s) +10. ) / 8.;

        if ( abs(psi) < __EPS__ ) {
            return 9./8.;
        } else {
            return result;
        }
    }

    template <typename eT = double>
    /**
     * @brief int_hotine_kernel_cyl - the value of the integral of the hotine kernel in cylidrical coordinates
     * \f$ U(\psi) = \int \sin(\psi) \cdot H(\psi) \mathrm{d} \psi = 2 \sin \frac{\psi}{2} + \ln \left( \sin \frac{\psi}{2} \left( 1 +\sin \frac{\psi}{2} \right ) \left[\frac{1+\sin \frac{\psi}{2}}{\sin \frac{\psi}{2}} \right ]^{\cos \psi} \right) \f$
     * @param psi
     * @return the value of the \f$ U(\psi) \f$ function, returns 0 if solution is singular
     */
    eT int_hotine_kernel_cyl ( eT psi ) {
        eT s = sin(psi/2.);

        eT result = 2.*s + log (s * (s+1.) * pow( (1.0+s)/s , cos(psi)));

        if ( isnan(result) || isinf(result) ) {
            return 0.0;
        } else {
            return result;
        }
    }

    /**
     * @brief compute_stokes_mat - computes the matrix, composed of the stokes kernel values in grid. Let the Stokes kernel
     * function be function of gridded coordinate differences. Original equation is in the function @see stokes_kernel .
     * Where the argument \f$ \psi \f$ can be expressed from following equation:
     * \f$ \sin^2 \frac{\psi}{2} = \sin^2 \left( \frac{\Delta \phi_{Q,P}}{2} \right) + \sin^2 \left( \frac{\Delta \lambda_{Q,P}}{2} \right) \cos \phi_Q \cos \phi_P \f$ ,
     * where the \f$ (\phi_P, \lambda_P ) \f$ are the spherical coordinates of the computational point P and \f$ (\phi_Q, \lambda_Q )\f$
     * are the data point Q coordinates. Then the Stokes function can be rewritten as a function of coordinate differences \f$ S(\psi) \rightarrow S( \phi_P, Delta \phi, \Delta \lambda) \f$
     * The matrix that is returned in following format. Because the \f$ \phi_P \f$ is common for all elements it will be omitted in notation.
     * \f$
       \Sigma =
       \left[
       \begin{array}{ccccc}
       \ddots & \vdots & \vdots & \vdots & \ldots
       \\\\
       \ldots & S ( 2 \Delta \phi , -  \Delta \lambda ) & S ( 2 \Delta \phi , 0 \cdot \Delta \lambda ) & S ( 2 \Delta \phi ,  \Delta \lambda )  & \ldots
       \\\\
       \ldots  & S ( \Delta \phi , -  \Delta \lambda ) & S ( \Delta \phi , 0 \cdot \Delta \lambda ) & S (  \Delta \phi ,  \Delta \lambda )  & \ldots
       \\\\
       \ldots  & S ( 0 \cdot \Delta \phi, -  \Delta \lambda ) & S ( 0 \cdot \Delta \phi , 0 \cdot \Delta \lambda ) & S ( 0 \cdot \Delta \phi ,  \Delta \lambda )  & \ldots
       \\\\
       \ldots  & S (-\Delta \phi , -  \Delta \lambda ) & S (- \Delta \phi , 0 \cdot \Delta \lambda ) & S ( -\Delta \phi ,  \Delta \lambda )  & \ldots
       \\\\
       \ldots  & S (-2\Delta \phi , -  \Delta \lambda ) & S (-2 \Delta \phi , 0 \cdot \Delta \lambda ) & S ( -2\Delta \phi ,  \Delta \lambda )  & \ldots
       \\\\
       \ldots & \vdots & \vdots & \vdots & \ddots \\\\
       \end{array}
       \right]
       \f$
     * where for the same point, the expression \f$  \frac{ 1 }{ \sqrt{ \pi \Delta \phi \Delta \lambda} } \f$ is returned to avoid singularity problems. In the end
     * the size of the matrix \f$ \Sigma \f$ is expanded be 50 percent to each direction with zero elements to avoid
     * cyclic convolution (e.g. zero padding).
     * @param phi0 - the latitude value of the point P in DEG
     * @param lam0 - the longitude value of the point P in DEG
     * @param dlat - the difference between two meridians in DEG
     * @param dlon - the difference between two parallels in DEG
     * @param sphererad - radius of the reference sphere
     * @param intrad - integration radius
     * @param drow - initial number of the rows from the middle point to one of the edges
     * @param dcol - initial number of the columns from the middle point to one of the edges
     * for false statment the zero padding is in both directions.
     * @return - the matrix \f$ \Sigma \f$
     */
    arma::mat compute_stokes_mat (const double& phi0,
                                   const double& lam0,
                                   const double& dlat,
                                   const double& dlon,
                                   const double& sphererad,
                                   const double& intrad,
                                   const int& drow,
                                   const int& dcol);

    /**
     * @brief kernelmat  computes the matrix, composed of the kernel values in grid. Let the Stokes kernel
     * function be function of gridded coordinate differences. Original equation is in the function @see stokes_kernel or @see hotine_kernel.
     * Where the argument \f$ \psi \f$ can be expressed from following equation:
     * \f$ \sin^2 \frac{\psi}{2} = \sin^2 \left( \frac{\Delta \phi_{Q,P}}{2} \right) + \sin^2 \left( \frac{\Delta \lambda_{Q,P}}{2} \right) \cos \phi_Q \cos \phi_P \f$ ,
     * where the \f$ (\phi_P, \lambda_P ) \f$ are the spherical coordinates of the computational point P and \f$ (\phi_Q, \lambda_Q )\f$
     * are the data point Q coordinates. Then the Stokes function can be rewritten as a function of coordinate differences \f$ S(\psi) \rightarrow S( \phi_P, Delta \phi, \Delta \lambda) \f$
     * The matrix that is returned in following format. Because the \f$ \phi_P \f$ is common for all elements it will be omitted in notation.
     * \f$
       \Sigma =
       \left[
       \begin{array}{ccccc}
       \ddots & \vdots & \vdots & \vdots & \ldots
       \\\\
       \ldots & S ( 2 \Delta \phi , -  \Delta \lambda ) & S ( 2 \Delta \phi , 0 \cdot \Delta \lambda ) & S ( 2 \Delta \phi ,  \Delta \lambda )  & \ldots
       \\\\
       \ldots  & S ( \Delta \phi , -  \Delta \lambda ) & S ( \Delta \phi , 0 \cdot \Delta \lambda ) & S (  \Delta \phi ,  \Delta \lambda )  & \ldots
       \\\\
       \ldots  & S ( 0 \cdot \Delta \phi, -  \Delta \lambda ) & S ( 0 \cdot \Delta \phi , 0 \cdot \Delta \lambda ) & S ( 0 \cdot \Delta \phi ,  \Delta \lambda )  & \ldots
       \\\\
       \ldots  & S (-\Delta \phi , -  \Delta \lambda ) & S (- \Delta \phi , 0 \cdot \Delta \lambda ) & S ( -\Delta \phi ,  \Delta \lambda )  & \ldots
       \\\\
       \ldots  & S (-2\Delta \phi , -  \Delta \lambda ) & S (-2 \Delta \phi , 0 \cdot \Delta \lambda ) & S ( -2\Delta \phi ,  \Delta \lambda )  & \ldots
       \\\\
       \ldots & \vdots & \vdots & \vdots & \ddots \\\\
       \end{array}
       \right]
       \f$
     * @param phi0 - the latitude value of the point P in DEG
     * @param lam0 - the longitude value of the point P in DEG
     * @param dlat - the difference between two meridians in DEG
     * @param dlon - the difference between two parallels in DEG
     * @param sphererad - radius of the reference sphere
     * @param intrad - integration radius
     * @param drow - initial number of the rows from the middle point to one of the edges
     * @param dc_l
     * @param dc_r
     * @param check_radius - check radius, if the distance between the running point of the integration
     * is less than the intrad parameter
     * @return
     */
    arma::mat kernelmat ( const double &phi0,
                          const double &lam0,
                          const double &dlat,
                          const double &dlon,
                          const double &sphererad,
                          const double &intrad,
                          const arma::uword &drow,
                          const arma::uword &dc_l,
                          const arma::uword &dc_r,
                          const bool& check_radius,
                          double (&kernel_fun) (double));


    /**
     * @brief kernelmat2
     * @param phi0
     * @param lam0
     * @param dlat
     * @param dlon
     * @param sphererad
     * @param intrad
     * @param dr_l
     * @param dr_u
     * @param dc_l
     * @param dc_r
     * @param check_radius
     * @param inner_area
     * @return
     */
    arma::mat kernelmat2 (const double &phi0,
                          const double &lam0,
                          const double &dlat,
                          const double &dlon,
                          const double &sphererad,
                          const double &intrad,
                          const arma::uword &dr_l, // low
                          const arma::uword &dr_u,
                          const arma::uword &dc_l,
                          const arma::uword &dc_r,
                          const bool& check_radius,
                          double (&kernel_fun) (double),
                          bool inner_area = true);

    /**
     * @brief kernel_legen_sum
     * @param phi0 - geodetic latitude of the center point
     * @param lam0 - geodetic longitude of the center point
     * @param dlat - value of the step for latitude
     * @param dlon - value of the step for longitude
     * @param sphererad - radius of the reference sphere
     * @param intrad - integration radius
     * @param dr_l - number of the lines below the center point
     * @param dr_u - number of the lines above the center point
     * @param dc_l - number of the columns left to the center point
     * @param dc_r - number of the columns right to the center point
     * @param check_radius - check radius {true, false}
     * @param kernel_type - kernel type Stokes or Hotine
     * # Stokes : \f$ S(\psi) = f$ \sum \limits_{n=2}^{\infty} \frac{2n+1}{n-1} P_n (\cos \psi) \f$
     * # Hotine : \f$ S(\psi) = f$ \sum \limits_{n=0}^{\infty} \frac{2n+1}{n+1} P_n (\cos \psi) \f$
     * @param n_min - the minimum boundary in previously introduced sums
     * @param n_max - the maximum boundary in previously introduced sums
     * @param inner_area
     * @return - matrix with kernel values, main purpose of such matrix is solving the
     * integral by convolution
     */
    arma::mat kernel_legen_sum ( const double &phi0,
                                 const double &lam0,
                                 const double &dlat,
                                 const double &dlon,
                                 const double &sphererad,
                                 const double &intrad,
                                 const arma::uword &dr_l, // low
                                 const arma::uword &dr_u,
                                 const arma::uword &dc_l,
                                 const arma::uword &dc_r,
                                 const bool& check_radius,
                                 const string& kernel_type,
                                 const unsigned& nmin,
                                 const unsigned& nmax,
                                 bool inner_area = true );

    /**
     * @brief l03_mat
     * @param phi0
     * @param lam0
     * @param dlat
     * @param dlon
     * @param sphererad
     * @param intrad
     * @param dr_l
     * @param dr_u
     * @param dc_l
     * @param dc_r
     * @param check_radius
     * @param inner_area
     * @return
     */
    arma::mat l03_mat (  const double &phi0,
                         const double &lam0,
                         const double &dlat,
                         const double &dlon,
                         const double &sphererad,
                         const double &intrad,
                         const arma::uword &dr_l, // low
                         const arma::uword &dr_u,
                         const arma::uword &dc_l,
                         const arma::uword &dc_r,
                         const bool& check_radius,
                         bool inner_area = true);


    /**
     * @brief compute_lmatrix similiar function to @see compute_stokes_mat , but instead of the value of the Stokes kernel function ( in closed form)
     * the function returns the \f$ \frac{1}{l^3 ( \Delta \phi  , \Delta \lambda )} \f$ , where the \f$ l \f$ is the distance on the reference sphere
     * computed as \f$ l = R \arccos \left(  \sin \phi_1 \sin \phi_2  + \cos \phi_1 \cos \phi_2 \cos ( \lambda_2 - \lambda_1) \right) \f$ where
     * R is the radius of the reference sphere. Or \f$ l = R \sin \frac{\psi}{2} \f$, Returns zero for \f$ \Delta \phi = 0 , \Delta \lambda = 0 \f$
     *
     * @param phi0 - the latitude value of the point P in DEG
     * @param lam0 - the longitude value of the point P in DEG
     * @param dlat - the difference between two meridians in DEG
     * @param dlon - the difference between two parallels in DEG
     * @param sphererad - radius of the reference sphere
     * @param intrad - integration radius
     * @param drow - initial number of the rows from the middle point to one of the edges
     * @param dcol - initial number of the columns from the middle point to one of the edges
     * @return
     */
    arma::mat compute_lmatrix ( const double& phi0,
                                const double& lam0,
                                const double& dlat,
                                const double& dlon,
                                const double& sphererad,
                                const double& intrad,
                                const int& drow,
                                const int& dcol);

    /**
     * @brief len_on_sphere
     * @param radius
     * @param b1
     * @param l1
     * @param b2
     * @param l2
     * @return
     */
    double len_on_sphere( double radius, double b1, double l1, double b2, double l2 );

    /**
     * @brief length_sphere - returns the length between two points \[$ P_1 (b_1, l_1), P_2(b_2,l_2) \f$ on sphere
     * @param b1 - in DEG
     * @param l1 - in DEG
     * @param b2 - in DEG
     * @param l2 - in DEG
     * @param rsphere - radius of the reference sphere, default value set to \f$ r_{sphere} =  1. \f$
     * @return
     */
    double length_sphere( double b1 , double l1, double b2, double l2, double rsphere = 1.0 );


    /**
     * @brief lhuilierTheorem - returns the area of a spherical triangle computed by l'Huilier's Theorem
     * @param a - in m
     * @param b - in m
     * @param c - in m
     * @param rsphere  radius of the reference sphere, default value set to \f$ r_{sphere} =  1. \f$
     * @return area of the spherical triangle, if \f$ r_{sphere} =  1. \f$ then the result is equal to
     * spherial excess of that triangle in radians
     */
    double lhuilierTheorem ( double a ,double b, double c , double rsphere = 1.0 );



    /**
     * @brief zero_area_effect - compute the zero area effect when solving the Stokes integral. The integral
     * kernel is solved in cylindrical coordinates when the singularity in original point \f$ psi = 0 \f$
     * @param phi0 - center point for the spherical rectangle (latitude in DEG)
     * @param lam0 - center point fot the spherical rectangle (longitude in DEG)
     * @param dphi - delta latitude in DEG
     * @param dlam - difference in longitude in DEG
     * @param kernel_int_fun - reference to indefinite integral of the stokes kernel function or hotine function
     *\f$ \int S(\psi) \sin \psi  \matrhm{d} \psi , \qquad \int H(\psi) \sin \psi \mathrm{d} \psi \f$
     * @return
     */
    double zero_area_effect ( const double& phi0 ,
                              const double& lam0,
                              const double& dphi,
                              const double& dlam,
                              double (&kernel_int_fun)(double) );

    template<typename eT>
    /**
     * @brief convert_tpot2qg - iterative solution for convertion the disturbing potential to height anomaly
     * zeta \f$ \zeta \f$.
     * @param tpot - disturbing potential
     * @param hell - ellipsoidal height
     * @param ell - reference ellipsoid
     * @return height anomaly
     */
    eT convert_tpot2qg( const eT& phi, const eT& tpot , const eT& hell , struct ellipsoid<eT> ell) {
        int i = 0;
        eT gamma = ell.get_normal_gravity(phi);
        eT zeta0 = tpot / gamma;
        eT dzeta = 999999.9;
        eT zeta;
        while ( abs(dzeta) > 0.0001 ) {
            gamma = ell.get_normal_gravity( phi , hell - zeta0 );
            zeta = tpot / gamma;

            dzeta = zeta - zeta0;
            zeta0 = zeta;

            if ( i++ == 10)
                break;
        }

        return zeta0;
    }


//===========================================================================================//
/**
* https://web.natur.cuni.cz/~bayertom/images/courses/mmk/mk8.pdf
* https://user.unob.cz/talhofer/Z%C3%A1klady%20matematick%C3%A9%20kartografie.pdf
* http://geo3.fsv.cvut.cz/~soukup/dip/jezek/kap1.html
*
 * @brief bl2jtsk - transforms geodetic latitude and longitude on the Bessel's ellipsoid
 * to plane coordinates in jtsk ( czech national coordinate system
 * @param b - geodetic latitude on the Bessel's ellipsoid in DEG
 * @param l - geodetic longitude on the Bessel's ellipsoid in DEG
 * @return vector C++ [ y x] in meetres
*/
vector<double> bl2jtsk( double b, double l);

/**
 * @brief jtsk2bl - transforms the rectangular plane coordinates (jtsk) to
 * geodetic latitude and longitude on the Bessel's ellipsoid
 * @param y - coordinate in +east direction
 * @param x - coordinate in +south direction
 * @return vector c++ [b, l] in deg format
 */
vector<double> jtsk2bl( double y, double x);


/**
 * @brief bilinearInterpolation - https://en.wikipedia.org/wiki/Bilinear_interpolation
 * @param x
 * @param y
 * @param x1
 * @param x2
 * @param y1
 * @param y2
 * @param f11
 * @param f12
 * @param f21
 * @param f22
 * @return \f$ f(x,y) = a_0 + a_1 x + a_2 y + a_3 xy \f$
 */
double bilinearInterpolation ( double x, double y, double x1, double x2 , double y1, double y2, double f11, double f12, double f21, double f22 );

}
#endif // GEODETIC_FUNCTIONS_H
