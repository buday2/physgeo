#ifndef FNALFS_H
#define FNALFS_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <sstream>
#include <string>
#include <fstream>
#include <typeinfo>
#include <utility>
#include <new>
#include <cstddef>         // std::size_t
#include <typeinfo>

#include <omp.h>

// own libraries
#include "lvec.h"
#include "physical_constants.h"

using namespace std;

template<typename T>
string TypeOf(T){
    string Type="unknown";
    if(is_same<T,int>::value) Type="int";
    if(is_same<T,unsigned int>::value) Type="uint";
    if(is_same<T,double>::value) Type="double";
    if(is_same<T,float>::value) Type="float";
    if(is_same<T,long double>::value) Type="long double";

    return Type;
}


/**
 * @brief The FnALFs class - Fully normalized associated Legendre functions
 * for convenience \f$ t = \sin \varphi \f$ and \f$ u = \cos \varphi \f$, the
 * The fully normalized associated Legendre functions \f$ \bar{P}_{nm} (t) \f$,
 * sometimes also called fully normalized harmonics, can be computed from the conventional
 * associated Legendre functions \f$ {P}_{nm} (t).
 *
 * \f$ \bar{P}_{nm} (t)  = N_{nm} P_{nm} (t) \f$ , where
 * \f$ N_{nm} = \sqrt{k(2n+1) \frac{(n-m)!}{(n+m)!} } \f$, \f$ k = 1 \f$ for
 * \f$ m = 0 \f$ , \f$ k=2 \f$ otherwise. This class is essential for Global Gravity
 * Field Modelling (GGFM class). This class is capable of computing the fnALFs using
 * the recursive algorithms and stored them into the vector (@see Lvec class) including
 * the first and second order derivatives.
 */
template <typename eT>
class FnALFs {
    public:
         /**
         * @brief FnALFs
         * @param n - degree ond order of the fnALFs
         * @param diff_order - derivative of the fnALF's, max degree is 2
         */
    FnALFs( const unsigned int& c_n, const string& c_algorithm , const unsigned int& c_diff_order ) {
        n_max = c_n;
        dero = c_diff_order;
        algorithm = c_algorithm;

        sc = relax_factor();

        col_order = false;

        if ( dero == 0 ) {
            p_nm.resize(n_max, col_order);
        } else if ( dero == 1 ) {
            p_nm.resize(n_max, col_order);
            dp_nm.resize(n_max, col_order);
        } else {
            p_nm.resize(n_max, col_order);
            dp_nm.resize(n_max, col_order);
            ddp_nm.resize(n_max, col_order);
        }

        wn_indexes = new unsigned[n_max +1 ];

        for (unsigned i = 0; i <= n_max; i++) {
            wn_indexes[i] = p_nm.return_index(i,i);
        }
    }

    virtual ~FnALFs() {

        delete [] wn_indexes;
    }

    void resize( const unsigned int& new_nmax) {
        n_max = new_nmax;


        if ( dero == 0 ) {
            p_nm.resize(n_max, col_order);
        } else if ( dero == 1 ) {
            p_nm.resize(n_max, col_order);
            dp_nm.resize(n_max, col_order);
        } else {
            p_nm.resize(n_max, col_order);
            dp_nm.resize(n_max, col_order);
            ddp_nm.resize(n_max, col_order);
        }
        delete [] wn_indexes;

        wn_indexes = new unsigned[n_max +1 ];

        for (unsigned i = 0; i <= n_max; i++) {
            wn_indexes[i] = p_nm.return_index(i,i);
        }
    }


        /**
         * @brief compute_FnALFs - compute fully normalized Assocciated Legendre's polynomials for the reference value
         * @param x_lat - spherical latitude of the reference value in DEG format. Possible inputs are 'stan_mod', 'standard', 'tmfcm' , 'tmfcm_2nd'.
         * The 'stan_mod' and 'standard' are numerically stable upto degree and order 700 and latitude from -80° upto +80°. The remaining three algorithms are
         * numerically stable upto degree and order 2700 and geodetic latitude from -89° upto +89°.
         *
         * @param alg - type of algorithm for the computation
         */
        void compute_FnALFs( const eT& x_lat ) {
            // Diff order max = 2, everything else will be set to 2

            x_val = x_lat;
            eT x = x_lat * DEG2RAD;


            //***************************** no derivative **********************************//
            if ( algorithm.compare( "stan_mod" ) == 0 ) {
                /* for x in -Pi/2 ti Pi/2 */

                p_nm[0] = static_cast<eT>( 1.0 * sc );                          //  P_{00}
                p_nm[1] = static_cast<eT>( sqrt(3.0) * sin(x) * sc );           //  P_{10}
                p_nm[wn_indexes[1]] = static_cast<eT>( sqrt(3.0) * cos(x) * sc);//  P_{11}

                for (unsigned int i = 2; i <= n_max; i++) {
                    eT i_ = static_cast<eT>(i);
                    p_nm[ wn_indexes[i] ] =
                            p_nm[ wn_indexes[i-1] ] * sqrt((2 * i_ + 1.0) / (2 * i_)) * cos(x);
                }

                for (unsigned int j = 0; j <= n_max; j++) {
                    eT j_ = static_cast<double>(j);
                    unsigned k = ( j < 2 ) ? 2 : j;


                    unsigned init_idx = p_nm.return_index(k,j);
                    for (unsigned int i = k; i <= n_max; i++) {
                        eT a_nm, b_nm;
                        eT i_ = static_cast<eT>(i);

                        if ( i == j  ) {
                            init_idx++;
                            continue;
                        } else if (j == 0) {
                            a_nm = sqrt((4.0 * i_ * i_ - 1.0) / (i_ * i_ - j_ * j_));
                            b_nm = sqrt(((2.0 * i_ + 1.0) * (i_ + j_ - 1.0) * (i_ - j_ - 1.0)) /
                                        ((i_ * i_ - j_ * j_) * (2.0 * i_ - 3.0)));
                            //p_nm(i,j) = p_nm(i - 1,j) * a_nm * sin(x) - p_nm(i - 2,j) * b_nm;
                            p_nm[init_idx] =  p_nm[init_idx-1] * a_nm * sin(x) - p_nm[init_idx-2] * b_nm;

                        } else if (j == i - 1) {
                            a_nm = sqrt((4.0 * i_ * i_ - 1.0) / (i_ * i_ - j_ * j_));
                            //p_nm(i,j) = p_nm(i - 1,j) * a_nm * sin(x);
                            p_nm[init_idx] = p_nm[init_idx-1] * a_nm * sin(x);
                        } else {
                            a_nm = sqrt((4.0 * i_ * i_ - 1.0) / (i_ * i_ - j_ * j_));
                            b_nm = sqrt(((2.0 * i_ + 1.0) * (i_ + j_ - 1.0) * (i_ - j_ - 1.0)) /
                                        ((i_ * i_ - j_ * j_) * (2.0 * i_ - 3.0)));
                            //p_nm(i,j) = p_nm(i - 1,j) * a_nm * sin(x) - p_nm(i - 2,j) * b_nm;
                            p_nm[init_idx] = p_nm[init_idx-1] * a_nm * sin(x) - p_nm[init_idx-2] * b_nm;
                        }

                        init_idx++;
                    }
                }
//            } else if ( algorithm.compare( "tmfrm" ) == 0 ) {
//                /* tmfrm = the modified forward row method
//                 * divided by therm u_m = cos^m(x)
//                 *  x in [-Pi/2, Pi/2] */

//                p_nm[0] = 1.0 * sc;                   //  P_{00}
//                p_nm[1] = sqrt(3.0) * sin(x) * sc;    //  P_{10}
//                p_nm[wn_indexes[1]] = sqrt(3.0) * sc; //  P_{11}

//                for ( unsigned i = 2; i <= n_max; i++) {
//                    eT i_ = static_cast<eT>(i);
//                    p_nm[wn_indexes[i]] = sqrt((2.0 * i_ + 1.0) / (2.0 * i_)) * p_nm[wn_indexes[i-1]];
//                }

//                for ( unsigned int i = 2; i <= n_max; i++) {
//                    eT i_ = static_cast<eT>(i);

//                    unsigned init_idx = wn_indexes[i];
//                    for (unsigned int j = i;/*need 0 index too*/; j--) { // need conversion to unsigned int
//                        eT g_nm, h_nm, j_, denom;
//                        j_ = static_cast<eT>(j);
//                        denom = (j == 0) ? sqrt(2.0) : 1.0;

//                        if (i == j) {  // sectorial
//                            init_idx--;
//                            continue;
//                        } else if (j == 0) {  // already edited
//                            h_nm = sqrt((i_ + j_ + 2.0) * (i_ - j - 1.0) /
//                                        ((i_ - j_) * (i_ + j_ + 1.0)));
//                            g_nm = 2.0 / sqrt(i_ * (i_ + 1.0));

//                            p_nm[init_idx] = (g_nm * sin(x) * p_nm[init_idx+1] -
//                                             cos(x) * cos(x) * h_nm * p_nm[init_idx+2]) / denom ;

//                            break; // hits the ground (end of j index cycle)
//                        } else if (i == j + 1) {
//                            g_nm = sqrt(2.0 * i_);
//                            p_nm[init_idx] = (g_nm * sin(x) * p_nm[init_idx+1]) / denom;

//                        } else /*if ( i > j)*/ {
//                            h_nm = sqrt((i_ + j_ + 2.0) * (i_ - j - 1.0) /
//                                        ((i_ - j_) * (i_ + j_ + 1.0)));
//                            g_nm = (2. * (j_ + 1.)) / sqrt((i_ - j_) * (i_ + j_ + 1.));

//                            p_nm[init_idx] = (g_nm * sin(x) * p_nm[init_idx+1] -
//                                         cos(x)*cos(x) * h_nm * p_nm[init_idx+2]) / denom;
//                        }

//                        init_idx--;
//                    }
//                }
            } else if ( algorithm.compare( "tmfcm" ) == 0 ) {
                /* the modified forward column method
                 * for x in -Pi/2 to Pi/2
                 * relaxing factor u^m = cos^m (x)
                 * t = sin(x)                          */
                p_nm[0]= static_cast<eT>( 1.0 * sc );                     //  P_{00}
                p_nm[1]= static_cast<eT>( sqrt(3.0) * sin(x) * sc);       //  P_{10}
                p_nm[wn_indexes[1]] =static_cast<eT>( sqrt(3.0) * sc);    //  P_{11}


                for ( unsigned int i = 2; i <= n_max; i++) { /* sectorial coefficients */
                    eT i_ = static_cast<eT>(i);
                    p_nm[ wn_indexes[i] ] = sqrt((2.0 * i_ + 1.0) / (2.0 * i_)) * p_nm[wn_indexes[i-1]];
                }

                for (unsigned int j = 0; j <= n_max; j++) {
                    eT j_ = static_cast<eT>(j);
                    unsigned k = ( j < 2 ) ? 2 : j;
                    unsigned init_idx = p_nm.return_index(k,j);

                    //#pragma omp parallel for
                        for (unsigned int i = k; i <= n_max; i++) {
                        eT a_nm, b_nm;

                        eT i_ = static_cast<eT>(i);
                        if (i == j) {  // sectorial
                            init_idx++;
                            continue;
                        } else if (j == i - 1) {
                            a_nm = sqrt((4.0 * i_ * i_ - 1.0) / (i_ * i_ - j_ * j_));
                            p_nm[init_idx] = p_nm[init_idx-1]  * a_nm * sin(x);
                        } else /*if ( i>j )*/ {
                            a_nm = sqrt((4.0 * i_ * i_ - 1.0) / (i_ * i_ - j_ * j_));
                            b_nm = sqrt(((2.0 * i_ + 1.0) * (i_ + j_ - 1.0) * (i_ - j_ - 1.0)) /
                                        ((i_ * i_ - j_ * j_) * (2.0 * i_ - 3.0)));
                            p_nm[init_idx] = p_nm[init_idx-1] * a_nm * sin(x) - p_nm[init_idx-2] * b_nm;
                        }
                        init_idx++;
                    }
                }
            } else if ( algorithm.compare("tmfcm_2nd") == 0 ) {
                /* Second modified forward column method */
                p_nm[0] = static_cast<eT>( 1.0 * sc);                       // p_00/p_00
                p_nm[1] = static_cast<eT>( 1.0 * sqrt(3.0) * sin(x) * sc);  // p_10/p_00
                p_nm[wn_indexes[1]] = static_cast<eT>( 1.0 * sc );           // p_11/p_11

                for ( unsigned int i = 2; i <= n_max; i++) { /* sectorial coefficients */
                    p_nm[ wn_indexes[i] ] = sc;
                }

                //#pragma omp parallel for
                for (unsigned int j = 0; j <= n_max; j++) {
                    eT j_ = static_cast<eT>(j);
                    unsigned k = ( j < 2 ) ? 2 : j;
                    unsigned init_idx = p_nm.return_index(k,j);

                    //#pragma omp parallel for
                    for (unsigned int i = k; i <= n_max; i++) {
                        eT a_nm, b_nm;
                        eT i_ = static_cast<eT>(i);

                        if (i == j) {
                            init_idx++;
                            continue;
                        } else if (j == i - 1) {
                            a_nm = sqrt((4.0 * i_ * i_ - 1.0) / (i_ * i_ - j_ * j_));
                            p_nm[init_idx] = p_nm[init_idx-1]  * a_nm * sin(x);
                        } else /*if ( i>j )*/ {
                            a_nm = sqrt((4.0 * i_ * i_ - 1.0) / (i_ * i_ - j_ * j_));
                            b_nm = sqrt(((2.0 * i_ + 1.0) * (i_ + j_ - 1.0) * (i_ - j_ - 1.0)) /
                                        ((i_ * i_ - j_ * j_) * (2.0 * i_ - 3.0)));
                            p_nm[init_idx] = p_nm[init_idx-1] * a_nm * sin(x) - p_nm[init_idx-2] * b_nm;
                        }

                        init_idx++;
                    }
                }
            } else {  // Type of the solution is set to \"standard\" by default
                p_nm[0] = static_cast<eT>( 1.0 * sc );                          //  P_{00}
                p_nm[1] = static_cast<eT>( sqrt(3.0) * sin(x) * sc );           //  P_{10}
                p_nm[wn_indexes[1]] = static_cast<eT>( sqrt(3.0) * cos(x) * sc);//  P_{11}

                for (unsigned int i = 2; i <= n_max; i++) {
                    eT i_ = static_cast<eT>(i);
                    p_nm[ wn_indexes[i] ] =
                            p_nm[ wn_indexes[i-1] ] * sqrt((2 * i_ + 1.0) / (2 * i_)) * cos(x);
                }


                //#pragma omp parallel for
                for (unsigned int j = 0; j <= n_max; j++) {
                    eT j_ = static_cast<double>(j);
                    unsigned k = ( j < 2 ) ? 2 : j;

                    unsigned init_idx = p_nm.return_index(k,j);
                    for (unsigned int i = k; i <= n_max; i++) {
                        eT a_nm, b_nm;
                        eT i_ = static_cast<eT>(i);

                        if ( i == j  ) {
                            init_idx++;
                            continue;
                        } else if (j == i - 1) {
                            a_nm = sqrt((4.0 * i_ * i_ - 1.0) / (i_ * i_ - j_ * j_));

                            p_nm[init_idx] = p_nm[init_idx-1] * a_nm * sin(x);
                        } else {
                            a_nm = sqrt((4.0 * i_ * i_ - 1.0) / (i_ * i_ - j_ * j_));
                            b_nm = sqrt(((2.0 * i_ + 1.0) * (i_ + j_ - 1.0) * (i_ - j_ - 1.0)) /
                                        ((i_ * i_ - j_ * j_) * (2.0 * i_ - 3.0)));
                            p_nm[init_idx] = p_nm[init_idx-1] * a_nm * sin(x) - p_nm[init_idx-2] * b_nm;

                        }
                        init_idx++;
                    } 
                }
            }

            if (dero == 0) {
                goto end_diff0;
            }
            //****************************************************************************//
            //******************************** FIRST DERIVATIVE **************************//
            //****************************************************************************//
            if ( algorithm.compare( "stan_mod") == 0 ) {
                dp_nm[0] = static_cast<eT>( 0.0 );                                   // P_{00}
                dp_nm[1] = static_cast<eT>(sc * sqrt(3.0) * cos(x));                 // P_{10}
                dp_nm[wn_indexes[1]] =static_cast<eT>(-1.*sc * sqrt(3.0) * sin(x));   // P_{11}

                for (unsigned int i = 2; i <= n_max; i++) {
                    eT i_ = static_cast<eT>(i);
                    eT e_nm = -1.0 * i_;
                    dp_nm[ wn_indexes[i] ] = e_nm * tan(x) * p_nm[wn_indexes[i]];
                }


                for (unsigned int j = 0; j <= n_max; j++) {
                    eT j_ = static_cast<double>(j);
                    unsigned k = ( j < 2 ) ? 2 : j;
                    unsigned init_idx = p_nm.return_index(k,j);

                    for (unsigned int i = k; i <= n_max; i++) {
                        eT e_nm;
                        eT i_ = static_cast<eT>(i);

                        if ( i == j  ) {
                            init_idx++;
                            continue;
                        } else {
                            e_nm = sqrt((i_ * i_ - j_ * j_) * (2. * i_ + 1.) / (2. * i_ - 1.0));
                            dp_nm[init_idx] =
                                    e_nm / cos(x) * p_nm[init_idx-1] - i_ * tan(x) * p_nm[init_idx];
                        }
                        init_idx++;
                    }
                }
            } else if ( algorithm.compare("tmfcm") == 0 ) {
                /* the modified forward column method   */
                /* x=phi, in [-90° , +90°]              */
                /* relaxing factor is u^k = cos^k(x)    */

                dp_nm[0] = static_cast<eT>(0.0);                                           // P_{00}
                dp_nm[1] = static_cast<eT>(sc * sqrt(3.0) * cos(x));                       // P_{10}
                dp_nm[wn_indexes[1]] = static_cast<eT>(-1.0 * sc * sqrt(3.0) * tan(x));    // P_{11}

                for (unsigned int i = 2; i <= n_max; i++) {
                    // p_nm already divided in previous algorithm
                    eT i_ = static_cast<eT>(i);
                    eT e_nm = sqrt((2. * i_ + 1.) / (2. * i_));
                    dp_nm[ wn_indexes[i] ] = e_nm * (dp_nm[wn_indexes[i-1]] - tan(x) * p_nm[ wn_indexes[i-1] ]);
                }

                //#pragma omp parallel for
                for (unsigned int j = 0; j <= n_max; j++) {
                    eT j_ = static_cast<eT>(j);
                    unsigned k = ( j < 2 ) ? 2 : j;
                    unsigned init_idx = p_nm.return_index(k,j);

                    //#pragma omp parallel for
                    for (unsigned int i = k; i <= n_max; i++) {
                        eT e_nm, f_nm;
                        eT i_ = static_cast<eT>(i);

                        if ( i == j ) {
                            init_idx++;
                            continue;
                        } else if (i == j - 1) {
                            e_nm = sqrt((4. * i_ * i_ - 1.) / (i_ * i_ - j_ * j_));
                            dp_nm[init_idx] =
                                    e_nm * (sin(x) * dp_nm[init_idx-1] + cos(x) * p_nm[init_idx-1]);
                        } else {
                            e_nm = sqrt((4. * i_ * i_ - 1.) / (i_ * i_ - j_ * j_));
                            f_nm = sqrt((2. * i_ + 1.) * (i_ + j_ - 1.) * (i_ - j_ - 1.)) /
                                    sqrt((i_ * i_ - j_ * j_) * (2. * i_ - 3.));

                            dp_nm[init_idx] =
                                    (e_nm * (sin(x) * dp_nm[init_idx-1] + cos(x) * p_nm[init_idx-1]))
                                     -( f_nm * dp_nm[init_idx-2] );
                        }
                        init_idx++;
                    }
                }
//            } else if ( algorithm.compare("tmfrm") == 0 ) {
//                /* the modified forward column method   */
//                /* x=phi, in [-90° , +90°]              */
//                /* relaxing factor is u^k = cos^k(x)    */
//                dp_nm[0]             = 0.0;                           // P_{00}
//                dp_nm[1]             = sc * sqrt(3.0) * cos(x);       // P_{10}
//                dp_nm[wn_indexes[1]] = -1.0 * sc * sqrt(3.0) * tan(x);// P_{11}

//                for (unsigned int i = 2; i <= n_max; i++) {
//                    // p_nm already divided in previous algorithm
//                    eT i_ = static_cast<eT>(i);
//                    eT e_nm = sqrt((2. * i_ + 1.) / (2. * i_));
//                    dp_nm[ wn_indexes[i] ] = e_nm * (dp_nm[wn_indexes[i-1]] - tan(x) * p_nm[ wn_indexes[i-1] ]);
//                }

//                for ( unsigned int i = 2; i <= n_max; i++) {
//                    eT i_ = static_cast<eT>(i);

//                    unsigned init_idx = wn_indexes[i];
//                    for (unsigned int j = i;/*need 0 index too*/; j--) { // need conversion to unsigned int
//                        eT e_nm, f_nm, j_;
//                        j_ = static_cast<eT>(j);

//                        //cout << ":)tmfrm " << i << " " << j << "\t\t" << init_idx << " " << wn_indexes[i] <<endl;
//                        if (i == j) {  // sectorial
//                            init_idx--;
//                            continue;
////                        } else if (i == j - 1) {
////                            e_nm = sqrt((4. * i_ * i_ - 1.) / (i_ * i_ - j_ * j_));
////                            dp_nm[init_idx] =
////                                    e_nm * (sin(x) * dp_nm[init_idx+1] + cos(x) * p_nm[init_idx+1]);
//                        } else {
//                            e_nm = sqrt( (i_- j_+ 1.) / (i_ + j_ +1 ));
//                            f_nm = j_;

//                            dp_nm[init_idx] = (abs(sin(x)) * e_nm * dp_nm[init_idx+1] - f_nm*cos(x)*p_nm[init_idx])/(sin(x)*sin(x)) ;
//                        }

//                        init_idx--;
//                        if ( j == 0 )
//                            break;
//                    }
//                }
            } else if (algorithm.compare("tmfcm_2nd") == 0) {
                /* the modified forward column method of 2nd type
                 * x = phi in [-90°,90°]
                 * relaxing factor is P_mm, n=m
                 */
                dp_nm[0] = static_cast<eT>( 0.0 );                           // P_{00}
                dp_nm[1] = static_cast<eT>(sc * sqrt(3.0) * cos(x));         // P_{10}
                dp_nm[wn_indexes[1]] = static_cast<eT>(-1.0 * sc * tan(x));  // P_{11}

                //#pragma omp for schedule(dynamic,1) private (k)
                for (unsigned int k = 2; k <= n_max; k++) {
                    // p_nm already divided in previous algorithm
                    eT i_ = static_cast<eT>(k);
                    dp_nm[ wn_indexes[k] ] = -sc * 1.0 * i_ * tan(x);
                }

                //#pragma omp parallel for
                for (unsigned int j = 0; j <= n_max; j++) {
                    eT j_ = static_cast<eT>(j);
                    unsigned k = ( j < 2 ) ? 2 : j;
                    unsigned init_idx = p_nm.return_index(k,j);

                    //#pragma omp parallel for
                    for (unsigned int i = k; i <= n_max; i++) {
                        eT e_nm;
                        eT i_ = static_cast<eT>(i);

                        if (i == j) {
                            init_idx++;
                            continue;
                        } else  if (i == j - 1) {
                            e_nm = sqrt((4. * i_ * i_ - 1.) / (i_ * i_ - j_ * j_));
                            dp_nm[init_idx] =
                                    e_nm * (sin(x) * dp_nm[init_idx-1] + cos(x) * p_nm[init_idx-1]);
                        } else {
                            e_nm = sqrt((i_ * i_ - j_ * j_) * (2. * i_ + 1.) / (2. * i_ - 1.0));
                            dp_nm(i,j) =
                                    e_nm / cos(x) * p_nm[init_idx-1] - i_ * tan(x) * p_nm[init_idx];
                        }
                        init_idx++;
                    }
                }
            } else {
                // Type of the solution is set to \"standard\" by default
                dp_nm[0]               = static_cast<eT>(0.0);                     // P_{00}
                dp_nm[1]               = static_cast<eT>(sc * sqrt(3.0) * cos(x)); // P_{10}
                dp_nm[ wn_indexes[1] ] = static_cast<eT>(-1.0 * sc * sqrt(3.0) * sin(x)); // P_{11}

                for (unsigned int i = 2; i <= n_max; i++) {
                    eT e_nm;
                    eT i_ = static_cast<eT>(i);
                    e_nm = sqrt((2.0 * i_ + 1.0) / (2.0 * i_));
                    dp_nm[ wn_indexes[i]] =
                            e_nm * (cos(x) * dp_nm[wn_indexes[i-1]] - sin(x) * p_nm[wn_indexes[i-1]]);
                }

                for (unsigned int j = 0; j <= n_max; j++) {
                    eT j_ = static_cast<double>(j);
                    unsigned k = ( j < 2 ) ? 2 : j;


                    unsigned init_idx = p_nm.return_index(k,j);
                    for (unsigned int i = k; i <= n_max; i++) {
                        eT e_nm, f_nm;
                        eT i_ = static_cast<eT>(i);

                        if ( i == j  ) {
                            init_idx++;
                            continue;
                        } else  if (j == (i - 1)) {
                            e_nm = sqrt((4.0 * i_ * i_ - 1.0) / (i_ * i_ - j_ * j_));
                            dp_nm[init_idx] =
                                    e_nm * (sin(x) * dp_nm[init_idx-1] + cos(x) * p_nm[init_idx-1] );
                        } else {
                            e_nm = sqrt((4.0 * i_ * i_ - 1.0) / (i_ * i_ - j_ * j_));
                            f_nm = sqrt((2.0 * i_ + 1.0) * (i_ + j_ - 1.0) * (i_ - j_ - 1.0));
                            f_nm /= sqrt((i_ * i_ - j_ * j_) * (2.0 * i_ - 3.0));

                            dp_nm[init_idx]  =
                                    e_nm * (sin(x) * dp_nm[init_idx-1] + cos(x) * p_nm[init_idx-1] );
                            dp_nm[init_idx]  -= f_nm * dp_nm[init_idx-2] ;
                        }
                        init_idx++;
                    }
                }
            }

            if (dero == 1) {
                goto end_diff1;
            }

            // ================================================================ //
            // END of 1st derivative of fnALFs                                  //
            // BEGIN of 2st derivative of fnALFs                                //
            // ================================================================ //
            /* Legendre differential equation
             * (1-x^2) y" - 2x y' + [ n(n+1) - m^2/(1-x^2)] y = 0
             */
            /* From Hofmann - Wellenhof Physical geodesy
             * eq (1.46)
             * \f$ \sin \theta (P^m_n)^{\prime\prime} + \cos \theta (P^m_n)^{\prime} + \left[ n(n+1) \sin \theta  - \frac{m^2}{\sin \theta}\right] P^m_n = 0 \f$
             * where \f$ \theta \in [0, \Pi] \$
             * */

            // \f$ P_{nm}^{\prime \prime} (\cos \varphi) = \left(  \frac{m^2}{\sin^2 \varphi} - n(n+1) \right) P_{nm} - \frac{P_{nm}^{\prime}}{\tan \varphi}, \forall m \leq n \f$
            // \f$ \frac{P_{nm}^{\prime \prime} (\cos \varphi) }{\sin^m \varphi} = \left(  \frac{m^2}{\sin^2 \varphi} - n(n+1) \right) \frac{ P_{nm} }{\sin^m \varphi} - \frac{P_{nm}^{\prime}}{\tan \varphi \cdot \sin^m \varphi}, \forall m \leq n \f$
            for (unsigned int j = 0; j <= n_max; j++) {
                eT j_ = static_cast<eT>(j);
                unsigned init_idx = wn_indexes[j];
                for (unsigned int i = j; i <= n_max; i++) {
                    eT i_ = static_cast<eT>(i);
                    ddp_nm[init_idx]=
                            ((j_ * j_) / (cos(x) * cos(x)) - i_ * (i_ + 1.)) * p_nm[init_idx] +
                            dp_nm[init_idx] * tan(x);

                    init_idx++;
                }
            }


//            if ( algorithm.compare("tmfrm") == 0 ) {

//                for ( unsigned int i = 2; i <= n_max; i++) {
//                    eT i_ = static_cast<eT>(i);

//                    unsigned init_idx = wn_indexes[i];
//                    for (unsigned int j = i;/*need 0 index too*/; j--) { // need conversion to unsigned int
//                        eT j_ = static_cast<eT>(j);

//                        ddp_nm[init_idx]=
//                                ((j_ * j_) / (cos(x) * cos(x)) - i_ * (i_ + 1.)) * p_nm[init_idx] +
//                                 dp_nm[init_idx]/tan(x);

//                        init_idx--;
//                        if ( j == 0)
//                            break;
//                    }
//                }
//            }

        end_diff0:
        end_diff1:;
        }



//        void compute_2nd_derivative() {

//        }

        // Pointers/references to protected data;
        const string& n_algorithm() const {
            return  algorithm;
        }

        /**
         * @brief operator <<  - overloading the operator for ostream
         * @param stream - stream, where the @see FnALFs of @param fncoeff should be sent
         * @param fncoeff
         * @return
         */
        friend ostream& operator<<( ostream& stream, FnALFs<eT> fncoeff ){
            if ( fncoeff.dero == 0 ) {
                cout << "n   m   P_{nm}\n";

                if ( fncoeff.col_order ) {
                    for ( unsigned n = 0; n <= fncoeff.n_max; n++ ) {
                        for (unsigned m =0; m <= n; m++ ) {
                            stream << n << "    " << m << "   " <<  fncoeff.p_nm(n,m)<< endl;
                        }
                    }
                } else {
                    for ( unsigned m = 0; m <= fncoeff.n_max; m++ ) {
                        for (unsigned n =m; n <= fncoeff.n_max ; n++ ) {
                            stream << n << "    " << m << "   " << fncoeff.p_nm(n,m)<< endl;
                        }
                    }
                }
            } else if ( fncoeff.dero == 1 ) {
                cout << "n   m   P_{nm}    dP_{nm}\n";

                if ( fncoeff.col_order ) {
                    for ( unsigned n = 0; n <= fncoeff.n_max; n++ ) {
                        for (unsigned m =0; m <= n; m++ ) {
                            stream << n << "    " << m << "    " << fncoeff.p_nm(n,m)
                                                       << "    " << fncoeff.dp_nm(n,m) << endl;
                        }
                    }
                } else {
                    for ( unsigned m = 0; m <= fncoeff.n_max; m++ ) {
                        for (unsigned n =m; n <= fncoeff.n_max ; n++ ) {
                            stream << n << "    " << m << "    " << fncoeff.p_nm(n,m)
                                                       << "    " << fncoeff.dp_nm(n,m) << endl;
                        }
                    }
                }
            } else {
                cout << "n   m   P_{nm}    dP_{nm}    ddP_{nm}\n";

                if ( fncoeff.col_order ) {
                    for ( unsigned n = 0; n <= fncoeff.n_max; n++ ) {
                        for (unsigned m =0; m <= n; m++ ) {
                            stream << n << "    " << m << "    " << fncoeff.p_nm(n,m)
                                                       << "    " << fncoeff.dp_nm(n,m)
                                                       << "    " << fncoeff.ddp_nm(n,m) << endl;
                        }
                    }
                } else {
                    for ( unsigned m = 0; m <= fncoeff.n_max; m++ ) {
                        for (unsigned n =m; n <= fncoeff.n_max ; n++ ) {
                            stream << n << "    " << m << "    " << fncoeff.p_nm(n,m)
                                                       << "    " << fncoeff.dp_nm(n,m)
                                                       << "    " << fncoeff.ddp_nm(n,m) << endl;
                        }
                    }
                }
            }
            return  stream;
        }

        const Lvec<eT>& ref2p_nm() const {
            if ( p_nm.nelem() == 0 ) {
                string err_mes = "FnALFs::ref2p_nm() calls reference to empty Lvec object.";
                throw runtime_error( err_mes );
            } else {
                return  p_nm;
            }
        }

        const Lvec<eT>& ref2dp_nm() const {
            if ( dp_nm.nelem() == 0 ) {
                string err_mes = "FnALFs::ref2dp_nm() calls reference to empty Lvec object.";
                throw runtime_error( err_mes );
            } else {
                return  dp_nm;
            }
        }

        const Lvec<eT>& ref2ddp_nm() const {
            if ( ddp_nm.nelem() == 0 ) {
                string err_mes = "FnALFs::ref2ddp_nm() calls reference to empty Lvec object.";
                throw runtime_error( err_mes );
            } else {
                return  ddp_nm;
            }
        }

        void* operator new( size_t total_size ) {
            void *p = ::new FnALFs();

            return p;
        }

        void operator delete (void* p) {
            free(p);
        }

    //**************************************************************************************************************************************************************************//
    protected:
        inline eT relax_factor() {
          /* has to be called as relax_factor<double>(), ....
           * used as scaling parameter for legendre coefficients
           */
              eT var;
              if (typeid(var) == typeid(double)) {
                return 1.0e-280;
              } else if (typeid(var) == typeid(long double)) {
                return 1.0e-280;
              } else if (typeid(var) == typeid(float)) {
                return 1.0e-35;
              } else {
                return 1;
              }
         }


        string algorithm;    ///< ALgorithm used to calcute fully normalized
                             ///< assocciated Legendre's polynomial. The possible options are : 'stan_mod', 'standard', 'tmfcm', 'tmfcm_2nd'
        eT x_val;            ///< The reference value of angle used for computation. \f$ x_{val} \in (-90,90) \f$
        Lvec<eT> p_nm;       ///< The values of fnALFs stored in two dimensional field
        Lvec<eT> dp_nm;      ///< The values of first derivative of the fnALFs stored in two dimensional field
        Lvec<eT> ddp_nm;     ///< The values of second derivative of the fnALFs stored in two dimensional field
        unsigned int n_max;  ///< Maximum degree of the fnALF's
        unsigned int dero;   ///< Derivative order

        unsigned* wn_indexes;///< whole number indexes it Lvec data ( indexes such as 00,11,22,33,.... )
        bool col_order;      ///< see Lvec class for ordering the element (columns vs rows)

        eT sc;               ///< relax factor used for computation;



};
#endif // FNALFS_H
