#ifndef PHYS_MATH_H
#define PHYS_MATH_H

#include <cmath>
#include <string>
#include <stdexcept>

namespace phys_math {

/**
 * @brief factorial - return the factorial of the number based on
 * gamma function
 * @param n an integer
 * @return
 */
template< typename eT = double >
eT factorial( const unsigned int& n ) {
    eT fac_res = 1;

    if ( n <= 170 ) {
        if ( n == 0 ) {
            return fac_res ;
        } else {
            return static_cast<eT>( std::tgamma( static_cast<eT>(n+1.0) ) );
        }
    } else {
        std::string err_msg = "phys_math::factorial can compute factorial only in range 0-170," + std::to_string(n)+" given.";
        throw std::runtime_error( err_msg );
    }
}

} // namespace phys_math


#endif // PHYS_MATH_H
