#ifndef GAUSSQUAD_H
#define GAUSSQUAD_H

#include <iomanip>
#include <iostream>
#include <cmath>

#define ARMA_USE_HDF5
#include <hdf5.h>
#include <armadillo>

#include "physical_constants.h"
#include "constants.h"

using namespace std;

/**
 * @brief The Gaussquad - no numerical stability tested yet for higher legen polynomials than 100, but for integration just 42 is max degree. So use only 42 as maxval
 * -> Alternate approach added, using the agorithms to find the roots of LegenP with zeros of the Bessel function of the first kind. Precomputed with Maple and stored
 * -> into "hdf5" format. The two matrixec are loaded when constructor is called.
 */
class Gaussquad
{
public:
    /**
     * @brief Gaussquad - creates the table of nodes and  weights for degree of n (Legendre polynomials)
     * @brief prepath - prefix of path to Bessel J0 (first kind)
     * @param n
     */
    Gaussquad(unsigned n=10 , const string& path = PATH_DATA_BESSEL0_HDF5);

    /**
     * @brief int1d \f$ \int_a^b f(x) \mathrm{d}x \approx \frac{b-a}{2} \sum_{i=1}^n w_i \cdot f \left( \frac{b-a}{2} x_i + \frac{b+a}{2} \right) \f$
     * @param a - lower boundary of an integral
     * @param b - upper boundary of an integral
     * @return value of \f$ \int_a^b f(x) \mathrm{d}x \f$
     */
    double int1d ( double (*fx)(double), double a, double b);

    /**
     * @brief int2d \f$ \int_{x_1}^{x_2} \int_{y_1}^{y_2} f(x,y) \mathrm{d}y \mathrm{d}x \approx \frac{x_2-x_1}{2}  \frac{y_2-y_1}{2} \sum_{i=1}^n w_i \cdot f \left( \frac{b-a}{2} x_i + \frac{b+a}{2} \right) \f$
     * @param x1
     * @param x2
     * @param y1
     * @param y2
     * @return
     */
    double int2d ( double (*fxy)(double, double), double x1, double x2, double y1, double y2);

    /**
     * @brief view - show the nodes and weights
     */
    void view();


    /**
     * @brief legendreP
     * @param xvec
     * @param n
     * @return
     */
    arma::vec legendreP ( arma::vec xvec, unsigned n );

    /**
     * @brief dlegendreP
     * \f$  P_{i+1}^n (x) = \frac{ (2i - 1) \cdot x * P_i^n(x) - (i-1) P_{i-1}^n(x) }{i}  \f$
     * @param xvec
     * @param n
     * @return
     */
    arma::vec dlegendreP (arma::vec xvec, unsigned n);


    /**
     * @brief legen_zeros
     * @param n
     * @param alg
     * @param alpha
     * @param beta
     * @return matrix with n rows and 2 columns. [x_i , w_i], nodes with weigts
     */
    arma::mat legen_zeros( unsigned n , bool alg = false , double alpha = 0.1 , double beta = -0.3);

    /**
     * @brief transform_nodes transform nodes for velues on x-axis (\f$ x \in [-1,-1] \f$ ) to new interval
     * \f$ x \in [c,d] \f$.
     * @param c - left boundary of a new interval
     * @param d - right boundary of a new interval
     * @return vector with transfored x-values from interval \f$ [-1,+1] \rightarrow [c, d] \f$
     */
    arma::vec transform_nodes ( double c, double d );

    /**
     * @brief return_weights - return weights as a vector
     * @return
     */
    arma::vec return_weights();

protected: // CAN/SHOULD BE REWRITTEN INTO CLASS AND OPERATORS OVERLOADING
    /**
     * @brief poly_construction - poly is givem xvec(0) + x*xvec(1) + x^2*xvex(2) + ...
     * @param xi
     * @param xvec
     * @return
     */
    double poly_construction ( double xi, arma::colvec xvec);


    /**
     * @brief Gaussquad::polynom_dotproduct - polynomial multiplication
     * @param p1
     * @param p2
     * @return
     */
    arma::colvec polynom_dotproduct (arma::colvec p1, arma::colvec p2);

    /**
     * @brief pol_dotproduct
     * @param xvec
     * @param yvec
     * @return
     */
    double pol_dotproduct ( arma::colvec xvec, arma::colvec yvec);

    /**
     * @brief add_polynoms
     * @param xvec
     * @param yvec
     * @return
     */
    arma::colvec add_polynoms  ( arma::colvec xvec, arma::colvec yvec);

    /**
     * @brief substract_polynoms
     * @param xvec
     * @param yvec
     * @return
     */
    arma::colvec substract_polynoms  ( arma::colvec xvec, arma::colvec yvec);

    /**
     * @brief legendre_pol
     * @param m
     * @return
     */
    arma::colvec legendre_pol ( unsigned int m);

    /**
     * @brief poly_derivative
     * @param xvec - vector to differentiate
     * @param n - order of the derivative
     * @return \f$  \frac{\partial^n f(x)}{\partial x^n} \f$ in vector form
     */
    arma::colvec poly_derivative( arma::colvec xvec, unsigned n );

    /**
     * @brief nodes_weights - compute nodes and weights for Gaussian Quadrature rule!
     * Generate nodes as roots of given Legendre polynomial of degree n. Nodes are given
     * from equation \f$ w_i = \frac{2}{(1-x_i^2)(P^\prime_n (x_i))^2} \f$ or
     * \f$ w_i = \frac{2(1-x_i^2)}{(n+1)^2 \cdot (P_{n+1} (x_i))^2} \f$. The weights \f$ w_i \f$
     * satisfy \f$ \sum \limits_{i=1}^n  w_i = 2\f$
     * @param n
     * @return
     */

private:
    /**
     * @brief nodes_weights
     * @param n
     * @return
     */
    arma::mat nodes_weights(unsigned n);

    arma::vec besselJ0; // roots of the Bessel function of the first kind \f$ J_0 (x) \f$
    arma::vec besselJalpha;  // roots of the Bessel function of the first kind \f$ J_\alpha (x) , \alpha = 0.1 \f$

    ///< VARIABLES
    double a = -1.; ///< interval where, the Legendre polynomials are defined \f$ x \in [-1 , 1] \f$ , where \textit{a=-1}
    double b =  1.; ///< interval where, the Legendre polynomials are defined \f$ x \in [-1 , 1] \f$ , where \textit{b=+1}
    arma::mat nw; ///< nw = nodes and weights
                  ///< matrix with [x_i  w_i] values

};
#endif // GAUSSQUAD_H
