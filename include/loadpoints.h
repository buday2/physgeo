#ifndef LOADPOINTS_H
#define LOADPOINTS_H

#include <iomanip>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <ostream>
#include <map>
#include <vector>
#include <cstring>
#include <string>
#include <cctype>
#include <cstdio>

#include <unistd.h>
#include <armadillo>

using namespace std;

namespace load_p{

inline bool f_exists (const string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}

inline int first_char (const char* c){
int i = 0;
    while (c[i]){
        if ( isspace(c[i])){
            i++;
        } else {
            return i;
        }
    }
}

    template <typename Any, typename Typ>
    void load_points (map< Any, vector<Typ> >& points,
                      string path,
                      unsigned int noc ) {
        /* path where to locate txt file with data (coordinates)
         * noc - number of columns a want to load into map
         * if the first character of line is '#' - skip
         * format of intput - point_ID  X1  X2  ....    X_noc
         */
        /* check if file really exists */
        //map < Any, vector<Typ> > points;
        vector<Typ> points_vec;
        vector<string> vs;
        string word, line;
        Any point_id;
        int st;

        bool t = f_exists( path.c_str());

        if ( t == false ){
            cout << "File \"" << path.c_str() << "\" does not exist." << endl;
            return;
        } else {
            ifstream f_in ( path.c_str());
            while ( getline( f_in, line)){
                st = first_char( line.c_str() );
                if ( line[st] == '#' ){
                    continue; // commented lines
                } else {
                    istringstream is (line);
                    while ( is >> word )
                        vs.push_back( word );
                }

                for ( unsigned int i=1; i<= noc; i++){
                    points_vec.push_back( (Typ) stod(vs[i].c_str()  ));
                }

                point_id =   stod( vs[0] ) ;
                points[point_id] = points_vec;

                vs.clear();
                points_vec.clear();
            }
        }
    }

    template <typename Typ>
    void load_points ( map< string, vector<Typ> >& points,
                       const string& path,
                       unsigned int noc ) {
        /* path where to locate txt file with data (coordinates)
         * noc - number of columns a want to load into map
         * if the first character of line is '#' - skip
         * format of intput - point_ID  X1  X2  ....    X_noc
         */
        /* check if file really exists */
        // map < string, vector<Typ> > points; // rewritten to void format
        points.clear();
        vector<Typ> points_vec;
        vector<string> vs;
        string word, line;
        //string point_id;
        int st;

        bool t = f_exists( path.c_str());

        if ( t == false ){
            cout << "File \"" << path.c_str() << "\" does not exist." << endl;
            return;
        } else {
            ifstream f_in ( path.c_str());
            while ( getline( f_in, line)) {
                st = first_char( line.c_str() );
                if ( line[st] == '#' ){
                    continue; // commented lines
                } else {
                    istringstream is (line);
                    while ( is >> word ) {
                        vs.push_back( word );
                    }
                }

                Typ var;
                if (  typeid(var) == typeid(string) ) {
//                    for ( unsigned int i=1; i<= noc; i++){
//                        points_vec.push_back( vs[i] );
//                    }
                } else {
                    for ( unsigned int i=1; i<= noc; i++){
                        try {
                            points_vec.push_back( (Typ) stold(vs[i]) );
                        } catch ( const runtime_error& e ) {
                            cout << "Ups. Something went wrong while loading data\n";
                            cout << e.what() << endl;
                            points_vec.push_back( 1./0. );
                        } catch (...) {
                            cout << "Function \" load_points ( map< string, vector<Typ> >& points,)" << endl;
                            cout << "                          string path," << endl;
                            cout << "                          unsigned int noc )\"" << endl;
                            cout << "Failed to load data! Function aborted!!" << endl;
                            return;
                        }
                    }
                }

                string key_word = vs[0];
                //points.emplace( key_word , points_vec );
                points[key_word] = points_vec;

                vs.clear();
                points_vec.clear();
            }
        }
    }

    template<typename Any, typename Typ>
    void print_points (map < Any,vector<Typ> > points, unsigned int p = 10 ) {
        /* print map loaded with load_points(...) function
         * p - number of decimal places
         */
        int vl; // vector length
        for (typename map < Any,vector<Typ> >::iterator it=points.begin();
                                                it!=points.end();
                                                it++){
            vl = (it->second).size();
            cout << setw(p+2) << it->first;
            for (unsigned i=0; i<vl; i++){
                cout << "\t" << setw(p+2) << setprecision(p) << it->second[i];
            }
            cout << endl;
        };
    }


    template<typename Typ>
    void print_points (map < string, vector<Typ> > points,
                       unsigned int p = 10 ) {
        /* print map loaded with load_points(...) function
         * p - number of decimal places
         */
        unsigned int vl; // vector length

        for (typename map < string, vector<Typ> >::iterator it=points.begin();
                                                it!=points.end();
                                                it++){
            vl = (it->second).size();
            cout<< it-> first;
            for ( unsigned int i=0; i<vl; i++) {
                cout<< "\t" << setw(p+2) << setprecision(p) << it->second[i];
            }
            cout << endl;
        };
    }

    template<typename Any, typename Typ>
    void print_points2file ( map<Any , vector<Typ> > points, const char* filename, unsigned int p,
                            const char* rewrite ){
        /* print map to file, you can rewrite the old file or append the map to it
         * rewrite is option by default, for append rewrite = "a"
         */
        ofstream f;
        string report = (string("reports/")+(filename)).c_str();

        if ( strcmp(rewrite, "a" )== 0){
            f.open( report, ios::app);
        } else {
            f.open( report, ios::ate);
        }
        int vl; // vector length

        for (typename map < Any,vector<Typ> >::iterator it=points.begin();
                                                it!=points.end();
                                                it++){
            vl = (it->second).size();
            f << setw(p+2) << it->first;
            for (unsigned i=0; i<vl; i++){
                f << "\t" << setw(p+2) << setprecision(p) << it->second[i];
            };
            f << endl;
        };
        f.close();
    }

    template<typename Any, typename Typ>
    void print_points2file ( map<Any , vector<Typ> > points, const char* filename, unsigned int p){
        /* print map to file, you can rewrite the old file or append the map to it
         * rewrite is option by default, for append rewrite = "a"
         */
        const char* r="r";
        print_points2file(points, filename,p,r);
    }

    template<typename Any, typename Typ>
    void print_points2file ( map<Any , vector<Typ> > points, const char* filename){
        /* print map to file, you can rewrite the old file or append the map to it
         * rewrite is option by default, for append rewrite = "a"
         */
        const char* r="r";
        print_points2file(points, filename,10,r);
    }
} // end namespace load_p::
#endif // LOADPOINTS_H
