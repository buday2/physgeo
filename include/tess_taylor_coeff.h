#ifndef TESS_TAYLOR_COEFF_H
#define TESS_TAYLOR_COEFF_H
#include <cmath>

using namespace std;

namespace tess_t {
/* Coefficients of the taylor polynomial for:
    Optimized tesseroid formulas based on Cartesian integral kernels
    based on article:

    Optimized formulas for the gravitational field of a tesseroid
    Thomas Grombein · Kurt Seitz · Bernhard Heck
    Journal of Geodesy (2013) 87, 645-660
    DOI 10.1007/s00190-013-0636-1

    dx1 = r' \sin \psi \cos \alpha
    dx2 = r' \sin \psi \sin \alpha
    dx3 = r' \cos \psi − r

    \sin \psi \cos \alpha = \cos \varphi \sin \varphi' − \sin \varphi \cos
   \varphi' \cos \delta \lambda
    \sin \psi \sin \alpha = \cos \varphi \sin \delta \lambda

    l^2 = sum dx1_i^2

    \alpha_ij = second derivate of the 1/l^2 with respect to the coordinate
   system

    \beta_i, \beta_ik, \beta_ijk - coefficients of a taylor polynomial
                                   where ij are the coordinates indexes and
                                   k is index for derivative according to the
   independed variable
                                   k=1 is for r, k=2 is for \phi, k=3 is for
   \lambda

    \gamma_i, \gamma_ik, gamma_ijk - detto

    functions such as \beta_ijk = \beta_jik are symmetrical for i,j indexes

    inputs for all functions are:
        double r1, double phi1, double lambda1,double r2, double phi2, double
   lambda2

        where:
            r_i     are in units of metres
            \phi_i  are in units of radians
            \lambda are in units of radians

    all these functions are needed for Tesseroid Class
*/
double dx1(double r1,
           double phi1,
           double lambda1,
           double r2,
           double phi2,
           double lambda2);

double dx2(double r1,
           double phi1,
           double lambda1,
           double r2,
           double phi2,
           double lambda2);

double dx3(double r1,
           double phi1,
           double lambda1,
           double r2,
           double phi2,
           double lambda2);

double spherical_lenght( double r1,
                         double phi1,
                         double lambda1,
                         double r2,
                         double phi2,
                         double lambda2);

double length_sphere(double r1,
                     double phi1,
                     double lambda1,
                     double r2,
                     double phi2,
                     double lambda2);

double length_sphere_sq(double r1,
                                double phi1,
                                double lambda1,
                                double r2,
                                double phi2,
                                double lambda2) ;
double cos_psi ( double phi1,
                 double lambda1,
                 double phi2,
                 double lambda2 );

double k_pq (double r1,
             double phi1,
             double lambda1,
             double r2,
             double phi2,
             double lambda2);

double l1_pq (double r1,
             double phi1,
             double lambda1,
             double r2,
             double phi2,
             double lambda2);

double l2_pq (double r1,
             double phi1,
             double lambda1,
             double r2,
             double phi2,
             double lambda2);

double l3_pq (double r1,
             double phi1,
             double lambda1,
             double r2,
             double phi2,
             double lambda2);

double n11_pq ( double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);

double n12_pq ( double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);

double n13_pq ( double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);

double n21_pq ( double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);

double n22_pq ( double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);

double n23_pq ( double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);

double n31_pq ( double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);

double n32_pq ( double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);

double n33_pq ( double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);

double alpha_11(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double alpha_12(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double alpha_13(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double alpha_21(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double alpha_22(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double alpha_23(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double alpha_31(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double alpha_32(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double alpha_33(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);

double beta_1(double r1,
              double phi1,
              double lambda1,
              double r2,
              double phi2,
              double lambda2);
double beta_2(double r1,
              double phi1,
              double lambda1,
              double r2,
              double phi2,
              double lambda2);
double beta_3(double r1,
              double phi1,
              double lambda1,
              double r2,
              double phi2,
              double lambda2);

double beta_11(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double beta_12(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double beta_13(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double beta_21(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double beta_31(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double beta_22(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double beta_23(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double beta_32(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double beta_33(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);

double  beta_111(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_112(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_113(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_121(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_122(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_123(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_131(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_132(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_133(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_211(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_212(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_213(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_221(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_222(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_223(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_231(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_232(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_233(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_311(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_312(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_313(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_321(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_322(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_323(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_331(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_332(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double  beta_333(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);

double gamma_1(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double gamma_2(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);
double gamma_3(double r1,
               double phi1,
               double lambda1,
               double r2,
               double phi2,
               double lambda2);

double gamma_11(double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);
double gamma_12(double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);
double gamma_13(double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);
double gamma_21(double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);
double gamma_22(double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);
double gamma_23(double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);
double gamma_31(double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);
double gamma_32(double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);
double gamma_33(double r1,
                double phi1,
                double lambda1,
                double r2,
                double phi2,
                double lambda2);

double gamma_111(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_112(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_113(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_121(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_122(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_123(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_131(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_132(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_133(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_211(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_212(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_213(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_221(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_222(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_223(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_231(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_232(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_233(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_311(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_312(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_313(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_321(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_322(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_323(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_331(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_332(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
double gamma_333(double r1, double phi1, double lambda1,double r2, double phi2, double lambda2);
}

#endif // TESS_TAYLOR_COEFF_H
