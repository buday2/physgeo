#include <iostream>
#include <iomanip>
#include <cmath>

#ifndef DMS_H
#define DMS_H

using namespace std;


class dms {
    public:
        dms(double s_deg, double s_min, double s_sec);
        dms();
        dms(double degformat);
        //~dms();

        void deg2dms(double deg);

        double dms2deg();
        double dms2rad();
        double dms2grad();

        void rad2dms(double rad);
        // Operator overloading
        dms operator+(const dms& angle2) const;
        dms operator+=(const dms& angle2) const;
        dms operator-(const dms& angle2) const;
        dms operator*(const dms& angle2) const;
        dms operator/(const dms& angle2) const;

        friend ostream& operator<<(ostream& stream, dms angle) {
          string signum = (angle.sign == 1) ? "+" : "-";

          stream << signum << angle.deg << "  " << setw(3) << angle.minut << "  "
                 << angle.sec << " ";
          return stream;
        }

    private:
        int sign;      ///< Sign of the [d m s] type = {+1, -1}
        double deg;    ///< Degrees
        double minut;  ///< Minutes
        double sec;    ///< seconds

};


#endif // DMS_H
