#ifndef TESSQUAD_H
#define TESSQUAD_H

#include <cmath>
#include <iomanip>
#include <istream>


#include <armadillo>

#include "my_vector.h"
#include "physical_constants.h"

//static int num_of_splits = 1;

using namespace std;

/**
 * @brief The Tessquad class - class created for numerical integration of tesseroids with Gaussian quadrature algorithm.
 */
class Tessquad
{
public:
    Tessquad(double c_r , double c_brad, double c_lrad, unsigned n = 15 );

    /**
     * @brief int2d - integrate surface with 'density' function double (Tessquad::*gravfxy)(double, double, double),
     * For example the function can be \f$ V(\varphi, \lambda) = \iint \limits_\Omega \frac{1}{l(x_1, x_2, y_1, y_2 , \varphi, \lambda)} \mathrm{d}\Omega \f$
     * @param x1 - lower bound of the integration for x variable
     * @param x2 - upper bound of the integration for x variable
     * @param y1 - lower bound of the integration for y variable
     * @param y2 - upper bound of the integration for y variable
     * @param r0 - geocentric length for the surface thats beign integrated
     * @param rho - density of the condensation layer
     * @param kappa - Newton's gravity constant
     * @param tess_ratio - split ratio for the condition \f$ \Delta x_i < tessratio \cdot l \f$, where \textit{l} is distance between running points of integration
     * and the geoemetric center of the tesseroid.
     * @return \f$ \int \limits_{x_1}^{x_2} \int \limits_{y_1}^{y_2} f(x,y) \mathrm{d} y \mathrm{d} x  \f$
     */
    double int2d ( double (Tessquad::*gravfxy)(double, double, double), double b1, double b2,
                                                                        double l1, double l2,
                                                                        double r0, double rho, double kappa, double tess_ratio);
    /**
     * @brief int3d - the runnig point of the integration is \f$ (r, \varphi, \lambda ) \f$ , @see phi, @see lam, @see r .The integration function
     * Tessquad::*gravfxyz)(double, double,double) can represents the gravity potential or it's derivatives. See other functions/methods in this class.
     * @param r1 - upper layer (normal height/orthometric height or ellipsoidal height)
     * @param r2 - lower layer (normal height/orthometric height or ellipsoidal height)
     * @param b1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
     * @param b2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
     * @param l1 - \f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
     * @param l2 - \f$ \lambda_2 \f$ east boundary (longitude) of a tesseroid
     * @param rho - density of the tesseroid
     * @param kappa - Newton's gravity constant
     * @param tess_ratio - split ratio for the condition \f$ \Delta x_i < tessratio \cdot l \f$, where \textit{l} is distance between running points of integration
     * and the geoemetric center of the tesseroid.
     * @return \int\limits _{\lambda_{1}}^{\lambda_{2}}\int\limits _{\varphi_{1}}^{\varphi_{2}}\int\limits _{r_{1}}^{r_{2}}f(r^{\prime},\varphi^{\prime},\lambda^{\prime})\mathrm{{d}}r^{\prime}\mathrm{{d}}\varphi^{\prime}\mathrm{{d}}\lambda^{\prime}
     */
    double int3d (double (Tessquad::*gravfxyz)(double, double,double), double r1, double r2,
                                                                       double b1, double b2,
                                                                       double l1, double l2,
                                                                       double rho, double kappa, double tess_ratio);

    /**
     * @brief potential_u
     * @param rp
     * @param bp
     * @param lp
     * @return
     */
    double potential_u( double rp, double bp, double lp );

    /**
     * @brief gravity_r
     * @param rp - geocentric radius of point inside tesseroid ( point P)
     * @param bp - geodetic latitude of P
     * @param lp - geodetic longitude of P
     * @return \f$ \frac{\partial V}{\partial r} \f$
     */
    double gravity_r( double rp, double bp, double lp );

    /**
     * @brief gravity_phi
     * @param rp - geocentric radius of point inside tesseroid ( point P)
     * @param bp - geodetic latitude of P
     * @param lp - geodetic longitude of P
     * @return \f$ \frac{\partial V}{\partial \vaprhi} \f$
     */
    double gravity_phi( double rp, double bp, double lp );

    /**
     * @brief gravity_lambda
     * @param rp - geocentric radius of point inside tesseroid ( point P)
     * @param bp - geodetic latitude of P
     * @param lp - geodetic longitude of P
     * @return \f$ \frac{\partial V}{\partial \lambda} \f$
     */
    double gravity_lambda( double rp, double bp, double lp );

    /**
     * @brief marussi_rr
     * @param rp
     * @param bp
     * @param lp
     * @return
     */
    double marussi_rr( double rp, double bp, double lp );

    /**
     * @brief marussi_bb
     * @param rp
     * @param bp
     * @param lp
     * @return
     */
    double marussi_bb( double rp, double bp, double lp );
    /**
     * @brief marussi_ll
     * @param rp
     * @param bp
     * @param lp
     * @return
     */
    double marussi_ll( double rp, double bp, double lp );

    /**
     * @brief marussi_rb
     * @param rp
     * @param bp
     * @param lp
     * @return
     */
    double marussi_rb( double rp, double bp, double lp );

    /**
     * @brief marussi_rl
     * @param rp
     * @param bp
     * @param lp
     * @return
     */
    double marussi_rl( double rp, double bp, double lp );

    /**
     * @brief marusssi_bl
     * @param rp
     * @param bp
     * @param lp
     * @return
     */
    double marussi_bl( double rp, double bp, double lp );

    friend ostream& operator<<(ostream& stream, Tessquad tess) {
        stream << "====================================================================\n";
        stream << "phi [rad]   : " << tess.phi << endl;
        stream << "lambda [rad]: " << tess.lam << endl;
        stream << "h_ell [m]   : " << tess.hel << endl;
        stream << "r [m]       : " << tess.r << endl;
        stream << "matrix size : " << tess.degree << endl;
        stream << "============== nodes and weights ====================================\n";
      return stream;
    }
private:
    double phi; ///< geodetic latitude in radians
    double lam; ///< geodetic longitude in radians
    double hel; ///< ellipsoidal height in meetres
    double r; ///< geocentric radius
    unsigned degree;

    arma::mat nw; ///< matrix with nodes and weights
};

#endif // TESSQUAD_H
