#ifndef MY_PARSER_H
#define MY_PARSER_H

#include <iomanip>
#include <iostream>
#include <cmath>
#include <fstream>
#include <ostream>
#include <sstream>
#include <unordered_map>
#include <stdexcept>
#include <algorithm>
#include <clocale>
#include <omp.h>
#include <string>
#include <vector>


// Including Armadillo and preprocessor
#include <hdf5.h>
#define ARMA_USE_CXX11
#define ARMA_USE_OPENMP
#define ARMA_USE_HDF5
#include <armadillo>

// My PhysGeo Lib
#include "geodetic_functions.h"
#include "ggfm.h"
#include "ggfm_meat.h"
#include "tesseroid.h"
#include "lvec.h"
#include "physical_constants.h"
#include "points.h"
#include "progressbar.h"
#include "constants.h" // this file is generated via cmake
#include "timer.h"


//  Boost
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include <boost/filesystem/path.hpp>



namespace po = boost::program_options;

class my_parser
{
public:
    my_parser();
    virtual ~my_parser();

    int execute(const int& argc, char* argv[]);

protected:
    // output values
    const size_t ERROR_IN_COMMAND_LINE = 1;
    const size_t SUCCESS = 0;
    const size_t ERROR_UNHANDLED_EXCEPTION = 2;


    template <typename eT>
    /**
     * @brief search in vector for the key value
     * 
     * @param vec std::vector container with the data
     * @param key key that is beign looked for
     * @return true if the value is found
     * @return false otherwise
     */
    bool do_vector_contains(const std::vector<eT>& vec, const eT& key)
    {
        if (!vec.empty()) 
        {
            if(std::find(vec.begin(), vec.end(), key) != vec.end()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @brief load_points_form_file
     * @param path
     * @return
     */
    unordered_multimap<string, vector<double> > load_points_form_file(const string &path );

    /**
     * @brief swap_values - swap the values. Take the x value and assign it to the y and viceversa.
     * @param x
     * @param y
     */
    void swap_values( double &x, double &y);

    /**
     * @brief Splits the full path name of the file into suffix and prefix part.
     * Example: filename is input '/home/results.txt', By using reference the
     * preffix is then assigned value '/home/results' and suffix '.txt'
     * @param filename full file name
     * @param preffix return path + preffix
     * @param suffix return only suffix with the dot
     */
    void split_file_name(const std::string& filename, std::string& preffix, std::string& suffix);

    /**
     * @brief In order to GGFM grid computation work properly, it is requied to to have
     * 7 parameters in this specific order:
     * b_min, b_max, l_min, l_max, h_ell, delta_b, delta_l
     * 
     * Check if 7 values were provided. If not, manually asked to enter new values. 
     * Type q to end the program;
     * 
     * @return true if the new 7 values are provided
     * @return false otherwise
     */
    bool check_boundaries_vector();

    /**
     * @brief function that checks if "format" of the tess_dmr model is supported
     * 
     * @param format string to check
     * @param confirmed returned format type, default is set to "isgem"
     * 
     * @return true if the format is supported, false otherwise
     */
    bool handle_model_format(const std::string& format, std::string &confirmed);

    /**
     * @brief converts std::string to the double. 
     * 
     * @param str value to be converted to the <double>
     * @param success reference to bool, "true" if the conversion is successfull, "false" otherwise
     * @return double from converted string, returns 0.0 if the conversion fails.
     */
    double convert_std_string_to_double(const std::string& str, bool & success);

    /**
     * @brief converts std::string to the unsigned. 
     * 
     * @param str value to be converted to the <unsigned>
     * @param success reference to bool, "true" if the conversion is successfull, "false" otherwise
     * @return unsigned from converted string, returns 0 if the conversion fails.
     */
    unsigned convert_std_string_to_unsigned(const std::string& str, bool & success);

    /**
     * @brief parsing the string such as "-90 90 0 360 1 1 0" to vector x = {-90, 90, 0, 360, 1, 1, 0}.
     * Th function tries to parse all the words in string and covert them into double. If any argument 
     * can not be converted it is skipped and ignored.
     * 
     * @param args string in form (example) "-90 90 0 360 1 1 0"
     * @return std::vector<double> x = {-90, 90, 0, 360, 1, 1, 0}
     */
    std::vector<double> parse_boundaries( const std::string & args );

private:
        ///< Various arguments to run the program, all of these are being passed and processed via parser/boost command line options
        double love_number; ///< Value of the Love's number for treating the Earth's crust tides.
        std::string ellipsoid; ///< The reference ellipsoid for the computations. Default is "wgs84"
        std::vector<std::string> extra_data; // Contaning additional data/info if needed
        std::string file_name; ///< path to the file with input
        std::string crd_system; ///< In what coordinate system the coordinates are given to the program. Either ellipsoidal or spherical. 
        std::vector<std::string> computation_type; ///< Specify what computation type should be done. (Can be understood as specifying the module)
        std::string output_file; ///< Specify where the result should be saved, default value is 'result.txt'
        std::string algorithm_option; ///<Specify the algorithm for computing the fnALFs.
        std::vector<std::string> solution_options; ///< stands for the option between "sp", "grid", "ongrid"
        std::vector<std::string> density_options; ///< Specify the options for the FGFM, set the density value.
        std::vector<std::string> radius_options; ///< integration radius (-r option)
        std::string help_option;  ///< optional parameter passed to "help,h" to get further help
        std::string boundary_string; ///< the unparsed string for the --boundaries/-b option. For example "-90 90 0 360 1 1 0"
        std::vector<double> boundaries; ///< vector containing the boundaries for the grid computation
        std::vector<std::string> model_parameters; ///< path to the model such as GGFM, can contain editional parameters
        std::vector<std::string> parameters; ///< Defining what parameters should be computed
        std::string tide_system; ///< Permanent Tide system managment
        std::vector<unsigned int> degree_range; ///< Specify the cutoff min/max degree and order for GGFM
        std::string tell_me_more; ///< Get additional info about the command line option
        std::string extend_data; ///< Specify if the data should be extended when computing the Stokes'/Hotine's integral around the whole globe
        std::vector<std::string> kernel_options; ///<aditional options for kernels, when used to compute Stokes' or Hotine's integral 

        // Variables, objects, structures, pointers that are set by parser and are required for running the PhysGeo program
        std::string tess_model_format = "isgem"; // Model format of the tesseroid model/s for FGFM computations 
        std::string density_model_format = "isgem"; // Model format of the density model/s for FGFM computations 

        geo_f::ellipsoid<double> ell; ///< reference ellipsoid for <double> precision
        geo_f::ellipsoid<long double> ell_ld; ///< reference ellipsoid for <long double> precision



        // Functions that gives back some message
        std::string generate_help_message();
        void generate_physgeo_info();
        void generate_ggfm_module_help();
        void generate_fgfm_module_help();
        void generate_stokes_and_hotine_module_help();
        void get_license();
};

#endif // MY_PARSER_H
