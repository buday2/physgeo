// The MIT License (MIT)

// Copyright (c) 2019 Luigi Pertoldi

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

// ============================================================================
// Very simple progress bar for c++ loops with internal running variable

// Author: Luigi Pertoldi
// Created: 3 dic 2016

// Edited by Michal Buday
//           15.1.2020

// Notes: The bar must be used when there's no other possible source of output
//        inside the for loop


#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <iostream>
#include <chrono>
#include <string>
#include <thread>


class Progressbar {

    public:
      // default destructor
      ~Progressbar()                             = default;

      // delete everything else
      Progressbar           (Progressbar const&) = delete;
      Progressbar& operator=(Progressbar const&) = delete;
      Progressbar           (Progressbar&&)      = delete;
      Progressbar& operator=(Progressbar&&)      = delete;

      // default constructor, must call set_niter later
      Progressbar();
      Progressbar(int n, bool showbar=true);

      // reset bar to use it again
      void reset();
      // set number of loop iterations
      void set_niter(int iter);
      // chose your style
      void set_done_char(const std::string& sym) {done_char = sym;}
      void set_todo_char(const std::string& sym) {todo_char = sym;}
      void set_opening_bracket_char(const std::string& sym) {opening_bracket_char = sym;}
      void set_closing_bracket_char(const std::string& sym) {closing_bracket_char = sym;}

      // set name of the loop
      void set_name ( const     std::string& name);
      // to show only the percentage
      void show_bar(bool flag = true) {do_show_bar = flag;}
      // main function
      void update();

    private:

      int progress;             ///<
      int n_cycles;             ///< Total number of the cycles for timed loop
      int last_perc;            ///< last known percetage
      bool do_show_bar;         ///< bool value for showing the bar
      bool update_is_called;    ///<
      //double init_time;         ///< init time for timing the loop

      std::string job_done;     ///< name of the Process
      std::string done_char;
      std::string todo_char;    ///< char used to refill with 'todo_char'
      std::string opening_bracket_char;
      std::string closing_bracket_char;
};
#endif // PROGRESSBAR_H

/* Examples if used solo
 * by Buday

int main() {
    progressbar bar(237);


    #pragma omp parallel for
    for (int i = 0; i < 237; ++i) {
        arma::mat a = arma::randu<arma::mat>(500,500);
        arma::mat b = arma::randn<arma::mat>(500,500);

        arma::mat c = a.i()  * b.t();
        #pragma omp critical
        {
        bar.update();
        }
    }
    return 0;
}
*/
