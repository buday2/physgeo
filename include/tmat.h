#ifndef TMAT_H
#define TMAT_H

#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <exception>
#include <system_error>
#include <string>
#include <cmath>
#include <new>

#include <armadillo>

using namespace std;
/**
 * Class createt for storage of array data for Taylor polynomial expansion in the GGFM
 * model computation. Data are stored in column-ordered one dimensional array.
 */
template<typename eT=double>
class Tmat
{
public:
    Tmat() { /* default */}

    Tmat( const size_t& n, const size_t& m ) {
        this->rows_n = n;
        this->cols_n = m;
        this->elem_n = n*m;

        this->data = new eT[this->elem_n];
    }

    ~Tmat() {
        delete [] this->data;
        this->data = nullptr;
    }

    /**
     * @brief resize - change the size without keeping the elements, and setting all
     * new allcoted memory is set to zero value
     * @param n_new - new number of rows
     * @param m_new - new number of cols
     */
    void resize( size_t n_new , size_t m_new ) {
        this->rows_n = n_new;
        this->cols_n = m_new;
        this->elem_n = n_new*m_new;

        if ( this->data == nullptr ) {
            this->data = new eT[this->elem_n];
        } else {
            delete [] this->data;
            this->data = nullptr;

            this->data = new eT[this->elem_n];
        }

        for ( size_t i = 0;  i < this->elem_n ; i++ ) {
            this->data[i] = static_cast<eT>(0);
        }
    }

    /**
     * @brief col - return the column from @see Tmat as c++ stl vector container
     * @param m - number of the column
     * @return stl vector of the given column
     */
    vector<eT> col( const size_t& m ) const {
        if ( m >= 0 && m < this->cols_n ) {
            vector<eT> vctr( this->rows_n );

            size_t idx = this->return_index(0,m);
            unsigned j = 0;
            for ( size_t i = idx ; i < idx + this->rows_n; i++) {
                vctr[j] = this->data[i];
                j++;
            }

            return vctr;
        } else {
            string err_mes = "Tmat::col() input size_t out of the bounds m=" + to_string(m) +" out from " + to_string(this->cols_n) + " !";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief col - return the column from @see Tmat as c++ stl vector container
     * @param m - number of the column
     * @return stl vector of the given column
     */
    vector<eT> col( const size_t& m ) {
        if ( m >= 0 && m < this->cols_n ) {
            vector<eT> vctr( this->rows_n );

            size_t idx = this->return_index(0,m);
            unsigned j = 0;
            for ( size_t i = idx ; i < idx + this->rows_n; i++) {
                vctr[j] = this->data[i];
                j++;
            }

            return vctr;
        } else {
            string err_mes = "Tmat::col() input size_t out of the bounds m=" + to_string(m) +" out from " + to_string(this->cols_n) + " !";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief col - return the column from @see Tmat as armadillo vector
     * @param m - number of the column
     * @return stl vector of the given column
     */
    arma::vec col_arma( const size_t& m ) const {
        if ( m >= 0 && m < this->cols_n ) {
            arma::vec vctr( this->rows_n );

            size_t idx = this->return_index(0,m);
            unsigned j = 0;
            for ( size_t i = idx ; i < idx + this->rows_n; i++) {
                vctr(j) = static_cast<double>( this->data[i] );
                j++;
            }

            return vctr;
        } else {
            string err_mes = "Tmat::col_arma() input size_t out of the bounds m=" + to_string(m) +" out from " + to_string(this->cols_n) + " !";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief col - return the column from @see Tmat as armadillo vector
     * @param m - number of the column
     * @return stl vector of the given column
     */
    arma::vec col_arma( const size_t& m ) {
        if ( m >= 0 && m < this->cols_n ) {
            arma::vec vctr( this->rows_n );

            size_t idx = this->return_index(0,m);
            unsigned j = 0;
            for ( size_t i = idx ; i < idx + this->rows_n; i++) {
                vctr(j) = static_cast<double>( this->data[i] );
                j++;
            }

            return vctr;
        } else {
            string err_mes = "Tmat::col_arma() input size_t out of the bounds m=" + to_string(m) +" out from " + to_string(this->cols_n) + " !";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief row - convert row from @see Tmat to stl vector container
     * @param n - number of hte row
     * @return stl vector
     */
    vector<eT> row( const size_t& n ) {
        if ( n >= 0 && n < this->rows_n ) {
            vector<eT> vctr( this->cols_n );

            for ( size_t i = 0 ; i < this->cols_n; i++) {
                vctr[i] = *this(n,i);
            }

            return vctr;
        } else {
            string err_mes = "Tmat::row() input size_t out of the bounds n=" + to_string(n) +" out from " + to_string(this->rows_n) + " !";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief row - convert row from @see Tmat to stl vector container
     * @param n - number of hte row
     * @return stl vector
     */
    vector<eT> row( const size_t& n ) const {
        if ( n >= 0 && n < this->rows_n ) {
            vector<eT> vctr( this->cols_n );

            for ( size_t i = 0 ; i < this->cols_n; i++) {
                vctr[i] = this->data[this->rows_n*i + n];  // *this(n,i); // this->rows_n*i + n
            }

            return vctr;
        } else {
            string err_mes = "Tmat::row() input size_t out of the bounds n=" + to_string(n) +" out from " + to_string(this->rows_n) + " !";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief row - convert row from @see Tmat to stl vector container
     * @param n - number of hte row
     * @return stl vector
     */
    vector<eT> row_arma( const size_t& n ) {
        if ( n >= 0 && n < this->rows_n ) {
            arma::vec vctr( this->cols_n );

            for ( size_t i = 0 ; i < this->cols_n; i++) {
                vctr(i) = static_cast<double>( *this(n,i) );
            }

            return vctr;
        } else {
            string err_mes = "Tmat::row_arma() input size_t out of the bounds n=" + to_string(n) +" out from " + to_string(this->rows_n) + " !";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief row - convert row from @see Tmat to stl vector container
     * @param n - number of hte row
     * @return stl vector
     */
    vector<eT> row_arma( const size_t& n ) const {
        if ( n >= 0 && n < this->rows_n ) {
            arma::vec vctr( this->cols_n );

            for ( size_t i = 0 ; i < this->cols_n; i++) {
                vctr(i) = static_cast<double>( *this(n,i) );
            }

            return vctr;
        } else {
            string err_mes = "Tmat::row_arma() input size_t out of the bounds n=" + to_string(n) +" out from " + to_string(this->rows_n) + " !";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief return_index index in raw pointer memory
     * @param n - number of the row
     * @param m - number of the column
     * @return index in raw ptr data
     */
    size_t return_index( const size_t& n , const size_t& m ) {
        if ( n >= 0 && n < this->rows_n && m >= 0 && m < this->cols_n ) {
            return this->rows_n*m + n;
        } else {
            string err_mes = "Tmat::return_index(n,m) indexes out of the bounds !";
            throw runtime_error( err_mes );
        }
    }

    arma::mat convert2arma() {
        arma::mat a_mat( this->rows_n , this->cols_n );

        for ( unsigned i = 0; i < this->rows_n; i++ ) {
            for (unsigned j = 0; j < this->cols_n; j++) {
                a_mat(i,j) = static_cast<double>( this->data[ j*this->rows_n + i ]);
            }
        }

        return a_mat;
    }


    /**
     * @brief operator () - acces (i_row,j_col) element in Tmat
     * @param i - number of the row
     * @param j - number of the column
     * @return
     */
    eT& operator()( const size_t& i , const size_t& j) {
        if ( i >= 0 && i < this->rows_n && j >= 0 && j < this->cols_n ) {
            return this->data[ j*this->rows_n + i ];
        } else {
            string err_mes = "Tmat (operator()) indexes i,j=" + to_string(i) +"," + to_string(j) + " out of the bounds!";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief operator () - acces (i_row,j_col) element in Tmat
     * @param i - number of the row
     * @param j - number of the column
     * @return
     */
    eT& operator()( const size_t& i , const size_t& j) const {
        if ( i >= 0 && i < this->rows_n && j >= 0 && j < this->cols_n ) {
            return this->data[ j*this->rows_n + i ];
        } else {
            string err_mes = "Tmat (operator()) indexes i,j=" + to_string(i) +"," + to_string(j) + " out of the bounds!";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief operator [] - direct access (raw memory) to data
     * @param i
     * @return
     */
    eT& operator[](const size_t& i) {
        if ( i >= 0 && i < this->elem_n ) {
            return this->data[i];
        } else {
            string err_mes = "Tmat (operator[]) index i=" + to_string(i) + " out of the bounds (from " + to_string(this->elem_n) + ")!";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief operator [] - direct access (raw memory) to data
     * @param i
     * @return
     */
    eT& operator[](const size_t& i) const {
        if ( i >= 0 && i < this->elem_n ) {
            return this->data[i];
        } else {
            string err_mes = "Tmat (operator[]) index i=" + to_string(i) + " out of the bounds (from " + to_string(this->elem_n) + ")!";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief operator <<
     * @param stream
     * @return
     */
    friend ostream& operator <<( ostream& stream, const Tmat<eT>& matrix  ){
        for ( size_t n = 0; n < matrix.n_rows() ; n++) {
            for (size_t m = 0; m < matrix.n_cols() ; m++ ) {
                stream << matrix.data[ m*matrix.n_rows() + n ] << " ";
            }
            stream << endl;
        }
        return stream;
    }

    /**
     * @brief n_rows - returns the number of rows in @see Tmat class
     * @return
     */
    const size_t& n_rows() {
        return  this->rows_n;
    }

    /**
     * @brief n_rows - returns the number of rows in @see Tmat class
     * @return
     */
    const size_t& n_rows() const {
        return  this->rows_n;
    }

    /**
     * @brief n_cols - returns the number of columns in @see Tmat class
     * @return
     */
    const size_t& n_cols() {
        return  this->cols_n;
    }

    /**
     * @brief n_cols - returns the number of columns in @see Tmat class
     * @return
     */
    const size_t& n_cols() const {
        return  this->cols_n;
    }

    /**
     * @brief n_elem - returns the total number of elements in @see Tmat class
     * @return
     */
    const size_t& n_elem() {
        return  this->elem_n;
    }

    /**
     * @brief n_elem - returns the total number of elements in @see Tmat class
     * @return
     */
    const size_t& n_elem() const {
        return  this->elem_n;
    }

private:
    eT* data = nullptr;
    size_t rows_n = 0;
    size_t cols_n = 0;
    size_t elem_n = 0;
};

#endif // TMAT_H
