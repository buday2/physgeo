#ifndef GPRISM_H
#define GPRISM_H

#include <iomanip>
#include <iostream>
#include <cmath>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <fstream>

// include my own libraries
#include "geodetic_functions.h"
#include "physical_constants.h"

// Using 'ARMADILLO' Library
#include <armadillo>

using namespace std;

template<typename eT=double>
/**
 * @brief The Gprism class
 */
class Gprism
{
public:
    Gprism(){ /* cstr */};

    /**
     * @brief u
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u (const eT& x,
                 const eT& y,
                 const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  ( x*y*log(z+r)+y*z*log(x+r)+z*x*log(y+r) \
                - x*x/2.0*atan(y*z/(x*r)) \
                - y*y/2.0*atan(z*x/(y*r)) \
                - z*z/2.0*atan(x*y/(z*r)));
    }
    /* ==============================================================
                           FIRST DERIVATIVES
     ============================================================== */

    /**
     * @brief u_x
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_x (const eT& x,
                   const eT& y,
                   const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  ( x*log(y+r) + y*log(x+r) - z*atan(y*x/(z*r)) );
    }


    /**
     * @brief u_y
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_y (const eT& x,
                   const eT& y,
                   const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  ( y*log(z+r) + z*log(y+r) - x*atan(z*y/(x*r)) );
    }


    /**
     * @brief u_z
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_z (const eT& x,
                   const eT& y,
                   const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  ( z*log(x+r) + x*log(z+r) - y*atan(z*x/(y*r)) );
    }

    /* ==============================================================
                        SECOND DERIVATIVES
     ============================================================== */


    /**
     * @brief u_xx
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_xx (const eT& x,
                    const eT& y,
                    const eT& z)
    {
        return  ( -1.0*atan(y*z/(x*sqrt(x*x+y*y+z*z))));
    }

    /**
     * @brief u_yy
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_yy (const eT& x,
                    const eT& y,
                    const eT& z)
    {
        return  ( -1.0*atan(x*z/(y*sqrt(x*x+y*y+z*z))));
    }

    /**
     * @brief u_zz
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_zz (const eT& x,
                    const eT& y,
                    const eT& z)
    {
        return  ( -1.0*atan(y*x/(z*sqrt(x*x+y*y+z*z))));
    }


    /**
     * @brief u_xz
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_xz (const eT& x,
                    const eT& y,
                    const eT& z)
    {
        return  ( log(y + sqrt(x*x+y*y+z*z)) );
    }


    /**
     * @brief u_yz
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_yz (const eT& x,
                    const eT& y,
                    const eT& z)
    {
        return  ( log(x + sqrt(x*x+y*y+z*z)) );
    }


    /**
     * @brief u_xy
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_xy (const eT& x,
                    const eT& y,
                    const eT& z)
    {
        return  ( log(z + sqrt(x*x+y*y+z*z)) );
    }
    /* ==============================================================
                        THIRD DERIVATIVES
     ============================================================== */

    /**
     * @brief u_xxx
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_xxx (const eT& x,
                     const eT& y,
                     const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  (  (y*z/r)*( 1.0/(x*x+z*z)+1.0/(x*x+y*y)) );
    }

    /**
     * @brief u_yyy
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_yyy (const eT& x,
                     const eT& y,
                     const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  (  (x*z/r)*( 1.0/(y*y+z*z)+1.0/(x*x+y*y)) );
    }

    /**
     * @brief u_zzz
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_zzz (const eT& x,
                     const eT& y,
                     const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  (  (x*y/r)*( 1.0/(y*y+z*z)+1.0/(x*x+z*z)) );
    }

    /**
     * @brief u_xyz
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_xyz (const eT& x,
                     const eT& y,
                     const eT& z)
    {
        return  ( 1.0/sqrt(x*x+y*y+z*z) );
    }

    /**
     * @brief u_xxy
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_xxy (const eT& x,
                     const eT& y,
                     const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  ( (z*x/r)*( -1.0/(x*x+y*y)) );
    }

    /**
     * @brief u_xxz
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_xxz (const eT& x,
                     const eT& y,
                     const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  ( (y*x/r)*( -1.0/(x*x+z*z)) );
    }

    /**
     * @brief u_yyx
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_yyx (const eT& x,
                     const eT& y,
                     const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  ( (y*z/r)*( -1.0/(x*x+y*y)) );
    }

    /**
     * @brief u_yyz
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_yyz (const eT& x,
                     const eT& y,
                     const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  ( (y*x/r)*( -1.0/(z*z+y*y)) );
    }

    /**
     * @brief u_zzx
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_zzx (const eT& x,
                     const eT& y,
                     const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  ( (y*z/r)*( -1.0/(z*z+x*x)) );
    }

    /**
     * @brief u_zzy
     * @param x
     * @param y
     * @param z
     * @return
     */
    inline eT u_zzy (const eT& x,
                     const eT& y,
                     const eT& z)
    {
        eT r = sqrt(x*x+y*y+z*z);
        return  ( (y*x/r)*( -1.0/(z*z+y*y)) );
    }
    /* ==============================================================
                          END OF DERIVATIVES
     ============================================================== */
    /**
     * @brief integrate_u
     * @param x1
     * @param x2
     * @param y1
     * @param y2
     * @param z1
     * @param z2
     * @param deriv
     * @return
     */
    inline eT integrate_u (const eT& x1,
                           const eT& x2,
                           const eT& y1,
                           const eT& y2,
                           const eT& z1,
                           const eT& z2,
                           const char* deriv)
    {
        /*
         *
         *
         */
        eT x[2] = {x1, x2};
        eT y[2] = {y1, y2};
        eT z[2] = {z1, z2};

        eT integral =  0;
        eT pm_sign; // plus/minus sign

        if ( (strcmp( deriv, "")== 0)  || (strcmp( deriv, "none")== 0)  || (strcmp( deriv, "-")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "x")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_x(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "y")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_y(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "z")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_z(x[i],y[j],z[k]);
                    }
                }
            } // End  of the first derivatives
        } else if ( (strcmp( deriv, "xy")== 0) || ( strcmp( deriv, "yx")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xy(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( (strcmp( deriv, "xz")== 0) || ( strcmp( deriv, "zx")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xz(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( (strcmp( deriv, "yz")== 0) || ( strcmp( deriv, "zy")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yz(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "xx")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xx(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "yy")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yy(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "zz")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_zz(x[i],y[j],z[k]);
                    }
                }
            } // End  of the second derivatives
        } else if ( strcmp( deriv, "xxx")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xxx(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "yyy")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yyy(x[i],y[j],z[k]);
                    }
                }
            }
        } else if ( strcmp( deriv, "zzz")== 0 ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_zzz(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "xyz")== 0) || (strcmp( deriv, "xzy")== 0)
                   || (strcmp( deriv, "yzx")== 0) || (strcmp( deriv, "yxz")== 0)
                   || (strcmp( deriv, "zxy")== 0) || (strcmp( deriv, "zyx")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xyz(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "xxy")== 0) || (strcmp( deriv, "xyx")== 0)
                   || (strcmp( deriv, "yxx")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xxy(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "yyx")== 0) || (strcmp( deriv, "yxy")== 0)
                   || (strcmp( deriv, "xyy")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yyx(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "xxz")== 0) || (strcmp( deriv, "xzx")== 0)
                   || (strcmp( deriv, "zxx")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_xxz(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "yyz")== 0) || (strcmp( deriv, "yzy")== 0)
                   || (strcmp( deriv, "zyy")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yyz(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "zzx")== 0) || (strcmp( deriv, "zxz")== 0)
                   || (strcmp( deriv, "xzz")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yy(x[i],y[j],z[k]);
                    }
                }
            }
        } else if (   (strcmp( deriv, "zzy")== 0) || (strcmp( deriv, "zyz")== 0)
                   || (strcmp( deriv, "yzz")== 0) ){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++ ){
                    for (int k=0; k<2; k++){
                        pm_sign = ( (i+j+k+3)%2==0 ) ? 1.0 : -1.0;
                        integral += pm_sign*u_yy(x[i],y[j],z[k]);
                    }
                }
            }
        }
        return integral;
    }
};

#endif // GPRISM_H
