#ifndef TESSEROID_H
#define TESSEROID_H

#include <cmath>
#include <iostream>
#include <map>
#include <algorithm>
#include <initializer_list>
#include <vector>

#include <armadillo>

#include "tess_taylor_coeff.h"
#include "physical_constants.h"
#include "geodetic_functions.h"
#include "timer.h"
#include "progressbar.h"
#include "my_vector.h"
#include "omp.h"
#include "isgemgeoid.h"

namespace tess {
    struct header {
        string model_name = "unknown";
        string model_type = "unknown";
        string units      = "unknown";
        string ref_ell    = "unknown";
        string isg_format = "unknown";
        double lat_min;
        double lat_max;
        double lon_min;
        double lon_max;
        double nodata = -9999.999;
        double delta_lat;
        double delta_lon;
        unsigned nrows;
        unsigned ncols;
    };
}



/**
 * @brief The Tesseroid class can be used for forward gravity field modelling from digital elevation model (DEM).
 * The neccesssary data are DEM in various format (@see load_model_from_ascii ). The gravity quantities are computed
 * between two levels (@see upper_lay, @see lower_lay) with constant density. The following quantities can be obtained:
 * -# \f$ V(r,\varphi,\lambda)=\int\limits _{\lambda_{1}}^{\lambda_{2}}\int\limits _{\varphi_{1}}^{\varphi_{2}}\int\limits _{r_{1}}^{r_{2}}\dfrac{r^{\prime2}\cos\varphi^{\prime}}{\sqrt{r^{2}+r^{\prime2}-2rr^{\prime}\cdot(\sin\varphi\sin\varphi^{\prime}+\cos\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda))}}\mathrm{{d}}r^{\prime}\mathrm{{d}}\varphi^{\prime}\mathrm{{d}}\lambda^{\prime} \f$
 * -# \f$ \dfrac{\partial V}{\partial\varphi}=G\rho\iiint\limits _{\Omega}\dfrac{rr^{\prime}C_{\varphi}}{l^{3}}\mathrm{d}\Omega \f$
 * -# \f$ \dfrac{\partial V}{\partial\lambda}=G\rho\iiint\limits _{\Omega}\dfrac{rr^{\prime}C_{\lambda}}{l^{3}}\mathrm{d}\Omega \f$
 * -# \f$ \dfrac{\partial V}{\partial r}=G\rho\iiint\limits_{\Omega}\dfrac{r^{\prime}\cos\psi-r}{l^{3}}\mathrm{d}\Omega \f$
 * -# \f$ \dfrac{\partial^{2}V}{\partial\varphi^{2}}=G\rho\iiint\limits _{\Omega}\dfrac{rr^{\prime}}{l^{3}}\cdot\left(\dfrac{3rr^{\prime}C_{\varphi}^{2}}{l^{2}}-\cos\psi\right)\mathrm{d}\Omega \f$
 * -# \f$ \dfrac{\partial^{2}V}{\partial\lambda^{2}}=G\rho\iiint\limits _{\Omega}\dfrac{rr^{\prime}}{l^{3}}\cdot\left(\dfrac{3rr^{\prime}C_{\lambda}^{2}}{l^{2}}-\cos\varphi^{\prime}\cos\varphi\cos(\lambda^{\prime}-\lambda)\right)\mathrm{d}\Omega \f$
 * -# \f$ \dfrac{\partial^{2}V}{\partial r^{2}}=G\rho\iiint\limits _{\Omega}\dfrac{1}{l^{3}}\cdot\left(\dfrac{3(r^{\prime}\cos\psi-r)^{2}}{l^{2}}-1\right)\mathrm{d}\Omega \f$
 * -# \f$ \dfrac{\partial^{2}V}{\partial\varphi\partial\lambda}=G\rho\iiint\limits _{\Omega} \f$
 * -# \f$ \dfrac{rr^{\prime}\cos\varphi^{\prime}\sin(\lambda^{\prime}-\lambda)}{l^{3}}\cdot\left(\dfrac{3rr^{\prime}C_{\varphi}\cos\varphi}{l^{2}}-\sin\varphi\right)\mathrm{d}\Omega \f$
 * -# \f$ \dfrac{\partial^{2}V}{\partial\varphi\partial r}=G\rho\iiint\limits _{\Omega}\dfrac{r^{\prime}C_{\varphi}}{l^{3}}\cdot\left(\dfrac{3r(r^{\prime}\cos\psi-r)}{l^{2}}+1\right)\mathrm{d}\Omega \f$
 * -# \f$ \dfrac{\partial^{2}V}{\partial\lambda\partial r}=G\rho\iiint\limits _{\Omega}\dfrac{r^{\prime}C_{\lambda}}{l^{3}}\cdot\left(\dfrac{3r(r^{\prime}\cos\psi-r)}{l^{2}}+1\right)\mathrm{d}\Omega \f$
 *
 * Computations are based on "The gravity gradients can be calculated using the general formula of Grombein et al. (2010)."
 */
class Tesseroid {
    /// TODO (#mbuday#1#) : save output to file
    /// TODO (#mbuday#2#) : add #pragma to specific loops
    public:
        Tesseroid(); // Constructor
        virtual ~Tesseroid(); // destructor

        /**
         * @brief copy_header header info from struct @see tess::header
         * @param hinfo
         */
        void copy_header( const tess::header & hinfo );

        /**
         * @brief compare_header
         * @param hinfo header info
         * @param function - name of the function where is the class member called
         * @return
         */
        bool compare_header(  const tess::header &hinfo, const string& function);

        template<typename eT>
        /**
         * @brief get_header
         * @param nrows
         * @param ncols
         * @return
         */
        vector<eT> get_header( unsigned& nrows, unsigned& ncols) const {
            vector<eT> header(7); // bmin, bmax, lmin, lmax, deltab, deltal, nodata

            header[0] = static_cast<eT>( this->lat_min ); // minimum value of the latitude
            header[1] = static_cast<eT>( this->lat_max ); // maximum value of the latitude
            header[2] = static_cast<eT>( this->lon_min ); // minimum value of the longitude
            header[3] = static_cast<eT>( this->lon_max ); // maximum value of the longitude
            header[4] = static_cast<eT>( this->delta_lat ); // latitude step
            header[5] = static_cast<eT>( this->delta_lon ); // longitude step
            header[6] = static_cast<eT>( this->nodata );    // no data value

            nrows = this->upper_lay.n_rows;
            ncols = this->upper_lay.n_cols;

            return  header;
        }

        /**
         * @brief load_model_from_ascii
         * @param path
         * @param format
         * @param f_model_name
         * @param f_model_type
         * @param f_units
         * @param f_ref_ell
         * @param f_isg_format
         * @param nodata_value
         * @return
        *
        * b - geodetic latitude
        * l - geodetic longitude
        * hu_ell - upper layer of the "tesser"
        * hl_ell - lower layer

        * 0 - PointID b l hu_el hl_el
        * 1 - PointID l b hu_el hl_el
        * 2 - PointID b l hu_ell
        * 3 - b l hu_ell hl_ell
        * 4 - l b hu_ell hl_ell
        * 5 - b l hu_ell
        * 6 - l b hu_ell
         */
        bool load_model_from_ascii( const char* path,
                                    unsigned int format,
                                    const char* f_model_name = "Unknown",
                                    const char* f_model_type = "Unknown",
                                    const char* f_units = "m",
                                    const char* f_ref_ell = "Unknown",
                                    const char* f_isg_format = "1.0",
                                    double nodata_value = -9999.999);

        /**
         * @brief export2ascii
         * @param path
         * @param ptid
         * @return
         */
        bool export2csv( const string& path, bool ptid );

        /**
         * @brief load_isgem_model
         * @param path
         * @return
         */
        bool load_isgem_model( const string& path );

        /**
         * @brief set_lower_layer
         * @param path
         * @return
         */
        bool set_lower_layer ( const string& path );

        /**
         * @brief save_binary_file
         * @param fname
         */
        void save_binary_file( const string& fname );

        /** Print model
        *
        */
        void print_model();


        /**
         * @brief load_layer from file on the hard drive
         * @param path path to the file
         * @param data_format -
         * -# "isgem" for modified isgem format
         * -# "ascii" for classic storage of data in txt file
         * -# "others to come"...... #TODO#!!
         * @param layer that is being loaded
         * -# 1 for lower layer
         * -# otherwise it's loading the upper layer
         * @param col_order - format of the input
         * -# 0 - PointID b l hu_el hl_el
         * -# 1 - PointID l b hu_el hl_el
         * -# 2 - PointID b l hu_ell
         * -# 3 - b l hu_ell hl_ell
         * -# 4 - l b hu_ell hl_ell
         * -# 5 - b l hu_ell
         * -# 6 - l b hu_ell
         * @return true if the process was successful, false otherwise
         */
        bool load_layer(const string& path,
                        const string& data_format,
                        const int& layer,
                        const int& col_order,
                        bool overwrite = true);


        /**
         * @brief set_print_coeff set if the ceofficients of the model should be passed to ofstream <<
         * @param in_print_coeff
         */
        void set_print_coeff ( bool in_print_coeff);

        friend ostream& operator<<(ostream& stream, Tesseroid model) {
            stream << "begin_of_head ================================================\n";
            stream << "model name : " << model.model_name << endl;
            stream << "model type : " << model.model_type << endl;
            stream << "units      : " << model.units << endl;
            stream << "ref_ell    : " << model.ref_ell << endl;
            stream << "ISG format = " << model.isg_format << endl;
            stream << "lat min    = " << model.lat_min << endl;
            stream << "lat max    = " << model.lat_max << endl;
            stream << "lon min    = " << model.lon_min << endl;
            stream << "lon max    = " << model.lon_max << endl;
            stream << "nodata     = " << model.nodata << endl;
            stream << "delta lat  = " << model.delta_lat << endl;
            stream << "delta lon  = " << model.delta_lon << endl;
            stream << "nrows      = " << model.nrows << endl;
            stream << "ncols      = " << model.ncols << endl;
            stream << "end_of_head ==================================================\n";

            if ( model.print_coeff ) {
                arma::mat gmodel_out =  arma::flipud( model.upper_lay );
                gmodel_out.raw_print(stream); // http://arma.sourceforge.net/docs.html#print

                cout << "end_upper_layer ============================================ " <<endl;
                if (  !model.lower_lay.is_empty()  ) {
                    arma::mat gmodel_out_low =  arma::flipud( model.lower_lay );
                    gmodel_out_low.raw_print(stream); // http://arma.sourceforge.net/docs.html#print
                }
            }
          return stream;
        }


        /**!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!* Perform computation on grid data *!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!**/
        /**
         * @brief grav_on_grid
         * @param check_radius
         * @param radius
         * @param kappa
         * @param rho
         * @param ell
         * @return
         */
        arma::mat grav_on_grid (bool check_radius,
                                bool rrun_is_up,
                                double radius,
                                double kappa,
                                double rho,
                                struct geo_f::ellipsoid<double> ell,
                                double (Tesseroid::*gravf)(double,double,double,double,double,double,
                                                    double,double,double,double,double)
                                /*double (*gravf)(double,double,double,double,double,double,
                                      double,double,double,double,double) */);



        /**
         * @brief grav_on_grid #TODO#description
         * instead of the constant density for the whole grid, the density may vary
         * @param check_radius
         * @param rrun_is_up
         * @param radius
         * @param kappa
         * @param rho
         * @param ell
         * @return
         */
        arma::mat grav_on_grid ( bool check_radius,
                                 bool rrun_is_up,
                                 double radius,
                                 double kappa,
                                 const Isgemgeoid &rho,
                                 struct geo_f::ellipsoid<double> ell,
                                 double (Tesseroid::*gravf)(double,double,double,double,double,double,
                                                           double,double,double,double,double));


        /**
         * @brief grav_crd_list
         * @param crdmat
         * @param check_radius
         * @param radius
         * @param kappa
         * @param rho
         * @param ell
         */
        void grav_crd_list ( arma::mat &crdmat, // modified version of grav_on_grid function
                            bool check_radius,
                            double radius,
                            double kappa,
                            double rho,
                            struct geo_f::ellipsoid<double> ell,
                            double (Tesseroid::*gravf)(double,double,double,double,double,double,
                                                       double,double,double,double,double));


        /**
         * @brief grav_crd_list
         * @param crdmat
         * @param check_radius
         * @param radius
         * @param kappa
         * @param rho
         * @param ell
         */
        void grav_crd_list ( arma::mat &crdmat, // modified version of grav_on_grid function
                            bool check_radius,
                            double radius,
                            double kappa,
                            const Isgemgeoid &rho,
                            struct geo_f::ellipsoid<double> ell,
                            double (Tesseroid::*gravf)(double,double,double,double,double,double,
                                                       double,double,double,double,double));

        /**
         * @brief gravity_field
         * @param check_radius
         * @param radius
         * @param kappa
         * @param rho
         * @param ell
         * @param func_list
         * @return
         */
        arma::cube gravity_field( bool check_radius, double radius, double kappa, double rho,
                                    struct geo_f::ellipsoid<double> ell,
                                    initializer_list <double (*) (double,double,double,double,double,double,
                                    double,double,double,double,double) > func_list );

        /**
         * @brief grav_from_tessgrid
         * @param rrun
         * @param brun
         * @param lrun
         * @param check_radius
         * @param radius
         * @param kappa
         * @param rho
         * @param ell
         * @return
         */
        double grav_from_tessgrid( double rrun, double brun, double lrun,
                                bool check_radius, double radius, double kappa, double rho,
                                struct geo_f::ellipsoid<double> ell,
                                double (&gravf)(double,double,double,double,double,double,
                                      double,double,double,double,double) );
        /**
        Functions that calculate the gravitational potential and its first and second
        derivatives for the tesseroid.

        The gravity gradients can be calculated using the general formula of
        Grombein et al. (2010). Integrals are "replaced" by Taylor series.

        To maintain the standard convention, only for component gz the z axis is
        inverted, so a positive density results in positive gz

        Computation is based on the Taylor series approach, the coefficients are stored in
        #include "tess_taylor_coeff.h"/ #include "tess_taylor_coeff.cpp"

         * @brief m_ijk - \f$ r_i \f$ are in units of meetres, \f$ \varphi_i, \lambda_i \f$ are in radians
         * @param r - \f$ r \f$ 'geocentric length' of the point P (running point of an integration)
         * @param phi - \f$ \varphi \f$ geodetic latitude of the point P (running point of an integration)
         * @param lambda -  \f$ \lambda \f$ geodetic longitude of the point P (running point of an integration)
         * @param r1 - \f$ r_1 \f$ lower boundary of a tesseroid
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param r2 - \f$ r_2 \f$ upper boundary of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2  \f$east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @return
         */
        double m_ijk(
            double r,
            double phi,
            double lambda,
            double r1,
            double phi1,
            double lambda1,
            double r2,
            double phi2,
            double lambda2,
            double kappa,
            double rho,
            double ratio,
            double (&k_ijk) (double, double, double, double, double, double),
            double (&alpha_ij)(double, double, double, double, double, double),
            double (&beta1_ijk)(double, double, double, double, double, double),
            double (&beta2_ijk)(double, double, double, double, double, double),
            double (&gamma1_ijk)(double, double, double, double, double, double),
            double (&gamma2_ijk)(double, double, double, double, double, double),
            double (&gamma3_ijk)(double, double, double, double, double, double));


        /**
         * @brief potential_u - \f$ r_i \f$ are in units of meetres, \f$ \varphi_i, \lambda_i \f$ are in degrees
         * @param r - \f$ r \f$ 'geocentric length' of the point P (running point of an integration)
         * @param phi - \f$ \varphi \f$ geodetic latitude of the point P (running point of an integration)
         * @param lambda -  \f$ \lambda \f$ geodetic longitude of the point P (running point of an integration)
         * @param r1 - \f$ r_1 \f$ lower boundary of a tesseroid
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param r2 - \f$ r_2 \f$ upper boundary of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2 \f$ east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @return gravity potential of a tesseroid : \f$ V(r,\varphi,\lambda)=\int\limits _{\lambda_{1}}^{\lambda_{2}}\int\limits _{\varphi_{1}}^{\varphi_{2}}\int\limits _{r_{1}}^{r_{2}}\dfrac{r^{\prime2}\cos\varphi^{\prime}}{\sqrt{r^{2}+r^{\prime2}-2rr^{\prime}\cdot(\sin\varphi\sin\varphi^{\prime}+\cos\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda))}}\mathrm{{d}}r^{\prime}\mathrm{{d}}\varphi^{\prime}\mathrm{{d}}\lambda^{\prime} \f$
         */
        double potential_u (double r, double phi, double lambda, /* running point of the integration */
                            double r1, double phi1, double lambda1, /* boundaries of the tesseroid */
                            double r2, double phi2, double lambda2,
                            double kappa, double rho );

        /**
         * @brief gravity_r - \f$ r_i \f$ are in units of meetres, \f$ \varphi_i, \lambda_i \f$ are in degrees
         * @param r - \f$ r \f$ 'geocentric length' of the point P (running point of an integration)
         * @param phi - \f$ \varphi \f$ geodetic latitude of the point P (running point of an integration)
         * @param lambda -  \f$ \lambda \f$ geodetic longitude of the point P (running point of an integration)
         * @param r1 - \f$ r_1 \f$ lower boundary of a tesseroid
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param r2 - \f$ r_2 \f$ upper boundary of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2  \f$east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @return gravity value, obtained from following equations:
         * -# \f$r^{2}+r^{\prime2}-2rr^{\prime}\cdot(\sin\varphi\sin\varphi^{\prime}+\cos\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\varphi}=\frac{\partial\cos\psi}{\partial\varphi}=\cos\varphi\sin\varphi^{\prime}-\sin\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\lambda}=\frac{\partial\cos\psi}{\partial\lambda}=\cos\varphi\cos\varphi^{\prime}\sin(\lambda^{\prime}-\lambda) \f$
         * -# The final integral is given: \f$ \dfrac{\partial V}{\partial r}=G\rho\iiint\limits _{\Omega}\dfrac{r^{\prime}\cos\psi-r}{l^{3}}\mathrm{d}\Omega \f$
         */
        double gravity_r (double r, double phi, double lambda, /* running point of the integration */
                            double r1, double phi1, double lambda1, /* boundaries of the tesseroid */
                            double r2, double phi2, double lambda2,
                            double kappa, double rho );

        /**
         * @brief gravity_phi - \f$ r_i \f$ are in units of meetres, \f$ \varphi_i, \lambda_i \f$ are in degrees
         * @param r - \f$ r \f$ 'geocentric length' of the point P (running point of an integration)
         * @param phi - \f$ \varphi \f$ geodetic latitude of the point P (running point of an integration)
         * @param lambda -  \f$ \lambda \f$ geodetic longitude of the point P (running point of an integration)
         * @param r1 - \f$ r_1 \f$ lower boundary of a tesseroid
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param r2 - \f$ r_2 \f$ upper boundary of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2  \f$east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @return defleciton of the vertical value \f$ \xi \f$ (in radians), obtained from following equations:
         * -# \f$r^{2}+r^{\prime2}-2rr^{\prime}\cdot(\sin\varphi\sin\varphi^{\prime}+\cos\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\varphi}=\frac{\partial\cos\psi}{\partial\varphi}=\cos\varphi\sin\varphi^{\prime}-\sin\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\lambda}=\frac{\partial\cos\psi}{\partial\lambda}=\cos\varphi\cos\varphi^{\prime}\sin(\lambda^{\prime}-\lambda) \f$
         * -# The final integral is given: \f$ \dfrac{\partial V}{\partial\varphi}=G\rho\iiint\limits _{\Omega}\dfrac{rr^{\prime}C_{\varphi}}{l^{3}}\mathrm{d}\Omega \f$
         */
        double gravity_phi (double r, double phi, double lambda, /* running point of the integration */
                            double r1, double phi1, double lambda1, /* boundaries of the tesseroid */
                            double r2, double phi2, double lambda2,
                            double kappa, double rho );

        /**
         * @brief gravity_lambda - \f$ r_i \f$ are in units of meetres, \f$ \varphi_i, \lambda_i \f$ are in degrees
         * @param r - \f$ r \f$ 'geocentric length' of the point P (running point of an integration)
         * @param phi - \f$ \varphi \f$ geodetic latitude of the point P (running point of an integration)
         * @param lambda -  \f$ \lambda \f$ geodetic longitude of the point P (running point of an integration)
         * @param r1 - \f$ r_1 \f$ lower boundary of a tesseroid
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param r2 - \f$ r_2 \f$ upper boundary of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2  \f$east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @return defleciton of the vertical value \f$ \eta \f$ (in radians), obtained from following equations:
         * -# \f$r^{2}+r^{\prime2}-2rr^{\prime}\cdot(\sin\varphi\sin\varphi^{\prime}+\cos\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\varphi}=\frac{\partial\cos\psi}{\partial\varphi}=\cos\varphi\sin\varphi^{\prime}-\sin\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\lambda}=\frac{\partial\cos\psi}{\partial\lambda}=\cos\varphi\cos\varphi^{\prime}\sin(\lambda^{\prime}-\lambda) \f$
         * -# The final integral is given: \f$ \dfrac{\partial V}{\partial\lambda}=G\rho\iiint\limits _{\Omega}\dfrac{rr^{\prime}C_{\lambda}}{l^{3}}\mathrm{d}\Omega \f$
         */
        double gravity_lambda (double r, double phi, double lambda, /* running point of the integration */
                            double r1, double phi1, double lambda1, /* boundaries of the tesseroid */
                            double r2, double phi2, double lambda2,
                            double kappa, double rho );

        /**
         * @brief marussi_rr - \f$ r_i \f$ are in units of meetres, \f$ \varphi_i, \lambda_i \f$ are in degrees
         * @param r - \f$ r \f$ 'geocentric length' of the point P (running point of an integration)
         * @param phi - \f$ \varphi \f$ geodetic latitude of the point P (running point of an integration)
         * @param lambda -  \f$ \lambda \f$ geodetic longitude of the point P (running point of an integration)
         * @param r1 - \f$ r_1 \f$ lower boundary of a tesseroid
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param r2 - \f$ r_2 \f$ upper boundary of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2  \f$east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @return member of the Marussi's tenson  (in s^-2), obtained from following equations:
         * -# \f$r^{2}+r^{\prime2}-2rr^{\prime}\cdot(\sin\varphi\sin\varphi^{\prime}+\cos\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\varphi}=\frac{\partial\cos\psi}{\partial\varphi}=\cos\varphi\sin\varphi^{\prime}-\sin\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\lambda}=\frac{\partial\cos\psi}{\partial\lambda}=\cos\varphi\cos\varphi^{\prime}\sin(\lambda^{\prime}-\lambda) \f$
         * -# The final integral is given: \f$ \dfrac{\partial^{2}V}{\partial r^{2}}=G\rho\iiint\limits _{\Omega}\dfrac{1}{l^{3}}\cdot\left(\dfrac{3(r^{\prime}\cos\psi-r)^{2}}{l^{2}}-1\right)\mathrm{d}\Omega \f$
         */
        double marussi_rr  (double r, double phi, double lambda, /* running point of the integration */
                            double r1, double phi1, double lambda1, /* boundaries of the tesseroid */
                            double r2, double phi2, double lambda2,
                            double kappa, double rho );

        /**
         * @brief marussi_rb - \f$ r_i \f$ are in units of meetres, \f$ \varphi_i, \lambda_i \f$ are in degrees
         * @param r - \f$ r \f$ 'geocentric length' of the point P (running point of an integration)
         * @param phi - \f$ \varphi \f$ geodetic latitude of the point P (running point of an integration)
         * @param lambda -  \f$ \lambda \f$ geodetic longitude of the point P (running point of an integration)
         * @param r1 - \f$ r_1 \f$ lower boundary of a tesseroid
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param r2 - \f$ r_2 \f$ upper boundary of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2  \f$east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @return member of the Marussi's tenson  (in s^-2), obtained from following equations:
         * -# \f$r^{2}+r^{\prime2}-2rr^{\prime}\cdot(\sin\varphi\sin\varphi^{\prime}+\cos\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\varphi}=\frac{\partial\cos\psi}{\partial\varphi}=\cos\varphi\sin\varphi^{\prime}-\sin\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\lambda}=\frac{\partial\cos\psi}{\partial\lambda}=\cos\varphi\cos\varphi^{\prime}\sin(\lambda^{\prime}-\lambda) \f$
         * -# The final integral is given: \f$ \dfrac{\partial^{2}V}{\partial\varphi\partial r}=G\rho\iiint\limits _{\Omega}\dfrac{r^{\prime}C_{\varphi}}{l^{3}}\cdot\left(\dfrac{3r(r^{\prime}\cos\psi-r)}{l^{2}}+1\right)\mathrm{d}\Omega\f$
         */
        double marussi_rb (double r, double phi, double lambda, /* running point of the integration */
                            double r1, double phi1, double lambda1, /* boundaries of the tesseroid */
                            double r2, double phi2, double lambda2,
                            double kappa, double rho );

        /**
         * @brief marussi_rl - \f$ r_i \f$ are in units of meetres, \f$ \varphi_i, \lambda_i \f$ are in degrees
         * @param r - \f$ r \f$ 'geocentric length' of the point P (running point of an integration)
         * @param phi - \f$ \varphi \f$ geodetic latitude of the point P (running point of an integration)
         * @param lambda -  \f$ \lambda \f$ geodetic longitude of the point P (running point of an integration)
         * @param r1 - \f$ r_1 \f$ lower boundary of a tesseroid
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param r2 - \f$ r_2 \f$ upper boundary of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2  \f$east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @return member of the Marussi's tenson  (in s^-2), obtained from following equations:
         * -# \f$r^{2}+r^{\prime2}-2rr^{\prime}\cdot(\sin\varphi\sin\varphi^{\prime}+\cos\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\varphi}=\frac{\partial\cos\psi}{\partial\varphi}=\cos\varphi\sin\varphi^{\prime}-\sin\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\lambda}=\frac{\partial\cos\psi}{\partial\lambda}=\cos\varphi\cos\varphi^{\prime}\sin(\lambda^{\prime}-\lambda) \f$
         * -# The final integral is given: \f$ \dfrac{\partial^{2}V}{\partial\lambda\partial r}=G\rho\iiint\limits _{\Omega}\dfrac{r^{\prime}C_{\lambda}}{l^{3}}\cdot\left(\dfrac{3r(r^{\prime}\cos\psi-r)}{l^{2}}+1\right)\mathrm{d}\Omega \f$
         */
        double marussi_rl (double r, double phi, double lambda, /* running point of the integration */
                            double r1, double phi1, double lambda1, /* boundaries of the tesseroid */
                            double r2, double phi2, double lambda2,
                            double kappa, double rho );

        /**
         * @brief marussi_bb - \f$ r_i \f$ are in units of meetres, \f$ \varphi_i, \lambda_i \f$ are in degrees
         * @param r - \f$ r \f$ 'geocentric length' of the point P (running point of an integration)
         * @param phi - \f$ \varphi \f$ geodetic latitude of the point P (running point of an integration)
         * @param lambda -  \f$ \lambda \f$ geodetic longitude of the point P (running point of an integration)
         * @param r1 - \f$ r_1 \f$ lower boundary of a tesseroid
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param r2 - \f$ r_2 \f$ upper boundary of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2  \f$east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @return member of the Marussi's tenson  (in s^-2), obtained from following equations:
         * -# \f$r^{2}+r^{\prime2}-2rr^{\prime}\cdot(\sin\varphi\sin\varphi^{\prime}+\cos\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\varphi}=\frac{\partial\cos\psi}{\partial\varphi}=\cos\varphi\sin\varphi^{\prime}-\sin\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\lambda}=\frac{\partial\cos\psi}{\partial\lambda}=\cos\varphi\cos\varphi^{\prime}\sin(\lambda^{\prime}-\lambda) \f$
         * -# The final integral is given: \f$ \dfrac{\partial^{2}V}{\partial\varphi^{2}}=G\rho\iiint\limits _{\Omega}\dfrac{rr^{\prime}}{l^{3}}\cdot\left(\dfrac{3rr^{\prime}C_{\varphi}^{2}}{l^{2}}-\cos\psi\right)\mathrm{d}\Omega \f$
         */
        double marussi_bb (double r, double phi, double lambda, /* running point of the integration */
                            double r1, double phi1, double lambda1, /* boundaries of the tesseroid */
                            double r2, double phi2, double lambda2,
                            double kappa, double rho );

        /**
         * @brief marussi_bl - \f$ r_i \f$ are in units of meetres, \f$ \varphi_i, \lambda_i \f$ are in degrees
         * @param r - \f$ r \f$ 'geocentric length' of the point P (running point of an integration)
         * @param phi - \f$ \varphi \f$ geodetic latitude of the point P (running point of an integration)
         * @param lambda -  \f$ \lambda \f$ geodetic longitude of the point P (running point of an integration)
         * @param r1 - \f$ r_1 \f$ lower boundary of a tesseroid
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param r2 - \f$ r_2 \f$ upper boundary of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2  \f$east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @return member of the Marussi's tenson  (in s^-2), obtained from following equations:
         * -# \f$r^{2}+r^{\prime2}-2rr^{\prime}\cdot(\sin\varphi\sin\varphi^{\prime}+\cos\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\varphi}=\frac{\partial\cos\psi}{\partial\varphi}=\cos\varphi\sin\varphi^{\prime}-\sin\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\lambda}=\frac{\partial\cos\psi}{\partial\lambda}=\cos\varphi\cos\varphi^{\prime}\sin(\lambda^{\prime}-\lambda) \f$
         * -# The final integral is given: \f$ \dfrac{\partial^{2}V}{\partial\varphi\partial\lambda}=G\rho\iiint\limits _{\Omega}\dfrac{rr^{\prime}\cos\varphi^{\prime}\sin(\lambda^{\prime}-\lambda)}{l^{3}}\cdot\left(\dfrac{3rr^{\prime}C_{\varphi}\cos\varphi}{l^{2}}-\sin\varphi\right)\mathrm{d}\Omega \f$
         */
        double marussi_bl (double r, double phi, double lambda, /* running point of the integration */
                            double r1, double phi1, double lambda1, /* boundaries of the tesseroid */
                            double r2, double phi2, double lambda2,
                            double kappa, double rho );

        /**
         * @brief marussi_ll - \f$ r_i \f$ are in units of meetres, \f$ \varphi_i, \lambda_i \f$ are in degrees
         * @param r - \f$ r \f$ 'geocentric length' of the point P (running point of an integration)
         * @param phi - \f$ \varphi \f$ geodetic latitude of the point P (running point of an integration)
         * @param lambda -  \f$ \lambda \f$ geodetic longitude of the point P (running point of an integration)
         * @param r1 - \f$ r_1 \f$ lower boundary of a tesseroid
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param r2 - \f$ r_2 \f$ upper boundary of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2  \f$east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @return member of the Marussi's tenson  (in s^-2), obtained from following equations:
         * -# \f$r^{2}+r^{\prime2}-2rr^{\prime}\cdot(\sin\varphi\sin\varphi^{\prime}+\cos\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\varphi}=\frac{\partial\cos\psi}{\partial\varphi}=\cos\varphi\sin\varphi^{\prime}-\sin\varphi\cos\varphi^{\prime}\cos(\lambda^{\prime}-\lambda) \f$
         * -# \f$ C_{\lambda}=\frac{\partial\cos\psi}{\partial\lambda}=\cos\varphi\cos\varphi^{\prime}\sin(\lambda^{\prime}-\lambda) \f$
         * -# The final integral is given: \f$ \dfrac{\partial^{2}V}{\partial\lambda^{2}}=G\rho\iiint\limits _{\Omega}\dfrac{rr^{\prime}}{l^{3}}\cdot\left(\dfrac{3rr^{\prime}C_{\lambda}^{2}}{l^{2}}-\cos\varphi^{\prime}\cos\varphi\cos(\lambda^{\prime}-\lambda)\right)\mathrm{d}\Omega \f$
         */
        double marussi_ll (double r, double phi, double lambda, /* running point of the integration */
                            double r1, double phi1, double lambda1, /* boundaries of the tesseroid */
                            double r2, double phi2, double lambda2,
                            double kappa, double rho );

        double handle_no_data( arma::mat A );

        ///< Temporary functions, ment to be deleted!

        /**
         * @brief temp_ref2upperlat Temporary functions, ment to be deleted!
         * @return
         */
        arma::mat& temp_ref2upperlay() {
            return upper_lay;
        }

        arma::mat& temp_ref2lowerlay() {
            return lower_lay;
        }

        /**
         * @brief temp_set_lowerlay Temporary functions, ment to be deleted!
         * @param llay
         */
        void temp_set_lowerlay ( arma::mat llay ) {
           this->lower_lay = llay;
        }

        void temp_see_size () {
            cout << upper_lay.n_rows << "\t" << upper_lay.n_cols << endl;
            cout << lower_lay.n_rows << "\t" << lower_lay.n_cols << endl;
        }

        /**
         * @brief interpolate_value - using inverse distance algorithm
         * @param phi
         * @param lambda
         * @return
         */
        double interpolate_value( double phi, double lambda);

        /**
         * @brief m_ij_condlay -  computes the value od the 2nd Helmert condensation
         * layer
         * @param r - running point of the integration
         * @param phi
         * @param lambda
         * @param ru - upper layer (normal height/orthometric height)
         * @param phi1 - \f$ \varphi_1 \f$ south boundary (latitude) of a tesseroid
         * @param lambda1 - f$ \lambda_1 \f$ west boundary (longitude) of a tesseroid
         * @param phi2 - \f$ \varphi_2 \f$ north boundary (latitude) of a tesseroid
         * @param lambda2 - f$ \lambda_2  \f$east boundary (longitude) of a tesseroid
         * @param kappa - Newton's gravity constant
         * @param rho - \$f \rho \f$ constant density of a tesseroid
         * @param ratio
         * @return
         */

        /**
         * @brief do_check - checks if the both layers are loaded, in the case if the lower layer is not set,
         * the zero matrix with the same size as the upper layer data is created.
         */
        bool do_check();

//        double m_ij_condlay (double r,
//                            double phi,
//                            double lambda,
//                            double ru,
//                            double phi1,
//                            double lambda1,
//                            double phi2,
//                            double lambda2,
//                            double kappa,
//                            double rho,
//                            double ratio,
//                            double (&k_ijk) (double, double, double, double, double, double),
//                            double (&alpha_ij)(double, double, double, double, double, double),
//                            double (&beta1_ijk)(double, double, double, double, double, double),
//                            double (&beta2_ijk)(double, double, double, double, double, double),
//                            double (&gamma1_ijk)(double, double, double, double, double, double),
//                            double (&gamma2_ijk)(double, double, double, double, double, double),
//                            double (&gamma3_ijk)(double, double, double, double, double, double));

        /* ============================================================
         * P R I V A T E
         * ========================================================= */
private:
    string model_name;        ///< Name of the DMR model
    string model_type;        ///< Type of the model, digital terrain/relief/elevation model
    string units;             ///< Units
    string ref_ell;           ///< reference ellipsoid
    string isg_format;        ///< Version of the isgem format.
    double lat_min;           ///< Minimum value of the latitude
    double lat_max;           ///< Maximum value of the latitude
    double lon_min;           ///< Minimum value of the longitude
    double lon_max;           ///< Maximum value of the longitude
    double nodata = -9999.999;///< NoData value
    double delta_lat;         ///< Difference between two meridians
    double delta_lon;         ///< Difference between two parallels
    bool print_coeff = false; ///< show the data , if the model is printed, false is set by default
    bool upper_set = false;   ///< Check if the upper layer is set/laoded data
    bool lower_set = false;   ///< Check if the lower layer is set/laoded data
    unsigned int nrows;       ///< Number of columns
    unsigned int ncols;       ///< Number of rows
    arma::mat upper_lay;      ///< Data stored in matrix style grid - upper layer
    arma::mat lower_lay;      ///< Data stored in matrix style grid - lower layer

    /**
     * @brief conv2index - convert difference between lat/lon of an arbitraty point
     * and lat_min/lon_min to index as unsigned int
     * @param xval - number in double precision representing \f$ \frac{\varphi - \varphi_{min}}{\Delta_\varphi} \f$
     * @return index for matrix or vector manipulation as unsigned int
     */
    unsigned int conv2index( double xval );

    /**
     * @brief conv2index2 - convert difference between lat/lon of an arbitraty point
     * and lat_min/lon_min to index as int
     * @param xval - number in double precision representing \f$ \frac{\varphi - \varphi_{min}}{\Delta_\varphi} \f$
     * @return index for matrix or vector manipulation as int
     */
    int conv2index2( double xval );

    /**
     * @brief load_mat_from_ascii
     * @param path - path to the file
     * @param format - format of the input
     * -# 0 - PointID b l hu_el hl_el
     * -# 1 - PointID l b hu_el hl_el
     * -# 2 - PointID b l hu_ell
     * -# 3 - b l hu_ell hl_ell
     * -# 4 - l b hu_ell hl_ell
     * -# 5 - b l hu_ell
     * -# 6 - l b hu_ell
     * @param load_col - nu,ber of column with the data stored
     * @param basinfo - reference to vector in which the basic geometry info will be stored
     * in following order:
     * -# \f$ \varphi_{min} \f$ minimum value of the geodetic latitude
     * -# \f$ \varphi_{max} \f$ maximum value of the geodetic latitude
     * -# \f$ \lambda_{min} \f$ minimum value of the geodetic longitude
     * -# \f$ \lambda_{max} \f$ maximum value of the geodetic longitude
     * -# nodata value
     * -# \f$ \Delta \varphi \f$ the difference between two meridians
     * -# \f$ \Delta \lambda \f$ the difference between two parallels
     * @param header reference to the vector with the header info (model name, model type, units, reference ellipsoid etc.)
     * @param success bool value if the loading of the data was successfull
     * @param rewrite_header
     * @param nodata_value
     * @return data stored in armadillo matrix
     */
    arma::mat load_mat_from_ascii( const string &path,
                                   unsigned int format,
                                   unsigned int load_col,
                                   tess::header &hinfo,
                                   bool &success,
                                   bool rewrite_header = false,
                                   double nodata_value = -9999.999);

    /**
     * @brief load_isgem_model - load a layer from ascii file, formated in modified
     * isgem model (the values are stored to the center).
     * @param path - path to the file
     * @param basinfo - reference to vector in which the basic geometry info will be stored
     * in following order:
     * -# \f$ \varphi_{min} \f$ minimum value of the geodetic latitude
     * -# \f$ \varphi_{max} \f$ maximum value of the geodetic latitude
     * -# \f$ \lambda_{min} \f$ minimum value of the geodetic longitude
     * -# \f$ \lambda_{max} \f$ maximum value of the geodetic longitude
     * -# nodata value
     * -# \f$ \Delta \varphi \f$ the difference between two meridians
     * -# \f$ \Delta \lambda \f$ the difference between two parallels
     *
     * @param header reference to the vector with the header info (model name, model type, units, reference ellipsoid etc.)
     * @param success bool value if the loading of the data was successfull
     * @param rewrite_header
     * @param nodata_value
     * @return data stored in armadillo matrix
     */
    arma::mat load_isgem_model (const string &path,
                                tess::header &hinfo,
                                bool &success,
                                bool rewrite_header = false);

    /**
     * @brief load_csv_model
     * @param basinfo reference to vector in which the basic geometry info will be stored
     * in following order:
     * -# \f$ \varphi_{min} \f$ minimum value of the geodetic latitude
     * -# \f$ \varphi_{max} \f$ maximum value of the geodetic latitude
     * -# \f$ \lambda_{min} \f$ minimum value of the geodetic longitude
     * -# \f$ \lambda_{max} \f$ maximum value of the geodetic longitude
     * -# nodata value
     * -# \f$ \Delta \varphi \f$ the difference between two meridians
     * -# \f$ \Delta \lambda \f$ the difference between two parallels
     *
     * @param header reference to the vector with the header info (model name, model type, units, reference ellipsoid etc.)
     * @param success bool value if the loading of the data was successfull
     * @param rewrite_header
     * @param nodata_value
     * @return data stored in armadillo matrix
     */
    arma::mat load_csv_model ( const string &path,
                               const tess::header &hinfo,
                               bool &success,
                               bool rewrite_header = false,
                               double nodata_value = -9999.999);

    /**
     * @brief check_layers_size check if the upper layer and lower layer has the same size (number
     * of rows and cols).
     * @return true if the dimensions are the same, false otherwise
     */
    bool check_layers_size();

};
#endif // TESSEROID_H
