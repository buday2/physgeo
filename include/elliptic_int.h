#ifndef ELLIPTIC_INT_H
#define ELLIPTIC_INT_H

#include <cmath>
#include <limits>
#include <iomanip>
#include <iostream>

namespace ell_int {

/**
* @param m0
* @param order
*/
double* coeff_Ej(const double& m0, int& order);

/**
* @param m0
* @param order
*/
double* coeff_Kj(const double& m0, int& order);

/**
*/
double* coeff_Hj();

/**
*/
double* coeff_qj();

/**
  * @param m
  * @return Compute the value of the complete elliptic integral
  */
double comellint_1stknd(const double& m);

/**
  * @param m
  * @return Compute the value of the complete elliptic integral 2nd kind
  */
double comellint_2stknd(const double& m);

}
#endif // ELLIPTIC_INT_H
