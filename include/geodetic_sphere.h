#ifndef GEODETIC_SPHERE_H
#define GEODETIC_SPHERE_H

#include <iostream>
#include <cmath>
#include <iomanip>

#include "dms.h"
#include "physical_constants.h"

using namespace std;

namespace geo_sphere {

    template <typename Any>
    Any signum( Any number ){
        Any sign_val;

        if ( number > 0 ){
            sign_val = 1.;
        } else if ( number < 0){
            sign_val = -1.;
        } else {
            sign_val = 0.;
        }
        return sign_val;
    }

    ///< Input in radians !!!
    template <typename Any>
    void get_right_interval( Any &angle){
        if ( angle > 2.*M_PI ){
            angle -= 2.*M_PI;
        } else if ( angle < 0. ){
            angle += 2.*M_PI;
        }
    }

    /** @brief geodetic1 - Computes the first geodetic problem - for given point \f$ P( \varphi_1, \lambda_1) \f$
    * and given azimuth and length computes the coordinates of the second points
    * \f$ Q( \varphi_2, \lambda_2) \f$ and azimuth from Q to P.
    *
    * @param phi1 - latitude of the point P
    * @param lambda1 - longitude of the point P
    * @param alpha1 - azimuth at P towards Q
    * @param s1 - length on the sphere between P and Q
    *
    * @param phi2 - latitude of the calculated point Q
    * @param lambda2 - longitude of the Q
    * @param alpha2 - azimuth QP
    */
    void geodetic1( dms phi1 , dms lambda1, dms alpha1, double s1, double radius,
                    dms& phi2, dms& lambda2, dms& alpha2);

    /** @brief geodetic1 - Computes the first geodetic problem - for given point \f$ P( \varphi_1, \lambda_1) \f$
    * and given azimuth and length computes the coordinates of the second points
    * \f$ Q( \varphi_2, \lambda_2) \f$ and azimuth from Q to P.
    *
    * @param phi1 - latitude of the point P
    * @param lambda1 - longitude of the point P
    * @param alpha1 - azimuth at P towards Q
    * @param s1 - length on the sphere between P and Q
    *
    * @param phi2 - latitude of the calculated point Q
    * @param lambda2 - longitude of the Q
    * @param alpha2 - azimuth QP
    */
    void geodetic1( const double& phi1 , const double& lambda1, const double& alpha1, const double& s1, const double& radius,
                    double& phi2, double& lambda2, double& alpha2);

    /**
     * @brief geodetic2 - Comptutes the second geodetic problem, for two given points  \f$ P_1( \varphi_1, \lambda_1) \f$
     * and  \f$ P_2( \varphi_2, \lambda_2) \f$ computes the distance between them on the reference sphere and azimuths
     * toward each other.
     * @param phi1 - latitude of the first given point
     * @param lambda1 - longitude of the first given point
     * @param phi2 - latitude of the second given point
     * @param lambda2 - longitude of the second given point
     * @param R - radius of the reference sphere
     * @param alpha1 - azimuth \f$ \alpha_12 \f$
     * @param alpha2 - azimutn \f$ \alpha_21 \f$
     * @param s1 - length between points P1 and P2 on sphere surface
     */
    void geodetic2(dms phi1,dms lambda1, dms phi2, dms lambda2,
                 double R, dms &alpha1, dms &alpha2, double &s1);

    /**
     * @brief geodetic2 - Comptutes the second geodetic problem, for two given points  \f$ P_1( \varphi_1, \lambda_1) \f$
     * and  \f$ P_2( \varphi_2, \lambda_2) \f$ computes the distance between them on the reference sphere and azimuths
     * toward each other.
     * @param phi1 - latitude of the first given point
     * @param lambda1 - longitude of the first given point
     * @param phi2 - latitude of the second given point
     * @param lambda2 - longitude of the second given point
     * @param R - radius of the reference sphere
     * @param alpha1 - azimuth \f$ \alpha_12 \f$
     * @param alpha2 - azimutn \f$ \alpha_21 \f$
     * @param s1 - length between points P1 and P2 on sphere surface
     */
    void geodetic2(const double& phi1,
                   const double& lambda1,
                   const double& phi2,
                   const double& lambda2,
                   const double& R,
                   double &alpha1,
                   double &alpha2,
                   double &s1);
}

#endif // GEODETIC_SPHERE_H
