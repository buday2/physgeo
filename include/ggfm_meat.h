#ifndef GGFM_MEAT_H
#define GGFM_MEAT_H

#include <iostream>
#include <cmath>
#include <typeinfo>
#include <utility>
#include <cstddef>
#include <cstdio>
#include <iomanip>
#include <istream>
#include <ostream>

// OpenPragma Library
#include <omp.h>

// include armadillo
#include <armadillo>

using namespace std;
//---------------------------------------------------------------------------------------------------------------------------------
namespace ggfm_f { // plus_sum
template <typename eT>
inline eT plus_sum(const eT& c_nm,
                   const eT& s_nm,
                   const eT& p_nm,
                   const eT& m,
                   const eT& lambda) {
  return (p_nm * (c_nm * cos(m * lambda) + s_nm * sin(m * lambda)));
}

// p_nm is for legendre associated coefficient of second kind or it's derivative
// and also can be multiplied by required constant e.g m*p_nm, or m^2 *
// ddp_nm,...
template <typename eT>
inline eT minus_sum(const eT& c_nm,
                        const eT& s_nm,
                        const eT& p_nm,
                        const eT& m,
                        const eT& lambda) {
  return (p_nm * (-c_nm * sin(m * lambda) + s_nm * cos(m * lambda)));
}

template <typename eT>
inline eT minus_sum2(const eT& c_nm,
                     const eT& s_nm,
                     const eT& p_nm,
                     const eT& m,
                     const eT& lambda) {
  return (p_nm * m * (-c_nm * sin(m * lambda) + s_nm * cos(m * lambda)));
}

template <typename eT>
inline eT plus_sum2(const eT& c_nm,
                        const eT& s_nm,
                        const eT& p_nm,
                        const eT& m,
                        const eT& lambda) {
  return (-p_nm * m * m * (c_nm * cos(m * lambda) + s_nm * sin(m * lambda)));
}


/**
 * @brief plus_lstokes1 - returns sum
 * @param pc_nm - multiplied Stoke's (cos) coefficient with fnALFs for given degree (n) and order (m)
 * @param ps_nm - multiplied Stoke's (sin) coefficient with fnALFs for given degree (n) and order (m)
 * @param m - order
 * @param lambda - longitude
 * @return \f$ P_{nm} {nm} \cos ( m \lambda )  + P_{nm} S_{nm} \sin ( m \lambda ) \f$
 */
template <typename eT>
inline eT plus_lstokes1 ( const eT& pc_nm, const eT& ps_nm, const eT& m, const eT& lambda ) {
    eT mlambda = m * lambda;
    return  ( pc_nm * cos ( mlambda ) + ps_nm * sin( mlambda ) ) ;
}


/**
 * @brief plus_lstokes2 - returns sum
 * @param pc_nm - multiplied Stoke's (cos) coefficient with fnALFs for given degree (n) and order (m)
 * @param ps_nm - multiplied Stoke's (sin) coefficient with fnALFs for given degree (n) and order (m)
 * @param m - order
 * @param lambda - longitude
 * @return \f$ -m^2(P_{nm} {nm} \cos ( m \lambda )  + P_{nm} S_{nm} \sin ( m \lambda )) \f$
 */
template <typename eT>
inline eT plus_lstokes2 ( const eT& pc_nm, const eT& ps_nm, const eT& m, const eT& lambda ) {
    eT mlambda = m * lambda;
    return  -m*m*( pc_nm * cos ( mlambda ) + ps_nm * sin( mlambda ) ) ;
}

/**
 * @brief minus_lstokes1 - returns sum
 * @param pc_nm - multiplied Stoke's (cos) coefficient with fnALFs for given degree (n) and order (m)
 * @param ps_nm - multiplied Stoke's (sin) coefficient with fnALFs for given degree (n) and order (m)
 * @param m - order
 * @param lambda - longitude
 * @return \f$ -P_{nm} C_{nm} \cos ( m \lambda )  + P_{nm}S_{nm}\sin ( m \lambda ) \f$
 */
template <typename eT>
inline eT minus_lstokes1 ( const eT& pc_nm, const eT& ps_nm, const eT& m , const eT& lambda ) {
    eT mlambda = m * lambda;
    return  ( -pc_nm * cos ( mlambda ) + ps_nm * sin( mlambda ) ) ;
}

/**
 * @brief minus_lstokes1 - returns sum
 * @param pc_nm - multiplied Stoke's (cos) coefficient with fnALFs for given degree (n) and order (m)
 * @param ps_nm - multiplied Stoke's (sin) coefficient with fnALFs for given degree (n) and order (m)
 * @param m - order
 * @param lambda - longitude
 * @return \f$ m(-P_{nm} C_{nm} \cos ( m \lambda )  + P_{nm}S_{nm}\sin ( m \lambda )) \f$
 */
template <typename eT>
inline eT minus_lstokes2 ( const eT& pc_nm, const eT& ps_nm, const eT& m , const eT& lambda ) {
    eT mlambda = m * lambda;
    return  m*( -pc_nm * sin ( mlambda ) + ps_nm * cos( mlambda ) ) ;
}

template <typename eT>
inline eT n_multiply0(const eT& n) {
  return 1.;
}

template <typename eT>
inline eT n_multiply1(const eT& n) {
  return n + 1.;
}

template <typename eT>
inline eT n_multiply2(const eT& n) {
  return (n + 1.) * (n + 2.);
}


template <typename eT>
inline eT n_multiply3(const eT& n) {
  return (n - 1.);
}

template  <typename eT>
inline eT n_multiply4(const eT& n)
{
    return (n-1.) * (n+2.);
}

template  <typename eT>
inline eT n_multiply5(const eT& n)
{
    return (n-1.) * (n+2.) * (n+3.);
}

template <typename eT>
inline eT um_function1(const int& i, const eT& idouble, const eT& phi) {
  eT um = (i != 0) ? cos(phi) : static_cast<eT>(1.0);
  return um;
}

template <typename eT>
inline eT um_function2(const int& i, const eT& idouble, const eT& phi) {
  eT um;
  if (i == 0) {
    um = static_cast<eT>(1.0);
  } else if (i == 1) {
    um = sqrt(3.0) * cos(phi);
  } else {
    um = sqrt((2.0 * idouble + 1.0) / (2.0 * idouble)) * cos(phi);
  }  // end of relaxing factor
  return um;
}

template <typename Any>
inline Any relax_factor() {
  /* has to be called as relax_factor<double>(), ....
   * used as scaling parameter for legendre coefficients
   */
      Any var;
      if (typeid(var) == typeid(double)) {
        return 1.0e-280;
      } else if (typeid(var) == typeid(long double)) {
        return 1.0e-280;
      } else if (typeid(var) == typeid(float)) {
        return 1.0e-35;
      } else {
        return 1;
      }
    }
}

namespace ggfm_idx {
/**
 * @brief return_rowcol
 * @param e
 * @param nrows
 * @param ncols
 * @param row
 * @param col
 * @return
 */
bool return_rowcol ( const arma::uword &e,
                     const arma::uword &nrows,
                     const arma::uword &ncols,
                     arma::uword &row,
                     arma::uword &col );

/**
 * @brief return_rowcol2
 * @param e
 * @param amat
 * @param row
 * @param col
 * @return
 */
bool return_rowcol2 ( const arma::uword &e,
                      const arma::mat &amat,
                      arma::uword &row,
                      arma::uword &col );


}


#endif // GGFM_MEAT_H
