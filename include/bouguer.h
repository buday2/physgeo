#ifndef BOUGUER_H
#define BOUGUER_H

#include <cmath>
#include <iostream>
#include <exception>
#include <errno.h>

#include "physical_constants.h"

namespace bouguer {

template <typename T>
inline double sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

/**
 * @brief The Bouguer class - deals with evaluation of the gravitational signal from topography.
 * Area of the study is the effect of topography on three different gravity field
 * quantities: the gravitational potential V and its first and second order radial
 * derivatives \f$ \partial V / \partial r \f$ , \f$ \partial^2 V / \partial r^2 \f$
 *
 * There are 5 possibilities :
 * -# Point is above the bouguer layer \f$ P_1 ( z>0) \f$
 * -# Point is at the top edge of the layer \f$ P_2 ( z=0) \f$
 * -# Point is inside the layer \f$ P_3 ( 0<z<-h) \f$
 * -# Point is at the lower edge of the Bouguer layer \f$ P_4 ( z=-h) \f$
 * -# Point is under the layer  \f$ P_5 ( z<-h) \f$
 */
class Bouguer
{
public:
    /**
     * @brief Bouguer - empty constructor, default values \f$ \rho = 2670 kg m^{-3} \f$
     * and \f$ G = 6.67408 \cdot 10^{-11} [m^3 kg^{-1} s^{-2}] \f$
     */
    Bouguer();

    /**
     * @brief Bouguer constructor with possibility to enter new values of $\rho , G \f$
     * @param n_rho - rewrites the class value of the \f$ \rho \f$ - density
     * @param n_G - rewrites the class value of the \f$ G \f$ - Newton's gravity constant
     */
    Bouguer( const double &n_rho, const double& n_G );

    /**
     * @brief gpot_v_plate - calculates the gravitational potential of the limited Bouguer plate ( cylinder geometry ).
     * Solution is given by the integral \f$ V(P) = G \rho \int \limits_0^{2\pi}   \int \limits_0^{R}  \int \limits_{-h}^{0}  r^\prime \sqrt{ r^{\prime \, 2} + (z - z^\prime)^2 } dz^\prime dr^\prime d\alpha^\prime  \f$
     * @param R - radius of the cylidrical body
     * @param h - height of the Bouguer plate
     * @param z - height of the point
     * @param pcase - which case of the mutual position of the point P and the Bouguer plate, @see Bouguer
     * @return
     */
    double gpot_v_limited_plate( const double& R ,
                                 const double& h ,
                                 const double& z ,
                                 const int& pcase  );

    /**
     * @brief gpot_v_unlimited_plate calculates the gravitational potential of the unlimited Bouguer plate ( cylinder geometry ).
     * @param h
     * @param z
     * @param pcase
     * @return
     */
    double gpot_v_unlimited_plate( const double& h,
                                   const double& z,
                                   const int& pcase);

    /**
     * @brief gravity_r_unlimited_plate
     * @param h
     * @param z
     * @param pcase
     * @return
     */
    double gravity_r_unlimited_plate ( const double& h,
                                       const double& z,
                                       const int& pcase );

    /**
     * @brief marussi_rr_inlimited_plate
     * @param h
     * @param z
     * @param pcase
     * @return
     */
    double marussi_rr_unlimited_plate( const double& h,
                                       const double& z,
                                       const int& pcase);

    /**
     * @brief gravity_r_limited_plate
     * @param R
     * @param h
     * @param z
     * @param pcase
     * @return
     */
    double gravity_r_limited_plate( const double &R ,
                                    const double& h ,
                                    const double& z ,
                                    const int& pcase  );

    /**
     * @brief marussi_rr_limited_plate
     * @param R
     * @param h
     * @param z
     * @param pcase
     * @return
     */
    double marussi_rr_limited_plate( const double &R ,
                                     const double& h ,
                                     const double& z ,
                                     const int& pcase  );

    /**
     * @brief gpot_v_limited_shell
     * @param r1
     * @param r2
     * @param r - sphere radius
     * @param intradius - integration radius
     * @return
     */
    double gpot_v_limited_shell( const double& r1, // r1 = R -sphere radius
                                 const double& r2, // r2 = R+h ->( height + sphrad )
                                 const double& r, // position of the point P , so for z=h r = R+h
                                 const double& intradius);

    /**
     * @brief gpot_v_limited_shell
     * @param r1
     * @param r2
     * @param r - sphere radius
     * @param intradius - integration radius
     * @return
     */
    double gpot_v_unlimited_shell( const double& r1, // r1 = R -sphere radius
                                   const double& r2, // r2 = R+h - height + sphrad
                                   const double& r);

    /**
     * @brief gravity_r_limited_shell
     * @param r1
     * @param r2
     * @param r
     * @param intradius
     * @return
     */
    double gravity_r_limited_shell( const double& r1, // r1 = R -sphere radius
                                    const double& r2, // r2 = R+h ->( height + sphrad )
                                    const double& r, // position of the point P , so for z=h r = R+h
                                    const double& intradius);

    /**
     * @brief gravity_r_unlimited_shell
     * @param r1
     * @param r2
     * @param r
     * @return
     */
    double gravity_r_unlimited_shell( const double& r1, // r1 = R -sphere radius
                                      const double& r2, // r2 = R+h - height + sphrad
                                      const double& r);

    /**
     * @brief marussi_rr_unlimited_shell
     * @param r1
     * @param r2
     * @param r
     * @return
     */
    double marussi_rr_unlimited_shell( const double& r1, // r1 = R -sphere radius
                                       const double& r2, // r2 = R+h - height + sphrad
                                       const double& r);

    double marussi_rr_limited_shell( const double& r1, // r1 = R -sphere radius
                                     const double& r2, // r2 = R+h - height + sphrad
                                     const double& r, // position of the point P , so for z=h r = R+h
                                     const double& intradius) {
        std::cout << "Bouguer::marussi_rr_limited_shell() not finished. @see the source code!" << std::endl;
        return (r1+r2+r+intradius) * 0.0;
    }

    /**
     * @brief set_new_rho
     * @param n_rho
     */
    void set_new_rho( const double& n_rho ) {
        this->rho = n_rho;
    }

    /**
     * @brief set_new_Newton
     * @param n_G
     */
    void set_new_Newton( const double n_G );

private:
    double rho = 2670.0 ; ///< average density
    double G  = NG_CONST; ///< Newton's gravity constant
};


} // end namespace
#endif // BOUGUER_H
