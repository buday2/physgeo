#ifndef INTERPOLATIONS_H
#define INTERPOLATIONS_H

#include <vector>
#include <cmath>
#include <map>
#include <new>
#include <iostream>
#include <cstddef>
#include <algorithm>
#include <sstream>
#include <string>
#include <omp.h>

#include "geodetic_functions.h"
#include <armadillo>

const double EPS = 2.220446049250313e-16;

using namespace std;

namespace interpol{
    void free_matrix(double **A, int lines);
    double Lagrange_Int(map<double, double> InputData,
                        double T, unsigned int n);
    double Newton_Int(  map<double, double> InputData,
                        double T, unsigned int n);
    void order_by_col ( arma::mat& dat, unsigned int col );
    arma::mat find_closest_points3D( double x, double y, double z, arma::mat xyz, unsigned int i);
    double length_on_sphere (double b1, double b2, double l1, double l2, double r);
    double close_neighbour_method(double phi, double lambda,  arma::mat xyz,
                                      double maxdist, unsigned int maxpt);
    arma::mat load_arma_mat (const char* path );
};


#endif // INTERPOLATIONS_H
