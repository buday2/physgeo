#ifndef ISGEMGEOID_H
#define ISGEMGEOID_H

#include <iomanip>
#include <iostream>
#include <fstream>
#include <map>
#include <cmath>
#include <algorithm>
#include <vector>
#include <complex>
#include <omp.h>
#include <exception>
#include <errno.h>
#include <string>

#include <hdf5.h>
#define ARMA_USE_HDF5
//#define ARMA_USE_OPENMP
#include <armadillo>
#include <armadillo_bits/include_hdf5.hpp>

#include "progressbar.h"
#include "geodetic_functions.h"
#include "physical_constants.h"
#include "fnalfs.h"
#include "legendre.h"
#include "gaussquad.h"
#include "ggfm_meat.h"
#include "timer.h"
#include "bouguer.h"

#pragma omp declare reduction( + : arma::rowvec : omp_out += omp_in ) \
  initializer( omp_priv = omp_orig )

using namespace std;
/**
 * @brief The Isgemgeoid class
 */
class Isgemgeoid
{
    public:
        Isgemgeoid();
        ~Isgemgeoid(); // destructor

        /**
         * @brief reset - reset the data matrix, use cautiously
         */
        void reset();

        /**
         * @brief load_model_from_ascii - load the modified version of the ISGEM format,
         * instead, the center point is given by pair \f$ (\varphi, \lambda) \f$ and also the
         * grid value is related to the center point
         * @param path - path to the model on HDD
         * @param format - the order of columns, containing the variables:
         *      -# format = 0 , order [PointID b l hu_ell]
         *      -# format = 1 , order [PointID l b hu_ell]
         *      -# format = 2 , order [PointID b l hu_ell]
         *      -# format = 3 , order [b l hu_ell]
         *      -# format = 4 , order [l b hu_ell]
         *      -# format = 5 , order [b l hu_ell]
         *      -# format = 6 , order [l b hu_ell]
         * @param f_model_name - name of the model
         * @param f_model_type - model type (DEM, DSM, etc..)
         * @param f_units - units
         * @param f_ref_ell - reference ellipsoid
         * @param f_isg_format -
         * @param nodata_value - value for NODATA items
         * @return true if the process was succesfull, false otherwise
         */
        bool load_model_from_ascii( const string& path,
                                    unsigned int format ,
                                    const char* f_model_name,
                                    const char* f_model_type,
                                    const char* f_units,
                                    const char* f_ref_ell,
                                    const char* f_isg_format,
                                    double nodata_value) ;

        /**
         * @brief export2ascii - export currently stored values to ascii formated grid
         * @param path - path to file
         * @param ptid - true for point ID column, false if only \f$ \varphi, \lambda, grid value\f$
         * @return true if the process was succesfull, false otherwise
         */
        bool export2ascii( const string& path, bool ptid ) ;

        /**
         * @brief export2_esri_grid - see https://en.wikipedia.org/wiki/Esri_grid
         * An Esri grid is a raster GIS file format developed by Esri, which has two formats:
         * 1. A proprietary binary format, also known as an ARC/INFO GRID, ARC GRID and many other variations
         * 2. A non-proprietary ASCII format, also known as an ARC/INFO ASCII GRID
         * @param path - path where to export the data
         * @return true if the process was succesfull, false otherwise
         */
        bool export2_esri_grid( const string& path , bool center =true ) ;

        /**
         * @brief Isgemgeoid::load_isgem_model
         * @param path
         * @return true if loading model was succesfull, false otherwise
         */
        bool load_isgem_model ( const string& path );

        /**
         * @brief print_isgem_model
         * @param print_coeff
         */
        void print_isgem_model( const string& print_coeff = "no" );

        /**
         * @brief get_value
         * @param i
         * @param j
         * @return
         */
        double get_value( unsigned int i , unsigned int j  );

        /**
         * @brief get_value
         * @param i
         * @param j
         * @return
         */
        double get_value( unsigned int i , unsigned int j  ) const ;

        /**
         * @brief get_value
         * @param phi
         * @param lambda
         * @return
         */
        double get_value( double phi, double lambda);

        /**
         * @brief interpolate_value interpolates value from grid, #TODO# nodata value should not be included
         * @param phi - geodetic latitude of an arbitraty point in DEG format
         * @param lambda - geodetic longitude of an arbitraty point in DEG format
         * @param alg - algorithm for interpolation, nearest neighbour is defualt. Also the algorithm based on
         * surface of spherical triangles is available as "sphtriangle" option
         * @return an interpolated value for an arbitrary point \f$ P(\varphi, \lambda)\f$ from the grid data
         */
        double interpolate_value( const double& phi, const double& lambda, const string& alg = "nearest", bool *isinbound = nullptr ) const ;

        /**
         * @brief interpolate_value interpolates value from grid, #TODO# nodata value should not be included
         * @param phi - geodetic latitude of an arbitraty point in DEG format
         * @param lambda - geodetic longitude of an arbitraty point in DEG format
         * @param alg - algorithm for interpolation, nearest neighbour is defualt. Also the algorithm based on
         * surface of spherical triangles is available as "sphtriangle" option
         * @return an interpolated value for an arbitrary point \f$ P(\varphi, \lambda)\f$ from the grid data
         */
        double interpolate_value( const double& phi, const double& lambda, const string& alg = "nearest", bool *isinbound = nullptr ) ;


        /**
         * @brief interpolate_grid - returns grid created from set of phi_vec, lam_vec,
         * \f$ \vec{\varphi} = \left \{ \varphi_1 , \ldots , \varphi_n \right \}, \vec{\lambda}  = \left \{ \lambda_1 , \ldots , \lambda_m \right \}  .
         * Grid is created as \f$ A_{n,m} = f( \vec{\varphi} \times \vec{\lambda} )  \f$.
         * @param phi_vec - steps/values in \f$ \varphi \f$ coordinate, \f$ \vec{\varphi} = \left \{ \varphi_1 , \ldots , \varphi_n \right \} \f$.
         * @param lam_vec - steps/values in \f$ \lambda \f$ coordinate, \f$ \vec{\lambda} = \left \{ \lambda_1 , \ldots , \lambda_n \right \} \f$.
         * @return values in grid as mentioned above
         */
        arma::mat interpolate_grid ( const arma::vec &phi_vec , const arma::vec &lam_vec );

        /**
         * @brief mean_value_row
         * @param row_number
         * @return
         */
        double mean_value_row( size_t row_number );

        /**
         * @brief mean_value_row
         * @param row_number
         * @return
         */
        double mean_value_row( size_t row_number ) const;


        /**
         * @brief get_index
         * @param phi
         * @param lambda
         * @param i
         * @param j
         */
        void get_index( const double& phi,
                        const double& lambda,
                        unsigned int &i,
                        unsigned int &j) const ;

        /**
         * @brief get_coordinates
         * @param i
         * @param j
         * @param phi
         * @param lam
         */
        void get_coordinates ( const unsigned int& i,
                               const unsigned int& j,
                               double &phi,
                               double &lam ) const;
        /**
         * @brief swap_minmax if min value is bigger then max
         * the function switches the values
         * @param min
         * @param max
         */
        void swap_minmax( double &min, double &max);



        /**
         * @brief get_header - return the header info for the @see Isgemgeoid model
         * @param nrows - return the number of the rows in data
         * @param ncols - return the number of the cols in data
         * @return stl vector container with 7 elements orderd in:
         * -# index 0 - minimum value of the latitude
         * -# index 1 - maximum value of the latitude
         * -# index 2 - minimum value of the longitude
         * -# index 3 - maximum value of the longitude
         * -# index 4 - latitude step
         * -# index 5 - longitude step
         * -# index 6 - nodata value
         */
        template<typename eT>
        vector<eT> get_header( unsigned& nrows, unsigned& ncols) {
            vector<eT> header(7); // bmin, bmax, lmin, lmax, deltab, deltal, nodata

            header[0] = static_cast<eT>( this->lat_min ); // minimum value of the latitude
            header[1] = static_cast<eT>( this->lat_max ); // maximum value of the latitude
            header[2] = static_cast<eT>( this->lon_min ); // minimum value of the longitude
            header[3] = static_cast<eT>( this->lon_max ); // maximum value of the longitude
            header[4] = static_cast<eT>( this->delta_lat ); // latitude step
            header[5] = static_cast<eT>( this->delta_lon ); // longitude step
            header[6] = static_cast<eT>( this->nodata );    // no data value

            nrows = this->geoid_model.n_rows;
            ncols = this->geoid_model.n_cols;

            return  header;
        }


        /**
         * @brief get_header - return the header info for the @see Isgemgeoid model
         * @param nrows - return the number of the rows in data
         * @param ncols - return the number of the cols in data
         * @return stl vector container with 7 elements orderd in:
         * -# index 0 - minimum value of the latitude
         * -# index 1 - maximum value of the latitude
         * -# index 2 - minimum value of the longitude
         * -# index 3 - maximum value of the longitude
         * -# index 4 - latitude step
         * -# index 5 - longitude step
         * -# index 6 - nodata value
         */
        template<typename eT>
        vector<eT> get_header( unsigned& nrows, unsigned& ncols) const {
            vector<eT> header(7); // bmin, bmax, lmin, lmax, deltab, deltal, nodata

            header[0] = static_cast<eT>( this->lat_min ); // minimum value of the latitude
            header[1] = static_cast<eT>( this->lat_max ); // maximum value of the latitude
            header[2] = static_cast<eT>( this->lon_min ); // minimum value of the longitude
            header[3] = static_cast<eT>( this->lon_max ); // maximum value of the longitude
            header[4] = static_cast<eT>( this->delta_lat ); // latitude step
            header[5] = static_cast<eT>( this->delta_lon ); // longitude step
            header[6] = static_cast<eT>( this->nodata );    // no data value

            nrows = this->geoid_model.n_rows;
            ncols = this->geoid_model.n_cols;

            return  header;
        }

        /**
         * @brief resample_model - Creates a new model from existing one. The new "isgemgeoid" model is created
         * from the existing model with new boundaries and steps. The values for the new model are calculated
         * as the arithmetic mean from the old one. The "nodata" values are ignored. From this fact, an unexpected
         * behaviour could be observed near the areas with "nodata" values.
         * @param n_lat_min - a boundary value for the derived model
         * @param n_lat_max - a boundary value for the derived model
         * @param n_lon_min - a boundary value for the derived model
         * @param n_lon_max - a boundary value for the derived model
         * @param n_dlat - a new step value for the derived model
         * @param n_dlon - a new step value for the derived model
         * @param algorithm - an algorithm used to interpolate data in grid. Nearest neighbour is the default option.
         * @return the resampled model derived from the existing one using new parameters
         * @see Isgemgeoid
         */
        Isgemgeoid resample_model (double &n_lat_min,
                                   double &n_lat_max,
                                   double &n_lon_min,
                                   double &n_lon_max,
                                   double &n_dlat,
                                   double &n_dlon,
                                   string algorithm = "nearest" ) ;


        /**
         * @brief set_print_coeff set if the ceofficients of the model should be passed to ofstream <<
         * @param in_print_coeff
         */
        void set_print_coeff ( bool in_print_coeff);

        /**
         * @brief save2hdf5
         * @param path
         * @param matrix name
         * @return - not flipping the data!!!
         */
        bool save2hdf5(const string& path , const string &matrix_name);

        /**
         * @brief read_hdf5
         * @param path
         * @return
         */
        bool read_hdf5( const string& path );

        /**
         * @brief g2qg_separation = Geoid 2 quasigeoid separation : Computes the geoid and
         * quasigeoid separation based on article published  by: Lars. E. Sjoberg -
         * A strict formula for geoid-to-quasigeoid separation
         *
         *
         *                    \Delta g_P^BO        V_g^T - V_P^T      \delta g^BO - \delta_P^BO
         * N - \Zeta \approx --------------- H + ----------------- + ---------------------------
         *                       \gamma              \gamma                      \gamma
         *
         * \Delta g_P^BO - refined Bouguer gravity anomaly
         * \delta_P^BO   - gravity disturbance
         * V_g^T         - topographic potential at the geoid
         * V_P^T         - topographic potential at the surface point
         * \delta g^BO   - mean value of the gravity disturbance between sea level and
         *                 the height of the computation point
         *
         *
         * @param protocol
         * @param f_out
         * @return
         */
        bool g2qg_separation( const char* protocol,
                              ofstream f_out,
                              arma::mat dg_pbo,
                              arma::mat delta,
                              arma::mat v_gt,
                              arma::mat v_pt,
                              arma::mat dg_bo);

        /**
         * @brief compute_slope - calculate slope dor DEM/other model based on ArcGis algorithm
         * http://desktop.arcgis.com/en/arcmap/10.3/tools/spatial-analyst-toolbox/how-slope-works.htm
         *
         * For each cell, the Slope tool calculates the maximum rate of change in value from that cell
         * to its neighbors. Basically, the maximum change in elevation over the distance between
         * the cell and its eight neighbors identifies the steepest downhill descent from the cell.
         *
         * Conceptually, the tool fits a plane to the z-values of a 3 x 3 cell neighborhood around
         * the processing or center cell. The slope value of this plane is calculated using the
         * average maximum technique (see References). The direction the plane faces is the aspect
         * for the processing cell. The lower the slope value, the flatter the terrain; the higher
         * the slope value, the steeper the terrain.
         *
         * If there is a cell location in the neighborhood with a NoData z-value, the z-value of
         * the center cell will be assigned to the location. At the edge of the raster, at least
         * three cells (outside the raster's extent) will contain NoData as their z-values. These
         * cells will be assigned the center cell's z-value. The result is a flattening of the
         * 3 x 3 plane fitted to these edge cells, which usually leads to a reduction in the slope.
         *
         * The submatrix 3x3
         * a | b | c
         * ---------
         * d | e | f
         * ---------
         * g | h | i
         *
         * \f$ \frac{\Delta z}{\Delta x} = \frac{(c + 2f + i) - (a + 2d + g)}{(8 * x_{cellsize}} \f$
         * \f$ \frac{\Delta z}{\Delta z} = \frac{(g + 2h + i) - (a + 2b + c)}{(8 * y_{cellsize}} \f$
         *
         * \f$ slope = \sqrt{ \left(  \frac{\Delta z}{\Delta x} \right)^2 + \left(  \frac{\Delta z}{\Delta y} \right)^2   } \f$
         *
         * @param ell - ellipsoid for calculation the x,y cellsize (latitude, longitude)
         * @return - raster in unitless format
         */
        Isgemgeoid compute_slope( geo_f::ellipsoid<double> ell );


        void see_matrix_size() {
            cout << geoid_model.n_rows << " " << geoid_model.n_cols << endl;
        }

        /**
         * @brief gdist_molodenskyG1 - returns raster model from integration
         * \f$ 	\delta g_1 = \frac{R^2}{2 \pi} \iint \limits_\sigma
                                         \frac{h- h_p}{l_0^3}
                                         \left[
                                         \delta g(\phi^\prime , \lambda^\prime ) -
                                               \frac{1}{8 \pi}  \iint \limits_\sigma \delta g(\phi^\prime , \lambda^\prime )
                                                       H( \psi ) \mathrm{d} \sigma
                                         \right]      \mathrm{d} \sigma \f$
         * @param sphrad - radius of the refeerence sphere
         * @param intradius - integration radius
         * @param terrain - terrain model - DEM
         * @param ell - reference ellipsoid
         * @param phi_bound - [phi_min , phi_max]
         * @return
         */
        Isgemgeoid gdist_molodenskyG1(const double& sphrad ,
                                      const double& intradius ,
                                      const bool &check_radius,
                                      const bool &extend_data,
                                      const Isgemgeoid& terrain ,
                                      geo_f::ellipsoid<double>& ell,
                                      const vector<double>& phi_bound);

        /**
         * @brief convert_T0_to_deltag_prime - the integral solution obtained from function @see hotine_integral ,
         * return the $\f \delta g^\prime  = \delta g - \frac{1}{8 \pi} \iint_\sigma  \delta g H(\psi) \mathrm \sigma \f$.
         * Because the return value from function @see hotine_integral is disturbing potential, then it can be rewritten into
         * $\f \delta g^\prime  = \delta g - \frac{T_0}{2 R} \f$ .
         * @param sphrad - radius of the reference sphere
         * @param gdist - original value of the gravity disturbance
         * @return the input values for the @see gdist_molodenskyG1
         */
        Isgemgeoid convert_T0_to_deltag_prime ( const double& sphrad , Isgemgeoid gdist);

        /**
         * @brief Isgemgeoid::molodensky_G1term - computes the \f$ G_1 \f$ term in Molodensky theory, the following equations are given
         *  \f$ G_1 = \frac{R^2}{2\pi} \iint_\sigma \frac{H - H_p}{l^3} \left( \Delta g + \frac{3 }{2R} T_0 \right) \mathrm{d} \sigma \f$ ,
         * where \f$ H, H_P \f$ are Molodensky normal heights, \f$ l \f$ Euclidian distance between the computation points,  \$ \gamma \f$ normal gravity
         * and \f \zeta_0 \f$ approximate height anomaly computed from \f$T_0 \f$.
         * @param sphrad - radius of the reference sphere used
         * @param intradius - integration radius
         * @param t0 matrix with corresponding size ( @see nrows, @see ncols ) with the values of the \textit{T} value,
         * approximate value of the disturbing potential.
         * @param bouguer matrix with corresponding size ( @see nrows, @see ncols ) with the values of the Bouguer anomaly
         * @return
         */
        Isgemgeoid molodensky_G1term(const double &sphrad ,
                                     const double &intradius ,
                                     const bool &check_radius,
                                     const bool &extend_data ,
                                     const Isgemgeoid &terrain,
                                     const Isgemgeoid &tpot0 ,
                                     geo_f::ellipsoid<double> &ell,
                                     const vector<double>& phi_bound);


        /**
         * @brief stokes_integral - for single point solution
         * @param sphrrad - radius of the reference sphere
         * @param intradius
         * @param ell
         *
         */
         void stokes_integral( const double& sphrad,
                               const double& intradius,
                               arma::mat& coords,
                               geo_f::ellipsoid<double> ell );

         /**
         * @brief stokes_integral \f$ \zeta = \frac{R}{4 \pi \gamma} \\int_\Omega ( \Delta g_{FA} + G_1 ) S (\psi ) \mathrm{d}\Omega \f$,
         * where \f$ \gamma \f$ is normal gravity on telluroid, \f$ \Delta g_{FA} \f$ is the Molodensky free air gravity
         * anomaly on the surface of the topography, \f$ S(\psi)\f$ is the Stoke's kernel, and \f$ G_1 \f$ is the first-order Molodensky
         * gravity correction term.
         * @param sphrad - radius of the reference sphere
         * @param intradius - integration radius
         * @param check_radius - check the integration radius
         * @param extend_data - because the convolution and the fact than we are dealing with the sphere the extention of the data is
         * that the same gravity data are aare copied in the front and to the back of the original data.
         * @param remove_bouguer_shell
         * @param terrain
         * @param ell - rotational ellipsoid
         * @param phi_bound - limitations for the computations in geodetic latitude
         * @return
         */
        Isgemgeoid stokes_integral (const double &sphrad,
                                    const double &intradius,
                                    const bool& check_radius,
                                    const bool& extend_data,
                                    const bool &remove_bouguer_shell,
                                    Isgemgeoid terrain,
                                    geo_f::ellipsoid<double> ell,
                                    double (&kernel_fun)(double),
                                    double (&kernel_fun_int)(double),
                                    const vector<double>& phi_bound);

        /**
         * @brief kernel_spectral_form
         * @param sphrad
         * @param intradius
         * @param check_radius
         * @param extend_data
         * @param ell
         * @param nmin
         * @param nmax
         * @param kernel_type
         * @param phi_bound
         * @return
         */
        Isgemgeoid kernel_spectral_form( const double& sphrad,
                                         const double& intradius,
                                         const bool& check_radius,
                                         const bool& extend_data,
                                         geo_f::ellipsoid<double> ell,
                                         const unsigned& nmin,
                                         const unsigned& nmax,
                                         const string& kernel_type,
                                         const vector<double>& phi_bound);

        /**
         * @brief hotine_integral
         * @param sphrad - radius of the reference sphere
         * @param intradius - integration radius
         * @param check_radius - check the integration radius
         * @param extend_data - because the convolution and the fact than we are dealing with the sphere the extention of the data is
         * that the same gravity data are aare copied in the front and to the back of the original data.
         * @param ell  - rotational ellipsoid
         * @param phi_bound - limitations for the computations in geodetic latitude
         * @return
         */
        Isgemgeoid hotine_integral(const double &sphrad, // [m]
                                   const double &intradius, // [m]
                                   const bool& check_radius,
                                   const bool& extend_data,
                                   geo_f::ellipsoid<double> ell,
                                   double (&kernel_fun)(double),
                                   double (&kernel_fun_int)(double),
                                   const vector<double>& phi_bound);

        /**
         * @brief hotine_kernel_integral - calculate the hotine integral function all around the globe
         * \f$ \iint \limits_\sigma H(\psi) \mathrm{d} \sigma \f$, function is just for testing a hypothesis,
         * not a numerical contribution to gravity field modelling.
         * @param dphi
         * @param dlam
         * @param ell - reference rotational ellipsoid
         * @return
         */
        Isgemgeoid hotine_kernel_integral( const double& dphi ,
                                           const double& dlam ,
                                           geo_f::ellipsoid<double> ell,
                                           double (&kernel_fun)(double),
                                           double (&kernel_fun_int)(double));

        /**
         * @brief convert_tpot2qg - convert the Isgemgeoid model containing the disturbance gravity raster
         * to Molodensky height anomaly. Tide system transformation is from article:  Robert Tenzer &
         * Viliam Vatrt & Ahmed Abdalla & Nadim Dayoub \"Assessment of the LVD offsets for the
         * normal-orthometric heights and different permanent tide systems—a case study of New Zealand\".
         * @param terrain - ellipsoidal heights
         * @param tide_from - tide system conversion from tidal displacement
         * @param tide_to - to tidal system
         * @param k - tidal Love number
         * @param h - tidal Love number
         * @param ell - reference ellipsoid
         * @param deltaT - difference in potential , for example \f$ \Delta T = W_0 - U_0 \f$ , where \f$ U_0 \f$
         * is normal potential of the reference ellipsoid, \f$ W_0 \f$ is geopotential value on geoid.
         */
        bool convert_tpot2qg(Isgemgeoid terrain ,
                             const string& tide_from ,
                             const string& tide_to ,
                             const double &k,
                             const double &h,
                             geo_f::ellipsoid<double> ell ,
                             double deltaT);

        /**
         * @brief bouguer_shell
         * @param geometry
         * @param limit
         * @param quantity
         * @param sphrad
         * @param intradius
         * @return
         */
        Isgemgeoid bouguer_shell( const string& geometry ,
                                  const string& limit,
                                  const string& quantity,
                                  const double& sphrad,
                                  const double& intradius);

        /**
         * @brief cutoff_rows_columns
         * @param nmin
         * @param nmax
         * @param mmin
         * @param mmax
         * @return
         */
        Isgemgeoid cutoff_rows_columns (unsigned &nmin,
                                        unsigned &nmax,
                                        unsigned &mmin,
                                        unsigned &mmax );

        /**
         * @brief set_header - set_new_header_information
         * @param n_model_name
         * @param n_model_type
         * @param n_units
         * @param n_ref_ell
         * @param n_isg_format
         * @param n_lat_min
         * @param n_print_coeff
         */
        void set_header ( const string& n_model_name,
                          const string& n_model_type,
                          const string& n_units,
                          const string& n_ref_ell,
                          const string& n_isg_format,
                          const bool&   n_print_coeff);

        /**
         * @brief get_value
         * @param i
         * @param j
         * @return
         */
        double get_value(int i, int j);

        /**
         * @brief get_value
         * @param i
         * @param j
         * @return
         */
        double get_value(int i, int j) const;

        const string& get_name() {
            return this->model_name;
        }

        const string& get_name() const {
            return this->model_name;
        }




        ///////////////////////////////////////////////////////////////////////
        // Overloading operators                                             //
        ///////////////////////////////////////////////////////////////////////
        /**
         * @brief operator << can be used for stream output, if the coefficients are
         * needed to be displayed the  @see print_coeff needs to be set to true
         * @param stream
         * @param model
         * @return
         */
        friend ostream& operator<<(ostream& stream, Isgemgeoid model) {
            stream << "begin_of_head ================================================\n";
            stream << "model name : " << model.model_name   << endl;
            stream << "model type : " << model.model_type   << endl;
            stream << "units      : " << model.units        << endl;
            stream << "ref_ell    : " << model.ref_ell      << endl;
            stream << "ISG format = " << model.isg_format   << endl;
            stream << "lat min    = " << model.lat_min      << endl;
            stream << "lat max    = " << model.lat_max      << endl;
            stream << "lon min    = " << model.lon_min      << endl;
            stream << "lon max    = " << model.lon_max      << endl;
            stream << "nodata     = " << model.nodata       << endl;
            stream << "delta lat  = " << model.delta_lat    << endl;
            stream << "delta lon  = " << model.delta_lon    << endl;
            stream << "nrows      = " << model.nrows        << endl;
            stream << "ncols      = " << model.ncols        << endl;
            stream << "refsph [m] = " << setprecision(10) << model.sphererad    << endl;
            stream << "end_of_head ==================================================\n";

            if ( model.print_coeff ) {
                arma::mat gmodel_out =  arma::flipud( model.geoid_model );
                gmodel_out.raw_print(stream); // http://arma.sourceforge.net/docs.html#print
            }
          return stream;
        }

private:
        /**
         * @brief handle_no_data
         * @param subgrid
         * @return arithmetic mean value from subgrid ignoring nodata values
         */
        double handle_no_data( arma::mat subgrid );

        /**
         * @brief conv2index - convert difference between lat/lon of an arbitraty point
         * and lat_min/lon_min to index as unsigned int
         * @param xval - number in double precision representing \f$ \frac{\varphi - \varphi_{min}}{\Delta_\varphi} \f$
         * @return index for matrix or vector manipulation
         */
        unsigned int conv2index( double xval );


        string model_name; ///< Name of the model
        string model_type; ///< Type of the model. E.g. geoid/quasigeoid and the
                                ///< type of geoid such as gravimetric, hybrid, GNSS/leveling.
        string units;      ///< Units used by model (meetres, feets, ...)
        string ref_ell;    ///< Reference ellipsoid.
        string isg_format; ///< Version of the isgem format.
        bool print_coeff = false; ///< Value that is used for function @see operator<<
                                  ///< for printing the parameters of the Isgemgeoid.
                                  ///< If @param print_coeff is set to true, the ceofficients
                                  ///< are also displayed.
        double lat_min;           ///< Minimum value of geodetic latitude.
        double lat_max;           ///< Maximum value of geodetic latitude.
        double lon_min;           ///< Minimum value of geodetic longitude.
        double lon_max;           ///< Maximum value of geodetic longitude.
        double nodata;            ///< Value that is used for areas where are
                                  ///< no data available. Usually value -9999.999

        double sphererad = 0.0;   ///< Radius of the reference sphere used in Hotine/Molodensky integral
        double delta_lat;         ///< Distance between the two parallels in the model.
        double delta_lon;         ///< Distance between the two meridians in the model.
        unsigned int nrows;       ///< Number of the rows
        unsigned int ncols;       ///< Number of the columns
        arma::mat geoid_model;    ///< Data of the Isgemgeoid model
};

#endif // ISGEMGEOID_H
