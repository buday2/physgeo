#ifndef GGFM_H
#define GGFM_H

#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <map>
#include <typeinfo>
#include <utility>
#include <cstddef>
#include <cstdio>
#include <iomanip>
#include <istream>
#include <ostream>
#include <new>

// OpenPragma Library
#include <omp.h>

// hfd5
#include <hdf5.h>

// include armadillo
#define ARMA_USE_CXX11
#define ARMA_USE_OPENMPs
#define ARMA_USE_HDF5
#include <armadillo>

// own libraries
#include "geodetic_functions.h"
#include "physical_constants.h"
#include "fnalfs.h"
#include "progressbar.h"
#include "lvec.h"
#include "ggfm_meat.h"
#include "tmat.h"
#include "isgemgeoid.h"
#include "phys_math.h"

using namespace std;

inline bool f_exists(const char* fname) {
  if (access(fname, F_OK) != -1) {
    return true;
  } else {
    return false;
  }
}

/**
 * @brief The Ggfm class - class that is computing different quantities from global gravity field models. Using classes @see Lvec
 * for storing Stoke's coefficients and @see FnALFs to compute fully normalized accossiated Legendre polynomials. Currenctly available
 * quantities ( can be computed from model ):
 * -# gravity potential ( @see gpot_v , @see gpot_v_grid )
 * -# gravitational potential ( @see gpot_w , @see gpot_w_grid )
 * -# disturbace potential ( @see gpot_t , @see gpot_t_grid )
 * -# height anomaly ( @see height_anom, @see height_anom_grod)
 * -# deflection of the vertical \f$ \xi \f$ ( @see def_vert_xi, @see def_vert_xi_grid )
 * -# deflection of the vertical \f$ \eta \f$ ( @see def_vert_eta, @see def_vert_eta_grid )
 * -# total value of the defleciton of the vertival (@see def_vertical, @see def_vertical_grid)
 * -# \$ \frac{\partial V}{\partial x} ( @see grav_x , @see grav_x_grid )
 * -# \$ \frac{\partial V}{\partial y} ( @see grav_y , @see grav_y_grid )
 * -# \$ \frac{\partial V}{\partial z} ( @see grav_z , @see grav_z_grid )
 * -# gravity anomaly spherical approximation (@see grav_anom_sa, @see grav_anom_sa_grid)
 * -# gravity disturbace ( @see grav_dist_sa, @see grav_dist_sa_grid)
 * -# magnitude of the gravity vector (@see grav_vec_mag, @see grav_vec_mag_grid )
 */
template<typename eT=double>
class Ggfm {
 public:
    Ggfm() {/*ctor*/}
    ~Ggfm(){}

  /**
  * @param path - path to model stored on disk drive
  * @param alg - algorithm that will be used for calculation
  * @param overwriteC00term - if it's true the C_{00} is set to 0
  * @see
  */
    void icgem_loadggm(const string& path, const string& alg, bool overwriteC00term = true) {
  /* loading geopotential model from standardized txt file from ICGEM-postdam
   * input is only path, recommended is to use double precision
   * struct_type is needed, because compiler is not able to deduce argument from
   * string only
   */

        bool t = f_exists(path.c_str());
       // bool is_gfc = true;
        if (t == false) {
            cout << "File \"" << path.c_str() << "\" does not exist." << endl;
            // return ggm;
        } else {
            this->algorithm = alg;

            if ( this->algorithm.compare( "tmfrm" ) == 0 ) { // method removed!
                this->col_order = true;
            } else {
                this->col_order = false;
            }


            n_min = 99999;

            ifstream f((path).c_str());

            vector<string> v;
            string line, gfc, word;
            unsigned int n, m;
            eT cnm, snm, sig_c_cm, sig_s_nm;
            string data_reading = "header";

            while (getline(f, line)) {
                if (data_reading == "header") {
                    istringstream is(line);  // load whole line
                    // new string declared "gfc    "
                    while (is >> word) {
                        v.push_back(word);
                    }  // spliting <string> line to separate words


                    if (v[0] == "modelname" || line.find("Model name") != string::npos) {
                        model_name = v.back().c_str();
                    } else if (v[0] == "max_degree" ||  line.find("Max degree") != string::npos) {
                        this->n_max = stoi( v.back().c_str());

                        this->c_nm.resize( this->n_max , col_order); // - resize the Lvec<eT> to store the Stokes coefficients
                        this->s_nm.resize( this->n_max , col_order);

                    } else if (v[0] == "earth_gravity_constant" || line.find("Model's GM") != string::npos ) {
                        replace(v.back().begin(), v.back().end(), 'D', 'e');
                        gm = static_cast<eT>( stod( v.back().c_str()) );
                    } else if (v[0] == "radius" || line.find("Model's radius") != string::npos) {
                        replace(v.back().begin(), v.back().end(), 'D', 'e');
                        radius = static_cast<eT>( stod( v.back().c_str()) );
                    } else if (v[0] == "errors") {
                        errors = (v[1]).c_str();
                    } else if (v[0] == "norm") {
                        normalized = (v[1]).c_str();
                    } else if (v[0] == "tide_system") {
                        if ((v[1] == "zero_tide") || (v[1] == "mean_tide") ||
                                (v[1] == "tide_free")) {
                            tide_system = (v[1]).c_str();
                        } else {
                            tide_system = (v[1] + "_" + v[2]).c_str();
                        }
                    } else if (v[0] == "end_of_head" || line.find("end_of_head") != string::npos) {
                        data_reading = "coefficients";
                    }  // else { continue; };

                    v.clear();
                    word.clear();
                    line.clear();
                } else if (data_reading == "coefficients") {
                    replace(line.begin(), line.end(), 'D', 'e');  // replace all 'D' to 'e'
                    istringstream is(line);

//                    if ( is_gfc ) {
//                        try {
//                            is >> n >> m >> cnm >> snm >> sig_c_cm >> sig_s_nm;
//                        } catch (...) {
//                            is_gfc = false;
//                            is >> n >> m >> cnm >> snm >> sig_c_cm >> sig_s_nm;
//                        }
//                    } else {
//                        is >> n >> m >> cnm >> snm >> sig_c_cm >> sig_s_nm;
//                    }
                    is >> gfc >> n >> m >> cnm >> snm >> sig_c_cm >> sig_s_nm;
                    n_min = (n_min > n) ? n : n_min;

                    c_nm(n,m) = cnm; // c_nm(n,m) = c_nm;
                    s_nm(n,m) = snm; // s_nm(n,m) = s_nm;
                }
            }
            f.close();  // closing ggm<icgem> file

            if ( overwriteC00term ) {
                c_nm[0] = 1.0; // c_nm(0,0) = 1. ;
            }
        }
    }

    void save2hdf5 ( const string& path ) {
        arma::vec c_nm_arma( c_nm.nelem() );
        arma::vec s_nm_arma( s_nm.nelem() );

        for ( unsigned i = 0; i < c_nm.nelem(); i++) {
            c_nm_arma(i) = this->c_nm[i];
            s_nm_arma(i) = this->s_nm[i];
        }

        c_nm_arma.save ( arma::hdf5_name( path, "c_nm", arma::hdf5_opts::append ) );
        s_nm_arma.save ( arma::hdf5_name( path, "s_nm", arma::hdf5_opts::append ) );

        // Saving header to separate file //
        size_t lastindex = path.find_last_of(".");
        string path2header = path.substr(0, lastindex);
        path2header+=".header";

        ofstream fheader; fheader.open(  path2header , ios_base::trunc);

        if ( fheader.is_open() ) {
            fheader << "begin_of_head ================================================\n";
            fheader << "Model name     :  " << this->model_name << endl;
            fheader << "Product type   :  " << this->product_type << endl;
            fheader << "Error type     :  " << this->errors << endl;
            fheader << "Normalized     :  " << this->normalized << endl;
            fheader << "Tide system    :  " << this->tide_system << endl;
            fheader << "Model's radius := " << setw(20) << setprecision(15) << this->radius << endl;
            fheader << "Model's GM     := " << setw(20) << setprecision(15) << this->gm << endl;
            fheader << "Max degree     := " << this->n_max << endl;
            fheader << "Min degree     := " << this->n_min << endl;
            fheader << "end_of_head ==================================================\n";
        } else {
            cerr << "Something goes wrong with  void Ggfm::save2hdf5 ( const string& path )!" << endl;
        }
        fheader.close();
    }

    /**
     * @brief load_hdf5_model
     * @param path2data - path to hdf5 file with the coeffcients, in that same directory the *.header file must be placed. The vector
     * of \f$ C_nm, S_nm \f$ coefficients is ordered in following order : \f$ [P_{00}, P_{10}, P_{20}, P_{30}, \ldots ,  P_{n_{max} 0 }, P_{11}, P_{21}, P_{31} , \ldots ,  P_{n_{max} 1 }, P_{22} , \ldots  ] \f$.
     */
    void load_hdf5_model( const string& path2data , const string& algtype)
    {
        // set the algorithm:
        if ( (algtype == "tmfcm" ) || ( algtype == "tmfcm_2nd") || algtype == "standard" || algtype == "stan_mod") {
            this->algorithm = algtype;
        } else {
            cout << "Ggfm::load_hdf5_model unknown type of computation algorithm. Algorith is se to \"standard\".";
            this->algorithm = "standard";
        }

        arma::vec arma_cnm;
        arma::vec arma_snm;

        if ( f_exists( path2data.c_str() ) ) {
            arma_cnm.load( arma::hdf5_name( path2data, "c_nm" ) );
            arma_snm.load( arma::hdf5_name( path2data, "s_nm" ) );
        } else {
            cerr << "Can not find the Ggfm model in hdf5 format. File not found \"" << path2data << "\"" << endl;
        }

        // loading header info
        size_t lastindex = path2data.find_last_of(".");
        string path2header = path2data.substr(0, lastindex);
        path2header+=".header";

        if ( f_exists( path2header.c_str() ) ) {
            ifstream header_in; header_in.open( path2header );

            if ( header_in.is_open() ) {
                string line, gfc, word;
                vector<string> v;
                //size_t found;

                while ( getline( header_in , line ) ) {

                    istringstream is(line);  // load whole line
                    // new string declared "gfc    "
                    while (is >> word) {
                        v.push_back(word);
                    }

                    size_t v_len = v.size();

                    if ( line.rfind( "Model name") != string::npos ||
                         line.rfind( "model name") != string::npos) {
                        // loading model name
                        this->model_name = v[ v_len-1 ];
                    } else if ( line.rfind( "Product type") != string::npos ||
                                line.rfind( "product type") != string::npos ) {
                        // loading product type
                        this->product_type = v[ v_len-1 ];
                    } else if ( line.rfind( "Error type") != string::npos ||
                                line.rfind( "error type") != string::npos ) {
                        // are errors also attached
                        this->errors = v[ v_len-1 ];
                    } else if ( line.rfind( "Normalized") != string::npos ) {
                        this->normalized = v[ v_len-1 ];
                    } else if ( line.rfind( "Tide system") != string::npos ) {
                        // loading tide ssytem
                        this->tide_system = v[ v_len-1 ];
                    } else if ( line.rfind( "Model's radius") != string::npos ||
                                line.rfind( "model's radius") != string::npos ) {
                        // loading model radius
                        replace(v[v_len-1].begin(), v[v_len-1].end(), 'D', 'e');

                        this->radius = static_cast<eT>(stod( v[ v_len-1 ] ));
                    } else if ( line.rfind( "Model's GM") != string::npos  ) {
                        // loading model's GM
                        replace(v[v_len-1].begin(), v[v_len-1].end(), 'D', 'e');

                        this->gm = static_cast<eT>(stod( v[ v_len-1 ] ));
                    } else if ( line.rfind( "Max degree") != string::npos  ||
                                line.rfind( "max degree") != string::npos ) {
                        this->n_max = static_cast<unsigned int>( stoi( v[ v_len-1 ] ) );
                    } else if ( line.rfind( "Min degree") != string::npos  ||
                                line.rfind( "min degree") != string::npos  ) {
                        this->n_min = static_cast<unsigned int>( stoi( v[ v_len-1 ] ) );
                    }
                    v.clear();
                }
            header_in.close();

            this->c_nm.resize( this->n_max ,false);
            this->s_nm.resize( this->n_max ,false);

            for (unsigned int i = 0; i < this->c_nm.nelem(); i++) {
                this->c_nm[i] = arma_cnm(i);
                this->s_nm[i] = arma_snm(i);
            }
            cout << "HDF5 model successfully loaded!\n";
        } else {
            cerr << "Can not find the Ggfm model in hdf5 format. File not found \"" << path2header << "\"" << endl;
            cout << "Empty header file was created \"" << path2header << "\"." << endl;
            ofstream fheader; fheader.open(  path2header , ios_base::trunc);

            if ( fheader.is_open() ) {
                fheader << "begin_of_head ================================================\n";
                fheader << "Model name     :  " << endl;
                fheader << "Product type   :  " << endl;
                fheader << "Error type     :  " << endl;
                fheader << "Normalized     :  " << endl;
                fheader << "Tide system    :  " << endl;
                fheader << "Model's radius := " << endl;
                fheader << "Model's GM     := " << endl;
                fheader << "Max degree     := " << endl;
                fheader << "Min degree     := " << endl;
                fheader << "end_of_head ==================================================\n";
            } else {
                cerr << "Something goes wrong with  void Ggfm::save2hdf5 ( const string& path )!" << endl;
            }
            fheader.close();
            throw runtime_error( "Ggfm::load_hdf5_model could not load Ggfm in hdf5 format." );
        }
      }
    }

  /**
  * @brief Computes the gravitational potential (no cetrifugal force) at point P. The input coordinates of an arbitrary point P
  * are geodetic coordinates above certain ellipsoid (e.g. wgs84, grs80, etc. )
  * The value is given by eqation:
  * \f[
    V(r, \varphi, \lambda) = \frac{GM}{r}\sum_{n=n_{min}}^{n_{max}} \left(\frac{R}{r}\right)^{n} \sum_{m=0}^{n} (\bar{C}_{n,m}\cos m\lambda+\bar{S}_{n,m}\sin m\lambda)\bar{P}_{n,m}(\sin \varphi)
    \f]
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravitational potential (no cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
  */
    eT gpot_v( const eT& phi,
               const eT& lambda,
               const eT& height,
               const unsigned int& min_n,
               const unsigned int& max_n,
               const string& tide_mode,
               const eT& ln,
               const geo_f::ellipsoid<eT>& ell) {

        vector<eT> ruv = geo_f::blh2ruv(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates
        //FnALFs legen(max_n, 0);
        FnALFs<eT> legen(max_n, algorithm, 0);
        legen.compute_FnALFs(ruv[1]);

        eT v_pot = sum_stokes(ruv[0], ruv[1], ruv[2], phi, min_n,
                max_n, tide_mode, ln, ell, legen.ref2p_nm(),
                ggfm_f::n_multiply0<eT>, ggfm_f::plus_sum<eT>);

        return  v_pot * gm / ruv[0];
    }

  /**
  * @brief Computes the gravity potential (with cetrifugal force) at point P. The input coordinates of an arbitrary point P
  * are geodetic coordinates above certain ellipsoid (e.g. wgs84, grs80, etc. )
  * The value is given by eqation:
  * \f[
    W(r, \varphi, \lambda) =\frac{GM}{r} \sum_{n=n_{min}}^{n_{max}} \left(\frac{R}{r}\right)^{n}\sum_{m=0}^{n} (\bar{C}_{n,m}\cos m\lambda+\bar{S}_{n,m}\sin m\lambda)\bar{P}_{n,m}(\sin\varphi)+\frac{1}{2}\omega^{2}r^{2}\cos^{2}\varphi
   \f]
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
  */
    eT gpot_w( const eT& phi,
               const eT& lambda,
               const eT& height,
               const unsigned int& min_n,
               const unsigned int& max_n,
               const string& tide_mode,
               const eT& ln,
               const geo_f::ellipsoid<eT>& ell) {

        vector<eT> ruv = geo_f::blh2ruv<eT>(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates

        eT v_potential = gpot_v(phi, lambda, height, min_n,
                                          max_n, tide_mode, ln, ell);
        return v_potential +
                .5 * pow(ell.omega * ruv[0] * cos(ruv[1] * DEG2RAD), 2.0);
    }


  /**
   * @brief gpot_t -Computes the disturbing gravity potential  at point P. The input coordinates of an arbitrary point P
  * are geodetic coordinates above certain ellipsoid (e.g. wgs84, grs80, etc. ). From the gravity potential the normal gravity field
  * is substracted.
  * \f[
    T(r, \varphi, \lambda) =\frac{GM}{r} \sum_{n=n_{min}}^{n_{max}} \left(\frac{R}{r}\right)^{n}\sum_{m=0}^{n} (\Delta\bar{C}_{n,m}\cos m\lambda+\Delta\bar{S}_{n,m}\sin m\lambda)\bar{P}_{n,m}(\sin\varphi)
    \f]
    ,where \f$ \Delta\bar{C}_{n,m}, \Delta\bar{S}_{n,m} \f$ are given by equations:
    \f$ \Delta\bar{S}_{m,n} =\bar{S}_{m,n}-\bar{S}_{m,n}^{Ell} \frac{GM^{Ell}}{GM}  \left(\frac{a^{Ell}}{R}\right)^{n}=\bar{S}_{m,n} \f$
    \f$ 	\Delta\bar{C}_{m,n}^{Ell} =
    \left\{
    \begin{tabular}{l} (-1)^{n}\frac{3e^{2n}}{(2n+1)(2n+3)\sqrt{4n+1}}\left(1-n-5^{3/2}n\frac{\bar{C}^{Ell}_{2,0}}{e^{2}}\right)  \text{if}\; n=0,2,4,\ldots,20,\; m=0 \\ 0  \text{else}
    \end{tabular} \right
    \f$

  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
    eT gpot_t(  const eT& phi,
                const eT& lambda,
                const eT& height,
                const unsigned int& min_n,
                const unsigned int& max_n,
                const string& tide_mode,
                const eT& ln,
                const geo_f::ellipsoid<eT>& ell) {

        vector<eT> ruv = geo_f::blh2ruv<eT>(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates
        FnALFs<eT> legen(max_n, algorithm, 0);
        legen.compute_FnALFs(ruv[1]);

        eT t_potential = Ggfm<eT>::sum_stokes_nnf(
                    ruv[0], ruv[1], ruv[2], phi, min_n, max_n, tide_mode, ln, ell,
                legen.ref2p_nm(), ggfm_f::n_multiply0<eT>, ggfm_f::plus_sum<eT>);

        return t_potential * gm / ruv[0];
    }


  /**
   * @brief gpot_v_grid - returns value of gravitational potential (no cetrifugal force) for points in grid (@see gpot_v )
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
  */
    arma::mat gpot_v_grid( const eT& b_min,
                           const eT& b_max,
                           const eT& l_min,
                           const eT& l_max,
                           const eT& height,
                           const eT& bstep,
                           const eT& lstep,
                           const unsigned int& min_n,
                           const unsigned int& max_n,
                           const string& tide_mode,
                           const eT& ln,
                           const geo_f::ellipsoid<eT>& ell,
                           ofstream &out_stream,
                           bool save2file = true )
    {

        void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                              const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                              vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients;

        arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                     min_n,
                                                                     max_n,
                                                                     0,
                                                                     tide_mode,
                                                                     ln,
                                                                     ell,
                                                                     ggfm_f::n_multiply0<eT>,
                                                                     ggfm_f::plus_lstokes1<eT>,
                                                                     mltiply_stokes_leg );

        for (unsigned int i = 0; i < grid_data.n_rows; i++) {
            eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
            // Loop for parallel computations
            #pragma omp parallel for shared( u, grid_data , gm)
            for (unsigned int j = 0; j < grid_data.n_cols; j++) {
                eT v = l_min + ( static_cast<eT>(j)  * lstep ); // actual longitude , spherical long. == ell. long
                vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
                grid_data(i, j) *=  gm / ruv[0];
            }
        }

        if ( save2file ) {
            try {
                Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                             grid_data, min_n, max_n , tide_mode , ln, ell, "m^2*s^-2");

            } catch  ( ... ) {
                cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_v_grid().\"" << endl;
            }
        }

        return grid_data;
    }

    /**
     * @brief gpot_v_ongrid #TODO#description
     * @param terrain
     * @param min_n
     * @param max_n
     * @param taylor_order
     * @param tide_mode
     * @param ln
     * @param ell
     * @param crds_system - spherical or ellipsoidal coordinates,{sph,ell}, ell by default
     * @param out_stream
     * @param save2file
     * @return
     */
    arma::mat gpot_v_ongrid( const Isgemgeoid& terrain,
                             const unsigned& min_n,
                             const unsigned& max_n,
                             const unsigned& taylor_order,
                             const string& tide_mode,
                             const eT& ln,
                             const geo_f::ellipsoid<eT>& ell,
                             const string& crds_system,
                             ofstream &out_stream,
                             bool save2file = true)
    {
        void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
                                         const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
                                         const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
                                       = &Ggfm<eT>::compute_lumped_coeff_taylor;

        arma::mat grid_data = ongrid_computation_template( terrain , // terrain model
                                                       min_n, max_n,
                                                       0,            // derivative order of fnALFs
                                                       taylor_order, // const unsigned& taylor_order,
                                                       0 ,           // const unsigned& start_k,
                                                       false ,       // const bool& even,
                                                       1 ,           // const unsigned& init_r_power,
                                                       tide_mode,    // tide system
                                                       ln,           // love number ofr tide_system conversion
                                                       ell,          // ellipsoid
                                                       crds_system,  // spherical or ellipsoidal coordinates,{sph,ell}, ell by default
                                                       ggfm_f::n_multiply0<eT>,
                                                       ggfm_f::plus_lstokes1<eT>,
                                                       taylor_lumped
                                                       );
        if ( save2file ) {
            try {
                unsigned rows, cols;
                vector<eT> header = terrain.get_header<eT>(rows, cols);
                Ggfm<eT>::print_grid_header( out_stream, header[0], header[1], header[2], header[3],
                                             0, header[4], header[5],
                                             grid_data, min_n, max_n , tide_mode , ln, ell, "m^2*s^-2");

            } catch  ( ... ) {
                cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_v_ongrid().\"" << endl;
            }
        }

        return grid_data;
    }

  /**
   * @brief gpot_w_grid returns value of gravity potential (with cetrifugal force) for points in grid (@see gpot_w )
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
    arma::mat gpot_w_grid( const eT& b_min,
                           const eT& b_max,
                           const eT& l_min,
                           const eT& l_max,
                           const eT& height,
                           const eT& bstep,
                           const eT& lstep,
                           const unsigned int& min_n,
                           const unsigned int& max_n,
                           const string& tide_mode,
                           const eT& ln,
                           const geo_f::ellipsoid<eT>& ell,
                           ofstream &out_stream,
                           bool save2file = true )
    {

        void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                              const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                              vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients;

        arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                     min_n,
                                                                     max_n,
                                                                     0,
                                                                     tide_mode,
                                                                     ln,
                                                                     ell,
                                                                     ggfm_f::n_multiply0<eT>,
                                                                     ggfm_f::plus_lstokes1<eT>,
                                                                     mltiply_stokes_leg );

        for (unsigned int i = 0; i < grid_data.n_rows; i++) {
            eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
            // Loop for parallel computations
            #pragma omp parallel for shared( u, grid_data , gm)
            for (unsigned int j = 0; j < grid_data.n_cols; j++) {
                eT v = l_min + ( static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
                vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
                grid_data(i, j) *=  gm / ruv[0];
                grid_data(i, j) +=  .5 * pow(ell.omega * ruv[0] * cos(ruv[1] * DEG2RAD), 2.0);
            }
        }

        if (  save2file ) {
            try {
                Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                             grid_data, min_n, max_n , tide_mode , ln, ell, "m^2*s^-2");

            } catch  ( ... ) {
                cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_v_grid().\"" << endl;
            }
        }

        return grid_data;
    }

    /**
     * @brief gpot_w_ongrid #TODO#description
     * @param terrain
     * @param min_n
     * @param max_n
     * @param taylor_order
     * @param tide_mode
     * @param ln
     * @param ell
     * @param out_stream
     * @param save2file
     * @return
     */
    arma::mat gpot_w_ongrid( const Isgemgeoid& terrain,
                             const unsigned& min_n,
                             const unsigned& max_n,
                             const unsigned& taylor_order,
                             const string& tide_mode,
                             const eT& ln,
                             const geo_f::ellipsoid<eT>& ell,
                             const string& crds_system,
                             ofstream &out_stream,
                             bool save2file = true)
    {

        arma::mat grid_data = this->gpot_v_ongrid( terrain,
                                                   min_n,
                                                   max_n,
                                                   taylor_order,
                                                   tide_mode,
                                                   ln,
                                                   ell,
                                                   crds_system,
                                                   out_stream,
                                                   false);
        unsigned rows, cols;
        vector<eT> header = terrain.get_header<eT>(rows, cols);
        eT b_min = header[0];
        eT l_min = header[2];
        eT bstep = header[4];
        eT lstep = header[5];
        eT nodat = header[6];

        for (unsigned int i = 0; i < grid_data.n_rows; i++) {
            eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
            // Loop for parallel computations
            #pragma omp parallel for shared( u, grid_data , gm)
            for (unsigned int j = 0; j < grid_data.n_cols; j++) {
                eT dr = terrain.get_value(i,j);

                if ( dr == nodat ) {
                    grid_data(i, j) = nodat;
                    continue;
                }

                eT v = l_min + ( static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
                vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, dr, ell  );
                grid_data(i, j) +=  .5 * pow(ell.omega * ruv[0] * cos(ruv[1] * DEG2RAD), 2.0);
            }
        }


        if ( save2file ) {
            try {
                Ggfm<eT>::print_grid_header( out_stream, header[0], header[1], header[2], header[3],
                                             0, header[4], header[5],
                                             grid_data, min_n, max_n , tide_mode , ln, ell, "m^2*s^-2");

            } catch  ( ... ) {
                cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_v_ongrid().\"" << endl;
            }
        }

        return grid_data;
    }

  /**
   * @brief gpot_t_grid returns value of disturbing potential for points in grid (@see gpot_t )
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
    arma::mat gpot_t_grid( const eT& b_min,
                           const eT& b_max,
                           const eT& l_min,
                           const eT& l_max,
                           const eT& height,
                           const eT& bstep,
                           const eT& lstep,
                           const unsigned int& min_n,
                           const unsigned int& max_n,
                           const string& tide_mode,
                           const eT& ln,
                           const geo_f::ellipsoid<eT>& ell,
                           ofstream &out_stream,
                           bool save2file = true )
    {
        void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                              const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                              vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients_nnf;


        arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                     min_n,
                                                                     max_n,
                                                                     0,
                                                                     tide_mode,
                                                                     ln,
                                                                     ell,
                                                                     ggfm_f::n_multiply0<eT>,
                                                                     ggfm_f::plus_lstokes1<eT>,
                                                                     mltiply_stokes_leg );


        for (unsigned int i = 0; i < grid_data.n_rows; i++) {
            eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
            // Loop for parallel computations
            #pragma omp parallel for shared( u, grid_data, gm )
            for (unsigned int j = 0; j < grid_data.n_cols; j++) {
                eT v = l_min + ( static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
                vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
                grid_data(i, j) *=  gm / ruv[0];
            }
        }

        if ( save2file ) {
            try {
                Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                             grid_data, min_n, max_n , tide_mode , ln, ell,  "m^2*s^-2");
            } catch  ( ... ) {
                cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_v_grid().\"" << endl;
            }
        }
        return grid_data;
    }

    /**
     * @brief gpot_t_ongrid #TODO#description
     * @param terrain
     * @param min_n
     * @param max_n
     * @param taylor_order
     * @param tide_mode
     * @param ln
     * @param ell
     * @param out_stream
     * @param save2file
     * @return
     */
    arma::mat gpot_t_ongrid( const Isgemgeoid& terrain,
                             const unsigned& min_n,
                             const unsigned& max_n,
                             const unsigned& taylor_order,
                             const string& tide_mode,
                             const eT& ln,
                             const geo_f::ellipsoid<eT>& ell,
                             const string& crds_system,
                             ofstream &out_stream,
                             bool save2file = true)
    {
        void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
                                         const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
                                         const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
                                       = &Ggfm<eT>::compute_lumped_coeff_taylor_nnf;

        arma::mat grid_data = this->ongrid_computation_template( terrain , // terrain model
                                                       min_n, max_n,
                                                       0,            // derivative order of fnALFs
                                                       taylor_order, // const unsigned& taylor_order,
                                                       0 ,           // const unsigned& start_k,
                                                       false ,        // const bool& even,
                                                       1 ,           // const unsigned& init_r_power,
                                                       tide_mode,    // tide system
                                                       ln,           // love number ofr tide_system conversion
                                                       ell,          // ellipsoid
                                                       crds_system,
                                                       ggfm_f::n_multiply0<eT>,
                                                       ggfm_f::plus_lstokes1<eT>,
                                                       taylor_lumped
                                                       );


        if ( save2file ) {
            try {
                unsigned rows, cols;
                vector<eT> header = terrain.get_header<eT>(rows, cols);
                Ggfm<eT>::print_grid_header( out_stream, header[0], header[1], header[2], header[3],
                                             0, header[4], header[5],
                                             grid_data, min_n, max_n , tide_mode , ln, ell, "m^2*s^-2");

            } catch  ( ... ) {
                cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_t_ongrid().\"" << endl;
            }
        }
        return grid_data;
    }

    /**
     * @brief gpot_t_r_ongrid #TODO#description first radial derivation
     * @param terrain
     * @param min_n
     * @param max_n
     * @param taylor_order
     * @param tide_mode
     * @param ln
     * @param ell
     * @param out_stream
     * @param save2file
     * @return
     */
    arma::mat gpot_t_rr_ongrid( const Isgemgeoid& terrain,
                             const unsigned& min_n,
                             const unsigned& max_n,
                             const unsigned& taylor_order,
                             const string& tide_mode,
                             const eT& ln,
                             const geo_f::ellipsoid<eT>& ell,
                             const string& crds_system,
                             ofstream &out_stream,
                             bool save2file = true)
    {
        void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
                                         const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
                                         const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
                                       = &Ggfm<eT>::compute_lumped_coeff_taylor_nnf;

        arma::mat grid_data = this->ongrid_computation_template( terrain , // terrain model
                                                       min_n, max_n,
                                                       0,            // derivative order of fnALFs
                                                       taylor_order, // const unsigned& taylor_order,
                                                       2 ,           // const unsigned& start_k,
                                                       false ,        // const bool& even,
                                                       3 ,           // const unsigned& init_r_power,
                                                       tide_mode,    // tide system
                                                       ln,           // love number ofr tide_system conversion
                                                       ell,          // ellipsoid
                                                       crds_system,
                                                       ggfm_f::n_multiply2<eT>,
                                                       ggfm_f::plus_lstokes1<eT>,
                                                       taylor_lumped
                                                       ) * SI2EOTVOS;


        if ( save2file ) {
            try {
                unsigned rows, cols;
                vector<eT> header = terrain.get_header<eT>(rows, cols);
                Ggfm<eT>::print_grid_header( out_stream, header[0], header[1], header[2], header[3],
                                             0, header[4], header[5],
                                             grid_data, min_n, max_n , tide_mode , ln, ell, "Eotvos");

            } catch  ( ... ) {
                cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_t_ongrid().\"" << endl;
            }
        }
        return grid_data;
    }

  /**
  * @brief height_anom - Theheight anomaly \f$ \zeta (\varphi, ,\lambda) \f$, the well known approximation of the geoid undulation according to Molodensky’s theory,
  * can be defined by the distance from the Earth’s surface to the point where the normal potential \f$U\f$ has the same value
  * as the geopotentialWat the Earth’s surface.
  * \f$
    \zeta(r, \varphi, \lambda) = \frac{1}{\gamma(0,\varphi)} \cdot \left( \frac{GM}{r} \sum_{n=n_{min}}^{n_{max}} \left(\frac{R}{r}\right)^{n}\sum_{m=0}^{n} (\Delta\bar{C}_{n,m}\cos m\lambda+\Delta\bar{S}_{n,m}\sin m\lambda)\bar{P}_{n,m}(\sin\varphi) \right)
    \f$
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
    eT height_anom (const eT& phi,
                    const eT& lambda,
                    const eT& height,
                    const unsigned int& min_n,
                    const unsigned int& max_n,
                    const string& tide_mode,
                    const eT& ln,
                    const geo_f::ellipsoid<eT>& ell) {

        vector<eT> ruv = geo_f::blh2ruv(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates

        eT t_potential = Ggfm::gpot_t(phi, lambda, height, min_n,
                                      max_n, tide_mode, ln, ell);

        eT gamma = geo_f::compute_av_normal_gravity<eT>(phi, height, ell);
        return t_potential / gamma;
    }

  /**
   * @brief height_anom_grid - returns value of disturbing potential for points in grid (@see height_anom )
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
    arma::mat height_anom_grid(const eT& b_min,
                               const eT& b_max,
                               const eT& l_min,
                               const eT& l_max,
                               const eT& height,
                               const eT& bstep,
                               const eT& lstep,
                               const unsigned int& min_n,
                               const unsigned int& max_n,
                               const string& tide_mode,
                               const eT& ln,
                               const geo_f::ellipsoid<eT>& ell,
                               ofstream &out_stream,
                               bool save2file = true )
    {

        void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                              const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                              vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients_nnf;


        arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                     min_n,
                                                                     max_n,
                                                                     0,
                                                                     tide_mode,
                                                                     ln,
                                                                     ell,
                                                                     ggfm_f::n_multiply0<eT>,
                                                                     ggfm_f::plus_lstokes1<eT>,
                                                                     mltiply_stokes_leg );

        for (unsigned int i = 0; i < grid_data.n_rows; i++) {
            eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
            // Loop for parallel computations
            eT gamma = geo_f::compute_av_normal_gravity<eT>(u, height, ell);
            #pragma omp parallel for shared( u, grid_data , gm, gamma)
            for (unsigned int j = 0; j < grid_data.n_cols; j++) {
                eT v = l_min + ( static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
                vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
                grid_data(i, j) *=  gm / (ruv[0] * gamma);
            }
        }

        if ( save2file ) {
            try {
                Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                             grid_data, min_n, max_n , tide_mode , ln, ell, "m");
            } catch  ( ... ) {
                cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_v_grid().\"" << endl;
            }
        }
        return grid_data;
    }

    /**
     * @brief height_anom_ongrid #TODO# description
     * @param terrain
     * @param min_n
     * @param max_n
     * @param taylor_order
     * @param tide_mode
     * @param ln
     * @param ell
     * @param out_stream
     * @param save2file
     * @return
     */
    arma::mat height_anom_ongrid(const Isgemgeoid& terrain,
                                 const unsigned& min_n,
                                 const unsigned& max_n,
                                 const unsigned& taylor_order,
                                 const string& tide_mode,
                                 const eT& ln,
                                 const geo_f::ellipsoid<eT>& ell,
                                 const string& crds_system,
                                 ofstream &out_stream,
                                 bool save2file = true)
    {

        arma::mat grid_data = this->gpot_t_ongrid( terrain,
                                                   min_n,
                                                   max_n,
                                                   taylor_order,
                                                   tide_mode,
                                                   ln,
                                                   ell,
                                                   crds_system,
                                                   out_stream,
                                                   false);

        unsigned i1,i2;
        vector<eT> header = terrain.get_header<eT>(i1,i2);

        eT b_min = header[0];
        eT b_max = header[1];
        eT l_min = header[2];
        eT l_max = header[3];
        eT bstep = header[4];
        eT lstep = header[5];
        eT nodat = header[6];


        for (unsigned int i = 0; i < grid_data.n_rows; i++) {
            eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
            // Loop for parallel computations

            #pragma omp parallel for shared( u, grid_data , gm)
            for (unsigned int j = 0; j < grid_data.n_cols; j++) {
                eT dr = terrain.get_value(i,j);

                if ( dr == nodat ) {
                    grid_data(i, j) = nodat;
                    continue;
                }

                eT gamma = geo_f::compute_av_normal_gravity<eT>(u, dr, ell);
                grid_data(i, j) /= gamma;
            }
        }

        if ( save2file ) {
            try {
                Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, 0, bstep, lstep,
                                             grid_data, min_n, max_n , tide_mode , ln, ell, "m");
            } catch  ( ... ) {
                cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::height_anom_ongrid().\"" << endl;
            }
        }
        return grid_data;
    }

  /**
  * @brief grav_dist_sa
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
    eT grav_dist_sa(const eT& phi,
                    const eT& lambda,
                    const eT& height,
                    const unsigned int& min_n,
                    const unsigned int& max_n,
                    const string& tide_mode,
                    const eT& ln,
                    const geo_f::ellipsoid<eT>& ell) {
        vector<eT> ruv = geo_f::blh2ruv(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates

        FnALFs<eT> legen(max_n, algorithm, 0);
        legen.compute_FnALFs(ruv[1]);

        eT gdsa = Ggfm<eT>::sum_stokes_nnf(
                    ruv[0], ruv[1], ruv[2], phi, min_n, max_n, tide_mode, ln, ell,
                legen.ref2p_nm(), ggfm_f::n_multiply1<eT>, ggfm_f::plus_sum<eT>);

        return (gdsa * gm / (ruv[0] * ruv[0]));
    }

  /**
  * @brief grav_anom_sa - the spherical approximation \f$ \delta g_{sa} \f$ of the gravity anomaly , i.e. the (negative) radial derivative of the disturbing potential T,
  * calculated from a spherical harmonic expansion of \f$T\f$
        \f$
        \delta g_{sa}(r\varphi,\lambda)=-\frac{\partial T(r,\varphi,\lambda)}{\partial r} =
        =\frac{GM}{r^{2}}\sum_{n=n_{min}}^{n_{max}} \left(\frac{R}{r}\right)^{n}(n+1)\sum_{m=0}^{n} (\Delta\bar{C}_{n,m}\cos m\lambda+\Delta\bar{S}_{n,m}\sin m\lambda)\bar{P}_{n,m}(\sin\varphi)
        \f$
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
    eT grav_anom_sa(const eT& phi,
                    const eT& lambda,
                    const eT& height,
                    const unsigned int& min_n,
                    const unsigned int& max_n,
                    const string& tide_mode,
                    const eT& ln,
                    const geo_f::ellipsoid<eT>& ell)   {
        vector<eT> ruv = geo_f::blh2ruv(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates

        FnALFs<eT> legen(max_n, algorithm, 0);
        legen.compute_FnALFs(ruv[1]);

        eT gdsa = Ggfm<eT>::sum_stokes_nnf(
                           ruv[0], ruv[1], ruv[2], phi, min_n, max_n, tide_mode, ln, ell,
                          legen.ref2p_nm(), ggfm_f::n_multiply3<eT>, ggfm_f::plus_sum<eT>);

        return gdsa * gm / (ruv[0] * ruv[0]);
    }

    /**
  * @brief grav_anom_sa_r - first radial derivative of the gravity anomaly
        \f$
        \partial_r \delta g_{sa}(r\varphi,\lambda)=-\frac{GM}{r^{3}} \sum_m \cos( m \lambda) \sum_n (n-1)(n+2) Qc_{nm} + \win( m \lambda) \sum_n (n-1)(n+2) Qs_{nm}
        \f$,
        where \f$ Qc = (R/r)^n P_{nm} \Delta C_{nm} \f$, \f$ Qs \f$ is the same but \f$ C_{nm} \f$ is replaced by \f$ \S_{nm} \f$
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
     */
    eT grav_anom_sa_r(const eT& phi,
                      const eT& lambda,
                      const eT& height,
                      const unsigned int& min_n,
                      const unsigned int& max_n,
                      const string& tide_mode,
                      const eT& ln,
                      const geo_f::ellipsoid<eT>& ell)   {
          vector<eT> ruv = geo_f::blh2ruv(
                      phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
          // to spherical coordinates

          FnALFs<eT> legen(max_n, algorithm, 0);
          legen.compute_FnALFs(ruv[1]);

          eT gdsa = Ggfm<eT>::sum_stokes_nnf(
                             ruv[0], ruv[1], ruv[2], phi, min_n, max_n, tide_mode, ln, ell,
                            legen.ref2p_nm(), ggfm_f::n_multiply4<eT>, ggfm_f::plus_sum<eT>);

          return -gdsa * gm / (ruv[0] * ruv[0] * ruv[0]);
   }

    /**
  * @brief grav_anom_sa_rr - second radial derivative of the gravity anomaly
        \f$
        \partial_r \delta g_{sa}(r\varphi,\lambda)=\frac{GM}{r^{4}} \sum_m \cos( m \lambda) \sum_n (n-1)(n+2)(n+3) Qc_{nm} + \win( m \lambda) \sum_n (n-1)(n+2)(n+3) Qs_{nm}
        \f$,
        where \f$ Qc = (R/r)^n P_{nm} \Delta C_{nm} \f$, \f$ Qs \f$ is the same but \f$ C_{nm} \f$ is replaced by \f$ \S_{nm} \f$
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
     */
    eT grav_anom_sa_rr(const eT& phi,
                      const eT& lambda,
                      const eT& height,
                      const unsigned int& min_n,
                      const unsigned int& max_n,
                      const string& tide_mode,
                      const eT& ln,
                      const geo_f::ellipsoid<eT>& ell)   {
          vector<eT> ruv = geo_f::blh2ruv(
                      phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
          // to spherical coordinates

          FnALFs<eT> legen(max_n, algorithm, 0);
          legen.compute_FnALFs(ruv[1]);

          eT gdsa = Ggfm<eT>::sum_stokes_nnf(
                             ruv[0], ruv[1], ruv[2], phi, min_n, max_n, tide_mode, ln, ell,
                            legen.ref2p_nm(), ggfm_f::n_multiply5<eT>, ggfm_f::plus_sum<eT>);

          return -gdsa * gm / (ruv[0] * ruv[0] * ruv[0]);
   }

  /**
  * @brief def_vert_xi - The standard definitions of the components of the deflections of the vertical using an approximation based on a sphere of unit radius.
  * The \f$ \xi \f$ is defined as \f$ \xi = \Phi - \varphi, \f$ where \f$ \Phi \f$ is astronomic latitude and \f$ \varphi \f$  is geodetic latitude.
  * From global gravity field models the value can be obtained as:
    \f$
    \eta(r,\varphi,\lambda)=-\frac{1}{r\gamma(r,\varphi)\cos\varphi}\frac{\partial T(r\varphi,\lambda)}{\partial \lambda}=
    -\frac{GM}{r^{2}\gamma(r,\varphi)\cos\varphi}\sum_{n=n_{min}}^{n_{max}} \left(\frac{R}{r}\right)^{n}\sum_{m=0}^{n} (\Delta\bar{S}_{n,m}\cos m\lambda-\Delta\bar{C}_{n,m}\sin m\lambda)m\bar{P}_{n,m}(\sin\varphi)
    \f$
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
    eT def_vert_xi(const eT& phi,
                   const eT& lambda,
                   const eT& height,
                   const unsigned int& min_n,
                   const unsigned int& max_n,
                   const string& tide_mode,
                   const eT& ln,
                   const geo_f::ellipsoid<eT>& ell)   {

        vector<eT> ruv = geo_f::blh2ruv(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates
        FnALFs<eT> legen(max_n, algorithm, 1);
        legen.compute_FnALFs(ruv[1]);

        eT xi = Ggfm<eT>::sum_stokes_nnf(
                    ruv[0], ruv[1], ruv[2], phi, min_n, max_n, tide_mode, ln, ell,
                legen.ref2dp_nm(), ggfm_f::n_multiply0<eT>, ggfm_f::plus_sum<eT>);

        eT gamma = geo_f::compute_av_normal_gravity<eT>(phi, height, ell);
        return (-xi * gm / (ruv[0] * ruv[0] * gamma)) * RAD2SEC;
    }

  /**
  * @brief def_vert_eta - The standard definitions of the components of the deflections of the vertical using an approximation based on a sphere of unit radius.
  * The \f$ \xi \f$ is defined as \f$ \eta = (\Lambda - \lambda) \cdot \cos \varphi, \f$ where \f$ \Lambda \f$ is astronomic longitude and \f$ \lambda \f$  is geodetic longitude,
  * \f$ \varphi \f$ is geodetic latitude.
    \f$
    \xi(r,\varphi,\lambda)=-\frac{1}{r\gamma(r,\varphi)}\frac{\partial T(r\varphi,\lambda)}{\partial \varphi}=
    -\frac{GM}{r^{2}\gamma(r,\varphi)}\sum_{n=n_{min}}^{n_{max}} \left(\frac{R}{r}\right)^{n}\sum_{m=0}^{n} (\Delta\bar{C}_{n,m}\cos m\lambda+\Delta\bar{S}_{n,m}\sin m\lambda)\frac{\mathrm{d}\bar{P}_{n,m}(\sin(\varphi))}{\mathrm{d}\varphi}
    \f$
  * From global gravity field models the value can be obtained as:
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
    eT def_vert_eta(const eT& phi,
                    const eT& lambda,
                    const eT& height,
                    const unsigned int& min_n,
                    const unsigned int& max_n,
                    const string& tide_mode,
                    const eT& ln,
                    const geo_f::ellipsoid<eT>& ell)   {
        vector<eT> ruv = geo_f::blh2ruv(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates
        FnALFs<eT> legen(max_n, algorithm, 0);
        legen.compute_FnALFs(ruv[1]);

        eT eta = Ggfm::sum_stokes_nnf(
                    ruv[0], ruv[1], ruv[2], phi, min_n, max_n, tide_mode, ln, ell,
                legen.ref2p_nm(), ggfm_f::n_multiply0<eT>, ggfm_f::minus_sum2<eT>);

        eT gamma = geo_f::compute_av_normal_gravity(phi, height, ell);
        return (-eta * gm / (ruv[0] * ruv[0] * gamma * cos(ruv[1] * DEG2RAD))) * RAD2SEC;
    }


  /**
  * @brief def_vertical -  The standard definitions of the components of the deflections of the vertical using an approximation based on a sphere of unit radius. This
  * function returns the total value (magnitude of the vector) for deflection of the vertical defined as:
    \f$
    \Theta = \sqrt{ \eta^2 + \xi^2 }.
    \f$
  * @see def_vert_xi, @see def_vert_eta.
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
    eT def_vertical(const eT& phi,
                    const eT& lambda,
                    const eT& height,
                    const unsigned int& min_n,
                    const unsigned int& max_n,
                    const string& tide_mode,
                    const eT& ln,
                    const geo_f::ellipsoid<eT>& ell)  {
        vector<eT> ruv = geo_f::blh2ruv(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates
        FnALFs<eT> legen(max_n, algorithm, 1);
        legen.compute_FnALFs(ruv[1]);

        eT xi = Ggfm<eT>::sum_stokes_nnf(
                    ruv[0], ruv[1], ruv[2], phi,  min_n, max_n, tide_mode, ln, ell,
                legen.ref2dp_nm(), ggfm_f::n_multiply0<eT>, ggfm_f::plus_sum<eT>);
        eT eta = Ggfm<eT>::sum_stokes_nnf(
                    ruv[0], ruv[1], ruv[2], phi,  min_n, max_n, tide_mode, ln, ell,
                legen.ref2p_nm(), ggfm_f::n_multiply0<eT>, ggfm_f::minus_sum2<eT>);

        eT gamma = geo_f::compute_av_normal_gravity<eT>(phi, height, ell);
        xi *= -gm / (ruv[0] * ruv[0] * gamma);
        eta*= -gm / (ruv[0] * ruv[0] * gamma * cos(ruv[1] * DEG2RAD));

        return sqrt(xi * xi + eta * eta) * RAD2SEC;
    }

    /**
     * @brief gpot_vr - derivation of the gravity potential (no centrifugal potential)
     * #TODO# desription
     * @param phi
     * @param lambda
     * @param height
     * @param min_n
     * @param max_n
     * @param tide_mode
     * @param ln
     * @param ell
     * @return
     */
    eT gpot_vr (const eT &phi,
                const eT &lambda,
                const eT &height,
                const unsigned int &min_n,
                const unsigned int &max_n,
                const string& tide_mode,
                const eT &ln,
                const geo_f::ellipsoid<eT> &ell) {
        vector<eT> ruv = geo_f::blh2ruv<eT>(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates
        FnALFs<eT> legen(max_n, algorithm, 0);
        legen.compute_FnALFs(ruv[1]);

        eT v_r = Ggfm<eT>::sum_stokes(ruv[0], ruv[1], ruv[2], phi, min_n,
                max_n, tide_mode, ln, ell, legen.ref2p_nm(),
                ggfm_f::n_multiply1<eT>, ggfm_f::plus_sum<eT>);

        // vc - derivation of the centrifugal potential
        return (-gm * v_r / (ruv[0] * ruv[0]) );
    }

    /**
     * @brief gpot_vr - derivation of the gravity potential (no centrifugal potential)
     * #TODO# desription
     * @param phi
     * @param lambda
     * @param height
     * @param min_n
     * @param max_n
     * @param tide_mode
     * @param ln
     * @param ell
     * @return
     */
    eT gpot_vrr (const eT &phi,
                 const eT &lambda,
                 const eT &height,
                 const unsigned int &min_n,
                 const unsigned int &max_n,
                 const string& tide_mode,
                 const eT &ln,
                 const geo_f::ellipsoid<eT> &ell) {
        vector<eT> ruv = geo_f::blh2ruv<eT>(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates
        FnALFs<eT> legen(max_n, algorithm, 0);
        legen.compute_FnALFs(ruv[1]);

        eT v_r = Ggfm<eT>::sum_stokes(ruv[0], ruv[1], ruv[2], phi, min_n,
                max_n, tide_mode, ln, ell, legen.ref2p_nm(),
                ggfm_f::n_multiply2<eT>, ggfm_f::plus_sum<eT>);

        // vc - derivation of the centrifugal potential
        return (gm * v_r / (ruv[0] * ruv[0] * ruv[0]) );
    }

  /**
  * @brief grav_gx - \f$ \frac{\partial W}{\partial \varphi} \f$
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
    eT grav_gx(const eT &phi,
               const eT &lambda,
               const eT &height,
               const unsigned int &min_n,
               const unsigned int &max_n,
               const string& tide_mode,
               const eT &ln,
               const geo_f::ellipsoid<eT> &ell)  {

        vector<eT> ruv = geo_f::blh2ruv<eT>(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates

        FnALFs<eT> legen(max_n, algorithm, 1);
        legen.compute_FnALFs(ruv[1]);

        eT v_phi = Ggfm<eT>::sum_stokes(ruv[0], ruv[1], ruv[2], phi, min_n,
                max_n, tide_mode, ln, ell, legen.ref2dp_nm(),
                ggfm_f::n_multiply0<eT>, ggfm_f::plus_sum<eT>);

        eT vc_phi = -1 * ell.omega*ell.omega*ruv[0]*ruv[0]*cos(ruv[1] * DEG2RAD) *
                sin(ruv[1] * DEG2RAD);

        // vc - derivation of the centrifugal potential
        return (v_phi + vc_phi) / ruv[0];
    }

  /**
  * @brief grav_gy
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
    eT grav_gy(const eT& phi,
               const eT& lambda,
               const eT& height,
               const unsigned int& min_n,
               const unsigned int& max_n,
               const string& tide_mode,
               const eT& ln,
               const geo_f::ellipsoid<eT>& ell)  {
        ///< TODO: derivative according to \f$ \lambda \f$ is not correct

        vector<eT> ruv = geo_f::blh2ruv<eT>(
                    phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
        // to spherical coordinates
        FnALFs<eT> legen(max_n, algorithm, 0);
        legen.compute_FnALFs(ruv[1]);

        eT v_l = Ggfm<eT>::sum_stokes(ruv[0], ruv[1], ruv[2], phi, min_n,
                max_n, tide_mode, ln, ell, legen.ref2p_nm(),
                ggfm_f::n_multiply1<eT>,
                ggfm_f::minus_sum2<eT>);  ///< TODO this sum

        return (-v_l / (ruv[0] * cos(ruv[1] * DEG2RAD)));
    }

  /**
   * @brief grav_gz
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
  eT grav_gz(const eT& phi,
             const eT& lambda,
             const eT& height,
             const unsigned int& min_n,
             const unsigned int& max_n,
             const string& tide_mode,
             const eT& ln,
             const geo_f::ellipsoid<eT>& ell)  {

      vector<eT> ruv = geo_f::blh2ruv<eT>(
                  phi, lambda, height, ell);  // converting geodetic ellipsoidal coordinates
      // to spherical coordinates
      FnALFs<eT> legen(max_n, algorithm, 0);
      legen.compute_FnALFs(ruv[1]);

      eT v_r = Ggfm<eT>::sum_stokes(ruv[0], ruv[1], ruv[2], phi, min_n,
              max_n, tide_mode, ln, ell, legen.ref2p_nm(),
              ggfm_f::n_multiply1<eT>, ggfm_f::plus_sum<eT>);

      eT vc_r = ell.omega * ell.omega * ruv[0] * cos(ruv[1] * DEG2RAD) *
              cos(ruv[1] * DEG2RAD);
      // vc - derivation of the centrifugal potential
      return (-gm / (ruv[0] * ruv[0]) * v_r + vc_r);
  }

  /**
  * @brief grav_vec_mag
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of gravity potential (with cetrifugal force) at point P (see equation above).
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
  eT grav_vec_mag(const eT& phi,
                  const eT& lambda,
                  const eT& height,
                  const unsigned int& min_n,
                  const unsigned int& max_n,
                  const string& tide_mode,
                  const eT& ln,
                  const geo_f::ellipsoid<eT>& ell )  {
      vector<eT> ruv = geo_f::blh2ruv<eT>( phi, lambda, height, ell);
      // converting geodetic ellipsoidal coordinates
      // to spherical coordinates

      FnALFs<eT> legen(max_n, algorithm, 1);
      legen.compute_FnALFs(ruv[1]);


      eT v_phi = Ggfm<eT>::sum_stokes(ruv[0], ruv[1], ruv[2], phi, min_n,
              max_n, tide_mode, ln, ell, legen.ref2dp_nm(),
              ggfm_f::n_multiply0<eT>, ggfm_f::plus_sum<eT>);

      eT v_l = Ggfm<eT>::sum_stokes(ruv[0], ruv[1], ruv[2], phi, min_n,
              max_n, tide_mode, ln, ell, legen.ref2p_nm(),
              ggfm_f::n_multiply1<eT>,
              ggfm_f::minus_sum<eT>);

      eT v_r = Ggfm<eT>::sum_stokes(ruv[0], ruv[1], ruv[2], phi, min_n,
              max_n, tide_mode, ln, ell, legen.ref2p_nm(),
              ggfm_f::n_multiply1<eT>, ggfm_f::plus_sum<eT>);

      // vc - derivation of the centrifugal potential
      eT vc_r = ell.omega * ell.omega * ruv[0] * cos(ruv[1] * DEG2RAD) *
              cos(ruv[1] * DEG2RAD);
      eT vc_phi = -1. * ell.omega * ell.omega * ruv[0]* ruv[0] * cos(ruv[1] * DEG2RAD) *
              sin(ruv[1] * DEG2RAD);

      // Components of the gravity vector
      eT gy = (-v_l / (ruv[0] * cos(ruv[1] * DEG2RAD)));
      eT gx = (v_phi + vc_phi) / ruv[0];
      eT gz = (-gm / (ruv[0] * ruv[0]) * v_r + vc_r);

      return sqrt(gx * gx + gy * gy + gz * gz);
  }

  /**
   * @brief grav_dist_sa_grid
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
  arma::mat grav_dist_sa_grid(const eT& b_min,
                              const eT& b_max,
                              const eT& l_min,
                              const eT& l_max,
                              const eT& height,
                              const eT& bstep,
                              const eT& lstep,
                              const unsigned int& min_n,
                              const unsigned int& max_n,
                              const string& tide_mode,
                              const eT& ln,
                              const geo_f::ellipsoid<eT>& ell,
                              ofstream &out_stream,
                              bool save2file = true )
  {
      void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                            const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                            vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients_nnf;


      arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                   min_n,
                                                                   max_n,
                                                                   0,
                                                                   tide_mode,
                                                                   ln,
                                                                   ell,
                                                                   ggfm_f::n_multiply1<eT>,
                                                                   ggfm_f::plus_lstokes1<eT>,
                                                                   mltiply_stokes_leg );

      for (unsigned int i = 0; i < grid_data.n_rows; i++) {
          eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
          // Loop for parallel computations
          #pragma omp parallel for shared( u, grid_data , gm)
          for (unsigned int j = 0; j < grid_data.n_cols; j++) {
              eT v = l_min + (static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
              vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
              grid_data(i, j) *= gm / (ruv[0] * ruv[0]);
          }
      }

      if (save2file)  {
          try {
              Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell, "m*s^-2");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_v_grid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief grav_dist_sa_ongrid #TODO# DESCRIPTION
   * @param terrain
   * @param min_n
   * @param max_n
   * @param taylor_order
   * @param tide_mode
   * @param ln
   * @param ell
   * @param out_stream
   * @param save2file
   * @return
   */
  arma::mat grav_dist_sa_ongrid(const Isgemgeoid& terrain,
                                const unsigned& min_n,
                                const unsigned& max_n,
                                const unsigned& taylor_order,
                                const string& tide_mode,
                                const eT& ln,
                                const geo_f::ellipsoid<eT>& ell,
                                const string& crds_system,
                                ofstream &out_stream,
                                bool save2file = true)
  {
      void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
                                       const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
                                       const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
                                     = &Ggfm<eT>::compute_lumped_coeff_taylor_nnf;


      arma::mat grid_data =  this->ongrid_computation_template( terrain , // terrain model
                                                                min_n, max_n,
                                                                0,            // derivative order of fnALFs
                                                                taylor_order, // const unsigned& taylor_order,
                                                                1 ,           // const unsigned& start_k,
                                                                false ,        // const bool& even,
                                                                2 ,           // const unsigned& init_r_power,
                                                                tide_mode,    // tide system
                                                                ln,           // love number ofr tide_system conversion
                                                                ell,          // ellipsoid
                                                                crds_system,
                                                                ggfm_f::n_multiply1<eT>,
                                                                ggfm_f::plus_lstokes1<eT>,
                                                                taylor_lumped
                                                                );


      if ( save2file ) {
          try {
              unsigned rows, cols;
              vector<eT> header = terrain.get_header<eT>(rows, cols);
              Ggfm<eT>::print_grid_header(  out_stream, header[0], header[1], header[2], header[3],
                                            0, header[4], header[5],
                                            grid_data, min_n, max_n , tide_mode , ln, ell, "m*s^-2");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::grav_dist_sa_ongrid().\"" << endl;
          }
      }
      return grid_data;
  }


  /**
   * @brief grav_anom_sa_grid
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
  arma::mat grav_anom_sa_grid(const eT& b_min,
                              const eT& b_max,
                              const eT& l_min,
                              const eT& l_max,
                              const eT& height,
                              const eT& bstep,
                              const eT& lstep,
                              const unsigned int& min_n,
                              const unsigned int& max_n,
                              const string& tide_mode,
                              const eT& ln,
                              const geo_f::ellipsoid<eT>& ell,
                              ofstream &out_stream,
                              bool save2file = true )
  {
      void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                            const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                            vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients_nnf;


      arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                   min_n,
                                                                   max_n,
                                                                   0,
                                                                   tide_mode,
                                                                   ln,
                                                                   ell,
                                                                   ggfm_f::n_multiply3<eT>,
                                                                   ggfm_f::plus_lstokes1<eT>,
                                                                   mltiply_stokes_leg );


      for (unsigned int i = 0; i < grid_data.n_rows; i++) {
          eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
          // Loop for parallel computations
#pragma omp parallel for shared( u, grid_data , gm)
          for (unsigned int j = 0; j < grid_data.n_cols; j++) {
              eT v = l_min + (static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
              vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
              grid_data(i, j) *= gm / (ruv[0] * ruv[0]);
          }
      }

      if ( save2file ) {
          try {
              Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell, "m*s^-2");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::grav_anom_sa_grid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief grav_anom_sa_r_grid - first radial derivative of the gravity anomaly
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
  arma::mat grav_anom_sa_r_grid(const eT& b_min,
                              const eT& b_max,
                              const eT& l_min,
                              const eT& l_max,
                              const eT& height,
                              const eT& bstep,
                              const eT& lstep,
                              const unsigned int& min_n,
                              const unsigned int& max_n,
                              const string& tide_mode,
                              const eT& ln,
                              const geo_f::ellipsoid<eT>& ell,
                              ofstream &out_stream,
                              bool save2file = true )
  {
      void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                            const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                            vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients_nnf;


      arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                   min_n,
                                                                   max_n,
                                                                   0,
                                                                   tide_mode,
                                                                   ln,
                                                                   ell,
                                                                   ggfm_f::n_multiply4<eT>,
                                                                   ggfm_f::plus_lstokes1<eT>,
                                                                   mltiply_stokes_leg );


      for (unsigned int i = 0; i < grid_data.n_rows; i++) {
          eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
          // Loop for parallel computations
#pragma omp parallel for shared( u, grid_data , gm)
          for (unsigned int j = 0; j < grid_data.n_cols; j++) {
              eT v = l_min + (static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
              vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
              grid_data(i, j) *= -gm / (ruv[0] * ruv[0] * ruv[0]);
          }
      }

      if ( save2file ) {
          try {
              Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell, "s^-2");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::grav_anom_sa_grid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief grav_anom_sa_ongrid #TODO#description
   * @param terrain
   * @param min_n
   * @param max_n
   * @param taylor_order
   * @param tide_mode
   * @param ln
   * @param ell
   * @param out_stream
   * @param save2file
   * @return
   */
  arma::mat grav_anom_sa_ongrid(const Isgemgeoid& terrain,
                                const unsigned& min_n,
                                const unsigned& max_n,
                                const unsigned& taylor_order,
                                const string& tide_mode,
                                const eT& ln,
                                const geo_f::ellipsoid<eT>& ell,
                                const string& crds_system,
                                ofstream &out_stream,
                                bool save2file = true)
  {
      void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
                                       const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
                                       const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
                                     = &Ggfm<eT>::compute_lumped_coeff_taylor_nnf;


      arma::mat grid_data =  this->ongrid_computation_template( terrain , // terrain model
                                                                min_n, max_n,
                                                                0,            // derivative order of fnALFs
                                                                taylor_order, // const unsigned& taylor_order,
                                                                1 ,           // const unsigned& start_k,
                                                                false ,        // const bool& even,
                                                                2 ,           // const unsigned& init_r_power,
                                                                tide_mode,    // tide system
                                                                ln,           // love number ofr tide_system conversion
                                                                ell,          // ellipsoid
                                                                crds_system,
                                                                ggfm_f::n_multiply3<eT>,
                                                                ggfm_f::plus_lstokes1<eT>,
                                                                taylor_lumped
                                                                );


      if ( save2file ) {
          try {
              unsigned rows, cols;
              vector<eT> header = terrain.get_header<eT>(rows, cols);
              Ggfm<eT>::print_grid_header(  out_stream, header[0], header[1], header[2], header[3],
                                            0, header[4], header[5],
                                            grid_data, min_n, max_n , tide_mode , ln, ell, "m*s^-2");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::grav_anom_sa_ongrid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief grav_anom_sa_r_ongrid - first radial derivative of the gravity anomaly (in case of GGFM alone model Free air anomaly)
   * @param terrain
   * @param min_n
   * @param max_n
   * @param taylor_order
   * @param tide_mode
   * @param ln
   * @param ell
   * @param crds_system
   * @param out_stream
   * @param save2file
   * @return
   */
  arma::mat grav_anom_sa_r_ongrid(const Isgemgeoid& terrain,
                                const unsigned& min_n,
                                const unsigned& max_n,
                                const unsigned& taylor_order,
                                const string& tide_mode,
                                const eT& ln,
                                const geo_f::ellipsoid<eT>& ell,
                                const string& crds_system,
                                ofstream &out_stream,
                                bool save2file = true)
  {
      void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
                                       const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
                                       const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
                                     = &Ggfm<eT>::compute_lumped_coeff_taylor_nnf;


      arma::mat grid_data =  this->ongrid_computation_template( terrain , // terrain model
                                                                min_n, max_n,
                                                                0,            // derivative order of fnALFs
                                                                taylor_order, // const unsigned& taylor_order,
                                                                2 ,           // const unsigned& start_k,
                                                                true ,        // const bool& even,
                                                                3 ,           // const unsigned& init_r_power,
                                                                tide_mode,    // tide system
                                                                ln,           // love number ofr tide_system conversion
                                                                ell,          // ellipsoid
                                                                crds_system,
                                                                ggfm_f::n_multiply4<eT>,
                                                                ggfm_f::plus_lstokes1<eT>,
                                                                taylor_lumped
                                                                );


      if ( save2file ) {
          try {
              unsigned rows, cols;
              vector<eT> header = terrain.get_header<eT>(rows, cols);
              Ggfm<eT>::print_grid_header(  out_stream, header[0], header[1], header[2], header[3],
                                            0, header[4], header[5],
                                            grid_data, min_n, max_n , tide_mode , ln, ell, "s^-2");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::grav_anom_sa_ongrid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief grav_anom_sa_rr_ongrid - second radial derivative of the gravity anomaly (in case of GGFM alone model Free air anomaly)
   * @param terrain
   * @param min_n
   * @param max_n
   * @param taylor_order
   * @param tide_mode
   * @param ln
   * @param ell
   * @param crds_system
   * @param out_stream
   * @param save2file
   * @return
   */
  arma::mat grav_anom_sa_rr_ongrid(const Isgemgeoid& terrain,
                                const unsigned& min_n,
                                const unsigned& max_n,
                                const unsigned& taylor_order,
                                const string& tide_mode,
                                const eT& ln,
                                const geo_f::ellipsoid<eT>& ell,
                                const string& crds_system,
                                ofstream &out_stream,
                                bool save2file = true)
  {
      void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
                                       const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
                                       const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
                                     = &Ggfm<eT>::compute_lumped_coeff_taylor_nnf;


      arma::mat grid_data =  this->ongrid_computation_template( terrain , // terrain model
                                                                min_n, max_n,
                                                                0,            // derivative order of fnALFs
                                                                taylor_order, // const unsigned& taylor_order,
                                                                3 ,           // const unsigned& start_k,
                                                                false ,        // const bool& even,
                                                                4 ,           // const unsigned& init_r_power,
                                                                tide_mode,    // tide system
                                                                ln,           // love number ofr tide_system conversion
                                                                ell,          // ellipsoid
                                                                crds_system,
                                                                ggfm_f::n_multiply5<eT>,
                                                                ggfm_f::plus_lstokes1<eT>,
                                                                taylor_lumped
                                                                );


      if ( save2file ) {
          try {
              unsigned rows, cols;
              vector<eT> header = terrain.get_header<eT>(rows, cols);
              Ggfm<eT>::print_grid_header(  out_stream, header[0], header[1], header[2], header[3],
                                            0, header[4], header[5],
                                            grid_data, min_n, max_n , tide_mode , ln, ell, "m^-1 s^-2");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::grav_anom_sa_ongrid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief def_vert_xi_grid - @see def_vert_xi
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
  arma::mat def_vert_xi_grid(  const eT &b_min,
                               const eT &b_max,
                               const eT &l_min,
                               const eT &l_max,
                               const eT &height,
                               const eT &bstep,
                               const eT &lstep,
                               const unsigned int &min_n,
                               const unsigned int &max_n,
                               const string& tide_mode,
                               const eT &ln,
                               const geo_f::ellipsoid<eT> &ell,
                               ofstream &out_stream,
                               bool save2file = true )
  {
      void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                            const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                            vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients_nnf;

      arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                   min_n,
                                                                   max_n,
                                                                   1,
                                                                   tide_mode,
                                                                   ln,
                                                                   ell,
                                                                   ggfm_f::n_multiply0<eT>,
                                                                   ggfm_f::plus_lstokes1<eT>,
                                                                   mltiply_stokes_leg );

      for (unsigned int i = 0; i < grid_data.n_rows; i++) {
          eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
          // Loop for parallel computations
          eT gamma = geo_f::compute_av_normal_gravity<eT>(u, height, ell);
          #pragma omp parallel for shared( u, grid_data , gm, gamma)
          for (unsigned int j = 0; j < grid_data.n_cols; j++) {
              eT v = l_min + (static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
              vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
              grid_data(i, j) *= -gm / (ruv[0] * ruv[0] * gamma) * RAD2SEC;
          }
      }

      if (save2file) {
          try {
              Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell, "angle seconds \"");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::def_vert_xi_grid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief def_vert_xi_ongrid #TODO#DESCRIPTION
   * @param terrain
   * @param min_n
   * @param max_n
   * @param taylor_order
   * @param tide_mode
   * @param ln
   * @param ell
   * @param out_stream
   * @param save2file
   * @return
   */
  arma::mat def_vert_xi_ongrid(  const Isgemgeoid& terrain,
                                 const unsigned& min_n,
                                 const unsigned& max_n,
                                 const unsigned& taylor_order,
                                 const string& tide_mode,
                                 const eT& ln,
                                 const geo_f::ellipsoid<eT>& ell,
                                 const string& crds_system,
                                 ofstream &out_stream,
                                 bool save2file = true)
   {
       void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
                                        const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
                                        const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
                                      = &Ggfm<eT>::compute_lumped_coeff_taylor_nnf;

      arma::mat grid_data =  this->ongrid_computation_template( terrain , // terrain model
                                                                min_n, max_n,
                                                                1,  // derivative order of fnALFs
                                                                taylor_order,  // const unsigned& taylor_order,
                                                                1, // const unsigned& start_k,
                                                                true ,       // const bool& even,
                                                                2,  //const unsigned& init_r_power,
                                                                tide_mode,
                                                                ln,
                                                                ell,
                                                                crds_system,
                                                                ggfm_f::n_multiply0<eT>,
                                                                ggfm_f::plus_lstokes1<eT>,
                                                                taylor_lumped );

      unsigned i1,i2;
      vector<eT> header = terrain.get_header<eT>(i1,i2);

      eT b_min = header[0];      eT b_max = header[1];
      eT l_min = header[2];      eT l_max = header[3];
      eT bstep = header[4];      eT lstep = header[5];
      eT nodat = header[6];

      for (unsigned int i = 0; i < grid_data.n_rows; i++) {
          eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude

          #pragma omp parallel for shared( u, grid_data )
          for (unsigned int j = 0; j < grid_data.n_cols; j++) {
              eT dr = terrain.get_value(i,j);

              if ( dr == nodat ) {
                  grid_data(i, j) = nodat;
                  continue;
              }

              eT gamma = geo_f::compute_av_normal_gravity<eT>(u, dr, ell);
              grid_data(i, j) *= RAD2SEC/gamma;
          }
      }

      if (save2file) {
          try {
              Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, 0, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell, "angle seconds \"");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::def_vert_xi_grid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief def_vert_eta_grid
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
  arma::mat def_vert_eta_grid(const eT& b_min,
                              const eT& b_max,
                              const eT& l_min,
                              const eT& l_max,
                              const eT& height,
                              const eT& bstep,
                              const eT& lstep,
                              const unsigned int& min_n,
                              const unsigned int& max_n,
                              const string& tide_mode,
                              const eT& ln,
                              const geo_f::ellipsoid<eT>& ell,
                              ofstream &out_stream,
                              bool save2file = true )
  {
      void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                            const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                            vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients_nnf;

      arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                   min_n,
                                                                   max_n,
                                                                   0,
                                                                   tide_mode,
                                                                   ln,
                                                                   ell,
                                                                   ggfm_f::n_multiply0<eT>,
                                                                   ggfm_f::minus_lstokes2<eT>,
                                                                   mltiply_stokes_leg );

      for (unsigned int i = 0; i < grid_data.n_rows; i++) {
          eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
          eT gamma = geo_f::compute_av_normal_gravity<eT>(u, height, ell);

          // Loop for parallel computations
          #pragma omp parallel for shared( u, grid_data , gm, gamma)
          for (unsigned int j = 0; j < grid_data.n_cols; j++) {
              eT v = l_min + (static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
              vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
              grid_data(i, j) *=  (-gm / (ruv[0] * ruv[0] * gamma * cos( ruv[1] * DEG2RAD))) * RAD2SEC;
          }
      }

      if (save2file) {
          try {
              Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell,  "angle seconds \"");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::def_vert_eta_grid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief def_vert_eta_ongrid #TODO#description
   * @param terrain
   * @param min_n
   * @param max_n
   * @param taylor_order
   * @param tide_mode
   * @param ln
   * @param ell
   * @param out_stream
   * @param save2file
   * @return
   */
  arma::mat def_vert_eta_ongrid( const Isgemgeoid& terrain,
                                 const unsigned& min_n,
                                 const unsigned& max_n,
                                 const unsigned& taylor_order,
                                 const string& tide_mode,
                                 const eT& ln,
                                 const geo_f::ellipsoid<eT>& ell,
                                 const string& crds_system,
                                 ofstream &out_stream,
                                 bool save2file = true)
   {
       void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
                                        const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
                                        const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
                                      = &Ggfm<eT>::compute_lumped_coeff_taylor_nnf;

      arma::mat grid_data =  this->ongrid_computation_template( terrain , // terrain model
                                                                min_n, max_n,
                                                                0,
                                                                taylor_order,
                                                                1, // const unsigned& start_k,
                                                                true,  // const bool& even,
                                                                2, // const unsigned& init_r_power,
                                                                tide_mode,
                                                                ln,
                                                                ell,
                                                                crds_system,
                                                                ggfm_f::n_multiply0<eT>,
                                                                ggfm_f::minus_lstokes2<eT>,
                                                               taylor_lumped );

      unsigned i1,i2;
      vector<eT> header = terrain.get_header<eT>(i1,i2);

      eT b_min = header[0];
      eT b_max = header[1];
      eT l_min = header[2];
      eT l_max = header[3];
      eT bstep = header[4];
      eT lstep = header[5];
      eT nodat = header[6];

      for (unsigned int i = 0; i < grid_data.n_rows; i++) {
          eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude

          #pragma omp parallel for shared( u, grid_data )
          for (unsigned int j = 0; j < grid_data.n_cols; j++) {
              eT dr = terrain.get_value(i,j);
              eT v = l_min + (static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long

              if ( dr == nodat ) {
                  grid_data(i, j) = nodat;
                  continue;
              }

              eT gamma = geo_f::compute_av_normal_gravity<eT>(u, dr, ell);
              vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, dr, ell  );

              grid_data(i, j) *= RAD2SEC/(gamma * cos(ruv[1] * DEG2RAD )  );
          }
      }

      if (save2file) {
          try {
              Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, 0, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell, "angle seconds \"");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::def_vert_xi_grid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief def_vertical_grid
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
  arma::mat def_vertical_grid(const eT& b_min,
                              const eT& b_max,
                              const eT& l_min,
                              const eT& l_max,
                              const eT& height,
                              const eT& bstep,
                              const eT& lstep,
                              const unsigned int& min_n,
                              const unsigned int& max_n,
                              const string& tide_mode,
                              const eT& ln,
                              const geo_f::ellipsoid<eT>& ell,
                              ofstream &out_stream,
                              bool save2file = true )
  {
      arma::mat xi_mat =  Ggfm<eT>::def_vert_xi_grid( b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                      min_n, max_n, tide_mode, ln, ell, out_stream, false);
      arma::mat eta_mat = Ggfm<eT>::def_vert_eta_grid( b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                       min_n, max_n, tide_mode, ln, ell, out_stream, false);

      arma::mat grid_data =arma::sqrt(eta_mat%eta_mat + xi_mat%xi_mat);

      if (save2file) {
          try {
              Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell,  "angle seconds \"");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::def_vertical_grid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief def_vertical_ongrid #TODO#DESCRIPTION
   * @param terrain
   * @param min_n
   * @param max_n
   * @param taylor_order
   * @param tide_mode
   * @param ln
   * @param ell
   * @param out_stream
   * @param save2file
   * @return
   */
  arma::mat def_vertical_ongrid(const Isgemgeoid& terrain,
                                const unsigned& min_n,
                                const unsigned& max_n,
                                const unsigned& taylor_order,
                                const string& tide_mode,
                                const eT& ln,
                                const geo_f::ellipsoid<eT>& ell,
                                const string& crds_system,
                                ofstream &out_stream,
                                bool save2file = true )
  {
      arma::mat xi_mat =  this->def_vert_xi_ongrid( terrain , min_n, max_n, taylor_order, tide_mode,
                                                    ln, ell, crds_system, out_stream, false);
      arma::mat eta_mat = this->def_vert_eta_ongrid( terrain , min_n, max_n, taylor_order, tide_mode,
                                                     ln, ell, crds_system, out_stream, false);

      arma::mat grid_data =arma::sqrt(eta_mat%eta_mat + xi_mat%xi_mat);

      if (save2file) {
          try {
              unsigned rows, cols;
              vector<eT> header = terrain.get_header<eT>(rows, cols);
              Ggfm<eT>::print_grid_header(  out_stream, header[0], header[1], header[2], header[3],
                                            0, header[4], header[5],
                                            grid_data, min_n, max_n , tide_mode , ln, ell,  "angle seconds \"");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::def_vertical_grid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief grav_gx_grid
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
  arma::mat grav_gx_grid(  const eT &b_min,
                           const eT &b_max,
                           const eT &l_min,
                           const eT &l_max,
                           const eT &height,
                           const eT &bstep,
                           const eT &lstep,
                           const unsigned int &min_n,
                           const unsigned int &max_n,
                           const string& tide_mode,
                           const eT &ln,
                           const geo_f::ellipsoid<eT> &ell,
                           ofstream &out_streamm,
                           bool save2file = true )
  {
      void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                            const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                            vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients;

      arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                   min_n,
                                                                   max_n,
                                                                   1,
                                                                   tide_mode,
                                                                   ln,
                                                                   ell,
                                                                   ggfm_f::n_multiply0<eT>,
                                                                   ggfm_f::plus_lstokes1<eT>,
                                                                   mltiply_stokes_leg );


      for (unsigned int i = 0; i < grid_data.n_rows; i++) {
          eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
          // Loop for parallel computations
#pragma omp parallel for shared( u, grid_data , gm)
          for (unsigned int j = 0; j < grid_data.n_cols; j++) {
              eT v = l_min + (static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
              vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );

              eT vc_phi = -1 * ell.omega*ell.omega*ruv[0]*ruv[0]*cos(ruv[1] * DEG2RAD) *
                      sin(ruv[1] * DEG2RAD);
              grid_data(i, j) +=  vc_phi;
              grid_data(i, j) /=  ruv[0];
          }
      }

      if ( save2file ) {
          try {
              Ggfm<eT>::print_grid_header( out_streamm, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell, "m*s^-2");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::grav_gx_grid().\"" << endl;
          }
      }

      return grid_data;
  }

  /**
   * @brief grav_gy_grid
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
  arma::mat grav_gy_grid(const eT &b_min,
                         const eT &b_max,
                         const eT &l_min,
                         const eT &l_max,
                         const eT &height,
                         const eT &bstep,
                         const eT &lstep,
                         const unsigned int &min_n,
                         const unsigned int &max_n,
                         const string& tide_mode,
                         const eT &ln,
                         const geo_f::ellipsoid<eT> &ell,
                         ofstream &out_streamm,
                         bool save2file = true )
  {
      void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                            const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                            vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients;

      arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                   min_n,
                                                                   max_n,
                                                                   0,
                                                                   tide_mode,
                                                                   ln,
                                                                   ell,
                                                                   ggfm_f::n_multiply1<eT>,
                                                                   ggfm_f::minus_lstokes2<eT>,
                                                                   mltiply_stokes_leg );

      for (unsigned int i = 0; i < grid_data.n_rows; i++) {
          eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
          // Loop for parallel computations
          #pragma omp parallel for shared( u, grid_data , gm)
          for (unsigned int j = 0; j < grid_data.n_cols; j++) {
              eT v = l_min + (static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
              vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );

              grid_data(i, j) *=  -1./(ruv[0] * cos(ruv[1] * DEG2RAD));
          }
      }

      if (save2file) {
          try {
              Ggfm<eT>::print_grid_header( out_streamm, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell, "m*s^-2");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_v_grid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief grav_gz_grid
   * @param b_min - minimum value of geodetic latitude in grid
   * @param b_max - maximum value of geodetic latitude in grid
   * @param l_min - minimum value of geodetic longitude in grid
   * @param l_max - maximum value of geodetic longitude in grid
   * @param height - ellipsoidal height of grid points
   * @param bstep - step in latitude
   * @param lstep - step in longitude
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @param out_stream - reference to stream, stream with the header and results
  * @return data in matrix format. Index of matrix (0,0) is for b_min , l_min. Rows are indexes for geodetic latitudem columns are indexes
  * for geodetic longitude.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see grid_computation
  * @see FnALFs
   */
  arma::mat grav_gz_grid(  const eT &b_min,
                           const eT &b_max,
                           const eT &l_min,
                           const eT &l_max,
                           const eT &height,
                           const eT &bstep,
                           const eT &lstep,
                           const unsigned int &min_n,
                           const unsigned int &max_n,
                           const string& tide_mode,
                           const eT &ln,
                           const geo_f::ellipsoid<eT> &ell,
                           ofstream &out_stream,
                           bool save2file = true )
  {
      void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                            const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                            vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients;

      arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                   min_n,
                                                                   max_n,
                                                                   0,
                                                                   tide_mode,
                                                                   ln,
                                                                   ell,
                                                                   ggfm_f::n_multiply1<eT>,
                                                                   ggfm_f::plus_lstokes1<eT>,
                                                                   mltiply_stokes_leg );


      for (unsigned int i = 0; i < grid_data.n_rows; i++) {
          eT u = b_min + ( static_cast<eT>(i) * bstep);  // current geodetic latitude
          // Loop for parallel computations
          #pragma omp parallel for shared( u, grid_data , gm)
          for (unsigned int j = 0; j < grid_data.n_cols; j++) {
              eT v = l_min + (static_cast<eT>(j) * lstep ); // actual longitude , spherical long. == ell. long
              vector<eT> ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );

              // vc - derivation of the centrifugal potential
              eT vc_r = ell.omega * ell.omega * ruv[0] * cos(ruv[1] * DEG2RAD) * cos(ruv[1] * DEG2RAD);
              grid_data(i, j) *=  (-gm / (ruv[0] * ruv[0])) ;
              grid_data(i, j) += vc_r;
          }
      }

      if ( save2file ) {
          try {
              Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell, "m*s^-2");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_v_grid().\"" << endl;
          }
      }
      return grid_data;
  }


  arma::mat grav_vec_mag_grid(  const eT &b_min,
                           const eT &b_max,
                           const eT &l_min,
                           const eT &l_max,
                           const eT &height,
                           const eT &bstep,
                           const eT &lstep,
                           const unsigned int &min_n,
                           const unsigned int &max_n,
                           const string& tide_mode,
                           const eT &ln,
                           const geo_f::ellipsoid<eT> &ell,
                           ofstream &out_stream,
                           bool save2file = true )
  {
      arma::mat gx = Ggfm<eT>::grav_gx_grid( b_min, b_max, l_min, l_max, height, bstep, lstep,
                                             min_n,max_n,tide_mode,ln,ell, out_stream, false);
      arma::mat gy = Ggfm<eT>::grav_gy_grid( b_min, b_max, l_min, l_max, height, bstep, lstep,
                                              min_n,max_n,tide_mode,ln,ell, out_stream, false);
      arma::mat gz = Ggfm<eT>::grav_gz_grid( b_min, b_max, l_min, l_max, height, bstep, lstep,
                                              min_n,max_n,tide_mode,ln,ell, out_stream, false);

      arma::mat grid_data = arma::sqrt(gx % gx + gy % gy + gz % gz);

      if ( save2file ) {
          try {
              Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell, "m*s^-2");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_v_grid().\"" << endl;
          }
      }
      return grid_data;
  }

  /**
   * @brief grav_gx_ongrid
   * @param terrain
   * @param min_n
   * @param max_n
   * @param taylor_order
   * @param tide_mode
   * @param ln
   * @param ell
   * @param crds_system
   * @param out_stream
   * @param save2file
   * @return
   */
  arma::mat grav_gx_ongrid(const Isgemgeoid& terrain,
                           const unsigned& min_n,
                           const unsigned& max_n,
                           const unsigned& taylor_order,
                           const string& tide_mode,
                           const eT& ln,
                           const geo_f::ellipsoid<eT>& ell,
                           const string& crds_system,
                           ofstream &out_stream,
                            bool save2file = true)
  {
    void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
             const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
             const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
           = &Ggfm<eT>::compute_lumped_coeff_taylor_nnf;


    arma::mat grid_data =  this->ongrid_computation_template( terrain , // terrain model
                                      min_n, max_n,
                                      1,            // derivative order of fnALFs
                                      taylor_order, // const unsigned& taylor_order,
                                      0 ,           // const unsigned& start_k,
                                      false ,       // const bool& even,
                                      1 ,           // const unsigned& init_r_power,
                                      tide_mode,    // tide system
                                      ln,           // love number ofr tide_system conversion
                                      ell,          // ellipsoid
                                      crds_system,
                                      ggfm_f::n_multiply0<eT>,
                                      ggfm_f::plus_lstokes1<eT>,
                                      taylor_lumped
                                      );

    unsigned rows, cols;
    vector<eT> header = terrain.get_header<eT>(rows, cols);

    for ( unsigned int i = 0; i < grid_data.n_rows; i++) {
        eT u = header[0] + ( static_cast<eT>(i) * header[4]);  // current geodetic latitude
        // Loop for parallel computations
        #pragma omp parallel for shared( u, grid_data , gm, terrain)
        for ( unsigned int j = 0; j < grid_data.n_cols; j++) {
            eT height = terrain.get_value(i,j);

             eT v = header[2] + (static_cast<eT>(j) * header[5] ); // actual longitude , spherical long. == ell. long

             vector<eT> ruv(3);
             if ( crds_system == "sph" ) {
                 ruv[1] = u;
                 ruv[2] = v;
                 ruv[0] = this->radius + height;
             } else {
                 ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
             }

             eT vc_phi = -1 * ell.omega*ell.omega*ruv[0]*ruv[0]*cos(ruv[1] * DEG2RAD) *
                     sin(ruv[1] * DEG2RAD);
             grid_data(i, j) +=  vc_phi;
             grid_data(i, j) /=  ruv[0];
        }
    }

    if ( save2file ) {
        try {
            Ggfm<eT>::print_grid_header(  out_stream, header[0], header[1], header[2], header[3],
                          0, header[4], header[5],
                          grid_data, min_n, max_n , tide_mode , ln, ell, "m s^-2");
        } catch  ( ... ) {
            cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::grav_anom_sa_ongrid().\"" << endl;
        }
    }
    return grid_data;
  }

  /**
   * @brief grav_gy_ongrid
   * @param terrain
   * @param min_n
   * @param max_n
   * @param taylor_order
   * @param tide_mode
   * @param ln
   * @param ell
   * @param crds_system
   * @param out_stream
   * @param save2file
   * @return
   */
  arma::mat grav_gy_ongrid(const Isgemgeoid& terrain,
                           const unsigned& min_n,
                           const unsigned& max_n,
                           const unsigned& taylor_order,
                           const string& tide_mode,
                           const eT& ln,
                           const geo_f::ellipsoid<eT>& ell,
                           const string& crds_system,
                           ofstream &out_stream,
                            bool save2file = true)
  {
    void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
             const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
             const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
           = &Ggfm<eT>::compute_lumped_coeff_taylor_nnf;


    arma::mat grid_data =  this->ongrid_computation_template( terrain , // terrain model
                                      min_n, max_n,
                                      0,            // derivative order of fnALFs
                                      taylor_order, // const unsigned& taylor_order,
                                      0 ,           // const unsigned& start_k,
                                      false ,        // const bool& even,
                                      1 ,           // const unsigned& init_r_power,
                                      tide_mode,    // tide system
                                      ln,           // love number ofr tide_system conversion
                                      ell,          // ellipsoid
                                      crds_system,
                                      ggfm_f::n_multiply1<eT>,
                                      ggfm_f::minus_lstokes2<eT>,
                                      taylor_lumped
                                      );

    unsigned rows, cols;
    vector<eT> header = terrain.get_header<eT>(rows, cols);

    for ( unsigned int i = 0; i < grid_data.n_rows; i++) {
        eT u = header[0] + ( static_cast<eT>(i) * header[4]);  // current geodetic latitude
        // Loop for parallel computations
        #pragma omp parallel for shared( u, grid_data , gm, terrain)
        for ( unsigned int j = 0; j < grid_data.n_cols; j++) {
            eT height = terrain.get_value(i,j);

             eT v = header[2] + (static_cast<eT>(j) * header[5] ); // actual longitude , spherical long. == ell. long

             vector<eT> ruv(3);
             if ( crds_system == "sph" ) {
                 ruv[1] = u;
                 ruv[2] = v;
                 ruv[0] = this->radius + height;
             } else {
                 ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
             }
             grid_data(i, j) +=  -1./(ruv[0] * cos(ruv[1] * DEG2RAD));
        }
    }

    if ( save2file ) {
        try {
            Ggfm<eT>::print_grid_header(  out_stream, header[0], header[1], header[2], header[3],
                          0, header[4], header[5],
                          grid_data, min_n, max_n , tide_mode , ln, ell, "m s^-2");
        } catch  ( ... ) {
            cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::grav_anom_sa_ongrid().\"" << endl;
        }
    }
    return grid_data;
  }

  /**
   * @brief grav_gz_ongrid
   * @param terrain
   * @param min_n
   * @param max_n
   * @param taylor_order
   * @param tide_mode
   * @param ln
   * @param ell
   * @param crds_system
   * @param out_stream
   * @param save2file
   * @return
   */
  arma::mat grav_gz_ongrid(const Isgemgeoid& terrain,
                           const unsigned& min_n,
                           const unsigned& max_n,
                           const unsigned& taylor_order,
                           const string& tide_mode,
                           const eT& ln,
                           const geo_f::ellipsoid<eT>& ell,
                           const string& crds_system,
                           ofstream &out_stream,
                            bool save2file = true)
  {
    void (Ggfm<eT>::*taylor_lumped)( const eT&, const eT& , const unsigned&, const unsigned&, const string&,
             const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &, const unsigned&,
             const unsigned&, Tmat<eT>&, Tmat<eT>&, eT (&) (const eT&))
           = &Ggfm<eT>::compute_lumped_coeff_taylor_nnf;


    arma::mat grid_data =  this->ongrid_computation_template( terrain , // terrain model
                                      min_n, max_n,
                                      0,            // derivative order of fnALFs
                                      taylor_order, // const unsigned& taylor_order,
                                      1 ,           // const unsigned& start_k,
                                      true ,        // const bool& even,
                                      2 ,           // const unsigned& init_r_power,
                                      tide_mode,    // tide system
                                      ln,           // love number ofr tide_system conversion
                                      ell,          // ellipsoid
                                      crds_system,
                                      ggfm_f::n_multiply1<eT>,
                                      ggfm_f::plus_lstokes1<eT>,
                                      taylor_lumped
                                      );

    unsigned rows, cols;
    vector<eT> header = terrain.get_header<eT>(rows, cols);

    for ( unsigned int i = 0; i < grid_data.n_rows; i++) {
        eT u = header[0] + ( static_cast<eT>(i) * header[4]);  // current geodetic latitude
        // Loop for parallel computations
        #pragma omp parallel for shared( u, grid_data , gm, terrain)
        for ( unsigned int j = 0; j < grid_data.n_cols; j++) {
            eT height = terrain.get_value(i,j);

             eT v = header[2] + (static_cast<eT>(j) * header[5] ); // actual longitude , spherical long. == ell. long

             vector<eT> ruv(3);
             if ( crds_system == "sph" ) {
                 ruv[1] = u;
                 ruv[2] = v;
                 ruv[0] = this->radius + height;
             } else {
                 ruv = geo_f::blh2ruv<eT>( u , v, height, ell  );
             }
             eT vc_r = ell.omega * ell.omega * ruv[0] * cos(ruv[1] * DEG2RAD) * cos(ruv[1] * DEG2RAD);
             grid_data(i, j) *=  (-gm / (ruv[0] * ruv[0])) ;
             grid_data(i, j) += vc_r;
        }
    }

    if ( save2file ) {
        try {
            Ggfm<eT>::print_grid_header(out_stream, header[0], header[1], header[2], header[3],
                          0, header[4], header[5],
                          grid_data, min_n, max_n , tide_mode , ln, ell, "m s^-2");
        } catch  ( ... ) {
            cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::grav_anom_sa_ongrid().\"" << endl;
        }
    }
    return grid_data;
  }

 /**
  * @brief geoid_simplified
  * @param phi - \f$ \varphi \f$ geodetic latitude of an arbitrary point P
  * @param lambda - \f$ \lambda \f$ geodetic longitude of an arbitrary point P
  * @param height - ellipsoidal height of an arbitrary point P
  * @param algorithm - used algorithm for computation of fully normallized  assocciated Legendre polynomials. The possible inputs are:
  * -# "standard" - numerical stability up to degree and order approximately up to 700, is also set as default method
  * -# "stand_mod" - slightly different formulas than "standard", same numerical results
  * -# "tmfcm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfrm" - numerical stability up to degree and order approximately up to 2700, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * -# "tmfcm_2nd" -numerical stability up to degree and order approximately up to 2900, for \f$ -89^\circ < \vaprhi < +89^\circ \f$
  * @param min_n - the minimum degree of an GGFM used in computation
  * @param max_n - the maximum degree of an GGFM used in computation
  * @param tide_mode - the tide system used for the computation. Possible inputs:
  * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
  * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
  * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
  * @param ln - The Love's number used for Permanent Tide Systems transformation
  * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
  * @return the value of geoid undulation for siplified equation for simplified Bruin's formula.
  *
  * @see sum_stokes
  * @see tide_system_transf
  * @see FnALFs
   */
  eT geoid_simplified( const eT& phi,
                       const eT& lambda,
                       const eT& height,
                       const unsigned int& min_n,
                       const unsigned int& max_n,
                       const string& tide_mode,
                       const eT& ln,
                       const geo_f::ellipsoid<eT>& ell  ) {
    eT gravpot_w = this->gpot_w(phi, lambda, height, min_n, max_n, tide_mode, ln, ell);

    cerr << "Ggfm<eT>::geoid_simplified not finished! Sorry...\n";
    return  gravpot_w;
  }

  arma::mat terrain_grid(  const eT &b_min,
                           const eT &b_max,
                           const eT &l_min,
                           const eT &l_max,
                           const eT &height,
                           const eT &bstep,
                           const eT &lstep,
                           const unsigned int &min_n,
                           const unsigned int &max_n,
                           const string& tide_mode,
                           const eT &ln,
                           const geo_f::ellipsoid<eT> &ell,
                           ofstream &out_stream,
                           bool save2file = true )
  {
      void (Ggfm<eT>::*mltiply_stokes_leg) (const eT&, const eT&, const unsigned int& ,  const unsigned int &,
                                            const string&, const eT&, const geo_f::ellipsoid<eT>&, const Lvec<eT> &,
                                            vector<eT> &, vector<eT> &, eT (&)(const eT&)) = &Ggfm<eT>::compute_lumped_coefficients_terrain;

      arma::mat grid_data = Ggfm<eT>::grid_computation_template(   b_min, b_max, l_min, l_max, height, bstep, lstep,
                                                                   min_n,
                                                                   max_n,
                                                                   0,
                                                                   tide_mode,
                                                                   ln,
                                                                   ell,
                                                                   ggfm_f::n_multiply0<eT>,
                                                                   ggfm_f::plus_lstokes1<eT>,
                                                                   mltiply_stokes_leg );
      if ( save2file ) {
          try {
              Ggfm<eT>::print_grid_header( out_stream, b_min, b_max, l_min, l_max, height, bstep, lstep,
                                           grid_data, min_n, max_n , tide_mode , ln, ell, "m");
          } catch  ( ... ) {
              cerr << "Unable to pass the the out put to stream. Function \"arma::mat Ggfm::gpot_v_grid().\"" << endl;
          }
      }
      return grid_data;
  }
  //------------------------------------------------------------------------------------------------------

  /**
   * @brief degree_var - compute degree variances for given global gravity field
   * model for \f$ n \in [ n_{min}, n_{max}] \f$
   * @param radius - reference radius which for the degree varainces are calculated
   * @param nmin - minimum degree
   * @param nmax - max degree
   * @param quantity - gravity quantity for which the degree variance is computed
   * @return vector with data
   */
  arma::mat degree_var(const eT& r ,
                       const unsigned int& nmin,
                       const unsigned int& nmax,
                       const string& quantity,
                       const geo_f::ellipsoid<eT> &ell)
  {
      unsigned int nn_max = ( nmax > n_max ) ? n_max : nmax; 

      bool with_normal_field = true;

      eT dg_fac;
      unsigned int nelem =static_cast<unsigned int>(nmax - nmin + 1);

      arma::mat dvar_g = arma::zeros<arma::mat>(nelem , 2);
      arma::vec c_n = arma::zeros<arma::vec>(nelem);

      // Create sums defined as \f$ C_n = \sum_m ( C_{nm}^2 + S_{nm}^2 ) \f$
      // the sums are created based on the fact, if the normal field is removed or not
      unsigned n = 0;
      if ( with_normal_field ) {
          for (unsigned int i = nmin; i <= nn_max; i++) {
              // eT n_double = static_cast<eT>(i);
              eT sum_c_nm = 0.0 ; //  \f$ \sum_{m = n_{min}}^{n_{max}} C_{nm} \f$  - latex eq
              eT sum_s_nm = 0.0 ; // \f$ \sum_{m = n_{min}}^{n_{max}} S_{nm} \f$  - latex eq
              for (unsigned int j = 0; j <= i; j++) {
                  //double jj = static_cast<double>(j);

                  sum_c_nm += this->c_nm(i,j)*this->c_nm(i,j);
                  sum_s_nm += this->s_nm(i,j)*this->s_nm(i,j);
              }

              c_n(n) = static_cast<double>(sum_c_nm + sum_s_nm);
              n++;
          }
      } else /* without normal field - T_pot, deflection of the vertical, etc. */ {
          // Tide mode conversion //
          eT alpha1 = static_cast<eT>(11.18033988749894848204586834365638117720309179805762862135L);

          for (unsigned int i = nmin; i <= nn_max; i++) {
              eT n_double = static_cast<eT>(i);
              eT sum_c_nm = 0.0 ; //  \f$ \sum_{m = n_{min}}^{n_{max}} C_{nm} \f$  - latex eq
              eT sum_s_nm = 0.0 ; //  \f$ \sum_{m = n_{min}}^{n_{max}} S_{nm} \f$  - latex eq
              for (unsigned int j = 0; j <= i; j++) {
                  // Normal field subtraction
                  eT nn, sign_brac, frac, brac, gms, dc_nm = 0.0 ;
                  if (i == 0 && j == 0) {
                      dc_nm = static_cast<eT>(-1.0) * (ell.gm / this->gm);
                  } else if ( i <= 20 &&  j == 0 &&  i%2 == 0 && i > 0) {
                      /* ell.j20 has to be defined */

                      nn = n_double / 2;
                      sign_brac = (i % 4 == 0) ? 1 : -1;

                      frac = (3.0 * pow(ell.eccentricity, nn)) /
                              ((2.0 * nn + 1.0) * (2.0 * nn + 3.0) * sqrt(4.0 * nn + 1.0));
                      brac = 1.0 - nn -
                              alpha1 * nn * (ell.j20) /
                              (ell.eccentricity);
                      gms = (ell.gm / gm) * pow((ell.a_axis) / radius, nn);

                      dc_nm = -1. * sign_brac * frac * brac * gms;

                      dc_nm *= (ell.gm / this->gm);
                      dc_nm *= pow(ell.a_axis / this->radius, nn);
                  } else {
                      dc_nm = .0;
                  }

                  // C_nm, S_nm squares
                  sum_c_nm += (dc_nm + this->c_nm(i,j))*(dc_nm+this->c_nm(i,j));
                  sum_s_nm += this->s_nm(i,j)*this->s_nm(i,j);
              }

              c_n(n) = static_cast<double>(sum_c_nm + sum_s_nm);
              n++;
          }
      }

      eT (*ptr2fun)(const eT&);
      double gm_div_r, rm_div_r ;
      rm_div_r = this->radius / r;

      if ( quantity == "gpot_v" ) {
        ptr2fun = &ggfm_f::n_multiply0;
        gm_div_r = this->gm / (this->radius);
      } else if ( quantity == "gpot_w" ) {
        ptr2fun = &ggfm_f::n_multiply0;
        gm_div_r = this->gm / (this->radius);
      } else if ( quantity == "gpot_t" ) {
        ptr2fun = &ggfm_f::n_multiply0;
        gm_div_r = this->gm / (this->radius);
      } else if ( quantity == "grav_gy" ) {
        ptr2fun = &ggfm_f::n_multiply1;
        gm_div_r = this->gm / (this->radius );
      } else if ( quantity == "grav_gx" ) {
        ptr2fun = &ggfm_f::n_multiply0;
        gm_div_r = this->gm / (this->radius );
      } else if ( quantity == "grav_gz" ) {
        ptr2fun = &ggfm_f::n_multiply1;
        gm_div_r = this->gm / (this->radius * this->radius);
      } else if ( quantity == "def_vert_eta" ) {
        gm_div_r = this->gm / (this->radius * this->radius);
        ptr2fun = &ggfm_f::n_multiply0;
      } else if ( quantity == "def_vert_xi" ) {
        gm_div_r = this->gm / (this->radius * this->radius);
        ptr2fun = &ggfm_f::n_multiply0;
      } else if ( quantity == "def_vert" ) {
        ptr2fun = &ggfm_f::n_multiply0;
        gm_div_r = this->gm / (this->radius * this->radius);
      } else if ( quantity == "grav_anom_sa" ) {
        gm_div_r = this->gm / (this->radius * this->radius);
        ptr2fun = &ggfm_f::n_multiply3;
      } else if ( quantity == "grav_dist_sa" ) {
        ptr2fun = &ggfm_f::n_multiply1;
        gm_div_r = this->gm / (this->radius * this->radius);
      } else if ( quantity == "height_anomaly" ) { //-
        ptr2fun = &ggfm_f::n_multiply0;
        gm_div_r = this->gm / (this->radius);
      } else {
         string errmsg = "Ggfm<eT>::degree_var - unknown quantity parameter :\"" + quantity + "\". ";
         throw runtime_error( errmsg );
      }

      n = 0;
      for (unsigned int i = nmin; i <= nmax; i++) {
        double ni =  static_cast<double>(i);
        dvar_g(n,0) = ni;
        dvar_g(n,1) = c_n(n) * pow( ptr2fun(ni) * pow(rm_div_r , ni ) * gm_div_r  , 2.);
        n++;
      }


      return dvar_g;
  }


  /** The summation of the Stokes' coefficients without the normal gravity
  * field. The normal gravity
  * field coefficients are based on the well known formula.
  *
  * @param r - geocentric length in units of [m]  (radius)
  * @param u - spherical latitude in radians
  * @param v - spherical longitude in radians
  * @param algorithm
  * @param min_n
  * @param max_n
  * @param tide_mode
  * @param ln
  * @param ell
  * @param multiply
  * @param sum_coeff
  */
  eT  sum_stokes(const eT& r,
                 const eT& u,
                 const eT& v,
                 const eT& phi,  // spherical coordinates -->> final version
                 const unsigned int& min_n,
                 const unsigned int& max_n,
                 const string& tide_mode,
                 const eT& ln,
                 const geo_f::ellipsoid<eT>& ell,
                 const Lvec<eT>& p_nm,
                 eT (&mltply_fce)(const eT &),
                 eT (&sum_coeff)(const eT &, const eT &, const eT &, const eT &, const eT &)) {
      // ------------------------------------------------------------------------------------
      unsigned mmax_n;
      if ( max_n > n_max ) {
        mmax_n = n_max;
      } else {
        mmax_n = max_n;
      }

      if ( algorithm == "tmfcm" || algorithm == "tmfcm_2nd") {

          // Reference/pointers to "relaxing" function
          eT (*um_func)(const int&, const eT&, const eT&);

          if ( algorithm == "tmfcm") {
              um_func = &ggfm_f::um_function1<eT>;
          } else {
              um_func = &ggfm_f::um_function2<eT>;
          }

          // Summation over stokes coefficients
          eT sum_m = 0.;
          for ( unsigned int j = mmax_n ;; j--) {
              unsigned i_min = (j < min_n) ? min_n : j;
              unsigned idx_cs_nm  = c_nm.return_index( i_min,j );
              unsigned idx_p_nm = p_nm.return_index( i_min, j);

              eT m = static_cast<eT>(j);
              eT sum_n = 0.;
              eT dc_nm;
              eT a_r = pow(radius / r, static_cast<eT>(i_min) );

              for (unsigned int i = i_min; i <= mmax_n; i++) {
                  eT n = static_cast<eT>(i);

                  if (i == 2 && j == 0) {
                      // transformation from tide_system of the ggfm to required tide system
                      // tide_system (from) >> tide_mode(to)
                      dc_nm = Ggfm<eT>::tide_system_transf( phi, ln, this->tide_system, tide_mode, ell);

                  } else {
                      dc_nm = 0;
                  }

                  sum_n += mltply_fce(n) * a_r *
                          sum_coeff(c_nm[idx_cs_nm] + dc_nm, s_nm[idx_cs_nm], p_nm[idx_p_nm], m, v * DEG2RAD);
                  a_r *= radius / r;  // faster approach than pow()

                  idx_cs_nm++;
                  idx_p_nm++;
              }
              sum_m += sum_n;
              sum_m *= um_func(j, m, u * DEG2RAD);

              if (j == 0)
                  break;
          }
          return sum_m / ggfm_f::relax_factor<eT>();
      } else {
          // Default is algorithm set to standard/stan_mod
          eT sum_m = 0;
          for ( unsigned int j = mmax_n ;; j--) {
              unsigned i_min = (j < min_n) ? min_n : j;
              unsigned idx_cs_nm  = c_nm.return_index( i_min,j );
              unsigned idx_p_nm = p_nm.return_index( i_min, j);


              eT m = static_cast<eT>(j);
              eT sum_n = 0, dc_nm;  //= 0.;
              eT a_r = pow(radius / r, static_cast<eT>(i_min) );
              for (unsigned int i = i_min; i <= mmax_n; i++) {
                  eT n = static_cast<eT>(i);

                  if (i == 2 && j == 0) {
                      // transformation from tide_system of the ggfm to required tide system
                      // tide_system (from) >> tide_mode(to)
                      dc_nm = Ggfm::tide_system_transf(phi, ln, this->tide_system,
                                                       tide_mode, ell);

                  } else {
                      dc_nm = 0;
                  }

                  sum_n += mltply_fce(n) * a_r *
                          sum_coeff(c_nm[idx_cs_nm] + dc_nm, s_nm[idx_cs_nm], p_nm[idx_p_nm], m, v * DEG2RAD);
                  a_r *= radius / r;  // faster approach than pow()

                  idx_cs_nm++;
                  idx_p_nm++;
              }
              sum_m += sum_n ;
              if (j == 0)
                  break;
          }
          return sum_m / ggfm_f::relax_factor<eT>();
      }
  }

  /** The summation of the Stokes' coefficients without the normal gravity field
  * (nnf = no normal field). The normal gravity field coefficients are based on
  * the well known formula.
  *
  * @param r - geocentric length in units of [m]  (radius)
  * @param u - spherical latitude in radians
  * @param v - spherical longitude in radians
  * @param algorithm
  * @param min_n
  * @param max_n
  * @param tide_mode
  * @param ln
  * @param ell
  * @param multiply
  * @param sum_coeff
  */
  eT sum_stokes_nnf(
          const eT& r,
          const eT& u,
          const eT& v,
          const eT& phi,
          const unsigned int& min_n,
          const unsigned int& max_n,
          const string& tide_mode,
          const eT& ln,
          const geo_f::ellipsoid<eT>& ell,
          const Lvec<eT>& p_nm,  // it would be better if i have used the class
          eT (&mltply_fce)(const eT&),
          eT (&sum_coeff)(const eT&, const eT&, const eT&, const eT&, const eT&)) {
      //-----------------------------------------------------------------------------------------
      unsigned mmax_n;
      if ( max_n > n_max ) {
        mmax_n = n_max;
      } else {
        mmax_n = max_n;
      }

      //* constants used for computation *//
      eT alpha1 = static_cast<eT>( 11.18033988749894848204586834365638117720309179805762862135L);

      if (algorithm == "tmfrm" || algorithm == "tmfcm" ||
              algorithm == "tmfcm_2nd") {
          // Reference/pointers to "relaxing" function
          eT (*um_func)(const int&, const eT&, const eT&);

          if (algorithm == "tmfrm" || algorithm == "tmfcm") {
              um_func = &ggfm_f::um_function1;
          } else {
              um_func = &ggfm_f::um_function2;
          }

          // Summation over stokes coefficients
          eT sum_m = 0.;
          for ( unsigned int j = mmax_n ;; j--) {
              unsigned i_min = (j < min_n) ? min_n : j;
              unsigned idx_cs_nm  = c_nm.return_index( i_min,j );
              unsigned idx_p_nm = p_nm.return_index( i_min, j);


              eT m = static_cast<eT>(j);
              eT sum_n = 0.;
              eT a_r = pow(radius / r, static_cast<eT>(i_min) );
              for (unsigned int i = i_min; i <= mmax_n; i++) {
                  eT n = static_cast<eT>(i);

                  // Normal field subtraction
                  eT nn, sign_brac, frac, brac, gms, dc_nm = 0.0 ;
                  if (i == 0 && j == 0) {
                      dc_nm = static_cast<eT>(-1.0) * (ell.gm / gm);
                  } else if ( i <= 20 &&  j == 0 &&  i%2 == 0 && i > 0) {
                      /* ell.j20 has to be defined */

                      nn = n / 2.0;
                      sign_brac = (i % 4 == 0) ? 1.0 : -1.0;
                      frac = (3.0 * pow(ell.eccentricity, nn)) /
                              ((2.0 * nn + 1.0) * (2.0 * nn + 3.0) * sqrt(4.0 * nn + 1.0));
                      brac = 1.0 - nn -
                              alpha1 * nn * (ell.j20) /
                              (ell.eccentricity);
                      gms = (ell.gm / gm) * pow((ell.a_axis) / radius, nn);

                      dc_nm = -1. * sign_brac * frac * brac * gms;

                      dc_nm *= (ell.gm / gm);
                      dc_nm *= pow(ell.a_axis / radius, nn);

                      if (i == 2 && j == 0) {
                          // transformation from tide_system of the ggfm to required tide
                          // system // tide_system (from) >> tide_mode(to)
                          dc_nm += tide_system_transf( phi, ln, this->tide_system,
                                  tide_mode, ell);
                      }
                  } else {
                      dc_nm = .0;
                  }

                  sum_n += a_r * mltply_fce(n) * sum_coeff(c_nm[idx_cs_nm] + dc_nm, s_nm[idx_cs_nm],
                                                           p_nm[idx_p_nm], m, v * DEG2RAD);

                  a_r *= radius / r;
                  idx_cs_nm++;
                  idx_p_nm++;
              }
              sum_m += sum_n;
              sum_m *= um_func(j, m, u * DEG2RAD);

              if (j==0){
                  break;
              }
          }
          return sum_m / ggfm_f::relax_factor<eT>();
      } else {
          // Default is algorithm set to standard/stan_mod
          eT sum_m = 0.;
          for ( unsigned int j = mmax_n ;; j--) {
              unsigned i_min = (j < min_n) ? min_n : j;
              unsigned idx_cs_nm  = c_nm.return_index( i_min,j );
              unsigned idx_p_nm = p_nm.return_index( i_min, j);

              eT m = static_cast<eT>(j);
              eT sum_n = 0.;
              eT a_r = pow(radius / r, static_cast<eT>(i_min) );
              for (unsigned int i = i_min; i <= mmax_n; i++) {
                  eT n = static_cast<eT>(i);
                  eT nn, sign_brac, frac, brac, gms, dc_nm = 0 /*must reconsider*/;
                  if (i == 0 && j == 0) {
                      dc_nm = static_cast<eT>(-1.0) * (ell.gm / gm);
                  } else if (i % 2 == 0 && i <= 20 && i > 0 && j == 0) {
                      /* ell.j20 has to be defined */

                      nn = n / 2.0;
                      sign_brac = (i % 4 == 0) ? 1.0 : -1.0;
                      frac = (3.0 * pow(ell.eccentricity, nn)) /
                              ((2.0 * nn + 1.0) * (2.0 * nn + 3.0) * sqrt(4.0 * nn + 1.0));
                      brac = 1.0 - nn -
                              alpha1 * nn * ( ell.j20) /
                              (ell.eccentricity);
                      gms = (ell.gm / gm) * pow((ell.a_axis) / radius, nn);

                      dc_nm = -1. * sign_brac * frac * brac * gms;

                      dc_nm *= (ell.gm / gm);
                      dc_nm *= pow( ell.a_axis / radius, nn);

                      if (i == 2 && j == 0) {
                          // transformation from tide_system of the ggfm to required tide
                          // system
                          // tide_system (from) >> tide_mode(to)
                          dc_nm += Ggfm::tide_system_transf( phi, ln, this->tide_system,
                                  tide_mode, ell);
                      }
                  } else {
                      dc_nm = .0;
                  }
                  sum_n += a_r *  mltply_fce(n) * sum_coeff(c_nm[idx_cs_nm] + dc_nm, s_nm[idx_cs_nm], p_nm[idx_p_nm], m,
                                     v * DEG2RAD);
                  a_r *= radius / r;  // faster approach than pow()
                  idx_cs_nm++;
                  idx_p_nm++;
              }
              sum_m += sum_n;
              if (j == 0)
                  break;
          }
          return sum_m / ggfm_f::relax_factor<eT>();
      }
  }

  /** Prints the Global gravity field model.
  @param showcoeff = {'true', 'false'}, no is default
  @param upto = show coefficients up to degree and order, default set to n_max

  @see Ggfm        */
  void print_ggfm(const bool &showcoeff = false, const unsigned int& upto = 0) {
      // PRINT HEADER INFO
      cout << "\nPrinting description of Global gravity field model." << endl;
      cout << "--------------------------------------------" << endl;
      cout << "                  HEADER                    " << endl;
      cout << "--------------------------------------------" << endl;
      cout << setw(15) << "Model name     :" << setw(20) << model_name << endl;
      cout << setw(15) << "Product type   :" << setw(20) << product_type << endl;
      cout << setw(15) << "Error type     :" << setw(20) << errors << endl;
      cout << setw(15) << "Normalized     :" << setw(20) << normalized << endl;
      cout << setw(15) << "Tide system    :" << setw(20) << tide_system << "\n"
           << endl;
      cout << setw(15) << "Model's radius:=" << setw(20) << setprecision(12)
           << radius << endl;
      cout << setw(15) << "Model's GM    :=" << setw(20) << setprecision(12) << gm
           << endl;
      cout << setw(15) << "Max degree    :=" << setw(20) << n_max << endl;
      cout << setw(15) << "Min degree    :=" << setw(20) << n_min << endl;

      if ( showcoeff ) {
          cout << "--------------------------------------------" << endl;
          cout << "            SHOWING  COEFFICIENTS           " << endl;
          cout << "--------------------------------------------" << endl;

          for (unsigned int i = 0; i <= upto; i++) {
              for (unsigned int j = 0; j <= i; j++) {
                  cout << setw(6) << i << setw(6) << j << "\t";
                  cout << setw(18) << setprecision(12) << c_nm(i,j) << "\t";
                  cout << setw(18) << setprecision(12) << s_nm(i,j) << endl;
              }

              if (upto > n_max)
                  break;
          }
      }
  }

  /**
  * @param c_20
  * @param phi - latitude
  * @param k - Love number (zero frequency)
  * @param from - from tide system
  * @param to - to tide system
  * @param ell
  *
  * @see
  */
  // template <typename Any, typename Typ>
  eT tide_system_transf(   const eT& phi,
                           const eT& k,
                           const string& from,
                           const string& to,
                           const struct geo_f::ellipsoid<eT>& ell) {
      /* permanent tide system transformation in more general formula
      gm - gravity-mass constant for the reference ellipsoid
      a  - semi-major axis of the reference ellipsoid
      e  - first numerical eccentricity of the reference ellipsoid
      f  - flattening parameter of the reference ellipsoid
      g  - mean gravity
      r  - local ellipsoidal radius
      phi- geodetic latitude
      k  - love numbers for Earth's plasticity
      */

      eT m, r, g, kk, ee, x, delta_C20;
      if (from.compare(to) == 0) {
          delta_C20 = 0.0;
          return delta_C20;
      }

      x = phi * DEG2RAD;
      ee = static_cast<eT>( ell.eccentricity );
      //n_rad = ell.a_axis / (1.0 - ee * sin(x) * sin(x));
      r = ell.a_axis*sqrt(1. - (ee*(1.-ee)*sin(x)*sin(x))  /
                               (1. - ee*sin(x)*sin(x)));

      //kk = ((sqrt(1.0 - ee) * ell.g_p - ell.g_e) / ell.g_e);
      kk = (ell.b_axis * ell.g_p ) / ( ell.a_axis * ell.g_e ) - 1.;

      g = ell.g_e * (1.0 + kk * sin(x) * sin(x)) /
              sqrt(1.0 - ee * sin(x) * sin(x));

      m = -.198 * r * r * r * g /
              (ell.a_axis * ell.a_axis * ell.gm * sqrt(5.0));
      if ((from.compare("zero tide") == 0 || from.compare("zero_tide") == 0) &&
              (to.compare("mean tide") == 0 || to.compare("mean_tide") == 0)) {
          // conversion zero tide >> mean tide
          return m;
      } else if ((from.compare("zero tide") == 0 ||
                  from.compare("zero_tide") == 0) &&
                 (to.compare("tide free") == 0 || to.compare("tide_free") == 0)) {
          delta_C20 = -k * m;
      } else if ((from.compare("mean tide") == 0 ||
                  from.compare("mean_tide") == 0) &&
                 (to.compare("zero tide") == 0 || to.compare("zero_tide") == 0)) {
          delta_C20 = -m;
      } else if ((from.compare("mean tide") == 0 ||
                  from.compare("mean_tide") == 0) &&
                 (to.compare("tide free") == 0 || to.compare("tide_free") == 0)) {
          delta_C20 = -(1.0 + k) * m;
      } else if ((from.compare("tide free") == 0 ||
                  from.compare("tide_free") == 0) &&
                 (to.compare("zero tide") == 0 || to.compare("zero_tide") == 0)) {
          delta_C20 = -k*m;
      } else if ((from.compare("tide free") == 0 ||
                  from.compare("tide_free") == 0) &&
                 (to.compare("mean tide") == 0 || to.compare("mean_tide") == 0)) {
          delta_C20 = (1.0 + k) * m;
      } else {
          delta_C20 = 0.;
      }

      return delta_C20;
  }

  /**
   * @brief get_degree_and_order
   * @param min
   * @param max
   */
  void get_degree_and_order ( unsigned int& min , unsigned int& max )
  {
      min = this->n_min;
      max = this->n_max;
  }

  /**
   * @brief model_tide_system
   * @return model's tide system
   */
  const string& model_tide_system ()
  {
      return tide_system;
  }


  friend ostream& operator<<(ostream& stream, Ggfm model) {
      stream << "begin_of_head ================================================\n";
      stream << "Model name     :" << model.model_name << endl;
      stream << "Product type   :" << model.product_type << endl;
      stream << "Error type     :" << model.errors << endl;
      stream << "Normalized     :" << model.normalized << endl;
      stream << "Tide system    :" << model.tide_system << endl;
      stream << "Model's radius:=" << model.radius << endl;
      stream << "Model's GM    :=" << model.gm << endl;
      stream << "Max degree    :=" << model.n_max << endl;
      stream << "Min degree    :=" << model.n_min << endl;
      stream << "end_of_head ==================================================\n";

        if ( model.show_coeff ) {
            for (unsigned int i = 0; i <= model.n_max; i++) {
                for (unsigned int j = 0; j <= i; j++) {
                    stream << i << " " << j << "\t";
                    stream << model.c_nm(i,j) << "\t";
                    stream << model.s_nm(i,j) << endl;
                }
            }
        }
    return stream;
  }

  void change_algorithm( const string& new_alg ) {
      algorithm.clear();
      algorithm.assign( new_alg );
  }

  const string& show_algorithm() const {
      return algorithm;
  }

  void set_show_coeff( const bool& showcc ) {
      this->show_coeff = showcc;
  }

  /////////////////////////
  /// Protected members ///
  /// ///////////////// ///
  /// ///////////////// ///
 protected:

  /**
   * @brief sum_stokes_with_legendre
   * @param u - spherical latitude in radians
   * @param v - spherical/geodetical longitude in radians
   * @param min_n
   * @param max_n
   * @param pc_nm
   * @param ps_nm
   * @return
   */
  eT sum_stokes_with_legendre (const eT& u ,// radians
                               const eT& v, // radians
                               const unsigned int& min_n,
                               const unsigned int& max_n,
                               const vector<eT>& pc_nm,
                               const vector<eT>& ps_nm,
                               eT (&sum_lstokes)( const eT&,
                                                  const eT&,
                                                  const eT&,
                                                  const eT& ) ) {
      unsigned mmax_n;
      if ( max_n > n_max ) {
        mmax_n = n_max;
      } else {
        mmax_n = max_n;
      }

      eT sum_m = 0.;
      if ( algorithm == "tmfcm" || algorithm == "tmfcm_2nd") {
          // Algorithm of summation for modified columns algorithms with relaxing factors
          // Reference/pointers to "relaxing" function
          eT (*um_func)(const int&, const eT&, const eT&);

          if (algorithm == "tmfcm" ) {
              um_func = &ggfm_f::um_function1<eT>;
          } else {
              um_func = &ggfm_f::um_function2<eT>;
          }
          for ( unsigned int j = mmax_n ;; j--) {
              eT m = static_cast<eT>(j);

              sum_m += sum_lstokes( pc_nm[j], ps_nm[j] , m, v);
              sum_m *= um_func(j, m, u );

              if (j == 0)
                  break;
          }
      } else {
          for ( unsigned int j = mmax_n ;; j--) {
              eT m = static_cast<eT>(j);

              sum_m +=  sum_lstokes( pc_nm[j], ps_nm[j] , m, v);

              if (j == 0)
                  break;
          }
      }
      return  sum_m/ggfm_f::relax_factor<eT>();
  }

  /**
   * @brief multiply_stokes_with_legendre - based on article published in Journal of geodesy :
   * Numerical computation of spherical harmonics of arbitraty degree and order by extedning exponent
   * of floating point numbers, written by Toshio Fukushima is grid computation formulated following.
   * The lumped coefficients are defined as :
   * -# \f$ c_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar C_{nm} \bar P_{nm} ( \sin \phi ) \f$
   * -# \f$ s_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar S_{nm} \bar P_{nm} ( \sin \phi ) \f$
   *
   * where R is reference radius of the used GGFM model, r is geocentric length, \f$ \bar C_{nm} , \bar S_{nm} \f$
   * are Stoke's coefficients and \f$ \bar P_{nm} (\sin \varphi) \f$ are fully normalized assocciated Legendre polynomials
   * for geocentric latitude \f$ \phi \f$. In this case the multiplication also includes factor based on function \f$ g(n) \f$.
   * For example for \f$ \frac{\partial V}{\partial r}\f$ the function is  \f$ g(n) = n + 1 \g$. @see
   * @param r - geocentric radius in meetres
   * @param phi - geodetic latitude
   * @param min_n - the minimum degree of an GGFM used in computation
   * @param max_n - the maximum degree of an GGFM used in computation
   * @param tide_mode - the tide system used for the computation. Possible inputs:
   * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
   * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
   * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
   * @param ln - The Love's number used for Permanent Tide Systems transformation
   * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
   * @param p_nm - fully normalized accossiated Legendre polynomials or it's derivative
   * @param pc_nm - reference to output \f$ PC_{nm} = \alpha_n \cdot C_{nm} \cdot P_{nm} \f$, where \f$ \alpha_n \f$ is arbitraty coeff or function
   * @param ps_nm - reference to output \f$ PC_{nm} = \alpha_n \cdot S_{nm} \cdot P_{nm} \f$, where \f$ \alpha_n \f$ is arbitraty coeff or function
   */
  void multiply_stokes_with_legendre(const eT& r,
                                     const eT& phi,
                                     const unsigned int& min_n,
                                     const unsigned int& max_n,
                                     const string& tide_mode,
                                     const eT& ln,
                                     const geo_f::ellipsoid<eT>& ell,
                                     const Lvec<eT> &p_nm,
                                     Lvec<eT> &pc_nm,
                                     Lvec<eT> &ps_nm,
                                     eT (&mltply_fce)(const eT&))
  {
      unsigned mmax_n;
      if ( max_n > n_max ) {
        mmax_n = n_max;
      } else {
        mmax_n = max_n;
      }

      pc_nm.resize( p_nm.nmaxdeg(), this->col_order );
      ps_nm.resize( p_nm.nmaxdeg(), this->col_order );

      // Tide mode conversion //
      eT dc_nm = Ggfm<eT>::tide_system_transf(phi, ln, this->tide_system, tide_mode, ell);

      for ( unsigned int j = mmax_n ;; j--) {
          unsigned i_min = (j < min_n) ? min_n : j;
          unsigned idx_cs_nm  = c_nm.return_index( i_min,j );
          unsigned idx_p_nm = p_nm.return_index( i_min,j);

          eT a_r = pow(radius / r, static_cast<eT>(i_min) );
          for (unsigned int i = i_min; i <= mmax_n; i++) {
              eT n = static_cast<eT>(i);

              if ( j == 0 && i == 2 ) {
                  pc_nm[idx_p_nm] = (c_nm[idx_cs_nm] + dc_nm ) * p_nm[idx_p_nm] * a_r * mltply_fce( n );
              } else {
                  pc_nm[idx_p_nm] = c_nm[idx_cs_nm] * p_nm[idx_p_nm] * a_r * mltply_fce( n );
              }

              ps_nm[idx_p_nm] = s_nm[idx_cs_nm] * p_nm[idx_p_nm] * a_r * mltply_fce( n );

              a_r *= radius / r;  // faster approach than pow()

              idx_cs_nm++;
              idx_p_nm++;
          }

          if (j == 0)
              break;
      }
  }

  /**
   * @brief compute_lumped_coeff_taylor - computes the lumped coefficients if the gradient approach is used.
   * More can be found in literature.
   * @param r - reference geocentric length for the \f$  r_0  \f$ , by default the ellipsoidal height is set to
   * zero ( \f$ h_ell = 0 \f$ ).
   * @param phi - latitude for tide system conversion
   * @param min_n - the minimum degree of an GGFM used in computation
   * @param max_n - the maximum degree of an GGFM used in computation
   * @param tide_mode - the tide system used for the computation. Possible inputs:
   * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
   * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
   * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
   * @param ln - The Love's number used for Permanent Tide Systems transformation
   * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
   * @param p_nm - fully normalized accossiated Legendre polynomials or it's derivative
   * @param taylor_order - degree of the used taylor polynomial
   * @param start_k - init k for prod product \f$ \prod \limits_{i = k}^s (n + k )
   * @param pc_taylor - @see Tmat class with \f$ PC_{nm} = \alpha_n \cdot C_{nm} \cdot P_{nm} \f$,
   * where \f$ \alpha_n \f$ is arbitraty coeff or function and all is computed with respect to taylor
   * polynomial degree.
   * @param ps_taylor - @see Tmat class with \f$ PS_{nm} = \alpha_n \cdot S_{nm} \cdot P_{nm} \f$,
   * where \f$ \alpha_n \f$ is arbitraty coeff or function and all is computed with respect to taylor
   * polynomial degree.
   */
  void compute_lumped_coeff_taylor( const eT& r,
                                    const eT& phi,
                                    const unsigned& min_n,
                                    const unsigned& max_n,
                                    const string& tide_mode,
                                    const eT& ln,
                                    const geo_f::ellipsoid<eT>& ell,
                                    const Lvec<eT> &p_nm,
                                    const unsigned& taylor_order,
                                    const unsigned& start_k,
                                    Tmat<eT>& pc_taylor,
                                    Tmat<eT>& ps_taylor,
                                    eT (&mltply_fce) (const eT&)
                                    ) {
      unsigned mmax_n;
      if ( max_n > n_max ) {
      mmax_n = n_max;
      } else {
      mmax_n = max_n;
      }

      pc_taylor.resize( max_n+1, taylor_order+1 ); // [x0 , dx/dr , d^2x/dr^2 ,....]
      ps_taylor.resize( max_n+1, taylor_order+1 );

      // Tide mode conversion //
      eT dc_nm = Ggfm<eT>::tide_system_transf( phi, ln, this->tide_system, tide_mode, ell);

      eT k = static_cast<eT>(start_k); // for prod product \f$ \prod \limits_{i = k}^{Taylor order} ( n+k+i ) \f$
      for ( unsigned int j = mmax_n ;; j--) {
          unsigned i_min = (j < min_n) ? min_n : j;
          unsigned idx_cs_nm  = c_nm.return_index( i_min,j );
          unsigned idx_p_nm = p_nm.return_index( i_min,j);

          eT a_r = pow(radius / r, static_cast<eT>(i_min) );
          for (unsigned int i = i_min; i <= mmax_n; i++) {
              eT n = static_cast<eT>(i);
              eT pc_nm , ps_nm ; // multiplication of P_nm with S_nm, C_nm, (R/r)^n etc...

              if ( j == 0 && i == 2 ) {
                  pc_nm = (c_nm[idx_cs_nm] + dc_nm ) * p_nm[idx_p_nm] * a_r * mltply_fce( n ); // TideSystem conversion
              } else {
                  pc_nm = c_nm[idx_cs_nm] * p_nm[idx_p_nm] * a_r * mltply_fce( n );
              }
              ps_nm = s_nm[idx_cs_nm] * p_nm[idx_p_nm] * a_r * mltply_fce( n );


              eT prod_n = static_cast<eT>(1.);
              // k should be +1 compared to the eq, since for example
              // (n+1) is already part of "start_k"
              /*
               * This part of the code is a little tricky
               * Take for example the gravity disturbance
               * the initial coeff is (n+1)is part of  mltply_fce( n )
               * for derivatives it goes:
               * first derivative: (n+1) * [n+2]
               * 2nd :  (n+1) * [n+2] * [n+3]
               * 3rd : (n+1) * [n+2] * [n+3] * [n+4]
               * etc.
               * That means next loop work like this:
               * l | prod_n
               * 0 | 1
               * 1 | n+2
               * 2 | (n+2)*(n+3)
               * 3 | (n+2)(n+3)(n+4)
               *
               * remember (n+1) and i tis part of  mltply_fce( n )
               **/
              for ( unsigned l = 0 ; l <= taylor_order; l++ ) {
                  if ( l != 0 ) {
                      prod_n *= ( n + k + static_cast<eT>(l));
                  }

                  pc_taylor(j,l) += pc_nm * prod_n;
                  ps_taylor(j,l) += ps_nm * prod_n;
              }

              a_r *= radius / r;  // faster approach than pow()
              idx_cs_nm++;
              idx_p_nm++;
          }

          if (j == 0)
              break;
      }
  }

  /**
   * @brief compute_lumped_coeff_taylor_nnf - computes the lumped coefficients if the gradient approach is used.
   * More can be found in literature. The normal field of the reference ellipsoid is removed.
   * @param r - reference geocentric length for the \f$  r_0  \f$ , by default the ellipsoidal height is set to
   * zero ( \f$ h_ell = 0 \f$ ).
   * @param phi - latitude for tide system conversion
   * @param min_n - the minimum degree of an GGFM used in computation
   * @param max_n - the maximum degree of an GGFM used in computation
   * @param tide_mode - the tide system used for the computation. Possible inputs:
   * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
   * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
   * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
   * @param ln - The Love's number used for Permanent Tide Systems transformation
   * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
   * @param p_nm - fully normalized accossiated Legendre polynomials or it's derivative
   * @param taylor_order - degree of the used taylor polynomial
   * @param start_k - init k for prod product \f$ \prod \limits_{i = k}^s (n + k )
   * @param pc_taylor - @see Tmat class with \f$ PC_{nm} = \alpha_n \cdot C_{nm} \cdot P_{nm} \f$,
   * where \f$ \alpha_n \f$ is arbitraty coeff or function and all is computed with respect to taylor
   * polynomial degree.
   * @param ps_taylor - @see Tmat class with \f$ PS_{nm} = \alpha_n \cdot S_{nm} \cdot P_{nm} \f$,
   * where \f$ \alpha_n \f$ is arbitraty coeff or function and all is computed with respect to taylor
   * polynomial degree.
   */
  void compute_lumped_coeff_taylor_nnf( const eT& r,
                                        const eT& phi,
                                        const unsigned& min_n,
                                        const unsigned& max_n,
                                        const string& tide_mode,
                                        const eT& ln,
                                        const geo_f::ellipsoid<eT>& ell,
                                        const Lvec<eT> &p_nm,
                                        const unsigned& taylor_order,
                                        const unsigned& start_k,
                                        Tmat<eT>& pc_taylor,
                                        Tmat<eT>& ps_taylor,
                                        eT (&mltply_fce) (const eT&)
                                        ) {
      unsigned mmax_n;
      if ( max_n > n_max ) {
      mmax_n = n_max;
      } else {
      mmax_n = max_n;
      }

      pc_taylor.resize( max_n+1, taylor_order+1 ); // [x0 , dx/dr , d^2x/dr^2 ,....]
      ps_taylor.resize( max_n+1, taylor_order+1 );

      // Tide mode conversion //
      eT alpha1 = static_cast<eT>( 11.18033988749894848204586834365638117720309179805762862135L);

      eT k = static_cast<eT>(start_k); // for prod product \f$ \prod \limits_{i = k}^{Taylor order} ( n+k+i ) \f$
      for ( unsigned int j = mmax_n ;; j--) {
          unsigned i_min = (j < min_n) ? min_n : j;
          unsigned idx_cs_nm  = c_nm.return_index( i_min,j );
          unsigned idx_p_nm = p_nm.return_index( i_min,j);

          eT a_r = pow(radius / r, static_cast<eT>(i_min) );
          for (unsigned int i = i_min; i <= mmax_n; i++) {
              eT n = static_cast<eT>(i);
              eT pc_nm , ps_nm ; // multiplication of P_nm with S_nm, C_nm, (R/r)^n etc...
              eT nn, sign_brac, frac, brac, gms, dc_nm = 0.0 /*must reconsider*/;
              if (i == 0 && j == 0) {
                  dc_nm = static_cast<eT>(-1.0) * (ell.gm / gm);
              } else if (i % 2 == 0 && i <= 20 && j == 0) {
                  /* ell.j20 has to be defined */

                  nn = n / 2.0;
                  sign_brac = (i % 4 == 0) ? 1.0 : -1.0;
                  frac = (3.0 * pow(ell.eccentricity, nn)) /
                          ((2.0 * nn + 1.0) * (2.0 * nn + 3.0) * sqrt(4.0 * nn + 1.0));
                  brac = 1.0 - nn -
                          alpha1 * nn * ( ell.j20) /
                          (ell.eccentricity);
                  gms = (ell.gm / gm) * pow((ell.a_axis) / radius, nn);

                  dc_nm = -1. * sign_brac * frac * brac * gms;

                  dc_nm *= (ell.gm / gm);
                  dc_nm *= pow( ell.a_axis / radius, nn);

                  if (i == 2 && j == 0) {
                      // transformation from tide_system of the ggfm to required tide
                      // system
                      // tide_system (from) >> tide_mode(to)
                      dc_nm += Ggfm<eT>::tide_system_transf( phi, ln, this->tide_system,
                              tide_mode, ell);
                  }
              } else {
                  dc_nm = .0;
              }

              if ( i % 2 == 0 && i <= 20 && j == 0  ) {
                  pc_nm = (c_nm[idx_cs_nm] + dc_nm ) * p_nm[idx_p_nm] * a_r * mltply_fce( n ); // TideSystem conversion
              } else {
                  pc_nm = c_nm[idx_cs_nm] * p_nm[idx_p_nm] * a_r * mltply_fce( n );
              }
              ps_nm = s_nm[idx_cs_nm] * p_nm[idx_p_nm] * a_r * mltply_fce( n );

              eT prod_n = static_cast<eT>(1.);
              /*
               * This part of the code is a little tricky
               * Take for example the gravity disturbance
               * the initial coeff is (n+1)is part of  mltply_fce( n )
               * for derivatives it goes:
               * first derivative: (n+1) * [n+2]
               * 2nd :  (n+1) * [n+2] * [n+3]
               * 3rd : (n+1) * [n+2] * [n+3] * [n+4]
               * etc.
               * That means next loop work like this:
               * l | prod_n
               * 0 | 1
               * 1 | n+2
               * 2 | (n+2)*(n+3)
               * 3 | (n+2)(n+3)(n+4)
               *
               * remember (n+1) and i tis part of  mltply_fce( n )
               * that means for gravity disturbance in spherical approx
               * the k should be 1
               **/
              for ( unsigned l = 0 ; l <= taylor_order; l++ ) {
                  if ( l != 0 ) {
                      prod_n *= ( n + k + static_cast<eT>(l));
                  }

                  pc_taylor(j,l) += pc_nm * prod_n;
                  ps_taylor(j,l) += ps_nm * prod_n;
              }

              a_r *= radius / r;  // faster approach than pow()
              idx_cs_nm++;
              idx_p_nm++;
          }

          if (j == 0)
              break;
      }
  }
  /**
   * @brief compute_lumped_coefficients - computes the lumped coefficients for computation of the GGFM. Input parameters:
   * @param r - geocentric radius in meetres
   * @param phi - geodetic latitude
   * @param min_n - the minimum degree of an GGFM used in computation
   * @param max_n - the maximum degree of an GGFM used in computation
   * @param tide_mode - the tide system used for the computation. Possible inputs:
   * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
   * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
   * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
   * @param ln - The Love's number used for Permanent Tide Systems transformation
   * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
   * @param p_nm - fully normalized accossiated Legendre polynomials or it's derivative
   * @param pc_m - reference to vector defined as: \f$ c_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar C_{nm} \bar P_{nm} ( \sin \phi ) \f$
   * @param ps_m - reference to vector defined as: \f$ s_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar S_{nm} \bar P_{nm} ( \sin \phi ) \f$
   *
   * Returns the pc_m, ps_m vectors.
   */
  void compute_lumped_coefficients( const eT& r ,
                                    const eT& phi,
                                    const unsigned& min_n,
                                    const unsigned& max_n,
                                    const string& tide_mode,
                                    const eT& ln,
                                    const geo_f::ellipsoid<eT>& ell,
                                    const Lvec<eT> &p_nm,
                                    vector<eT> &pc_m,
                                    vector<eT> &ps_m,
                                    eT (&mltply_fce) (const eT&)
                                    ) {
        unsigned mmax_n;
        if ( max_n > n_max ) {
        mmax_n = n_max;
        } else {
        mmax_n = max_n;
        }

        pc_m.clear(); pc_m.resize( max_n+1 );
        ps_m.clear(); ps_m.resize( max_n+1 );

        // Tide mode conversion //
        eT dc_nm = Ggfm<eT>::tide_system_transf(phi, ln, this->tide_system, tide_mode, ell);


        for ( unsigned int j = mmax_n ;; j--) {
            unsigned i_min = (j < min_n) ? min_n : j;
            unsigned idx_cs_nm  = c_nm.return_index( i_min,j );
            unsigned idx_p_nm = p_nm.return_index( i_min,j);

            eT pc_m_sum = 0.; // \f$ c_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar C_{nm} \bar P_{nm} ( \sin \phi ) \f$
            eT ps_m_sum = 0.; // \f$ s_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar S_{nm} \bar P_{nm} ( \sin \phi ) \f$

            eT a_r = pow(radius / r, static_cast<eT>(i_min) );
            for (unsigned int i = i_min; i <= mmax_n; i++) {
                eT n = static_cast<eT>(i);

                if ( j == 0 && i == 2 ) {
                    pc_m_sum += (c_nm[idx_cs_nm] + dc_nm ) * p_nm[idx_p_nm] * a_r * mltply_fce( n );
                } else {
                    pc_m_sum += c_nm[idx_cs_nm] * p_nm[idx_p_nm] * a_r * mltply_fce( n );
                }

                ps_m_sum += s_nm[idx_cs_nm] * p_nm[idx_p_nm] * a_r * mltply_fce( n );

                a_r *= radius / r;  // faster approach than pow()

                idx_cs_nm++;
                idx_p_nm++;
            }

            pc_m[j] = pc_m_sum;
            ps_m[j] = ps_m_sum;

            if (j == 0)
                break;
        }
  }


  /**
   * @brief compute_lumped_coefficients_terrain #TODO#DESCRIPTION
   * to allow the usage of already created templates, the unused parameters
   * will be ignored
   * @param r
   * @param phi
   * @param min_n
   * @param max_n
   * @param tide_mode
   * @param ln
   * @param ell
   * @param p_nm
   * @param pc_m
   * @param ps_m
   */
  void compute_lumped_coefficients_terrain (const eT& r ,
                                            const eT& phi,
                                            const unsigned& min_n,
                                            const unsigned& max_n,
                                            const string& tide_mode,
                                            const eT& ln,
                                            const geo_f::ellipsoid<eT>& ell,
                                            const Lvec<eT> &p_nm,
                                            vector<eT> &pc_m,
                                            vector<eT> &ps_m,
                                            eT (&mltply_fce) (const eT&) )
  {
      unsigned mmax_n;
      if ( max_n > n_max ) {
      mmax_n = n_max;
      } else {
      mmax_n = max_n;
      }

      pc_m.clear(); pc_m.resize( max_n+1 );
      ps_m.clear(); ps_m.resize( max_n+1 );

      // Tide mode conversion //
      //eT dc_nm = Ggfm<eT>::tide_system_transf(phi, ln, this->tide_system, tide_mode, ell);


      for ( unsigned int j = mmax_n ;; j--) {
          unsigned i_min = (j < min_n) ? min_n : j;
          unsigned idx_cs_nm  = c_nm.return_index( i_min,j );
          unsigned idx_p_nm = p_nm.return_index( i_min,j);

          eT pc_m_sum = 0.; // \f$ c_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar C_{nm} \bar P_{nm} ( \sin \phi ) \f$
          eT ps_m_sum = 0.; // \f$ s_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar S_{nm} \bar P_{nm} ( \sin \phi ) \f$

          for (unsigned int i = i_min; i <= mmax_n; i++) {
              //eT n = static_cast<eT>(i);
//              if ( j == 0 && i == 2 ) {
//                  pc_m_sum += (c_nm[idx_cs_nm] + dc_nm ) * p_nm[idx_p_nm];
//              } else {
//                  pc_m_sum += c_nm[idx_cs_nm] * p_nm[idx_p_nm];
//              }

              pc_m_sum += c_nm[idx_cs_nm] * p_nm[idx_p_nm];
              ps_m_sum += s_nm[idx_cs_nm] * p_nm[idx_p_nm];

              idx_cs_nm++;
              idx_p_nm++;
          }

          pc_m[j] = pc_m_sum;
          ps_m[j] = ps_m_sum;

          if (j == 0)
              break;
      }
  }

  /**
   * @brief compute_lumped_coefficients_nnf - computes the lumped coefficients for computation of the GGFM with removal of normal gravity field.
   * Input parameters:
   * @param r - geocentric radius in meetres
   * @param phi - geodetic latitude
   * @param min_n - the minimum degree of an GGFM used in computation
   * @param max_n - the maximum degree of an GGFM used in computation
   * @param tide_mode - the tide system used for the computation. Possible inputs:
   * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
   * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
   * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
   * @param ln - The Love's number used for Permanent Tide Systems transformation
   * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
   * @param p_nm - fully normalized accossiated Legendre polynomials or it's derivative
   * @param pc_m - reference to vector defined as: \f$ c_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar C_{nm} \bar P_{nm} ( \sin \phi ) \f$
   * @param ps_m - reference to vector defined as: \f$ s_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar S_{nm} \bar P_{nm} ( \sin \phi ) \f$
   *
   * Returns the pc_m, ps_m vectors.
   */
  void compute_lumped_coefficients_nnf( const eT& r ,
                                        const eT& phi,
                                        const unsigned& min_n,
                                        const unsigned& max_n,
                                        const string& tide_mode,
                                        const eT& ln,
                                        const geo_f::ellipsoid<eT>& ell,
                                        const Lvec<eT> &p_nm,
                                        vector<eT>& pc_m,
                                        vector<eT>& ps_m,
                                        eT (&mltply_fce) (const eT&)
                                        ) {
        unsigned mmax_n;
        if ( max_n > n_max ) {
        mmax_n = n_max;
        } else {
        mmax_n = max_n;
        }

        // Tide mode conversion //
        eT alpha1 = static_cast<eT>( 11.18033988749894848204586834365638117720309179805762862135L);
        pc_m.clear(); pc_m.resize( max_n+1 );
        ps_m.clear(); ps_m.resize( max_n+1 );

        for ( unsigned int j = mmax_n ;; j--) {
            unsigned i_min = (j < min_n) ? min_n : j;
            unsigned idx_cs_nm  = c_nm.return_index( i_min,j );
            unsigned idx_p_nm = p_nm.return_index( i_min,j);

            eT pc_m_sum = 0.; // \f$ c_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar C_{nm} \bar P_{nm} ( \sin \phi ) \f$
            eT ps_m_sum = 0.; // \f$ s_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar S_{nm} \bar P_{nm} ( \sin \phi ) \f$

            eT a_r = pow(radius / r, static_cast<eT>(i_min) );
            for (unsigned int i = i_min; i <= mmax_n; i++) {
                eT n = static_cast<eT>(i);
                eT nn, sign_brac, frac, brac, gms, dc_nm = 0.0 /*must reconsider*/;
                if (i == 0 && j == 0) {
                    dc_nm = static_cast<eT>(-1.0) * (ell.gm / gm);
                } else if (i % 2 == 0 && i <= 20 && j == 0) {
                    /* ell.j20 has to be defined */

                    nn = n / 2.0;
                    sign_brac = (i % 4 == 0) ? 1.0 : -1.0;
                    frac = (3.0 * pow(ell.eccentricity, nn)) /
                            ((2.0 * nn + 1.0) * (2.0 * nn + 3.0) * sqrt(4.0 * nn + 1.0));
                    brac = 1.0 - nn -
                            alpha1 * nn * ( ell.j20) /
                            (ell.eccentricity);
                    gms = (ell.gm / gm) * pow((ell.a_axis) / radius, nn);

                    dc_nm = -1. * sign_brac * frac * brac * gms;

                    dc_nm *= (ell.gm / gm);
                    dc_nm *= pow( ell.a_axis / radius, nn);

                    if (i == 2 && j == 0) {
                        // transformation from tide_system of the ggfm to required tide
                        // system
                        // tide_system (from) >> tide_mode(to)
                        dc_nm += Ggfm<eT>::tide_system_transf( phi, ln, this->tide_system,
                                tide_mode, ell);
                    }
                } else {
                    dc_nm = .0;
                }

                if ( i % 2 == 0 && i <= 20 && j == 0 ) {
                    pc_m_sum += (c_nm[idx_cs_nm] + dc_nm ) * p_nm[idx_p_nm] * a_r * mltply_fce( n );
                } else {
                    pc_m_sum += c_nm[idx_cs_nm] * p_nm[idx_p_nm] * a_r * mltply_fce( n );
                }

                ps_m_sum += s_nm[idx_cs_nm] * p_nm[idx_p_nm] * a_r * mltply_fce( n );

                a_r *= radius / r;  // faster approach than pow()

                idx_cs_nm++;
                idx_p_nm++;
            }

            pc_m[j] = pc_m_sum;
            ps_m[j] = ps_m_sum;

            if (j == 0)
                break;
        }
  }

  /**
   * @brief multiply_stokes_with_legendre_nnf - based on article published in Journal of geodesy :
   * Numerical computation of spherical harmonics of arbitraty degree and order by extedning exponent
   * of floating point numbers, written by Toshio Fukushima. Also the tide system is transformed and
   * normal gravity field model is removed.
   * The lumped coefficients are defined as :
   * -# \f$ c_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar C_{nm} \bar P_{nm} ( \sin \phi ) \f$
   * -# \f$ s_m = \sum \limits_{n=0}^n_{max} \left( \frac{R}{r} \right)^n \bar S_{nm} \bar P_{nm} ( \sin \phi ) \f$
   *
   * where R is reference radius of the used GGFM model, r is geocentric length, \f$ \bar C_{nm} , \bar S_{nm} \f$
   * are Stoke's coefficients and \f$ \bar P_{nm} (\sin \varphi) \f$ are fully normalized assocciated Legendre polynomials
   * for geocentric latitude \f$ \phi \f$. In this case the multiplication also includes factor based on function \f$ g(n) \f$.
   * For example for \f$ \frac{\partial V}{\partial r}\f$ the function is  \f$ g(n) = n + 1 \g$. @see
   * @param r - geocentric radius
   * @param phi - geodetic latitude
   * @param min_n - the minimum degree of an GGFM used in computation
   * @param max_n - the maximum degree of an GGFM used in computation
   * @param tide_mode - the tide system used for the computation. Possible inputs:
   * -# "mean tide" or "mean_tide" - The Geoid would exist in the presence of the Sun and the Moon (equivalently if no permanent tidal effects are removed).
   * -# "tide free" or "tide_free" - Based on thought that the Earth is rid of all direct and indirect effects of Sun and Moon.
   * -# "zero tide" or "zero_tide" - All direct tidal effects of the Sun and the Moon are removed, but indirect effect mostly related to the elasticity of the Earth is retained.
   * @param ln - The Love's number used for Permanent Tide Systems transformation
   * @param ell - Used reference ellipsoid (also look for @see ellipsoid in geodetic_functions.h )
   * @param p_nm - fully normalized accossiated Legendre polynomials or it's derivative
   * @param pc_nm - reference to output \f$ PC_{nm} = \alpha_n \cdot C_{nm} \cdot P_{nm} \f$, where \f$ \alpha_n \f$ is arbitraty coeff or function
   * @param ps_nm - reference to output \f$ PC_{nm} = \alpha_n \cdot S_{nm} \cdot P_{nm} \f$, where \f$ \alpha_n \f$ is arbitraty coeff or function
   */
  void multiply_stokes_with_legendre_nnf(const eT& r,
                                         const eT& phi,
                                         const unsigned int& min_n,
                                         const unsigned int& max_n,
                                         const string& tide_mode,
                                         const eT& ln,
                                         const geo_f::ellipsoid<eT>& ell,
                                         const Lvec<eT> &p_nm,
                                         Lvec<eT> &pc_nm,
                                         Lvec<eT> &ps_nm,
                                         eT (&mltply_fce)(const eT&))
  {
      unsigned mmax_n;
      if ( max_n > n_max ) {
        mmax_n = n_max;
      } else {
        mmax_n = max_n;
      }

      pc_nm.resize( n_max, col_order );
      ps_nm.resize( n_max, col_order );

      // Tide mode conversion //
      eT alpha1 = static_cast<eT>( 11.18033988749894848204586834365638117720309179805762862135L);

      for ( unsigned int j = mmax_n ;; j--) {
          unsigned i_min = (j < min_n) ? min_n : j;
          unsigned idx_cs_nm  = c_nm.return_index( i_min,j );
          unsigned idx_p_nm = p_nm.return_index( i_min, j);

          eT a_r = pow(radius / r, static_cast<eT>(i_min) );
          for (unsigned int i = i_min; i <= max_n; i++) {
              eT n = static_cast<eT>(i);
              eT nn, sign_brac, frac, brac, gms, dc_nm = 0.0 /*must reconsider*/;
              if (i == 0 && j == 0) {
                  dc_nm = static_cast<eT>(-1.0) * (ell.gm / gm);
              } else if (i % 2 == 0 && i <= 20 && j == 0) {
                  /* ell.j20 has to be defined */

                  nn = n / 2.0;
                  sign_brac = (i % 4 == 0) ? 1.0 : -1.0;
                  frac = (3.0 * pow(ell.eccentricity, nn)) /
                          ((2.0 * nn + 1.0) * (2.0 * nn + 3.0) * sqrt(4.0 * nn + 1.0));
                  brac = 1.0 - nn -
                          alpha1 * nn * ( ell.j20) /
                          (ell.eccentricity);
                  gms = (ell.gm / gm) * pow((ell.a_axis) / radius, nn);

                  dc_nm = -1. * sign_brac * frac * brac * gms;

                  dc_nm *= (ell.gm / gm);
                  dc_nm *= pow( ell.a_axis / radius, nn);

                  if (i == 2 && j == 0) {
                      // transformation from tide_system of the ggfm to required tide
                      // system
                      // tide_system (from) >> tide_mode(to)
                      dc_nm += Ggfm<eT>::tide_system_transf( phi, ln, this->tide_system,
                              tide_mode, ell);
                  }
              } else {
                  dc_nm = .0;
              }

              ps_nm[idx_cs_nm] = s_nm[idx_cs_nm]            * p_nm[idx_p_nm] * a_r * mltply_fce( n );
              pc_nm[idx_cs_nm] = (c_nm[idx_cs_nm] + dc_nm ) * p_nm[idx_p_nm] * a_r * mltply_fce( n );

              a_r *= radius / r;  // faster approach than pow()
              idx_cs_nm++;
              idx_p_nm++;
          }
          if (j == 0)
              break;
      }
  }

  eT sum_stokes_taylor(  const eT& r ,// m
                         const eT& u ,// radians
                         const eT& v, // radians
                         const eT& dr, // \f$ \Delta r = r - r_0 \approx h_{ell} \f$
                         const bool& even, // odd or even for (-1)^{odd/even}
                         const unsigned& init_r_power,
                         const unsigned& max_n,
                         const Tmat<eT>& pc_nm,
                         const Tmat<eT>& ps_nm,
                         eT (&sum_lstokes)( const eT&,
                                            const eT&,
                                            const eT&,
                                            const eT& ) )
  {
        unsigned mmax_n;
        if ( max_n > n_max ) {
          mmax_n = n_max;
        } else {
          mmax_n = max_n;
        }

        unsigned taylor_order = pc_nm.n_cols();// Taylor order +1, zero term is first column

        eT sum_m = 0.;
        if ( algorithm == "tmfcm" || algorithm == "tmfcm_2nd") {
            // Algorithm of summation for modified columns algorithms with relaxing factors
            // Reference/pointers to "relaxing" function
            eT (*um_func)(const int&, const eT&, const eT&);

            if (algorithm == "tmfcm" ) {
                um_func = &ggfm_f::um_function1<eT>;
            } else {
                um_func = &ggfm_f::um_function2<eT>;
            }

            for ( unsigned int j = mmax_n ;; j--) {
                eT m = static_cast<eT>(j);

                vector<eT> tpc = pc_nm.row(j);
                vector<eT> tps = ps_nm.row(j);

                eT sum_t = 0;
                for ( unsigned int i = 0; i < taylor_order; i++) {
                    eT nfac = phys_math::factorial<eT>(i);

                    eT minus_one;
                    if ( even ) {
                        minus_one = static_cast<eT>( pow(-1, i+1) ); // ( 2*i % 2 == 0 ) ? -1 : +1 ;
                    } else {
                        minus_one =  static_cast<eT>( pow(-1, i) ); //( 2*i % 2 == 0 ) ? +1 : -1 ;
                    }

                    sum_t += minus_one / (pow ( r , static_cast<eT>( init_r_power + i) ) * nfac )// r_0^{-(i+init_r_power)}
                            * sum_lstokes( tpc[i], tps[i] , m, v)   // standard stokes summation
                            * pow( dr , static_cast<eT>(i) );       // \f$ \Delta r^i = (r-r_0)^i \approx h^i \f$
                }

                sum_m += sum_t;
                sum_m *= um_func(j, m, u );

                if (j == 0)
                    break;
            }
        } else {
            for ( unsigned int j = mmax_n ;; j--) {
                eT m = static_cast<eT>(j);

                vector<eT> tpc = pc_nm.row(j);
                vector<eT> tps = ps_nm.row(j);

                eT sum_t = 0;
                for ( unsigned int i = 0; i < taylor_order; i++) {
                    eT minus_one;
                    if ( even ) {
                        minus_one = static_cast<eT>( pow(-1, i) ); // ( 2*i % 2 == 0 ) ? -1 : +1 ;
                    } else {
                        minus_one =  static_cast<eT>( pow(-1, i) ); //( 2*i % 2 == 0 ) ? +1 : -1 ;
                    }

                    sum_t += minus_one / pow ( r , static_cast<eT>( init_r_power + i) ) // r_0^{-(i+init_r_power)}
                            * sum_lstokes( tpc[i], tps[i] , m, v)   // standard stokes summation
                            * pow( dr , static_cast<eT>(i) );       // \f$ \Delta r^i = (r-r_0)^i \approx h^i \f$
                }

                sum_m += sum_t;

                if (j == 0)
                    break;
            }
        }
        return this->gm * sum_m/ggfm_f::relax_factor<eT>();
  }

  /**
   * @brief grid_computation_template
   * @param b_min - minimum value of the geodetic latitude
   * @param b_max - maximum value of the geodetic latitude
   * @param l_min - minimum value of the geodetic longitude
   * @param l_max - maximum value of the geodetic longitude
   * @param height - height of the grid
   * @param bstep - step in latitude
   * @param lstep - step in longitude
   * @param algorithm - used type of the algorithm
   * @param min_n - minimum value of the used degree of ggfm
   * @param max_n - maximum value of the used degree of ggfm
   * @param derivative - used degree of the derivative
   * based on the required gravity parameter
   * @param tide_mode - used tide mode
   * @param ln - love number for the tide system transformations
   * @param ell - ellipsoid that are the coordinates based on
   * @return grid
   */
  arma::mat grid_computation_template(const eT& b_min,
                                      const eT& b_max,
                                      const eT& l_min,
                                      const eT& l_max,
                                      const eT& height,
                                      const eT& bstep,
                                      const eT& lstep,
                                      const unsigned int& min_n,
                                      const unsigned int& max_n,
                                      const unsigned int& derivative,
                                      const string& tide_mode,
                                      const eT& ln,
                                      const geo_f::ellipsoid<eT>& ell,
                                      eT (&mltply_fce)(const eT&),
                                      eT (&sum_coeff_in_stokes) (const eT& ,
                                                                 const eT&,
                                                                 const eT&,
                                                                 const eT&),
                                      void (Ggfm<eT>::*mtply_stokes_with_legendre) (const eT&,
                                                                                    const eT&,
                                                                                    const unsigned int&,
                                                                                    const unsigned int&,
                                                                                    const string&,
                                                                                    const eT&,
                                                                                    const geo_f::ellipsoid<eT>& ,
                                                                                    const Lvec<eT>& ,
                                                                                    vector<eT>&,
                                                                                    vector<eT>&,
                                                                                    eT (&)(const eT&))
                                      ) {
      // generate grid
      unsigned int b_count = static_cast<unsigned int>( abs( floor((b_max - b_min) / bstep)) );
      unsigned int l_count = static_cast<unsigned int>( abs( floor((l_max - l_min) / lstep)) );

      arma::mat grid = arma::zeros<arma::mat>(b_count+1, l_count+1);

      Progressbar p_bar( static_cast<int>(b_count) );
      p_bar.set_name( "Ggfm::grid_computation"  );
      for (unsigned int i = 0; i <= b_count; i++) {
            #pragma omp critical
            {  p_bar.update();  }

          eT b = DEG2RAD * ( b_min + ( static_cast<eT>(i) * bstep));  // current geodetic latitude
          eT n = ell.a_axis / sqrt(1 - ell.eccentricity * sin(b) * sin(b));
          eT u  =atan(tan(b) * (n * (1 - ell.eccentricity) + height) / (n + height));
          eT u_deg = RAD2DEG *u;

          // point geocentric length
          eT pt_rad =  sqrt((( 1 - ell.eccentricity/2 )*n+height)*(cos(b)*cos(b))*2*ell.eccentricity*n
                         +(n*(1-ell.eccentricity) + height)*(n*(1-ell.eccentricity) + height));

          // Cmputation of the fully normalized assocciated Legendre
          FnALFs<eT> legen(max_n, algorithm, derivative);
          legen.compute_FnALFs( u_deg );

          Lvec<eT> p_nm_vec;
          vector<eT> pc_nm_vec, ps_nm_vec;

          if ( derivative == 0 ) {
              p_nm_vec = legen.ref2p_nm();
          } else if ( derivative == 1 ) {
              p_nm_vec = legen.ref2dp_nm();
          } else {
              p_nm_vec = legen.ref2ddp_nm();
          }

          // Compute multiplications between fnALFs and C_nm, S_nm
          (this->*mtply_stokes_with_legendre)( pt_rad, b*RAD2DEG, min_n, max_n, tide_mode, ln, ell, p_nm_vec, pc_nm_vec, ps_nm_vec, mltply_fce );

          // Loop for parallel computations
          #pragma omp parallel shared(grid, u, pc_nm_vec, ps_nm_vec, min_n, max_n )
          {

          #pragma omp for
          for (unsigned int j = 0; j <= l_count; j++) {
              eT v = (l_min + ( static_cast<eT>(j) * lstep ))*DEG2RAD ; // actual longitude , spherical long. == ell. long

              grid(i, j) = Ggfm<eT>::sum_stokes_with_legendre( u ,    // spherical latitude
                                                               v ,    // spherical longitude
                                                               min_n,
                                                               max_n,      // interval of the used degree of the model
                                                               pc_nm_vec,  // P_{nm} * C_{nm} * (R/r)^n
                                                               ps_nm_vec,  // P_{nm} * S_{nm} * (R/r)^n
                                                               sum_coeff_in_stokes );
          }

          #pragma omp barrier
          }//  #pragma omp parallel shared(grid, u, pc_nm_vec, ps_nm_vec, min_n, max_n )

      }

      return grid;
  }

  /**
   * @brief ongrid_computation_template #TODO#description
   * @param terrain
   * @param min_n
   * @param max_n
   * @param derivative
   * @param taylor_order
   * @param start_k
   * @param even
   * @param init_r_power
   * @param tide_mode
   * @param ln
   * @param ell
   * @param crds_system - spherical or ellipsoidal coordinates,{sph,ell}, ell by default
   * @return
   */
  arma::mat ongrid_computation_template( Isgemgeoid terrain , // extract b_min, b_max, l_min, l_max, delta_b, delta_l, no_data, number of rows and columns
                                         const unsigned& min_n,
                                         const unsigned& max_n,
                                         const unsigned& derivative, // derivation of fnALFs
                                         const unsigned& taylor_order,
                                         const unsigned& start_k,
                                         const bool& even,
                                         const unsigned& init_r_power,
                                         const string& tide_mode,
                                         const eT& ln,
                                         const geo_f::ellipsoid<eT>& ell,
                                         const string& crds_system,
                                         eT (&mltply_fce)(const eT&),
                                         eT (&sum_coeff_in_stokes) (const eT& ,
                                                                    const eT&,
                                                                    const eT&,
                                                                    const eT&),
                                         void (Ggfm<eT>::*taylor_lumped_coeff)( const eT&,
                                                                                const eT&,
                                                                                const unsigned&,
                                                                                const unsigned&,
                                                                                const string&,
                                                                                const eT&,
                                                                                const geo_f::ellipsoid<eT>&,
                                                                                const Lvec<eT> &,
                                                                                const unsigned&,
                                                                                const unsigned&,
                                                                                Tmat<eT>&,
                                                                                Tmat<eT>&,
                                                                                eT (&) (const eT&)
                                                                                )
                                         ) {
      //--------------------------------------------------------------------------------------------------
      // Gathering header info from DEM
      unsigned rows, cols;
      vector<eT> header = terrain.get_header<eT>(rows, cols); //  bmin, bmax, lmin, lmax, deltab, deltal, nodata
      eT b_min = header[0];
      //eT b_max = header[1];
      eT l_min = header[2];
      //eT l_max = header[3];
      eT bstep = header[4];
      eT lstep = header[5];
      eT nodat = header[6];

      arma::mat grid(rows,cols);

      Progressbar p_bar( static_cast<int>(rows));
      p_bar.set_name( "Ggfm<eT>::ongrid_computation");
      for (unsigned int i = 0; i < rows; i++) {
        #pragma omp critical
        {  p_bar.update();  }

          eT n, u, u_deg, r0;
          eT b = DEG2RAD * ( b_min + ( static_cast<eT>(i) * bstep));  // current geodetic latitude
          eT mean_height = static_cast<eT>( terrain.mean_value_row( i )) ; // mean value of the height for given row

          if ( crds_system == "sph" ) {
              u_deg = b/DEG2RAD;
              u = b;
              r0 = this->radius + mean_height;
          } else /* crds_system == "ell"*/ {

              n = ell.a_axis / sqrt(1 - ell.eccentricity * sin(b) * sin(b));
              u  =atan(tan(b) * (n * (1 - ell.eccentricity) + mean_height) / (n + mean_height));
              u_deg = RAD2DEG *u;

              // point geocentric length
              r0 =  sqrt((( 1 - ell.eccentricity/2 )*n+mean_height)*(cos(b)*cos(b))*2*ell.eccentricity*n
                             +(n*(1-ell.eccentricity) + mean_height)*(n*(1-ell.eccentricity) + mean_height));
          }
          // Cmputation of the fully normalized assocciated Legendre
          FnALFs<eT> legen(max_n, algorithm, derivative);
          legen.compute_FnALFs( u_deg );

          Lvec<eT> p_nm_vec;
          Tmat<eT> pc_nm_taylor;
          Tmat<eT> ps_nm_taylor;

          if ( derivative == 0 ) {
              p_nm_vec = legen.ref2p_nm();
          } else if ( derivative == 1 ) {
              p_nm_vec = legen.ref2dp_nm();
          } else {
              p_nm_vec = legen.ref2ddp_nm();
          }

          // Compute multiplications between fnALFs and C_nm, S_nm
          (this->*taylor_lumped_coeff)( r0,   // m
                                        u_deg, //b*RAD2DEG,// deg
                                        min_n,
                                        max_n,
                                        tide_mode,
                                        ln, ell,
                                        p_nm_vec,
                                        taylor_order,
                                        start_k,
                                        pc_nm_taylor,
                                        ps_nm_taylor,
                                        mltply_fce );

          // Loop for parallel computations
          #pragma omp parallel shared(grid, u, pc_nm_taylor, ps_nm_taylor, min_n, max_n )
          {

          #pragma omp for
          for (unsigned int j = 0; j < cols; j++) {
              eT v = (l_min + ( static_cast<eT>(j) * lstep ))*DEG2RAD ; // actual longitude , spherical long. == ell. long
              eT dr = terrain.get_value(i,j) - mean_height;

              if ( dr == nodat ) {
                  grid(i, j) = nodat;
                  continue;
              }

              grid(i, j) = Ggfm<eT>::sum_stokes_taylor( r0,// geocentric radius
                                                        u ,    // spherical latitude
                                                        v ,    // spherical longitude
                                                        dr,    // dr - height of the point
                                                        even,  // bool first sign of the taylor +1/-1
                                                        init_r_power,
                                                        max_n,      // interval of the used degree of the model
                                                        pc_nm_taylor,  // P_{nm} * C_{nm} * (R/r)^n * ....
                                                        ps_nm_taylor,  // P_{nm} * S_{nm} * (R/r)^n * .....
                                                        sum_coeff_in_stokes );
          }

          #pragma omp barrier
          }//  #pragma omp parallel shared(grid, u, pc_nm_vec, ps_nm_vec, min_n, max_n )
      }

      return grid;
  }

  /**
   * @brief Ggfm::print_grid_header
   * @param stream
   * @param b_min
   * @param b_max
   * @param l_min
   * @param l_max
   * @param height
   * @param bstep
   * @param lstep
   * @param min_n
   * @param max_n
   * @param tide_mode
   * @param ln
   * @param ell
   */
  void print_grid_header( ostream& stream,
                          const eT& b_min,
                          const eT& b_max,
                          const eT& l_min,
                          const eT& l_max,
                          const eT& height,
                          const eT& bstep,
                          const eT& lstep,
                          arma::mat computed_data,
                          const unsigned int& min_n,
                          const unsigned int& max_n,
                          const string& tide_mode,
                          const eT& ln,
                          const geo_f::ellipsoid<eT>& ell,
                          string units_out ) {
      if ( stream.good() ) {
          //        unsigned int b_count = static_cast<unsigned int>( floor((b_max - b_min) / bstep)) +1 ;
          //        unsigned int l_count = static_cast<unsigned int>( floor((l_max - l_min) / lstep)) +1;
          stream << setw(20) << setprecision(15);
          stream << "begin_of_head ================================================\n";
          stream << "model name   :     " << model_name   << endl;
          stream << "model tidesystem : " << tide_system  << endl;
          stream << "used tidesystem :  " << tide_mode    << endl;
          stream << "product_type :     " << product_type << endl;
          stream << "normalized   :     " << normalized   << endl;   ///< Info about normalization of the coefficients
          stream << "model radius =     " << radius       << endl;   ///< radius of sphere - scaling parameter
          stream << "model gm     =     " << gm           << endl;   ///< geocentric gravity constant
          stream << "epoch        =     " << epoch        << endl;   ///< reference epoch for gravity field model
          stream << "ref_ell      :     " << ell.name     << endl;
          stream << "lat min      =     " << b_min        << endl;
          stream << "lat max      =     " << b_max        << endl;
          stream << "lon min      =     " << l_min        << endl;
          stream << "lon max      =     " << l_max        << endl;
          stream << "height       =     " << height       << endl;
          stream << "nodata       =     " << -9999.999    << endl;
          stream << "delta lat    =     " << bstep        << endl;
          stream << "delta lon    =     " << lstep        << endl;
          stream << "nrows        =     " << computed_data.n_rows << endl;
          stream << "ncols        =     " << computed_data.n_cols << endl;
          stream << "n_min        =     " << min_n        << endl;
          stream << "n_max        =     " << max_n        << endl;
          stream << "love number  =     " << ln           << endl;
          stream << "units        =     " << units_out    << endl;
          stream << "end_of_head ==================================================\n";
          arma::mat flipped_rows =  arma::flipud( computed_data );
          stream << scientific;
          flipped_rows.raw_print(stream); // http://arma.sourceforge.net/docs.html#print
      } else {
          cerr << "Unable to use the \"ofstream &stream\" in function void Ggfm::print_grid_header(...)" << endl;
      }
  }

  ///////////////////////
  ///< PRIVATE MEMBERS //
  ///////////////////////
 private:
  string model_name;    ///< Name of the GGFM
  bool show_coeff;      ///< If it's set to true it will also show the coefficients for
                        ///< the model
  bool col_order;       ///< @see Lvec and the ordering of the elements inside the vector (by columns, or by rows)
  Lvec<eT> c_nm;        ///< C_nm coefficients of the GGFM
  Lvec<eT> s_nm;        ///< S_nm coefficients of the GGFM
  /* c_nm_err */        ///< Missing c_nm formal errors
  /* s_nm_err */        ///< Missing s_nm formal errors
  eT radius;        ///< radius of sphere - scaling parameter
  eT gm;            ///< geocentric gravity constant
  float epoch;          ///< reference epoch for gravity field model
  unsigned int n_min;   ///< Maximum degree of the geopotential model
  unsigned int n_max;   ///< Minimum degree of the geopotential model
  string product_type;  ///< GGFM product type
  string normalized;    ///< Info about normalization of the coefficients
  string tide_system;   ///< Tidal system information {tide-free,
                        ///< zero-tide,mean-tide }
  string errors;        ///< Errors of the GGFM
  string algorithm;     ///< Algorithm used for computation ("stan_mod", "standard", "tmfrm", "tmfcm", "tmfcm_2nd" )

//  const string options = {"gpot_v",
//                          "gpot_w",
//                          "gpot_t",
//                          "grav_gy",
//                          "grav_gx",
//                          "grav_gz",
//                          "def_vert_eta",
//                          "def_vert_xi",
//                          "def_vert",
//                          "grav_anom_sa",
//                          "grav_dist_sa",
//                          "height_anomaly",
//                          "grav_vec_mag"};
};

#endif  // GGFM_H
