#ifndef PHYSICAL_CONSTANTS_H
#define PHYSICAL_CONSTANTS_H

/* UNDEF */
#undef CSPEED
#undef NG_CONST
#undef ELEM_CHARGE
#undef MASS_E
#undef MASS_N
#undef A_WGS
#undef B_WGS
#undef E_WGS
#undef F_WGS
#undef SI2MGAL
#undef DEG2RAD
#undef RAD2DEG
#undef RAD2SEC
#undef TESS_POT_RATIO
#undef TESS_GX_RATIO
#undef TESS_GXX_RATIO

/* MATH CONSTANTS */
#define PI_LD       3.14159265358979323851280895940618620443274267017841339111328125L
#define E_LD        2.71828182845904523536028747135266249775724709369995L

#define PI_D        3.141592653589793115997963468544185161590576171875
#define E_D         2.718281828459045235360287471352662497757247093699
/* SOLAR SYSTEM DYNAMICS */
/* MASS RATIO CONSTANTS
 * source: http://ssd.jpl.nasa.gov/?constants
 *
 */
#define SUN_MASS        1.9884159e+30       // [kg]
#define EARTH_MASS      5.9721864e+24       // [kg]
#define MOON_MASS       7.3458114e+22       // [kg]
#define MERCURY_MASS    3.3010423e+23       // [kg]
#define VENUS_MASS      4.8673205e+24       // [kg]
#define MARS_MASS       6.4169283e+23       // [kg]
#define JUPITER_MASS    1.8985234e+27       // [kg]
#define SATURN_MASS     5.6845960e+26       // [kg]
#define URANUS_MASS     8.6819089e+25       // [kg]
#define NEPTUNE_MASS    1.0243093e+26       // [kg]
#define PLUTO_MASS      1.4560109e+22       // [kg]

/* UNIVERSAL CONSTANTS */
#define CSPEED      299792458   // speed of light [m/s]
#define NG_CONST    6.67408e-11 // Newtonian gravity constant [m^3 kg^-1 s^-2]
#define NG_UNCER    6.7e-15     // -||- uncertainty [m^3 kg^-1 s^-2]

#define GM_S        1.32712442099e+20       // heliocentric gravity constant [m^3 s^-2]


/* ELECTROMAGNETIC CONSTANTS */
#define ELEM_CHARGE 1.6021765314e-19    // elementary charge [C]
#define MASS_E      9.109382616e-31     // mass of electron [kg]


/* ATOMIC AND NUCLEAR */
#define MASS_N      1.6749272829-27     // mass of neutron   [kg]

/* PHYSICO-CHEMICAL */

/* GEODETIC CONSTANTS */
#define WGS84_A       6378137.000           // [m] WGS84 semi-major axis
#define WGS84_B       6356752.31424518L     // [m] WGS84 semi-minor axis
#define WGS84_E       0.0818191908426215    // [-] WGS84 eccentricity
#define WGS84_F       0.003352811           // [-] WGS84 flatenning
#define WGS84_J20     108263.0e-8           // second zonal coefficient [unitless] - not normalized
#define WGS84_WE      7292115.0e-11         // speed of Earth's rotation [ rad s^-1]
#define WGS84_GM      3986004.4180e+8       // geocentric gravity constant [ m^3 s^-2 ]
#define WGS84_GE      9.78032677            // normal gravity acceleration on eqautor [ms^-2]
#define WGS84_GP      9.83218637            // normal gravity acceleration on poles [ -||- ]
#define WGS84_GMEAN   9.806224173           // mean value normal gravity (from mean value theorem)
#define WGS84_RMEAN   6367469.498           // from integral mean value theorem
#define WGS84_RLMEAN  6.36754098217714754616796613117603325027137630257097929e+06L
#define WGS84_RRMEAN  6.367480596e+06L
#define WGS84_C20     -0.484166774985e-03L; // Fully normalized J_20 of WGS84 ellipsoid

/* GEOPHYSICAL CONSTANTS */
#define MEAN_DENSITY  2670.0                // kg s^-3

/* CONSTANTS FOR NUMERICAL COMPUTATIONS */
#define __EPS__          2.22044604925031308084726e-16 // for double precision
#define SI2EOTVOS        1.0e9               // Conversion factor from SI units to Eotvos
                                             // [\f$ \frac{1}{s^2} = 10^9\ Eotvos \f$] */
#define SI2MGAL          1.0e5               // Conversion factor from SI units to mGal
#define DEG2RAD          0.01745329251994329576923691
#define RAD2DEG          57.29577951308232087679815
#define RAD2SEC          206264.80624709636322223


/* CONSTANTS FOR TESSEROID SPLITTING  */
#define TESS_POT_RATIO   6.0
#define TESS_GX_RATIO    9.0
#define TESS_GXX_RATIO   10.0

/* CONSTANTS FOR GAUSSIAN TESSEROID SPLITTING  */
#define TESSGQ_POT_RATIO   8.0
#define TESSGQ_GX_RATIO    10.0
#define TESSGQ_GXX_RATIO   12.0


#endif // PHYSICAL_CONSTANTS_H
