#ifndef LEGENDRE_H
#define LEGENDRE_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <string>
#include <fstream>
#include <typeinfo>
#include <utility>
#include <new>
#include <cstddef>         // std::size_t

#include <omp.h>
#define ARMA_USE_BLAS
#include <armadillo>

using namespace std;


// This code is modified version of TomoMiner C++ core  - Associated Legendre polynomials. I also have my own creation and it's
// stored in fnalfs.cpp/fnalfs.h , just was lazy to rewrite it to arma::cube format
namespace legendre{

    /**
     * @brief generate_legendre - generate legendre polynomials - not normalized.
     * @param l_max - degree and order of assocciated legendre polynomials
     * @param data - vector for which values the legen coeff are generated. Vector
     * of values \f$ data = [x_0, x_1 , \ldots , x_n] , x_i \in \mathbb{R}\f$ .
     * @return arma::cube object, where are 3 indexes, n_rows, n_cols, n_slices. Each
     * slice corresponds with \$ x_i \f$ , and \textit{n_rows} are for degrees and
     * \textit{n_cols} are for orders.
     */
    arma::cube legendre(unsigned int l_max, const arma::vec &data);

    /**
     * @brief generate_legendre - generate legendre polynomials - \f$ 4 \pi \f$ normalization.
     * @param l_max - degree and order of assocciated legendre polynomials
     * @param data - vector for which values the legen coeff are generated. Vector
     * of values \f$ data = [x_0, x_1 , \ldots , x_n] , x_i \in \mathbb{R}\f$ .
     * @return arma::cube object, where are 3 indexes, n_rows, n_cols, n_slices. Each
     * slice corresponds with \$ x_i \f$ , and \textit{n_rows} are for degrees and
     * \textit{n_cols} are for orders.
     */
    arma::cube normalized_legendre(unsigned int l_max, const arma::vec &data);

    /**
     * @brief legendre generate the vector of the legendre polynomials - not normalized
     * for argument x in interval \f$ x \in [-1. , +1. ] \f$
     * @param n_max - maximum degree of the polynomial
     * @param x - real value of the arguments
     * @return  arma::vec in [P_0 (x) , P_1(x) , .... , P_n(x)]
     */
    arma::vec legendre(unsigned int n_max, const double& x);

    /**
     * @brief sum_kernel - instead of using the closed kernel (Stokes' kernel, Hotine's Kernel)
     * @param p_n -
     * @param kernel_type - {stokes (default), hotine}
     * # \f$ S(\psi) = f$ \sum \limits_{n=2}^{\infty} \frac{2n+1}{n-1} P_n (\cos \psi) \f$
     * # \f$ S(\psi) = f$ \sum \limits_{n=0}^{\infty} \frac{2n+1}{n+1} P_n (\cos \psi) \f$
     * @param n_min - the minimum boundary in previously introduced sums
     * @param n_max - the maximum boundary in previously introduced sums
     * @return value of the previously introduced sums
     */
    double sum_kernel ( const arma::vec &p_n,
                        const string& kernel_type,
                        const unsigned &n_min,
                        const unsigned &n_max );

}
#endif // LEGENDRE_H
