#ifndef MY_VECTOR_H
#define MY_VECTOR_H

#include <iostream>
#include <stdexcept>
#include <exception>
#include <system_error>

using namespace std;

class my_vector {
public:
    my_vector();

    double& operator[](unsigned i) {
        if (i >= 3) {
            throw runtime_error("my_vector index out of the bounds!");
        }

        if (maxAccessedIndex < static_cast<int>(i) ) {
            maxAccessedIndex = static_cast<int>(i);
        }

        return mem[i];
    }

    double& operator[](int i) {
        if (i >= 3) {
            throw runtime_error("my_vector index out of the bounds!");
        }

        if (maxAccessedIndex < i ) {
            maxAccessedIndex = i;
        }

        return mem[i];
    }

    int size() {
        return maxAccessedIndex + 1;
    }
private:
    double mem[3];
    int maxAccessedIndex = -1;
};

#endif // MY_VECTOR_H

