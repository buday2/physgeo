#ifndef POINTS_H
#define POINTS_H

#include <string>
#include <map>
#include <algorithm>
#include <iomanip>
#include <iostream>

#include <vector>
#include <cmath>

#include <istream>
#include <ostream>
#include <fstream>
#include <sstream>

#include <armadillo>

using namespace std;

class Points
{
    public:
        Points();
        ~Points();

        /**
         * @brief clears the data from map<...> points
         */
        void clear();


        /**
         * @brief loadfromfile - load data from ascii file
         * @param path - path to the data
         * @param comments - symbol that is used for comments in given ascii file ( '#', '/' )
         */
        void load_from_file( const string& path,
                             const string& comments);

        /**
         * @brief loadfromfile - load data from ascii file
         * @param path - path to the data
         * @param noc - number of the columns that should be imported
         * @param comments - symbol that is used for comments in given ascii file ( '#', '/' )
         */
        void load_from_file( const string& path,
                             const string& comments,
                             unsigned int noc );


        /**
         * @brief operator <<
         * @param stream
         * @param crds
         * @return
         */
        friend ostream& operator<<(ostream& stream, Points crds) {
            for ( unsigned i = 0; i <crds.pointsID.size(); i++ ) {
                stream << crds.pointsID[i];
                for (unsigned j = 0; j < crds.coords.n_cols; j++ ) {
                    stream << "  " << arma::as_scalar( crds.coords(i, j) );
                }

                stream << endl;
            }
            return stream;
        }


        vector<string> pointsID; ///< Storing id of points as string
        arma::mat coords;        ///< coordinates stored with respect to the @see pointsID
    private:
        /**
         * @brief rwhch_from_beg
         * @param sentence
         */
        void rwhch_from_beg( string& sentence );
};

#endif // POINTS_H
