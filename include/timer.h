#ifndef TIMER_H
#define TIMER_H

#include <iomanip>
#include <iostream>
#include <ctime>
#include <omp.h>

#include "dms.h"

using namespace std;

/**
 * @brief The Timer class - measure the time elapsed based on
 * omp_wtime_get()
 */
class Timer {
    public:
        Timer( const string& info_timer );
        ~Timer();
    private:
        double t_init_time; ///< initial time value
        string t_timer;   ///< The methood that is measured.

};

#endif // TIMER_H
