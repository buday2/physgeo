#ifndef LVEC_H
#define LVEC_H

#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <exception>
#include <system_error>
#include <string>
#include <cmath>
#include <new>

using namespace std;

/**
 * @brief The Lvec class - vector, created speacially for computation of fully normalized
 * accossiated legendre polynomials up to high degrees and ondrers, but the elements are
 * aliigned in vector (one dimensional field) rather then matrix (triangle upper/lower).
 * Template is used for double/long double values.
 */
template <typename eT>
class Lvec
{
public:
    /**
     * @brief Lvec - copy constructor;
     * @param olvec
     */
    Lvec( const Lvec<eT>& olvec ) {
        data     = olvec.data;
        columns  = olvec.columns;
        n_elem   = olvec.n_elem;
        n_maxdeg = olvec.n_maxdeg;
    }

    Lvec(const unsigned& nmax, bool t = true) {
        n_elem = (nmax + 1)*(nmax+2)/2 ;
        n_maxdeg = nmax;

        columns = t;
        data = new eT[n_elem];
    }

    /**
     * @brief Lvec - empty constructor, use resize to configure Lvec, @see rezise
     */
    Lvec(){}

    ~Lvec(){
        delete [] data;
    }

    /**
     * @brief resize change size while not keeping elements or preserving layout
     * @param new_size
     * @param t
     */
    void resize( const unsigned& new_size, bool t = true ) {
        // creating new data
        this->n_maxdeg = new_size;
        this->n_elem   =(n_maxdeg+1)*(n_maxdeg+2)/2 ;
        this->columns  = t;


        if ( this->data == nullptr ) {
            this->data = new eT[this->n_elem];
        } else {
            delete [] this->data;
            this->data = nullptr;
            this->data = new eT[this->n_elem];
        }
    }

    /**
     * @brief reverse_nm
     * @param nelems
     * @param n
     * @param m
     */
    void reverse_nm (const unsigned& nelems, unsigned& n, unsigned& m)
    {
        if ( columns ) {
            n = (static_cast<unsigned int>(sqrt( 1 + 8*nelems)) - 1)/2;
            m = nelems - n*(n+1)/2;
        } else {
            m = 0;
            n = 0;

            int spare = static_cast<int>( nelems );
            int max_n = static_cast<int>( n_maxdeg+1 );
            if ( !(nelems == 0) ){
                while ( spare - max_n >= 0) {
                    spare -= max_n--;
                    m++;
                }
                n = nelems + m*(m+1)/2 - m*(n_maxdeg+1);
            }
        }
    }

    /**
     * @brief reverse_nm
     * @param nelems
     * @param n
     * @param m
     */
    void reverse_nm (const unsigned& nelems, unsigned& n, unsigned& m) const
    {
        if ( columns ) {
            n = (static_cast<unsigned int>(sqrt( 1 + 8*nelems)) - 1)/2;
            m = nelems - n*(n+1)/2;
        } else {
            m = 0;
            n = 0;

            int spare = static_cast<int>( nelems );
            int max_n = static_cast<int>( n_maxdeg+1 );
            if ( !(nelems == 0) ){
                while ( spare - max_n >= 0) {
                    spare -= max_n--;
                    m++;
                }
                n = nelems + m*(m+1)/2 - m*(n_maxdeg+1);
            }
        }
    }


    /**
     * @brief return_index
     * @param n
     * @param m
     */
    size_t return_index( const size_t& n, const size_t& m )
    {
        unsigned e;

        if ( columns ) {
            e = (n*n+n+2)/2 +m - 1;
        } else {
            e = (m*(n_maxdeg+1) - m*(m+1)/2 + n);
        }

        return e;
    }

    /**
     * @brief return_index
     * @param n
     * @param m
     */
    size_t return_index( const size_t& n, const size_t& m ) const
    {
        unsigned e;

        if ( columns ) {
            e = (n*n+n+2)/2 +m - 1;
        } else {
            e = (m*(n_maxdeg+1) - m*(m+1)/2 + n);
        }

        return e;
    }

    /**
     * @brief reverse_columns - reverse the ordering in data vector from
     * $ [P_{00}, P_{10}, P_{11}, P_{20}, P_{21}, P_{22}, P_{30}, P_{31} , \ldots  ] to
     *  [P_{00}, P_{10}, P_{20}, P_{30}, \ldots ,  P_{n_{max} 0 }, P_{11}, P_{21}, P_{31} , \ldots ,  P_{n_{max} 1 }, P_{22} , \ldots  ]
     * and vice versa.
     */
    void reverse_columns()
    {
        eT* new_colums = new eT[n_elem];

        unsigned i = 0, e, nnn, mmm;
        if ( columns ) {
            for (unsigned n = 0; n <= n_maxdeg; n++) {
                for (unsigned m = 0; m <= n ; m++) {
                    Lvec<eT>::rev_nm( i,nnn,mmm, !columns);

                    e = Lvec<eT>::ridx( nnn,mmm, columns );
                    new_colums[e] = data[i++];
                }
            }
        } else {
            for (unsigned m =0 ; m <= n_maxdeg; m++ ) {
                for (unsigned n = m; n<= n_maxdeg; n++) {
                    Lvec<eT>::rev_nm( i,nnn,mmm, columns);
                    e = Lvec<eT>::ridx( nnn,mmm, !columns );

                    new_colums[e] = data[i++];
                }
            }
        }
        delete [] data;
        data = new_colums;

        columns = !columns;
    }

    /**
     * @brief max
     * @return maximum value for vector
     */
    eT max() {
        eT maxval = data[0];

        if ( n_elem > 0 ){
            for ( unsigned i = 0; i < n_elem; i++) {
                if ( data[i] > maxval )
                    maxval = data[i];
            }
        } else {
            maxval = 0;
        }

        return maxval;
    }

    /**
     * @brief min
     * @return minimum value for vector
     */
    eT min() {
        eT minval = data[0];
        if ( n_elem > 0 ) {
            for ( unsigned i = 0; i < n_elem; i++) {
                if ( data[i] < minval )
                    minval = data[i];
            }
        } else  {
            minval = 0;
        }

        return minval;
    }

    inline bool is_empty() {
        if ( n_elem > 0 ) {
            return true;
        } else {
            return false;
        }
    }


    //*> Overloading operators <*//
    /**
     * @brief operator []
     * @param i
     * @return
     */

    eT& operator[](const size_t& i) {
        if (i < n_elem) {
            return data[i];
        } else {
            string err_mes = "Lvec (operator[]) index i=" + to_string(i) + " out of the bounds (from " + to_string(n_elem) + ")!";
            throw runtime_error( err_mes );
        }
    }

    const eT& operator[](const size_t& i) const {
        if (i < n_elem) {
            return data[i];
        } else {
            string err_mes = "Lvec (const operator[]) index i=" + to_string(i) + " out of the bounds (from " + to_string(n_elem) + ")!";
            throw runtime_error( err_mes );
        }
    }

    /**
     * @brief operator ()
     * @param n
     * @param m
     * @return
     */
    eT &operator()(const unsigned& n, const unsigned& m) {

        if ( m > n_maxdeg || n > n_maxdeg ) {
            string err_mes = "Lvec ( operator() ) indexes (n,m) out of the bounds!";
            throw runtime_error( err_mes );
        } else {
            unsigned e;

            if ( columns ) {
                e = (n*n+n+2)/2 +m - 1;
            } else {
                e = (m*(n_maxdeg+1) - m*(m+1)/2 + n);
            }

            return data[e];
        }
    }

    /**
     * @brief operator ()
     * @param n
     * @param m
     * @return
     */
    const eT &operator()(const unsigned& n, const unsigned& m) const {

        if ( m > n_maxdeg || n > n_maxdeg ) {
            string err_mes = "Lvec ( operator() ) indexes (n,m) out of the bounds!";
            throw runtime_error( err_mes );
        } else {
            unsigned e;

            if ( columns ) {
                e = (n*n+n+2)/2 +m - 1;
            } else {
                e = (m*(n_maxdeg+1) - m*(m+1)/2 + n);
            }

            return data[e];
        }
    }

    /**
     * @brief operator =
     * @param nvec
     * @return
     */
    Lvec<eT>& operator=( const Lvec<eT>& nvec ) {
        if (this != &nvec ) {
            this->n_elem = nvec.n_elem;
            this->n_maxdeg = nvec.n_maxdeg;
            this->columns = nvec.columns;

            delete this->data;

            eT* new_colums = new eT[ nvec.n_elem ];
            for (unsigned i =0; i < nvec.n_elem; i++) {
                new_colums[i] = nvec.data[i];
            }

            this->data = new_colums;
        }

        return  *this;
    }

    /**
     * @brief operator + , adds two vector together (element wise)
     * @param nvec - new vector (vector that is added to current one)
     * @return old vector (ovec ) + new vector
     */
    Lvec<eT>& operator+( const Lvec<eT>& nvec ) const
    {
        if (this != &nvec ) {
            unsigned sum_up_n = (  this->n_maxdeg >= nvec.n_maxdeg ) ? nvec.n_maxdeg : this->n_maxdeg ;

            if ( this->columns ) {
                unsigned i = 0;
                for (unsigned n = 0; n <= sum_up_n; n++) {
                    for (unsigned m = 0; m <= n; m++) {
                        this->data[i] += nvec(n,m);
                        i++;
                    }
                }
            } else {
                unsigned i = 0;
                for (unsigned m = 0; m <= sum_up_n; m++) {
                    for (unsigned n = m; n <= sum_up_n; n++ ) {
                        this->data[i] += nvec(n,m);
                        i++;
                    }
                }
            }
        }
        return  *this;
    }

    /**
     * @brief operator +=
     * @param nvec
     * @return
     */
    Lvec<eT>& operator+=( const Lvec<eT>& nvec ) const
    {
        if (this != &nvec ) {
            unsigned sum_up_n = (  this->n_maxdeg >= nvec.n_maxdeg ) ? nvec.n_maxdeg : this->n_maxdeg ;

            if ( this->columns ) {
                unsigned i = 0;
                for (unsigned n = 0; n <= sum_up_n; n++) {
                    for (unsigned m = 0; m <= n; m++) {
                        this->data[i] += nvec(n,m);
                        i++;
                    }
                }
            } else {
                unsigned i = 0;
                for (unsigned m = 0; m <= sum_up_n; m++) {
                    for (unsigned n = m; n <= sum_up_n; n++ ) {
                        this->data[i] += nvec(n,m);
                        i++;
                    }
                }
            }
        }
        return  *this;
    }

    /**
     * @brief operator - , adds two vector together (element wise)
     * @param nvec - new vector (vector that is added to current one)
     * @return old vector (ovec ) + new vector
     */
    Lvec<eT>& operator-( const Lvec<eT>& nvec ) const
    {
        if (this != &nvec ) {
            unsigned sum_up_n = (  this->n_maxdeg >= nvec.n_maxdeg ) ? nvec.n_maxdeg : this->n_maxdeg ;

            if ( this->columns ) {
                unsigned i = 0;
                for (unsigned n = 0; n <= sum_up_n; n++) {
                    for (unsigned m = 0; m <= n; m++) {
                        this->data[i] -= nvec(n,m);
                        i++;
                    }
                }
            } else {
                unsigned i = 0;
                for (unsigned m = 0; m <= sum_up_n; m++) {
                    for (unsigned n = m; n <= sum_up_n; n++ ) {
                        this->data[i] -= nvec(n,m);
                        i++;
                    }
                }
            }
        }
        return  *this;
    }

    /**
     * @brief operator -=
     * @param nvec
     * @return
     */
    Lvec<eT>& operator-=( const Lvec<eT>& nvec ) const
    {
        if (this != &nvec ) {
            unsigned sum_up_n = (  this->n_maxdeg >= nvec.n_maxdeg ) ? nvec.n_maxdeg : this->n_maxdeg ;

            if ( this->columns ) {
                unsigned i = 0;
                for (unsigned n = 0; n <= sum_up_n; n++) {
                    for (unsigned m = 0; m <= n; m++) {
                        this->data[i] -= nvec(n,m);
                        i++;
                    }
                }
            } else {
                unsigned i = 0;
                for (unsigned m = 0; m <= sum_up_n; m++) {
                    for (unsigned n = m; n <= sum_up_n; n++ ) {
                        this->data[i] -= nvec(n,m);
                        i++;
                    }
                }
            }
        }
        return  *this;
    }

    /**
     * @brief operator * , adds two vector together (element wise)
     * @param nvec - new vector (vector that is added to current one)
     * @return old vector (ovec ) + new vector
     */
    Lvec<eT> operator*( const Lvec<eT>& nvec ) const
    {
        Lvec<eT> ovec;

        unsigned sum_up_n = (  ovec.n_maxdeg >= nvec.n_maxdeg ) ? nvec.n_maxdeg : ovec.n_maxdeg ;

        if ( ovec.columns ) {
            unsigned i = 0;
            for (unsigned n = 0; n <= sum_up_n; n++) {
                for (unsigned m = 0; m <= n; m++) {
                    ovec.data[i] *= nvec(n,m);
                    i++;
                }
            }
        } else {
            unsigned i = 0;
            for (unsigned m = 0; m <= sum_up_n; m++) {
                for (unsigned n = m; n <= sum_up_n; n++ ) {
                    ovec.data[i] *= nvec(n,m);
                    i++;
                }
            }
        }

        return ovec;
    }

    void* operator new(size_t sz) {
        void *p = ::new Lvec(sz);

        return p;
    }

    void operator delete (void* p) {
        free(p);
    }

    /**
     * @brief operator <<
     * @param stream
     * @return
     */
    friend ostream& operator <<( ostream& stream, const Lvec<eT>& vec  ){
        if ( vec.columns ) {
            for (unsigned n = 0; n <= vec.n_maxdeg; n++) {
                for (unsigned m = 0; m <=n; m++ ) {
                    stream << n << " " << m << "   " << vec(n,m) << endl;
                }
            }
        } else {
            for ( unsigned m = 0; m <= vec.n_maxdeg; m++ ) {
                for ( unsigned n = m; n <= vec.n_maxdeg; n++ ) {
                    stream << n << " " << m << "   " << vec(n,m) << endl;
                }
            }
        }
        return stream;
    }

    //******************************************************************************
    /**
     * @brief nelem - return the total number of elements in Lvec object. @see n_elem .
     * @return total number of the elements in Lvec object.
     */
    const unsigned& nelem() const {
        return n_elem ;
    }

    /**
     * @brief order_type
     * @return true for  true for f$ [P_{00}, P_{10}, P_{11}, P_{20}, P_{21}, P_{22}, P_{30}, P_{31} , \ldots  ] \f$,
     * false for [P_{00}, P_{10}, P_{20}, P_{30}, \ldots ,  P_{n_{max} 0 }, P_{11}, P_{21}, P_{31} , \ldots ,  P_{n_{max} 1 }, P_{22} , \ldots  ].
     * @see columns .
     */
    const bool& order_type() const {
        return columns;
    }

    /**
     * @brief rev_nm - @see reverse_nm, same function just with bool option
     * @param nelems
     * @param n
     * @param m
     * @param t
     */
    void rev_nm (const unsigned& nelems, unsigned& n, unsigned& m, const bool& t)
    {
        if ( t ) {
            n = (  static_cast<unsigned int>(sqrt( 1 + 8*nelems)) - 1)/2;
            m = nelems - n*(n+1)/2;
        } else {
            m = 0;
            n = 0;

            int spare = static_cast<int>( nelems );
            int max_n = static_cast<int>( n_maxdeg+1 );
            if ( !(nelems == 0) ){
                while ( spare - max_n >= 0) {
                    spare -= max_n--;
                    m++;
                }
                n = nelems + m*(m+1)/2 - m*(n_maxdeg+1);
            }
        }
    }

    /**
     * @brief ridx - @see return_index, same function just with bool option
     * @param n
     * @param m
     * @param t
     */
    unsigned ridx ( const unsigned& n, const unsigned& m, const bool& t )
    {
        unsigned e;

        if ( t ) {
            e = (n*n+n+2)/2 +m - 1;
        } else {
            e = (m*(n_maxdeg+1) - m*(m+1)/2 + n);
        }

        return e;
    }

    const unsigned& nmaxdeg() const {
        return n_maxdeg ;
    }

    const unsigned& nmaxdeg() {
        return n_maxdeg ;
    }

private:
    eT* data = nullptr;     ///< Data stored in one dimensional array
    bool columns; ///< true for \f$ [P_{00}, P_{10}, P_{11}, P_{20}, P_{21}, P_{22}, P_{30}, P_{31} , \ldots  ] \f$
                  ///< false \$ [P_{00}, P_{10}, P_{20}, P_{30}, \ldots ,  P_{n_{max} 0 }, P_{11}, P_{21}, P_{31} , \ldots ,  P_{n_{max} 1 }, P_{22} , \ldots  ] \f$

    unsigned n_maxdeg = 0;///< Maximum degree
    unsigned n_elem   = 0;///< Total number of elements (n+1)*(n+2)/2

};

#endif // LVEC_H
