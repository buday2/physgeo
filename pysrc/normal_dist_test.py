# D'Agostino and Pearson's Test
# for test data generation
from numpy.random import seed
from numpy.random import randn

from scipy.stats import normaltest
from scipy.stats import anderson

"""
Hard Fail
Your data may not be normal for lots of different reasons. Each test looks 
at the question of whether a sample was drawn from a Gaussian distribution 
from a slightly different perspective.

A failure of one normality test means that your data is not normal. 
As simple as that.

You can either investigate why your data is not normal and perhaps use data 
preparation techniques to make the data more normal.

Or you can start looking into the use of nonparametric statistical methods 
instead of the parametric methods.

Soft Fail
If some of the methods suggest that the sample is Gaussian and some not, 
then perhaps take this as an indication that your data is Gaussian-like.

In many situations, you can treat your data as though it is Gaussian 
and proceed with your chosen parametric statistical methods
"""

# normality test
def pearson_test( data, alpha = 0.05 ):
    stat, p = normaltest(data)
    print('Statistics=%.3f, p=%.3f' % (stat, p))
    # interpret
    if p > alpha:
	    print('Sample looks Gaussian (fail to reject H0)')
    else:
	    print('Sample does not look Gaussian (reject H0)')
	        
	    
# normality test
def anderson_test( data ):
    result = anderson(data)
    print('Statistic: %.3f' % result.statistic)
    p = 0
    for i in range(len(result.critical_values)):
	    sl, cv = result.significance_level[i], result.critical_values[i]

        if result.statistic < result.critical_values[i]:
            print('%.3f: %.3f, data looks normal (fail to reject H0)' % (sl, cv))
        else:
            print('%.3f: %.3f, data does not look normal (reject H0)' % (sl, cv))



