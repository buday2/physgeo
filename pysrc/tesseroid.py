# -*- coding: utf-8 -*-

import math
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator

from pysrc import geodetic_functions as geo_f


def plot_tess(fig, r1, b1, l1, r2, b2, l2, tess_colour='b'):
    mean_radius = 6367469.498  # from integral mean value theorem

    bvec = np.linspace(b1, b2, num=10, endpoint=True)
    lvec = np.linspace(l1, l2, num=10, endpoint=True)
    rvec = np.linspace(r1, r2, num=10, endpoint=True)

    b_vec, l_vec = np.meshgrid(bvec, lvec)

    # upper and lower piece
    xup = np.zeros(b_vec.shape)
    yup = np.zeros(b_vec.shape)
    zup = np.zeros(b_vec.shape)
    xlw = np.zeros(b_vec.shape)
    ylw = np.zeros(b_vec.shape)
    zlw = np.zeros(b_vec.shape)

    #left and right
    xlf = np.zeros(b_vec.shape)
    ylf = np.zeros(b_vec.shape)
    zlf = np.zeros(b_vec.shape)
    xrg = np.zeros(b_vec.shape)
    yrg = np.zeros(b_vec.shape)
    zrg = np.zeros(b_vec.shape)

#left and right
    xfr = np.zeros(b_vec.shape)
    yfr = np.zeros(b_vec.shape)
    zfr = np.zeros(b_vec.shape)
    xbc = np.zeros(b_vec.shape)
    ybc = np.zeros(b_vec.shape)
    zbc = np.zeros(b_vec.shape)

    r_vec, b_vec = np.meshgrid(rvec, bvec)
    # plot left and right
    for i in range(b_vec.shape[0]):
        for j in range(b_vec.shape[1]):
            x1, y1, z1 = geo_f.ruv2xyz(r_vec[i, j], b_vec[i, j], l1)
            x2, y2, z2 = geo_f.ruv2xyz(r_vec[i, j], b_vec[i, j], l2)

            xlf[i, j] = x1
            ylf[i, j] = y1
            zlf[i, j] = z1
            xrg[i, j] = x2
            yrg[i, j] = y2
            zrg[i, j] = z2

    b_vec, l_vec = np.meshgrid(bvec, lvec)
    # plot upper and lower layer
    for i in range(b_vec.shape[0]):
        for j in range(b_vec.shape[1]):
            x1, y1, z1 = geo_f.ruv2xyz(r1, b_vec[i, j], l_vec[i, j])
            x2, y2, z2 = geo_f.ruv2xyz(r2, b_vec[i, j], l_vec[i, j])

            xlw[i, j] = x1
            ylw[i, j] = y1
            zlw[i, j] = z1
            xup[i, j] = x2
            yup[i, j] = y2
            zup[i, j] = z2

    r_vec, l_vec = np.meshgrid(rvec, lvec)
    for i in range(b_vec.shape[0]):
        for j in range(b_vec.shape[1]):
            x1, y1, z1 = geo_f.ruv2xyz(r_vec[i, j], b1, l_vec[i, j])
            x2, y2, z2 = geo_f.ruv2xyz(r_vec[i, j], b2, l_vec[i, j])

            xfr[i, j] = x1
            yfr[i, j] = y1
            zfr[i, j] = z1
            xbc[i, j] = x2
            ybc[i, j] = y2
            zbc[i, j] = z2

    ax = fig.gca(projection='3d')
    ax.plot_surface(xlw, ylw, zlw, linewidth=0, color=tess_colour)
    ax.plot_surface(xup, yup, zup, linewidth=0, color=tess_colour)
    ax.plot_surface(xlf, ylf, zlf, linewidth=0, color=tess_colour)
    ax.plot_surface(xrg, yrg, zrg, linewidth=0, color=tess_colour)
    ax.plot_surface(xfr, yfr, zfr, linewidth=0, color=tess_colour)
    ax.plot_surface(xbc, ybc, zbc, linewidth=0, color=tess_colour)


def split_tess(npmat, rrun, brun, lrun, r1, b1, l1, r2, b2, l2, ratio=2.):
    from math import radians, sin, cos, acos, tan

    r0 = .5 * (r1 + r2)
    b0 = radians(.5 * (b1 + b2))
    l0 = radians(.5 * (l1 + l2))

    phi1 = radians(b1)
    lambda1 = radians(l1)
    phi2 = radians(b2)
    lambda2 = radians(l2)

    d_r = abs(r1 - r2)
    d_phi = r2 * acos(sin(phi1) * sin(phi2) + cos(phi1) * cos(phi2))
    d_lambda = r2 * acos(sin(b0) * sin(b0) + cos(b0) *
                         cos(b0) * cos(lambda2 - lambda1))

    s = geo_f.spherical_lenght_radians(
        rrun, radians(brun), radians(lrun), r0, b0, l0)

    if (s < ratio * d_phi) or (s < ratio * d_lambda) or (s < ratio * d_r):
        if (s < ratio * d_r):
            rlist = [r1, .5 * (r1 + r2), r2]
        else:
            rlist = [r1, r2]

        if (s < ratio * d_phi):
            blist = [b1, .5 * (b1 + b2), b2]
        else:
            blist = [b1, b2]

        if (s < ratio * d_lambda):
            llist = [l1, .5 * (l1 + l2), l2]
        else:
            llist = [l1, l2]

        for i in range(len(rlist) - 1):
            for j in range(len(blist) - 1):
                for k in range(len(llist) - 1):
                    npmat = split_tess(npmat,
                                       rrun,
                                       brun,
                                       lrun,
                                       rlist[i],
                                       blist[j],
                                       llist[k],
                                       rlist[i + 1],
                                       blist[j + 1],
                                       llist[k + 1])

        return npmat
    else:
        npmat_row = np.array([[r1, b1, l1, r2, b2, l2]])

        if npmat.shape[0] == 0:
            npmat = np.array(npmat_row)
        else:
            npmat = np.vstack((npmat, npmat_row))

        return npmat