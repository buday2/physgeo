# -*- coding: utf-8 -*-
# Written for python3

import math
from pysrc import geodetic_functions as geo_f
# import numpy as np
from math import pi as M_PI  # because code conversion from C++


# dummy function
def sgn(x: float):
    return math.copysign(1, x)


"""
Brief description
-----------------
There are two different types of Bouguer gravity anomaly that are based
on distinctly different conceptual models: the planar Bouguer anomaly and
the spherical Bouguer anomaly (e.g. Takin & Talwani 1966; Karl 1971; Qureshi
1976; Ervin 1977; LaFehr 1991; Chapin 1996; LaFehr1998; Talwani 1998;
Smith 2001; Van ́ıˇceket al.2001; Novak et al.2001). The planar version uses
an infinitely extending plate of thickness equal to the orthometric height
of the topography at the point of interest to remove the gravitational
effect of the topography, whereas the spherical version uses a spherical
shell of thickness equal to the orthometric height of the topography at
the point of interest. If used alone, these conceptual models yield simple
(spherical and planar) Bouguer anomalies. In order to model the gravitational
attraction of the ‘roughness’of the topography residual to the Bouguer plate
or shell, then ‘terrain corrections’ must be (algebraically) added to produce
the sphericalcomplete Bouguer anomaly and planar complete Bouguer anomaly,
respectively.In this paper, we attempt to answer three recently posed
questions:

    (1)  Is the complete spherical Bouguer gravity anomaly the same
as the ‘standard’ (and more widely used) complete planar Bouguer gravity
anomaly?

    (2)  How can either the complete spherical or complete planar
Bouguer gravity anomaly be continued downward from the surface of theEarth
to the geoid?

    (3)  Is either the complete spherical or the complete planar
Bouguer gravity anomaly harmonic above the geoid?

    In order to answer these and related questions, we have returned to the
very basic principles and definitions of the theory of the Earth’s gravity
field. This led to the need  to introduce a distinction between ‘solid’
(i.e. defined in the 3-D sense) and ‘surface’ (i.e. defined in the 2-D sense)
gravity anomalies. From this it will be demonstrated that the spherical
complete Bouguer anomaly is, as should be expected, very differentfrom
the planar complete Bouguer anomaly. It will also be shown that while
the spherical complete Bouguer anomaly is indeed harmonic (i.e.it satisfies
Laplace’s equation) above the geoid, the planar complete Bouguer anomaly
is not harmonic. Finally, while the spherical complete Bouguer anomaly can be
continued from the surface of the Earth down to the geoid using
Poisson’s approach, the planar complete Bougueranomaly cannot.
"""


class Bouguer:
    """
    The complete Bouguer anomaly is defined as :
        Delta g_{CBA} = g(P) - gamma(P_0)
                             - delta g_F(P)
                             - delta g_{sph}(P) + TC(P)
                             + delta g_{atm}(P) [mGal]

        where :
            g(P) - drift-corrected measurement gravity
            gamma (P_0) - normal gravity field on reference ellipsoid surface
            delta g_F(P) - free-air correction
            delta g_{sph} (P) - gravitational effect of truncated sph layer
            TC(P) - topographic correction (usually rho = 2.67 g/cm^3 )
            delta g_{atm} (P) - atmospheric correction
    """

    def __init__(self):
        """
        Creates a class for Bouguer anomaly computation
        """
        self.br = 166735        # Bullard radius in [m]
        self.r0 = 6367469.498   # mean radius of the Earth, integral mean value theorem
        self.rho = 2670         # mean value of tha mass density
        self.G = 6.6743015e-11  # Newton's gravity constant

    def computeAtmosphericCorrection(self, h_n: float = 0.0) -> float:
        """
        The gravity effect
        of the atmospheric mass is approximated with a model atmo-
        sphere using an analytical expression described by Ecker and
        Mittermayer (1969) and reprinted in Moritz (1980) or calcu-
        lated to the nearest one-hundredth of a milligal up to a height
        of 10 km with the equation (Wenzel, 1985)
        """
        dg_a = 0.874 - 9.9e-05 * h_n + 3.5625e-09 * h_n**2
        # in mGal
        # dg_a = 0.871 - 1.0298e-04 * self.h_n + 5.3105e-09 * self.h_n**2 #
        # mGal

        return dg_a / 10000.


    def gpot_v_limited_plate(self, R, h, z, pcase):
        G_rho_pi = self.rho * self.G * M_PI

        lh = math.sqrt(R * R + (h + z) * (h + z))
        l0 = math.sqrt(R * R + z * z)

        result = 0.0
        if (pcase == 1):
            #  Point is above the bouguer layer \f$ P_1 ( z>0) \f$
            result = (R * R * math.log((lh + h + z) / (z + l0)) -
                      h * h + h * (lh - 2. * z) + z * (lh - l0))
        elif (pcase == 2):
            # Point is at the top edge of the layer \f$ P_2 ( z=0) \f$
            result = (
                R *
                R *
                math.log(
                    (math.sqrt(
                        h *
                        h +
                        R *
                        R) +
                        h) /
                    (R)) -
                h *
                h +
                h *
                math.sqrt(
                    h *
                    h +
                    R *
                    R))
        elif (pcase == 3):
            # Point is inside the layer \f$ P_3 ( 0<z<-h) \f$
            result = (R * R * math.log((lh + h + z) / (z + l0)) -
                      h * h + h * (lh - 2. * z) - 2. * z * z + z * (lh - l0))
        elif (pcase == 4):
            result = R * R * \
                math.log(R / (math.sqrt(h * h + R * R) - h) + h * h + h * math.sqrt(h * h + R * R))
            # Point is at the lower edge of the Bouguer layer \f$ P_4 ( z=-h)
            # \f$
        elif (pcase == 5):
            # Point is under the layer  \f$ P_5 ( z<-h) \f$
            result = (R * R * math.log((lh + h + z) / (z + l0)) +
                      h * h + h * (lh + 2. * z) + z * (lh - l0))
        else:
            # error
            errmsg = "Bouguer::gpot_v_limited_plate() invalid pcase option.\n"
            raise RuntimeError(errmsg)

        return G_rho_pi * result

    def gpot_v_unlimited_plate(self, h: float, z: float, pcase: int) -> float:
        return float('Inf')

    def gravity_r_unlimited_plate(
            self,
            h: float,
            z: float,
            pcase: int) -> float:
        G_rho_pi = 2. * self.G * self.rho * M_PI
        result = 0.0
        if (pcase == 1):
            #  Point is above the bouguer layer \f$ P_1 ( z>0) \f$
            result = -h
        elif (pcase == 2):
            # Point is at the top edge of the layer \f$ P_2 ( z=0) \f$
            result = -h
        elif (pcase == 3):
            # Point is inside the layer \f$ P_3 ( 0<z<-h) \f$
            result = -(h + 2. * z)
        elif (pcase == 4):
            result = h
            # Point is at the lower edge of the Bouguer layer
            # \f$ P_4 ( z=-h) \f$
        elif (pcase == 5):
            # Point is under the layer  \f$ P_5 ( z<-h) \f$
            result = h
        else:
            # error
            errmsg = "Bouguer::gravity_r_unlimited_plate() invalid pcase option.\n"
            raise RuntimeError(errmsg)

        return G_rho_pi * result

    def marussi_rr_unlimited_plate(
            self,
            h: float,
            z: float,
            pcase: int) -> float:
        G_rho_pi = self.rho * self.G * M_PI
        result = 0.0
        if (pcase == 1):
            #  Point is above the bouguer layer
            # \f$ P_1 ( z>0) \f$
            result = 0.0
        elif (pcase == 2):
            # Point is at the top edge of the layer
            # \f$ P_2 ( z=0) \f$
            result = h / 0.
        elif (pcase == 3):
            # Point is inside the layer \f$ P_3 ( 0<z<-h) \f$
            result = -4.
        elif (pcase == 4):
            result = z / 0.
            # Point is at the lower edge of the Bouguer layer
            # \f$ P_4 ( z=-h) \f$
        elif (pcase == 5):
            # Point is under the layer  \f$ P_5 ( z<-h) \f$
            result = 0.0
        else:
            # error
            errmsg = "Bouguer::marussi_rr_inlimited_plate() invalid pcase option.\n"
            raise RuntimeError(errmsg)

        return G_rho_pi * result

    def gravity_r_limited_plate(
            self,
            R: float,
            h: float,
            z: float,
            pcase: int) -> float:
        G_rho_pi = -2. * self.rho * self.G * M_PI

        result = 0.0
        if (pcase == 1):
            #  Point is above the bouguer layer \f$ P_1 ( z>0) \f$
            result = -math.sqrt(h * h + 2. * h * z + R *
                                R + z * z) + h + math.sqrt(R * R + h * h)
        elif (pcase == 2):
            # Point is at the top edge of the layer \f$ P_2 ( z=0) \f$
            result = -math.sqrt(h * h + R * R) + h + R
        elif (pcase == 3):
            # Point is inside the layer \f$ P_3 ( 0<z<-h) \f$
            result = -math.sqrt(h * h + 2. * h * z + R * R +
                                z * z) + h + math.sqrt(R * R + h * h) + 2. * z
        elif (pcase == 4):
            result = math.sqrt(h * h + R * R) - h - R
            # Point is at the lower edge of the Bouguer layer
            # \f$ P_4 ( z=-h) \f$
        elif (pcase == 5):
            # Point is under the layer  \f$ P_5 ( z<-h) \f$
            result = -math.sqrt(h * h + 2. * h * z + R *
                                R + z * z) - h + math.sqrt(R * R + z * z)
        else:
            # error
            errmsg = "Bouguer::gpot_v_plate() invalid pcase option.\n"
            raise RuntimeError(errmsg)

        return G_rho_pi * result

    def marussi_rr_limited_plate(
            self,
            R: float,
            h: float,
            z: float,
            pcase: int) -> float:
        G_rho_pi = 2. * self.rho * self.G * M_PI

        result = 0.0
        if (pcase == 1):
            # Point is above the bouguer layer
            # \f$ P_1 ( z>0) \f$
            result = (h + z) / math.sqrt((h + z) * (h + z) +
                                         R * R) - z / math.sqrt(R * R + z * z)
        elif (pcase == 2):
            # Point is at the top edge of the layer
            # \f$ P_2 ( z=0) \f$
            result = h / math.sqrt(R * R + h * h)
        elif (pcase == 3):
            # Point is inside the layer
            # \f$ P_3 ( 0<z<-h) \f$
            result = (h + z) / math.sqrt((h + z) * (h + z) + \
                      R * R) - z / math.sqrt(R * R + z * z) - 2.
        elif (pcase == 4):
            result = h / math.sqrt(R * R + h * h)
            # Point is at the lower edge of the Bouguer layer
            # \f$ P_4 ( z=-h) \f$
        elif (pcase == 5):
            # Point is under the layer  \f$ P_5 ( z<-h) \f$
            result = (h + z) / math.sqrt((h + z) * (h + z) +
                                         R * R) - z / math.sqrt(R * R + z * z)
        else:
            # error
            errmsg = "Bouguer::marussi_rr_limited_plate() invalid pcase option.\n"
            raise RuntimeError(errmsg)

        return G_rho_pi * result

    """
    // r1 = R -sphere radius
    // r2 = R+h ->( height + sphrad )
    // position of the point P , so for z=h r = R+h
    // intradius - integration radius
    """

    def gpot_v_limited_shell(
            self,
            r1: float,
            r2: float,
            r: float,
            intradius: float) -> float:
        G_rho_pi = 2. * self.rho * self.G * M_PI
        r_mean = .5 * (r1 + r2)
        psi0 = intradius / r_mean  # geocentric angle in radians

        l1 = math.sqrt(r * r + r1 * r1 - 2. * r * r1 * math.cos(psi0))
        l2 = math.sqrt(r * r + r2 * r2 - 2. * r * r2 * math.cos(psi0))

        result = ((r1 - r) * abs(r - r1) * (r + 2. * r1) + (r - r2) * abs(r - r2) * (r + 2. * r2)) / (6. * r) \
            + 0.5 * r * r * math.sin(psi0) * math.sin(psi0) * math.cos(psi0) \
            * math.log((r2 + l2 - r * math.cos(psi0)) / (r1 + l1 - r * math.cos(psi0))) \
            + (l2 * l2 * l2 - l1 * l1 * l1) / (3. * r) \
            + .5 * math.cos(psi0) * (l2 * (r2 - r * math.cos(psi0)) - l1 * (r1 - r * math.cos(psi0)))

        return G_rho_pi * result

    def gpot_v_unlimited_shell(self, r1: float, r2: float, r: float) -> float:
        # // position of the point P , so for z=h r = R+h
        G_rho_pi = M_PI * self.G * self.rho / (3. * r)

        return G_rho_pi * ((r * r + r2 * r - 2. * r2 * r2) * abs(r - r2)
                           - (r * r + r1 * r - 2. * r1 * r1) * abs(r - r1)
                           - 2. * r1 * r1 * r1 - 3. * r * r1 * r1 + 2. * r2 * r2 * r2 + 3. * r * r2 * r2)

    def gravity_r_limited_shell(
            self,
            r1: float,
            r2: float,
            r: float,
            intradius: float) -> float:
        G_rho_pi = self.rho * self.G * M_PI / 3.
        r_mean = .5 * (r1 + r2)
        psi0 = intradius / r_mean  # geocentric angle in radians

        l1 = math.sqrt(r * r + r1 * r1 - 2. * r * r1 * math.cos(psi0))
        l2 = math.sqrt(r * r + r2 * r2 - 2. * r * r2 * math.cos(psi0))

        result = ((r * r + r2 * r - 2. * r2 * r2) * sgn(r - r2) - (r * r + r1 * r - 2. * r1 * r1) * sgn(r - r1)) / r \
            + (abs(r - r2) * (r * r + 2. * r2 * r2) - abs(r - r1) * (r * r + 2. * r1 * r1)) / (r * r) \
            + 3. * r * r * math.sin(psi0) * math.sin(psi0) * math.cos(psi0) \
            * (((r1 + l1) * math.cos(psi0) - r) / (l1 * (-r * math.cos(psi0) + r1 + l1))
               - ((r2 + l2) * math.cos(psi0) - r) / (l2 * (-r * math.cos(psi0) + r2 + l2))
               ) \
            + 3. * math.cos(psi0) \
            * ((r * math.cos(psi0) - r1) * (r - r1 * math.cos(psi0)) / l1
               - (r * math.cos(psi0) - r2) * (r - r2 * math.cos(psi0)) / l2
               ) \
            + 3. * math.cos(psi0) * math.cos(psi0) * (l1 - l2) \
            + 6. * (l1 * (r1 * math.cos(psi0) - r) + l2 * (r - r2 * math.cos(psi0))) / r \
            + 2. * (l1 * l1 * l1 - l2 * l2 * l2) / (r * r) \
            + 6. * r * math.sin(psi0) * math.sin(psi0) * math.cos(psi0) \
            * math.log((-r * math.cos(psi0) + r2 + l2) / (-r * math.cos(psi0) + r1 + l1))
        return G_rho_pi * result

    def gravity_r_unlimited_shell(
            self,
            r1: float,
            r2: float,
            r: float) -> float:
        G_rho_pi = M_PI * self.G * self.rho / (3. * r * r)

        return G_rho_pi * ((r * r + r2 * r - 2. * r2 * r2) * sgn(r - r2) * r
                           - (r * r + r1 * r - 2. * r1 * r1) * sgn(r - r1) * r
                           + (r * r + 2. * r2 * r2) * abs(r - r2)
                           - (r * r + 2. * r1 * r1) * abs(r - r1) - 2. * (r2 * r2 * r2 - r1 * r1 * r1))

    def marussi_rr_unlimited_shell(
            self,
            r1: float,
            r2: float,
            r: float) -> float:
        G_rho_pi = -2. * M_PI * self.G * self.rho / (3. * r * r * r)

        return G_rho_pi * (
            -2. * r1 * r1 * abs(r - r1) + 2. * r2 * r2 * abs(r - r2)
            + 3. * r * r * r * (sgn(r - r1) - sgn(r - r2))
            + 2. * r1 * r1 * r * sgn(r - r1) - 2. * r2 * r2 * r * sgn(r - r2)
            + 2. * r1 * r1 * r1 - 2. * r2 * r2 * r2)

    def set_new_Newton(self, n_G: float):
        self.G = n_G

    def set_new_density(self, n_rho: float):
        self.rho = n_rho
