from __future__ import division
import sys
import os
from mpl_toolkits.basemap import Basemap
from scipy.signal import convolve2d
import pylab
import scipy.ndimage as ndimage
from scipy.interpolate import griddata
from scipy.interpolate import interpn
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
from pysrc import geodetic_functions as geo_f
from pysrc.plot_isg import Isgem_model, Terrain, Gravity, Gravity2, Grayscale, Grayscale2

from pysrc.normal_dist_test import pearson_test, anderson_test


""" NUMPY LIB """
import numpy as np
import math

""" MY LIB """


""" MATPLOTLIB """

""" PYTHON3 """

""" BASEMAP PLOT """


def calculate_connection(
        blz_mat: np.array,
        ell=geo_f.wgs84_ell(),
        *args,
        **kwargs):
    """
    eq :
    \Delta \zeta = \mu +  a ( \phi_i - \phi_0) + b ( \lambda_i - \lambda_0) \cos \psi_i

    unknown \mu, a, b
    """
    if ("b0" in kwargs):
        b0 = kwargs.get("b0")
    else:
        b0 = np.mean(blz_mat[:, 0])

    if ("l0" in kwargs):
        l0 = kwargs.get("l0")
    else:
        l0 = np.mean(blz_mat[:, 1])

    if ("degree" in kwargs):
        degree = kwargs.get("degree")
    else:
        degree = 1

    protbool = False
    if ("protocol" in kwargs):
        protbool = True
        path2protocol = kwargs.get("protocol")

    nameconnection = ""
    if ("name" in kwargs):
        nameconnection = kwargs.get("name")

    """ END OF KWARGS """
    nr, nc = blz_mat.shape
    n_rad = ell.get_n_radius(b0)
    m_rad = ell.get_m_radius(b0)

#    delta_bi = np.radians(blz_mat[:,0] - b0 ) * m_rad
#    delta_li = np.radians(blz_mat[:,1] - l0) * math.cos( math.radians( b0 )) * n_rad

    delta_bi = np.radians(blz_mat[:, 0] - b0)
    delta_li = np.radians(blz_mat[:, 1] - l0) * math.cos(math.radians(b0))

    z = blz_mat[:, 2]
    zi = z
    Amat = np.zeros((nr, 3))

    Amat[:, 0] = 1
    Amat[:, 1] = delta_bi
    Amat[:, 2] = delta_li

    iter = int(0)
    Pmat = np.eye(nr, nr)

    nshape = nr  # initial value
    nnshape = nr + 1  # iteration

    dx = np.array((0, 0))

    dx_i = np.array((0, 0))

    while nshape != nnshape:
        nshape = Amat.shape[0]
        n_inv = np.linalg.inv(np.dot(Amat.T, Amat))
        dx = np.dot(n_inv, np.dot(Amat.T, zi))

        mu = dx[0]
        a = dx[1]
        b = dx[2]

        v = zi - (mu + a * delta_bi + b * delta_li)
        vmean = np.mean(v)
        m0 = np.sqrt(np.dot(v.T, v) / (nr - 3))

        idx = np.where(np.absolute(v) <= 3. * m0)

        new_r = len(idx[0])

        Amat = np.zeros((new_r, 3))

        delta_bi = delta_bi[idx]
        delta_li = delta_li[idx]
        zi = zi[idx]

        delta_bi = np.squeeze(delta_bi.T)
        delta_li = np.squeeze(delta_li.T)
        zi = np.squeeze(zi.T)

        Amat[:, 0] = 1
        Amat[:, 1] = delta_bi
        Amat[:, 2] = delta_li

        nnshape = Amat.shape[0]

        iter += 1
        if (iter == 10):
            break

    covMatrix = n_inv * m0**2
    wmat, vmat = np.linalg.eig(covMatrix)
    # final statistics
    if protbool:  # if True protocol will be created
        fprot = open(path2protocol, "a+")
        fprot.write(nameconnection + ", ")
        fprot.write(f'{blz_mat.shape[0]:6d} ,')  # total number points
        # Number of points rejected
        fprot.write(f'{blz_mat.shape[0] - nnshape:6d} ,')
        fprot.write(f'{dx[0]:2.5f} ,')  # Datum shift
        fprot.write(f'{math.sqrt(n_inv[0,0]) * m0:2.5f} ,')  # Datum shift STD
        fprot.write(f'{dx[1]:2.6f} ,')  # Datum slope
        fprot.write(f'{math.sqrt(n_inv[1,1]) * m0:2.6f} ,')  # Datum slope STD
        fprot.write(f'{dx[2]:2.5f} ,')  # Datum slope
        fprot.write(f'{math.sqrt(n_inv[2,2]) * m0:2.5f} ,')  # Datum slope STD
        fprot.write(f'{zi.max():2.5f} ,')  # Maximum shift
        fprot.write(f'{zi.min():2.5f} ,')  # min shift
        fprot.write(f'{np.mean(zi):2.5f} ,')  # zi_average
        fprot.write(f'{np.median(zi):2.5f} ,')  # zi_media
        fprot.write(f'{np.quantile(zi, .25):2.5f} ,')  # q25
        fprot.write(f'{np.quantile(zi, .75):2.5f} ,')  # q75
        fprot.write(f'{m0:2.5f} ,')  # m0
        fprot.write(f'{np.mean(v):2.5f} ,')  # v_mean
        fprot.write(f'{v.max():2.5f} ,')  # vmax
        fprot.write(f'{v.min():2.5f} ,')  # vmin
        fprot.write(f'{np.median(v):2.5f} ,')  # v median
        fprot.write(f'{b0:2.5f} ,')  # b0
        fprot.write(f'{l0:2.5f} ,')  # l0
        fprot.write(f'{n_rad:7.4f} ,')  # nrad
        fprot.write(f'{m_rad:7.4f} ,')  # mrad
        fprot.write("\n")
    else:
        print("----------------------------------------------------------")
        print("Final statistics for FAST GBVP approach: ")
        print("Total number points : ", blz_mat.shape[0])
        print("Number of points rejected : ", blz_mat.shape[0] - nnshape)
        print("Datum shift :  ", dx[0], "  +- ", math.sqrt(n_inv[0, 0]) * m0)
        print("tilt a :       ", dx[1], "  +- ", math.sqrt(n_inv[1, 1]) * m0)
        print("tilt b :       ", dx[2], "  +- ", math.sqrt(n_inv[2, 2]) * m0)
        print("Maximum shift: ", zi.max())
        print("Minimum shift: ", zi.min())
        print("zi_average   : ", np.mean(zi))
        print("zi_median    : ", np.median(zi))
        print("zi_q25       : ", np.quantile(zi, .25))
        print("zi_q75       : ", np.quantile(zi, .75))
        print("m0 :           ", m0)
        print("v_mean :       ", np.mean(v))
        print("v_max  :       ", v.max())
        print("v_min :        ", v.min())
        print("v_median :     ", np.median(v))
        print("(b0,l0)       (", b0, " , ", l0, ")")
        print("Covariance matrix:")
        print(covMatrix)
        print("Eigen values and eigen vectors:")
        print(wmat)
        print(vmat)
        print("Correlations:")

        nr, nc = covMatrix.shape
        corMatrix = np.zeros((nr,nc)) *m0*m0
        for i in range(0,nr):
            for j in range(0,nc):
                if i == j:
                    continue
                else:
                    r_xy = covMatrix[i,j]/math.sqrt( covMatrix[i,i] * covMatrix[j,j])
                    corMatrix[i,j] = r_xy

        print(corMatrix)

        print(" normal distribution test ")
        pearson_test(v,0.95)
        anderson_test(v)
        print("----------------------------------------------------------")

    bdeg = np.degrees(delta_bi) + b0
    ldeg = np.degrees(delta_li) / np.cos(np.radians(bdeg)) + l0
    filtered_data = np.block([[bdeg], [ldeg], [zi], [v]]).T

    # [\mu , a , b] \mu is average offset, a tilt in latitude direction, b is tilt in longitude direciton
    return dx, filtered_data


def plot_residuals(
    b: np.array,
    l: np.array,
    z: np.array,
    boundaries: np.array,
    *args,
        **kwargs):
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close()  # Close a figure window
    SAVE = False
    if ("save2file" in kwargs):
        SAVE = True
        path2save = kwargs.get("save2file")

    if ("format" in kwargs):
        fmat = kwargs.get("format")
    else:
        fmat = "png"

    if ("dpi" in kwargs):
        imagedpi = kwargs.get("dpi")
    else:
        imagedpi = None

    if ("orientation" in kwargs):
        i_orientation = kwargs.get("orientation")
    else:
        i_orientation = 'portrait'

    if ("papertype" in kwargs):
        papertyp = kwargs.get("papertype")
    else:
        papertyp = None

    b_min = boundaries[0]
    b_max = boundaries[1]
    l_min = boundaries[2]
    l_max = boundaries[3]

    b0 = .5 * (b_min + b_max)
    l0 = .5 * (l_min + l_max)

    stepphi = 0.5
    steplam = 0.5

    m = Basemap(
        llcrnrlon=l_min,
        llcrnrlat=b_min,
        urcrnrlon=l_max,
        urcrnrlat=b_max,
        resolution='i',
        projection='tmerc',
        lon_0=l0,
        lat_0=b0)

    parallels = m.drawparallels(
        np.arange(
            b_min,
            b_max,
            stepphi),
        labels=[
            1,
            0,
            0,
            0],
        fontsize=15)
    meridians = m.drawmeridians(
        np.arange(
            l_min,
            l_max,
            steplam),
        labels=[
            0,
            0,
            1,
            0],
        fontsize=15,
        rotation=90)
    m.drawcountries(
        linewidth=2.50,
        linestyle='solid',
        color='k',
        antialiased=1,
        ax=None,
        zorder=None)
    #m.drawcoastlines(linewidth=2.50, linestyle='solid', color='k', antialiased=1)
    m.shadedrelief()

    poschange = np.where(z >= 0)
    negchange = np.where(z < 0)

    xi, yi = m(l, b)
    zerosi = np.zeros_like(z)
    SCALE = None  # 1/0.1

    m.scatter(xi, yi, marker='.', color='r')
    QV1 = m.quiver(
        xi[poschange],
        yi[poschange],
        zerosi[poschange],
        z[poschange],
        scale=SCALE,
        units='width',
        color="r")  # positive change
    QV2 = m.quiver(
        xi[negchange],
        yi[negchange],
        zerosi[negchange],
        z[negchange],
        scale=SCALE,
        units='width',
        color="b")  # negative change

    plt.quiverkey(QV1, 0, 0, 1, '10 cm', coordinates='data')
    #plt.quiverkey(QV2, 1.2, 0.520, 1/SCALE, 'arrow 2', coordinates='data')
    plt.legend()

    if SAVE:
        plt.savefig(path2save, dpi=imagedpi, orientation=i_orientation,
                    papertype=papertyp, format=fmat)
    else:
        plt.show()


# -------------------------------------------------------------------------------
def plot_contour(b: np.array,
                 l: np.array,
                 z: np.array,
                 *args,
                 ** kwargs):
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure

    if ("boundaries" in kwargs):
        bound_vec = kwargs.get("boundaries")
        if (len(bound_vec) == 4):  # without the steplam adn stepphi
            b_min = bound_vec[0]
            b_max = bound_vec[1]
            l_min = bound_vec[2]
            l_max = bound_vec[3]
            stepphi = 0.5
            steplam = 0.5

        elif (len(bound_vec) == 6):  # with the steplam adn stepphi
            b_min = bound_vec[0]
            b_max = bound_vec[1]
            l_min = bound_vec[2]
            l_max = bound_vec[3]
            stepphi = bound_vec[4]
            steplam = bound_vec[5]
        else:
            raise RuntimeError(
                "plot_contour() incorrect number of args in boundaries[].\n")
    else:
        b_min = b.min()
        b_max = b.max()
        l_min = l.min()
        l_max = l.max()

        db = abs(b_max - b_min)
        dl = abs(l_max - l_min)

        b_min -= db * .075
        b_max += db * .075
        l_min -= dl * .075
        l_max += dl * .075

        stepphi = 0.5
        steplam = 0.5

    b0 = .5 * (b_min + b_max)
    l0 = .5 * (l_min + l_max)

    extend = False
    if ("extend_area" in kwargs):
        extend = True
        ext_area = kwargs.get("extend_area")

        if (len(ext_area) == 4):
            blow = ext_area[0]
            bup = ext_area[1]
            lwest = ext_area[2]
            least = ext_area[3]
        else:
            blow, bup, lwest, least = 0.0, 0.0, 0.0, 0.0

    h_range_manual = False
    if ("h_range" in kwargs):
        h_range_manual = True
        hRangeVec = kwargs.get("h_range")
        if (len(hRangeVec) == 3):
            h_min = hRangeVec[0]
            h_max = hRangeVec[1]
            h_step = hRangeVec[2]
        else:
            print(
                "incorrect number of arguments in  option \"h_range\" , 3 are needed!\n")

            h_min = z.min()
            h_max = z.max()
            h_step = abs(h_max - h_min) / 100.
    else:
        h_min = z.min()
        h_max = z.max()
        h_step = abs(h_max - h_min) / 100.

    if ("alliasing" in kwargs):
        aliasing = kwargs.get("alliasing")
    else:
        aliasing = 0

    imodel = False
    if ("om_bound" in kwargs):
        imodel = True
        omBoundVec = kwargs.get("om_bound")

        if (omBoundVec.size != 0):
            imodel = True
            bm_min = omBoundVec[0]
            bm_max = omBoundVec[1]
            lm_min = omBoundVec[2]
            lm_max = omBoundVec[3]

            if (len(omBoundVec) == 5):
                dphi = omBoundVec[4]
                dlam = omBoundVec[4]
            elif (len(omBoundVec) == 6):
                dphi = omBoundVec[4]
                dlam = omBoundVec[5]
            else:
                dphi, dlam = 0.02, 0.02
    else:
        imodel = False
    ### ------------ ###
    #  map projection  #
    ### ------------ ###
    m = Basemap(
        llcrnrlon=l_min,
        llcrnrlat=b_min,
        urcrnrlon=l_max,
        urcrnrlat=b_max,
        resolution='i',
        projection='tmerc',
        lon_0=l0,
        lat_0=b0)

    dx_l, dy_l, dx_k, dy_k = 0.0, 0.0, 0.0, 0.0
    if (extend):  # extend data
        dx_l, dy_l = m(l0 - lwest, b0 - blow)
        dx_k, dy_k = m(l0 + least, b0 - bup)
    else:
        dx_l, dy_l, dx_k, dy_k = 0.0, 0.0, 0.0, 0.0

    parallels = m.drawparallels(
        np.arange(
            b_min,
            b_max,
            stepphi),
        labels=[
            1,
            0,
            0,
            0],
        fontsize=15)
    meridians = m.drawmeridians(
        np.arange(
            l_min,
            l_max,
            steplam),
        labels=[
            0,
            0,
            1,
            0],
        fontsize=15,
        rotation=90)
    m.drawcountries(
        linewidth=2.50,
        linestyle='solid',
        color='k',
        antialiased=1,
        ax=None,
        zorder=None)
    #m.drawcoastlines(linewidth=2.50, linestyle='solid', color='k', antialiased=1)

    xp, yp = m(l, b)
    if (imodel):
        n_phi = int(math.ceil(abs(bm_max - bm_min) / dphi) + 1)
        n_lam = int(math.ceil(abs(lm_max - lm_min) / dlam) + 1)
        phi_vec = np.linspace(bm_min, bm_max, num=n_phi, endpoint=True)
        lam_vec = np.linspace(lm_min, lm_max, num=n_lam, endpoint=True)

        phi_grid, lam_grid = np.meshgrid(phi_vec, lam_vec)
        xi_grid, yi_grid = m(lam_grid, phi_grid)

    # draw countours
    # --------------
    # creating grid
    numcols, numrows = 2200, 1400

    xi = np.linspace(xp.min() - dx_l, xp.max() + dx_k, numcols)
    yi = np.linspace(yp.min() - dy_l, yp.max() + dy_k, numrows)
    xg, yg = np.meshgrid(xi, yi)

    hi = griddata((xp, yp), z, (xg, yg), method="nearest")

    print(xp.shape, yp.shape, z.shape, xg.shape, yg.shape)
    hi = ndimage.gaussian_filter(hi, sigma=aliasing, order=0)
    interp_points = np.block([[xp], [yp]]).T

    cbar_step = h_step
    cbar_min = h_min
    cbar_max = h_max
    cbar_range = cbar_max - cbar_min

    clevels = []
    temp = cbar_min
    while temp <= cbar_max:
        clevels.append(temp)
        temp = temp + cbar_step

    cflevels = []
    temp = cbar_min
    while temp <= cbar_max:
        cflevels.append(temp)
        temp = temp + cbar_step * 5

    #print( clevels )
    matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
    cs1 = m.contour(
        xg,
        yg,
        hi,
        6,
        colors='k',
        levels=cflevels,
        linestyle='solid',
        linewidth=1.)
    plt.clabel(cs1, fontsize=13, inline=1, fmt="%1.2f",
               inline_spacing=0)  # - contours labels in figure
    # Colourfull hypsometry
    cs2 = m.contourf(xg, yg, hi, levels=clevels, cmap=plt.cm.jet)
    # add colorbar
    cbar = m.colorbar(cs2, location='bottom', pad="10%")
    cbar.ax.set_xticklabels(cbar.ax.get_xticklabels(), rotation='vertical')

    for t in cbar.ax.get_xticklabels():
        t.set_fontsize(16)

    cbar.set_label("m", usetex='False', fontsize=18, weight='bold')

    print("gbvp::plot_contour() plt.show() suspended!")
    # plt.show()

    if (imodel):
        hi_model = griddata(
            (xp, yp), z, (xi_grid.T, yi_grid.T), method="nearest")
        hi_model = ndimage.gaussian_filter(hi_model, sigma=2, order=0)
        # hi_model = griddata((xi, yi), hi.T, (xi_grid.T, yi_grid.T),  method="nearest"))
        #m.scatter(xg , yg , marker='.',color='r')
        r, c = hi_model.shape
        mod = Isgem_model()
        mod.model_name = "Unknown"
        mod.model_type = "Unknown"
        mod.units = "Unknown"
        mod.ref_ell = "Unknown"
        mod.lat_min = phi_vec.min()
        mod.lat_max = phi_vec.max()
        mod.lon_min = lam_vec.min()
        mod.lon_max = lam_vec.max()
        mod.dlat = abs(mod.lat_min - mod.lat_max) / float(r)
        mod.dlon = abs(mod.lon_min - mod.lon_max) / float(c)
        mod.nrows = r
        mod.ncols = c
        mod.nodata = -9999.999
        mod.isg_format = "Unknown"
        mod.data = hi_model

        return mod
    else:
        return Isgem_model()


def split10percent(blz_mat: np.array):
    nr, nc = blz_mat.shape

    mat90 = np.empty((0, nc))
    mat10 = np.empty((0, nc))

    print(mat90.shape, mat10.shape)
    for i in range(0, nr):
        if (i % 10 == 0):
            mat10 = np.append(mat10, [blz_mat[i, :]], axis=0)
        else:
            mat90 = np.append(mat90, [blz_mat[i, :]], axis=0)

    print("mat10 shape : ", mat10.shape)
    print("mat90 shape : ", mat90.shape)
    print("blz_mat shape:", blz_mat.shape)

    return mat10, mat90


def moving_average_2d(data, window):
    """Moving average on two-dimensional data.
    """
    # Makes sure that the window function is normalized.
    window /= window.sum()
    # Makes sure data array is a numpy array or masked array.
    if type(data).__name__ not in ['ndarray', 'MaskedArray']:
        data = np.asarray(data)
    # The output array has the same dimensions as the input data
    # (mode='same') and symmetrical boundary conditions are assumed
    # (boundary='symm').
    return convolve2d(data, window, mode='same', boundary='symm')

# extrapolate NaN


def extrapolate_nans(x, y, v):
    if np.ma.is_masked(v):
        nans = v.mask
    else:
        nans = np.isnan(v)
    notnans = np.logical_not(nans)
    v[nans] = scipy.interpolate.griddata((x[notnans], y[notnans]), v[notnans],
                                         (x[nans], y[nans]), method='nearest').ravel()
    return v


def plot_histogram(v: np.array, units="cm", *args, **kwargs):
    vmax = v.max()
    vmin = v.min()

    CONSTANT = 100
    if (units == "cm"):
        CONSTANT = 100
    elif (units == "mm"):
        CONSTANT = 1000
    elif (units == "dm"):
        CONSTANT = 10
    else:
        CONSTANT = 100  # cm is default

    if ("CONSTANT" in kwargs):
        CONSTANT = kwargs.get("CONSTANT")

    SAVE = False
    if ("save2file" in kwargs):
        SAVE = True
        path2save = kwargs.get("save2file")

    if ("format" in kwargs):
        fmat = kwargs.get("format")
    else:
        fmat = "png"

    if ("dpi" in kwargs):
        imagedpi = kwargs.get("dpi")
    else:
        imagedpi = None

    if ("orientation" in kwargs):
        i_orientation = kwargs.get("orientation")
    else:
        i_orientation = 'portrait'

    if ("papertype" in kwargs):
        papertyp = kwargs.get("papertype")
    else:
        papertyp = None

    vmax_ceil = math.ceil(vmax * CONSTANT)
    vmin_floor = math.floor(vmin * CONSTANT)

    #n_bins = int(  abs(vmax_ceil - vmin_floor) + 1 )
    num_bins = int(abs(vmax_ceil - vmin_floor) + 1)
    n_bins = np.linspace(
        vmin_floor,
        vmax_ceil,
        num=num_bins,
        endpoint=True) / CONSTANT

    ### Histogram ###
    plt.clf()
    # N is the count in each bin, bins is the lower-limit of the bin
    N, bins, patches = plt.hist(
        v, bins=n_bins, edgecolor='black', facecolor='gray', histtype="stepfilled")
    #plt.tick_params(axis='both', which='major', direction='out', labelsize=13)
    plt.xticks(n_bins)
    plt.xticks(rotation=90)
    plt.grid(color='k', linestyle=':', linewidth=1)

    if SAVE:
        plt.savefig(path2save, dpi=imagedpi, orientation=i_orientation,
                    papertype=papertyp, format=fmat)
    else:
        plt.show()


def remove_outliers(data: np.array, *args, **kwargs):
    zetaGNSS = data[:, 2] - data[:, 3]  # GNSS levelling
    zeta = data[:, 6]  # NEWZETA

    dz = zetaGNSS - zeta

    dz_mean = np.mean(dz)
    dz_std = np.std(dz)

    idx = np.where(np.absolute(dz_mean - dz) <= 3. * dz_std)

    odata = data[idx][:]
    return odata
