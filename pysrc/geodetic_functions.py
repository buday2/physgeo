# -*- coding: utf-8 -*-

from math import sin, cos, radians, sqrt, acos, degrees, atan, pow, sinh, asinh, asin, acosh, atan2, cosh, tan
from math import pi as PI
import numpy as np
import inspect


class wgs84_ell:
    def __init__(self):
        self.a_axis = 6378137.000          # [m] WGS84 semi-major axis
        self.b_axis = 6356752.31424518  # [m] WGS84 semi-minor axis
        self.eccentricity = 0.0818191908426215**2  # [-] WGS84 eccentricity
        self.f = 1. / 298.257223563  # [-] WGS84 flatenning
        # second zonal coefficient [unitless] - not normalized
        self.j20 = 108263.0e-8
        self.omega = 7292115.0e-11  # speed of Earth's rotation [ rad s^-1]
        self.gm = 3986004.4180e+8  # geocentric gravity constant [ m^3 s^-2 ]
        self.g_e = 9.78032677  # normal gravity acceleration on eqautor [ms^-2]
        # normal gravity acceleration on poles [ -||- ]7ĵx
        self.g_p = 9.83218637
        # mean value normal gravity (from mean value theorem)
        self.g_m = 9.806224173
        self.c20 = -0.484166774985e-03  # Fully normalized J_20 of WGS84 ellipsoid
        self.m = (self.omega * self.a_axis)**2 * self.b_axis / self.gm

        E = sqrt(self.a_axis * self.a_axis - self.b_axis * self.b_axis)
        self.U0 = self.gm / E * \
            atan(E / self.b_axis) + self.a_axis * self.a_axis * self.omega * self.omega / 3.

    def update2GRS80(self):
        self.a_axis = 6378137.000          # [m] WGS84 semi-major axis
        self.b_axis = 6356752.3141  # [m] WGS84 semi-minor axis
        self.eccentricity = 0.00669438002290  # [-] WGS84 eccentricity
        self.f = 0.00335281068118  # [-] WGS84 flatenning
        # second zonal coefficient [unitless] - not normalized
        self.j20 = 108263.0e-8
        self.omega = 7292115.0e-11  # speed of Earth's rotation [ rad s^-1]
        self.gm = 3986005.0e+8  # geocentric gravity constant [ m^3 s^-2 ]
        # normal gravity acceleration on eqautor [ms^-2]
        self.g_e = 9.7803267715
        # normal gravity acceleration on poles [ -||- ]7ĵx
        self.g_p = 9.8321863685
        # mean value normal gravity (from mean value theorem)
        self.g_m = .5 * (self.g_e + self.g_p)
        self.c20 = -0.484166774985e-03  # Fully normalized J_20 of WGS84 ellipsoid
        self.m = (self.omega * self.a_axis)**2 * self.b_axis / self.gm

        E = sqrt(self.a_axis * self.a_axis - self.b_axis * self.b_axis)
        self.U0 = self.gm / E * \
            atan(E / self.b_axis) + self.a_axis * self.a_axis * self.omega * self.omega / 3.

    def update2Bessel(self):
        self.a_axis = 6377397.1550          # [m] WGS84 semi-major axis
        self.b_axis = 6356078.9633  # [m] WGS84 semi-minor axis
        self.eccentricity = (6377397.1550**2 - 6356078.9633**2) / \
            6377397.1550**2  # [-] WGS84 eccentricity
        self.f = 0.00335281068118  # [-] WGS84 flatenning
        # second zonal coefficient [unitless] - not normalized
        self.j20 = 108263.0e-8
        self.omega = 7292115.0e-11  # speed of Earth's rotation [ rad s^-1]
        self.gm = 3986005.0e+8  # geocentric gravity constant [ m^3 s^-2 ]
        # normal gravity acceleration on eqautor [ms^-2]
        self.g_e = 9.7803267715
        # normal gravity acceleration on poles [ -||- ]7ĵx
        self.g_p = 9.8321863685
        # mean value normal gravity (from mean value theorem)
        self.g_m = .5 * (self.g_e + self.g_p)
        self.c20 = -0.484166774985e-03  # Fully normalized J_20 of WGS84 ellipsoid
        self.m = (self.omega * self.a_axis)**2 * self.b_axis / self.gm

        E = sqrt(self.a_axis * self.a_axis - self.b_axis * self.b_axis)
        self.U0 = self.gm / E * \
            atan(E / self.b_axis) + self.a_axis * self.a_axis * self.omega * self.omega / 3.

    def __str__(self):
        show_str = "a_axis [m]       := " + str(self.a_axis) + "\n" \
            + "b_axis [m]       := " + str(self.b_axis) + "\n" \
            + "e^2 [unitless]   := " + str(self.eccentricity) + "\n" \
            + "f [unitless]     := " + str(self.f) + "\n" \
            + "J_20 [unitless]  := " + str(self.j20) + "\n" \
            + "omega [rad s^-1] := " + str(self.omega) + "\n" \
            + "GM [ m^3 s^-2 ]  := " + str(self.gm) + "\n" \
            + "g_e [m s^-2]     := " + str(self.g_e) + "\n" \
            + "g_p [m s^-2]     := " + str(self.g_p) + "\n" \
            + "g_m [m s^-2]     := " + str(self.g_m) + "\n" \
            + "c20 [unitless]   := " + str(self.c20) + "\n" \
            + "m [unitless]     := " + str(self.m) + "\n" \
            + "U0 [m^2 s^-2]    := " + str(self.U0) + "\n" \

        return show_str

    def mean_normal_gravity(self, phi: float, h: float) -> float:
        gamma0 = self.get_normal_gravity(phi)

        b = radians(phi)
        return gamma0 * (1 - h / self.a_axis * (1. + self.f + self.m -
                                                2. * self.f * sin(b) * sin(b)) + h**2 / self.a_axis**2)

    def get_normal_gravity(self, phi: float) -> float:
        k = (self.b_axis * self.g_p) / (self.a_axis * self.g_e) - 1.

        brad = radians(phi)
        g_n = self.g_e * ((1 + k * sin(brad) * sin(brad)) /
                          sqrt(1. - self.eccentricity * sin(brad) * sin(brad)))

        return g_n

    def normal_gravity(self, phi: float, hell: float) -> float:
        gamma_0 = self.get_normal_gravity(phi)

        gamma = gamma_0 * (1. - (2. - (self.b_axis / self.a_axis) + pow(self.omega * self.a_axis, 2.) * self.b_axis / self.gm
                                 - 2. * ((self.a_axis - self.b_axis) / self.a_axis) * pow(sin(radians(phi)), 2.0))
                           * hell / self.a_axis + pow(hell / self.a_axis, 2.0)
                           )

        return gamma

    def derivative_gamma_h(self, phi: float, hell: float) -> float:
        gamma_0 = self.get_normal_gravity(phi)

        gamma = gamma_0 * (1. - (2. - (self.b_axis / self.a_axis) + pow(self.omega * self.a_axis,
                                                                        2.) * self.b_axis / self.gm - 2. * ((self.a_axis - self.b_axis) / self.a_axis) * pow(sin(radians(phi)),
                                                                                                                                                             2.0)) * 1. / self.a_axis + 2. * hell * pow(1. / self.a_axis,
                                                                                                                                                                                                        2.0))

        return gamma

    def get_U0(self) -> float:
        ee = self.eccentricity
        return (self.gm / (sqrt(ee) * self.a_axis)) * atan(sqrt(ee) / (1.0 - self.f)
                                                           ) + (1.0 / 3.0) * self.omega * self.omega * self.a_axis * self.a_axis

    def get_n_radius(self, phi: float) -> float:
        return self.a_axis / \
            sqrt(1. - self.eccentricity * (sin(radians(phi)))**2)

    def get_m_radius(self, phi: float) -> float:
        return (self.a_axis * (1.0 - self.eccentricity)) / pow(1.0 -
                                                               self.eccentricity * pow(sin(radians(phi)), 2.0), 3.0 / 2.0)

    def get_mean_radius(self, phi: float) -> float:
        m = self.get_m_radius(phi)
        n = self.get_n_radius(phi)

        return sqrt(m * n)

    def normal_potential(self, u: float, w: float):
        urad = radians(u)
        P20 = (3. * cos(urad) * cos(urad) - 1.) / 2.
        E = self.a_axis * sqrt(self.eccentricity)

        acotsinhw = (PI / 2. - atan(sinh(w)))

        m = (3. * self.b_axis**2 / E**2 + 1) * \
            atan(E / self.b_axis) - 3. * self.b_axis / E
        wa = self.omega * self.omega * self.a_axis * self.a_axis / 3.

        W1 = self.gm / E * acotsinhw
        W2 = wa / m * (((3. * sinh(w) * sinh(w) + 1) *
                        acotsinhw - 3. * sinh(w))) * P20
        W3 = self.omega * self.omega * E**2 * \
            sin(urad) * sin(urad) * cosh(w) * cosh(w) / 2.

        return W1 + W2 + W3

    def normal_gravity_uw(self, u: float, w: float):
        """
        /* \gamma(N) = \gamma(H_q,GM,W_0,\omega.J2)
         * derivative of function U(u,w) in the direction of the normal (opposite
         * direction as gradient )
         */
        """
        u = radians(u)

        """ reading ellipsoid parameters """
        a = self.a_axis
        ee = self.eccentricity
        _omega_ = self.omega
        _gm_ = self.gm

        __e = sqrt(a * a - a * a * (1.0 - ee))  # square of linear eccentricity

        h_u = a * sqrt(ee) * sqrt(cosh(w) * cosh(w) -
                                  sin(u) * sin(u))    # lame coeff
        h_w = h_u                                               # lame coeff
        # lame coeff
        h_v = a * sqrt(ee) * sin(u) * cosh(w)

        P20 = (3.0 * pow(cos(u), 2.0) - 1.0) / 2.0         # legen. poly 20

        m = ((3. - 2. * ee) / ee) \
            * atan(sqrt(ee / (1. - ee))) - 3. * sqrt((1.0 - ee) / ee)

        du_w = -1.0 * _gm_ / (__e * cosh(w)) + pow(_omega_ * a, 2.0) / (3.0 * m) \
            * (6 * sinh(w) * cosh(w) * (PI / 2.0 - atan(sinh(w))) - (6.0 * sinh(w) * sinh(w) + 4.0)
               / cosh(w)) * P20 + 2.0 / 3.0 * _omega_ * _omega_ * __e * __e * sinh(w) * cosh(w) * (1.0 - P20)
        du_u = pow(_omega_ * __e * cosh(w), 2.0) * sin(u) * cos(u) - pow(_omega_ * a, 2.0) / (2.0 * m) \
            * ((3.0 * sinh(w) * sinh(w) + 1.0) * (PI / 2.0 - atan(sinh(w))) - 3.0 * sinh(w)) \
            * sin(2 * u)

        gamma = du_w / h_w + .5 * pow(du_u / h_u, 2.0) / (du_w / h_w)
        return gamma

    def mean_gravity_uw(self, u: float, w: float, inum=200):
        E = self.a_axis * sqrt(self.eccentricity)
        w0 = acosh(self.a_axis / E)

        w_vec = np.linspace(w0, w, num=inum, endpoint=True)

        gamma0 = self.normal_gravity_uw(u, w0)
        gamma100 = self.normal_gravity_uw(u, w)

        dw = 0.0
        gamma_n = 0.0
        for i in range(0, inum):
            #dw += w_vec[i] - w_vec[i-1]
            gamma_n += self.normal_gravity_uw(u, w_vec[i])

        return gamma_n / float(inum)

    def np_mean_gravity_uw(self, u: np.array, w: np.array, inum=200):
        mean_gamma_vec = np.empty_like(u)

        for i in range(0, len(u)):
            mean_gamma_vec[i] = abs(self.mean_gravity_uw(u[i], w[i], inum))

        return mean_gamma_vec

    def free_air_ellipsoid(self, phi: float, h_el: float) -> float:
        """
        https://geodesy.noaa.gov/GRAV-D/data/NGS_GRAV-D_General_Airborne_Gravity_Data_User_Manual_v2.0.pdf
        Compute the free-air effect using for gravity disturbance

        h_el - ellipsoidal height
        phi - geodetic latitude

        f = \frac{a-b}{b}

        g_{FAC} = \frac{\partial \gamma}{\partial h}
                = \frac{2 \gamma_e}{a}
                    \left(
                    1 + f + \frac{\omega^2 a^2 b}{GM}
                      -2f \sin \varphi^2
                    \right) h_{el}

                  - \frac{3 \gamma_e}{a^2} h^2_{el}
        """
        sinus_phi2 = pow(sin(radians(phi)), 2.)

        # First order term
        c1 = 2. * self.g_e / self.a_axis * (1. + self.f +
                                            self.a_axis**2 * self.b_axis * self.omega**2 / self.gm
                                            - 2. * self.f * sinus_phi2)
        # Second degree term
        c2 = 3. * self.g_e / pow(self.a_axis, 2.0)

        #print(c1, c2 ,  c1*h_el,c2*h_el*h_el )
        return c1 * h_el - c2 * h_el * h_el


class level_ell:
    def __init__(self, a0: float,
                 gm: float,
                 w0: float,
                 omega: float,
                 j_20: float,
                 name="Unknown",
                 tide_system="Unknown"):
        """
        /**
         * @brief compute_level_ell
         * @param a0 - a major axis of refer. ell of geo model
         * @param gm - geocentric gravitational constant
         * @param w0 - geopotential value on geoid
         * @param omega - angular speed of the Earth's rotation
         * @param j_20 - second stokes zonal coefficient fully normalized
         * @param name - name of the ellipsoid
         * @param tide_system - needed because second zonal stokec coefficient
         * @return
         */
        """
        a = gm / w0  # a0 = 6378136.3; -> for egm2008
        alpha0 = 1.0 / 298.25231  # IAGSC3 1995
        j_20 = j_20 * sqrt(5.0)

        q = pow(omega, 2.) * pow(a0, 3.) / gm
        ee = (2.0 * alpha0 - alpha0 * alpha0)

        iter = int(0)
        while True:
            alpha = .5 * alpha0 * alpha0 - 1.5 * (a0 * a0 / (a * a)) * j_20 + 2.0 / 15.0 * q * (a * a * a / (a0 * a0 * a0)) \
                * pow(ee, 3.0 / 2.0) / ((3.0 - 2.0 * ee) / ee * atan(sqrt(ee / (1.0 - ee)))
                                        - 3.0 * sqrt((1.0 - ee) / ee))
            ee = (2.0 * alpha - alpha * alpha)
            a1 = (gm / w0) * ((1.0 / sqrt(ee)) * atan(sqrt(ee) / (1.0 - \
                  alpha)) + (1.0 / 3.0) * pow(omega, 2.0) * pow(a, 3.0) / gm)

            da = a - a1
            dalpha = alpha - alpha0
            alpha0 = alpha
            a = a1

            # evaluate the conditions
            if (abs(da) <= 1.0e-6):
                break
            elif iter == 20:
                print(
                    "class level_ell() : Maximum number of allowed iterations exceeded !!\nCheck your inputs !!")
                break

            iter += 1

        e_prime = sqrt(ee / (1.0 - ee))
        q = (3.0 / pow(e_prime, 2.0) + 1.0) * atan(e_prime) - 3. / e_prime
        g_p = gm / (a * a * (sqrt(1.0 - ee))) - (2. / 3.) * omega * omega * \
            a * (1.0 - e_prime / q * (1.0 - 1. / e_prime * atan(e_prime)))
        g_e = gm / (a * a) - (2. / 3.) * omega * omega * a * (sqrt(1. - ee) +
                                                              (4. / 3.) * sqrt(ee) / q * (1.0 - 1. / e_prime * atan(e_prime)))

        self.name = name
        self.tide_system = tide_system
        self.a_axis = a
        self.f = alpha
        self.b_axis = a * sqrt(1.0 - ee)
        self.gm = gm
        self.omega = omega
        self.j20 = j_20 / sqrt(5.0)  # fully normalized
        self.w0 = w0
        self.eccentricity = ee
        self.g_e = g_e
        self.g_p = g_p
        self.q = omega * omega * a * a * a / gm

        print("Number of iterations needed : ", iter)

    def compute_normal_potential(self, u: np.array, w: np.array):
        if u.shape != w.shape:
            raise RuntimeError(
                "level_ell::compute_normal_potential different size of the inputs!")

        urad = np.radians(u)

        P20 = (3. * np.multiply(np.cos(urad), np.cos(urad)) - 1.) / 2.
        U1 = (3.0 * np.multiply(np.sinh(w), np.sinh(w)) + 1.0) * \
            (PI / 2.0 - np.arctan(np.sinh(w))) - 3.0 * np.sinh(w)
        U2 = (((3.0 - 2.0 * self.eccentricity) / self.eccentricity) * atan(sqrt(self.eccentricity / \
              (1.0 - self.eccentricity))) - 3.0 * sqrt((1.0 - self.eccentricity) / self.eccentricity))
        U3 = 1.0 / 3.0 * self.q * \
            pow(sqrt(self.eccentricity), 3.0) * np.multiply(np.cosh(w), np.cosh(w)) * (1.0 - P20)

        U = self.gm / (sqrt(self.eccentricity) * self.a_axis) \
            * (PI / 2.0 - np.arctan(np.sinh(w)) + (1.0 / 3.0) * sqrt(self.eccentricity)
               * self.q * np.multiply((U1 / U2), P20) + U3)

        return U

    def normal_potential(self, u: float, w: float):
        urad = radians(u)
        P20 = (3. * cos(urad) * cos(urad) - 1.) / 2.
        E = self.a_axis * sqrt(self.eccentricity)

        acotsinhw = (PI / 2. - atan(sinh(w)))

        m = (3. * self.b_axis**2 / E**2 + 1) * \
            atan(E / self.b_axis) - 3. * self.b_axis / E
        wa = self.omega * self.omega * self.a_axis * self.a_axis / 3.

        W1 = self.gm / E * acotsinhw
        W2 = wa / m * (((3. * sinh(w) * sinh(w) + 1) *
                        acotsinhw - 3. * sinh(w))) * P20
        W3 = self.omega * self.omega * E**2 * \
            sin(urad) * sin(urad) * cosh(w) * cosh(w) / 2.

        return W1 + W2 + W3

    def compute_normal_gravity(self, u: np.array, w: np.array):
        """
        /* \gamma(N) = \gamma(H_q,GM,W_0,\omega.J2)
         * derivative of function U(u,w) in the direction of the normal (opposite
         * direction as gradient )
         */
        """
        u = np.radians(u)

        # reading ellipsoid parameters
        a = self.a_axis
        ee = self.eccentricity
        _omega_ = self.omega
        _gm_ = self.gm

        __e = sqrt(a * a - a * a * (1. - ee))  # square of linear eccentricity

        h_u = a * sqrt(ee) * np.sqrt(np.cosh(w) * np.cosh(w) -
                                     np.sin(u) * np.sin(u))  # lame coeff
        # lame coeff
        h_w = h_u
        # lame coeff
        h_v = a * sqrt(ee) * np.sin(u) * np.cosh(w)

        P20 = (3. * np.multiply(np.cos(u), np.cos(u)) - 1.) / \
            2.    # legen. poly 20
        m = ((3. - 2. * ee) / ee) \
            * atan(sqrt(ee / (1. - ee))) - 3. * sqrt((1.0 - ee) / ee)

        du_w = -1.0 * _gm_ / (__e * np.cosh(w)) + pow(_omega_ * a, 2.0) / (3.0 * m) \
            * (6. * np.sinh(w) * np.cosh(w) * (PI / 2.0 - np.arctan(np.sinh(w))) - (6.0 * np.sinh(w) * np.sinh(w) + 4.0)
               / np.cosh(w)) * P20 + 2.0 / 3.0 * _omega_ * _omega_ * __e * __e * np.sinh(w) * np.cosh(w) * (1.0 - P20)
        du_u = np.power(_omega_ * __e * np.cosh(w), 2.0) * np.sin(u) * np.cos(u) - pow(_omega_ * a, 2.0) / (2.0 * m) \
            * ((3.0 * np.sinh(w) * np.sinh(w) + 1.0) * (PI / 2.0 - np.arctan(np.sinh(w))) - 3.0 * np.sinh(w)) \
            * np.sin(2 * u)

        gamma = np.divide(
            du_w, h_w) + .5 * np.divide(
            np.power(
                np.divide(
                    du_u, h_u), 2.0), (np.divide(
                        du_w, h_w)))

        return gamma

    def normal_gravity_uw(self, u: float, w: float):
        """
        /* \gamma(N) = \gamma(H_q,GM,W_0,\omega.J2)
         * derivative of function U(u,w) in the direction of the normal (opposite
         * direction as gradient )
         */
        """
        u = radians(u)

        """ reading ellipsoid parameters """
        a = self.a_axis
        ee = self.eccentricity
        _omega_ = self.omega
        _gm_ = self.gm

        __e = sqrt(a * a - a * a * (1.0 - ee))  # square of linear eccentricity

        h_u = a * sqrt(ee) * sqrt(cosh(w) * cosh(w) -
                                  sin(u) * sin(u))    # lame coeff
        h_w = h_u                                               # lame coeff
        # lame coeff
        h_v = a * sqrt(ee) * sin(u) * cosh(w)

        P20 = (3.0 * pow(cos(u), 2.0) - 1.0) / 2.0         # legen. poly 20

        m = ((3. - 2. * ee) / ee) \
            * atan(sqrt(ee / (1. - ee))) - 3. * sqrt((1.0 - ee) / ee)

        du_w = -1.0 * _gm_ / (__e * cosh(w)) + pow(_omega_ * a, 2.0) / (3.0 * m) \
            * (6 * sinh(w) * cosh(w) * (PI / 2.0 - atan(sinh(w))) - (6.0 * sinh(w) * sinh(w) + 4.0)
               / cosh(w)) * P20 + 2.0 / 3.0 * _omega_ * _omega_ * __e * __e * sinh(w) * cosh(w) * (1.0 - P20)
        du_u = pow(_omega_ * __e * cosh(w), 2.0) * sin(u) * cos(u) - pow(_omega_ * a, 2.0) / (2.0 * m) \
            * ((3.0 * sinh(w) * sinh(w) + 1.0) * (PI / 2.0 - atan(sinh(w))) - 3.0 * sinh(w)) \
            * sin(2 * u)

        gamma = du_w / h_w + .5 * pow(du_u / h_u, 2.0) / (du_w / h_w)
        return gamma

    def mean_gravity_uw(self, u: float, w: float, inum=200):
        E = self.a_axis * sqrt(self.eccentricity)
        w0 = acosh(self.a_axis / E)

        w_vec = np.linspace(w0, w, num=inum, endpoint=True)

        gamma0 = self.normal_gravity_uw(u, w0)
        gamma100 = self.normal_gravity_uw(u, w)

        dw = 0.0
        gamma_n = 0.0
        for i in range(0, inum):
            #dw += w_vec[i] - w_vec[i-1]
            gamma_n += self.normal_gravity_uw(u, w_vec[i])

        return gamma_n / float(inum)

    def np_mean_gravity_uw(self, u: np.array, w: np.array, inum=200):
        mean_gamma_vec = np.empty_like(u)

        for i in range(0, len(u)):
            #print ( i , type(u[i]) , type(w[i]))
            mean_gamma_vec[i] = abs(self.mean_gravity_uw(u[i], w[i], inum))

        return mean_gamma_vec

    def __str__(self):
        return "name              := " + self.name + "\n" \
            + "tide_system       := " + self.tide_system + "\n" \
              + "a_axis [m]        := " + str(self.a_axis) + "\n" \
              + "b_axis [m]        := " + str(self.b_axis) + "\n" \
              + "1/f [unitless]    := " + str(1. / self.f) + "\n" \
              + "gm [ m**3 s**-2]  := " + str(self.gm) + "\n" \
              + "omega [rad s**-1] := " + str(self.omega) + "\n" \
              + "j20 [unitless]    := " + str(self.j20) + "\n" \
              + "w0 [m**2 s**-2]   := " + str(self.w0) + "\n" \
              + "g_e [m**2 s**-2]  := " + str(self.g_e) + "\n" \
              + "g_p [m**2 s**-2]  := " + str(self.g_p) + "\n"


def blh2xyz(phi: float, lam: float, hel: float, ell: wgs84_ell):
    from math import pi as PI

    a_axis = ell.a_axis
    eccentricity = ell.eccentricity

    b = radians(phi)
    l = radians(lam)

    n = a_axis / sqrt(1 - eccentricity * sin(b) * sin(b))

    x = (n + hel) * cos(b) * cos(l)
    y = (n + hel) * cos(b) * sin(l)
    z = (n * (1 - eccentricity) + hel) * sin(b)
    return x, y, z


def np_blh2xyz(phi, lam, hel, ell):
    from math import pi as PI

    a_axis = ell.a_axis
    eccentricity = ell.eccentricity

    b = np.radians(phi)
    l = np.radians(lam)

    n = a_axis * \
        np.reciprocal(np.sqrt(1 - eccentricity * np.multiply(np.sin(b), np.sin(b))))

    x = np.multiply((n + hel), np.multiply(np.cos(b), np.cos(l)))
    y = np.multiply((n + hel), np.multiply(np.cos(b), np.sin(l)))
    z = np.multiply((n * (1 - eccentricity) + hel), np.sin(b))
    return x, y, z


def blh2ruv(phi: float, lam: float, hel: float, ell):
    from math import pi as PI
    x, y, z = blh2xyz(phi, lam, hel, ell)

    r = sqrt(x**2 + y**2 + z**2)
    u = degrees(atan(z / sqrt(x**2 + y**2)))
    v = lam

    return r, u, v

def np_uvw2xyz(u: np.array, v: np.array, w: np.array, ell):
    urad = np.radians(u)
    vrad = np.radians(v)

    ae = ell.a_axis * sqrt(ell.eccentricity)
    x = ae * np.sin(urad) * np.cos(vrad) * np.cosh(w)
    y = ae * np.sin(urad) * np.sin(vrad) * np.cosh(w)
    z = ae * np.cos(urad) * np.sinh(w)

    return x, y, z


def uvw2xyz(u: float, v: float, w: float, ell):
    urad = np.radians(u)
    vrad = np.radians(v)

    ae = ell.a_axis * sqrt(ell.eccentricity)
    x = ae * sin(urad) * cos(vrad) * cosh(w)
    y = ae * sin(urad) * sin(vrad) * cosh(w)
    z = ae * cos(urad) * sinh(w)

    return x, y, z


def np_blh2ruv(phi, lam, hel, ell):
    from math import pi as PI

    x, y, z = np_blh2xyz(phi, lam, hel, ell)

    a_axis = ell.a_axis
    eccentricity = ell.eccentricity

    b = np.radians(phi)
    l = np.radians(lam)

    p_mat = np.sqrt(np.multiply(x, x) + np.multiply(y, y))
    z_divby_p_mat = np.divide(z, p_mat)

    r = np.sqrt(np.multiply(x, x) + np.multiply(y, y) + np.multiply(z, z))
    u = (180. / PI) * np.arctan(z_divby_p_mat)
    v = lam

    return r, u, v


def ruv2xyz(r: float, u: float, v: float):
    phi = radians(u)
    lam = radians(v)

    x = r * cos(phi) * cos(lam)
    y = r * cos(phi) * sin(lam)
    z = r * sin(phi)

    return x, y, z


def xyz2blh(X: float, Y: float, Z: float, ell: wgs84_ell):
    from math import pi as PI

    a = ell.a_axis
    ee = ell.eccentricity

    L = atan2(Y, X)
    if (L < 0):
        L += 2.0 * PI

    # Iterative method of computation
    p = sqrt(X * X + Y * Y)
    B = atan(Z / (p * (1.0 - ee)))
    NN = a / sqrt(1.0 - ee * pow(sin(B), 2.0))
    H = p / cos(B) - NN

    dB = 10
    dH = 10
    i = 0
    while ((dB > 1e-12 and dH > 1e-6) or i != 50):
        Bi = atan((Z * (NN + H)) / (p * (H + NN * (1.0 - ee))))
        Ni = a / sqrt(1.0 - ee * pow(sin(Bi), 2.0))
        Hi = p / cos(Bi) - Ni

        dB = abs(B - Bi)
        dH = abs(H - Hi)
        B = Bi
        H = Hi
        NN = Ni
        i += 1

    return degrees(B), degrees(L), H


def xyz2uvw(X: float, Y: float, Z: float, ell):
    """
    * X = a e \sin u \cos v \cosh w
    * Y = a e \sin u \sin v \cosh w
    * Z = a e \cos u \sinh w

    ell = level ellipsoid
    """
    a = ell.a_axis
    b = ell.b_axis
    ee = ell.eccentricity
    e = sqrt(ee)
    v = atan2(Y, X)
    p = sqrt(X * X + Y * Y)
    r = sqrt(X * X + Y * Y + Z * Z)

    dd1 = 1.0 / (2.0 * pow(a * e, 2.0)) \
        * (pow(a * e, 2.0) + r * r + sqrt(pow(pow(a * e, 2.0) + r * r, 2.0)
                                          - 4.0 * pow(a * e, 2.0) * p * p))

    dd2 = 1.0 / (2.0 * pow(a * e, 2.0)) \
        * (pow(a * e, 2.0) + r * r - sqrt(pow(pow(a * e, 2.0) + r * r, 2.0)
                                          - 4.0 * pow(a * e, 2.0) * p * p))

    if ((abs(dd1) > 1.0) and (dd2 == 1.0)):
        dd = dd2 - 1.0e-10
    elif ((abs(dd1) > 1.0) and (dd2 != 1.0)):
        dd = dd2
    elif((dd1 < 1.0) and (dd2 < 0.0)):
        dd = dd1
    else:
        dd = dd1

    f = Z / (a * e * sqrt(1.0 - dd))
    if (Z > 0.0):
        w = asinh(f)
        u = asin(sqrt(dd))
    elif (Z < 0.0):
        u = PI - asin(sqrt(dd))
        w = asinh(-1.0 * f)
    else:
        u = asin(sqrt(dd - 1.0e-10))
        w = acosh(1.0 / e)

    return degrees(u), degrees(v), w


def np_xyz2uvw(x: np.array, y: np.array, z: np.array, ell):
    u_vec = np.empty_like(x)
    v_vec = np.empty_like(x)
    w_vec = np.empty_like(x)

    for i in range(0, u_vec.size):
        u, v, w = xyz2uvw(x[i], y[i], z[i], ell)
        u_vec[i] = u
        v_vec[i] = v
        w_vec[i] = w

    return u_vec, v_vec, w_vec


def d3_length(x1, y1, z1, x2, y2, z2):
    return sqrt((x1 - x2)**2 + (y1 - y2)**2 + (z1 - z2)**2)


def d3_sph_len(r, phi1, lambda1, phi2, lambda2):
    b1 = radians(phi1)
    l1 = radians(lambda1)
    b2 = radians(phi2)
    l2 = radians(lambda2)
    return (r * acos(sin(b1) * sin(b2) + cos(b1) * cos(b2) * cos(l2 - l1)))


def spherical_lenght(r1, phi1, lambda1, r2, phi2, lambda2):
    phi1 = radians(phi1)
    lambda1 = radians(lambda1)
    phi2 = radians(phi2)
    lambda2 = radians(lambda2)
    return sqrt(r1 * r1 + r2 * r2 - 2. * r1 * r2 * (sin(phi1) *
                                                    sin(phi2) + cos(phi1) * cos(phi2) * cos(lambda2 - lambda1)))


def spherical_lenght_radians(r1, phi1, lambda1, r2, phi2, lambda2):
    return sqrt(r1 * r1 + r2 * r2 - 2. * r1 * r2 * (sin(phi1) *
                                                    sin(phi2) + cos(phi1) * cos(phi2) * cos(lambda2 - lambda1)))


def normal_ell_gravity(phi, ell):
    # based on Somigliana equation:
    # normal gravity on ellipsoid surface
    # phi in deg format
    from math import pi as PI

    phi *= PI / 180.0
    k = (sqrt(1.0 - ell.eccentricity) * ell.g_p) / ell.g_e - 1.0
    gamma = ell.g_e * (1.0 + k * sin(phi) * sin(phi))
    gamma /= sqrt(1.0 - ell.eccentricity * sin(phi) * sin(phi))
    return gamma


def np_normal_ell_gravity(phi, ell):
    from math import pi as PI

    if phi.shape[0] == 0:  # not inspect.isclass(ell) :
        print("In function \"normal_ell_gravity\" a variable \"ell\" must be class!")
        return 0.
    else:
        phi_vec = phi * (PI / 180.0)
        k = (sqrt(1.0 - ell.eccentricity) * ell.g_p) / ell.g_e - 1.0
        gamma_nom = ell.g_e * (1.0 + k * np.sin(phi_vec) * np.sin(phi_vec))
        gamma_denom = np.sqrt(
            1.0 -
            ell.eccentricity *
            np.sin(phi_vec) *
            np.sin(phi_vec))

        return np.divide(gamma_nom, gamma_denom)


def compute_av_normal_gravity(phi, h_ell, ell):
    from math import pi as PI

    gamma = normal_ell_gravity(phi, ell)
    phi *= PI / 180.0
    a_axis = ell.a_axis
    b_axis = a_axis * sqrt(1. - ell.eccentricity)

    g = gamma * (1. - (2. - (b_axis / a_axis) + (ell.omega * a_axis)**2. * b_axis / ell.gm
                       - 2. * ((a_axis - b_axis) / a_axis) * (sin(phi))**2.0)
                 * h_ell / a_axis + (h_ell / a_axis)**2.0)
    return (g)


def np_compute_av_normal_gravity(phi, h_ell, ell):
    from math import pi as PI

    phi_vec = phi * PI / 180.0
    a_axis = ell.a_axis
    b_axis = a_axis * sqrt(1. - ell.eccentricity)

    gamma_ell = np_normal_ell_gravity(phi, ell)
    a_axis = ell.a_axis
    b_axis = a_axis * sqrt(1. - ell.eccentricity)

    gamma_upward = (1. -
                    (2. -
                     (b_axis /
                      a_axis) +
                        (ell.omega *
                         a_axis)**2. *
                        b_axis /
                        ell.gm -
                        2. *
                        ((a_axis -
                          b_axis) /
                         a_axis)) *
                    np.multiply((np.multiply(np.sin(phi_vec), np.sin(phi_vec))), h_ell) /
                    a_axis +
                    np.multiply(h_ell, h_ell) /
                    (a_axis)**2.0)

    g = np.multiply(gamma_ell, gamma_upward)
    return (g)


def get_zeta(phi: float, hel: float, tpot: float, ell) -> float:
    iter = int(0)

    zeta0 = tpot / ell.get_normal_gravity(phi)
    dzeta = 999.999

    while abs(dzeta) > 0.0001:
        gamma = ell.normal_gravity(phi, hel - zeta0)

        zeta1 = tpot / gamma
        dzeta = zeta1 - zeta0
        zeta0 = zeta1

        if (iter == 10):
            break

        iter += 1

    return zeta0  # , iter


def tide_system_transf(
        c_20: float,
        radius: float,
        k: float,
        _from: str,
        _to: str):
    """
    tide sytems: "zero tide", "mean tide, "tide free"
    c_20 is input from ggm
    k - love number (0.3 for EGM96, .29525 for EGM2008
    radius - reference radius of the gravity field model
    !! simplified version of algorithm
    based on mean value theorem and WGS84 ellipsoid

    mean gravity = 9.806224173 from wgs84 ellipsoid
    mean radius  = 6.36754098217714754616796613117603325027137630257097929e+06
    a_axis = 6378137.000
    GM = 3986005.0e+8
    """

    #m = (Any) -1.382520964080156800e-08;
    #m = (Any) -1.390366816361558784e-08;
    # m= \frac{-1}{r \sqrt{4 \pi} \cdot .}
    m = (1.0 / (radius * sqrt(4. * PI)) * (-0.31455))

    if ((_from.lower() == "zero tide" or _from.lower() == "zero_tide") and
            (_to.lower() == "mean tide" or _to.lower() == "mean_tide")):
        return c_20 + m
    elif ((_from.lower() == "zero tide" or _from.lower() == "zero_tide") and
          (_to.lower() == "tide free" or _to.lower() == "tide_free")):
        return c_20 - k * m
    elif ((_from.lower() == "mean tide" or _from.lower() == "mean_tide") and
          (_to.lower() == "zero tide" or _to.lower() == "zero_tide")):
        return c_20 - m
    elif ((_from.lower() == "mean tide" or _from.lower() == "mean_tide") and
          (_to.lower() == "tide free" or _to.lower() == "tide_free")):
        return c_20 - (1.0 + k) * m
    elif ((_from.lower() == "tide free" or _from.lower() == "tide_free") and
          (_to.lower() == "zero tide" or _to.lower() == "zero_tide")):
        return c_20 + k * m
    elif ((_from.lower() == "tide free" or _from.lower() == "tide_free") and
          (_to.lower() == "mean tide" or _to.lower() == "mean_tide")):
        return c_20 - (1.0 + k) * m
    else:
        return c_20


def h_tide_system_transf(
        bdeg: float,
        k: float,
        h: float,
        _from: str,
        _to: str,
        ell):

    # reduction of the geodetic latitude
    psi = atan(tan(radians(bdeg)) / (1. - ell.eccentricity))
    deltaW_ZT = -0.198 * (3. / 2. * sin(psi) * sin(psi) - .5)

    if ((_from.lower() == "zero tide" or _from.lower() == "zero_tide") and
            (_to.lower() == "mean tide" or _to.lower() == "mean_tide")):
        return -deltaW_ZT
    elif ((_from.lower() == "zero tide" or _from.lower() == "zero_tide") and
          (_to.lower() == "tide free" or _to.lower() == "tide_free")):
        return (k - h) * deltaW_ZT
    elif ((_from.lower() == "mean tide" or _from.lower() == "mean_tide") and
          (_to.lower() == "zero tide" or _to.lower() == "zero_tide")):
        return deltaW_ZT
    elif ((_from.lower() == "mean tide" or _from.lower() == "mean_tide") and
          (_to.lower() == "tide free" or _to.lower() == "tide_free")):
        return (1. + k + h) * deltaW_ZT
    elif ((_from.lower() == "tide free" or _from.lower() == "tide_free") and
          (_to.lower() == "zero tide" or _to.lower() == "zero_tide")):
        return -(k - h) * deltaW_ZT
    elif ((_from.lower() == "tide free" or _from.lower() == "tide_free") and
          (_to.lower() == "mean tide" or _to.lower() == "mean_tide")):
        return -(1. + k + h) * deltaW_ZT
    else:
        return 0.0
