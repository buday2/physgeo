# -*- coding: utf-8 -*-

try:
    from mpl_toolkits.basemap import Basemap
except:
    raise ImportError("Unable to locate the Basemap toolkit on your computer.")

import numpy as np
from pylab import ylim, xlim
import math
import matplotlib.pyplot as plt
import matplotlib
from scipy import ndimage
from matplotlib.ticker import FixedFormatter  # fixed format for axis labels
from mpl_toolkits import axes_grid1
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.axes import Axes
from itertools import chain
import sys
import math


from scipy.interpolate import interpn
from scipy.interpolate import griddata

import pysrc.geodetic_functions as geo_f

from scipy.signal import convolve2d


# << Own colourbars >>
Terrain = LinearSegmentedColormap.from_list('mycmap', [(0.000, '#C8D785'),
                                                       (0.050, '#ABD9B1'),
                                                       (0.100, '#7CC478'),
                                                       (0.150, '#75C178'),
                                                       (0.200, '#AFCCA6'),
                                                       (0.250, '#DBD04F'),
                                                       (0.300, '#D6CF0E'),
                                                       (0.350, '#F1A700'),
                                                       (0.400, '#C0990D'),
                                                       (0.450, '#D3C070'),
                                                       (0.500, '#F0DBA1'),
                                                       (0.550, '#FCECC0'),
                                                       (0.600, '#F8FAEA'),
                                                       (0.650, '#E5FEFA'),
                                                       (0.700, '#DBFFFD'),
                                                       (0.750, '#D6FBFC'),
                                                       (0.800, '#B7F4F7'),
                                                       (0.850, '#73E0F1'),
                                                       (0.900, '#1DBCEF'),
                                                       (0.950, '#0089F5'),
                                                       (1.000, '#0050FA')]
                                            )

Gravity2 = LinearSegmentedColormap.from_list('mycmap', [(0.000, '#FF00FF'),
                                                        (0.050, '#FF88FF'),
                                                        (0.100, '#FFCCFF'),
                                                        (0.150, '#9900CC'),
                                                        (0.200, '#9988CC'),
                                                        (0.250, '#99FFCC'),
                                                        (0.300, '#00FFFF'),
                                                        (0.350, '#88FFFF'),
                                                        (0.400, '#DDFFFF'),
                                                        (0.450, '#FFFF00'),
                                                        (0.500, '#FFFF88'),
                                                        (0.550, '#FFFFCC'),
                                                        (0.600, '#FF0000'),
                                                        (0.650, '#FF8800'),
                                                        (0.700, '#FFCC00'),
                                                        (0.750, '#FFCC88'),
                                                        (0.800, '#FFCCCC'),
                                                        (0.850, '#88FF88'),
                                                        (0.900, '#88FFCC'),
                                                        (0.950, '#DDFFCC'),
                                                        (1.000, '#FFFFFF')]
                                             )

Gravity = LinearSegmentedColormap.from_list('mycmap', [(0.00000, '#FF00FF'),
                                                       (0.15265, '#9900CC'),
                                                       (0.35960, '#00FFFF'),
                                                       (0.55172, '#FFFF00'),
                                                       (0.64039, '#FFFF00'),
                                                       (0.77836, '#FF0000'),
                                                       (1.00000, '#FFFFFF')]  # white
                                            )

Grayscale = LinearSegmentedColormap.from_list('mycmap', [(0.00000, '#FFFFFF'),
                                                         (0.10000, '#E6E6E6'),
                                                         (0.20000, '#CCCCCC'),
                                                         (0.30000, '#B3B3B3'),
                                                         (0.40000, '#999999'),
                                                         (0.50000, '#808080'),
                                                         (0.60000, '#666666'),
                                                         (0.70000, '#4D4D4D'),
                                                         (0.80000, '#333333'),
                                                         (0.90000, '#191919'),
                                                         (1.00000, '#000000')]
                                              )


Grayscale2 = LinearSegmentedColormap.from_list('mycmap', [(0.00000, '#000000'),
                                                          (0.10000, '#191919'),
                                                          (0.20000, '#333333'),
                                                          (0.30000, '#4D4D4D'),
                                                          (0.40000, '#666666'),
                                                          (0.50000, '#808080'),
                                                          (0.60000, '#999999'),
                                                          (0.70000, '#B3B3B3'),
                                                          (0.80000, '#CCCCCC'),
                                                          (0.90000, '#E6E6E6'),
                                                          (1.00000, '#FFFFFF')]
                                               )


def moving_average_2d(data, window):
    """Moving average on two-dimensional data.
    """
    # Makes sure that the window function is normalized.
    window /= window.sum()
    # Makes sure data array is a numpy array or masked array.
    if type(data).__name__ not in ['ndarray', 'MaskedArray']:
        data = np.asarray(data)

    # The output array has the same dimensions as the input data
    # (mode='same') and symmetrical boundary conditions are assumed
    # (boundary='symm').
    return convolve2d(data, window, mode='same', boundary='symm')


def mycolorbar(ax, cbar):
    fig = ax.get_figure()
    cax = cbar.ax
    inv = ax.transAxes.inverted()
    R = fig.canvas.get_renderer()
    caxY = cax.yaxis
    ylab = caxY.get_label()
    ticklabelbbox = inv.transform(
        caxY.get_ticklabel_extents(R)[1].get_points())
    ylab.set_verticalalignment('center')
    ylab.set_horizontalalignment('center')
    ylab.set_rotation(0)
    xpos, ypos = np.max(ticklabelbbox[:, 0]), (3 / 4)
    caxY.set_label_coords(xpos, ypos, transform=ax.transAxes)
    return xpos


def add_colorbar(im, aspect=20, pad_fraction=0.5, **kwargs):
    from mpl_toolkits import axes_grid1
    """Add a vertical color bar to an image plot."""
    divider = axes_grid1.make_axes_locatable(im.axes)
    width = axes_grid1.axes_size.AxesY(im.axes, aspect=1 / aspect)
    pad = axes_grid1.axes_size.Fraction(pad_fraction, width)
    current_ax = plt.gca()
    cax = divider.append_axes("right", size=width, pad=pad)
    plt.sca(current_ax)

    return im.axes.figure.colorbar(im, cax=cax, **kwargs)


def length_sphere(b1: float, l1: float, b2: float, l2: float, rsphere=1.)-> float:
    phi1 = math.radians(b1)
    phi2 = math.radians(b2)
    lam1 = math.radians(l1)
    lam2 = math.radians(l2)

    return rsphere * math.acos(math.sin(phi1) * math.sin(phi2) +
                               math.cos(phi1) * math.cos(phi2) * math.cos(lam1 - lam2))


def lhuilierTheorem(a: float, b: float, c: float, rsphere: float =1.)-> float:
    """
    a,b,c lengths of the triangle sides
    r radius of a reference sphere
    """

    a /= rsphere
    b /= rsphere
    c /= rsphere

    s = .5 * (a + b + c)

    p = math.tan(s / 2) * math.tan((s - a) / 2) * \
        math.tan((s - b) / 2) * math.tan((s - c) / 2)

    if p < 0:
        spherical_exces = 0.
    else:
        spherical_exces = 4. * math.atan(math.sqrt(p))

    return rsphere * rsphere * spherical_exces  # in radias


def multidim_intersect(arr1, arr2):

    arr1set = set([tuple(x) for x in arr1])
    arr2set = set([tuple(x) for x in arr2])

    return np.array([x for x in arr1set & arr2set])


def get_right_lat(lat):
    if lat < - 90.:
        return -90.
    elif lat > 90.:
        return 90.
    else:
        return lat

# ---------------------------------------------------------------------------------------------------------------------------------


class Isgem_model:
    def __init__(self):
        self.model_name = "Unknown"
        self.model_type = "Unknown"
        self.units = "Unknown"
        self.ref_ell = "Unknown"
        self.lat_min = None
        self.lat_max = None
        self.lon_min = None
        self.lon_max = None
        self.dlat = None
        self.dlon = None
        self.nrows = None
        self.ncols = None
        self.nodata = None
        self.isg_format = "Unknown"
        pass

    def load_isgem(self, fpath):
        # read header first
        # Class should be used here
        if sys.version_info[0] < 3:
            from types import StringType
            if isinstance(fpath, StringType):
                f_exist = True
            else:
                f_exist = False
        else:
            if isinstance(fpath, str):
                f_exist = True
            else:
                f_exist = False

        if f_exist:
            fo = open(fpath, "r")
            fo_lines = fo.readlines()

            eoh = int(0)
            for i in range(0, len(fo_lines)):
                if "end_of_head" in fo_lines[i]:
                    eoh = int(i)  # end of header
                    break

            header = fo_lines[0:eoh]
            self.loaded_header = header
            self.data = np.loadtxt(fo_lines[eoh + 1::])
            self.data = np.flipud(self.data)

            for line in header:
                words = line.split()

                if "model name" in line:
                    self.model_name = words[-1]
                elif "model type" in line:
                    self.model_type = words[-1]
                elif "units" in line:
                    self.units = words[2]
                elif "reference" in line:
                    self.ref_ell = words[-1]
                elif "lat min" in line:
                    self.lat_min = float(words[3])
                elif "lat max" in line:
                    self.lat_max = float(words[3])
                elif "lon min" in line:
                    self.lon_min = float(words[3])
                elif "lon max" in line:
                    self.lon_max = float(words[3])
                elif "delta lat" in line:
                    self.dlat = float(words[3])
                elif "delta lon" in line:
                    self.dlon = float(words[3])
                elif "nrows" in line:
                    self.nrows = int(words[2])
                elif "ncols" in line:
                    self.ncols = int(words[2])
                elif "nodata" in line:
                    self.nodata = float(words[2])
                elif "ISG format" in line:
                    self.isg_format = words[-1]

            m_nrows, m_ncols = self.data.shape
            if (self.nrows != m_nrows or self.ncols != m_ncols):
                self.nrows = m_nrows
                self.ncols = m_ncols

            fo.close()
        else:
            raise TypeError('f must be a file object or a file name.')

    def __add__(self, new_model):
        srows, scols = self.data.shape
        nrows, ncols = new_model.data.shape

        if abs(self.dlat -
               new_model.dlat) < 1.0e-8 and abs(self.dlon -
                                                new_model.dlon) < 1.0e-8:
            if abs(self.lat_min -
                   new_model.lat_min) < 1.0e-8 and abs(self.lat_max -
                                                       new_model.lat_max) < 1.0e-8 and abs(self.lon_min -
                                                                                           new_model.lon_min) < 1.0e-8 and abs(self.lon_max -
                                                                                                                               new_model.lon_max) < 1.0e-8:
                # Do simple staff, just math operation on two matrixes (element
                # wise)

                try:
                    # Finding all nodata elements in current model
                    s_index = np.nonzero(abs(self.data - self.nodata) > 1.0e-8)

                    # Finding all nodata elements in new model
                    n_index = np.nonzero(
                        abs(new_model.data - new_model.nodata) > 1.0e-8)

                    # Finding intersection of the indexes
                    s_index = np.transpose(np.array([s_index[0], s_index[1]]))
                    n_index = np.transpose(np.array([n_index[0], n_index[1]]))

                    intersec_index = multidim_intersect(s_index, n_index)

                    self.data[intersec_index[:,
                                             0],
                              intersec_index[:,
                                             1]] = self.data[intersec_index[:,
                                                                            0],
                                                             intersec_index[:,
                                                                            1]] + new_model.data[intersec_index[:,
                                                                                                                0],
                                                                                                 intersec_index[:,
                                                                                                                1]]

                    return self
                except (RuntimeError, TypeError, NameError):
                    print(
                        "Could not  perform an arithmetic operation with ISGEM models.")
            else:  # more general approach then the previous one
                nmin_old, mmin_old = self.get_index_of(
                    new_model.lat_min, new_model.lon_min)
                nmax_old, mmax_old = self.get_index_of(
                    new_model.lat_max, new_model.lon_max)

                nmin_new, mmin_new = new_model.get_index_of(
                    self.lat_min, self.lon_min)
                nmax_new, mmax_new = new_model.get_index_of(
                    self.lat_max, self.lon_max)

                old_cutout = self.data[nmin_old:nmax_old, mmin_old:mmax_old]
                new_cutout = new_model.data[nmin_new:nmax_new,
                                            mmin_new:mmax_new]

                # Finding all nodata elements in current model
                s_index = np.nonzero(abs(old_cutout - self.nodata) > 1.0e-8)

                # Finding all nodata elements in new model
                n_index = np.nonzero(
                    abs(new_cutout - new_model.nodata) > 1.0e-8)

                # Finding intersection of the indexes
                s_index = np.transpose(np.array([s_index[0], s_index[1]]))
                n_index = np.transpose(np.array([n_index[0], n_index[1]]))

                i_index = multidim_intersect(
                    s_index, n_index)  # intersection of the indexes

                # arithmetic operation
                new_cutout_data = np.empty(old_cutout.shape)
                new_cutout_data.fill(self.nodata)

                new_cutout_data[i_index[:,
                                        0],
                                i_index[:,
                                        1]] = old_cutout[i_index[:,
                                                                 0],
                                                         i_index[:,
                                                                 1]] + new_cutout[i_index[:,
                                                                                          0],
                                                                                  i_index[:,
                                                                                          1]]

                self.data[nmin_old:nmax_old,
                          mmin_old:mmax_old] = new_cutout_data
        else:
            print(
                "Could not  perform an arithmetic operation with ISGEM models. Different size of the pixels.")

    def __sub__(self, new_model):
        srows, scols = self.data.shape
        nrows, ncols = new_model.data.shape

        if abs(self.dlat -
               new_model.dlat) < 1.0e-8 and abs(self.dlon -
                                                new_model.dlon) < 1.0e-8:
            if abs(self.lat_min -
                   new_model.lat_min) < 1.0e-8 and abs(self.lat_max -
                                                       new_model.lat_max) < 1.0e-8 and abs(self.lon_min -
                                                                                           new_model.lon_min) < 1.0e-8 and abs(self.lon_max -
                                                                                                                               new_model.lon_max) < 1.0e-8:
                # Do simple staff, just math operation on two matrixes (element
                # wise)

                try:
                    # Finding all nodata elements in current model
                    s_index = np.nonzero(abs(self.data - self.nodata) > 1.0e-8)

                    # Finding all nodata elements in new model
                    n_index = np.nonzero(
                        abs(new_model.data - new_model.nodata) > 1.0e-8)

                    # Finding intersection of the indexes
                    s_index = np.transpose(np.array([s_index[0], s_index[1]]))
                    n_index = np.transpose(np.array([n_index[0], n_index[1]]))

                    intersec_index = multidim_intersect(s_index, n_index)

                    self.data[intersec_index[:,
                                             0],
                              intersec_index[:,
                                             1]] = self.data[intersec_index[:,
                                                                            0],
                                                             intersec_index[:,
                                                                            1]] - new_model.data[intersec_index[:,
                                                                                                                0],
                                                                                                 intersec_index[:,
                                                                                                                1]]

                    return self
                except (RuntimeError, TypeError, NameError):
                    print(
                        "Could not  perform an arithmetic operation with ISGEM models.")
            else:  # more general approach then the previous one
                nmin_old, mmin_old = self.get_index_of(
                    new_model.lat_min, new_model.lon_min)
                nmax_old, mmax_old = self.get_index_of(
                    new_model.lat_max, new_model.lon_max)

                nmin_new, mmin_new = new_model.get_index_of(
                    self.lat_min, self.lon_min)
                nmax_new, mmax_new = new_model.get_index_of(
                    self.lat_max, self.lon_max)

                old_cutout = self.data[nmin_old:nmax_old, mmin_old:mmax_old]
                new_cutout = new_model.data[nmin_new:nmax_new,
                                            mmin_new:mmax_new]

                # Finding all nodata elements in current model
                s_index = np.nonzero(abs(old_cutout - self.nodata) > 1.0e-8)

                # Finding all nodata elements in new model
                n_index = np.nonzero(
                    abs(new_cutout - new_model.nodata) > 1.0e-8)

                # Finding intersection of the indexes
                s_index = np.transpose(np.array([s_index[0], s_index[1]]))
                n_index = np.transpose(np.array([n_index[0], n_index[1]]))

                i_index = multidim_intersect(
                    s_index, n_index)  # intersection of the indexes

                # arithmetic operation
                new_cutout_data = np.empty(old_cutout.shape)
                new_cutout_data.fill(self.nodata)

                new_cutout_data[i_index[:,
                                        0],
                                i_index[:,
                                        1]] = old_cutout[i_index[:,
                                                                 0],
                                                         i_index[:,
                                                                 1]] - new_cutout[i_index[:,
                                                                                          0],
                                                                                  i_index[:,
                                                                                          1]]

                self.data[nmin_old:nmax_old,
                          mmin_old:mmax_old] = new_cutout_data
        else:
            print(
                "Could not  perform an arithmetic operation with ISGEM models. Different size of the pixels.")

    def __mul__(self, new_model):
        srows, scols = self.data.shape
        nrows, ncols = new_model.data.shape

        if abs(self.dlat -
               new_model.dlat) < 1.0e-8 and abs(self.dlon -
                                                new_model.dlon) < 1.0e-8:
            if abs(self.lat_min -
                   new_model.lat_min) < 1.0e-8 and abs(self.lat_max -
                                                       new_model.lat_max) < 1.0e-8 and abs(self.lon_min -
                                                                                           new_model.lon_min) < 1.0e-8 and abs(self.lon_max -
                                                                                                                               new_model.lon_max) < 1.0e-8:
                # Do simple staff, just math operation on two matrixes (element
                # wise)

                try:
                    # Finding all nodata elements in current model
                    s_index = np.nonzero(abs(self.data - self.nodata) > 1.0e-8)

                    # Finding all nodata elements in new model
                    n_index = np.nonzero(
                        abs(new_model.data - new_model.nodata) > 1.0e-8)

                    # Finding intersection of the indexes
                    s_index = np.transpose(np.array([s_index[0], s_index[1]]))
                    n_index = np.transpose(np.array([n_index[0], n_index[1]]))

                    intersec_index = multidim_intersect(s_index, n_index)

                    self.data[intersec_index[:, 0], intersec_index[:, 1]] = np.multiply(
                        self.data[intersec_index[:, 0], intersec_index[:, 1]], new_model.data[intersec_index[:, 0], intersec_index[:, 1]])

                    return self
                except (RuntimeError, TypeError, NameError):
                    print(
                        "Could not  perform an arithmetic operation with ISGEM models.")
            else:  # more general approach then the previous one
                nmin_old, mmin_old = self.get_index_of(
                    new_model.lat_min, new_model.lon_min)
                nmax_old, mmax_old = self.get_index_of(
                    new_model.lat_max, new_model.lon_max)

                nmin_new, mmin_new = new_model.get_index_of(
                    self.lat_min, self.lon_min)
                nmax_new, mmax_new = new_model.get_index_of(
                    self.lat_max, self.lon_max)

                old_cutout = self.data[nmin_old:nmax_old, mmin_old:mmax_old]
                new_cutout = new_model.data[nmin_new:nmax_new,
                                            mmin_new:mmax_new]

                # Finding all nodata elements in current model
                s_index = np.nonzero(abs(old_cutout - self.nodata) > 1.0e-8)

                # Finding all nodata elements in new model
                n_index = np.nonzero(
                    abs(new_cutout - new_model.nodata) > 1.0e-8)

                # Finding intersection of the indexes
                s_index = np.transpose(np.array([s_index[0], s_index[1]]))
                n_index = np.transpose(np.array([n_index[0], n_index[1]]))

                i_index = multidim_intersect(
                    s_index, n_index)  # intersection of the indexes

                # arithmetic operation
                new_cutout_data = np.empty(old_cutout.shape)
                new_cutout_data.fill(self.nodata)

                new_cutout_data[i_index[:, 0], i_index[:, 1]] = np.multiply(
                    old_cutout[i_index[:, 0], i_index[:, 1]], new_cutout[i_index[:, 0], i_index[:, 1]])

                self.data[nmin_old:nmax_old,
                          mmin_old:mmax_old] = new_cutout_data
        else:
            print(
                "Could not  perform an arithmetic operation with ISGEM models. Different size of the pixels.")

    def __div__(self, new_model):
        srows, scols = self.data.shape
        nrows, ncols = new_model.data.shape

        if abs(self.dlat -
               new_model.dlat) < 1.0e-8 and abs(self.dlon -
                                                new_model.dlon) < 1.0e-8:
            if abs(self.lat_min -
                   new_model.lat_min) < 1.0e-8 and abs(self.lat_max -
                                                       new_model.lat_max) < 1.0e-8 and abs(self.lon_min -
                                                                                           new_model.lon_min) < 1.0e-8 and abs(self.lon_max -
                                                                                                                               new_model.lon_max) < 1.0e-8:
                # Do simple staff, just math operation on two matrixes (element
                # wise)

                try:
                    # Finding all nodata elements in current model
                    s_index = np.nonzero(abs(self.data - self.nodata) > 1.0e-8)

                    # Finding all nodata elements in new model
                    n_index = np.nonzero(
                        abs(new_model.data - new_model.nodata) > 1.0e-8)

                    # Finding intersection of the indexes
                    s_index = np.transpose(np.array([s_index[0], s_index[1]]))
                    n_index = np.transpose(np.array([n_index[0], n_index[1]]))

                    intersec_index = multidim_intersect(s_index, n_index)

                    self.data[intersec_index[:, 0], intersec_index[:, 1]] = np.divide(
                        self.data[intersec_index[:, 0], intersec_index[:, 1]], new_model.data[intersec_index[:, 0], intersec_index[:, 1]])

                    return self
                except (RuntimeError, TypeError, NameError):
                    print(
                        "Could not  perform an arithmetic operation with ISGEM models.")
            else:  # more general approach then the previous one
                nmin_old, mmin_old = self.get_index_of(
                    new_model.lat_min, new_model.lon_min)
                nmax_old, mmax_old = self.get_index_of(
                    new_model.lat_max, new_model.lon_max)

                nmin_new, mmin_new = new_model.get_index_of(
                    self.lat_min, self.lon_min)
                nmax_new, mmax_new = new_model.get_index_of(
                    self.lat_max, self.lon_max)

                old_cutout = self.data[nmin_old:nmax_old, mmin_old:mmax_old]
                new_cutout = new_model.data[nmin_new:nmax_new,
                                            mmin_new:mmax_new]

                # Finding all nodata elements in current model
                s_index = np.nonzero(abs(old_cutout - self.nodata) > 1.0e-8)

                # Finding all nodata elements in new model
                n_index = np.nonzero(
                    abs(new_cutout - new_model.nodata) > 1.0e-8)

                # Finding intersection of the indexes
                s_index = np.transpose(np.array([s_index[0], s_index[1]]))
                n_index = np.transpose(np.array([n_index[0], n_index[1]]))

                i_index = multidim_intersect(
                    s_index, n_index)  # intersection of the indexes

                # arithmetic operation
                new_cutout_data = np.empty(old_cutout.shape)
                new_cutout_data.fill(self.nodata)

                new_cutout_data[i_index[:, 0], i_index[:, 1]] = np.divide(
                    old_cutout[i_index[:, 0], i_index[:, 1]], new_cutout[i_index[:, 0], i_index[:, 1]])

                self.data[nmin_old:nmax_old,
                          mmin_old:mmax_old] = new_cutout_data
        else:
            print(
                "Could not  perform an arithmetic operation with ISGEM models. Different size of the pixels.")

    def plot_isgem(self, save_fig2file, crd_system: str='utm', show_plot: bool = True,
                   #boundaries = [],
                   #stepphi = .25,
                   #steplam = .25,
                   #color_map = Gravity,
                   *args,
                   ** kwargs):

        plt.cla()   # Clear axis
        plt.clf()   # Clear figure
        plt.close()  # Close a figure window
        #print ( self.data.shape, self.nrows, self.ncols )
        # boundaries - list of 2  or 6 elements [z_min, z_max] or [phi_min,
        # phi_max, lam_min, lam_max, z_min, z_max]
        if ("boundaries" in kwargs):
            boundaries = kwargs.get("boundaries")

            if len(boundaries) == 4:
                binterval = -self.lat_min + self.lat_max
                linterval = -self.lon_min + self.lon_max

                b_min = boundaries[0]
                b_max = boundaries[1]
                l_min = boundaries[2]
                l_max = boundaries[3]
                stepphi = 0.25
                steplam = 0.25

                b_min = get_right_lat(b_min)
                b_max = get_right_lat(b_max)
            elif len(boundaries) == 6:
                b_min = boundaries[0]
                b_max = boundaries[1]
                l_min = boundaries[2]
                l_max = boundaries[3]
                stepphi = boundaries[4]
                steplam = boundaries[5]
            else:
                raise Exception(
                    'The inputed list \"boundaries\" should have 4 or 6 elements.')
        else:
            binterval = -self.lat_min + self.lat_max
            linterval = -self.lon_min + self.lon_max

            b_min = self.lat_min - binterval / 10
            b_max = self.lat_max + binterval / 10
            l_min = self.lon_min - linterval / 10
            l_max = self.lon_max + linterval / 10

            b_min = get_right_lat(b_min)
            b_max = get_right_lat(b_max)

            stepphi = 0.25
            steplam = 0.25

        b0 = (b_min + b_max) / 2.
        l0 = (l_min + l_max) / 2.
        # ! Leaving out the nodata values
        nan_val = np.nonzero(self.data == float('nan'))
        nod_indexes = np.nonzero(self.data == self.nodata)
        val_indexes = np.nonzero(self.data != self.nodata)

        if ("color_map" in kwargs):
            color_map = kwargs.get("color_map")
        else:
            color_map = Gravity

        plotContours = False
        if ("contours" in kwargs):
            plotContours = True
            contours = kwargs.get("contours")

            cmin = contours[0]
            cmax = contours[1]
            clev = contours[2]

            conlevels = []

            temp = cmin
            if (abs(cmin - cmax) < 2.0e-15):
                conlevels.append(cmin)
                conlevels.append(cmin + 2.0e-15)
            else:
                while temp <= cmax:
                    conlevels.append(temp)
                    temp = temp + clev
        else:
            plotContours = False

        h_range_manual = False
        if ("h_range" in kwargs):
            h_range_manual = True
            hRangeVec = kwargs.get("h_range")
            if (len(hRangeVec) == 3):
                h_min = hRangeVec[0]
                h_max = hRangeVec[1]
                h_step = hRangeVec[2]
            elif (len(hRangeVec) == 2):  # h_range
                h_min = hRangeVec[0]
                h_max = hRangeVec[1]
                h_step = abs(h_max - h_min) / 100.
            else:
                print(
                    "incorrect number of arguments in  option \"h_range\" , 3 are needed!\n")

                h_min = self.data.min()
                h_max = self.data.max()
                h_step = abs(h_max - h_min) / 100.
        else:
            h_min = self.data.min()
            h_max = self.data.max()
            h_step = abs(h_max - h_min) / 100.

        # rewritte the units of the model plotted manualy
        if ("units" in kwargs):
            self.units = kwargs.get("units")

        dpimage = 200
        if ("dpi" in kwargs):
            dpimage = kwargs.get("dpi")
        else:
            dpimage = 200

        r_val = 0.
        for i in range(0, np.size(self.data, 0)):
            el_found = False
            for j in range(0, np.size(self.data, 1)):
                if (self.data[i][j] != self.nodata):
                    r_val = self.data[i][j]
                    el_found = True
                    break
            if el_found:
                break

        self.data[nod_indexes] = r_val

        clevels = []
        temp = h_min
        if (abs(h_max - h_min) < 2.0e-15):
            clevels.append(h_min)
            clevels.append(h_min + 2.0e-15)
        else:
            while temp <= h_max:
                clevels.append(temp)
                temp = temp + h_step

        self.data[nod_indexes] = self.nodata
        del temp, nod_indexes, r_val

        #### ====== STARTING THE PLOT ITSELF ====== ####
        if (crd_system == 'utm'):
            m = Basemap(
                llcrnrlon=l_min,
                llcrnrlat=b_min,
                urcrnrlon=l_max,
                urcrnrlat=b_max,
                resolution='i',
                projection='tmerc',
                lon_0=l0,
                lat_0=b0)
        elif (crd_system == 'eckert4'):  # Eckert IV Projection
            m = Basemap(projection='eck4', lon_0=0, resolution='c')
            # m.fillcontinents(color='coral',lake_color='aqua')
            m.drawmapboundary(fill_color='aqua')
            # m.drawcoastlines()
        elif (crd_system == 'kav7'):  # Kavrayskiy VII Projection
            m = Basemap(projection='kav7', lon_0=0, resolution='c')
            # m.fillcontinents(color='coral',lake_color='aqua')
            m.drawmapboundary(fill_color='aqua')
        elif (crd_system == 'moll'):
            m = Basemap(projection='moll', lat_0=b0, lon_0=l0)
        elif (crd_system == "cyl"):
            m = Basemap(projection='cyl', resolution=None,
                        llcrnrlat=-90, urcrnrlat=90,
                        llcrnrlon=-180, urcrnrlon=180)
        else:
            raise NameError('Unknown coordinate system. Function ends here.')
            return False

        # draw parallels and meridians.
        parallels = m.drawparallels(
            np.arange(
                b_min,
                b_max,
                stepphi),
            labels=[
                1,
                0,
                0,
                0],
            fontsize=15)
        meridians = m.drawmeridians(
            np.arange(
                l_min,
                l_max,
                steplam),
            labels=[
                0,
                0,
                1,
                0],
            fontsize=15,
            rotation=90)
        m.drawcountries(
            linewidth=2.50,
            linestyle='solid',
            color='k',
            antialiased=1,
            ax=None,
            zorder=None)

        try:
            m.drawcoastlines(
                linewidth=2.50,
                linestyle='solid',
                color='k',
                antialiased=1)
        except BaseException:
            print("Unable to draw the drawcoastlines.")
            pass

        b_vec = np.linspace(
            self.lat_min,
            self.lat_max,
            self.nrows,
            endpoint=True)
        l_vec = np.linspace(
            self.lon_min,
            self.lon_max,
            self.ncols,
            endpoint=True)

        l_vec, b_vec = np.meshgrid(l_vec, b_vec)
        xi, yi = m(l_vec, b_vec)
        xmin, ymin = m(l_min, b_min)
        xmax, ymax = m(l_max, b_max)

        plt.xlim(xmin, xmax)
        plt.ylim(ymin, ymax)

        matplotlib.rcParams['contour.negative_linestyle'] = 'solid'

        LEGEND_SET = False
        if ("legend" in kwargs):
            LEGEND_SET = True
            legenda = kwargs.get("legend")

        if ("points" in kwargs):
            bl = kwargs.get("points")

            clrs = "rbmgyk"

            if (isinstance(bl, type(np.array(())))):
                xp, yp = m(bl[:, 1], bl[:, 0])

                if LEGEND_SET:
                    m.plot(
                        xp,
                        yp,
                        marker='.',
                        color='r',
                        linestyle="None",
                        label=legenda)
                else:
                    m.plot(xp, yp, marker='.', color='r', linestyle="None")
            elif isinstance(bl, type([])):
                it = int(0)

                for matrix in bl:
                    xp, yp = m(matrix[:, 1], matrix[:, 0])

                    if LEGEND_SET:
                        m.plot(
                            xp,
                            yp,
                            marker='.',
                            color=clrs[it],
                            linestyle="None",
                            label=legenda[it])
                    else:
                        m.plot(
                            xp,
                            yp,
                            marker='.',
                            color=clrs[it],
                            linestyle="None")

                    it += 1
            else:
                print("Isgem_model::plot_isgem() option \"points\" not recognized.")

        if LEGEND_SET:
            plt.legend()

        if (plotContours):  # plot contours
            cs1 = m.contour(
                xi,
                yi,
                self.data,
                6,
                colors='k',
                levels=conlevels,
                linestyle='solid',
                linewidth=1.75)
            plt.clabel(
                cs1,
                fontsize=13,
                inline=1,
                fmt="%1.2f",
                inline_spacing=0)

        cs2 = m.contourf(xi, yi, self.data, levels=clevels, cmap=color_map)
        cbar = m.colorbar(cs2, location='bottom', pad="10%")
        cbar.ax.set_xticklabels(cbar.ax.get_xticklabels(), rotation='vertical')

        for t in cbar.ax.get_xticklabels():
            t.set_fontsize(14)

        cbar.set_label(self.units, usetex='False', fontsize=18, weight='bold')

        if show_plot:
            plt.show()
        else:
            plt.savefig(save_fig2file, dpi=dpimage, bbox_inches='tight')

        plt.clf()

    def fast_interpolate(self, b: np.array, l: np.array, z: np.array):
        """
        """
        n = len(b)

        z_int = np.empty_like(z)
        for i in range(0, n):
            z_int[i] = self.interpolate_value(b[i], l[i])

        v = z - z_int

        v_mean = np.mean(v)
        v_std = np.std(v)
        v_max = np.max(v)
        v_min = np.min(v)
        v_50 = np.quantile(v, 0.5)  # meadian
        v_25 = np.quantile(v, 0.25)  # q25
        v_75 = np.quantile(v, 0.75)  # q75

        print("v_min := ", v_min)
        print("v_max := ", v_max)
        print("v_mean:= ", v_mean)
        print("v_std := ", v_std)
        print("v_median := ", v_50)
        print("v_q25 := ", v_25)
        print("v_q75 := ", v_75)
        print(abs(v_25 - v_50), abs(v_75 - v_50),
              abs(v_25 - v_50) - abs(v_75 - v_50))

        return z_int, v

    def print_header(self):
        # print header info
        print("model name: " + self.model_name)
        print("model type: " + self.model_type)
        print("units:      " + self.units)
        print("reference:  " + self.ref_ell)
        print("lat min:    " + str(self.lat_min))
        print("lat max:    " + str(self.lat_max))
        print("lon min:    " + str(self.lon_min))
        print("lon max:    " + str(self.lon_max))
        print("delta lat:  " + str(self.dlat))
        print("delta lon:  " + str(self.dlon))
        print("nrows:      " + str(self.nrows))
        print("ncols:      " + str(self.ncols))
        print("nodata:     " + str(self.nodata))
        print("ISG format: " + self.isg_format)

    def change_header_size(self, new_nrows, new_ncols):
        self.nrows = new_nrows
        self.ncols = new_ncols

    def print2file(
            self,
            path,
            name_of_mat="unkown",
            use_hdf5=False,
            use_original_header=False,
            *args,
            **kwargs):
        if ("format" in kwargs):
            format = kwargs.get("format")
        else:
            format = '%3.9e'

        if (use_original_header):
            hdinfo = ""
            for item in self.loaded_header:
                hdinfo = hdinfo + item

            hdinfo = hdinfo + "end_of_head =================================================="
        else:
            hdinfo = "begin_of_head ================================================\n" \
                + "model name : " + self.model_name + "\n"\
                + "model type : " + self.model_type + "\n" \
                + "units :      " + self.units + "\n"\
                + "reference :  " + self.ref_ell + "\n" \
                + "lat min :    " + str(self.lat_min) + "\n"\
                + "lat max :    " + str(self.lat_max) + "\n" \
                + "lon min :    " + str(self.lon_min) + "\n"\
                + "lon max :    " + str(self.lon_max) + "\n"\
                + "delta lat :  " + str(self.dlat) + "\n"\
                + "delta lon :  " + str(self.dlon) + "\n"\
                + "nrows :      " + str(int(self.nrows)) + "\n"\
                + "ncols :      " + str(int(self.ncols)) + "\n"\
                + "nodata :     " + str(self.nodata) + "\n"\
                + "ISG format : " + self.isg_format + "\n" \
                + "end_of_head =================================================="

        if use_hdf5:
            import h5py
            # used for jtsk2etrs transformation
            # not flipped data - so index [0,0] stands for phi_min, lam_min

            h5f = h5py.File(path, 'a')
            # saving the header separetaly

            header_mat = np.array([self.lat_min,
                                   self.lat_max,
                                   self.lon_min,
                                   self.lon_max,
                                   self.nodata,
                                   self.dlat,
                                   self.dlon,
                                   self.nrows,
                                   self.ncols])

            h5f.create_dataset(name_of_mat + '_header', data=header_mat)
            h5f.create_dataset(name_of_mat, data=np.transpose(self.data))

            h5f.close()

        else:
            #selfdata_flipped = np.flipud(self.data )
            #np.savetxt( path , np.transpose(selfdata_flipped) , fmt = '%3.7e' , header = hdinfo, comments='')
            np.savetxt(
                path,
                np.flipud(
                    self.data),
                fmt=format,
                header=hdinfo,
                comments='')

    def export_to_esri_ascii(self, fpath, *args, **kwargs):
        """
        ncols         4
        nrows         6
        xllcorner     0.0
        yllcorner     0.0
        cellsize      50.0
        NODATA_value  -9999
        -9999 -9999 5 2
        -9999 20 100 36
        3 8 35 10
        32 42 50 6
        88 75 27 9
        13 5 1 -9999
        """
        if ("format" in kwargs):
            format = kwargs.get("format")
        else:
            format = '%3.9e'

        nr, nc = self.data.shape

        hdinfo = "ncols        " + str(nc) + "\n" \
            + "nrows        " + str(nr) + "\n" \
            + "xllcorner    " + str("{:3.10f}".format(self.lon_min - self.dlon / 2.)) + "\n" \
            + "yllcorner    " + str("{:3.10f}".format(self.lat_min - self.dlat / 2.)) + "\n" \
            + "cellsize     " + str("{:3.10f}".format(self.dlat)) + "\n"    \
            + "NODATA_value " + str("{:3.10f}".format(self.nodata))

        np.savetxt(
            fpath,
            np.flipud(
                self.data),
            fmt=format,
            header=hdinfo,
            comments='')

    def load_ascii_from_arcgis(self, fpath):
        """
        ncols         13043
        nrows         4439
        xllcorner     11,800833334488
        yllcorner     47,500833332886
        cellsize      0,0008333333333
        NODATA_value  -9999
        1 2 3 4 6.....

        The center of the lower left cell
        NCOLS xxx
                NROWS xxx
                XLLCENTER xxx
                YLLCENTER xxx
                CELLSIZE xxx
                NODATA_VALUE xxx
                row 1
                row 2
                ...
                row n

                The lower left corner of the lower left cell

                NCOLS xxx
                NROWS xxx
                XLLCORNER xxx
                YLLCORNER xxx
                CELLSIZE xxx
                NODATA_VALUE xxx
                row 1
                row 2
                ...
                row n
        """
        if sys.version_info[0] < 3:
            #print ( "This package is running under python 2.x " )
            from types import StringType
            if isinstance(fpath, StringType):
                f_exist = True
            else:
                f_exist = False
        else:
            #print ( "This package is running under python 3" )
            if isinstance(fpath, str):
                f_exist = True
            else:
                f_exist = False

        # print type(fpath), StringType
        if f_exist:
            fo = open(fpath, "r")
            fo_lines = fo.readlines()
            fo.close()

            eoh = 6
            header = fo_lines[0:eoh]

            np_mat = []
            for i in range(6, len(fo_lines)):
                np_mat.append((fo_lines[i]).replace(",", "."))

            del fo_lines

            #self.data = np.loadtxt( (fo_lines[eoh+1::]).replace(",", ".") )
            self.data = np.loadtxt(np_mat)
            self.data = np.flipud(self.data)

            cornbool = False
            for line in header:
                words = line.split()
                #print (words)

                self.model_name = "Unknown"
                self.model_type = "Unknown"
                self.units = "m"
                self.ref_ell = "Unknown"
                self.isg_format = "1.0"

                if "yllcorner" in line.lower():
                    self.lat_min = float(words[-1].replace(",", "."))
                    cornbool = True
                elif "xllcorner" in line.lower():
                    self.lon_min = float(words[-1].replace(",", "."))
                    cornbool = True
                elif "xllcenter" in line.lower():
                    self.lon_min = float(words[-1].replace(",", "."))
                    cornbool = False
                elif "yllcenter" in line.lower():
                    self.lat_min = float(words[-1].replace(",", "."))
                    cornbool = False
                elif "cellsize" in line.lower():
                    self.dlat = float(words[-1].replace(",", "."))
                    self.dlon = float(words[-1].replace(",", "."))
                elif "nrows" in line.lower():
                    self.nrows = int(words[-1].replace(",", "."))
                elif "ncols" in line.lower():
                    self.ncols = int(words[-1].replace(",", "."))
                elif "nodata_value" in line.lower() or "NODATA_value" in line.lower():
                    self.nodata = float(words[-1].replace(",", "."))
                elif "dx" in line.lower():
                    self.dlon = float(words[-1].replace(",", "."))
                elif "dy" in line.lower():
                    self.dlat = float(words[-1].replace(",", "."))

            if cornbool:
                self.lat_min += self.dlat / 2.
                self.lon_min += self.dlon / 2.

            self.lat_max = self.lat_min + self.nrows * self.dlat
            self.lon_max = self.lon_min + self.ncols * self.dlon

            print(
                "Loaded size:",
                (self.data).shape,
                " , detected size: ",
                self.nrows,
                ",",
                self.ncols)
        else:
            raise TypeError('f must be a file object or a file name.')

    def load_from_matrix(
            self,
            matdata,
            model_name='None',
            model_type='None',
            units='meetres',
            reference='Unknown',
            nodata=-9999.999,
            isg_format='1.0'):
        # Loads matrix into an modified isgem model (the valeu is linked to the
        # center of a cell)
        b_vec = matdata[:, 0]
        l_vec = matdata[:, 1]
        h_vec = matdata[:, 2]

        self.nodata = nodata        # NoData value
        self.model_name = model_name
        self.model_type = model_type
        self.units = units
        self.reference = reference
        self.isg_format = isg_format

        b_uniq = np.unique(b_vec)
        l_uniq = np.unique(l_vec)

        # ----------------------------------------------------
        self.lat_min = min(b_uniq)
        self.lat_max = max(b_uniq)
        self.lon_min = min(l_uniq)
        self.lon_max = max(l_uniq)

        self.nrows = int(max(b_uniq.shape))
        self.ncols = int(max(l_uniq.shape))

        self.dlat = (self.lat_max - self.lat_min) / (self.nrows - 1)
        self.dlon = (self.lon_max - self.lon_min) / (self.ncols - 1)

        self.data = np.empty([self.nrows, self.ncols])
        self.data.fill(self.nodata)

        for i in range(0, max(matdata.shape)):
            n = int(round((b_vec[i] - self.lat_min) / self.dlat))
            m = int(round((l_vec[i] - self.lon_min) / self.dlon))

            self.data[n, m] = h_vec[i]

    def load_ascii(
            self,
            path,
            fmat=1,
            model_name='None',
            model_type='None',
            units='meetres',
            reference='Unknown',
            nodata=-9999.999,
            isg_format='1.0'):
        """
        Load from classic ascii file such as (format type - fmat):
            1 - ptid b l h (default)
            2 - ptid l b h
            3 - b l h
            4 - l b h
        """

        raw_dat = np.loadtxt(path)

        if (int(fmat) == 2):
            pt_id = raw_dat[:, 0]
            b_vec = raw_dat[:, 2]
            l_vec = raw_dat[:, 1]
            h_vec = raw_dat[:, 3]
        elif (int(fmat) == 3):
            b_vec = raw_dat[:, 0]
            l_vec = raw_dat[:, 1]
            h_vec = raw_dat[:, 2]
        elif (int(fmat) == 4):
            b_vec = raw_dat[:, 1]
            l_vec = raw_dat[:, 0]
            h_vec = raw_dat[:, 2]
        else:
            pt_id = raw_dat[:, 0]
            b_vec = raw_dat[:, 1]
            l_vec = raw_dat[:, 2]
            h_vec = raw_dat[:, 3]

        self.nodata = nodata        # NoData value
        self.model_name = model_name
        self.model_type = model_type
        self.units = units
        self.reference = reference
        self.isg_format = isg_format

        b_uniq = np.unique(b_vec)
        l_uniq = np.unique(l_vec)

        # ----------------------------------------------------
        self.lat_min = min(b_uniq)
        self.lat_max = max(b_uniq)
        self.lon_min = min(l_uniq)
        self.lon_max = max(l_uniq)

        self.nrows = int(max(b_uniq.shape))
        self.ncols = int(max(l_uniq.shape))

        self.dlat = (self.lat_max - self.lat_min) / (self.nrows - 1)
        self.dlon = (self.lon_max - self.lon_min) / (self.ncols - 1)

        self.data = np.empty([self.nrows, self.ncols])
        self.data.fill(self.nodata)

        for i in range(0, max(raw_dat.shape)):
            n = int(round((b_vec[i] - self.lat_min) / self.dlat))
            m = int(round((l_vec[i] - self.lon_min) / self.dlon))

            self.data[n, m] = h_vec[i]

    def load_postdam_gfz(self, fpath=None, switch_cols=False):
        if fpath is None:
            print("Forgot to include path? ")
        else:
            if sys.version_info[0] < 3:
                #print ( "This package is running under python 2.x " )
                from types import StringType
                if isinstance(fpath, StringType):
                    f_exist = True
                else:
                    f_exist = False
            else:
                #print ( "This package is running under python 3" )
                if isinstance(fpath, str):
                    f_exist = True
                else:
                    f_exist = False

            # print type(fpath), StringType
            if f_exist:
                fo = open(fpath, "r")
                fo_lines = fo.readlines()

                eoh = int(0)
                for i in range(0, len(fo_lines)):
                    if "end_of_head" in fo_lines[i]:
                        eoh = int(i)  # end of header
                        break

                header = fo_lines[0:eoh]
                datacols = np.loadtxt(fo_lines[eoh + 1::])

                for line in header:
                    words = line.split()
                    #print (words)

                    if "modelname" in line:
                        self.model_name = words[1]
                    elif "functional" in line:
                        self.model_type = words[1]
                    elif "unit" in line:
                        self.units = words[1]
                    elif "refsysname" in line:
                        self.ref_ell = words[-1]
                    elif "gapvalue" in line:
                        self.nodata = float(words[1])
                    elif "ISG format" in line:
                        self.isg_format = words[3]
                    elif "tide_system" in line:
                        self.tide_system = line[1]

                switch_b = np.copy(datacols[:, 1])
                datacols[:, 1] = datacols[:, 0]
                datacols[:, 0] = switch_b

                if switch_cols:
                    datacols[:, 2] = datacols[:, 3]

                del switch_b

                self.load_from_matrix(datacols, model_name=self.model_name,
                                      model_type=self.model_type,
                                      units=self.units,
                                      reference=self.ref_ell,
                                      nodata=self.nodata,
                                      isg_format='1.0')
            else:
                raise TypeError('f must be a file object or a file name.')

    def plot_world(self, boundaries=None, numstep=100):
        """

        """
        if boundaries is None: boundaries = []
        
        m = Basemap(projection='moll', lat_0=0, lon_0=0)

        # lats and longs are returned as a dictionary
        lats = m.drawparallels(np.linspace(-90, 90, 13))
        lons = m.drawmeridians(np.linspace(-180, 180, 13))

        # keys contain the plt.Line2D instances
        lat_lines = chain(*(tup[1][0] for tup in lats.items()))
        lon_lines = chain(*(tup[1][0] for tup in lons.items()))
        all_lines = chain(lat_lines, lon_lines)

        # cycle through these lines and set the desired style
        for line in all_lines:
            line.set(linestyle='-', alpha=0.3, color='w')

        # draw parallels and meridians.
        parallels = m.drawparallels(
            np.arange(-90, 90, 10), labels=[1, 0, 0, 0], fontsize=12)
        meridians = m.drawmeridians(
            np.arange(-180, 180, 15), labels=[0, 0, 1, 0], fontsize=12, rotation=90)

        m.drawcoastlines(
            linewidth=2.50,
            linestyle='solid',
            color='k',
            antialiased=1,
            ax=None,
            zorder=None)

        # Z coordinate
        dat_min = 0.0
        dat_max = 0.0
        if len(boundaries) != 0:
            dat_min = boundaries[0]
            dat_max = boundaries[1]
        else:
            dat_min = self.data.min()
            dat_max = self.data.max()

        clevels = []
        temp = dat_min
        if (abs(dat_max - dat_min) < 2.0e-15):
            clevels.append(dat_min)
            clevels.append(dat_min + 2.0e-15)
        else:
            while temp <= dat_max:
                clevels.append(temp)
                temp = temp + (dat_max - dat_min) / numstep

        # converting coordinates from b,l to cartographi projection
        bgrid, lgrid = self.return_meshed_grid()
        lgrid = lgrid - 180

        xi, yi = m(lgrid, bgrid)
        #cs2 = m.contourf(xi,yi,self.data,cmap=Gravity )
        m.shadedrelief(scale=.2)
        #plt.plot( xi, yi , "b" , marker = "o", markersize=1 )
        cs = plt.contourf(xi, yi, self.data, levels=clevels, cmap=Gravity)

        cbar = m.colorbar(cs, location='bottom', pad="10%")
        cbar.ax.set_xticklabels(cbar.ax.get_xticklabels(), rotation='vertical')

        for t in cbar.ax.get_xticklabels():
            t.set_fontsize(11)
        plt.show()

    def ptr2header(self):
        hdinfo = "begin_of_head ================================================\n" \
            + "model name : " + self.model_name + "\n"\
            + "model type : " + self.model_type + "\n" \
            + "units :      " + self.units + "\n"\
            + "reference :  " + self.ref_ell + "\n" \
            + "lat min :    " + str(self.lat_min) + "\n"\
            + "lat max :    " + str(self.lat_max) + "\n" \
            + "lon min :    " + str(self.lon_min) + "\n"\
            + "lon max :    " + str(self.lon_max) + "\n"\
            + "delta lat :  " + str(self.dlat) + "\n"\
            + "delta lon :  " + str(self.dlon) + "\n"\
            + "nrows :      " + str(int(self.nrows)) + "\n"\
            + "ncols :      " + str(int(self.ncols)) + "\n"\
            + "nodata :     " + str(self.nodata) + "\n"\
            + "ISG format : " + self.isg_format + "\n" \
            + "end_of_head =================================================="

        return hdinfo

    def return_meshed_grid(self):
        b_vec = np.linspace(
            self.lat_min,
            self.lat_max,
            self.nrows,
            endpoint=True)
        l_vec = np.linspace(
            self.lon_min,
            self.lon_max,
            self.ncols,
            endpoint=True)

        l_vec, b_vec = np.meshgrid(l_vec, b_vec)
        return b_vec, l_vec

    def generate_bl_file(self, save2file: str):
        b_vec = np.linspace(
            self.lat_min,
            self.lat_max,
            self.nrows,
            endpoint=True)
        l_vec = np.linspace(
            self.lon_min,
            self.lon_max,
            self.ncols,
            endpoint=True)

        n_elem = self.nrows * self.ncols
        bl = np.zeros((n_elem, 2))

        i = int(0)
        for bval in b_vec:
            for lval in l_vec:
                bl[i, 0] = bval
                bl[i, 1] = lval

                i += 1

        np.savetxt(save2file, bl, fmt="%3.8e")

    def change_data(self, newdata):
        self.data = newdata

    def set_units(self, new_units):
        self.units = new_units

    def export2csv(self, foname):
        nr, nc = self.data.shape

        myfile = open(foname, 'w')

        for i in range(0, nr):
            bdeg = self.lat_min + float(i) * self.dlat
            bstr = "{:2.9f}".format(bdeg)

            for j in range(0, nc):
                ldeg = self.lon_min + float(j) * self.dlon
                lstr = "{:2.9f}".format(ldeg)

                zstr = "{:2.5f}".format(self.data[i, j])

                myfile.write(bstr + "," + lstr + "," + zstr + "\n")

        myfile.close()

    # check if given point is inside the model boundaries

    def isptin(self, bdeg, ldeg):  # is point in
        if bdeg >= self.lat_min and bdeg <= self.lat_max \
                and ldeg >= self.lon_min and ldeg <= self.lon_max:
            return True
        else:
            return False

    # returns the index in data matrix for an arbitraty point P(bdeg,ldeg)
    # if the inputvalue is less than min. value of the matrix then returns 0
    # if the input value is greater than max value of the max bound, returns
    # the max possible index
    def get_index_of(self, bdeg, ldeg):
        nrows, ncols = self.data.shape

        n = math.floor((bdeg - self.lat_min) / self.dlat)
        m = math.floor((ldeg - self.lon_min) / self.dlon)

        if n < 0:
            n = 0
        elif n >= nrows:
            n = nrows - 1

        if m < 0:
            m = 0
        elif m >= ncols:
            m = ncols - 1

        return n, m
# if bdeg >= self.lat_min and bdeg <= self.lat_max and ldeg >=
# self.lon_min and ldeg <= self.lon_max :

#        else :
#            if n < 0 and m < 0 :
#                return 0,0
#            elif n < 0 and m >= ncols :
#                return 0, ncols-1
#            elif n >= nrows and m < 0 :
#                return nrows - 1 , 0
#            else :
#                return nrows - 1, ncols -1

    def get_spherical_coordinated(self, n, m):
        """
        @param n - position in grid (latitude)
        @param m - position in grid (longitude)

        @return lat, lon for given indexes
        """

        lat = self.lat_min + n * self.dlat
        lon = self.lon_min + m * self.dlon

        return lat, lon

    def get_area_statistics(
            self,
            bmin: float,
            bmax: float,
            lmin: float,
            lmax: float):
        nmin, mmin = self.get_index_of(bmin, lmin)
        nmax, mmax = self.get_index_of(bmax, lmax)

#        print ( math.floor ( (bmin - self.lat_min ) / self.dlat )  )
#        print ( math.floor ( (bmax - self.lat_min ) / self.dlat )  )


#        print ("nmin := " , nmin , " , mmin := " , mmin )
#        print ("nmax := " , nmax , " , mmax := " , mmax )
#        print ( self.data.shape )

        v_max_value = self.data[nmin: nmax][:, mmin: mmax].max()
        v_min_value = self.data[nmin: nmax][:, mmin: mmax].min()
        v_sigma = np.std(self.data[nmin: nmax][:, mmin: mmax])
        v_mean = np.mean(self.data[nmin: nmax][:, mmin: mmax])

        return v_min_value, v_max_value, v_mean, v_sigma

    def cut_data_matrix(
            self,
            bmin: float,
            bmax: float,
            lmin: float,
            lmax: float):
        nmin, mmin = self.get_index_of(bmin, lmin)
        nmax, mmax = self.get_index_of(bmax, lmax)

        return self.data[nmin: nmax][:, mmin: mmax]

    def stats_area(self, bmin: float, lmin: float, bmax: float, lmax: float):
        nmin, mmin = self.get_index_of(bmin, lmin)
        nmax, mmax = self.get_index_of(bmax, lmax)

        print("min_val := ", np.amin(self.data[nmin: nmax][:, mmin: mmax]))
        print("max_val := ", np.amax(self.data[nmin: nmax][:, mmin: mmax]))

    def interpolate_value(self, bdeg, ldeg):
        """
        In order to introduce the considered linear interpolation schemes, let us denote by \f$ P_1 , P_2 , P_3 \f$ and by \f$ \alpha_1 , \alpha_2 , \alpha_3 \f$ the
        vertices and angles of a spherical triangle T on a unit sphere. Moreover, we denote by \f$ a_1 , a_2 , a_3 \f$ the triangle edges, i.e.,
        the geodesic arcs joining two of its vertices: \f$ a_1 \f$ joins \f$ P_2, P_3 \f$ and so on.
        """

        boolvalisptin = self.isptin(bdeg, ldeg)

        if boolvalisptin:
            rowidx, colidx = self.get_index_of(bdeg, ldeg)
            bcenter, lcenter = self.get_spherical_coordinated(rowidx, colidx)

            bcenter += self.dlat / 2.
            lcenter += self.dlon / 2.
            # === Selecting spherical triangle ===

            bdif = bdeg - bcenter
            ldif = ldeg - lcenter

            phi_list = []
            lam_list = []
            fvalues = []  # function values, values obtained from the grid
            if bdif < 0 and ldif < 0:
                # bottom left, bottom right, upper left (bl, br, ul)
                phi_list.append(bcenter - self.dlat / 2.)
                phi_list.append(bcenter - self.dlat / 2.)
                phi_list.append(bcenter + self.dlat / 2.)
                phi_list.append(bcenter - self.dlat / 2.)

                lam_list.append(lcenter - self.dlon / 2.)
                lam_list.append(lcenter + self.dlon / 2.)
                lam_list.append(lcenter - self.dlon / 2.)
                lam_list.append(lcenter - self.dlon / 2.)

                fvalues.append(self.data[rowidx + 1, colidx])
                fvalues.append(self.data[rowidx, colidx])
                fvalues.append(self.data[rowidx, colidx + 1])

            elif bdif >= 0 and ldif < 0:
                # bl, ul, ur
                phi_list.append(bcenter - self.dlat / 2.)
                phi_list.append(bcenter + self.dlat / 2.)
                phi_list.append(bcenter + self.dlat / 2.)
                phi_list.append(bcenter - self.dlat / 2.)

                lam_list.append(lcenter - self.dlon / 2.)
                lam_list.append(lcenter - self.dlon / 2.)
                lam_list.append(lcenter + self.dlon / 2.)
                lam_list.append(lcenter - self.dlon / 2.)

                fvalues.append(self.data[rowidx + 1, colidx + 1])
                fvalues.append(self.data[rowidx, colidx])
                fvalues.append(self.data[rowidx + 1, colidx])
            elif bdif >= 0 and ldif >= 0:
                # br, ul, ur
                phi_list.append(bcenter - self.dlat / 2.)
                phi_list.append(bcenter + self.dlat / 2.)
                phi_list.append(bcenter + self.dlat / 2.)
                phi_list.append(bcenter - self.dlat / 2.)

                lam_list.append(lcenter + self.dlon / 2.)
                lam_list.append(lcenter - self.dlon / 2.)
                lam_list.append(lcenter + self.dlon / 2.)
                lam_list.append(lcenter + self.dlon / 2.)

                fvalues.append(self.data[rowidx + 1, colidx + 1])
                fvalues.append(self.data[rowidx, colidx + 1])
                fvalues.append(self.data[rowidx + 1, colidx])
            else:
                # bl, br, ur
                phi_list.append(bcenter - self.dlat / 2.)
                phi_list.append(bcenter - self.dlat / 2.)
                phi_list.append(bcenter + self.dlat / 2.)
                phi_list.append(bcenter - self.dlat / 2.)

                lam_list.append(lcenter - self.dlon / 2.)
                lam_list.append(lcenter + self.dlon / 2.)
                lam_list.append(lcenter + self.dlon / 2.)
                lam_list.append(lcenter - self.dlon / 2.)

                fvalues.append(self.data[rowidx + 1, colidx + 1])
                fvalues.append(self.data[rowidx, colidx])
                fvalues.append(self.data[rowidx, colidx + 1])

            s1 = length_sphere(
                phi_list[0],
                lam_list[0],
                phi_list[1],
                lam_list[1])
            s2 = length_sphere(
                phi_list[1],
                lam_list[1],
                phi_list[2],
                lam_list[2])
            s3 = length_sphere(
                phi_list[2],
                lam_list[2],
                phi_list[0],
                lam_list[0])

            surface_triangle = lhuilierTheorem(s1, s2, s3)

            del s1, s2, s3

            w = 0.
            pw = 0.
            for i in range(
                    0, 3):  # 0,1, 2   , last point in list is the same as the first one
                s1 = length_sphere(
                    phi_list[i], lam_list[i], phi_list[i + 1], lam_list[i + 1])
                s2 = length_sphere(phi_list[i], lam_list[i], bdeg, ldeg)
                s3 = length_sphere(
                    bdeg, ldeg, phi_list[i + 1], lam_list[i + 1])

                # in radians, of given triangle
                surface = lhuilierTheorem(s1, s2, s3)
                weight = surface / surface_triangle

                w += weight
                pw += weight * fvalues[i]

            return pw / w
        else:
            return 0.

    def split_vertically(self):
        up = Isgem_model()
        low = Isgem_model()

        # shape of the data matrix
        nr, nc = self.data.shape

        nr_2 = int(nr / 2)

        low_data = self.data[0: nr_2, :]
        up_data = self.data[nr_2::, :]

        lnr, lnc = low_data.shape
        unr, unc = up_data.shape

        # create the header info
        up.model_name = self.model_name
        up.model_type = self.model_type
        up.units = self.units
        up.ref_ell = self.ref_ell

        up.lon_min = self.lon_min
        up.lon_max = self.lon_max
        up.dlat = self.dlat
        up.dlon = self.dlon
        up.nodata = self.nodata
        up.isg_format = self.isg_format
        up.ncols = self.ncols
        up.nrows = int(unr)

        up.lat_min = self.lat_min + float(nr_2) * self.dlat
        up.lat_max = self.lat_max
        # ------------------------
        low.model_name = self.model_name
        low.model_type = self.model_type
        low.units = self.units
        low.ref_ell = self.ref_ell

        low.lon_min = self.lon_min
        low.lon_max = self.lon_max
        low.dlat = self.dlat
        low.dlon = self.dlon
        low.nodata = self.nodata
        low.isg_format = self.isg_format
        low.ncols = self.ncols
        low.lat_min = self.lat_min
        low.lat_max = self.lat_min + float(nr_2 - 1) * self.dlat
        low.nrows = int(lnr)

        up.data = up_data
        low.data = low_data

        return low, up

    def convert_qg2tpot(self, m_terrain):
        """
        convert the quasigeoid model to disturbance potential
        T = \zeta \cdot \gamma (Q)
        """
        n_r, n_c = self.data.shape
        m_r, m_c = m_terrain.data.shape

        WGS84 = geo_f.wgs84_ell()

        if n_r == m_r and n_c == m_c:
            # terrain and qgeoid has the same resolution
            for i in range(0, n_r):
                bdeg = self.lat_min + float(i) * self.dlat
                for j in range(0, n_c):
                    h_telluroid = m_terrain.data[i, j] - self.data[i, j]
                    gamma = WGS84.normal_gravity(bdeg, h_telluroid)

                    self.data[i, j] *= gamma

            return True
        else:
            return False

    def convert_tpot2qg(self, m_terrain):
        n_r, n_c = self.data.shape
        m_r, m_c = m_terrain.data.shape

        WGS84 = geo_f.wgs84_ell()

        if n_r == m_r and n_c == m_c:
            # terrain and qgeoid has the same resolution
            for i in range(0, n_r):
                bdeg = self.lat_min + float(i) * self.dlat
                for j in range(0, n_c):
                    zeta = geo_f.get_zeta(
                        bdeg, m_terrain.data[i, j], self.data[i, j], WGS84)
                    self.data[i, j] = zeta

            return True
        else:
            return False

    def tide_conversion(self, from_ts, to_ts, k, h, ell=geo_f.wgs84_ell()):
        """
        k = 0.29525 egm standard
        h = 0.6
        """
        if (from_ts.lower() == to_ts.lower()):
            return False
        else:
            nr, nc = self.data.shape

            for i in range(0, nr):
                bdeg = self.lat_min + float(i) * self.dlat

                # reduction of the geodetic latitude
                psi = math.atan(math.tan(math.radians(bdeg)) /
                                (1. - ell.eccentricity))
                deltaW_ZT = -0.198 * \
                    (3. / 2. * math.sin(psi) * math.sin(psi) - .5)

                if (from_ts.lower() == "tide_free" and to_ts.lower() == "mean_tide"):
                    deltaH = -(1 + k + h) * deltaW_ZT
                elif (from_ts.lower() == "mean_tide" and to_ts.lower() == "tide_free"):
                    deltaH = (1 + k + h) * deltaW_ZT
                elif (from_ts.lower() == "tide_free" and to_ts.lower() == "zero_tide"):
                    deltaH = -(k - h) * deltaW_ZT
                elif (from_ts.lower() == "zero_tide" and to_ts.lower() == "tide_free"):
                    deltaH = (k - h) * deltaW_ZT
                elif (from_ts.lower() == "mean_tide" and to_ts.lower() == "zero_tide"):
                    deltaH = deltaW_ZT
                elif (from_ts.lower() == "zero_tide" and to_ts.lower() == "mean_tide"):
                    deltaH = -deltaW_ZT

                self.data[i, :] = self.data[i, :] - deltaH

            return True

    def join_vertically(self, upper_model, lower_model):
        if abs(upper_model.lat_min - lower_model.lat_max) >= 2 * \
                upper_model.dlat:
            print("Can not merge the models together, vertical gap is too large!\n")
            raise RuntimeError(
                "Isgem_model::join_vertically can not merge the models together, vertical gap is too large!\n")

        if abs(upper_model.dlat - lower_model.dlat) >= 1.0e-09:
            raise RuntimeError(
                "Can not join vertically models: " +
                upper_model.model_name +
                " and " +
                lower_model.model_name +
                ". Different dlat value.")

        if abs(upper_model.dlon - lower_model.dlon) >= 1.0e-09:
            raise RuntimeError(
                "Can not join vertically models: " +
                upper_model.model_name +
                " and " +
                lower_model.model_name +
                ". Different dlon value.")

        if abs(upper_model.ncols != lower_model.ncols):
            raise RuntimeError(
                "Can not join vertically models: " +
                upper_model.model_name +
                " and " +
                lower_model.model_name +
                ". Different number of columns.")

        self.data = np.vstack((lower_model.data, upper_model.data))
        self.nrows, self.ncols = self.data.shape
        self.lat_min = lower_model.lat_min
        self.lon_min = lower_model.lon_min
        self.lon_max = .5 * (lower_model.lon_max + upper_model.lon_max)
        self.dlat = .5 * (lower_model.dlat + upper_model.dlat)
        self.dlon = .5 * (lower_model.dlon + upper_model.dlon)
        self.lat_max = upper_model.lat_max

        self.model_name = upper_model.model_name
        self.model_type = upper_model.model_type
        self.units = upper_model.units
        self.ref_ell = upper_model.ref_ell
        self.nodata = upper_model.nodata
        self.isg_format = upper_model.isg_format

        return self

    def resample_grid(
            self,
            bmin: float,
            bmax: float,
            lmin: float,
            lmax: float,
            delta_b: float,
            delta_l: float,
            fpathout=None):
        """
        creates the resampled grid from original data, interpolation based on surface of the spherical triangles

        bmin,lmin,bmax,lmax in DEG format
        """

        print("function Isgem_model::resample_grid() has no check build in for the input parameters!")

        nr = int(round((bmax - bmin) / delta_b)) + 1
        nc = int(round((lmax - lmin) / delta_l)) + 1

        bvec = np.linspace(bmin, bmax, num=nr, endpoint=True)
        lvec = np.linspace(lmin, lmax, num=nc, endpoint=True)

        resampledmodel = np.zeros((nr, nc))

        for i in range(0, nr):
            for j in range(0, nc):
                interpolatedval = self.interpolate_value(bvec[i], lvec[j])
                resampledmodel[i, j] = interpolatedval

        newmodel = Isgem_model()
        newmodel.model_name = self.model_name
        newmodel.model_type = self.model_type
        newmodel.units = self.units
        newmodel.ref_ell = self.ref_ell
        newmodel.lat_min = bmin
        newmodel.lat_max = bmax
        newmodel.lon_min = lmin
        newmodel.lon_max = lmax
        newmodel.dlat = delta_b
        newmodel.dlon = delta_l
        newmodel.nrows = int(nr)
        newmodel.ncols = int(nc)
        newmodel.nodata = self.nodata
        newmodel.isg_format = self.isg_format
        newmodel.data = resampledmodel

        if (fpathout is not None):
            newmodel.print2file(fpathout)

        return newmodel

    def get_subgrid(self, bmin: float, bmax: float, lmin: float, lmax: float):
        nmin, mmin = self.get_index_of(bmin, lmin)
        nmax, mmax = self.get_index_of(bmax, lmax)

        n_lat_min = self.lat_min + float(nmin) * self.dlat
        n_lat_max = self.lat_min + float(nmax) * self.dlat
        n_lon_min = self.lon_min + float(mmin) * self.dlon
        n_lon_max = self.lon_min + float(mmax) * self.dlon

        new_data = self.data[nmin: nmax][:, mmin: mmax]
        nr, nc = new_data.shape

        newmodel = Isgem_model()
        newmodel.model_name = self.model_name
        newmodel.model_type = self.model_type
        newmodel.units = self.units
        newmodel.ref_ell = self.ref_ell
        newmodel.lat_min = n_lat_min
        newmodel.lat_max = n_lat_max
        newmodel.lon_min = n_lon_min
        newmodel.lon_max = n_lon_max
        newmodel.dlat = self.dlat
        newmodel.dlon = self.dlon
        newmodel.nrows = nr
        newmodel.ncols = nc
        newmodel.nodata = self.nodata
        newmodel.isg_format = self.isg_format
        newmodel.data = new_data

        return newmodel
