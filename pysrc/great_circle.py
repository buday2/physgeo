#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import sin, cos, tan, atan, radians, degrees, asin, acos, atan2, pi


def sign(a): return (a > 0) - (a < 0)


def get_right_interval(angle: float) -> float:
    if (angle > 2. * pi):
        angle -= 2. * pi
    elif (angle < 0.):
        angle += 2. * pi

    return angle


def geodetic1(
        phi1: float,
        lambda1: float,
        alpha1: float,
        s1: float,
        radius=1.0):
    """
    * @brief geodetic1 - Computes the first geodetic problem - for given point \f$ P( \varphi_1, \lambda_1) \f$
    * and given azimuth and length computes the coordinates of the second points
    * \f$ Q( \varphi_2, \lambda_2) \f$ and azimuth from Q to P.
    *
    * @param phi1 - latitude of the point P
    * @param lambda1 - longitude of the point P
    * @param alpha1 - azimuth at P towards Q
    * @param s1 - length on the sphere between P and Q
    *
    * @param phi2 - latitude of the calculated point Q
    * @param lambda2 - longitude of the Q
    * @param alpha2 - azimuth QP
    """
    b1 = radians(phi1)
    l1 = radians(lambda1)
    a1 = radians(alpha1)
    sr = s1 / radius

    b2 = asin(sin(b1) * cos(sr) + cos(b1) * sin(sr) * cos(a1))
    deltal = sign(
        a1) * abs(acos((cos(sr) - sin(b1) * sin(b2)) / (cos(b1) * cos(b2))))

    a = (cos(b1) * sin(a1) / (cos(b2)))
    b = -1. * cos(a1) * cos(deltal) + sin(a1) * sin(deltal) * sin(b1)

    a2 = 2. * pi - atan2(a, b)

    a1 = get_right_interval(a1)
    a2 = get_right_interval(a2)

    return degrees(b2), degrees(l1 + deltal), degrees(a2)


def geodetic2(phi1: float, lambda1: float, phi2: float, lambda2: float, R=1.0):
    """
     * @brief geodetic2 - Comptutes the 2nd geodetic problem, for two given points  \f$ P_1( \varphi_1, \lambda_1) \f$
     * and  \f$ P_2( \varphi_2, \lambda_2) \f$ computes the distance between them on the reference sphere and azimuths
     * toward each other.
     * @param phi1 - latitude of the first given point
     * @param lambda1 - longitude of the first given point
     * @param phi2 - latitude of the second given point
     * @param lambda2 - longitude of the second given point
     * @param R - radius of the reference sphere
     * @param alpha1 - azimuth \f$ \alpha_12 \f$
     * @param alpha2 - azimutn \f$ \alpha_21 \f$
     * @param s1 - length between points P1 and P2 on sphere surface
    """
    # Radius of the used sphere
    #R = 6382083.583 // [m]
    # converting units to rad
    b1 = radians(phi1)
    l1 = radians(lambda1)
    b2 = radians(phi2)
    l2 = radians(lambda2)

    # computations
    dlambda = l2 - l1  # rad
    s1 = acos(sin(b1) * sin(b2) + cos(b1) * cos(b2) * cos(dlambda))    # rad
    a = atan2(cos((b1 - b2) / 2.), sin((b2 + b1) / 2.)
              * tan(dlambda / 2.))  # rad
    b = atan2(sin((b1 - b2) / 2.), cos((b2 + b1) / 2.)
              * tan(dlambda / 2.))  # rad

    a1 = (a + b)          # rad
    a2 = 2. * pi - (a - b)  # rad
    # Conditions azimuth in [0,2 pi]
    a1 = get_right_interval(a1)
    a2 = get_right_interval(a2)

    # results
    alpha1 = degrees(a1)  # DMS
    alpha2 = degrees(a2)  # DMS

    s1 *= R  # [m]
    return alpha1, alpha2, s1
