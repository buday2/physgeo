#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
from math import radians, degrees, pi, trunc
def sign(a): return (a > 0) - (a < 0)


class dms:
    def __init__(self):
        self._sign = float(sign(1))
        self._d = 0
        self._m = 0
        self._s = 0

    def dms(self, d: float, m: float, s: float):
        self._sign = float(sign(d))
        self._d = abs(d)
        self._m = abs(m)
        self._s = abs(s)

    def deg(self, degf: float):
        self._sign = sign(degf)

        dd = abs(degf)
        self._d = trunc(dd)
        self._m = trunc((dd - self._d) * 60.)
        self._s = (dd - self._d - self._m / 60) * 3600

    def grad(self, gradf: float):
        self.deg(gradf * 9. / 10.)

    def rad(self, radf: float):
        self.deg(degrees(radf))

    def dms2deg(self):
        return self._sign * (self._d + self._m / 60. + self._s / 3600.)

    def dms2rad(self):
        return radians(self.dms2deg())

    def dms2gon(self):
        return self.dms2deg() * 10. / 9.

    def __str__(self):
        signum = ""
        if self._sign >= 0:
            signum = "+"
        else:
            signum = "-"

        return signum + str(int(self._d)) + ":" + \
            str(int(self._m)) + ":" + str(self._s)

    def __add__(self, new: dms):
        outval = dms()

        deg1 = self.dms2deg()
        deg2 = new.dms2deg()
        outval.deg(deg1 + deg2)
        return outval

    def __sub__(self, new: dms):
        outval = dms()

        deg1 = self.dms2deg()
        deg2 = new.dms2deg()
        outval.deg(deg1 - deg2)
        return outval
