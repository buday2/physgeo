from math import sqrt, tan, atan, pi, degrees


def lhuilierTheorem(a, b, c, r):
    """
    a,b,c lengths of the triangle sides
    r radius of a reference sphere
    """

    a = a / r
    b = b / r
    c = c / r

    s = .5 * (a + b + c)

    spherical_exces = 4. * \
        atan(sqrt(tan(s / 2) * tan((s - a) / 2) * tan((s - b) / 2) * tan((s - c) / 2)))

    return degrees(spherical_exces)
