# -*- coding: utf-8 -*-

import math
import numpy as np
import sys

# matplotlib
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.gridspec as gridspec


def psi(b1: float, l1: float, b2: float, l2: float) -> float:
    """
    b1,l1,b2,l2 in DEG format
    """

    br1 = math.radians(b1)
    lr1 = math.radians(l1)
    br2 = math.radians(b2)
    lr2 = math.radians(l2)

    return math.acos(math.sin(br1) * math.sin(br2)
                     + math.cos(br1) * math.cos(br2) * math.cos(lr2 - lr1))


def psi_inputs_in_rads(
        br1: float,
        lr1: float,
        br2: float,
        lr2: float) -> float:
    """
    b1,l1,b2,l2 in RAD format
    """
    return math.acos(math.sin(br1) * math.sin(br2)
                     + math.cos(br1) * math.cos(br2) * math.cos(lr2 - lr1))


def stokes_kernel(psi: float) -> float:
    s = math.sin(psi / 2)
    return 1 / s - 4 - 6 * s + 10 * s * s - \
        (3 - 6 * s * s) * math.log(s + s * s)


def np_stokes_kernel(psi):
    s = np.sin(psi / 2.)
    return 1. / s - 4 - 6 * s + 10. * s * s - \
        (3. - 6. * s * s) * np.log(s + s * s)


def hotine_kernel(psi: float) -> float:
    s = 1. / math.sin(psi / 2.)

    return s - math.log(1. + s)


def np_hotine_kernel(psi: float)-> float:
    s = 1. / np.sin(psi / 2.)

    return s - np.log(1. + s)


def kernel_mat(
        b0: float,
        l0: float,
        db: float,
        dl: float,
        dr: int,
        dc_l: int,
        dc_r: int):
    dcol = dc_l + dc_r

    kmat = np.zeros((2 * dr + 1, dcol))

    phi0 = math.radians(b0)
    lam0 = math.radians(l0)
    dlat = math.radians(db)
    dlon = math.radians(dl)

    if dcol % 2 == 0:
        for i in range(0, 2 * dr + 1):
            bi = phi0 + float(i - dr) * dlat

            for j in range(0, dcol):
                li = lam0 + float(j - dc_l) * dlat
                psi = psi_inputs_in_rads(phi0, lam0, bi, li)

                if (i == dr and j == dc_l):
                    continue

                kmat[i, j] = psi
    else:
        for i in range(0, 2 * dr + 1):
            bi = phi0 + float(i - dr) * dlat

            for j in range(0, dcol):
                li = lam0 + float(j - dc_l) * dlat
                psi = psi_inputs_in_rads(phi0, lam0, bi, li)

                if (i == dr and j == dc_r):
                    continue

                kmat[i, j] = psi

    return kmat


def kernel_mat2(
        b0: float,
        l0: float,
        db: float,
        dl: float,
        dr_l: int,
        dr_u: int,
        dc_l: int,
        dc_r: int)-> np.array:
    dcol = dc_l + dc_r
    drow = dr_u + dr_l

    kmat = np.zeros((drow, dcol))

    phi0 = math.radians(b0)
    lam0 = math.radians(l0)
    dlat = math.radians(db)
    dlon = math.radians(dl)

    if dcol % 2 == 0:
        for i in range(0, drow):
            bi = phi0 + float(i - dr_l) * dlat
            db = math.degrees(bi - phi0)
            for j in range(0, dcol):
                li = lam0 + float(j - dc_l) * dlat
                dl = math.degrees(li - lam0)
                psi = psi_inputs_in_rads(phi0, lam0, bi, li)


                kmat[i, j] = psi
    else:
        for i in range(0, drow):
            bi = phi0 + float(i - dr_l) * dlat
            db = math.degrees(bi - phi0)
            for j in range(0, dcol):
                li = lam0 + float(j - dc_r) * dlat
                dl = math.degrees(li - lam0)
                psi = psi_inputs_in_rads(phi0, lam0, bi, li)

                kmat[i, j] = psi
    return kmat


def generate_bl_grid(
        bmin: float,
        bmax: float,
        lmin: float,
        lmax: float,
        num=10):
    bvec = np.linspace(bmin, bmax, num, endpoint=True)
    lvec = np.linspace(lmin, lmax, num, endpoint=True)

    return bvec, lvec


def compute_kernel_grid(bvec: np.array, lvec: np.array)-> np.array:
    nrows = max(bvec.shape)
    ncols = max(lvec.shape)

    kgrid = np.zeros((nrows, ncols))

    b0 = bvec[0]
    l0 = lvec[0]

    for i in range(0, nrows):
        for j in range(0, ncols):
            if i == 0 and j == 0:
                continue
            else:
                kgrid[i, j] = stokes_kernel(psi(b0, l0, bvec[i], lvec[j]))

    return kgrid


def compute_psi_grid(bvec: np.array, lvec: np.array)-> np.array:
    nrows = max(bvec.shape)
    ncols = max(lvec.shape)

    pgrid = np.zeros((nrows, ncols))

    b0 = bvec[0]
    l0 = lvec[0]

    for i in range(0, nrows):
        for j in range(0, ncols):
            pgrid[i, j] = psi(b0, l0, bvec[i], lvec[j])

    return pgrid


def print_npmat(np_array)-> None:
    r, c = np_array.shape

    for i in range(0, r):
        for j in range(0, c):
            print("%2.5f" % np_array[i, j], end="    ")

        print("")
