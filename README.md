This "package" is called [PhysGeo](https://gitlab.com/buday2/physgeo/) and was created by Michal Buday and use for the various purposes of the doctoral thesis. It is distributed under MIT licence.

The package can be divided into 3 major parts. The First part is devoted to compute the synhesis from Global Gravity field models, the second part is dedicated to forward gravity field modelling and the last part is capable to compute the Stokes' and Hotine's integral.

# How to install

In order to be able to use this library for your own scientific computations you first have to install C/C++ compiler to be able to compile the source code. Additional C++ libraries are required:
- **[Armadillo](http://arma.sourceforge.net/)** linear algebra software for the C++ 
- **[openMP](https://www.openmp.org/)** The OpenMP API specification for parallel programming 
- **[hdf5](https://www.hdfgroup.org/downloads/hdf5/)** support of handling hdf5 files 
- **[boost](https://www.boost.org/users/download/)** C++ library 

Python libraries:
- **[basemap](https://matplotlib.org/basemap/users/)** for displaying the computed data
- **matplotlib** 
- **numpy**

Use cmake to create makefile (the PhysGeo project can be built in any folder the user desires):

```{: .language-bash}
    cd build
    cmake ..    
    make
```

# Global Gravity Field Models

## Glogal Gravity Field Models (**--computation-type,-c** ggfm)

- Computation options (**--parameters,-p** option)
    - the parameter of the Earth's gravity field to calculate, more than one option is actually allowed ( -p gpot_w gpot_t )

- Computational options for GGFMs
    - 'gpot_v' (gravity potential)
    - 'gpot_w' (gravitational potential)
    - 'gpot_t' (disturbing potential)
    - 'def_vert_eta' (deflection of the vertical - east-west)
    - 'def_vert_xi' (deflection of the vertical - north-south)
    - 'def_vert' (deflection of the vertical)
    - 'grav_anom_sa' (gravity anomaly in spherical form)
    - 'grav_dist_sa' (gravity disturbance in spherical form)
    - 'height_anomaly' (height anomaly)
    - 'grav_gy'
    - 'grav_gx'
    - 'grav_gz'
    - 'terrain' (grid options only)

- Using algorithm for fully normalized assoc. Legen. poly. (**--algorithm,-a** option) 
    - 'stan_mod' - standard method of computation, numerically stable up to degree/order around 720
    - 'standard' - same as 'stan_mod' but different algorithm for the fnALFs is used
    - 'tmfcm' - modified method of computation, numerically stable up to degree/order around 2700
    - 'tmfcm_2nd' - modified method of computation, numerically stable up to degree/order around 2700

The second option is to used 128bit <long double> to extend the numerical stability up to ultra-high degrees/orders, but it is slower. Not recommended for models upto degree/order 2700. In order to use <long double>, the ```-c ggfm ld``` must be called, ld = long double

- solutionType (option **--solution,-s**) stands for the option between
    - 'sp' - single point solution option
    - 'grid' - the grid is output, but option **--boundaries,-b** is needed
    - 'ongrid' - performs the computation on DEM using the gradient approach, DEM must be in ISGEM model. 

- Degree order range (**degree-range,-d**) if the cut for degree is required
    - Specify the range of GGFM model degree [min, max]
    - otherwise the maximum range is used

- set the grid boundaries for "**-s** grid" option
    - **--boundaries,-b** "bmin bmax lmin lmax height deltab deltal"
    - all values in DEG format, except h argument
    - passed as single string argument, parser automatically handles it
    - **bmin, bmax** is latitude range
    - **lmin, lmax** is longitude range
    - **deltab** step in latitude direction
    - **deltal** step in longitude direction
    - **h** reference ellipsoidal height

- path to the GGFM model (option **--model,-m**)
    - default format is International Centre for Global Earth Models (ICGEM) style
    - downloaded from webpage [ICGEM](http://icgem.gfz-potsdam.de/tom_longtime) in gfc format
    - but if the option is called -m $path2ggfm hdf5, the hdf5 format can be used, see the source code
    - in ggfm.h file, still the header is needed

- path to file with points, if single point option is called (**--file,-f**)
    - file in format **PTID BDEG LDEG HELL** (+more columns, but they are ignored)

- output option (**--output,-o**)
    - if it is not used, default name is **result.txt** for all computations

- Permanent Tide system managment is option (**--tide-system,-n**)
    - if it is not used, the tide system of the model's tide system is used (default value)
    - options are "mean_tide", "tide_free", "zero_tide"

- ellipsoidGeometry is the reference ellipsoid for the computations (**--ellipsoid,-e**)
    - 'wgs84' is default
    - 'grs80' or 'bessel' ellipsoid
    - if the user needs his own, must be defined separately (see geodetic_funcions.h and my_parser.cpp)

- Love number for $C_{20}$ term transformation  (option **--love-number,-l**)
    - if it is not set, the value 0.29525 is used

### Examples 
- single point solution (**--solution,-s** sp)

```{: .language-bash}
    path2points="data/CZEPOS.txt" # points of the permanent GNSS network in Czechia
    path2model="model/ggm_GRACE16.gfc"
    output="CZEPOS_gpotW.txt"
    ./PhysGeo -c ggfm -a tmfcm -s sp -m $path2ggfm -p gpot_w -f ${path2points} -n zero_tide -o ${output}
```


Same computation but the GGFM is in hdf5 format with a separate header file, the program automatically searches for the header file

```{: .language-bash}
    path2model="model/ggm_GRACE16.hdf5"

    ./PhysGeo -c ggfm -a tmfcm -s sp -m $path2ggfm hdf5 -p gpot_w -f ${path2points} \
    -n zero_tide -o ${output}
```


- grid option (-s grid)
    - grid with the constant ellispiodal height (-b option)


```{: .language-bash}
    ./PhysGeo -c ggfm -a tmfcm -s grid -m $path2ggfm hdf5 -p height_anomaly \
    -b "-90 90 0 360 100 5 5" -n zero_tide
```

Computes the grid around the globe in 5 degrees step and constant ellipsoidal height 100 m.

```{: .language-bash}
    # ongrid option (-s ongrid) - gradient approach
    DEM="data/ETOPO1.isg"
    ./PhysGeo -c ggfm -s ongrid -f ${DEM} -m ${path2model} -a tmfcm -p grav_anom_sa \
    -n zero_tide -o ETOPO1_Delta_g.isg 
```

# Gravity field modelling from the Digital terrain model
## Forward gravity field modelling  (**--computation-type,-c** tess)

- Computation options (**--parameters,-p** option)
    - 'gpot' - gravity potential $V$
    - 'grav_r' - first radial derivative $V^{\prime}_{r}$
    - 'grav_b' - $V^{\prime}_{B}$
    - 'grav_l' - $V^{\prime}_{L}$
    - 'marussi_rr' - part of the Marussi tensor $V^{\prime \prime}_{rr}$
    - 'marussi_rb' - part of the Marussi tensor $V^{\prime \prime}_{rB}$
    - 'marussi_rl' - part of the Marussi tensor $V^{\prime \prime}_{rL}$
    - 'marussi_bb' - part of the Marussi tensor $V^{\prime \prime}_{BB}$
    - 'marussi_ll' - part of the Marussi tensor $V^{\prime \prime}_{LL}$
    - 'marussi_bl' - part of the Marussi tensor $V^{\prime \prime}_{BL}$

    
    

- solutionType (option **--solution,-s**) stands for the option between
    - 'sp' - single point solution option
    - 'grid' - the grid is output (computation is performed on the given DEM)

- density value (**--density,-d** option)
    - in this case, d stands for density
    - default value is 2670 $kg \cdot m^{-3}$
    - or also ISGEM format density model can be used
    - but the model has to have the same resolution and the boundaries as the DEM
    - example: -d ${density} isgem , first argument is the path second argument is format of the density model

- integration radius (**--radius,-r** option)
    - r radius of the integration , 111320 $m$ approx 1 degree (default), input in $m$

- **--model,-m** path to DEM
    - of two models are given, the first path serves as a model
    - of the upper layer, second model represents lower layer
    - both layers must have the same spatial resolution, boundaries, number
    - of columns, rows, etc.

- -**--extra-data** option
    - additional information to the DEM
    - for example the format of the inputs

- output option (**--output,-o**)
    - if it is not used, default name is **result.txt** for all computations

Following example is for computing the residual terrain modelling (but model GMTED is not included)

```{: .language-bash}

    upper_lay="GMTED2010_150_mean_ME_ell_height.isg"
    lower_lay="DEM_GMTED-terrain.txt"
    density="eu_density.isg"

    ./PhysGeo -c tess -s grid -r 18520 -p gpot -m ${upper_lay} ${lower_lay} \
    -n isgem -d ${density} isgem
```

```{: .language-bash}
    # using ETOPO1 for grav_r computaions for CZEPOS
    model="data/ETOPO1.isg"
    points="data/CZEPOS.txt"
    ./PhysGeo -c tess p grav_r -s sp -f ${points} -m ${model} \
    -r 125000 -o CZEPOS_gravr_from_ETOPI1.txt
```


## Solving Stokes or Hotine integrals via convolution ( -c geoid ) 

- Computation options (**--parameters,-p** option)
    - 'hotine' - solving Hotine's integral, using grav. dist. sa
    - 'stokes' - solving Stokes' integral (gravity anomalies)
    - 'hotine_g1' - first term after the g.dist. member in fixed GBVP
    - 'stokes_g1' - first term after the g.anom. member in fixed GBVP
    - see my_parser.cpp

- **--extend-data,-x** extend data (needed if the integration is around the whole globe)
    - because the integrals are solved by convolution, for the whole globe
    the data must be extended, that means the "western" hemisphere is copied behind the eastern and vice versa. The extended grid then looks (E W E W) the results are then cut to original resolution
    - **-x** true or **--extend-data** true

- **--radius,r** option 
    - can be used to set the radius of the reference sphere and the integration
    - radius, the default sphere radius is $6.3710087714e+6$ and the integration
    - radius is 1 DEG based on the ref. sphere radius. The arguments are provided as
    - **-r** SPHRADIUS INTRADIUS or **-r** SPHRADIUS

- **--boundaries,-b** option
    - if the integration around the globe is needed and only results for certain geo.
    latitudes is needed, phi_min phi_max in DEG format can be used
    - -b "40 51.35" as an example, the output grid will be in those boundaries

- output option (**--output,-o**)
    - if it is not used, default name is **result.txt** for all computations

- **--model,-m** option 
    - stands for model or models, based on the **-p** option (hotine_g1 requireMateřská společnost (Konsolidováno)

For whole world and the area of the Czech Republic

```{: .language-bash}
    gdist="gdist_MT_EIGEN-10min_grid.txt" # can be easily computed using the ggfm section, step 10' x 10' for example
    ./PhysGeo -c geoid -p hotine -s grid -x true -m ${gdist} -b "47.5 51.5" \
    -o zeta0_EIGEN6C4_MT.txt
```
 
## Structure of the project explanation

Project **PhysGeo** is structured as following.
## See examples:

There are multiple examples stored in the folder *examples/*:

- **fgfm.md**  - Examples related to the "Forward gravity field moddeling"
- **ggfm.md** - Examples related to the "Global gravity field modeling"
- **geoid.md** - Examples related to the Stokes' and Hotine's integrals
## Citation of the doctoral thesis
 ```{: .language-latex}
    BUDAY, Michal. Geophysical methods of integration of the local vertical datums into World
    Height System. Brno, 2020, 188 p. Doctoral thesis. Brno University of Technology, Faculty
    of Civil Engineering, Department of Geodesy. Supervised by prof. Ing. Viliam Vatrt, DrSC.
```
